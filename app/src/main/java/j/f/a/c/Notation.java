package j.f.a.c;

import n.n.c.Intrinsics;

/* compiled from: Notation.kt */
public final class Notation {
    public final char a;
    public final String b;
    public final boolean c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [?[OBJECT, ARRAY], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Notation) {
                Notation notation = (Notation) obj;
                if (!(notation.a == 0) || !Intrinsics.a((Object) null, (Object) notation.b) || !(!notation.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        return "Notation(character=\u0000, characterSet=null, isOptional=false)";
    }
}
