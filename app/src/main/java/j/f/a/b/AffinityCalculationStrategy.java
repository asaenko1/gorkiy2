package j.f.a.b;

/* compiled from: AffinityCalculationStrategy.kt */
public enum AffinityCalculationStrategy {
    WHOLE_STRING,
    PREFIX,
    CAPACITY,
    EXTRACTED_VALUE_CAPACITY
}
