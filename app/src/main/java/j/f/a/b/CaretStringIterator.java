package j.f.a.b;

import j.f.a.c.CaretString;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;

/* compiled from: CaretStringIterator.kt */
public class CaretStringIterator {
    public final CaretString a;
    public int b;

    public /* synthetic */ CaretStringIterator(CaretString caretString, int i2, int i3) {
        i2 = (i3 & 2) != 0 ? 0 : i2;
        if (caretString != null) {
            this.a = caretString;
            this.b = i2;
            return;
        }
        Intrinsics.a("caretString");
        throw null;
    }

    public boolean a() {
        return this.b < this.a.b;
    }

    public boolean b() {
        int ordinal = this.a.c.ordinal();
        if (ordinal == 0) {
            int i2 = this.b;
            int i3 = this.a.b;
            if (i2 <= i3) {
                return true;
            }
            if (i2 == 0 && i3 == 0) {
                return true;
            }
            return false;
        } else if (ordinal != 1) {
            throw new NoWhenBranchMatchedException();
        } else if (this.b < this.a.b) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [char[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Character c() {
        if (this.b >= this.a.a.length()) {
            return null;
        }
        String str = this.a.a;
        if (str != null) {
            char[] charArray = str.toCharArray();
            Intrinsics.a((Object) charArray, "(this as java.lang.String).toCharArray()");
            int i2 = this.b;
            char c = charArray[i2];
            this.b = i2 + 1;
            return Character.valueOf(c);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
