package j.e.a;

import l.b.Observable;
import l.b.Observer;

public abstract class InitialValueObservable<T> extends Observable<T> {

    public final class a extends Observable<T> {
        public a() {
        }

        public void b(Observer<? super T> observer) {
            InitialValueObservable.this.c((Observer) observer);
        }
    }

    public final void b(Observer<? super T> observer) {
        c((Observer) observer);
        observer.b(c());
    }

    public abstract T c();

    public abstract void c(Observer<? super T> observer);
}
