package j.e.b;

import l.b.Observable;
import l.b.t.Consumer;

public abstract class Relay<T> extends Observable<T> implements Consumer<T> {
    public abstract void a(Object obj);
}
