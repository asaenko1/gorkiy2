package j.b.a.m.o.c;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import java.nio.ByteBuffer;

public final class ByteBufferBitmapImageDecoderResourceDecoder implements ResourceDecoder<ByteBuffer, Bitmap> {
    public final BitmapImageDecoderResourceDecoder a = new BitmapImageDecoderResourceDecoder();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.o.ImageDecoderResourceDecoder.a(android.graphics.ImageDecoder$Source, int, int, j.b.a.m.g):j.b.a.m.m.Resource<T>
     arg types: [android.graphics.ImageDecoder$Source, int, int, j.b.a.m.Options]
     candidates:
      j.b.a.m.o.ImageDecoderResourceDecoder.a(java.lang.Object, int, int, j.b.a.m.Options):j.b.a.m.m.Resource
      j.b.a.m.ResourceDecoder.a(java.lang.Object, int, int, j.b.a.m.g):j.b.a.m.m.Resource<Z>
      j.b.a.m.o.ImageDecoderResourceDecoder.a(android.graphics.ImageDecoder$Source, int, int, j.b.a.m.g):j.b.a.m.m.Resource<T> */
    public Resource a(Object obj, int i2, int i3, Options options) {
        return this.a.a(ImageDecoder.createSource((ByteBuffer) obj), i2, i3, (g) options);
    }

    public boolean a(Object obj, Options options) {
        ByteBuffer byteBuffer = (ByteBuffer) obj;
        return true;
    }
}
