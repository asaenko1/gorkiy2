package j.b.a.m.n.y;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import i.b.k.ResourcesFlusher;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.Options;
import j.b.a.m.g;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.n.ModelLoaderFactory;
import j.b.a.m.n.r;
import j.b.a.r.ObjectKey;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public final class QMediaStoreUriLoader<DataT> implements ModelLoader<Uri, DataT> {
    public final Context a;
    public final ModelLoader<File, DataT> b;
    public final ModelLoader<Uri, DataT> c;
    public final Class<DataT> d;

    public static abstract class a<DataT> implements ModelLoaderFactory<Uri, DataT> {
        public final Context a;
        public final Class<DataT> b;

        public a(Context context, Class<DataT> cls) {
            this.a = context;
            this.b = cls;
        }

        public final ModelLoader<Uri, DataT> a(r rVar) {
            return new QMediaStoreUriLoader(this.a, rVar.a(File.class, this.b), rVar.a(Uri.class, this.b), this.b);
        }
    }

    public static final class b extends a<ParcelFileDescriptor> {
        public b(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    public static final class c extends a<InputStream> {
        public c(Context context) {
            super(context, InputStream.class);
        }
    }

    public QMediaStoreUriLoader(Context context, ModelLoader<File, DataT> modelLoader, ModelLoader<Uri, DataT> modelLoader2, Class<DataT> cls) {
        this.a = context.getApplicationContext();
        this.b = modelLoader;
        this.c = modelLoader2;
        this.d = cls;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri = (Uri) obj;
        return new ModelLoader.a(new ObjectKey(uri), new d(this.a, this.b, this.c, uri, i2, i3, options, this.d));
    }

    public boolean a(Object obj) {
        return Build.VERSION.SDK_INT >= 29 && ResourcesFlusher.a((Uri) obj);
    }

    public static final class d<DataT> implements DataFetcher<DataT> {

        /* renamed from: l  reason: collision with root package name */
        public static final String[] f1695l = {"_data"};
        public final Context b;
        public final ModelLoader<File, DataT> c;
        public final ModelLoader<Uri, DataT> d;

        /* renamed from: e  reason: collision with root package name */
        public final Uri f1696e;

        /* renamed from: f  reason: collision with root package name */
        public final int f1697f;
        public final int g;
        public final Options h;

        /* renamed from: i  reason: collision with root package name */
        public final Class<DataT> f1698i;

        /* renamed from: j  reason: collision with root package name */
        public volatile boolean f1699j;

        /* renamed from: k  reason: collision with root package name */
        public volatile DataFetcher<DataT> f1700k;

        public d(Context context, ModelLoader<File, DataT> modelLoader, ModelLoader<Uri, DataT> modelLoader2, Uri uri, int i2, int i3, g gVar, Class<DataT> cls) {
            this.b = context.getApplicationContext();
            this.c = modelLoader;
            this.d = modelLoader2;
            this.f1696e = uri;
            this.f1697f = i2;
            this.g = i3;
            this.h = gVar;
            this.f1698i = cls;
        }

        public void a(f fVar, DataFetcher.a<? super DataT> aVar) {
            try {
                DataFetcher<DataT> d2 = d();
                if (d2 == null) {
                    aVar.a((Exception) new IllegalArgumentException("Failed to build fetcher for: " + this.f1696e));
                    return;
                }
                this.f1700k = d2;
                if (this.f1699j) {
                    cancel();
                } else {
                    d2.a(fVar, aVar);
                }
            } catch (FileNotFoundException e2) {
                aVar.a((Exception) e2);
            }
        }

        public void b() {
            DataFetcher<DataT> dataFetcher = this.f1700k;
            if (dataFetcher != null) {
                dataFetcher.b();
            }
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
            this.f1699j = true;
            DataFetcher<DataT> dataFetcher = this.f1700k;
            if (dataFetcher != null) {
                dataFetcher.cancel();
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final j.b.a.m.l.DataFetcher<DataT> d() {
            /*
                r9 = this;
                boolean r0 = android.os.Environment.isExternalStorageLegacy()
                r1 = 0
                if (r0 == 0) goto L_0x007e
                j.b.a.m.n.ModelLoader<java.io.File, DataT> r0 = r9.c
                android.net.Uri r8 = r9.f1696e
                android.content.Context r2 = r9.b     // Catch:{ all -> 0x0077 }
                android.content.ContentResolver r2 = r2.getContentResolver()     // Catch:{ all -> 0x0077 }
                java.lang.String[] r4 = j.b.a.m.n.y.QMediaStoreUriLoader.d.f1695l     // Catch:{ all -> 0x0077 }
                r5 = 0
                r6 = 0
                r7 = 0
                r3 = r8
                android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0077 }
                if (r2 == 0) goto L_0x005d
                boolean r3 = r2.moveToFirst()     // Catch:{ all -> 0x0074 }
                if (r3 == 0) goto L_0x005d
                java.lang.String r3 = "_data"
                int r3 = r2.getColumnIndexOrThrow(r3)     // Catch:{ all -> 0x0074 }
                java.lang.String r3 = r2.getString(r3)     // Catch:{ all -> 0x0074 }
                boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x0074 }
                if (r4 != 0) goto L_0x0046
                java.io.File r4 = new java.io.File     // Catch:{ all -> 0x0074 }
                r4.<init>(r3)     // Catch:{ all -> 0x0074 }
                r2.close()
                int r2 = r9.f1697f
                int r3 = r9.g
                j.b.a.m.Options r5 = r9.h
                j.b.a.m.n.ModelLoader$a r0 = r0.a(r4, r2, r3, r5)
                goto L_0x00a2
            L_0x0046:
                java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch:{ all -> 0x0074 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
                r1.<init>()     // Catch:{ all -> 0x0074 }
                java.lang.String r3 = "File path was empty in media store for: "
                r1.append(r3)     // Catch:{ all -> 0x0074 }
                r1.append(r8)     // Catch:{ all -> 0x0074 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0074 }
                r0.<init>(r1)     // Catch:{ all -> 0x0074 }
                throw r0     // Catch:{ all -> 0x0074 }
            L_0x005d:
                java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch:{ all -> 0x0074 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
                r1.<init>()     // Catch:{ all -> 0x0074 }
                java.lang.String r3 = "Failed to media store entry for: "
                r1.append(r3)     // Catch:{ all -> 0x0074 }
                r1.append(r8)     // Catch:{ all -> 0x0074 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0074 }
                r0.<init>(r1)     // Catch:{ all -> 0x0074 }
                throw r0     // Catch:{ all -> 0x0074 }
            L_0x0074:
                r0 = move-exception
                r1 = r2
                goto L_0x0078
            L_0x0077:
                r0 = move-exception
            L_0x0078:
                if (r1 == 0) goto L_0x007d
                r1.close()
            L_0x007d:
                throw r0
            L_0x007e:
                android.content.Context r0 = r9.b
                java.lang.String r2 = "android.permission.ACCESS_MEDIA_LOCATION"
                int r0 = r0.checkSelfPermission(r2)
                if (r0 != 0) goto L_0x008a
                r0 = 1
                goto L_0x008b
            L_0x008a:
                r0 = 0
            L_0x008b:
                if (r0 == 0) goto L_0x0094
                android.net.Uri r0 = r9.f1696e
                android.net.Uri r0 = android.provider.MediaStore.setRequireOriginal(r0)
                goto L_0x0096
            L_0x0094:
                android.net.Uri r0 = r9.f1696e
            L_0x0096:
                j.b.a.m.n.ModelLoader<android.net.Uri, DataT> r2 = r9.d
                int r3 = r9.f1697f
                int r4 = r9.g
                j.b.a.m.Options r5 = r9.h
                j.b.a.m.n.ModelLoader$a r0 = r2.a(r0, r3, r4, r5)
            L_0x00a2:
                if (r0 == 0) goto L_0x00a6
                j.b.a.m.l.DataFetcher<Data> r1 = r0.c
            L_0x00a6:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.n.y.QMediaStoreUriLoader.d.d():j.b.a.m.l.DataFetcher");
        }

        public Class<DataT> a() {
            return this.f1698i;
        }
    }
}
