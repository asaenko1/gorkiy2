package j.b.a.m.m;

import android.util.Log;
import com.bumptech.glide.Registry;
import j.b.a.m.Encoder;
import j.b.a.m.a;
import j.b.a.m.e;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.m.DataFetcherGenerator;
import j.b.a.m.m.g;
import j.b.a.m.n.ModelLoader;
import j.b.a.s.LogTime;
import java.util.Collections;
import java.util.List;

public class SourceGenerator implements DataFetcherGenerator, DataFetcherGenerator.a {
    public final DecodeHelper<?> b;
    public final DataFetcherGenerator.a c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public DataCacheGenerator f1673e;

    /* renamed from: f  reason: collision with root package name */
    public Object f1674f;
    public volatile ModelLoader.a<?> g;
    public DataCacheKey h;

    public SourceGenerator(DecodeHelper<?> decodeHelper, g.a aVar) {
        this.b = decodeHelper;
        this.c = aVar;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r6v1, types: [j.b.a.m.m.c0.DiskCache$b, j.b.a.m.m.DataCacheWriter] */
    public boolean a() {
        Object obj = this.f1674f;
        if (obj != null) {
            this.f1674f = null;
            long a = LogTime.a();
            try {
                Encoder a2 = this.b.c.b.b.a(obj.getClass());
                if (a2 != null) {
                    ? dataCacheWriter = new DataCacheWriter(a2, obj, this.b.f1612i);
                    this.h = new DataCacheKey(this.g.a, this.b.f1617n);
                    this.b.b().a(this.h, dataCacheWriter);
                    if (Log.isLoggable("SourceGenerator", 2)) {
                        Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.h + ", data: " + obj + ", encoder: " + a2 + ", duration: " + LogTime.a(a));
                    }
                    this.g.c.b();
                    this.f1673e = new DataCacheGenerator(Collections.singletonList(this.g.a), this.b, this);
                } else {
                    throw new Registry.NoSourceEncoderAvailableException(obj.getClass());
                }
            } catch (Throwable th) {
                this.g.c.b();
                throw th;
            }
        }
        DataCacheGenerator dataCacheGenerator = this.f1673e;
        if (dataCacheGenerator != null && dataCacheGenerator.a()) {
            return true;
        }
        this.f1673e = null;
        this.g = null;
        boolean z = false;
        while (!z) {
            if (!(this.d < this.b.c().size())) {
                break;
            }
            List<ModelLoader.a<?>> c2 = this.b.c();
            int i2 = this.d;
            this.d = i2 + 1;
            this.g = c2.get(i2);
            if (this.g != null && (this.b.f1619p.a(this.g.c.c()) || this.b.c(this.g.c.a()))) {
                this.g.c.a(this.b.f1618o, new SourceGenerator0(this, this.g));
                z = true;
            }
        }
        return z;
    }

    public void cancel() {
        ModelLoader.a<?> aVar = this.g;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    public void f() {
        throw new UnsupportedOperationException();
    }

    public void a(e eVar, Object obj, DataFetcher<?> dataFetcher, a aVar, e eVar2) {
        this.c.a(eVar, obj, dataFetcher, this.g.c.c(), eVar);
    }

    public void a(e eVar, Exception exc, DataFetcher<?> dataFetcher, a aVar) {
        this.c.a(eVar, exc, dataFetcher, this.g.c.c());
    }
}
