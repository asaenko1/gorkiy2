package j.b.a.m.m;

import j.b.a.m.Key;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.m.DataFetcherGenerator;
import j.b.a.m.n.ModelLoader;

/* compiled from: SourceGenerator */
public class SourceGenerator0 implements DataFetcher.a<Object> {
    public final /* synthetic */ ModelLoader.a b;
    public final /* synthetic */ SourceGenerator c;

    public SourceGenerator0(SourceGenerator sourceGenerator, ModelLoader.a aVar) {
        this.c = sourceGenerator;
        this.b = aVar;
    }

    public void a(Object obj) {
        SourceGenerator sourceGenerator = this.c;
        ModelLoader.a<?> aVar = this.b;
        ModelLoader.a<?> aVar2 = sourceGenerator.g;
        if (aVar2 != null && aVar2 == aVar) {
            SourceGenerator sourceGenerator2 = this.c;
            ModelLoader.a aVar3 = this.b;
            DiskCacheStrategy diskCacheStrategy = sourceGenerator2.b.f1619p;
            if (obj == null || !diskCacheStrategy.a(aVar3.c.c())) {
                DataFetcherGenerator.a aVar4 = sourceGenerator2.c;
                Key key = aVar3.a;
                DataFetcher<Data> dataFetcher = aVar3.c;
                aVar4.a(key, obj, dataFetcher, dataFetcher.c(), sourceGenerator2.h);
                return;
            }
            sourceGenerator2.f1674f = obj;
            sourceGenerator2.c.f();
        }
    }

    public void a(Exception exc) {
        SourceGenerator sourceGenerator = this.c;
        ModelLoader.a<?> aVar = this.b;
        ModelLoader.a<?> aVar2 = sourceGenerator.g;
        if (aVar2 != null && aVar2 == aVar) {
            SourceGenerator sourceGenerator2 = this.c;
            ModelLoader.a aVar3 = this.b;
            DataFetcherGenerator.a aVar4 = sourceGenerator2.c;
            DataCacheKey dataCacheKey = sourceGenerator2.h;
            DataFetcher<Data> dataFetcher = aVar3.c;
            aVar4.a(dataCacheKey, exc, dataFetcher, dataFetcher.c());
        }
    }
}
