package j.b.a.m.n.y;

import android.content.Context;
import android.net.Uri;
import i.b.k.ResourcesFlusher;
import j.b.a.m.Options;
import j.b.a.m.l.p.ThumbFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.n.ModelLoaderFactory;
import j.b.a.m.n.r;
import j.b.a.m.o.c.VideoDecoder;
import j.b.a.r.ObjectKey;
import java.io.InputStream;

public class MediaStoreVideoThumbLoader implements ModelLoader<Uri, InputStream> {
    public final Context a;

    public static class a implements ModelLoaderFactory<Uri, InputStream> {
        public final Context a;

        public a(Context context) {
            this.a = context;
        }

        public ModelLoader<Uri, InputStream> a(r rVar) {
            return new MediaStoreVideoThumbLoader(this.a);
        }
    }

    public MediaStoreVideoThumbLoader(Context context) {
        this.a = context.getApplicationContext();
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri = (Uri) obj;
        if (ResourcesFlusher.a(i2, i3)) {
            Long l2 = (Long) options.a(VideoDecoder.d);
            if (l2 != null && l2.longValue() == -1) {
                ObjectKey objectKey = new ObjectKey(uri);
                Context context = this.a;
                return new ModelLoader.a(objectKey, ThumbFetcher.a(context, uri, new ThumbFetcher.b(context.getContentResolver())));
            }
        }
        return null;
    }

    public boolean a(Object obj) {
        Uri uri = (Uri) obj;
        return ResourcesFlusher.a(uri) && uri.getPathSegments().contains("video");
    }
}
