package j.b.a.m.m;

import j.b.a.m.m.ActiveResources;

/* compiled from: ActiveResources */
public class ActiveResources0 implements Runnable {
    public final /* synthetic */ ActiveResources b;

    public ActiveResources0(ActiveResources activeResources) {
        this.b = activeResources;
    }

    public void run() {
        ActiveResources activeResources = this.b;
        if (activeResources != null) {
            while (true) {
                try {
                    activeResources.a((ActiveResources.b) activeResources.c.remove());
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                }
            }
        } else {
            throw null;
        }
    }
}
