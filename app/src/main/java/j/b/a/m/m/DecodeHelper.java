package j.b.a.m.m;

import com.bumptech.glide.Registry;
import j.b.a.GlideContext;
import j.b.a.Priority;
import j.b.a.m.Key;
import j.b.a.m.Options;
import j.b.a.m.Transformation;
import j.b.a.m.e;
import j.b.a.m.m.DecodeJob;
import j.b.a.m.m.Engine;
import j.b.a.m.m.c0.DiskCache;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.o.UnitTransformation;
import j.b.a.p.LoadPathCache;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class DecodeHelper<Transcode> {
    public final List<ModelLoader.a<?>> a = new ArrayList();
    public final List<e> b = new ArrayList();
    public GlideContext c;
    public Object d;

    /* renamed from: e  reason: collision with root package name */
    public int f1610e;

    /* renamed from: f  reason: collision with root package name */
    public int f1611f;
    public Class<?> g;
    public DecodeJob.d h;

    /* renamed from: i  reason: collision with root package name */
    public Options f1612i;

    /* renamed from: j  reason: collision with root package name */
    public Map<Class<?>, Transformation<?>> f1613j;

    /* renamed from: k  reason: collision with root package name */
    public Class<Transcode> f1614k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f1615l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f1616m;

    /* renamed from: n  reason: collision with root package name */
    public Key f1617n;

    /* renamed from: o  reason: collision with root package name */
    public Priority f1618o;

    /* renamed from: p  reason: collision with root package name */
    public DiskCacheStrategy f1619p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1620q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f1621r;

    public <Data> LoadPath<Data, ?, Transcode> a(Class<Data> cls) {
        Class<Data> cls2 = cls;
        Registry registry = this.c.b;
        Class<?> cls3 = this.g;
        Class<Transcode> cls4 = this.f1614k;
        LoadPath<Data, ?, Transcode> a2 = registry.f370i.a(cls2, cls3, cls4);
        if (registry.f370i == null) {
            throw null;
        } else if (LoadPathCache.c.equals(a2)) {
            return null;
        } else {
            if (a2 == null) {
                ArrayList arrayList = new ArrayList();
                Iterator it = ((ArrayList) registry.c.b(cls2, cls3)).iterator();
                while (it.hasNext()) {
                    Class cls5 = (Class) it.next();
                    Iterator it2 = ((ArrayList) registry.f369f.b(cls5, cls4)).iterator();
                    while (it2.hasNext()) {
                        Class cls6 = (Class) it2.next();
                        DecodePath decodePath = r1;
                        DecodePath decodePath2 = new DecodePath(cls, cls5, cls6, registry.c.a(cls2, cls5), registry.f369f.a(cls5, cls6), registry.f371j);
                        arrayList.add(decodePath);
                    }
                }
                if (arrayList.isEmpty()) {
                    a2 = null;
                } else {
                    a2 = new LoadPath<>(cls, cls3, cls4, arrayList, registry.f371j);
                }
                registry.f370i.a(cls2, cls3, cls4, a2);
            }
            return a2;
        }
    }

    public DiskCache b() {
        return ((Engine.c) this.h).a();
    }

    public boolean c(Class<?> cls) {
        return a(cls) != null;
    }

    public <Z> Transformation<Z> b(Class<Z> cls) {
        Transformation<Z> transformation = this.f1613j.get(cls);
        if (transformation == null) {
            Iterator<Map.Entry<Class<?>, Transformation<?>>> it = this.f1613j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    transformation = (Transformation) next.getValue();
                    break;
                }
            }
        }
        if (transformation != null) {
            return transformation;
        }
        if (!this.f1613j.isEmpty() || !this.f1620q) {
            return (UnitTransformation) UnitTransformation.b;
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    public List<ModelLoader.a<?>> c() {
        if (!this.f1615l) {
            this.f1615l = true;
            this.a.clear();
            List a2 = this.c.b.a(this.d);
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ModelLoader.a a3 = ((ModelLoader) a2.get(i2)).a(this.d, this.f1610e, this.f1611f, this.f1612i);
                if (a3 != null) {
                    this.a.add(a3);
                }
            }
        }
        return this.a;
    }

    public List<e> a() {
        if (!this.f1616m) {
            this.f1616m = true;
            this.b.clear();
            List<ModelLoader.a<?>> c2 = c();
            int size = c2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ModelLoader.a aVar = c2.get(i2);
                if (!this.b.contains(aVar.a)) {
                    this.b.add(aVar.a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }
}
