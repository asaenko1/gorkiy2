package j.b.a.m.m.b0;

public final class ByteArrayAdapter implements ArrayAdapterInterface<byte[]> {
    public int a(Object obj) {
        return ((byte[]) obj).length;
    }

    public String a() {
        return "ByteArrayPool";
    }

    public int b() {
        return 1;
    }

    public Object newArray(int i2) {
        return new byte[i2];
    }
}
