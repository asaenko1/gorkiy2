package j.b.a.m.m;

import j.b.a.m.e;
import java.util.HashMap;
import java.util.Map;

public final class Jobs {
    public final Map<e, EngineJob<?>> a = new HashMap();
    public final Map<e, EngineJob<?>> b = new HashMap();

    public final Map<e, EngineJob<?>> a(boolean z) {
        return z ? this.b : this.a;
    }
}
