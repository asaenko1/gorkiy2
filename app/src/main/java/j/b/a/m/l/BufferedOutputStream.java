package j.b.a.m.l;

import com.crashlytics.android.core.LogFileManager;
import j.b.a.m.m.b0.ArrayPool;
import java.io.OutputStream;

public final class BufferedOutputStream extends OutputStream {
    public final OutputStream b;
    public byte[] c;
    public ArrayPool d;

    /* renamed from: e  reason: collision with root package name */
    public int f1598e;

    public BufferedOutputStream(OutputStream outputStream, ArrayPool arrayPool) {
        this.b = super;
        this.d = arrayPool;
        this.c = (byte[]) arrayPool.b(LogFileManager.MAX_LOG_SIZE, byte[].class);
    }

    /* JADX INFO: finally extract failed */
    public void close() {
        try {
            flush();
            this.b.close();
            byte[] bArr = this.c;
            if (bArr != null) {
                this.d.put(bArr);
                this.c = null;
            }
        } catch (Throwable th) {
            this.b.close();
            throw th;
        }
    }

    public void flush() {
        int i2 = this.f1598e;
        if (i2 > 0) {
            this.b.write(this.c, 0, i2);
            this.f1598e = 0;
        }
        this.b.flush();
    }

    public void write(int i2) {
        byte[] bArr = this.c;
        int i3 = this.f1598e;
        int i4 = i3 + 1;
        this.f1598e = i4;
        bArr[i3] = (byte) i2;
        if (i4 == bArr.length && i4 > 0) {
            this.b.write(bArr, 0, i4);
            this.f1598e = 0;
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i2, int i3) {
        int i4 = 0;
        do {
            int i5 = i3 - i4;
            int i6 = i2 + i4;
            if (this.f1598e != 0 || i5 < this.c.length) {
                int min = Math.min(i5, this.c.length - this.f1598e);
                System.arraycopy(bArr, i6, this.c, this.f1598e, min);
                int i7 = this.f1598e + min;
                this.f1598e = i7;
                i4 += min;
                byte[] bArr2 = this.c;
                if (i7 == bArr2.length && i7 > 0) {
                    this.b.write(bArr2, 0, i7);
                    this.f1598e = 0;
                    continue;
                }
            } else {
                this.b.write(bArr, i6, i5);
                return;
            }
        } while (i4 < i3);
    }
}
