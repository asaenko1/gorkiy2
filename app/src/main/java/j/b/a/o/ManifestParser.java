package j.b.a.o;

import java.lang.reflect.InvocationTargetException;

@Deprecated
public final class ManifestParser {
    public static GlideModule a(String str) {
        try {
            Class<?> cls = Class.forName(str);
            try {
                Object newInstance = cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                if (newInstance instanceof GlideModule) {
                    return (GlideModule) newInstance;
                }
                throw new RuntimeException("Expected instanceof GlideModule, but found: " + newInstance);
            } catch (InstantiationException e2) {
                a(cls, e2);
                throw null;
            } catch (IllegalAccessException e3) {
                a(cls, e3);
                throw null;
            } catch (NoSuchMethodException e4) {
                a(cls, e4);
                throw null;
            } catch (InvocationTargetException e5) {
                a(cls, e5);
                throw null;
            }
        } catch (ClassNotFoundException e6) {
            throw new IllegalArgumentException("Unable to find GlideModule implementation", e6);
        }
    }

    public static void a(Class<?> cls, Exception exc) {
        throw new RuntimeException("Unable to instantiate GlideModule implementation for " + cls, exc);
    }
}
