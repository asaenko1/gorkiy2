package j.b.a;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import i.h.e.ContextCompat;
import j.b.a.m.m.DiskCacheStrategy;
import j.b.a.m.m.k;
import j.b.a.m.o.g.GifDrawable;
import j.b.a.n.ConnectivityMonitor;
import j.b.a.n.ConnectivityMonitorFactory;
import j.b.a.n.DefaultConnectivityMonitor;
import j.b.a.n.DefaultConnectivityMonitorFactory;
import j.b.a.n.Lifecycle;
import j.b.a.n.NullConnectivityMonitor;
import j.b.a.n.RequestManagerTreeNode;
import j.b.a.n.RequestTracker;
import j.b.a.n.TargetTracker;
import j.b.a.n.i;
import j.b.a.q.Request;
import j.b.a.q.RequestListener;
import j.b.a.q.RequestOptions;
import j.b.a.q.h.Target;
import j.b.a.s.Util;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class RequestManager implements ComponentCallbacks2, i {

    /* renamed from: m  reason: collision with root package name */
    public static final RequestOptions f1551m;
    public final Glide b;
    public final Context c;
    public final Lifecycle d;

    /* renamed from: e  reason: collision with root package name */
    public final RequestTracker f1552e;

    /* renamed from: f  reason: collision with root package name */
    public final RequestManagerTreeNode f1553f;
    public final TargetTracker g = new TargetTracker();
    public final Runnable h = new a();

    /* renamed from: i  reason: collision with root package name */
    public final Handler f1554i = new Handler(Looper.getMainLooper());

    /* renamed from: j  reason: collision with root package name */
    public final ConnectivityMonitor f1555j;

    /* renamed from: k  reason: collision with root package name */
    public final CopyOnWriteArrayList<RequestListener<Object>> f1556k;

    /* renamed from: l  reason: collision with root package name */
    public RequestOptions f1557l;

    public class a implements Runnable {
        public a() {
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [j.b.a.RequestManager, j.b.a.n.LifecycleListener] */
        public void run() {
            ? r0 = RequestManager.this;
            r0.d.a(r0);
        }
    }

    public class b implements ConnectivityMonitor.a {
        public final RequestTracker a;

        public b(RequestTracker requestTracker) {
            this.a = requestTracker;
        }

        public void a(boolean z) {
            if (z) {
                synchronized (RequestManager.this) {
                    RequestTracker requestTracker = this.a;
                    Iterator it = ((ArrayList) Util.a(requestTracker.a)).iterator();
                    while (it.hasNext()) {
                        Request request = (Request) it.next();
                        if (!request.c() && !request.d()) {
                            request.clear();
                            if (!requestTracker.c) {
                                request.b();
                            } else {
                                requestTracker.b.add(request);
                            }
                        }
                    }
                }
            }
        }
    }

    static {
        RequestOptions requestOptions = (RequestOptions) new RequestOptions().a(Bitmap.class);
        requestOptions.u = true;
        f1551m = requestOptions;
        ((RequestOptions) new RequestOptions().a(GifDrawable.class)).u = true;
        RequestOptions requestOptions2 = (RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().a((k) DiskCacheStrategy.b)).a((f) Priority.LOW)).a(true);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [j.b.a.RequestManager, j.b.a.n.LifecycleListener] */
    public RequestManager(Glide glide, Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode, Context context) {
        ConnectivityMonitor connectivityMonitor;
        RequestTracker requestTracker = new RequestTracker();
        ConnectivityMonitorFactory connectivityMonitorFactory = glide.h;
        this.b = glide;
        this.d = lifecycle;
        this.f1553f = requestManagerTreeNode;
        this.f1552e = requestTracker;
        this.c = context;
        Context applicationContext = context.getApplicationContext();
        b bVar = new b(requestTracker);
        if (((DefaultConnectivityMonitorFactory) connectivityMonitorFactory) != null) {
            boolean z = ContextCompat.a(applicationContext, "android.permission.ACCESS_NETWORK_STATE") == 0;
            if (Log.isLoggable("ConnectivityMonitor", 3)) {
                Log.d("ConnectivityMonitor", z ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor" : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
            }
            if (z) {
                connectivityMonitor = new DefaultConnectivityMonitor(applicationContext, bVar);
            } else {
                connectivityMonitor = new NullConnectivityMonitor();
            }
            this.f1555j = connectivityMonitor;
            if (Util.b()) {
                this.f1554i.post(this.h);
            } else {
                lifecycle.a(this);
            }
            lifecycle.a(this.f1555j);
            this.f1556k = new CopyOnWriteArrayList<>(glide.d.f1547e);
            a(glide.d.a());
            glide.a((RequestManager) this);
            return;
        }
        throw null;
    }

    public synchronized void a(RequestOptions requestOptions) {
        RequestOptions requestOptions2 = (RequestOptions) requestOptions.clone();
        if (requestOptions2.u) {
            if (!requestOptions2.w) {
                throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
            }
        }
        requestOptions2.w = true;
        requestOptions2.u = true;
        this.f1557l = requestOptions2;
    }

    public synchronized boolean b(Target<?> target) {
        Request b2 = target.b();
        if (b2 == null) {
            return true;
        }
        if (!this.f1552e.a(b2)) {
            return false;
        }
        this.g.b.remove(target);
        target.a((Request) null);
        return true;
    }

    public synchronized void c() {
        g();
        this.g.c();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [j.b.a.RequestManager, j.b.a.n.LifecycleListener] */
    public synchronized void d() {
        this.g.d();
        for (Target a2 : Util.a(this.g.b)) {
            a(a2);
        }
        this.g.b.clear();
        RequestTracker requestTracker = this.f1552e;
        Iterator it = ((ArrayList) Util.a(requestTracker.a)).iterator();
        while (it.hasNext()) {
            requestTracker.a((Request) it.next());
        }
        requestTracker.b.clear();
        this.d.b(this);
        this.d.b(this.f1555j);
        this.f1554i.removeCallbacks(this.h);
        this.b.b((RequestManager) this);
    }

    public synchronized RequestOptions e() {
        return this.f1557l;
    }

    public synchronized void f() {
        RequestTracker requestTracker = this.f1552e;
        requestTracker.c = true;
        Iterator it = ((ArrayList) Util.a(requestTracker.a)).iterator();
        while (it.hasNext()) {
            Request request = (Request) it.next();
            if (request.isRunning()) {
                request.a();
                requestTracker.b.add(request);
            }
        }
    }

    public synchronized void g() {
        RequestTracker requestTracker = this.f1552e;
        requestTracker.c = false;
        Iterator it = ((ArrayList) Util.a(requestTracker.a)).iterator();
        while (it.hasNext()) {
            Request request = (Request) it.next();
            if (!request.c() && !request.isRunning()) {
                request.b();
            }
        }
        requestTracker.b.clear();
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public void onLowMemory() {
    }

    public void onTrimMemory(int i2) {
    }

    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.f1552e + ", treeNode=" + this.f1553f + "}";
    }

    public synchronized void a() {
        f();
        this.g.a();
    }

    public void a(Target<?> target) {
        if (target != null) {
            boolean b2 = b(target);
            Request b3 = target.b();
            if (!b2 && !this.b.a(target) && b3 != null) {
                target.a((Request) null);
                b3.clear();
            }
        }
    }

    public synchronized void a(Target<?> target, j.b.a.q.b bVar) {
        this.g.b.add(target);
        RequestTracker requestTracker = this.f1552e;
        requestTracker.a.add(bVar);
        if (!requestTracker.c) {
            bVar.b();
        } else {
            bVar.clear();
            if (Log.isLoggable("RequestTracker", 2)) {
                Log.v("RequestTracker", "Paused, delaying request");
            }
            requestTracker.b.add(bVar);
        }
    }
}
