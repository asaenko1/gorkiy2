package j.b.a.s;

import i.e.ArrayMap;
import i.e.SimpleArrayMap;

public final class CachedHashCodeArrayMap<K, V> extends ArrayMap<K, V> {

    /* renamed from: j  reason: collision with root package name */
    public int f1773j;

    public V a(int i2, V v) {
        this.f1773j = 0;
        int i3 = (i2 << 1) + 1;
        V[] vArr = this.c;
        V v2 = vArr[i3];
        vArr[i3] = v;
        return v2;
    }

    public void clear() {
        this.f1773j = 0;
        super.clear();
    }

    public V d(int i2) {
        this.f1773j = 0;
        return super.d(i2);
    }

    public int hashCode() {
        if (this.f1773j == 0) {
            this.f1773j = super.hashCode();
        }
        return this.f1773j;
    }

    public V put(K k2, V v) {
        this.f1773j = 0;
        return super.put(k2, v);
    }

    public void a(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.f1773j = 0;
        super.a((SimpleArrayMap) simpleArrayMap);
    }
}
