package j.b.a.q.h;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class BitmapImageViewTarget extends ImageViewTarget<Bitmap> {
    public BitmapImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    public void a(Object obj) {
        ((ImageView) this.b).setImageBitmap((Bitmap) obj);
    }
}
