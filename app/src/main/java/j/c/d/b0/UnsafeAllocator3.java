package j.c.d.b0;

/* compiled from: UnsafeAllocator */
public final class UnsafeAllocator3 extends UnsafeAllocator {
    public <T> T a(Class<T> cls) {
        throw new UnsupportedOperationException("Cannot allocate " + cls);
    }
}
