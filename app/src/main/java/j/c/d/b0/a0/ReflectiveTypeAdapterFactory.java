package j.c.d.b0.a0;

import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.c.d.ExclusionStrategy;
import j.c.d.FieldAttributes;
import j.c.d.FieldNamingStrategy;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.a0.Since;
import j.c.d.a0.Until;
import j.c.d.b0.ConstructorConstructor;
import j.c.d.b0.Excluder;
import j.c.d.b0.ObjectConstructor;
import j.c.d.b0.a0.j;
import j.c.d.b0.b0.ReflectionAccessor;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class ReflectiveTypeAdapterFactory implements TypeAdapterFactory {
    public final ConstructorConstructor b;
    public final FieldNamingStrategy c;
    public final Excluder d;

    /* renamed from: e  reason: collision with root package name */
    public final JsonAdapterAnnotationTypeAdapterFactory f2562e;

    /* renamed from: f  reason: collision with root package name */
    public final ReflectionAccessor f2563f = ReflectionAccessor.a;

    public static abstract class b {
        public final String a;
        public final boolean b;
        public final boolean c;

        public b(String str, boolean z, boolean z2) {
            this.a = str;
            this.b = z;
            this.c = z2;
        }
    }

    public ReflectiveTypeAdapterFactory(ConstructorConstructor constructorConstructor, FieldNamingStrategy fieldNamingStrategy, Excluder excluder, JsonAdapterAnnotationTypeAdapterFactory jsonAdapterAnnotationTypeAdapterFactory) {
        this.b = constructorConstructor;
        this.c = fieldNamingStrategy;
        this.d = excluder;
        this.f2562e = jsonAdapterAnnotationTypeAdapterFactory;
    }

    public boolean a(Field field, boolean z) {
        boolean z2;
        Excluder excluder = this.d;
        Class<?> type = field.getType();
        if (!(excluder.a(type) || excluder.a(type, z))) {
            if ((excluder.c & field.getModifiers()) == 0 && ((excluder.b == -1.0d || excluder.a((Since) field.getAnnotation(Since.class), (Until) field.getAnnotation(Until.class))) && !field.isSynthetic() && ((excluder.d || !excluder.c(field.getType())) && !excluder.b(field.getType())))) {
                List<j.c.d.b> list = z ? excluder.f2547e : excluder.f2548f;
                if (!list.isEmpty()) {
                    FieldAttributes fieldAttributes = new FieldAttributes(field);
                    Iterator it = list.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (((ExclusionStrategy) it.next()).a(fieldAttributes)) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                z2 = false;
            } else {
                z2 = true;
            }
            if (!z2) {
                return true;
            }
        }
        return false;
    }

    public static final class a<T> extends TypeAdapter<T> {
        public final ObjectConstructor<T> a;
        public final Map<String, j.b> b;

        public a(ObjectConstructor<T> objectConstructor, Map<String, j.b> map) {
            this.a = objectConstructor;
            this.b = map;
        }

        public T a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            T a2 = this.a.a();
            try {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    b bVar = this.b.get(jsonReader.nextName());
                    if (bVar != null) {
                        if (bVar.c) {
                            ReflectiveTypeAdapterFactory0 reflectiveTypeAdapterFactory0 = (ReflectiveTypeAdapterFactory0) bVar;
                            Object a3 = reflectiveTypeAdapterFactory0.f2565f.a(jsonReader);
                            if (a3 != null || !reflectiveTypeAdapterFactory0.f2566i) {
                                reflectiveTypeAdapterFactory0.d.set(a2, a3);
                            }
                        }
                    }
                    jsonReader.skipValue();
                }
                jsonReader.endObject();
                return a2;
            } catch (IllegalStateException e2) {
                throw new JsonSyntaxException(e2);
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }

        public void a(JsonWriter jsonWriter, T t2) {
            TypeAdapter typeAdapter;
            if (t2 == null) {
                jsonWriter.nullValue();
                return;
            }
            jsonWriter.beginObject();
            try {
                for (b next : this.b.values()) {
                    ReflectiveTypeAdapterFactory0 reflectiveTypeAdapterFactory0 = (ReflectiveTypeAdapterFactory0) next;
                    boolean z = false;
                    if (reflectiveTypeAdapterFactory0.b) {
                        if (reflectiveTypeAdapterFactory0.d.get(t2) != t2) {
                            z = true;
                        }
                    }
                    if (z) {
                        jsonWriter.name(next.a);
                        ReflectiveTypeAdapterFactory0 reflectiveTypeAdapterFactory02 = (ReflectiveTypeAdapterFactory0) next;
                        Object obj = reflectiveTypeAdapterFactory02.d.get(t2);
                        if (reflectiveTypeAdapterFactory02.f2564e) {
                            typeAdapter = reflectiveTypeAdapterFactory02.f2565f;
                        } else {
                            typeAdapter = new TypeAdapterRuntimeTypeWrapper(reflectiveTypeAdapterFactory02.g, reflectiveTypeAdapterFactory02.f2565f, reflectiveTypeAdapterFactory02.h.b);
                        }
                        super.a(jsonWriter, obj);
                    }
                }
                jsonWriter.endObject();
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.b0.a0.ReflectiveTypeAdapterFactory.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      j.c.d.b0.a0.ReflectiveTypeAdapterFactory.a(j.c.d.k, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T>
      j.c.d.TypeAdapterFactory.a(j.c.d.k, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T>
      j.c.d.b0.a0.ReflectiveTypeAdapterFactory.a(java.lang.reflect.Field, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0195 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x017b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> j.c.d.TypeAdapter<T> a(j.c.d.k r35, j.c.d.c0.TypeToken<T> r36) {
        /*
            r34 = this;
            r11 = r34
            r12 = r35
            r0 = r36
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            java.lang.Class<? super T> r1 = r0.a
            boolean r2 = r13.isAssignableFrom(r1)
            r14 = 0
            if (r2 != 0) goto L_0x0012
            return r14
        L_0x0012:
            j.c.d.b0.ConstructorConstructor r2 = r11.b
            j.c.d.b0.ObjectConstructor r15 = r2.a(r0)
            j.c.d.b0.a0.ReflectiveTypeAdapterFactory$a r10 = new j.c.d.b0.a0.ReflectiveTypeAdapterFactory$a
            java.util.LinkedHashMap r9 = new java.util.LinkedHashMap
            r9.<init>()
            boolean r2 = r1.isInterface()
            if (r2 == 0) goto L_0x002c
        L_0x0025:
            r14 = r9
            r33 = r10
            r28 = r15
            goto L_0x01d8
        L_0x002c:
            java.lang.reflect.Type r8 = r0.b
            r7 = r0
            r6 = r1
        L_0x0030:
            if (r6 == r13) goto L_0x0025
            java.lang.reflect.Field[] r5 = r6.getDeclaredFields()
            int r4 = r5.length
            r3 = 0
            r2 = 0
        L_0x0039:
            if (r2 >= r4) goto L_0x01b1
            r1 = r5[r2]
            r0 = 1
            boolean r16 = r11.a(r1, r0)
            boolean r17 = r11.a(r1, r3)
            if (r16 != 0) goto L_0x0060
            if (r17 != 0) goto L_0x0060
            r19 = r2
            r20 = r4
            r31 = r5
            r36 = r6
            r32 = r7
            r14 = r9
            r33 = r10
            r22 = r13
            r28 = r15
            r29 = 0
            r15 = r8
            goto L_0x017b
        L_0x0060:
            j.c.d.b0.b0.ReflectionAccessor r3 = r11.f2563f
            r3.a(r1)
            java.lang.reflect.Type r3 = r7.b
            java.lang.reflect.Type r14 = r1.getGenericType()
            java.lang.reflect.Type r14 = j.c.d.b0.a.a(r3, r6, r14)
            java.lang.Class<j.c.d.a0.SerializedName> r3 = j.c.d.a0.SerializedName.class
            java.lang.annotation.Annotation r3 = r1.getAnnotation(r3)
            j.c.d.a0.SerializedName r3 = (j.c.d.a0.SerializedName) r3
            if (r3 != 0) goto L_0x0086
            j.c.d.FieldNamingStrategy r3 = r11.c
            java.lang.String r3 = r3.a(r1)
            java.util.List r3 = java.util.Collections.singletonList(r3)
            r19 = r2
            goto L_0x0097
        L_0x0086:
            java.lang.String r0 = r3.value()
            java.lang.String[] r3 = r3.alternate()
            r19 = r2
            int r2 = r3.length
            if (r2 != 0) goto L_0x009d
            java.util.List r3 = java.util.Collections.singletonList(r0)
        L_0x0097:
            r20 = r4
            r18 = 1
            r4 = r3
            goto L_0x00bd
        L_0x009d:
            java.util.ArrayList r2 = new java.util.ArrayList
            r20 = r4
            int r4 = r3.length
            r18 = 1
            int r4 = r4 + 1
            r2.<init>(r4)
            r2.add(r0)
            int r0 = r3.length
            r4 = 0
        L_0x00ae:
            if (r4 >= r0) goto L_0x00bc
            r21 = r0
            r0 = r3[r4]
            r2.add(r0)
            int r4 = r4 + 1
            r0 = r21
            goto L_0x00ae
        L_0x00bc:
            r4 = r2
        L_0x00bd:
            int r3 = r4.size()
            r0 = 0
            r2 = 0
        L_0x00c3:
            if (r2 >= r3) goto L_0x0168
            java.lang.Object r21 = r4.get(r2)
            r22 = r13
            r13 = r21
            java.lang.String r13 = (java.lang.String) r13
            r21 = r9
            if (r2 == 0) goto L_0x00d5
            r16 = 0
        L_0x00d5:
            j.c.d.c0.TypeToken r9 = new j.c.d.c0.TypeToken
            r9.<init>(r14)
            r23 = r0
            java.lang.Class<? super T> r0 = r9.a
            r24 = r2
            java.util.Map<java.lang.Class<?>, java.lang.Class<?>> r2 = j.c.d.b0.Primitives.a
            boolean r25 = r2.containsKey(r0)
            java.lang.Class<j.c.d.a0.JsonAdapter> r0 = j.c.d.a0.JsonAdapter.class
            java.lang.annotation.Annotation r0 = r1.getAnnotation(r0)
            j.c.d.a0.JsonAdapter r0 = (j.c.d.a0.JsonAdapter) r0
            if (r0 == 0) goto L_0x00fb
            j.c.d.b0.a0.JsonAdapterAnnotationTypeAdapterFactory r2 = r11.f2562e
            r26 = r1
            j.c.d.b0.ConstructorConstructor r1 = r11.b
            j.c.d.TypeAdapter r0 = r2.a(r1, r12, r9, r0)
            goto L_0x00fe
        L_0x00fb:
            r26 = r1
            r0 = 0
        L_0x00fe:
            if (r0 == 0) goto L_0x0103
            r27 = 1
            goto L_0x0105
        L_0x0103:
            r27 = 0
        L_0x0105:
            if (r0 != 0) goto L_0x010b
            j.c.d.TypeAdapter r0 = r12.a(r9)
        L_0x010b:
            r28 = r0
            j.c.d.b0.a0.ReflectiveTypeAdapterFactory0 r2 = new j.c.d.b0.a0.ReflectiveTypeAdapterFactory0
            r1 = r23
            r0 = r2
            r11 = r1
            r23 = r26
            r1 = r34
            r12 = r2
            r2 = r13
            r26 = r3
            r29 = 0
            r3 = r16
            r30 = r4
            r4 = r17
            r31 = r5
            r5 = r23
            r36 = r6
            r6 = r27
            r27 = r14
            r14 = r7
            r7 = r28
            r28 = r15
            r15 = r8
            r8 = r35
            r32 = r14
            r14 = r21
            r33 = r10
            r10 = r25
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
            java.lang.Object r0 = r14.put(r13, r12)
            j.c.d.b0.a0.ReflectiveTypeAdapterFactory$b r0 = (j.c.d.b0.a0.ReflectiveTypeAdapterFactory.b) r0
            if (r11 != 0) goto L_0x0149
            goto L_0x014a
        L_0x0149:
            r0 = r11
        L_0x014a:
            int r2 = r24 + 1
            r11 = r34
            r12 = r35
            r6 = r36
            r9 = r14
            r8 = r15
            r13 = r22
            r1 = r23
            r3 = r26
            r14 = r27
            r15 = r28
            r4 = r30
            r5 = r31
            r7 = r32
            r10 = r33
            goto L_0x00c3
        L_0x0168:
            r11 = r0
            r31 = r5
            r36 = r6
            r32 = r7
            r14 = r9
            r33 = r10
            r22 = r13
            r28 = r15
            r29 = 0
            r15 = r8
            if (r11 != 0) goto L_0x0195
        L_0x017b:
            int r2 = r19 + 1
            r11 = r34
            r12 = r35
            r6 = r36
            r9 = r14
            r8 = r15
            r4 = r20
            r13 = r22
            r15 = r28
            r5 = r31
            r7 = r32
            r10 = r33
            r3 = 0
            r14 = 0
            goto L_0x0039
        L_0x0195:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r15)
            java.lang.String r2 = " declares multiple JSON fields named "
            r1.append(r2)
            java.lang.String r2 = r11.a
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x01b1:
            r36 = r6
            r0 = r7
            r14 = r9
            r33 = r10
            r22 = r13
            r28 = r15
            r15 = r8
            java.lang.reflect.Type r0 = r0.b
            java.lang.reflect.Type r1 = r36.getGenericSuperclass()
            r2 = r36
            java.lang.reflect.Type r0 = j.c.d.b0.a.a(r0, r2, r1)
            j.c.d.c0.TypeToken r7 = new j.c.d.c0.TypeToken
            r7.<init>(r0)
            java.lang.Class<? super T> r6 = r7.a
            r11 = r34
            r12 = r35
            r15 = r28
            r14 = 0
            goto L_0x0030
        L_0x01d8:
            r0 = r28
            r1 = r33
            r1.<init>(r0, r14)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.a0.ReflectiveTypeAdapterFactory.a(j.c.d.Gson, j.c.d.c0.TypeToken):j.c.d.TypeAdapter");
    }
}
