package j.c.d.b0.a0;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.b0.ConstructorConstructor;
import j.c.d.b0.ObjectConstructor;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Collection;

public final class CollectionTypeAdapterFactory implements TypeAdapterFactory {
    public final ConstructorConstructor b;

    public CollectionTypeAdapterFactory(ConstructorConstructor constructorConstructor) {
        this.b = constructorConstructor;
    }

    public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
        Type type;
        Type type2 = typeToken.b;
        Class<? super T> cls = typeToken.a;
        if (!Collection.class.isAssignableFrom(cls)) {
            return null;
        }
        Type b2 = j.c.d.b0.a.b(type2, cls, Collection.class);
        if (b2 instanceof WildcardType) {
            b2 = ((WildcardType) b2).getUpperBounds()[0];
        }
        if (b2 instanceof ParameterizedType) {
            type = ((ParameterizedType) b2).getActualTypeArguments()[0];
        } else {
            type = Object.class;
        }
        return new a(kVar, type, kVar.a(new TypeToken(type)), this.b.a(typeToken));
    }

    public static final class a<E> extends TypeAdapter<Collection<E>> {
        public final TypeAdapter<E> a;
        public final ObjectConstructor<? extends Collection<E>> b;

        public a(k kVar, Type type, TypeAdapter<E> typeAdapter, ObjectConstructor<? extends Collection<E>> objectConstructor) {
            this.a = new TypeAdapterRuntimeTypeWrapper(kVar, super, type);
            this.b = objectConstructor;
        }

        public void a(JsonWriter jsonWriter, Object obj) {
            Collection<Object> collection = (Collection) obj;
            if (collection == null) {
                jsonWriter.nullValue();
                return;
            }
            jsonWriter.beginArray();
            for (Object a2 : collection) {
                this.a.a(jsonWriter, a2);
            }
            jsonWriter.endArray();
        }

        public Object a(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            Collection collection = (Collection) this.b.a();
            jsonReader.beginArray();
            while (jsonReader.hasNext()) {
                collection.add(this.a.a(jsonReader));
            }
            jsonReader.endArray();
            return collection;
        }
    }
}
