package j.c.d.b0.a0;

import com.google.gson.stream.JsonWriter;
import j.c.d.Gson;
import j.c.d.JsonDeserializationContext;
import j.c.d.JsonDeserializer;
import j.c.d.JsonSerializationContext;
import j.c.d.JsonSerializer;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import j.c.d.z;

public final class TreeTypeAdapter<T> extends TypeAdapter<T> {
    public final JsonSerializer<T> a;
    public final JsonDeserializer<T> b;
    public final Gson c;
    public final TypeToken<T> d;

    /* renamed from: e  reason: collision with root package name */
    public final TypeAdapterFactory f2567e;

    /* renamed from: f  reason: collision with root package name */
    public final TreeTypeAdapter<T>.defpackage.b f2568f = new b(this, null);
    public TypeAdapter<T> g;

    public final class b implements JsonSerializationContext, JsonDeserializationContext {
        public /* synthetic */ b(TreeTypeAdapter treeTypeAdapter, a aVar) {
        }
    }

    public static final class c implements TypeAdapterFactory {
        public final TypeToken<?> b;
        public final boolean c;
        public final Class<?> d;

        /* renamed from: e  reason: collision with root package name */
        public final JsonSerializer<?> f2569e;

        /* renamed from: f  reason: collision with root package name */
        public final JsonDeserializer<?> f2570f;

        public c(Object obj, TypeToken<?> typeToken, boolean z, Class<?> cls) {
            JsonDeserializer<?> jsonDeserializer = null;
            this.f2569e = obj instanceof JsonSerializer ? (JsonSerializer) obj : null;
            jsonDeserializer = obj instanceof JsonDeserializer ? (JsonDeserializer) obj : jsonDeserializer;
            this.f2570f = jsonDeserializer;
            j.c.a.a.c.n.c.a((this.f2569e == null && jsonDeserializer == null) ? false : true);
            this.b = typeToken;
            this.c = z;
            this.d = cls;
        }

        public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
            boolean z;
            TypeToken<?> typeToken2 = this.b;
            if (typeToken2 != null) {
                z = typeToken2.equals(typeToken) || (this.c && this.b.b == typeToken.a);
            } else {
                z = this.d.isAssignableFrom(typeToken.a);
            }
            if (z) {
                return new TreeTypeAdapter(this.f2569e, this.f2570f, kVar, typeToken, this);
            }
            return null;
        }
    }

    public TreeTypeAdapter(JsonSerializer<T> jsonSerializer, JsonDeserializer<T> jsonDeserializer, k kVar, TypeToken<T> typeToken, z zVar) {
        this.a = jsonSerializer;
        this.b = jsonDeserializer;
        this.c = kVar;
        this.d = typeToken;
        this.f2567e = zVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T>
     arg types: [j.c.d.TypeAdapterFactory, j.c.d.c0.TypeToken<T>]
     candidates:
      j.c.d.Gson.a(java.lang.String, java.lang.Class):T
      j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T> */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        throw new com.google.gson.JsonSyntaxException(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        throw new com.google.gson.JsonIOException(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
        throw new com.google.gson.JsonSyntaxException(r4);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0029 A[ExcHandler: NumberFormatException (r4v8 'e' java.lang.NumberFormatException A[CUSTOM_DECLARE]), Splitter:B:7:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030 A[ExcHandler: IOException (r4v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:7:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0037 A[ExcHandler: MalformedJsonException (r4v6 'e' com.google.gson.stream.MalformedJsonException A[CUSTOM_DECLARE]), Splitter:B:7:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public T a(com.google.gson.stream.JsonReader r4) {
        /*
            r3 = this;
            j.c.d.JsonDeserializer<T> r0 = r3.b
            if (r0 != 0) goto L_0x001a
            j.c.d.TypeAdapter<T> r0 = r3.g
            if (r0 == 0) goto L_0x0009
            goto L_0x0015
        L_0x0009:
            j.c.d.Gson r0 = r3.c
            j.c.d.TypeAdapterFactory r1 = r3.f2567e
            j.c.d.c0.TypeToken<T> r2 = r3.d
            j.c.d.TypeAdapter r0 = r0.a(r1, r2)
            r3.g = r0
        L_0x0015:
            java.lang.Object r4 = r0.a(r4)
            return r4
        L_0x001a:
            r4.peek()     // Catch:{ EOFException -> 0x003e, MalformedJsonException -> 0x0037, IOException -> 0x0030, NumberFormatException -> 0x0029 }
            r0 = 0
            j.c.d.TypeAdapter<j.c.d.q> r1 = j.c.d.b0.a0.TypeAdapters.X     // Catch:{ EOFException -> 0x0027, MalformedJsonException -> 0x0037, IOException -> 0x0030, NumberFormatException -> 0x0029 }
            java.lang.Object r4 = r1.a(r4)     // Catch:{ EOFException -> 0x0027, MalformedJsonException -> 0x0037, IOException -> 0x0030, NumberFormatException -> 0x0029 }
            j.c.d.JsonElement r4 = (j.c.d.JsonElement) r4     // Catch:{ EOFException -> 0x0027, MalformedJsonException -> 0x0037, IOException -> 0x0030, NumberFormatException -> 0x0029 }
            goto L_0x0044
        L_0x0027:
            r4 = move-exception
            goto L_0x0040
        L_0x0029:
            r4 = move-exception
            com.google.gson.JsonSyntaxException r0 = new com.google.gson.JsonSyntaxException
            r0.<init>(r4)
            throw r0
        L_0x0030:
            r4 = move-exception
            com.google.gson.JsonIOException r0 = new com.google.gson.JsonIOException
            r0.<init>(r4)
            throw r0
        L_0x0037:
            r4 = move-exception
            com.google.gson.JsonSyntaxException r0 = new com.google.gson.JsonSyntaxException
            r0.<init>(r4)
            throw r0
        L_0x003e:
            r4 = move-exception
            r0 = 1
        L_0x0040:
            if (r0 == 0) goto L_0x005a
            j.c.d.JsonNull r4 = j.c.d.JsonNull.a
        L_0x0044:
            r0 = 0
            if (r4 == 0) goto L_0x0059
            boolean r1 = r4 instanceof j.c.d.JsonNull
            if (r1 == 0) goto L_0x004c
            return r0
        L_0x004c:
            j.c.d.JsonDeserializer<T> r0 = r3.b
            j.c.d.c0.TypeToken<T> r1 = r3.d
            java.lang.reflect.Type r1 = r1.b
            j.c.d.b0.a0.TreeTypeAdapter<T>$b r2 = r3.f2568f
            java.lang.Object r4 = r0.a(r4, r1, r2)
            return r4
        L_0x0059:
            throw r0
        L_0x005a:
            com.google.gson.JsonSyntaxException r0 = new com.google.gson.JsonSyntaxException
            r0.<init>(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.a0.TreeTypeAdapter.a(com.google.gson.stream.JsonReader):java.lang.Object");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T>
     arg types: [j.c.d.TypeAdapterFactory, j.c.d.c0.TypeToken<T>]
     candidates:
      j.c.d.Gson.a(java.lang.String, java.lang.Class):T
      j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T> */
    public void a(JsonWriter jsonWriter, T t2) {
        JsonSerializer<T> jsonSerializer = this.a;
        if (jsonSerializer == null) {
            TypeAdapter<T> typeAdapter = this.g;
            if (typeAdapter == null) {
                typeAdapter = this.c.a((z) this.f2567e, (TypeToken) this.d);
                this.g = super;
            }
            super.a(jsonWriter, t2);
        } else if (t2 == null) {
            jsonWriter.nullValue();
        } else {
            TypeAdapters.X.a(jsonWriter, jsonSerializer.a(t2, this.d.b, this.f2568f));
        }
    }
}
