package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLongArray;

/* compiled from: Gson */
public final class Gson4 extends TypeAdapter<AtomicLongArray> {
    public final /* synthetic */ TypeAdapter a;

    public Gson4(TypeAdapter typeAdapter) {
        this.a = super;
    }

    public void a(JsonWriter jsonWriter, Object obj) {
        AtomicLongArray atomicLongArray = (AtomicLongArray) obj;
        jsonWriter.beginArray();
        int length = atomicLongArray.length();
        for (int i2 = 0; i2 < length; i2++) {
            this.a.a(jsonWriter, Long.valueOf(atomicLongArray.get(i2)));
        }
        jsonWriter.endArray();
    }

    public Object a(JsonReader jsonReader) {
        ArrayList arrayList = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            arrayList.add(Long.valueOf(((Number) this.a.a(jsonReader)).longValue()));
        }
        jsonReader.endArray();
        int size = arrayList.size();
        AtomicLongArray atomicLongArray = new AtomicLongArray(size);
        for (int i2 = 0; i2 < size; i2++) {
            atomicLongArray.set(i2, ((Long) arrayList.get(i2)).longValue());
        }
        return atomicLongArray;
    }
}
