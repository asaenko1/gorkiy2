package j.c.d;

public final class JsonNull extends JsonElement {
    public static final JsonNull a = new JsonNull();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof JsonNull);
    }

    public int hashCode() {
        return JsonNull.class.hashCode();
    }
}
