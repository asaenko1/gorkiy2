package j.c.d;

import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.c.d.b0.JavaVersion;
import j.c.d.b0.a0.s.ISO8601Utils;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class DefaultDateTypeAdapter extends TypeAdapter<Date> {
    public final Class<? extends Date> a;
    public final List<DateFormat> b = new ArrayList();

    public DefaultDateTypeAdapter(Class<? extends Date> cls, int i2, int i3) {
        a(cls);
        this.a = cls;
        this.b.add(DateFormat.getDateTimeInstance(i2, i3, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.b.add(DateFormat.getDateTimeInstance(i2, i3));
        }
        if (JavaVersion.a >= 9) {
            this.b.add(c.a(i2, i3));
        }
    }

    public String toString() {
        DateFormat dateFormat = this.b.get(0);
        if (dateFormat instanceof SimpleDateFormat) {
            StringBuilder a2 = outline.a("DefaultDateTypeAdapter(");
            a2.append(((SimpleDateFormat) dateFormat).toPattern());
            a2.append(')');
            return a2.toString();
        }
        StringBuilder a3 = outline.a("DefaultDateTypeAdapter(");
        a3.append(dateFormat.getClass().getSimpleName());
        a3.append(')');
        return a3.toString();
    }

    public static Class<? extends Date> a(Class<? extends Date> cls) {
        if (cls == Date.class || cls == java.sql.Date.class || cls == Timestamp.class) {
            return cls;
        }
        throw new IllegalArgumentException("Date type must be one of " + Date.class + ", " + Timestamp.class + ", or " + java.sql.Date.class + " but was " + cls);
    }

    public void a(JsonWriter jsonWriter, Date date) {
        if (date == null) {
            jsonWriter.nullValue();
            return;
        }
        synchronized (this.b) {
            jsonWriter.value(this.b.get(0).format(date));
        }
    }

    public Object a(JsonReader jsonReader) {
        Object date;
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        Date a2 = a(jsonReader.nextString());
        Class<? extends Date> cls = this.a;
        if (cls == Date.class) {
            return a2;
        }
        if (cls == Timestamp.class) {
            date = new Timestamp(a2.getTime());
        } else if (cls == java.sql.Date.class) {
            date = new java.sql.Date(a2.getTime());
        } else {
            throw new AssertionError();
        }
        return date;
    }

    public final Date a(String str) {
        synchronized (this.b) {
            for (DateFormat parse : this.b) {
                try {
                    Date parse2 = parse.parse(str);
                    return parse2;
                } catch (ParseException unused) {
                }
            }
            try {
                Date a2 = ISO8601Utils.a(str, new ParsePosition(0));
                return a2;
            } catch (ParseException e2) {
                throw new JsonSyntaxException(str, e2);
            }
        }
    }
}
