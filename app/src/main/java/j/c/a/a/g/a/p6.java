package j.c.a.a.g.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.SystemClock;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.n.b;

@TargetApi(14)
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class p6 implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ x5 b;

    public /* synthetic */ p6(x5 x5Var, z5 z5Var) {
        this.b = x5Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00cd A[Catch:{ Exception -> 0x01cd, all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x011d A[Catch:{ Exception -> 0x01cd, all -> 0x01cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0159 A[SYNTHETIC, Splitter:B:74:0x0159] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onActivityCreated(android.app.Activity r20, android.os.Bundle r21) {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            r3 = r21
            java.lang.String r0 = "referrer"
            j.c.a.a.g.a.x5 r4 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.p3 r4 = r4.f2052n     // Catch:{ Exception -> 0x01cd }
            java.lang.String r5 = "onActivityCreated"
            r4.a(r5)     // Catch:{ Exception -> 0x01cd }
            android.content.Intent r4 = r20.getIntent()     // Catch:{ Exception -> 0x01cd }
            if (r4 != 0) goto L_0x0025
            j.c.a.a.g.a.x5 r0 = r1.b
            j.c.a.a.g.a.y6 r0 = r0.s()
            r0.a(r2, r3)
            return
        L_0x0025:
            android.net.Uri r5 = r4.getData()     // Catch:{ Exception -> 0x01cd }
            if (r5 == 0) goto L_0x01c1
            boolean r6 = r5.isHierarchical()     // Catch:{ Exception -> 0x01cd }
            if (r6 != 0) goto L_0x0033
            goto L_0x01c1
        L_0x0033:
            j.c.a.a.g.a.x5 r6 = r1.b     // Catch:{ Exception -> 0x01cd }
            r6.k()     // Catch:{ Exception -> 0x01cd }
            boolean r4 = j.c.a.a.g.a.y8.a(r4)     // Catch:{ Exception -> 0x01cd }
            java.lang.String r6 = "auto"
            if (r4 == 0) goto L_0x0043
            java.lang.String r4 = "gs"
            goto L_0x0044
        L_0x0043:
            r4 = r6
        L_0x0044:
            java.lang.String r7 = r5.getQueryParameter(r0)     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.x5 r8 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.r4 r8 = r8.a     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.i9 r8 = r8.g     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.b3<java.lang.Boolean> r9 = j.c.a.a.g.a.k.E0     // Catch:{ Exception -> 0x01cd }
            boolean r8 = r8.a(r9)     // Catch:{ Exception -> 0x01cd }
            java.lang.String r9 = "Activity created with data 'referrer' without required params"
            java.lang.String r10 = "utm_medium"
            java.lang.String r11 = "_cis"
            java.lang.String r12 = "utm_source"
            java.lang.String r13 = "utm_campaign"
            java.lang.String r15 = "gclid"
            if (r8 != 0) goto L_0x0075
            j.c.a.a.g.a.x5 r8 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.r4 r8 = r8.a     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.i9 r8 = r8.g     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.b3<java.lang.Boolean> r14 = j.c.a.a.g.a.k.F0     // Catch:{ Exception -> 0x01cd }
            boolean r8 = r8.a(r14)     // Catch:{ Exception -> 0x01cd }
            if (r8 == 0) goto L_0x0071
            goto L_0x0075
        L_0x0071:
            r17 = r9
            r8 = 0
            goto L_0x00ca
        L_0x0075:
            boolean r8 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x01cd }
            if (r8 == 0) goto L_0x007c
            goto L_0x0071
        L_0x007c:
            boolean r8 = r7.contains(r15)     // Catch:{ Exception -> 0x01cd }
            if (r8 != 0) goto L_0x00a0
            boolean r8 = r7.contains(r13)     // Catch:{ Exception -> 0x01cd }
            if (r8 != 0) goto L_0x00a0
            boolean r8 = r7.contains(r12)     // Catch:{ Exception -> 0x01cd }
            if (r8 != 0) goto L_0x00a0
            boolean r8 = r7.contains(r10)     // Catch:{ Exception -> 0x01cd }
            if (r8 != 0) goto L_0x00a0
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.p3 r0 = r0.f2051m     // Catch:{ Exception -> 0x01cd }
            r0.a(r9)     // Catch:{ Exception -> 0x01cd }
            goto L_0x0071
        L_0x00a0:
            j.c.a.a.g.a.x5 r8 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.y8 r8 = r8.k()     // Catch:{ Exception -> 0x01cd }
            java.lang.String r14 = "https://google.com/search?"
            int r17 = r7.length()     // Catch:{ Exception -> 0x01cd }
            if (r17 == 0) goto L_0x00b5
            java.lang.String r14 = r14.concat(r7)     // Catch:{ Exception -> 0x01cd }
            r17 = r9
            goto L_0x00bd
        L_0x00b5:
            r17 = r9
            java.lang.String r9 = new java.lang.String     // Catch:{ Exception -> 0x01cd }
            r9.<init>(r14)     // Catch:{ Exception -> 0x01cd }
            r14 = r9
        L_0x00bd:
            android.net.Uri r9 = android.net.Uri.parse(r14)     // Catch:{ Exception -> 0x01cd }
            android.os.Bundle r8 = r8.a(r9)     // Catch:{ Exception -> 0x01cd }
            if (r8 == 0) goto L_0x00ca
            r8.putString(r11, r0)     // Catch:{ Exception -> 0x01cd }
        L_0x00ca:
            r9 = 1
            if (r3 != 0) goto L_0x011d
            j.c.a.a.g.a.x5 r14 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.y8 r14 = r14.k()     // Catch:{ Exception -> 0x01cd }
            android.os.Bundle r14 = r14.a(r5)     // Catch:{ Exception -> 0x01cd }
            if (r14 == 0) goto L_0x011a
            java.lang.String r5 = "intent"
            r14.putString(r11, r5)     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.x5 r5 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.r4 r5 = r5.a     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.i9 r5 = r5.g     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.b3<java.lang.Boolean> r11 = j.c.a.a.g.a.k.E0     // Catch:{ Exception -> 0x01cd }
            boolean r5 = r5.a(r11)     // Catch:{ Exception -> 0x01cd }
            if (r5 == 0) goto L_0x0110
            boolean r5 = r14.containsKey(r15)     // Catch:{ Exception -> 0x01cd }
            if (r5 != 0) goto L_0x0110
            if (r8 == 0) goto L_0x0110
            boolean r5 = r8.containsKey(r15)     // Catch:{ Exception -> 0x01cd }
            if (r5 == 0) goto L_0x0110
            java.lang.String r5 = "_cer"
            java.lang.String r11 = "gclid=%s"
            java.lang.Object[] r0 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x01cd }
            java.lang.String r16 = r8.getString(r15)     // Catch:{ Exception -> 0x01cd }
            r18 = 0
            r0[r18] = r16     // Catch:{ Exception -> 0x01cd }
            java.lang.String r0 = java.lang.String.format(r11, r0)     // Catch:{ Exception -> 0x01cd }
            r14.putString(r5, r0)     // Catch:{ Exception -> 0x01cd }
            goto L_0x0112
        L_0x0110:
            r18 = 0
        L_0x0112:
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            java.lang.String r5 = "_cmp"
            r0.a(r4, r5, r14)     // Catch:{ Exception -> 0x01cd }
            goto L_0x0120
        L_0x011a:
            r18 = 0
            goto L_0x0120
        L_0x011d:
            r18 = 0
            r14 = 0
        L_0x0120:
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.r4 r0 = r0.a     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.i9 r0 = r0.g     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.b3<java.lang.Boolean> r4 = j.c.a.a.g.a.k.F0     // Catch:{ Exception -> 0x01cd }
            boolean r0 = r0.a(r4)     // Catch:{ Exception -> 0x01cd }
            if (r0 == 0) goto L_0x0149
            if (r8 == 0) goto L_0x0149
            boolean r0 = r8.containsKey(r15)     // Catch:{ Exception -> 0x01cd }
            if (r0 == 0) goto L_0x0149
            if (r14 == 0) goto L_0x013e
            boolean r0 = r14.containsKey(r15)     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x0149
        L_0x013e:
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            java.lang.String r4 = "_lgclid"
            java.lang.String r5 = r8.getString(r15)     // Catch:{ Exception -> 0x01cd }
            r0.a(r6, r4, r5, r9)     // Catch:{ Exception -> 0x01cd }
        L_0x0149:
            boolean r0 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x01cd }
            if (r0 == 0) goto L_0x0159
            j.c.a.a.g.a.x5 r0 = r1.b
            j.c.a.a.g.a.y6 r0 = r0.s()
            r0.a(r2, r3)
            return
        L_0x0159:
            boolean r0 = r7.contains(r15)     // Catch:{ Exception -> 0x01cd }
            if (r0 == 0) goto L_0x0183
            boolean r0 = r7.contains(r13)     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x0181
            boolean r0 = r7.contains(r12)     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x0181
            boolean r0 = r7.contains(r10)     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x0181
            java.lang.String r0 = "utm_term"
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x0181
            java.lang.String r0 = "utm_content"
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x01cd }
            if (r0 == 0) goto L_0x0183
        L_0x0181:
            r0 = 1
            goto L_0x0184
        L_0x0183:
            r0 = 0
        L_0x0184:
            if (r0 != 0) goto L_0x019d
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.p3 r0 = r0.f2051m     // Catch:{ Exception -> 0x01cd }
            r4 = r17
            r0.a(r4)     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.x5 r0 = r1.b
            j.c.a.a.g.a.y6 r0 = r0.s()
            r0.a(r2, r3)
            return
        L_0x019d:
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ Exception -> 0x01cd }
            j.c.a.a.g.a.p3 r0 = r0.f2051m     // Catch:{ Exception -> 0x01cd }
            java.lang.String r4 = "Activity created with referrer"
            r0.a(r4, r7)     // Catch:{ Exception -> 0x01cd }
            boolean r0 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x01b7
            j.c.a.a.g.a.x5 r0 = r1.b     // Catch:{ Exception -> 0x01cd }
            java.lang.String r4 = "_ldl"
            r0.a(r6, r4, r7, r9)     // Catch:{ Exception -> 0x01cd }
        L_0x01b7:
            j.c.a.a.g.a.x5 r0 = r1.b
            j.c.a.a.g.a.y6 r0 = r0.s()
            r0.a(r2, r3)
            return
        L_0x01c1:
            j.c.a.a.g.a.x5 r0 = r1.b
            j.c.a.a.g.a.y6 r0 = r0.s()
            r0.a(r2, r3)
            return
        L_0x01cb:
            r0 = move-exception
            goto L_0x01e5
        L_0x01cd:
            r0 = move-exception
            j.c.a.a.g.a.x5 r4 = r1.b     // Catch:{ all -> 0x01cb }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x01cb }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ all -> 0x01cb }
            java.lang.String r5 = "Throwable caught in onActivityCreated"
            r4.a(r5, r0)     // Catch:{ all -> 0x01cb }
            j.c.a.a.g.a.x5 r0 = r1.b
            j.c.a.a.g.a.y6 r0 = r0.s()
            r0.a(r2, r3)
            return
        L_0x01e5:
            j.c.a.a.g.a.x5 r4 = r1.b
            j.c.a.a.g.a.y6 r4 = r4.s()
            r4.a(r2, r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.p6.onActivityCreated(android.app.Activity, android.os.Bundle):void");
    }

    public final void onActivityDestroyed(Activity activity) {
        this.b.s().f2137f.remove(activity);
    }

    public final void onActivityPaused(Activity activity) {
        y6 s2 = this.b.s();
        w6 a = s2.a(activity);
        s2.f2136e = s2.d;
        s2.d = null;
        l4 i2 = s2.i();
        a7 a7Var = new a7(s2, a);
        i2.o();
        ResourcesFlusher.b(a7Var);
        i2.a((p4<?>) new p4(i2, a7Var, "Task exception on worker thread"));
        f8 u = this.b.u();
        if (((b) u.a.f2092n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            l4 i3 = u.i();
            l8 l8Var = new l8(u, elapsedRealtime);
            i3.o();
            ResourcesFlusher.b(l8Var);
            i3.a((p4<?>) new p4(i3, l8Var, "Task exception on worker thread"));
            return;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
     arg types: [android.app.Activity, j.c.a.a.g.a.w6, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void */
    public final void onActivityResumed(Activity activity) {
        y6 s2 = this.b.s();
        s2.a(activity, s2.a(activity), false);
        a o2 = s2.o();
        if (((b) o2.a.f2092n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            l4 i2 = o2.i();
            d3 d3Var = new d3(o2, elapsedRealtime);
            i2.o();
            ResourcesFlusher.b(d3Var);
            i2.a((p4<?>) new p4(i2, d3Var, "Task exception on worker thread"));
            f8 u = this.b.u();
            if (((b) u.a.f2092n) != null) {
                long elapsedRealtime2 = SystemClock.elapsedRealtime();
                l4 i3 = u.i();
                i8 i8Var = new i8(u, elapsedRealtime2);
                i3.o();
                ResourcesFlusher.b(i8Var);
                i3.a((p4<?>) new p4(i3, i8Var, "Task exception on worker thread"));
                return;
            }
            throw null;
        }
        throw null;
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        w6 w6Var;
        y6 s2 = this.b.s();
        if (s2 == null) {
            throw null;
        } else if (bundle != null && (w6Var = s2.f2137f.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", w6Var.c);
            bundle2.putString(DefaultAppMeasurementEventListenerRegistrar.NAME, w6Var.a);
            bundle2.putString("referrer_name", w6Var.b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }
}
