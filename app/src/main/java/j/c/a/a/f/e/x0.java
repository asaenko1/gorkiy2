package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class x0 extends x3<x0, a> implements g5 {
    public static final x0 zzf;
    public static volatile m5<x0> zzg;
    public int zzc;
    public int zzd;
    public f4 zze = u4.f1909e;

    static {
        x0 x0Var = new x0();
        zzf = x0Var;
        x3.zzd.put(x0.class, x0Var);
    }

    public static /* synthetic */ void a(x0 x0Var) {
        if (x0Var != null) {
            x0Var.zze = u4.f1909e;
            return;
        }
        throw null;
    }

    public static a n() {
        return (a) zzf.g();
    }

    public final int i() {
        return this.zzd;
    }

    public final List<Long> j() {
        return this.zze;
    }

    public final int m() {
        return this.zze.size();
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<x0, a> implements g5 {
        public a() {
            super(x0.zzf);
        }

        public final a a(int i2) {
            i();
            x0 x0Var = (x0) super.c;
            x0Var.zzc |= 1;
            x0Var.zzd = i2;
            return this;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(x0.zzf);
        }

        public final a a(long j2) {
            i();
            x0 x0Var = (x0) super.c;
            if (!x0Var.zze.a()) {
                x0Var.zze = x3.a(x0Var.zze);
            }
            ((u4) x0Var.zze).a(j2);
            return this;
        }

        public final a a(Iterable<? extends Long> iterable) {
            i();
            x0 x0Var = (x0) super.c;
            if (!x0Var.zze.a()) {
                x0Var.zze = x3.a(x0Var.zze);
            }
            n2.a(iterable, x0Var.zze);
            return this;
        }

        public final a a() {
            i();
            x0.a((x0) super.c);
            return this;
        }
    }

    public final boolean a() {
        return (this.zzc & 1) != 0;
    }

    public final long a(int i2) {
        u4 u4Var = (u4) this.zze;
        u4Var.d(i2);
        return u4Var.c[i2];
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new x0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u0004\u0000\u0002\u0014", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                m5<x0> m5Var = zzg;
                if (m5Var == null) {
                    synchronized (x0.class) {
                        m5Var = zzg;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzf);
                            zzg = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
