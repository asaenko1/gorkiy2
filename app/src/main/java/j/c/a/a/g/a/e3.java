package j.c.a.a.g.a;

import android.os.Parcel;
import j.c.a.a.f.e.a;
import j.c.a.a.f.e.c2;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class e3 extends a implements f3 {
    public e3() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    public final boolean a(int i2, Parcel parcel, Parcel parcel2, int i3) {
        boolean z = false;
        switch (i2) {
            case 1:
                a((i) c2.a(parcel, i.CREATOR), (d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                a((x8) c2.a(parcel, x8.CREATOR), (d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                break;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                a((d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                a((i) c2.a(parcel, i.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                c((d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                break;
            case 7:
                d9 d9Var = (d9) c2.a(parcel, d9.CREATOR);
                if (parcel.readInt() != 0) {
                    z = true;
                }
                List<x8> a = a(d9Var, z);
                parcel2.writeNoException();
                parcel2.writeTypedList(a);
                break;
            case 9:
                byte[] a2 = a((i) c2.a(parcel, i.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a2);
                break;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                String b = b((d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(b);
                break;
            case 12:
                a((g9) c2.a(parcel, g9.CREATOR), (d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                a((g9) c2.a(parcel, g9.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                List<x8> a3 = a(parcel.readString(), parcel.readString(), c2.a(parcel), (d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a3);
                break;
            case 15:
                List<x8> a4 = a(parcel.readString(), parcel.readString(), parcel.readString(), c2.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                break;
            case 16:
                List<g9> a5 = a(parcel.readString(), parcel.readString(), (d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                break;
            case 17:
                List<g9> a6 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                break;
            case 18:
                d((d9) c2.a(parcel, d9.CREATOR));
                parcel2.writeNoException();
                break;
        }
        return true;
    }
}
