package j.c.a.a.g.a;

import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.p.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g9 extends a {
    public static final Parcelable.Creator<g9> CREATOR = new j9();
    public String b;
    public String c;
    public x8 d;

    /* renamed from: e  reason: collision with root package name */
    public long f1998e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1999f;
    public String g;
    public i h;

    /* renamed from: i  reason: collision with root package name */
    public long f2000i;

    /* renamed from: j  reason: collision with root package name */
    public i f2001j;

    /* renamed from: k  reason: collision with root package name */
    public long f2002k;

    /* renamed from: l  reason: collision with root package name */
    public i f2003l;

    public g9(g9 g9Var) {
        ResourcesFlusher.b(g9Var);
        this.b = g9Var.b;
        this.c = g9Var.c;
        this.d = g9Var.d;
        this.f1998e = g9Var.f1998e;
        this.f1999f = g9Var.f1999f;
        this.g = g9Var.g;
        this.h = g9Var.h;
        this.f2000i = g9Var.f2000i;
        this.f2001j = g9Var.f2001j;
        this.f2002k = g9Var.f2002k;
        this.f2003l = g9Var.f2003l;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, j.c.a.a.g.a.x8, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int, java.lang.String):android.animation.PropertyValuesHolder
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, j.c.a.a.g.a.i, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int, java.lang.String):android.animation.PropertyValuesHolder
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 2, this.b, false);
        ResourcesFlusher.a(parcel, 3, this.c, false);
        ResourcesFlusher.a(parcel, 4, (Parcelable) this.d, i2, false);
        ResourcesFlusher.a(parcel, 5, this.f1998e);
        ResourcesFlusher.a(parcel, 6, this.f1999f);
        ResourcesFlusher.a(parcel, 7, this.g, false);
        ResourcesFlusher.a(parcel, 8, (Parcelable) this.h, i2, false);
        ResourcesFlusher.a(parcel, 9, this.f2000i);
        ResourcesFlusher.a(parcel, 10, (Parcelable) this.f2001j, i2, false);
        ResourcesFlusher.a(parcel, 11, this.f2002k);
        ResourcesFlusher.a(parcel, 12, (Parcelable) this.f2003l, i2, false);
        ResourcesFlusher.k(parcel, a);
    }

    public g9(String str, String str2, x8 x8Var, long j2, boolean z, String str3, i iVar, long j3, i iVar2, long j4, i iVar3) {
        this.b = str;
        this.c = str2;
        this.d = x8Var;
        this.f1998e = j2;
        this.f1999f = z;
        this.g = str3;
        this.h = iVar;
        this.f2000i = j3;
        this.f2001j = iVar2;
        this.f2002k = j4;
        this.f2003l = iVar3;
    }
}
