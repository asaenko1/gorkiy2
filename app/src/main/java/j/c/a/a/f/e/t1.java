package j.c.a.a.f.e;

import android.util.Log;
import j.a.a.a.outline;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class t1 extends p1<Double> {
    public t1(v1 v1Var, String str, Double d) {
        super(v1Var, str, d, null);
    }

    public final /* synthetic */ Object a(Object obj) {
        if (obj instanceof Double) {
            return (Double) obj;
        }
        if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        }
        if (obj instanceof String) {
            try {
                return Double.valueOf(Double.parseDouble((String) obj));
            } catch (NumberFormatException unused) {
            }
        }
        String a = super.a();
        String valueOf = String.valueOf(obj);
        StringBuilder sb = new StringBuilder(valueOf.length() + outline.a(a, 27));
        sb.append("Invalid double value for ");
        sb.append(a);
        sb.append(": ");
        sb.append(valueOf);
        Log.e("PhenotypeFlag", sb.toString());
        return null;
    }
}
