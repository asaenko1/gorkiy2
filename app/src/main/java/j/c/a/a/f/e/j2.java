package j.c.a.a.f.e;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.0.1 */
public final class j2 extends WeakReference<Throwable> {
    public final int a;

    public j2(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == j2.class) {
            if (this == obj) {
                return true;
            }
            j2 j2Var = (j2) obj;
            return this.a == j2Var.a && get() == super.get();
        }
    }

    public final int hashCode() {
        return this.a;
    }
}
