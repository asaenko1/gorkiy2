package j.c.a.a.g.a;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class m6 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ x5 c;

    public m6(x5 x5Var, AtomicReference atomicReference) {
        this.c = x5Var;
        this.b = atomicReference;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                AtomicReference atomicReference = this.b;
                i9 i9Var = this.c.a.g;
                g3 q2 = this.c.q();
                q2.w();
                atomicReference.set(Double.valueOf(i9Var.c(q2.c, k.Q)));
                this.b.notify();
            } catch (Throwable th) {
                this.b.notify();
                throw th;
            }
        }
    }
}
