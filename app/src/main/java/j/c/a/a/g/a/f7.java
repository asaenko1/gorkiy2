package j.c.a.a.g.a;

import android.os.RemoteException;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f7 implements Runnable {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ fb c;
    public final /* synthetic */ z6 d;

    public f7(z6 z6Var, d9 d9Var, fb fbVar) {
        this.d = z6Var;
        this.b = d9Var;
        this.c = fbVar;
    }

    public final void run() {
        try {
            f3 f3Var = this.d.d;
            if (f3Var == null) {
                this.d.a().f2046f.a("Failed to get app instance id");
                return;
            }
            String b2 = f3Var.b(this.b);
            if (b2 != null) {
                this.d.p().g.set(b2);
                this.d.l().f2116l.a(b2);
            }
            this.d.D();
            this.d.k().a(this.c, b2);
        } catch (RemoteException e2) {
            this.d.a().f2046f.a("Failed to get app instance id", e2);
        } finally {
            this.d.k().a(this.c, (String) null);
        }
    }
}
