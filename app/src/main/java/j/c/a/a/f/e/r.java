package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class r extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Bundle f1903f;
    public final /* synthetic */ pb g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(pb pbVar, Bundle bundle) {
        super(true);
        this.g = pbVar;
        this.f1903f = bundle;
    }

    public final void a() {
        this.g.g.setConditionalUserProperty(this.f1903f, super.b);
    }
}
