package j.c.a.a.g.a;

import android.os.Parcelable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l implements Parcelable.Creator<i> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r11) {
        /*
            r10 = this;
            int r0 = i.b.k.ResourcesFlusher.b(r11)
            r1 = 0
            r2 = 0
            r5 = r1
            r6 = r5
            r7 = r6
            r8 = r2
        L_0x000b:
            int r1 = r11.dataPosition()
            if (r1 >= r0) goto L_0x0042
            int r1 = r11.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            r3 = 2
            if (r2 == r3) goto L_0x003d
            r3 = 3
            if (r2 == r3) goto L_0x0033
            r3 = 4
            if (r2 == r3) goto L_0x002e
            r3 = 5
            if (r2 == r3) goto L_0x0029
            i.b.k.ResourcesFlusher.i(r11, r1)
            goto L_0x000b
        L_0x0029:
            long r8 = i.b.k.ResourcesFlusher.g(r11, r1)
            goto L_0x000b
        L_0x002e:
            java.lang.String r7 = i.b.k.ResourcesFlusher.b(r11, r1)
            goto L_0x000b
        L_0x0033:
            android.os.Parcelable$Creator<j.c.a.a.g.a.h> r2 = j.c.a.a.g.a.h.CREATOR
            android.os.Parcelable r1 = i.b.k.ResourcesFlusher.a(r11, r1, r2)
            r6 = r1
            j.c.a.a.g.a.h r6 = (j.c.a.a.g.a.h) r6
            goto L_0x000b
        L_0x003d:
            java.lang.String r5 = i.b.k.ResourcesFlusher.b(r11, r1)
            goto L_0x000b
        L_0x0042:
            i.b.k.ResourcesFlusher.c(r11, r0)
            j.c.a.a.g.a.i r11 = new j.c.a.a.g.a.i
            r4 = r11
            r4.<init>(r5, r6, r7, r8)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.l.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new i[i2];
    }
}
