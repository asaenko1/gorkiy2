package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class k4 {
    public volatile f5 a;
    public volatile w2 b;

    static {
        j3.a();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.f.e.f5 a(j.c.a.a.f.e.f5 r2) {
        /*
            r1 = this;
            j.c.a.a.f.e.f5 r0 = r1.a
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            j.c.a.a.f.e.f5 r0 = r1.a     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.a = r2     // Catch:{ zzfn -> 0x0012 }
            j.c.a.a.f.e.w2 r0 = j.c.a.a.f.e.w2.c     // Catch:{ zzfn -> 0x0012 }
            r1.b = r0     // Catch:{ zzfn -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.a = r2     // Catch:{ all -> 0x001a }
            j.c.a.a.f.e.w2 r2 = j.c.a.a.f.e.w2.c     // Catch:{ all -> 0x001a }
            r1.b = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            j.c.a.a.f.e.f5 r2 = r1.a
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.k4.a(j.c.a.a.f.e.f5):j.c.a.a.f.e.f5");
    }

    public final w2 b() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                w2 w2Var = this.b;
                return w2Var;
            }
            if (this.a == null) {
                this.b = w2.c;
            } else {
                this.b = this.a.e();
            }
            w2 w2Var2 = this.b;
            return w2Var2;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k4)) {
            return false;
        }
        k4 k4Var = (k4) obj;
        f5 f5Var = this.a;
        f5 f5Var2 = k4Var.a;
        if (f5Var == null && f5Var2 == null) {
            return b().equals(k4Var.b());
        }
        if (f5Var != null && f5Var2 != null) {
            return f5Var.equals(f5Var2);
        }
        if (f5Var != null) {
            return f5Var.equals(k4Var.a(f5Var.k()));
        }
        return a(f5Var2.k()).equals(f5Var2);
    }

    public int hashCode() {
        return 1;
    }

    public final int a() {
        if (this.b != null) {
            return this.b.a();
        }
        if (this.a != null) {
            return this.a.c();
        }
        return 0;
    }
}
