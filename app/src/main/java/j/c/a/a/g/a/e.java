package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class e {
    public final String a;
    public final String b;
    public final long c;
    public final long d;

    /* renamed from: e  reason: collision with root package name */
    public final long f1965e;

    /* renamed from: f  reason: collision with root package name */
    public final long f1966f;
    public final long g;
    public final Long h;

    /* renamed from: i  reason: collision with root package name */
    public final Long f1967i;

    /* renamed from: j  reason: collision with root package name */
    public final Long f1968j;

    /* renamed from: k  reason: collision with root package name */
    public final Boolean f1969k;

    public e(String str, String str2, long j2, long j3, long j4, long j5, long j6, Long l2, Long l3, Long l4, Boolean bool) {
        long j7 = j2;
        long j8 = j3;
        long j9 = j4;
        long j10 = j6;
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(str2);
        boolean z = true;
        ResourcesFlusher.a(j7 >= 0);
        ResourcesFlusher.a(j8 >= 0);
        ResourcesFlusher.a(j9 >= 0);
        ResourcesFlusher.a(j10 < 0 ? false : z);
        this.a = str;
        this.b = str2;
        this.c = j7;
        this.d = j8;
        this.f1965e = j9;
        this.f1966f = j5;
        this.g = j10;
        this.h = l2;
        this.f1967i = l3;
        this.f1968j = l4;
        this.f1969k = bool;
    }

    public final e a(long j2) {
        return new e(this.a, this.b, this.c, this.d, this.f1965e, j2, this.g, this.h, this.f1967i, this.f1968j, this.f1969k);
    }

    public final e a(long j2, long j3) {
        return new e(this.a, this.b, this.c, this.d, this.f1965e, this.f1966f, j2, Long.valueOf(j3), this.f1967i, this.f1968j, this.f1969k);
    }

    public final e a(Long l2, Long l3, Boolean bool) {
        return new e(this.a, this.b, this.c, this.d, this.f1965e, this.f1966f, this.g, this.h, l2, l3, (bool == null || bool.booleanValue()) ? bool : null);
    }

    public e(String str, String str2, long j2, long j3, long j4) {
        this(str, str2, j2, j3, 0, j4, 0, null, null, null, null);
    }
}
