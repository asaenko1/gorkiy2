package j.c.a.a.f.e;

import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class f extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f1848f;
    public final /* synthetic */ pb g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(pb pbVar, String str) {
        super(true);
        this.g = pbVar;
        this.f1848f = str;
    }

    public final void a() {
        this.g.g.beginAdUnitExposure(this.f1848f, super.c);
    }
}
