package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class o6 implements Runnable {
    public final /* synthetic */ boolean b;
    public final /* synthetic */ x5 c;

    public o6(x5 x5Var, boolean z) {
        this.c = x5Var;
        this.b = z;
    }

    public final void run() {
        boolean c2 = this.c.a.c();
        boolean b2 = this.c.a.b();
        this.c.a.A = Boolean.valueOf(this.b);
        if (b2 == this.b) {
            this.c.a.a().f2052n.a("Default data collection state already set to", Boolean.valueOf(this.b));
        }
        if (this.c.a.c() == c2 || this.c.a.c() != this.c.a.b()) {
            this.c.a.a().f2049k.a("Default data collection is different than actual status", Boolean.valueOf(this.b), Boolean.valueOf(c2));
        }
        this.c.C();
    }
}
