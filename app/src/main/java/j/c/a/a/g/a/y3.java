package j.c.a.a.g.a;

import android.content.SharedPreferences;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y3 {
    public final String a;
    public final boolean b;
    public boolean c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ w3 f2130e;

    public y3(w3 w3Var, String str, boolean z) {
        this.f2130e = w3Var;
        ResourcesFlusher.b(str);
        this.a = str;
        this.b = z;
    }

    public final boolean a() {
        if (!this.c) {
            this.c = true;
            this.d = this.f2130e.v().getBoolean(this.a, this.b);
        }
        return this.d;
    }

    public final void a(boolean z) {
        SharedPreferences.Editor edit = this.f2130e.v().edit();
        edit.putBoolean(this.a, z);
        edit.apply();
        this.d = z;
    }
}
