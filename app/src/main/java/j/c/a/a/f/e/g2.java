package j.c.a.a.f.e;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g2<T> implements b2<T> {
    public volatile b2<T> b;
    public volatile boolean c;
    @NullableDecl
    public T d;

    public g2(b2<T> b2Var) {
        if (b2Var != null) {
            this.b = b2Var;
            return;
        }
        throw null;
    }

    public final T a() {
        if (!this.c) {
            synchronized (this) {
                if (!this.c) {
                    T a = this.b.a();
                    this.d = a;
                    this.c = true;
                    this.b = null;
                    return a;
                }
            }
        }
        return this.d;
    }

    public final String toString() {
        Object obj = this.b;
        if (obj == null) {
            String valueOf = String.valueOf(this.d);
            StringBuilder sb = new StringBuilder(valueOf.length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(">");
            obj = sb.toString();
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(valueOf2.length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }
}
