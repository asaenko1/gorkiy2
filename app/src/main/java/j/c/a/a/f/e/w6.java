package j.c.a.a.f.e;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class w6 extends Enum<w6> {
    public static final w6 zza = new w6("DOUBLE", 0, z6.DOUBLE, 1);
    public static final w6 zzb = new w6("FLOAT", 1, z6.FLOAT, 5);
    public static final w6 zzc = new w6("INT64", 2, z6.LONG, 0);
    public static final w6 zzd = new w6("UINT64", 3, z6.LONG, 0);
    public static final w6 zze = new w6("INT32", 4, z6.INT, 0);
    public static final w6 zzf = new w6("FIXED64", 5, z6.LONG, 1);
    public static final w6 zzg = new w6("FIXED32", 6, z6.INT, 5);
    public static final w6 zzh = new w6("BOOL", 7, z6.BOOLEAN, 0);
    public static final w6 zzi = new v6("STRING", z6.STRING);
    public static final w6 zzj = new y6("GROUP", z6.MESSAGE);
    public static final w6 zzk = new x6("MESSAGE", z6.MESSAGE);
    public static final w6 zzl = new a7("BYTES", z6.BYTE_STRING);
    public static final w6 zzm = new w6("UINT32", 12, z6.INT, 0);
    public static final w6 zzn = new w6("ENUM", 13, z6.ENUM, 0);
    public static final w6 zzo = new w6("SFIXED32", 14, z6.INT, 5);
    public static final w6 zzp = new w6("SFIXED64", 15, z6.LONG, 1);
    public static final w6 zzq = new w6("SINT32", 16, z6.INT, 0);
    public static final w6 zzr;
    public static final /* synthetic */ w6[] zzu;
    public final z6 zzs;
    public final int zzt;

    static {
        w6 w6Var = new w6("SINT64", 17, z6.LONG, 0);
        zzr = w6Var;
        zzu = new w6[]{zza, zzb, zzc, zzd, zze, zzf, zzg, zzh, zzi, zzj, zzk, zzl, zzm, zzn, zzo, zzp, zzq, w6Var};
    }

    public w6(String str, int i2, z6 z6Var, int i3) {
        this.zzs = z6Var;
        this.zzt = i3;
    }

    public static w6[] values() {
        return (w6[]) zzu.clone();
    }

    public /* synthetic */ w6(String str, int i2, z6 z6Var, int i3, t6 t6Var) {
        this.zzs = z6Var;
        this.zzt = i3;
    }
}
