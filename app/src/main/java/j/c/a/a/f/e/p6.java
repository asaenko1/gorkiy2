package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class p6 {
    public static final q6 a;

    static {
        q6 q6Var;
        if (!(m6.h && m6.g) || q2.a()) {
            q6Var = new s6();
        } else {
            q6Var = new u6();
        }
        a = q6Var;
    }

    public static /* synthetic */ int a(int i2) {
        if (i2 > -12) {
            return -1;
        }
        return i2;
    }

    public static /* synthetic */ int a(int i2, int i3) {
        if (i2 > -12 || i3 > -65) {
            return -1;
        }
        return i2 ^ (i3 << 8);
    }

    public static int a(int i2, int i3, int i4) {
        if (i2 > -12 || i3 > -65 || i4 > -65) {
            return -1;
        }
        return (i2 ^ (i3 << 8)) ^ (i4 << 16);
    }

    public static boolean a(byte[] bArr, int i2, int i3) {
        if (a.a(0, bArr, i2, i3) == 0) {
            return true;
        }
        return false;
    }

    public static /* synthetic */ int b(byte[] bArr, int i2, int i3) {
        byte b = bArr[i2 - 1];
        int i4 = i3 - i2;
        byte b2 = -1;
        if (i4 == 0) {
            if (b > -12) {
                b = -1;
            }
            return b;
        } else if (i4 == 1) {
            byte b3 = bArr[i2];
            if (b <= -12 && b3 <= -65) {
                b2 = b ^ (b3 << 8);
            }
            return b2;
        } else if (i4 == 2) {
            return a(b, bArr[i2], bArr[i2 + 1]);
        } else {
            throw new AssertionError();
        }
    }

    public static int a(CharSequence charSequence) {
        int length = charSequence.length();
        int i2 = 0;
        int i3 = 0;
        while (i3 < length && charSequence.charAt(i3) < 128) {
            i3++;
        }
        int i4 = length;
        while (true) {
            if (i3 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i3);
            if (charAt < 2048) {
                i4 += (127 - charAt) >>> 31;
                i3++;
            } else {
                int length2 = charSequence.length();
                while (i3 < length2) {
                    char charAt2 = charSequence.charAt(i3);
                    if (charAt2 < 2048) {
                        i2 += (127 - charAt2) >>> 31;
                    } else {
                        i2 += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i3) >= 65536) {
                                i3++;
                            } else {
                                throw new r6(i3, length2);
                            }
                        }
                    }
                    i3++;
                }
                i4 += i2;
            }
        }
        if (i4 >= length) {
            return i4;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(((long) i4) + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }
}
