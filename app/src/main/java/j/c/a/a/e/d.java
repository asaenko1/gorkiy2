package j.c.a.a.e;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

public final class d implements DynamiteModule.a {
    public final DynamiteModule.a.b a(Context context, String str, DynamiteModule.a.C0002a aVar) {
        DynamiteModule.a.b bVar = new DynamiteModule.a.b();
        bVar.a = aVar.a(context, str);
        int a = aVar.a(context, str, true);
        bVar.b = a;
        if (bVar.a == 0 && a == 0) {
            bVar.c = 0;
        } else if (bVar.b >= bVar.a) {
            bVar.c = 1;
        } else {
            bVar.c = -1;
        }
        return bVar;
    }
}
