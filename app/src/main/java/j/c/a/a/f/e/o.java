package j.c.a.a.f.e;

import android.util.Log;
import android.util.Pair;
import j.c.a.a.f.e.pb;
import j.c.a.a.g.a.v5;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class o extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ v5 f1884f;
    public final /* synthetic */ pb g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(pb pbVar, v5 v5Var) {
        super(true);
        this.g = pbVar;
        this.f1884f = v5Var;
    }

    public final void a() {
        for (int i2 = 0; i2 < this.g.d.size(); i2++) {
            if (this.f1884f.equals(this.g.d.get(i2).first)) {
                Log.w(this.g.a, "OnEventListener already registered.");
                return;
            }
        }
        pb.c cVar = new pb.c(this.f1884f);
        this.g.d.add(new Pair(this.f1884f, cVar));
        this.g.g.registerOnMeasurementEventListener(cVar);
    }
}
