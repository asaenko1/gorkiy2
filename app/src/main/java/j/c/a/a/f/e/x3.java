package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import com.google.android.gms.internal.measurement.zzfn;
import com.google.android.gms.internal.measurement.zzhv;
import j.c.a.a.f.e.x3;
import j.c.a.a.f.e.x3.a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class x3<MessageType extends x3<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends n2<MessageType, BuilderType> {
    public static Map<Object, x3<?, ?>> zzd = new ConcurrentHashMap();
    public i6 zzb = i6.f1866f;
    public int zzc = -1;

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static abstract class a<MessageType extends x3<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends p2<MessageType, BuilderType> {
        public final MessageType b;
        public MessageType c;
        public boolean d = false;

        public a(MessageType messagetype) {
            this.b = messagetype;
            this.c = (x3) messagetype.a(4, null, null);
        }

        public final BuilderType a(MessageType messagetype) {
            i();
            MessageType messagetype2 = this.c;
            o5.c.a((Object) messagetype2).b(messagetype2, messagetype);
            return this;
        }

        public /* synthetic */ Object clone() {
            a aVar = (a) this.b.a(5, null, null);
            aVar.a((x3) j());
            return aVar;
        }

        public final void i() {
            if (this.d) {
                MessageType messagetype = (x3) this.c.a(4, null, null);
                o5.c.a((Object) messagetype).b(messagetype, this.c);
                this.c = messagetype;
                this.d = false;
            }
        }

        public /* synthetic */ f5 j() {
            if (this.d) {
                return this.c;
            }
            MessageType messagetype = this.c;
            o5.c.a((Object) messagetype).b(messagetype);
            this.d = true;
            return this.c;
        }

        public final /* synthetic */ f5 k() {
            return this.b;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.f.e.x3.a(j.c.a.a.f.e.x3, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          j.c.a.a.f.e.n2.a(java.lang.Iterable, java.util.List):void
          j.c.a.a.f.e.x3.a(j.c.a.a.f.e.x3, boolean):boolean */
        public final boolean l() {
            return x3.a((x3) this.c, false);
        }

        public /* synthetic */ f5 m() {
            x3 x3Var = (x3) j();
            if (x3Var.l()) {
                return x3Var;
            }
            throw new zzhv();
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static abstract class b<MessageType extends b<MessageType, BuilderType>, BuilderType> extends x3<MessageType, BuilderType> implements g5 {
        public m3<Object> zzc = m3.d;

        public final m3<Object> a() {
            m3<Object> m3Var = this.zzc;
            if (m3Var.b) {
                this.zzc = (m3) m3Var.clone();
            }
            return this.zzc;
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static class c<T extends x3<T, ?>> extends o2<T> {
        public c(T t2) {
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static final class d {
        public static final /* synthetic */ int[] a = {1, 2, 3, 4, 5, 6, 7};

        public static int[] a() {
            return (int[]) a.clone();
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static class e<ContainingType extends f5, Type> extends h3<ContainingType, Type> {
    }

    public abstract Object a(int i2, Object obj, Object obj2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
     arg types: [j.c.a.a.f.e.x3, j.c.a.a.f.e.g3]
     candidates:
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void */
    public final void a(zzek zzek) {
        t5 a2 = o5.c.a(this);
        g3 g3Var = zzek.a;
        if (g3Var == null) {
            g3Var = new g3(zzek);
        }
        a2.a((Object) this, (c7) g3Var);
    }

    public final /* synthetic */ e5 b() {
        return (a) a(5, (Object) null, (Object) null);
    }

    public final int c() {
        if (this.zzc == -1) {
            this.zzc = o5.c.a(this).c(this);
        }
        return this.zzc;
    }

    public final /* synthetic */ e5 d() {
        a aVar = (a) a(5, (Object) null, (Object) null);
        aVar.a(this);
        return aVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((x3) a(6, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return o5.c.a(this).a(this, (x3) obj);
    }

    public final <MessageType extends x3<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> BuilderType g() {
        return (a) a(5, (Object) null, (Object) null);
    }

    public final BuilderType h() {
        BuilderType buildertype = (a) a(5, (Object) null, (Object) null);
        buildertype.i();
        MessageType messagetype = buildertype.c;
        o5.c.a((Object) messagetype).b(messagetype, this);
        return buildertype;
    }

    public int hashCode() {
        int i2 = super.zza;
        if (i2 != 0) {
            return i2;
        }
        int a2 = o5.c.a(this).a(this);
        super.zza = a2;
        return a2;
    }

    public final /* synthetic */ f5 k() {
        return (x3) a(6, (Object) null, (Object) null);
    }

    public final boolean l() {
        return a(this, Boolean.TRUE.booleanValue());
    }

    public String toString() {
        String obj = super.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(obj);
        j.c.a.a.c.n.c.a(this, sb, 0);
        return sb.toString();
    }

    public static <T extends x3<?, ?>> T a(Class<T> cls) {
        T t2 = (x3) zzd.get(cls);
        if (t2 == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t2 = (x3) zzd.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t2 == null) {
            t2 = (x3) ((x3) m6.a(cls)).a(6, (Object) null, (Object) null);
            if (t2 != null) {
                zzd.put(cls, t2);
            } else {
                throw new IllegalStateException();
            }
        }
        return t2;
    }

    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    public static final <T extends x3<T, ?>> boolean a(T t2, boolean z) {
        byte byteValue = ((Byte) t2.a(1, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean d2 = o5.c.a((Object) t2).d(t2);
        if (z) {
            t2.a(2, d2 ? t2 : null, null);
        }
        return d2;
    }

    public static f4 a(f4 f4Var) {
        int size = f4Var.size();
        return ((u4) f4Var).a(size == 0 ? 10 : size << 1);
    }

    public static <E> e4<E> a(e4<E> e4Var) {
        int size = e4Var.size();
        return e4Var.a(size == 0 ? 10 : size << 1);
    }

    public static <T extends x3<T, ?>> T a(x3 x3Var, byte[] bArr, j3 j3Var) {
        int length = bArr.length;
        T t2 = (x3) x3Var.a(4, (Object) null, (Object) null);
        try {
            t5 a2 = o5.c.a((Object) t2);
            a2.a(t2, bArr, 0, length, new s2(j3Var));
            a2.b(t2);
            if (super.zza != 0) {
                throw new RuntimeException();
            } else if (t2.l()) {
                return t2;
            } else {
                throw new zzfn(new zzhv().getMessage());
            }
        } catch (IOException e2) {
            if (e2.getCause() instanceof zzfn) {
                throw ((zzfn) e2.getCause());
            }
            throw new zzfn(e2.getMessage());
        } catch (IndexOutOfBoundsException unused) {
            throw zzfn.a();
        }
    }
}
