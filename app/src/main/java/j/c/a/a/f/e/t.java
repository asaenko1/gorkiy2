package j.c.a.a.f.e;

import android.app.Activity;
import android.os.Bundle;
import j.c.a.a.d.b;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class t extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Activity f1907f;
    public final /* synthetic */ Bundle g;
    public final /* synthetic */ pb.b h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(pb.b bVar, Activity activity, Bundle bundle) {
        super(true);
        this.h = bVar;
        this.f1907f = activity;
        this.g = bundle;
    }

    public final void a() {
        pb.this.g.onActivityCreated(new b(this.f1907f), this.g, super.c);
    }
}
