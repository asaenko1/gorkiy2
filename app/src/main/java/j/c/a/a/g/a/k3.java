package j.c.a.a.g.a;

import android.os.IBinder;
import android.os.IInterface;
import j.c.a.a.c.g;
import j.c.a.a.c.l.b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class k3 extends b<f3> {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public k3(android.content.Context r10, android.os.Looper r11, j.c.a.a.c.l.b.a r12, j.c.a.a.c.l.b.C0023b r13) {
        /*
            r9 = this;
            j.c.a.a.c.l.h r3 = j.c.a.a.c.l.h.a(r10)
            j.c.a.a.c.f r4 = j.c.a.a.c.f.b
            i.b.k.ResourcesFlusher.b(r12)
            i.b.k.ResourcesFlusher.b(r13)
            r5 = 93
            r8 = 0
            r0 = r9
            r1 = r10
            r2 = r11
            r6 = r12
            r7 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.k3.<init>(android.content.Context, android.os.Looper, j.c.a.a.c.l.b$a, j.c.a.a.c.l.b$b):void");
    }

    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof f3) {
            return (f3) queryLocalInterface;
        }
        return new h3(iBinder);
    }

    public final int i() {
        return g.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    public final String o() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    public final String p() {
        return "com.google.android.gms.measurement.START";
    }
}
