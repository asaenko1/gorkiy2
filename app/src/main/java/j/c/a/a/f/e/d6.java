package j.c.a.a.f.e;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d6 implements Iterator<Map.Entry<K, V>> {
    public int b = -1;
    public boolean c;
    public Iterator<Map.Entry<K, V>> d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ x5 f1844e;

    public /* synthetic */ d6(x5 x5Var, w5 w5Var) {
        this.f1844e = x5Var;
    }

    public final Iterator<Map.Entry<K, V>> a() {
        if (this.d == null) {
            this.d = this.f1844e.d.entrySet().iterator();
        }
        return this.d;
    }

    public final boolean hasNext() {
        if (this.b + 1 < this.f1844e.c.size() || (!this.f1844e.d.isEmpty() && a().hasNext())) {
            return true;
        }
        return false;
    }

    public final /* synthetic */ Object next() {
        this.c = true;
        int i2 = this.b + 1;
        this.b = i2;
        if (i2 < this.f1844e.c.size()) {
            return this.f1844e.c.get(this.b);
        }
        return (Map.Entry) a().next();
    }

    public final void remove() {
        if (this.c) {
            this.c = false;
            this.f1844e.d();
            if (this.b < this.f1844e.c.size()) {
                x5 x5Var = this.f1844e;
                int i2 = this.b;
                this.b = i2 - 1;
                x5Var.b(i2);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }
}
