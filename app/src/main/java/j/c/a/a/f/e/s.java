package j.c.a.a.f.e;

import android.app.Activity;
import j.c.a.a.d.b;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class s extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Activity f1904f;
    public final /* synthetic */ pb.b g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(pb.b bVar, Activity activity) {
        super(true);
        this.g = bVar;
        this.f1904f = activity;
    }

    public final void a() {
        pb.this.g.onActivityStarted(new b(this.f1904f), super.c);
    }
}
