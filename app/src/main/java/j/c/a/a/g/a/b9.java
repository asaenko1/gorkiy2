package j.c.a.a.g.a;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
public final class b9 implements Runnable {
    public final /* synthetic */ fb b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ AppMeasurementDynamiteService f1945e;

    public b9(AppMeasurementDynamiteService appMeasurementDynamiteService, fb fbVar, String str, String str2) {
        this.f1945e = appMeasurementDynamiteService;
        this.b = fbVar;
        this.c = str;
        this.d = str2;
    }

    public final void run() {
        z6 r2 = this.f1945e.a.r();
        fb fbVar = this.b;
        String str = this.c;
        String str2 = this.d;
        r2.d();
        r2.w();
        r2.a(new q7(r2, str, str2, r2.a(false), fbVar));
    }
}
