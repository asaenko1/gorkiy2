package j.c.a.a.g.a;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import j.c.a.a.c.f;
import j.c.a.a.c.g;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
public final class b8 implements Runnable {
    public final /* synthetic */ fb b;
    public final /* synthetic */ i c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ AppMeasurementDynamiteService f1944e;

    public b8(AppMeasurementDynamiteService appMeasurementDynamiteService, fb fbVar, i iVar, String str) {
        this.f1944e = appMeasurementDynamiteService;
        this.b = fbVar;
        this.c = iVar;
        this.d = str;
    }

    public final void run() {
        z6 r2 = this.f1944e.a.r();
        fb fbVar = this.b;
        i iVar = this.c;
        String str = this.d;
        r2.d();
        r2.w();
        y8 k2 = r2.k();
        if (k2 == null) {
            throw null;
        } else if (f.b.a(k2.a.a, g.GOOGLE_PLAY_SERVICES_VERSION_CODE) != 0) {
            r2.a().f2047i.a("Not bundling data. Service unavailable or out of date");
            r2.k().a(fbVar, new byte[0]);
        } else {
            r2.a(new k7(r2, iVar, str, fbVar));
        }
    }
}
