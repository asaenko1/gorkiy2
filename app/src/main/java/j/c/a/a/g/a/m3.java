package j.c.a.a.g.a;

import android.content.SharedPreferences;
import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class m3 implements Runnable {
    public final /* synthetic */ int b;
    public final /* synthetic */ String c;
    public final /* synthetic */ Object d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Object f2037e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Object f2038f;
    public final /* synthetic */ n3 g;

    public m3(n3 n3Var, int i2, String str, Object obj, Object obj2, Object obj3) {
        this.g = n3Var;
        this.b = i2;
        this.c = str;
        this.d = obj;
        this.f2037e = obj2;
        this.f2038f = obj3;
    }

    public final void run() {
        w3 l2 = this.g.a.l();
        if (l2.s()) {
            n3 n3Var = this.g;
            if (n3Var.c == 0) {
                if (n3Var.a.g.p()) {
                    n3 n3Var2 = this.g;
                    h9 h9Var = n3Var2.a.f2086f;
                    n3Var2.c = 'C';
                } else {
                    n3 n3Var3 = this.g;
                    h9 h9Var2 = n3Var3.a.f2086f;
                    n3Var3.c = 'c';
                }
            }
            n3 n3Var4 = this.g;
            if (n3Var4.d < 0) {
                n3Var4.a.g.o();
                n3Var4.d = 18079;
            }
            char charAt = "01VDIWEA?".charAt(this.b);
            n3 n3Var5 = this.g;
            char c2 = n3Var5.c;
            long j2 = n3Var5.d;
            boolean z = true;
            String a = n3.a(true, this.c, this.d, this.f2037e, this.f2038f);
            StringBuilder sb = new StringBuilder(outline.a(a, 24));
            sb.append("2");
            sb.append(charAt);
            sb.append(c2);
            sb.append(j2);
            sb.append(":");
            sb.append(a);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.c.substring(0, 1024);
            }
            a4 a4Var = l2.d;
            a4Var.f1933e.d();
            if (a4Var.f1933e.v().getLong(a4Var.a, 0) == 0) {
                a4Var.a();
            }
            if (sb2 == null) {
                sb2 = "";
            }
            long j3 = a4Var.f1933e.v().getLong(a4Var.b, 0);
            if (j3 <= 0) {
                SharedPreferences.Editor edit = a4Var.f1933e.v().edit();
                edit.putString(a4Var.c, sb2);
                edit.putLong(a4Var.b, 1);
                edit.apply();
                return;
            }
            long j4 = j3 + 1;
            if ((a4Var.f1933e.k().u().nextLong() & RecyclerView.FOREVER_NS) >= RecyclerView.FOREVER_NS / j4) {
                z = false;
            }
            SharedPreferences.Editor edit2 = a4Var.f1933e.v().edit();
            if (z) {
                edit2.putString(a4Var.c, sb2);
            }
            edit2.putLong(a4Var.b, j4);
            edit2.apply();
            return;
        }
        this.g.a(6, "Persisted config not initialized. Not logging error/warn");
    }
}
