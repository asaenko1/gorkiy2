package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.Parcel;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.0.1 */
public abstract class eb extends a implements fb {
    public eb() {
        super("com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    public final boolean a(int i2, Parcel parcel, Parcel parcel2, int i3) {
        if (i2 != 1) {
            return false;
        }
        a((Bundle) c2.a(parcel, Bundle.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
