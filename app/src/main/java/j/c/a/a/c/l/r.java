package j.c.a.a.c.l;

import android.content.Intent;
import androidx.fragment.app.Fragment;
import i.h.d.ActivityCompat;
import i.l.a.FragmentActivity;
import i.l.a.FragmentHostCallback;
import j.a.a.a.outline;

public final class r extends e {
    public final /* synthetic */ Intent b;
    public final /* synthetic */ Fragment c;
    public final /* synthetic */ int d;

    public r(Intent intent, Fragment fragment, int i2) {
        this.b = intent;
        this.c = fragment;
        this.d = i2;
    }

    public final void a() {
        Intent intent = this.b;
        if (intent != null) {
            Fragment fragment = this.c;
            int i2 = this.d;
            FragmentHostCallback fragmentHostCallback = fragment.f233t;
            if (fragmentHostCallback != null) {
                FragmentActivity fragmentActivity = FragmentActivity.this;
                fragmentActivity.f1290l = true;
                if (i2 == -1) {
                    try {
                        ActivityCompat.a(fragmentActivity, intent, -1, null);
                    } catch (Throwable th) {
                        fragmentActivity.f1290l = false;
                        throw th;
                    }
                } else {
                    FragmentActivity.b(i2);
                    ActivityCompat.a(fragmentActivity, intent, ((fragmentActivity.a(fragment) + 1) << 16) + (i2 & 65535), null);
                }
                fragmentActivity.f1290l = false;
                return;
            }
            throw new IllegalStateException(outline.a("Fragment ", fragment, " not attached to Activity"));
        }
    }
}
