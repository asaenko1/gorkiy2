package j.c.a.a.c;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;

public final class a0 implements Parcelable.Creator<z> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = ResourcesFlusher.b(parcel);
        boolean z = false;
        String str = null;
        IBinder iBinder = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 1) {
                str = ResourcesFlusher.b(parcel, readInt);
            } else if (i2 == 2) {
                iBinder = ResourcesFlusher.e(parcel, readInt);
            } else if (i2 == 3) {
                z = ResourcesFlusher.d(parcel, readInt);
            } else if (i2 != 4) {
                ResourcesFlusher.i(parcel, readInt);
            } else {
                z2 = ResourcesFlusher.d(parcel, readInt);
            }
        }
        ResourcesFlusher.c(parcel, b);
        return new z(str, iBinder, z, z2);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new z[i2];
    }
}
