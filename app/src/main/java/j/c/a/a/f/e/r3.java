package j.c.a.a.f.e;

import java.lang.reflect.Type;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public enum r3 {
    DOUBLE(0, u3.SCALAR, h4.DOUBLE),
    FLOAT(1, u3.SCALAR, h4.FLOAT),
    INT64(2, u3.SCALAR, h4.LONG),
    UINT64(3, u3.SCALAR, h4.LONG),
    INT32(4, u3.SCALAR, h4.INT),
    FIXED64(5, u3.SCALAR, h4.LONG),
    FIXED32(6, u3.SCALAR, h4.INT),
    BOOL(7, u3.SCALAR, h4.BOOLEAN),
    STRING(8, u3.SCALAR, h4.STRING),
    MESSAGE(9, u3.SCALAR, h4.MESSAGE),
    BYTES(10, u3.SCALAR, h4.BYTE_STRING),
    UINT32(11, u3.SCALAR, h4.INT),
    ENUM(12, u3.SCALAR, h4.ENUM),
    SFIXED32(13, u3.SCALAR, h4.INT),
    SFIXED64(14, u3.SCALAR, h4.LONG),
    SINT32(15, u3.SCALAR, h4.INT),
    SINT64(16, u3.SCALAR, h4.LONG),
    GROUP(17, u3.SCALAR, h4.MESSAGE),
    DOUBLE_LIST(18, u3.VECTOR, h4.DOUBLE),
    FLOAT_LIST(19, u3.VECTOR, h4.FLOAT),
    INT64_LIST(20, u3.VECTOR, h4.LONG),
    UINT64_LIST(21, u3.VECTOR, h4.LONG),
    INT32_LIST(22, u3.VECTOR, h4.INT),
    FIXED64_LIST(23, u3.VECTOR, h4.LONG),
    FIXED32_LIST(24, u3.VECTOR, h4.INT),
    BOOL_LIST(25, u3.VECTOR, h4.BOOLEAN),
    STRING_LIST(26, u3.VECTOR, h4.STRING),
    MESSAGE_LIST(27, u3.VECTOR, h4.MESSAGE),
    BYTES_LIST(28, u3.VECTOR, h4.BYTE_STRING),
    UINT32_LIST(29, u3.VECTOR, h4.INT),
    ENUM_LIST(30, u3.VECTOR, h4.ENUM),
    SFIXED32_LIST(31, u3.VECTOR, h4.INT),
    SFIXED64_LIST(32, u3.VECTOR, h4.LONG),
    SINT32_LIST(33, u3.VECTOR, h4.INT),
    SINT64_LIST(34, u3.VECTOR, h4.LONG),
    DOUBLE_LIST_PACKED(35, u3.PACKED_VECTOR, h4.DOUBLE),
    FLOAT_LIST_PACKED(36, u3.PACKED_VECTOR, h4.FLOAT),
    INT64_LIST_PACKED(37, u3.PACKED_VECTOR, h4.LONG),
    UINT64_LIST_PACKED(38, u3.PACKED_VECTOR, h4.LONG),
    INT32_LIST_PACKED(39, u3.PACKED_VECTOR, h4.INT),
    FIXED64_LIST_PACKED(40, u3.PACKED_VECTOR, h4.LONG),
    FIXED32_LIST_PACKED(41, u3.PACKED_VECTOR, h4.INT),
    BOOL_LIST_PACKED(42, u3.PACKED_VECTOR, h4.BOOLEAN),
    UINT32_LIST_PACKED(43, u3.PACKED_VECTOR, h4.INT),
    ENUM_LIST_PACKED(44, u3.PACKED_VECTOR, h4.ENUM),
    SFIXED32_LIST_PACKED(45, u3.PACKED_VECTOR, h4.INT),
    SFIXED64_LIST_PACKED(46, u3.PACKED_VECTOR, h4.LONG),
    SINT32_LIST_PACKED(47, u3.PACKED_VECTOR, h4.INT),
    SINT64_LIST_PACKED(48, u3.PACKED_VECTOR, h4.LONG),
    GROUP_LIST(49, u3.VECTOR, h4.MESSAGE),
    MAP(50, u3.MAP, h4.VOID);
    
    public static final r3[] zzbe;
    public static final Type[] zzbf = new Type[0];
    public final h4 zzaz;
    public final int zzba;
    public final u3 zzbb;
    public final Class<?> zzbc;
    public final boolean zzbd;

    /* access modifiers changed from: public */
    static {
        r3[] values = values();
        zzbe = new r3[values.length];
        for (r3 r3Var : values) {
            zzbe[r3Var.zzba] = r3Var;
        }
    }

    /* access modifiers changed from: public */
    r3(int i2, u3 u3Var, h4 h4Var) {
        int i3;
        this.zzba = i2;
        this.zzbb = u3Var;
        this.zzaz = h4Var;
        int i4 = q3.a[super.ordinal()];
        boolean z = true;
        if (i4 == 1) {
            this.zzbc = h4Var.zzl;
        } else if (i4 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = h4Var.zzl;
        }
        this.zzbd = (u3Var != u3.SCALAR || (i3 = q3.b[super.ordinal()]) == 1 || i3 == 2 || i3 == 3) ? false : z;
    }
}
