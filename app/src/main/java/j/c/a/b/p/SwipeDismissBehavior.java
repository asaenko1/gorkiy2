package j.c.a.b.p;

import android.view.View;
import com.google.android.material.behavior.SwipeDismissBehavior;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityViewCommand;
import j.c.a.b.h0.BaseTransientBottomBar11;

public class SwipeDismissBehavior implements AccessibilityViewCommand {
    public final /* synthetic */ com.google.android.material.behavior.SwipeDismissBehavior a;

    public SwipeDismissBehavior(com.google.android.material.behavior.SwipeDismissBehavior swipeDismissBehavior) {
        this.a = swipeDismissBehavior;
    }

    public boolean a(View view, AccessibilityViewCommand.a aVar) {
        boolean z = false;
        if (!this.a.a(view)) {
            return false;
        }
        boolean z2 = ViewCompat.k(view) == 1;
        if ((this.a.f404e == 0 && z2) || (this.a.f404e == 1 && !z2)) {
            z = true;
        }
        int width = view.getWidth();
        if (z) {
            width = -width;
        }
        ViewCompat.d(view, width);
        view.setAlpha(0.0f);
        SwipeDismissBehavior.b bVar = this.a.b;
        if (bVar != null) {
            ((BaseTransientBottomBar11) bVar).a(view);
        }
        return true;
    }
}
