package j.c.a.b.l0;

import android.view.View;
import android.widget.AdapterView;
import i.b.q.ListPopupWindow0;

public class MaterialAutoCompleteTextView implements AdapterView.OnItemClickListener {
    public final /* synthetic */ MaterialAutoCompleteTextView0 b;

    public MaterialAutoCompleteTextView(MaterialAutoCompleteTextView0 materialAutoCompleteTextView0) {
        this.b = materialAutoCompleteTextView0;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        Object obj;
        View view2 = null;
        if (i2 < 0) {
            ListPopupWindow0 listPopupWindow0 = this.b.f2292e;
            if (!listPopupWindow0.b()) {
                obj = null;
            } else {
                obj = listPopupWindow0.d.getSelectedItem();
            }
        } else {
            obj = this.b.getAdapter().getItem(i2);
        }
        this.b.setText(this.b.convertSelectionToString(obj), false);
        AdapterView.OnItemClickListener onItemClickListener = this.b.getOnItemClickListener();
        if (onItemClickListener != null) {
            if (view == null || i2 < 0) {
                ListPopupWindow0 listPopupWindow02 = this.b.f2292e;
                if (listPopupWindow02.b()) {
                    view2 = listPopupWindow02.d.getSelectedView();
                }
                view = view2;
                ListPopupWindow0 listPopupWindow03 = this.b.f2292e;
                if (!listPopupWindow03.b()) {
                    i2 = -1;
                } else {
                    i2 = listPopupWindow03.d.getSelectedItemPosition();
                }
                ListPopupWindow0 listPopupWindow04 = this.b.f2292e;
                if (!listPopupWindow04.b()) {
                    j2 = Long.MIN_VALUE;
                } else {
                    j2 = listPopupWindow04.d.getSelectedItemId();
                }
            }
            onItemClickListener.onItemClick(this.b.f2292e.d, view, i2, j2);
        }
        this.b.f2292e.dismiss();
    }
}
