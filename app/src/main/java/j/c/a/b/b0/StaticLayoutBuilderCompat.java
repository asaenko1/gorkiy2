package j.c.a.b.b0;

import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import java.lang.reflect.Constructor;

public final class StaticLayoutBuilderCompat {

    /* renamed from: k  reason: collision with root package name */
    public static boolean f2185k;

    /* renamed from: l  reason: collision with root package name */
    public static Constructor<StaticLayout> f2186l;

    /* renamed from: m  reason: collision with root package name */
    public static Object f2187m;
    public CharSequence a;
    public final TextPaint b;
    public final int c;
    public int d = 0;

    /* renamed from: e  reason: collision with root package name */
    public int f2188e;

    /* renamed from: f  reason: collision with root package name */
    public Layout.Alignment f2189f;
    public int g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f2190i;

    /* renamed from: j  reason: collision with root package name */
    public TextUtils.TruncateAt f2191j;

    public static class a extends Exception {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public a(java.lang.Throwable r3) {
            /*
                r2 = this;
                java.lang.String r0 = "Error thrown initializing StaticLayout "
                java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
                java.lang.String r1 = r3.getMessage()
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r2.<init>(r0, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.b0.StaticLayoutBuilderCompat.a.<init>(java.lang.Throwable):void");
        }
    }

    public StaticLayoutBuilderCompat(CharSequence charSequence, TextPaint textPaint, int i2) {
        this.a = charSequence;
        this.b = textPaint;
        this.c = i2;
        this.f2188e = charSequence.length();
        this.f2189f = Layout.Alignment.ALIGN_NORMAL;
        this.g = Integer.MAX_VALUE;
        this.h = true;
        this.f2191j = null;
    }

    public StaticLayout a() {
        if (this.a == null) {
            this.a = "";
        }
        int max = Math.max(0, this.c);
        CharSequence charSequence = this.a;
        if (this.g == 1) {
            charSequence = TextUtils.ellipsize(charSequence, this.b, (float) max, this.f2191j);
        }
        this.f2188e = Math.min(charSequence.length(), this.f2188e);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            if (this.f2190i) {
                this.f2189f = Layout.Alignment.ALIGN_OPPOSITE;
            }
            StaticLayout.Builder obtain = StaticLayout.Builder.obtain(charSequence, this.d, this.f2188e, this.b, max);
            obtain.setAlignment(this.f2189f);
            obtain.setIncludePad(this.h);
            obtain.setTextDirection(this.f2190i ? TextDirectionHeuristics.RTL : TextDirectionHeuristics.LTR);
            TextUtils.TruncateAt truncateAt = this.f2191j;
            if (truncateAt != null) {
                obtain.setEllipsize(truncateAt);
            }
            obtain.setMaxLines(this.g);
            return obtain.build();
        }
        if (!f2185k) {
            try {
                Class<TextDirectionHeuristic> cls = TextDirectionHeuristic.class;
                f2187m = this.f2190i && i2 >= 23 ? TextDirectionHeuristics.RTL : TextDirectionHeuristics.LTR;
                Constructor<StaticLayout> declaredConstructor = StaticLayout.class.getDeclaredConstructor(CharSequence.class, Integer.TYPE, Integer.TYPE, TextPaint.class, Integer.TYPE, Layout.Alignment.class, cls, Float.TYPE, Float.TYPE, Boolean.TYPE, TextUtils.TruncateAt.class, Integer.TYPE, Integer.TYPE);
                f2186l = declaredConstructor;
                declaredConstructor.setAccessible(true);
                f2185k = true;
            } catch (Exception e2) {
                throw new a(e2);
            }
        }
        try {
            Constructor<StaticLayout> constructor = f2186l;
            ResourcesFlusher.a(constructor);
            Object obj = f2187m;
            ResourcesFlusher.a(obj);
            return (StaticLayout) constructor.newInstance(charSequence, Integer.valueOf(this.d), Integer.valueOf(this.f2188e), this.b, Integer.valueOf(max), this.f2189f, obj, Float.valueOf(1.0f), Float.valueOf(0.0f), Boolean.valueOf(this.h), null, Integer.valueOf(max), Integer.valueOf(this.g));
        } catch (Exception e3) {
            throw new a(e3);
        }
    }
}
