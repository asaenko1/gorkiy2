package j.c.a.b.a0;

import android.animation.FloatEvaluator;
import android.animation.TypeEvaluator;

/* compiled from: FloatingActionButtonImpl */
public class FloatingActionButtonImpl2 implements TypeEvaluator<Float> {
    public FloatEvaluator a = new FloatEvaluator();

    public FloatingActionButtonImpl2(FloatingActionButtonImpl1 floatingActionButtonImpl1) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.animation.FloatEvaluator.evaluate(float, java.lang.Number, java.lang.Number):java.lang.Float}
     arg types: [float, java.lang.Float, java.lang.Float]
     candidates:
      ClspMth{android.animation.FloatEvaluator.evaluate(float, java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{android.animation.TypeEvaluator.evaluate(float, java.lang.Object, java.lang.Object):T}
      ClspMth{android.animation.FloatEvaluator.evaluate(float, java.lang.Number, java.lang.Number):java.lang.Float} */
    public Object evaluate(float f2, Object obj, Object obj2) {
        float floatValue = this.a.evaluate(f2, (Number) ((Float) obj), (Number) ((Float) obj2)).floatValue();
        if (floatValue < 0.1f) {
            floatValue = 0.0f;
        }
        return Float.valueOf(floatValue);
    }
}
