package j.c.a.b.k0;

import com.google.android.material.textfield.TextInputLayout;

public class CustomEndIconDelegate extends EndIconDelegate {
    public CustomEndIconDelegate(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    public void a() {
        super.a.setEndIconOnClickListener(null);
        super.a.setEndIconOnLongClickListener(null);
    }
}
