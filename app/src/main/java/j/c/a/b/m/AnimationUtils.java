package j.c.a.b.m;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import i.m.a.a.FastOutLinearInInterpolator;
import i.m.a.a.FastOutSlowInInterpolator;
import i.m.a.a.LinearOutSlowInInterpolator;

public class AnimationUtils {
    public static final TimeInterpolator a = new LinearInterpolator();
    public static final TimeInterpolator b = new FastOutSlowInInterpolator();
    public static final TimeInterpolator c = new FastOutLinearInInterpolator();
    public static final TimeInterpolator d = new LinearOutSlowInInterpolator();

    /* renamed from: e  reason: collision with root package name */
    public static final TimeInterpolator f2294e = new DecelerateInterpolator();

    public static float a(float f2, float f3, float f4) {
        return ((f3 - f2) * f4) + f2;
    }

    public static int a(int i2, int i3, float f2) {
        return Math.round(f2 * ((float) (i3 - i2))) + i2;
    }
}
