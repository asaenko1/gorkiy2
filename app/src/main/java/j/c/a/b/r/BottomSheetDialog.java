package j.c.a.b.r;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import i.b.k.AppCompatDialog;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import j.c.a.b.f;
import j.c.a.b.h;

public class BottomSheetDialog extends AppCompatDialog {
    public BottomSheetBehavior<FrameLayout> d;

    /* renamed from: e  reason: collision with root package name */
    public FrameLayout f2325e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f2326f;
    public boolean g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public BottomSheetBehavior.d f2327i;

    public class a implements View.OnClickListener {
        public a() {
        }

        public void onClick(View view) {
            BottomSheetDialog bottomSheetDialog = BottomSheetDialog.this;
            if (bottomSheetDialog.f2326f && bottomSheetDialog.isShowing()) {
                BottomSheetDialog bottomSheetDialog2 = BottomSheetDialog.this;
                if (!bottomSheetDialog2.h) {
                    TypedArray obtainStyledAttributes = bottomSheetDialog2.getContext().obtainStyledAttributes(new int[]{16843611});
                    bottomSheetDialog2.g = obtainStyledAttributes.getBoolean(0, true);
                    obtainStyledAttributes.recycle();
                    bottomSheetDialog2.h = true;
                }
                if (bottomSheetDialog2.g) {
                    BottomSheetDialog.this.cancel();
                }
            }
        }
    }

    public class c implements View.OnTouchListener {
        public c(BottomSheetDialog bottomSheetDialog) {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }
    }

    public class d extends BottomSheetBehavior.d {
        public d() {
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BottomSheetDialog(android.content.Context r4, int r5) {
        /*
            r3 = this;
            r0 = 1
            if (r5 != 0) goto L_0x0019
            android.util.TypedValue r5 = new android.util.TypedValue
            r5.<init>()
            android.content.res.Resources$Theme r1 = r4.getTheme()
            int r2 = j.c.a.b.b.bottomSheetDialogTheme
            boolean r1 = r1.resolveAttribute(r2, r5, r0)
            if (r1 == 0) goto L_0x0017
            int r5 = r5.resourceId
            goto L_0x0019
        L_0x0017:
            int r5 = j.c.a.b.k.Theme_Design_Light_BottomSheetDialog
        L_0x0019:
            r3.<init>(r4, r5)
            r3.f2326f = r0
            r3.g = r0
            j.c.a.b.r.BottomSheetDialog$d r4 = new j.c.a.b.r.BottomSheetDialog$d
            r4.<init>()
            r3.f2327i = r4
            r3.a(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.r.BottomSheetDialog.<init>(android.content.Context, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.coordinatorlayout.widget.CoordinatorLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View a(int i2, View view, ViewGroup.LayoutParams layoutParams) {
        b();
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) this.f2325e.findViewById(f.coordinator);
        if (i2 != 0 && view == null) {
            view = getLayoutInflater().inflate(i2, (ViewGroup) coordinatorLayout, false);
        }
        FrameLayout frameLayout = (FrameLayout) this.f2325e.findViewById(f.design_bottom_sheet);
        frameLayout.removeAllViews();
        if (layoutParams == null) {
            frameLayout.addView(view);
        } else {
            frameLayout.addView(view, layoutParams);
        }
        coordinatorLayout.findViewById(f.touch_outside).setOnClickListener(new a());
        ViewCompat.a(frameLayout, new b());
        frameLayout.setOnTouchListener(new c(this));
        return this.f2325e;
    }

    public final FrameLayout b() {
        if (this.f2325e == null) {
            FrameLayout frameLayout = (FrameLayout) View.inflate(getContext(), h.design_bottom_sheet_dialog, null);
            this.f2325e = frameLayout;
            BottomSheetBehavior<FrameLayout> b2 = BottomSheetBehavior.b((FrameLayout) frameLayout.findViewById(f.design_bottom_sheet));
            this.d = b2;
            BottomSheetBehavior.d dVar = this.f2327i;
            if (!b2.F.contains(dVar)) {
                b2.F.add(dVar);
            }
            this.d.a(this.f2326f);
        }
        return this.f2325e;
    }

    public void cancel() {
        if (this.d == null) {
            b();
        }
        super.cancel();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            window.clearFlags(67108864);
            window.addFlags(RecyclerView.UNDEFINED_DURATION);
            window.setLayout(-1, -1);
        }
    }

    public void onStart() {
        super.onStart();
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.d;
        if (bottomSheetBehavior != null && bottomSheetBehavior.w == 5) {
            bottomSheetBehavior.c(4);
        }
    }

    public void setCancelable(boolean z) {
        super.setCancelable(z);
        if (this.f2326f != z) {
            this.f2326f = z;
            BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.d;
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.a(z);
            }
        }
    }

    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        if (z && !this.f2326f) {
            this.f2326f = true;
        }
        this.g = z;
        this.h = true;
    }

    public void setContentView(int i2) {
        super.setContentView(a(i2, null, null));
    }

    public void setContentView(View view) {
        super.setContentView(a(0, view, null));
    }

    public class b extends AccessibilityDelegateCompat {
        public b() {
        }

        public boolean a(View view, int i2, Bundle bundle) {
            if (i2 == 1048576) {
                BottomSheetDialog bottomSheetDialog = BottomSheetDialog.this;
                if (bottomSheetDialog.f2326f) {
                    bottomSheetDialog.cancel();
                    return true;
                }
            }
            return super.a(view, i2, bundle);
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            if (BottomSheetDialog.this.f2326f) {
                accessibilityNodeInfoCompat.a.addAction(1048576);
                accessibilityNodeInfoCompat.a.setDismissable(true);
                return;
            }
            accessibilityNodeInfoCompat.a.setDismissable(false);
        }
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(a(0, view, layoutParams));
    }
}
