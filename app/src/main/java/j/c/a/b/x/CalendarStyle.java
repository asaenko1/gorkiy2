package j.c.a.b.x;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.l;

public final class CalendarStyle {
    public final CalendarItemStyle a;
    public final CalendarItemStyle b;
    public final CalendarItemStyle c;
    public final CalendarItemStyle d;

    /* renamed from: e  reason: collision with root package name */
    public final CalendarItemStyle f2360e;

    /* renamed from: f  reason: collision with root package name */
    public final CalendarItemStyle f2361f;
    public final CalendarItemStyle g;
    public final Paint h;

    public CalendarStyle(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(c.a(context, b.materialCalendarStyle, MaterialCalendar.class.getCanonicalName()), l.MaterialCalendar);
        this.a = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_dayStyle, 0));
        this.g = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_dayInvalidStyle, 0));
        this.b = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_daySelectedStyle, 0));
        this.c = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_dayTodayStyle, 0));
        ColorStateList a2 = c.a(context, obtainStyledAttributes, l.MaterialCalendar_rangeFillColor);
        this.d = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_yearStyle, 0));
        this.f2360e = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_yearSelectedStyle, 0));
        this.f2361f = CalendarItemStyle.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendar_yearTodayStyle, 0));
        Paint paint = new Paint();
        this.h = paint;
        paint.setColor(a2.getDefaultColor());
        obtainStyledAttributes.recycle();
    }
}
