package j.c.a.b.x;

import android.view.View;

/* compiled from: MaterialCalendar */
public class MaterialCalendar2 implements View.OnClickListener {
    public final /* synthetic */ MonthsPagerAdapter0 b;
    public final /* synthetic */ MaterialCalendar c;

    public MaterialCalendar2(MaterialCalendar materialCalendar, MonthsPagerAdapter0 monthsPagerAdapter0) {
        this.c = materialCalendar;
        this.b = monthsPagerAdapter0;
    }

    public void onClick(View view) {
        int t2 = this.c.N().t() - 1;
        if (t2 >= 0) {
            this.c.a(this.b.b(t2));
        }
    }
}
