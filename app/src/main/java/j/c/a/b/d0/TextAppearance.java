package j.c.a.b.d0;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextPaint;
import android.util.Log;
import android.util.TypedValue;
import i.b.k.ResourcesFlusher;
import i.h.e.b.ResourcesCompat;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.c.a.b.l;

public class TextAppearance {
    public final float a;
    public final ColorStateList b;
    public final int c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2196e;

    /* renamed from: f  reason: collision with root package name */
    public final ColorStateList f2197f;
    public final float g;
    public final float h;

    /* renamed from: i  reason: collision with root package name */
    public final float f2198i;

    /* renamed from: j  reason: collision with root package name */
    public final int f2199j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f2200k = false;

    /* renamed from: l  reason: collision with root package name */
    public Typeface f2201l;

    public TextAppearance(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, l.TextAppearance);
        this.a = obtainStyledAttributes.getDimension(l.TextAppearance_android_textSize, 0.0f);
        this.b = c.a(context, obtainStyledAttributes, l.TextAppearance_android_textColor);
        c.a(context, obtainStyledAttributes, l.TextAppearance_android_textColorHint);
        c.a(context, obtainStyledAttributes, l.TextAppearance_android_textColorLink);
        this.c = obtainStyledAttributes.getInt(l.TextAppearance_android_textStyle, 0);
        this.d = obtainStyledAttributes.getInt(l.TextAppearance_android_typeface, 1);
        int i3 = l.TextAppearance_fontFamily;
        i3 = !obtainStyledAttributes.hasValue(i3) ? l.TextAppearance_android_fontFamily : i3;
        this.f2199j = obtainStyledAttributes.getResourceId(i3, 0);
        this.f2196e = obtainStyledAttributes.getString(i3);
        obtainStyledAttributes.getBoolean(l.TextAppearance_textAllCaps, false);
        this.f2197f = c.a(context, obtainStyledAttributes, l.TextAppearance_android_shadowColor);
        this.g = obtainStyledAttributes.getFloat(l.TextAppearance_android_shadowDx, 0.0f);
        this.h = obtainStyledAttributes.getFloat(l.TextAppearance_android_shadowDy, 0.0f);
        this.f2198i = obtainStyledAttributes.getFloat(l.TextAppearance_android_shadowRadius, 0.0f);
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.content.Context, int, android.util.TypedValue, int, i.h.e.b.ResourcesCompat, android.os.Handler, boolean):android.graphics.Typeface
     arg types: [android.content.Context, int, android.util.TypedValue, int, j.c.a.b.d0.TextAppearance$a, ?[OBJECT, ARRAY], int]
     candidates:
      i.b.k.ResourcesFlusher.a(androidx.recyclerview.widget.RecyclerView$a0, i.r.d.OrientationHelper, android.view.View, android.view.View, androidx.recyclerview.widget.RecyclerView$o, boolean, boolean):int
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, android.animation.ValueAnimator, float, org.xmlpull.v1.XmlPullParser):android.animation.ValueAnimator
      i.b.k.ResourcesFlusher.a(android.content.Context, int, android.util.TypedValue, int, i.h.e.b.ResourcesCompat, android.os.Handler, boolean):android.graphics.Typeface */
    public void a(Context context, TextAppearanceFontCallback textAppearanceFontCallback) {
        a();
        if (this.f2199j == 0) {
            this.f2200k = true;
        }
        if (this.f2200k) {
            textAppearanceFontCallback.a(this.f2201l, true);
            return;
        }
        try {
            int i2 = this.f2199j;
            a aVar = new a(textAppearanceFontCallback);
            if (context.isRestricted()) {
                aVar.a(-4, (Handler) null);
                return;
            }
            ResourcesFlusher.a(context, i2, new TypedValue(), 0, (ResourcesCompat) aVar, (Handler) null, false);
        } catch (Resources.NotFoundException unused) {
            this.f2200k = true;
            textAppearanceFontCallback.a(1);
        } catch (Exception e2) {
            StringBuilder a2 = outline.a("Error loading font ");
            a2.append(this.f2196e);
            Log.d("TextAppearance", a2.toString(), e2);
            this.f2200k = true;
            textAppearanceFontCallback.a(-3);
        }
    }

    public class a extends ResourcesCompat {
        public final /* synthetic */ TextAppearanceFontCallback a;

        public a(TextAppearanceFontCallback textAppearanceFontCallback) {
            this.a = textAppearanceFontCallback;
        }

        public void a(Typeface typeface) {
            TextAppearance textAppearance = TextAppearance.this;
            textAppearance.f2201l = Typeface.create(typeface, textAppearance.c);
            TextAppearance textAppearance2 = TextAppearance.this;
            textAppearance2.f2200k = true;
            this.a.a(textAppearance2.f2201l, false);
        }

        public void a(int i2) {
            TextAppearance.this.f2200k = true;
            this.a.a(i2);
        }
    }

    public void a(Context context, TextPaint textPaint, TextAppearanceFontCallback textAppearanceFontCallback) {
        a();
        a(textPaint, this.f2201l);
        a(context, new TextAppearance0(this, textPaint, textAppearanceFontCallback));
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.f2198i;
        float f3 = this.g;
        float f4 = this.h;
        ColorStateList colorStateList2 = this.f2197f;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    public final void a() {
        String str;
        if (this.f2201l == null && (str = this.f2196e) != null) {
            this.f2201l = Typeface.create(str, this.c);
        }
        if (this.f2201l == null) {
            int i2 = this.d;
            if (i2 == 1) {
                this.f2201l = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.f2201l = Typeface.SERIF;
            } else if (i2 != 3) {
                this.f2201l = Typeface.DEFAULT;
            } else {
                this.f2201l = Typeface.MONOSPACE;
            }
            this.f2201l = Typeface.create(this.f2201l, this.c);
        }
    }

    public void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int i2 = (~typeface.getStyle()) & this.c;
        textPaint.setFakeBoldText((i2 & 1) != 0);
        textPaint.setTextSkewX((i2 & 2) != 0 ? -0.25f : 0.0f);
        textPaint.setTextSize(this.a);
    }
}
