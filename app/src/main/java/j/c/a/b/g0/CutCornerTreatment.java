package j.c.a.b.g0;

public class CutCornerTreatment extends CornerTreatment {
    public float a = -1.0f;

    public void a(ShapePath shapePath, float f2, float f3, float f4) {
        shapePath.a(0.0f, f4 * f3, 180.0f, 180.0f - f2);
        double d = (double) f4;
        double d2 = (double) f3;
        shapePath.a((float) (Math.sin(Math.toRadians((double) f2)) * d * d2), (float) (Math.sin(Math.toRadians((double) (90.0f - f2))) * d * d2));
    }
}
