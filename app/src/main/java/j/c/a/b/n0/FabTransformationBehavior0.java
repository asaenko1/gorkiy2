package j.c.a.b.n0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.Drawable;
import com.google.android.material.transformation.FabTransformationBehavior;
import j.c.a.b.w.CircularRevealWidget;

/* compiled from: FabTransformationBehavior */
public class FabTransformationBehavior0 extends AnimatorListenerAdapter {
    public final /* synthetic */ CircularRevealWidget a;
    public final /* synthetic */ Drawable b;

    public FabTransformationBehavior0(FabTransformationBehavior fabTransformationBehavior, CircularRevealWidget circularRevealWidget, Drawable drawable) {
        this.a = circularRevealWidget;
        this.b = drawable;
    }

    public void onAnimationEnd(Animator animator) {
        this.a.setCircularRevealOverlayDrawable(null);
    }

    public void onAnimationStart(Animator animator) {
        this.a.setCircularRevealOverlayDrawable(this.b);
    }
}
