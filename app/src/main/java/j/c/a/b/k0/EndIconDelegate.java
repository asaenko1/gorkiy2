package j.c.a.b.k0;

import android.content.Context;
import com.google.android.material.internal.CheckableImageButton;
import com.google.android.material.textfield.TextInputLayout;

public abstract class EndIconDelegate {
    public TextInputLayout a;
    public Context b;
    public CheckableImageButton c;

    public EndIconDelegate(TextInputLayout textInputLayout) {
        this.a = textInputLayout;
        this.b = textInputLayout.getContext();
        this.c = textInputLayout.getEndIconView();
    }

    public abstract void a();

    public void a(boolean z) {
    }

    public boolean a(int i2) {
        return true;
    }

    public boolean b() {
        return false;
    }
}
