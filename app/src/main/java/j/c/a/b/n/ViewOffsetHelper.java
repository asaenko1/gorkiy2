package j.c.a.b.n;

import android.view.View;
import i.h.l.ViewCompat;

public class ViewOffsetHelper {
    public final View a;
    public int b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f2303e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f2304f = true;
    public boolean g = true;

    public ViewOffsetHelper(View view) {
        this.a = view;
    }

    public void a() {
        View view = this.a;
        ViewCompat.e(view, this.d - (view.getTop() - this.b));
        View view2 = this.a;
        ViewCompat.d(view2, this.f2303e - (view2.getLeft() - this.c));
    }
}
