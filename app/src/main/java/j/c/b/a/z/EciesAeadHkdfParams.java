package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.EciesAeadDemParams;
import j.c.b.a.z.EciesHkdfKemParams;
import j.c.b.a.z.a1;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class EciesAeadHkdfParams extends GeneratedMessageLite<a1, a1.b> implements b1 {
    public static final EciesAeadHkdfParams h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<a1> f2450i;

    /* renamed from: e  reason: collision with root package name */
    public EciesHkdfKemParams f2451e;

    /* renamed from: f  reason: collision with root package name */
    public EciesAeadDemParams f2452f;
    public int g;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.EciesAeadHkdfParams.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.EciesAeadHkdfParams.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.EciesAeadHkdfParams.a.<clinit>():void");
        }
    }

    static {
        EciesAeadHkdfParams eciesAeadHkdfParams = new EciesAeadHkdfParams();
        h = eciesAeadHkdfParams;
        super.f();
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [j.c.e.MessageLite, j.c.b.a.z.EciesAeadDemParams] */
    /* JADX WARN: Type inference failed for: r1v4, types: [j.c.b.a.z.EciesHkdfKemParams, j.c.e.MessageLite] */
    public void a(CodedOutputStream codedOutputStream) {
        if (this.f2451e != null) {
            codedOutputStream.a(1, (MessageLite) k());
        }
        if (this.f2452f != null) {
            codedOutputStream.a(2, (MessageLite) g());
        }
        int i2 = this.g;
        if (i2 != EcPointFormat.UNKNOWN_FORMAT.value) {
            codedOutputStream.a(3, i2);
        }
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [j.c.e.MessageLite, j.c.b.a.z.EciesAeadDemParams] */
    /* JADX WARN: Type inference failed for: r2v4, types: [j.c.b.a.z.EciesHkdfKemParams, j.c.e.MessageLite] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (this.f2451e != null) {
            i3 = 0 + CodedOutputStream.b(1, (MessageLite) k());
        }
        if (this.f2452f != null) {
            i3 += CodedOutputStream.b(2, (MessageLite) g());
        }
        int i4 = this.g;
        if (i4 != EcPointFormat.UNKNOWN_FORMAT.value) {
            i3 += CodedOutputStream.c(3, i4);
        }
        super.d = i3;
        return i3;
    }

    public EciesAeadDemParams g() {
        EciesAeadDemParams eciesAeadDemParams = this.f2452f;
        return eciesAeadDemParams == null ? EciesAeadDemParams.f2446f : eciesAeadDemParams;
    }

    public EcPointFormat h() {
        EcPointFormat a2 = EcPointFormat.a(this.g);
        return a2 == null ? EcPointFormat.UNRECOGNIZED : a2;
    }

    public EciesHkdfKemParams k() {
        EciesHkdfKemParams eciesHkdfKemParams = this.f2451e;
        return eciesHkdfKemParams == null ? EciesHkdfKemParams.h : eciesHkdfKemParams;
    }

    public static final class b extends GeneratedMessageLite.b<a1, a1.b> implements b1 {
        public b() {
            super(EciesAeadHkdfParams.h);
        }

        public /* synthetic */ b(a aVar) {
            super(EciesAeadHkdfParams.h);
        }
    }

    /* JADX WARN: Type inference failed for: r5v3, types: [j.c.b.a.z.EciesHkdfKemParams, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r2v1, types: [j.c.b.a.z.EciesHkdfKemParams, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r5v6, types: [j.c.e.MessageLite, j.c.b.a.z.EciesAeadDemParams] */
    /* JADX WARN: Type inference failed for: r2v2, types: [j.c.e.MessageLite, j.c.b.a.z.EciesAeadDemParams] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                EciesAeadHkdfParams eciesAeadHkdfParams = (EciesAeadHkdfParams) obj2;
                this.f2451e = (EciesHkdfKemParams) kVar.a((MessageLite) this.f2451e, (MessageLite) eciesAeadHkdfParams.f2451e);
                this.f2452f = (EciesAeadDemParams) kVar.a((MessageLite) this.f2452f, (MessageLite) eciesAeadHkdfParams.f2452f);
                boolean z2 = this.g != 0;
                int i2 = this.g;
                if (eciesAeadHkdfParams.g != 0) {
                    z = true;
                }
                this.g = kVar.a(z2, i2, z, eciesAeadHkdfParams.g);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 10) {
                                EciesHkdfKemParams.b bVar = this.f2451e != null ? (EciesHkdfKemParams.b) this.f2451e.e() : null;
                                EciesHkdfKemParams eciesHkdfKemParams = (EciesHkdfKemParams) codedInputStream.a(EciesHkdfKemParams.h.i(), extensionRegistryLite);
                                this.f2451e = eciesHkdfKemParams;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2451e = (EciesHkdfKemParams) bVar.l();
                                }
                            } else if (g2 == 18) {
                                EciesAeadDemParams.b bVar2 = this.f2452f != null ? (EciesAeadDemParams.b) this.f2452f.e() : null;
                                EciesAeadDemParams eciesAeadDemParams = (EciesAeadDemParams) codedInputStream.a(EciesAeadDemParams.f2446f.i(), extensionRegistryLite);
                                this.f2452f = eciesAeadDemParams;
                                if (bVar2 != null) {
                                    bVar2.a(super);
                                    this.f2452f = (EciesAeadDemParams) bVar2.l();
                                }
                            } else if (g2 == 24) {
                                this.g = codedInputStream.d();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new EciesAeadHkdfParams();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2450i == null) {
                    synchronized (EciesAeadHkdfParams.class) {
                        if (f2450i == null) {
                            f2450i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2450i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
