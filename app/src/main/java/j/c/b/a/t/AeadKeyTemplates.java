package j.c.b.a.t;

import j.c.b.a.z.AesCtrHmacAeadKeyFormat;
import j.c.b.a.z.AesCtrKeyFormat;
import j.c.b.a.z.AesCtrParams;
import j.c.b.a.z.AesEaxKeyFormat;
import j.c.b.a.z.AesEaxParams;
import j.c.b.a.z.AesGcmKeyFormat;
import j.c.b.a.z.HashType;
import j.c.b.a.z.HmacKeyFormat;
import j.c.b.a.z.HmacParams;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.OutputPrefixType;
import j.c.e.ByteString;

public final class AeadKeyTemplates {
    public static final KeyTemplate a = a(32);

    static {
        a(16);
        a(16, 16);
        a(32, 16);
        a(16, 16, 32, 16, HashType.SHA256);
        a(32, 16, 32, 32, HashType.SHA256);
        KeyTemplate.b g = KeyTemplate.g();
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key");
        g.a(OutputPrefixType.TINK);
        KeyTemplate keyTemplate = (KeyTemplate) g.k();
    }

    public static KeyTemplate a(int i2) {
        AesGcmKeyFormat.b bVar = (AesGcmKeyFormat.b) AesGcmKeyFormat.f2426f.e();
        bVar.m();
        ((AesGcmKeyFormat) bVar.c).f2427e = i2;
        KeyTemplate.b g = KeyTemplate.g();
        ByteString a2 = ((AesGcmKeyFormat) bVar.k()).a();
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, a2);
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, "type.googleapis.com/google.crypto.tink.AesGcmKey");
        g.a(OutputPrefixType.TINK);
        return (KeyTemplate) g.k();
    }

    public static KeyTemplate a(int i2, int i3, int i4, int i5, HashType hashType) {
        AesCtrKeyFormat.b bVar = (AesCtrKeyFormat.b) AesCtrKeyFormat.g.e();
        AesCtrParams.b bVar2 = (AesCtrParams.b) AesCtrParams.f2407f.e();
        bVar2.m();
        ((AesCtrParams) bVar2.c).f2408e = i3;
        bVar.m();
        AesCtrKeyFormat.a((AesCtrKeyFormat) bVar.c, (AesCtrParams) bVar2.k());
        bVar.m();
        ((AesCtrKeyFormat) bVar.c).f2406f = i2;
        HmacKeyFormat.b bVar3 = (HmacKeyFormat.b) HmacKeyFormat.g.e();
        HmacParams.b bVar4 = (HmacParams.b) HmacParams.g.e();
        bVar4.m();
        HmacParams.a((HmacParams) bVar4.c, hashType);
        bVar4.m();
        ((HmacParams) bVar4.c).f2476f = i5;
        bVar3.m();
        HmacKeyFormat.a((HmacKeyFormat) bVar3.c, (HmacParams) bVar4.k());
        bVar3.m();
        ((HmacKeyFormat) bVar3.c).f2474f = i4;
        AesCtrHmacAeadKeyFormat.b bVar5 = (AesCtrHmacAeadKeyFormat.b) AesCtrHmacAeadKeyFormat.g.e();
        bVar5.m();
        AesCtrHmacAeadKeyFormat.a((AesCtrHmacAeadKeyFormat) bVar5.c, (AesCtrKeyFormat) bVar.k());
        bVar5.m();
        AesCtrHmacAeadKeyFormat.a((AesCtrHmacAeadKeyFormat) bVar5.c, (HmacKeyFormat) bVar3.k());
        KeyTemplate.b g = KeyTemplate.g();
        ByteString a2 = ((AesCtrHmacAeadKeyFormat) bVar5.k()).a();
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, a2);
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey");
        g.a(OutputPrefixType.TINK);
        return (KeyTemplate) g.k();
    }

    public static KeyTemplate a(int i2, int i3) {
        AesEaxKeyFormat.b bVar = (AesEaxKeyFormat.b) AesEaxKeyFormat.g.e();
        bVar.m();
        ((AesEaxKeyFormat) bVar.c).f2413f = i2;
        AesEaxParams.b bVar2 = (AesEaxParams.b) AesEaxParams.f2414f.e();
        bVar2.m();
        ((AesEaxParams) bVar2.c).f2415e = i3;
        bVar.m();
        AesEaxKeyFormat.a((AesEaxKeyFormat) bVar.c, (AesEaxParams) bVar2.k());
        KeyTemplate.b g = KeyTemplate.g();
        ByteString a2 = ((AesEaxKeyFormat) bVar.k()).a();
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, a2);
        g.m();
        KeyTemplate.a((KeyTemplate) g.c, "type.googleapis.com/google.crypto.tink.AesEaxKey");
        g.a(OutputPrefixType.TINK);
        return (KeyTemplate) g.k();
    }
}
