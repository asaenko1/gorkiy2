package j.c.b.a.b0;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.c0.f0;
import java.security.GeneralSecurityException;

public class StreamingAeadCatalogue implements Catalogue<f0> {
    public KeyManager<f0> a(String str, String str2, int i2) {
        KeyManager<f0> keyManager;
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 754366121 && lowerCase.equals("streamingaead")) ? (char) 0 : 65535) == 0) {
            int hashCode = str.hashCode();
            if (hashCode != -2002307740) {
                if (hashCode == -608502222 && str.equals("type.googleapis.com/google.crypto.tink.AesGcmHkdfStreamingKey")) {
                    c = 1;
                }
            } else if (str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacStreamingKey")) {
                c = 0;
            }
            if (c == 0) {
                keyManager = new AesCtrHmacStreamingKeyManager();
            } else if (c == 1) {
                keyManager = new AesGcmHkdfStreamingKeyManager();
            } else {
                throw new GeneralSecurityException(String.format("No support for primitive 'StreamingAead' with key type '%s'.", str));
            }
            if (keyManager.b() >= i2) {
                return keyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
