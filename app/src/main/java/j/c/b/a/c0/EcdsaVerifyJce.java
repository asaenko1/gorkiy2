package j.c.b.a.c0;

import j.c.a.a.c.n.c;
import j.c.b.a.PublicKeyVerify;
import java.security.interfaces.ECPublicKey;

public final class EcdsaVerifyJce implements PublicKeyVerify {
    public EcdsaVerifyJce(ECPublicKey eCPublicKey, Enums enums, EllipticCurves0 ellipticCurves0) {
        c.a(eCPublicKey.getW(), eCPublicKey.getParams().getCurve());
        Validators.a(enums);
        enums + "withECDSA";
    }
}
