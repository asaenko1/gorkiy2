package j.c.b.a.c0;

import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.c.b.a.Mac;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public final class MacJce implements Mac {
    public javax.crypto.Mac a;
    public final int b;
    public final String c;
    public final Key d;

    public MacJce(String str, Key key, int i2) {
        if (i2 >= 10) {
            char c2 = 65535;
            int hashCode = str.hashCode();
            if (hashCode != -1823053428) {
                if (hashCode != 392315118) {
                    if (hashCode == 392317873 && str.equals("HMACSHA512")) {
                        c2 = 2;
                    }
                } else if (str.equals("HMACSHA256")) {
                    c2 = 1;
                }
            } else if (str.equals("HMACSHA1")) {
                c2 = 0;
            }
            if (c2 != 0) {
                if (c2 != 1) {
                    if (c2 != 2) {
                        throw new NoSuchAlgorithmException(outline.a("unknown Hmac algorithm: ", str));
                    } else if (i2 > 64) {
                        throw new InvalidAlgorithmParameterException("tag size too big");
                    }
                } else if (i2 > 32) {
                    throw new InvalidAlgorithmParameterException("tag size too big");
                }
            } else if (i2 > 20) {
                throw new InvalidAlgorithmParameterException("tag size too big");
            }
            this.c = str;
            this.b = i2;
            this.d = key;
            javax.crypto.Mac a2 = EngineFactory.g.a(str);
            this.a = a2;
            a2.init(key);
            return;
        }
        throw new InvalidAlgorithmParameterException("tag size too small, need at least 10 bytes");
    }

    public byte[] a(byte[] bArr) {
        javax.crypto.Mac mac;
        try {
            mac = (javax.crypto.Mac) this.a.clone();
        } catch (CloneNotSupportedException unused) {
            mac = EngineFactory.g.a(this.c);
            mac.init(this.d);
        }
        mac.update(bArr);
        byte[] bArr2 = new byte[this.b];
        System.arraycopy(mac.doFinal(), 0, bArr2, 0, this.b);
        return bArr2;
    }

    public void a(byte[] bArr, byte[] bArr2) {
        if (!c.b(a(bArr2), bArr)) {
            throw new GeneralSecurityException("invalid MAC");
        }
    }
}
