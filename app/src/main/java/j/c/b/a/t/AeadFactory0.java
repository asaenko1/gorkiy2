package j.c.b.a.t;

import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.c.b.a.Aead;
import j.c.b.a.CryptoFormat;
import j.c.b.a.PrimitiveSet;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.logging.Logger;

/* compiled from: AeadFactory */
public class AeadFactory0 implements Aead {
    public final /* synthetic */ PrimitiveSet a;

    public AeadFactory0(PrimitiveSet primitiveSet) {
        this.a = primitiveSet;
    }

    public byte[] a(byte[] bArr, byte[] bArr2) {
        return c.a(this.a.b.a(), ((Aead) this.a.b.a).a(bArr, bArr2));
    }

    public byte[] b(byte[] bArr, byte[] bArr2) {
        if (bArr.length > 5) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, 5);
            byte[] copyOfRange2 = Arrays.copyOfRange(bArr, 5, bArr.length);
            for (PrimitiveSet.a aVar : this.a.a(copyOfRange)) {
                try {
                    return ((Aead) aVar.a).b(copyOfRange2, bArr2);
                } catch (GeneralSecurityException e2) {
                    Logger logger = AeadFactory.a;
                    StringBuilder a2 = outline.a("ciphertext prefix matches a key, but cannot decrypt: ");
                    a2.append(e2.toString());
                    logger.info(a2.toString());
                }
            }
        }
        for (PrimitiveSet.a aVar2 : this.a.a(CryptoFormat.a)) {
            try {
                return ((Aead) aVar2.a).b(bArr, bArr2);
            } catch (GeneralSecurityException unused) {
            }
        }
        throw new GeneralSecurityException("decryption failed");
    }
}
