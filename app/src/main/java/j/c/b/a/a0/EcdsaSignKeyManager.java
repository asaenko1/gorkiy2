package j.c.b.a.a0;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.PrivateKeyManager;
import j.c.b.a.PublicKeySign;
import j.c.b.a.c0.EcdsaSignJce;
import j.c.b.a.c0.Validators;
import j.c.b.a.p;
import j.c.b.a.z.EcdsaKeyFormat;
import j.c.b.a.z.EcdsaParams;
import j.c.b.a.z.EcdsaPrivateKey;
import j.c.b.a.z.EcdsaPublicKey;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;

public class EcdsaSignKeyManager implements PrivateKeyManager<p> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.EcdsaPrivateKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r6v15, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof EcdsaKeyFormat) {
            EcdsaParams g = ((EcdsaKeyFormat) messageLite).g();
            c.a(g);
            KeyPair a = c.a(c.b(g.g()));
            ECPoint w = ((ECPublicKey) a.getPublic()).getW();
            EcdsaPublicKey.b bVar = (EcdsaPublicKey.b) EcdsaPublicKey.f2442i.e();
            bVar.m();
            bVar.c.f2444e = 0;
            bVar.m();
            EcdsaPublicKey.a(bVar.c, g);
            ByteString a2 = ByteString.a(w.getAffineX().toByteArray());
            bVar.m();
            EcdsaPublicKey.a(bVar.c, a2);
            ByteString a3 = ByteString.a(w.getAffineY().toByteArray());
            bVar.m();
            EcdsaPublicKey.b(bVar.c, a3);
            EcdsaPrivateKey.b bVar2 = (EcdsaPrivateKey.b) EcdsaPrivateKey.h.e();
            bVar2.m();
            bVar2.c.f2440e = 0;
            bVar2.m();
            EcdsaPrivateKey.a(bVar2.c, (EcdsaPublicKey) bVar.k());
            ByteString a4 = ByteString.a(((ECPrivateKey) a.getPrivate()).getS().toByteArray());
            bVar2.m();
            EcdsaPrivateKey.a(bVar2.c, a4);
            return bVar2.k();
        }
        throw new GeneralSecurityException("expected EcdsaKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaPrivateKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.EcdsaPrivateKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((EcdsaPrivateKey) GeneratedMessageLite.a((GeneratedMessageLite) EcdsaPrivateKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized EcdsaPrivateKey proto", e2);
        }
    }

    public PublicKeySign a(MessageLite messageLite) {
        if (messageLite instanceof EcdsaPrivateKey) {
            EcdsaPrivateKey ecdsaPrivateKey = (EcdsaPrivateKey) messageLite;
            Validators.a(ecdsaPrivateKey.f2440e, 0);
            c.a(ecdsaPrivateKey.g().g());
            return new EcdsaSignJce(c.a(c.b(ecdsaPrivateKey.g().g().g()), ecdsaPrivateKey.g.d()), c.a(ecdsaPrivateKey.g().g().k()), c.a(ecdsaPrivateKey.g().g().h()));
        }
        throw new GeneralSecurityException("expected EcdsaPrivateKey proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.EcdsaKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((EcdsaKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) EcdsaKeyFormat.f2434f, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected EcdsaKeyFormat proto", e2);
        }
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.EcdsaPrivateKey".equals(str);
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.EcdsaPrivateKey");
        ByteString a = ((EcdsaPrivateKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.ASYMMETRIC_PRIVATE);
        return (KeyData) g.k();
    }
}
