package j.c.b.a.w;

import com.google.protobuf.InvalidProtocolBufferException;
import j.a.a.a.outline;
import j.c.b.a.Registry;
import j.c.b.a.c0.EciesAeadHkdfDemHelper;
import j.c.b.a.z.AesCtrHmacAeadKey;
import j.c.b.a.z.AesCtrHmacAeadKeyFormat;
import j.c.b.a.z.AesGcmKey;
import j.c.b.a.z.AesGcmKeyFormat;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.z1;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class RegistryEciesAeadHkdfDemHelper implements EciesAeadHkdfDemHelper {
    public final String a;
    public int b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesGcmKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrHmacAeadKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public RegistryEciesAeadHkdfDemHelper(KeyTemplate keyTemplate) {
        String str = keyTemplate.f2481e;
        this.a = str;
        if (str.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
            try {
                ByteString byteString = keyTemplate.f2482f;
                AesGcmKey aesGcmKey = (AesGcmKey) Registry.a((z1) keyTemplate);
                int i2 = ((AesGcmKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesGcmKeyFormat.f2426f, (f) byteString)).f2427e;
            } catch (InvalidProtocolBufferException e2) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e2);
            }
        } else if (this.a.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
            try {
                AesCtrHmacAeadKeyFormat aesCtrHmacAeadKeyFormat = (AesCtrHmacAeadKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrHmacAeadKeyFormat.g, (f) keyTemplate.f2482f);
                AesCtrHmacAeadKey aesCtrHmacAeadKey = (AesCtrHmacAeadKey) Registry.a((z1) keyTemplate);
                this.b = aesCtrHmacAeadKeyFormat.g().f2406f;
                int i3 = aesCtrHmacAeadKeyFormat.h().f2474f;
            } catch (InvalidProtocolBufferException e3) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesCtrHmacAeadKeyFormat", e3);
            }
        } else {
            StringBuilder a2 = outline.a("unsupported AEAD DEM key type: ");
            a2.append(this.a);
            throw new GeneralSecurityException(a2.toString());
        }
    }
}
