package j.c.c.g;

import android.util.Pair;
import j.c.a.a.i.a;
import j.c.a.a.i.e;

public final /* synthetic */ class s implements a {
    public final r a;
    public final Pair b;

    public s(r rVar, Pair pair) {
        this.a = rVar;
        this.b = pair;
    }

    public final Object a(e eVar) {
        this.a.a(this.b, eVar);
        return eVar;
    }
}
