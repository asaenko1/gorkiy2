package j.c.c.g;

import android.text.TextUtils;
import i.e.ArrayMap;
import j.c.a.a.i.f;
import java.util.Map;

public final class z {
    public int a = 0;
    public final Map<Integer, f<Void>> b = new ArrayMap();
    public final v c;

    public z(v vVar) {
        this.c = vVar;
    }

    public final synchronized boolean a() {
        return b() != null;
    }

    public final String b() {
        String b2;
        synchronized (this.c) {
            b2 = this.c.b();
        }
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        String[] split = b2.split(",");
        if (split.length <= 1 || TextUtils.isEmpty(split[1])) {
            return null;
        }
        return split[1];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        r3 = r0.split("!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r3.length != 2) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        r4 = r3[0];
        r3 = r3[1];
        r5 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r7 = r4.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        if (r7 == 83) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        if (r7 == 85) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        if (r4.equals("U") == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        r5 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0047, code lost:
        if (r4.equals("S") == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0049, code lost:
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004c, code lost:
        if (r5 == 0) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004e, code lost:
        if (r5 == 1) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r5 = r10.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005a, code lost:
        if (r10.a(r5) != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005c, code lost:
        r10.a(r10.d.a(com.google.firebase.iid.FirebaseInstanceId.g(), r5.a, r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006f, code lost:
        if (com.google.firebase.iid.FirebaseInstanceId.h() == false) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0071, code lost:
        android.util.Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007c, code lost:
        throw new java.io.IOException("token not available");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007d, code lost:
        r5 = r10.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0085, code lost:
        if (r10.a(r5) != false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0087, code lost:
        r10.a(r10.d.b(com.google.firebase.iid.FirebaseInstanceId.g(), r5.a, r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009a, code lost:
        if (com.google.firebase.iid.FirebaseInstanceId.h() == false) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009c, code lost:
        android.util.Log.d("FirebaseInstanceId", "subscribe operation succeeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a7, code lost:
        throw new java.io.IOException("token not available");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a8, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a9, code lost:
        r3 = java.lang.String.valueOf(r3.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b7, code lost:
        if (r3.length() != 0) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b9, code lost:
        r3 = "Topic sync failed: ".concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00be, code lost:
        r3 = new java.lang.String("Topic sync failed: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c3, code lost:
        android.util.Log.e("FirebaseInstanceId", r3);
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c8, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c9, code lost:
        if (r2 == false) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00cb, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00cc, code lost:
        monitor-enter(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        r2 = r9.b.remove(java.lang.Integer.valueOf(r9.a));
        a(r0);
        r9.a++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00e4, code lost:
        if (r2 != null) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e6, code lost:
        r2.a.a((java.lang.Object) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.google.firebase.iid.FirebaseInstanceId r10) {
        /*
            r9 = this;
        L_0x0000:
            monitor-enter(r9)
            java.lang.String r0 = r9.b()     // Catch:{ all -> 0x00f1 }
            r1 = 1
            if (r0 != 0) goto L_0x0017
            boolean r10 = com.google.firebase.iid.FirebaseInstanceId.h()     // Catch:{ all -> 0x00f1 }
            if (r10 == 0) goto L_0x0015
            java.lang.String r10 = "FirebaseInstanceId"
            java.lang.String r0 = "topic sync succeeded"
            android.util.Log.d(r10, r0)     // Catch:{ all -> 0x00f1 }
        L_0x0015:
            monitor-exit(r9)     // Catch:{ all -> 0x00f1 }
            return r1
        L_0x0017:
            monitor-exit(r9)     // Catch:{ all -> 0x00f1 }
            java.lang.String r2 = "FirebaseInstanceId"
            java.lang.String r3 = "!"
            java.lang.String[] r3 = r0.split(r3)
            int r4 = r3.length
            r5 = 2
            r6 = 0
            if (r4 != r5) goto L_0x00c8
            r4 = r3[r6]
            r3 = r3[r1]
            r5 = -1
            int r7 = r4.hashCode()     // Catch:{ IOException -> 0x00a8 }
            r8 = 83
            if (r7 == r8) goto L_0x0041
            r8 = 85
            if (r7 == r8) goto L_0x0037
            goto L_0x004a
        L_0x0037:
            java.lang.String r7 = "U"
            boolean r4 = r4.equals(r7)     // Catch:{ IOException -> 0x00a8 }
            if (r4 == 0) goto L_0x004a
            r5 = 1
            goto L_0x004a
        L_0x0041:
            java.lang.String r7 = "S"
            boolean r4 = r4.equals(r7)     // Catch:{ IOException -> 0x00a8 }
            if (r4 == 0) goto L_0x004a
            r5 = 0
        L_0x004a:
            java.lang.String r4 = "token not available"
            if (r5 == 0) goto L_0x007d
            if (r5 == r1) goto L_0x0052
            goto L_0x00c8
        L_0x0052:
            j.c.c.g.y r5 = r10.c()     // Catch:{ IOException -> 0x00a8 }
            boolean r7 = r10.a(r5)     // Catch:{ IOException -> 0x00a8 }
            if (r7 != 0) goto L_0x0077
            java.lang.String r4 = com.google.firebase.iid.FirebaseInstanceId.g()     // Catch:{ IOException -> 0x00a8 }
            j.c.c.g.b r7 = r10.d     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r5 = r5.a     // Catch:{ IOException -> 0x00a8 }
            j.c.a.a.i.e r3 = r7.a(r4, r5, r3)     // Catch:{ IOException -> 0x00a8 }
            r10.a(r3)     // Catch:{ IOException -> 0x00a8 }
            boolean r3 = com.google.firebase.iid.FirebaseInstanceId.h()     // Catch:{ IOException -> 0x00a8 }
            if (r3 == 0) goto L_0x00c8
            java.lang.String r3 = "unsubscribe operation succeeded"
            android.util.Log.d(r2, r3)     // Catch:{ IOException -> 0x00a8 }
            goto L_0x00c8
        L_0x0077:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ IOException -> 0x00a8 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00a8 }
            throw r3     // Catch:{ IOException -> 0x00a8 }
        L_0x007d:
            j.c.c.g.y r5 = r10.c()     // Catch:{ IOException -> 0x00a8 }
            boolean r7 = r10.a(r5)     // Catch:{ IOException -> 0x00a8 }
            if (r7 != 0) goto L_0x00a2
            java.lang.String r4 = com.google.firebase.iid.FirebaseInstanceId.g()     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r5 = r5.a     // Catch:{ IOException -> 0x00a8 }
            j.c.c.g.b r7 = r10.d     // Catch:{ IOException -> 0x00a8 }
            j.c.a.a.i.e r3 = r7.b(r4, r5, r3)     // Catch:{ IOException -> 0x00a8 }
            r10.a(r3)     // Catch:{ IOException -> 0x00a8 }
            boolean r3 = com.google.firebase.iid.FirebaseInstanceId.h()     // Catch:{ IOException -> 0x00a8 }
            if (r3 == 0) goto L_0x00c8
            java.lang.String r3 = "subscribe operation succeeded"
            android.util.Log.d(r2, r3)     // Catch:{ IOException -> 0x00a8 }
            goto L_0x00c8
        L_0x00a2:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ IOException -> 0x00a8 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00a8 }
            throw r3     // Catch:{ IOException -> 0x00a8 }
        L_0x00a8:
            r3 = move-exception
            java.lang.String r4 = "Topic sync failed: "
            java.lang.String r3 = r3.getMessage()
            java.lang.String r3 = java.lang.String.valueOf(r3)
            int r5 = r3.length()
            if (r5 == 0) goto L_0x00be
            java.lang.String r3 = r4.concat(r3)
            goto L_0x00c3
        L_0x00be:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r4)
        L_0x00c3:
            android.util.Log.e(r2, r3)
            r2 = 0
            goto L_0x00c9
        L_0x00c8:
            r2 = 1
        L_0x00c9:
            if (r2 != 0) goto L_0x00cc
            return r6
        L_0x00cc:
            monitor-enter(r9)
            java.util.Map<java.lang.Integer, j.c.a.a.i.f<java.lang.Void>> r2 = r9.b     // Catch:{ all -> 0x00ee }
            int r3 = r9.a     // Catch:{ all -> 0x00ee }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x00ee }
            java.lang.Object r2 = r2.remove(r3)     // Catch:{ all -> 0x00ee }
            j.c.a.a.i.f r2 = (j.c.a.a.i.f) r2     // Catch:{ all -> 0x00ee }
            r9.a(r0)     // Catch:{ all -> 0x00ee }
            int r0 = r9.a     // Catch:{ all -> 0x00ee }
            int r0 = r0 + r1
            r9.a = r0     // Catch:{ all -> 0x00ee }
            monitor-exit(r9)     // Catch:{ all -> 0x00ee }
            if (r2 == 0) goto L_0x0000
            r0 = 0
            j.c.a.a.i.y<TResult> r1 = r2.a
            r1.a(r0)
            goto L_0x0000
        L_0x00ee:
            r10 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00ee }
            throw r10
        L_0x00f1:
            r10 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00f1 }
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.g.z.a(com.google.firebase.iid.FirebaseInstanceId):boolean");
    }

    public final synchronized boolean a(String str) {
        synchronized (this.c) {
            String b2 = this.c.b();
            String valueOf = String.valueOf(str);
            if (!b2.startsWith(valueOf.length() != 0 ? ",".concat(valueOf) : new String(","))) {
                return false;
            }
            String valueOf2 = String.valueOf(str);
            this.c.a(b2.substring((valueOf2.length() != 0 ? ",".concat(valueOf2) : new String(",")).length()));
            return true;
        }
    }
}
