package j.c.c.e;

import com.google.firebase.components.DependencyCycleException;
import com.google.firebase.components.MissingDependencyException;
import i.b.k.ResourcesFlusher;
import j.c.c.f.c;
import j.c.c.f.d;
import j.c.c.h.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class j extends a {

    /* renamed from: e  reason: collision with root package name */
    public static final a<Set<Object>> f2519e = i.a;
    public final Map<d<?>, q<?>> a = new HashMap();
    public final Map<Class<?>, q<?>> b = new HashMap();
    public final Map<Class<?>, q<Set<?>>> c = new HashMap();
    public final p d;

    public j(Executor executor, Iterable<f> iterable, d<?>... dVarArr) {
        Set<l> set;
        this.d = new p(executor);
        ArrayList arrayList = new ArrayList();
        arrayList.add(d.a(this.d, p.class, d.class, c.class));
        for (f components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, dVarArr);
        HashMap hashMap = new HashMap(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            l lVar = new l(dVar);
            Iterator<Class<? super T>> it2 = dVar.a.iterator();
            while (true) {
                if (it2.hasNext()) {
                    Class next = it2.next();
                    m mVar = new m(next, !dVar.a(), null);
                    if (!hashMap.containsKey(mVar)) {
                        hashMap.put(mVar, new HashSet());
                    }
                    Set set2 = (Set) hashMap.get(mVar);
                    if (set2.isEmpty() || mVar.b) {
                        set2.add(lVar);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", next));
                    }
                }
            }
        }
        for (Set<l> it3 : hashMap.values()) {
            for (l lVar2 : it3) {
                for (n next2 : lVar2.a.b) {
                    if ((next2.c == 0) && (set = (Set) hashMap.get(new m(next2.a, next2.a(), null))) != null) {
                        for (l lVar3 : set) {
                            lVar2.b.add(lVar3);
                            lVar3.c.add(lVar2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set addAll : hashMap.values()) {
            hashSet.addAll(addAll);
        }
        HashSet hashSet2 = new HashSet();
        Iterator it4 = hashSet.iterator();
        while (it4.hasNext()) {
            l lVar4 = (l) it4.next();
            if (lVar4.a()) {
                hashSet2.add(lVar4);
            }
        }
        int i2 = 0;
        while (!hashSet2.isEmpty()) {
            l lVar5 = (l) hashSet2.iterator().next();
            hashSet2.remove(lVar5);
            i2++;
            for (l next3 : lVar5.b) {
                next3.c.remove(lVar5);
                if (next3.a()) {
                    hashSet2.add(next3);
                }
            }
        }
        if (i2 == arrayList.size()) {
            Iterator it5 = arrayList.iterator();
            while (it5.hasNext()) {
                d dVar2 = (d) it5.next();
                this.a.put(dVar2, new q(new g(this, dVar2)));
            }
            for (Map.Entry next4 : this.a.entrySet()) {
                d dVar3 = (d) next4.getKey();
                if (dVar3.a()) {
                    q qVar = (q) next4.getValue();
                    for (Class<? super T> put : dVar3.a) {
                        this.b.put(put, qVar);
                    }
                }
            }
            for (d next5 : this.a.keySet()) {
                Iterator<n> it6 = next5.b.iterator();
                while (true) {
                    if (it6.hasNext()) {
                        n next6 = it6.next();
                        if ((next6.b == 1) && !this.b.containsKey(next6.a)) {
                            throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", next5, next6.a));
                        }
                    }
                }
            }
            HashMap hashMap2 = new HashMap();
            for (Map.Entry next7 : this.a.entrySet()) {
                d dVar4 = (d) next7.getKey();
                if (!dVar4.a()) {
                    q qVar2 = (q) next7.getValue();
                    for (Class next8 : dVar4.a) {
                        if (!hashMap2.containsKey(next8)) {
                            hashMap2.put(next8, new HashSet());
                        }
                        ((Set) hashMap2.get(next8)).add(qVar2);
                    }
                }
            }
            for (Map.Entry entry : hashMap2.entrySet()) {
                this.c.put((Class) entry.getKey(), new q(new h((Set) entry.getValue())));
            }
            return;
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it7 = hashSet.iterator();
        while (it7.hasNext()) {
            l lVar6 = (l) it7.next();
            if (!lVar6.a() && !lVar6.b.isEmpty()) {
                arrayList2.add(lVar6.a);
            }
        }
        throw new DependencyCycleException(arrayList2);
    }

    public static /* synthetic */ Set a(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashSet.add(((q) it.next()).get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public <T> a<T> b(Class<T> cls) {
        ResourcesFlusher.b(cls, "Null interface requested.");
        return this.b.get(cls);
    }

    public <T> a<Set<T>> d(Class<T> cls) {
        q qVar = this.c.get(cls);
        if (qVar != null) {
            return qVar;
        }
        return f2519e;
    }
}
