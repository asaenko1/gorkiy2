package j.c.c.j;

import java.util.Iterator;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class c implements f {
    public final String a;
    public final d b;

    public c(Set<e> set, d dVar) {
        this.a = a(set);
        this.b = dVar;
    }

    public String a() {
        if (this.b.a().isEmpty()) {
            return this.a;
        }
        return this.a + ' ' + a(this.b.a());
    }

    public static String a(Set<e> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<e> it = set.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            sb.append(aVar.a);
            sb.append('/');
            sb.append(aVar.b);
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
}
