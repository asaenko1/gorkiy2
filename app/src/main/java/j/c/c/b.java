package j.c.c;

import android.content.Context;
import com.google.firebase.FirebaseApp;
import j.c.c.h.a;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public final /* synthetic */ class b implements a {
    public final FirebaseApp a;
    public final Context b;

    public b(FirebaseApp firebaseApp, Context context) {
        this.a = firebaseApp;
        this.b = context;
    }

    public Object get() {
        return FirebaseApp.a(this.a, this.b);
    }
}
