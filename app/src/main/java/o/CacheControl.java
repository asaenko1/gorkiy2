package o;

import java.util.concurrent.TimeUnit;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: CacheControl.kt */
public final class CacheControl {

    /* renamed from: n  reason: collision with root package name */
    public static final a f2812n = new a(null);
    public final boolean a;
    public final boolean b;
    public final int c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f2813e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f2814f;
    public final boolean g;
    public final int h;

    /* renamed from: i  reason: collision with root package name */
    public final int f2815i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f2816j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f2817k;

    /* renamed from: l  reason: collision with root package name */
    public final boolean f2818l;

    /* renamed from: m  reason: collision with root package name */
    public String f2819m;

    static {
        new CacheControl(true, false, -1, -1, false, false, false, -1, -1, false, false, false, null, null);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        if (timeUnit != null) {
            long j2 = (long) Integer.MAX_VALUE;
            long seconds = timeUnit.toSeconds(j2);
            new CacheControl(false, false, -1, -1, false, false, false, seconds > j2 ? Integer.MAX_VALUE : (int) seconds, -1, true, false, false, null, null);
            return;
        }
        Intrinsics.a("timeUnit");
        throw null;
    }

    public /* synthetic */ CacheControl(boolean z, boolean z2, int i2, int i3, boolean z3, boolean z4, boolean z5, int i4, int i5, boolean z6, boolean z7, boolean z8, String str, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = z;
        this.b = z2;
        this.c = i2;
        this.d = i3;
        this.f2813e = z3;
        this.f2814f = z4;
        this.g = z5;
        this.h = i4;
        this.f2815i = i5;
        this.f2816j = z6;
        this.f2817k = z7;
        this.f2818l = z8;
        this.f2819m = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        String str = this.f2819m;
        if (str != null) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        if (this.a) {
            sb.append("no-cache, ");
        }
        if (this.b) {
            sb.append("no-store, ");
        }
        if (this.c != -1) {
            sb.append("max-age=");
            sb.append(this.c);
            sb.append(", ");
        }
        if (this.d != -1) {
            sb.append("s-maxage=");
            sb.append(this.d);
            sb.append(", ");
        }
        if (this.f2813e) {
            sb.append("private, ");
        }
        if (this.f2814f) {
            sb.append("public, ");
        }
        if (this.g) {
            sb.append("must-revalidate, ");
        }
        if (this.h != -1) {
            sb.append("max-stale=");
            sb.append(this.h);
            sb.append(", ");
        }
        if (this.f2815i != -1) {
            sb.append("min-fresh=");
            sb.append(this.f2815i);
            sb.append(", ");
        }
        if (this.f2816j) {
            sb.append("only-if-cached, ");
        }
        if (this.f2817k) {
            sb.append("no-transform, ");
        }
        if (this.f2818l) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        this.f2819m = sb2;
        return sb2;
    }

    /* compiled from: CacheControl.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
         arg types: [java.lang.String, int, int, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
          n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
          n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00ca  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00ce  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final o.CacheControl a(o.Headers r27) {
            /*
                r26 = this;
                r0 = r26
                r1 = r27
                if (r1 == 0) goto L_0x0184
                int r3 = r27.size()
                r6 = 1
                r7 = 0
                r8 = 1
                r9 = 0
                r10 = 0
                r11 = 0
                r12 = -1
                r13 = -1
                r14 = 0
                r15 = 0
                r16 = 0
                r17 = -1
                r18 = -1
                r19 = 0
                r20 = 0
                r21 = 0
            L_0x0020:
                if (r7 >= r3) goto L_0x0173
                java.lang.String r2 = r1.c(r7)
                java.lang.String r4 = r1.d(r7)
                java.lang.String r5 = "Cache-Control"
                boolean r5 = n.r.Indent.a(r2, r5, r6)
                if (r5 == 0) goto L_0x0037
                if (r9 == 0) goto L_0x0035
                goto L_0x003f
            L_0x0035:
                r9 = r4
                goto L_0x0040
            L_0x0037:
                java.lang.String r5 = "Pragma"
                boolean r2 = n.r.Indent.a(r2, r5, r6)
                if (r2 == 0) goto L_0x0167
            L_0x003f:
                r8 = 0
            L_0x0040:
                r2 = 0
            L_0x0041:
                int r5 = r4.length()
                if (r2 >= r5) goto L_0x0162
                java.lang.String r5 = "=,;"
                int r5 = r0.a(r4, r5, r2)
                java.lang.String r2 = r4.substring(r2, r5)
                java.lang.String r6 = "(this as java.lang.Strin…ing(startIndex, endIndex)"
                n.n.c.Intrinsics.a(r2, r6)
                java.lang.CharSequence r2 = n.r.Indent.c(r2)
                java.lang.String r2 = r2.toString()
                int r1 = r4.length()
                if (r5 == r1) goto L_0x00b8
                char r1 = r4.charAt(r5)
                r24 = r3
                r3 = 44
                if (r1 == r3) goto L_0x00ba
                char r1 = r4.charAt(r5)
                r3 = 59
                if (r1 != r3) goto L_0x0077
                goto L_0x00ba
            L_0x0077:
                int r5 = r5 + 1
                int r1 = o.m0.Util.a(r4, r5)
                int r3 = r4.length()
                if (r1 >= r3) goto L_0x009f
                char r3 = r4.charAt(r1)
                r5 = 34
                if (r3 != r5) goto L_0x009f
                int r1 = r1 + 1
                r3 = 4
                r25 = r9
                r9 = 0
                int r3 = n.r.Indent.a(r4, r5, r1, r9, r3)
                java.lang.String r1 = r4.substring(r1, r3)
                n.n.c.Intrinsics.a(r1, r6)
                r5 = 1
                int r3 = r3 + r5
                goto L_0x00c1
            L_0x009f:
                r25 = r9
                r9 = 0
                java.lang.String r3 = ",;"
                int r3 = r0.a(r4, r3, r1)
                java.lang.String r1 = r4.substring(r1, r3)
                n.n.c.Intrinsics.a(r1, r6)
                java.lang.CharSequence r1 = n.r.Indent.c(r1)
                java.lang.String r1 = r1.toString()
                goto L_0x00c1
            L_0x00b8:
                r24 = r3
            L_0x00ba:
                r25 = r9
                r9 = 0
                int r5 = r5 + 1
                r3 = r5
                r1 = 0
            L_0x00c1:
                java.lang.String r5 = "no-cache"
                r6 = 1
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x00ce
                r5 = -1
                r10 = 1
                goto L_0x0159
            L_0x00ce:
                java.lang.String r5 = "no-store"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x00da
                r5 = -1
                r11 = 1
                goto L_0x0159
            L_0x00da:
                java.lang.String r5 = "max-age"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x00e9
                r5 = -1
                int r12 = o.m0.Util.b(r1, r5)
                goto L_0x0159
            L_0x00e9:
                r5 = -1
                java.lang.String r9 = "s-maxage"
                boolean r9 = n.r.Indent.a(r9, r2, r6)
                if (r9 == 0) goto L_0x00f7
                int r13 = o.m0.Util.b(r1, r5)
                goto L_0x0159
            L_0x00f7:
                java.lang.String r5 = "private"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x0102
                r5 = -1
                r14 = 1
                goto L_0x0159
            L_0x0102:
                java.lang.String r5 = "public"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x010d
                r5 = -1
                r15 = 1
                goto L_0x0159
            L_0x010d:
                java.lang.String r5 = "must-revalidate"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x0119
                r5 = -1
                r16 = 1
                goto L_0x0159
            L_0x0119:
                java.lang.String r5 = "max-stale"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x012a
                r2 = 2147483647(0x7fffffff, float:NaN)
                int r17 = o.m0.Util.b(r1, r2)
                r5 = -1
                goto L_0x0159
            L_0x012a:
                java.lang.String r5 = "min-fresh"
                boolean r5 = n.r.Indent.a(r5, r2, r6)
                if (r5 == 0) goto L_0x0138
                r5 = -1
                int r18 = o.m0.Util.b(r1, r5)
                goto L_0x0159
            L_0x0138:
                r5 = -1
                java.lang.String r1 = "only-if-cached"
                boolean r1 = n.r.Indent.a(r1, r2, r6)
                if (r1 == 0) goto L_0x0144
                r19 = 1
                goto L_0x0159
            L_0x0144:
                java.lang.String r1 = "no-transform"
                boolean r1 = n.r.Indent.a(r1, r2, r6)
                if (r1 == 0) goto L_0x014f
                r20 = 1
                goto L_0x0159
            L_0x014f:
                java.lang.String r1 = "immutable"
                boolean r1 = n.r.Indent.a(r1, r2, r6)
                if (r1 == 0) goto L_0x0159
                r21 = 1
            L_0x0159:
                r1 = r27
                r2 = r3
                r3 = r24
                r9 = r25
                goto L_0x0041
            L_0x0162:
                r24 = r3
                r25 = r9
                goto L_0x016a
            L_0x0167:
                r24 = r3
                r2 = r9
            L_0x016a:
                r5 = -1
                int r7 = r7 + 1
                r1 = r27
                r3 = r24
                goto L_0x0020
            L_0x0173:
                r2 = r9
                if (r8 != 0) goto L_0x0179
                r22 = 0
                goto L_0x017b
            L_0x0179:
                r22 = r2
            L_0x017b:
                o.CacheControl r1 = new o.CacheControl
                r23 = 0
                r9 = r1
                r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)
                return r1
            L_0x0184:
                java.lang.String r1 = "headers"
                n.n.c.Intrinsics.a(r1)
                r1 = 0
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: o.CacheControl.a.a(o.Headers):o.CacheControl");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
         arg types: [java.lang.String, char, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean */
        public final int a(String str, String str2, int i2) {
            int length = str.length();
            while (i2 < length) {
                if (Indent.a((CharSequence) str2, str.charAt(i2), false, 2)) {
                    return i2;
                }
                i2++;
            }
            return str.length();
        }
    }
}
