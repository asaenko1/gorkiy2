package o;

import com.crashlytics.android.answers.SessionEventTransform;
import j.a.a.a.outline;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import n.Lazy;
import n.i.Collections;
import n.i.Collections2;
import n.n.b.Functions;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.j;
import n.p.KProperty;
import o.m0.Util;

/* compiled from: Handshake.kt */
public final class Handshake {

    /* renamed from: e  reason: collision with root package name */
    public static final /* synthetic */ KProperty[] f2847e;

    /* renamed from: f  reason: collision with root package name */
    public static final a f2848f = new a(null);
    public final Lazy a;
    public final TlsVersion b;
    public final CipherSuite c;
    public final List<Certificate> d;

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(Handshake.class), "peerCertificates", "peerCertificates()Ljava/util/List;");
        Reflection.a(propertyReference1Impl);
        f2847e = new KProperty[]{propertyReference1Impl};
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.List<java.security.cert.Certificate>, java.util.List<? extends java.security.cert.Certificate>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Handshake(o.l0 r2, o.j r3, java.util.List<? extends java.security.cert.Certificate> r4, n.n.b.Functions<? extends java.util.List<? extends java.security.cert.Certificate>> r5) {
        /*
            r1 = this;
            r0 = 0
            if (r2 == 0) goto L_0x002b
            if (r3 == 0) goto L_0x0025
            if (r4 == 0) goto L_0x001f
            if (r5 == 0) goto L_0x0019
            r1.<init>()
            r1.b = r2
            r1.c = r3
            r1.d = r4
            n.Lazy r2 = j.c.a.a.c.n.c.a(r5)
            r1.a = r2
            return
        L_0x0019:
            java.lang.String r2 = "peerCertificatesFn"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x001f:
            java.lang.String r2 = "localCertificates"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x0025:
            java.lang.String r2 = "cipherSuite"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x002b:
            java.lang.String r2 = "tlsVersion"
            n.n.c.Intrinsics.a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.Handshake.<init>(o.TlsVersion, o.CipherSuite, java.util.List, n.n.b.Functions):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return ((X509Certificate) certificate).getSubjectDN().toString();
        }
        String type = certificate.getType();
        Intrinsics.a((Object) type, SessionEventTransform.TYPE_KEY);
        return type;
    }

    public final List<Certificate> a() {
        Lazy lazy = this.a;
        KProperty kProperty = f2847e[0];
        return (List) lazy.getValue();
    }

    public boolean equals(Object obj) {
        if (obj instanceof Handshake) {
            Handshake handshake = (Handshake) obj;
            return handshake.b == this.b && Intrinsics.a(handshake.c, this.c) && Intrinsics.a(handshake.a(), a()) && Intrinsics.a(handshake.d, this.d);
        }
    }

    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = a().hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + ((this.b.hashCode() + 527) * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder b2 = outline.b("Handshake{", "tlsVersion=");
        b2.append(this.b);
        b2.append(' ');
        b2.append("cipherSuite=");
        b2.append(this.c);
        b2.append(' ');
        b2.append("peerCertificates=");
        List<Certificate> a2 = a();
        ArrayList arrayList = new ArrayList(Collections.a(a2, 10));
        for (Certificate a3 : a2) {
            arrayList.add(a(a3));
        }
        b2.append(arrayList);
        b2.append(' ');
        b2.append("localCertificates=");
        List<Certificate> list = this.d;
        ArrayList arrayList2 = new ArrayList(Collections.a(list, 10));
        for (Certificate a4 : list) {
            arrayList2.add(a(a4));
        }
        b2.append(arrayList2);
        b2.append('}');
        return b2.toString();
    }

    /* compiled from: Handshake.kt */
    public static final class a {

        /* renamed from: o.Handshake$a$a  reason: collision with other inner class name */
        /* compiled from: Handshake.kt */
        public static final class C0040a extends j implements Functions<List<? extends Certificate>> {
            public final /* synthetic */ List c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0040a(List list) {
                super(0);
                this.c = list;
            }

            public Object b() {
                return this.c;
            }
        }

        /* compiled from: Handshake.kt */
        public static final class b extends j implements Functions<List<? extends Certificate>> {
            public final /* synthetic */ List c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(List list) {
                super(0);
                this.c = list;
            }

            public Object b() {
                return this.c;
            }
        }

        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public final Handshake a(SSLSession sSLSession) {
            List list;
            List list2;
            if (sSLSession != null) {
                String cipherSuite = sSLSession.getCipherSuite();
                if (cipherSuite == null) {
                    throw new IllegalStateException("cipherSuite == null".toString());
                } else if (!Intrinsics.a((Object) "SSL_NULL_WITH_NULL_NULL", (Object) cipherSuite)) {
                    CipherSuite a = CipherSuite.f2834t.a(cipherSuite);
                    String protocol = sSLSession.getProtocol();
                    if (protocol == null) {
                        throw new IllegalStateException("tlsVersion == null".toString());
                    } else if (!Intrinsics.a((Object) "NONE", (Object) protocol)) {
                        TlsVersion a2 = TlsVersion.Companion.a(protocol);
                        try {
                            Certificate[] peerCertificates = sSLSession.getPeerCertificates();
                            if (peerCertificates != null) {
                                list = Util.a((Certificate[]) Arrays.copyOf(peerCertificates, peerCertificates.length));
                            } else {
                                list = Collections2.b;
                            }
                        } catch (SSLPeerUnverifiedException unused) {
                            list = Collections2.b;
                        }
                        Certificate[] localCertificates = sSLSession.getLocalCertificates();
                        if (localCertificates != null) {
                            list2 = Util.a((Certificate[]) Arrays.copyOf(localCertificates, localCertificates.length));
                        } else {
                            list2 = Collections2.b;
                        }
                        return new Handshake(a2, a, list2, new b(list));
                    } else {
                        throw new IOException("tlsVersion == NONE");
                    }
                } else {
                    throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
                }
            } else {
                Intrinsics.a("$this$handshake");
                throw null;
            }
        }

        public final v a(l0 l0Var, j jVar, List<? extends Certificate> list, List<? extends Certificate> list2) {
            if (l0Var == null) {
                Intrinsics.a("tlsVersion");
                throw null;
            } else if (jVar == null) {
                Intrinsics.a("cipherSuite");
                throw null;
            } else if (list == null) {
                Intrinsics.a("peerCertificates");
                throw null;
            } else if (list2 != null) {
                return new Handshake(l0Var, jVar, Util.b(list2), new C0040a(Util.b(list)));
            } else {
                Intrinsics.a("localCertificates");
                throw null;
            }
        }
    }
}
