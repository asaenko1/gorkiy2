package o;

import java.util.concurrent.atomic.AtomicInteger;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.m0.d.Transmitter;

/* compiled from: RealCall.kt */
public final class RealCall implements Call {
    public Transmitter b;
    public boolean c;
    public final OkHttpClient d;

    /* renamed from: e  reason: collision with root package name */
    public final Request f2894e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f2895f;

    /* compiled from: RealCall.kt */
    public final class a implements Runnable {
        public volatile AtomicInteger b;
        public final Callback c;
        public final /* synthetic */ RealCall d;

        public a(RealCall realCall, g gVar) {
            if (gVar != null) {
                this.d = realCall;
                this.c = gVar;
                this.b = new AtomicInteger(0);
                return;
            }
            Intrinsics.a("responseCallback");
            throw null;
        }

        public final String a() {
            return this.d.f2894e.b.f2851e;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Thread, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0050 A[SYNTHETIC, Splitter:B:21:0x0050] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0070 A[Catch:{ all -> 0x004a, all -> 0x00a2 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r7 = this;
                java.lang.String r0 = "OkHttp "
                java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
                o.RealCall r1 = r7.d
                o.Request r1 = r1.f2894e
                o.HttpUrl r1 = r1.b
                java.lang.String r1 = r1.f()
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                java.lang.Thread r1 = java.lang.Thread.currentThread()
                java.lang.String r2 = "currentThread"
                n.n.c.Intrinsics.a(r1, r2)
                java.lang.String r2 = r1.getName()
                r1.setName(r0)
                o.RealCall r0 = r7.d     // Catch:{ all -> 0x00a2 }
                o.m0.d.Transmitter r0 = r0.b     // Catch:{ all -> 0x00a2 }
                r3 = 0
                if (r0 == 0) goto L_0x009c
                o.m0.d.Transmitter$b r0 = r0.c     // Catch:{ all -> 0x00a2 }
                r0.f()     // Catch:{ all -> 0x00a2 }
                o.RealCall r0 = r7.d     // Catch:{ IOException -> 0x004c }
                o.Response r0 = r0.b()     // Catch:{ IOException -> 0x004c }
                r4 = 1
                o.Callback r5 = r7.c     // Catch:{ IOException -> 0x0048 }
                o.RealCall r6 = r7.d     // Catch:{ IOException -> 0x0048 }
                r.OkHttpCall$a r5 = (r.OkHttpCall.a) r5
                r5.a(r6, r0)     // Catch:{ IOException -> 0x0048 }
                o.RealCall r0 = r7.d     // Catch:{ all -> 0x00a2 }
            L_0x0045:
                o.OkHttpClient r0 = r0.d     // Catch:{ all -> 0x00a2 }
                goto L_0x0088
            L_0x0048:
                r0 = move-exception
                goto L_0x004e
            L_0x004a:
                r0 = move-exception
                goto L_0x0092
            L_0x004c:
                r0 = move-exception
                r4 = 0
            L_0x004e:
                if (r4 == 0) goto L_0x0070
                o.m0.i.Platform$a r3 = o.m0.i.Platform.c     // Catch:{ all -> 0x004a }
                o.m0.i.Platform r3 = o.m0.i.Platform.a     // Catch:{ all -> 0x004a }
                r4 = 4
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x004a }
                r5.<init>()     // Catch:{ all -> 0x004a }
                java.lang.String r6 = "Callback failure for "
                r5.append(r6)     // Catch:{ all -> 0x004a }
                o.RealCall r6 = r7.d     // Catch:{ all -> 0x004a }
                java.lang.String r6 = r6.c()     // Catch:{ all -> 0x004a }
                r5.append(r6)     // Catch:{ all -> 0x004a }
                java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x004a }
                r3.a(r4, r5, r0)     // Catch:{ all -> 0x004a }
                goto L_0x0085
            L_0x0070:
                o.Callback r4 = r7.c     // Catch:{ all -> 0x004a }
                r.OkHttpCall$a r4 = (r.OkHttpCall.a) r4     // Catch:{ all -> 0x004a }
                if (r4 == 0) goto L_0x0091
                r.Callback r3 = r4.a     // Catch:{ all -> 0x007e }
                r.OkHttpCall r4 = r.OkHttpCall.this     // Catch:{ all -> 0x007e }
                r3.a(r4, r0)     // Catch:{ all -> 0x007e }
                goto L_0x0085
            L_0x007e:
                r0 = move-exception
                r.Utils.a(r0)     // Catch:{ all -> 0x004a }
                r0.printStackTrace()     // Catch:{ all -> 0x004a }
            L_0x0085:
                o.RealCall r0 = r7.d     // Catch:{ all -> 0x00a2 }
                goto L_0x0045
            L_0x0088:
                o.Dispatcher r0 = r0.b     // Catch:{ all -> 0x00a2 }
                r0.b(r7)     // Catch:{ all -> 0x00a2 }
                r1.setName(r2)
                return
            L_0x0091:
                throw r3     // Catch:{ all -> 0x004a }
            L_0x0092:
                o.RealCall r3 = r7.d     // Catch:{ all -> 0x00a2 }
                o.OkHttpClient r3 = r3.d     // Catch:{ all -> 0x00a2 }
                o.Dispatcher r3 = r3.b     // Catch:{ all -> 0x00a2 }
                r3.b(r7)     // Catch:{ all -> 0x00a2 }
                throw r0     // Catch:{ all -> 0x00a2 }
            L_0x009c:
                java.lang.String r0 = "transmitter"
                n.n.c.Intrinsics.b(r0)     // Catch:{ all -> 0x00a2 }
                throw r3
            L_0x00a2:
                r0 = move-exception
                r1.setName(r2)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: o.RealCall.a.run():void");
        }
    }

    public /* synthetic */ RealCall(OkHttpClient okHttpClient, Request request, boolean z, DefaultConstructorMarker defaultConstructorMarker) {
        this.d = okHttpClient;
        this.f2894e = request;
        this.f2895f = z;
    }

    public void a(Callback callback) {
        if (callback != null) {
            synchronized (this) {
                if (!this.c) {
                    this.c = true;
                } else {
                    throw new IllegalStateException("Already Executed".toString());
                }
            }
            Transmitter transmitter = this.b;
            if (transmitter != null) {
                transmitter.a();
                this.d.b.a(new a(this, callback));
                return;
            }
            Intrinsics.b("transmitter");
            throw null;
        }
        Intrinsics.a("responseCallback");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.List<o.y>]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final o.Response b() {
        /*
            r13 = this;
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            o.OkHttpClient r0 = r13.d
            java.util.List<o.y> r0 = r0.d
            n.i.Collections.a(r1, r0)
            o.m0.e.RetryAndFollowUpInterceptor r0 = new o.m0.e.RetryAndFollowUpInterceptor
            o.OkHttpClient r2 = r13.d
            r0.<init>(r2)
            r1.add(r0)
            o.m0.e.BridgeInterceptor r0 = new o.m0.e.BridgeInterceptor
            o.OkHttpClient r2 = r13.d
            o.CookieJar r2 = r2.f2870k
            r0.<init>(r2)
            r1.add(r0)
            o.m0.c.CacheInterceptor r0 = new o.m0.c.CacheInterceptor
            o.OkHttpClient r2 = r13.d
            o.Cache r2 = r2.f2871l
            r0.<init>(r2)
            r1.add(r0)
            o.m0.d.ConnectInterceptor r0 = o.m0.d.ConnectInterceptor.b
            r1.add(r0)
            boolean r0 = r13.f2895f
            if (r0 != 0) goto L_0x003e
            o.OkHttpClient r0 = r13.d
            java.util.List<o.y> r0 = r0.f2866e
            n.i.Collections.a(r1, r0)
        L_0x003e:
            o.m0.e.CallServerInterceptor r0 = new o.m0.e.CallServerInterceptor
            boolean r2 = r13.f2895f
            r0.<init>(r2)
            r1.add(r0)
            o.m0.e.RealInterceptorChain r10 = new o.m0.e.RealInterceptorChain
            o.m0.d.Transmitter r2 = r13.b
            java.lang.String r11 = "transmitter"
            r12 = 0
            if (r2 == 0) goto L_0x00b9
            r3 = 0
            r4 = 0
            o.Request r5 = r13.f2894e
            o.OkHttpClient r0 = r13.d
            int r7 = r0.z
            int r8 = r0.A
            int r9 = r0.B
            r0 = r10
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            r0 = 0
            o.Request r1 = r13.f2894e     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            o.Response r1 = r10.a(r1)     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            o.m0.d.Transmitter r2 = r13.b     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            if (r2 == 0) goto L_0x008a
            boolean r2 = r2.e()     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            if (r2 != 0) goto L_0x007f
            o.m0.d.Transmitter r0 = r13.b
            if (r0 == 0) goto L_0x007b
            r0.a(r12)
            return r1
        L_0x007b:
            n.n.c.Intrinsics.b(r11)
            throw r12
        L_0x007f:
            o.m0.Util.a(r1)     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            java.io.IOException r1 = new java.io.IOException     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            java.lang.String r2 = "Canceled"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            throw r1     // Catch:{ IOException -> 0x0090, all -> 0x008e }
        L_0x008a:
            n.n.c.Intrinsics.b(r11)     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            throw r12
        L_0x008e:
            r1 = move-exception
            goto L_0x00ab
        L_0x0090:
            r0 = move-exception
            o.m0.d.Transmitter r1 = r13.b     // Catch:{ all -> 0x00a8 }
            if (r1 == 0) goto L_0x00a4
            java.io.IOException r0 = r1.a(r0)     // Catch:{ all -> 0x00a8 }
            if (r0 != 0) goto L_0x00a3
            kotlin.TypeCastException r0 = new kotlin.TypeCastException     // Catch:{ all -> 0x00a8 }
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Throwable"
            r0.<init>(r1)     // Catch:{ all -> 0x00a8 }
            throw r0     // Catch:{ all -> 0x00a8 }
        L_0x00a3:
            throw r0     // Catch:{ all -> 0x00a8 }
        L_0x00a4:
            n.n.c.Intrinsics.b(r11)     // Catch:{ all -> 0x00a8 }
            throw r12
        L_0x00a8:
            r0 = move-exception
            r1 = r0
            r0 = 1
        L_0x00ab:
            if (r0 != 0) goto L_0x00b8
            o.m0.d.Transmitter r0 = r13.b
            if (r0 != 0) goto L_0x00b5
            n.n.c.Intrinsics.b(r11)
            throw r12
        L_0x00b5:
            r0.a(r12)
        L_0x00b8:
            throw r1
        L_0x00b9:
            n.n.c.Intrinsics.b(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: o.RealCall.b():o.Response");
    }

    public final String c() {
        StringBuilder sb = new StringBuilder();
        Transmitter transmitter = this.b;
        if (transmitter != null) {
            sb.append(transmitter.e() ? "canceled " : "");
            sb.append(this.f2895f ? "web socket" : "call");
            sb.append(" to ");
            sb.append(this.f2894e.b.f());
            return sb.toString();
        }
        Intrinsics.b("transmitter");
        throw null;
    }

    public void cancel() {
        Transmitter transmitter = this.b;
        if (transmitter != null) {
            transmitter.b();
        } else {
            Intrinsics.b("transmitter");
            throw null;
        }
    }

    public Object clone() {
        return a(this.d, this.f2894e, this.f2895f);
    }

    public Request f() {
        return this.f2894e;
    }

    public Response g() {
        synchronized (this) {
            if (!this.c) {
                this.c = true;
            } else {
                throw new IllegalStateException("Already Executed".toString());
            }
        }
        Transmitter transmitter = this.b;
        if (transmitter != null) {
            transmitter.c.f();
            Transmitter transmitter2 = this.b;
            if (transmitter2 != null) {
                transmitter2.a();
                try {
                    this.d.b.a(this);
                    return b();
                } finally {
                    Dispatcher dispatcher = this.d.b;
                    dispatcher.a(dispatcher.f2846f, this);
                }
            } else {
                Intrinsics.b("transmitter");
                throw null;
            }
        } else {
            Intrinsics.b("transmitter");
            throw null;
        }
    }

    public boolean h() {
        Transmitter transmitter = this.b;
        if (transmitter != null) {
            return transmitter.e();
        }
        Intrinsics.b("transmitter");
        throw null;
    }

    public static final RealCall a(OkHttpClient okHttpClient, Request request, boolean z) {
        if (okHttpClient == null) {
            Intrinsics.a("client");
            throw null;
        } else if (request != null) {
            RealCall realCall = new RealCall(okHttpClient, request, z, null);
            realCall.b = new Transmitter(okHttpClient, realCall);
            return realCall;
        } else {
            Intrinsics.a("originalRequest");
            throw null;
        }
    }
}
