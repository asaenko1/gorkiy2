package o;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import n.n.c.Intrinsics;

/* compiled from: EventListener.kt */
public abstract class EventListener {
    public static final EventListener a = new a();

    /* compiled from: EventListener.kt */
    public static final class a extends EventListener {
    }

    /* compiled from: EventListener.kt */
    public interface b {
        EventListener a(Call call);
    }

    public void a(Call call, InetSocketAddress inetSocketAddress, Proxy proxy) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (inetSocketAddress == null) {
            Intrinsics.a("inetSocketAddress");
            throw null;
        } else if (proxy == null) {
            Intrinsics.a("proxy");
            throw null;
        }
    }

    public void b(Call call, IOException iOException) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (iOException == null) {
            Intrinsics.a("ioe");
            throw null;
        }
    }

    public void a(Call call, Connection connection) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (connection == null) {
            Intrinsics.a("connection");
            throw null;
        }
    }

    public void a(Call call, IOException iOException) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (iOException == null) {
            Intrinsics.a("ioe");
            throw null;
        }
    }
}
