package o;

import java.nio.charset.Charset;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Charsets;
import o.MediaType;
import o.m0.Util;
import p.BufferedSink;
import p.ByteString;

/* compiled from: RequestBody.kt */
public abstract class RequestBody {
    public static final a a = new a(null);

    public static final RequestBody a(MediaType mediaType, ByteString byteString) {
        if (byteString != null) {
            return new RequestBody0(byteString, mediaType);
        }
        Intrinsics.a("content");
        throw null;
    }

    public long a() {
        return -1;
    }

    public abstract void a(BufferedSink bufferedSink);

    public abstract MediaType b();

    /* compiled from: RequestBody.kt */
    public static final class a {

        /* renamed from: o.RequestBody$a$a  reason: collision with other inner class name */
        /* compiled from: RequestBody.kt */
        public static final class C0042a extends RequestBody {
            public final /* synthetic */ byte[] b;
            public final /* synthetic */ MediaType c;
            public final /* synthetic */ int d;

            /* renamed from: e  reason: collision with root package name */
            public final /* synthetic */ int f2899e;

            public C0042a(byte[] bArr, MediaType mediaType, int i2, int i3) {
                this.b = bArr;
                this.c = mediaType;
                this.d = i2;
                this.f2899e = i3;
            }

            public long a() {
                return (long) this.d;
            }

            public MediaType b() {
                return this.c;
            }

            public void a(BufferedSink bufferedSink) {
                if (bufferedSink != null) {
                    bufferedSink.write(this.b, this.f2899e, this.d);
                } else {
                    Intrinsics.a("sink");
                    throw null;
                }
            }
        }

        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [byte[], java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final RequestBody a(String str, MediaType mediaType) {
            if (str != null) {
                Charset charset = Charsets.a;
                if (mediaType != null && (charset = MediaType.a(mediaType, null, 1)) == null) {
                    charset = Charsets.a;
                    MediaType.a aVar = MediaType.f2859f;
                    mediaType = MediaType.a.b(mediaType + "; charset=utf-8");
                }
                byte[] bytes = str.getBytes(charset);
                Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                return a(bytes, mediaType, 0, bytes.length);
            }
            Intrinsics.a("$this$toRequestBody");
            throw null;
        }

        public static /* synthetic */ RequestBody a(a aVar, byte[] bArr, MediaType mediaType, int i2, int i3, int i4) {
            if ((i4 & 1) != 0) {
                mediaType = null;
            }
            if ((i4 & 2) != 0) {
                i2 = 0;
            }
            if ((i4 & 4) != 0) {
                i3 = bArr.length;
            }
            return aVar.a(bArr, mediaType, i2, i3);
        }

        public final RequestBody a(byte[] bArr, MediaType mediaType, int i2, int i3) {
            if (bArr != null) {
                Util.a((long) bArr.length, (long) i2, (long) i3);
                return new C0042a(bArr, mediaType, i3, i2);
            }
            Intrinsics.a("$this$toRequestBody");
            throw null;
        }
    }
}
