package o;

import n.n.c.Intrinsics;
import p.BufferedSink;
import p.ByteString;

/* compiled from: RequestBody.kt */
public final class RequestBody0 extends RequestBody {
    public final /* synthetic */ ByteString b;
    public final /* synthetic */ MediaType c;

    public RequestBody0(ByteString byteString, MediaType mediaType) {
        this.b = byteString;
        this.c = mediaType;
    }

    public long a() {
        return (long) this.b.g();
    }

    public MediaType b() {
        return this.c;
    }

    public void a(BufferedSink bufferedSink) {
        if (bufferedSink != null) {
            bufferedSink.a(this.b);
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }
}
