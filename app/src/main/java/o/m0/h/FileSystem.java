package o.m0.h;

import java.io.File;
import p.Sink;
import p.Source;

/* compiled from: FileSystem.kt */
public interface FileSystem {
    public static final FileSystem a = new a$a();

    void a(File file);

    void a(File file, File file2);

    Source b(File file);

    Sink c(File file);

    void d(File file);

    Sink e(File file);

    boolean f(File file);

    long g(File file);
}
