package o.m0.i;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import o.c0;

/* compiled from: Jdk8WithJettyBootPlatform.kt */
public final class Jdk8WithJettyBootPlatform extends Platform {
    public final Method d;

    /* renamed from: e  reason: collision with root package name */
    public final Method f3048e;

    /* renamed from: f  reason: collision with root package name */
    public final Method f3049f;
    public final Class<?> g;
    public final Class<?> h;

    /* compiled from: Jdk8WithJettyBootPlatform.kt */
    public static final class a implements InvocationHandler {
        public boolean a;
        public String b;
        public final List<String> c;

        public a(List<String> list) {
            if (list != null) {
                this.c = list;
            } else {
                Intrinsics.a("protocols");
                throw null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object invoke(Object obj, Method method, Object[] objArr) {
            if (obj == null) {
                Intrinsics.a("proxy");
                throw null;
            } else if (method != null) {
                if (objArr == null) {
                    objArr = new Object[0];
                }
                String name = method.getName();
                Class<?> returnType = method.getReturnType();
                if (Intrinsics.a((Object) name, (Object) "supports") && Intrinsics.a(Boolean.TYPE, returnType)) {
                    return true;
                }
                if (!Intrinsics.a((Object) name, (Object) "unsupported") || !Intrinsics.a(Void.TYPE, returnType)) {
                    if (Intrinsics.a((Object) name, (Object) "protocols")) {
                        if (objArr.length == 0) {
                            return this.c;
                        }
                    }
                    if ((Intrinsics.a((Object) name, (Object) "selectProtocol") || Intrinsics.a((Object) name, (Object) "select")) && Intrinsics.a(String.class, returnType) && objArr.length == 1 && (objArr[0] instanceof List)) {
                        Object obj2 = objArr[0];
                        if (obj2 != null) {
                            List list = (List) obj2;
                            int size = list.size();
                            if (size >= 0) {
                                int i2 = 0;
                                while (true) {
                                    Object obj3 = list.get(i2);
                                    if (obj3 != null) {
                                        String str = (String) obj3;
                                        if (!this.c.contains(str)) {
                                            if (i2 == size) {
                                                break;
                                            }
                                            i2++;
                                        } else {
                                            this.b = str;
                                            return str;
                                        }
                                    } else {
                                        throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                                    }
                                }
                            }
                            String str2 = this.c.get(0);
                            this.b = str2;
                            return str2;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<*>");
                    } else if ((!Intrinsics.a((Object) name, (Object) "protocolSelected") && !Intrinsics.a((Object) name, (Object) "selected")) || objArr.length != 1) {
                        return method.invoke(this, Arrays.copyOf(objArr, objArr.length));
                    } else {
                        Object obj4 = objArr[0];
                        if (obj4 != null) {
                            this.b = (String) obj4;
                            return null;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                    }
                } else {
                    this.a = true;
                    return null;
                }
            } else {
                Intrinsics.a("method");
                throw null;
            }
        }
    }

    public Jdk8WithJettyBootPlatform(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        if (method == null) {
            Intrinsics.a("putMethod");
            throw null;
        } else if (method2 == null) {
            Intrinsics.a("getMethod");
            throw null;
        } else if (method3 == null) {
            Intrinsics.a("removeMethod");
            throw null;
        } else if (cls == null) {
            Intrinsics.a("clientProviderClass");
            throw null;
        } else if (cls2 != null) {
            this.d = method;
            this.f3048e = method2;
            this.f3049f = method3;
            this.g = cls;
            this.h = cls2;
        } else {
            Intrinsics.a("serverProviderClass");
            throw null;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list != null) {
            List<String> a2 = Platform.c.a(list);
            try {
                Object newProxyInstance = Proxy.newProxyInstance(Platform.class.getClassLoader(), new Class[]{this.g, this.h}, new a(a2));
                this.d.invoke(null, sSLSocket, newProxyInstance);
            } catch (InvocationTargetException e2) {
                throw new AssertionError("failed to set ALPN", e2);
            } catch (IllegalAccessException e3) {
                throw new AssertionError("failed to set ALPN", e3);
            }
        } else {
            Intrinsics.a("protocols");
            throw null;
        }
    }

    public String b(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            try {
                InvocationHandler invocationHandler = Proxy.getInvocationHandler(this.f3048e.invoke(null, sSLSocket));
                if (invocationHandler != null) {
                    a aVar = (a) invocationHandler;
                    if (!aVar.a && aVar.b == null) {
                        Platform.a.a(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", (Throwable) null);
                        return null;
                    } else if (aVar.a) {
                        return null;
                    } else {
                        return aVar.b;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type okhttp3.internal.platform.Jdk8WithJettyBootPlatform.AlpnProvider");
                }
            } catch (InvocationTargetException e2) {
                throw new AssertionError("failed to get ALPN selected protocol", e2);
            } catch (IllegalAccessException e3) {
                throw new AssertionError("failed to get ALPN selected protocol", e3);
            }
        } else {
            Intrinsics.a("socket");
            throw null;
        }
    }

    public void a(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            try {
                this.f3049f.invoke(null, sSLSocket);
            } catch (IllegalAccessException e2) {
                throw new AssertionError("failed to remove ALPN", e2);
            } catch (InvocationTargetException e3) {
                throw new AssertionError("failed to remove ALPN", e3);
            }
        } else {
            Intrinsics.a("sslSocket");
            throw null;
        }
    }
}
