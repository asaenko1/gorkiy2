package o.m0.i.g;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: StandardAndroidSocketAdapter.kt */
public final class StandardAndroidSocketAdapter extends AndroidSocketAdapter {

    /* renamed from: f  reason: collision with root package name */
    public static final a f3054f = new a(null);

    /* compiled from: StandardAndroidSocketAdapter.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StandardAndroidSocketAdapter(Class<? super SSLSocket> cls, Class<? super SSLSocketFactory> cls2, Class<?> cls3) {
        super(cls);
        if (cls == null) {
            Intrinsics.a("sslSocketClass");
            throw null;
        } else if (cls2 == null) {
            Intrinsics.a("sslSocketFactoryClass");
            throw null;
        } else if (cls3 != null) {
        } else {
            Intrinsics.a("paramClass");
            throw null;
        }
    }
}
