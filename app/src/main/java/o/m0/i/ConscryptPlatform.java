package o.m0.i;

import java.security.Provider;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.c0;
import org.conscrypt.Conscrypt;
import org.conscrypt.ConscryptHostnameVerifier;

/* compiled from: ConscryptPlatform.kt */
public final class ConscryptPlatform extends Platform {

    /* renamed from: e  reason: collision with root package name */
    public static final boolean f3046e;

    /* renamed from: f  reason: collision with root package name */
    public static final a f3047f = new a(null);
    public final Provider d;

    /* compiled from: ConscryptPlatform.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final boolean a(int i2, int i3, int i4) {
            Conscrypt.Version version = Conscrypt.version();
            if (version.major() != i2) {
                if (version.major() > i2) {
                    return true;
                }
                return false;
            } else if (version.minor() != i3) {
                if (version.minor() > i3) {
                    return true;
                }
                return false;
            } else if (version.patch() >= i4) {
                return true;
            } else {
                return false;
            }
        }
    }

    /* compiled from: ConscryptPlatform.kt */
    public static final class b implements ConscryptHostnameVerifier {
        public static final b a = new b();
    }

    static {
        boolean z = false;
        try {
            Class.forName("org.conscrypt.Conscrypt$Version");
            if (Conscrypt.isAvailable() && f3047f.a(2, 1, 0)) {
                z = true;
            }
        } catch (ClassNotFoundException unused) {
        }
        f3046e = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.Provider, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public ConscryptPlatform() {
        Provider build = Conscrypt.newProviderBuilder().provideTrustManager(true).build();
        Intrinsics.a((Object) build, "Conscrypt.newProviderBui…rustManager(true).build()");
        this.d = build;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.SSLContext, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public SSLContext a() {
        SSLContext instance = SSLContext.getInstance("TLS", this.d);
        Intrinsics.a((Object) instance, "SSLContext.getInstance(\"TLS\", provider)");
        return instance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.X509TrustManager, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public X509TrustManager b() {
        X509TrustManager defaultX509TrustManager = Conscrypt.getDefaultX509TrustManager();
        Intrinsics.a((Object) defaultX509TrustManager, "Conscrypt.getDefaultX509TrustManager()");
        return defaultX509TrustManager;
    }

    public void c(X509TrustManager x509TrustManager) {
        if (Conscrypt.isConscrypt(x509TrustManager)) {
            Conscrypt.setHostnameVerifier(x509TrustManager, b.a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.i.Platform.a(javax.net.ssl.SSLSocket, java.lang.String, java.util.List<o.c0>):void
     arg types: [javax.net.ssl.SSLSocket, java.lang.String, java.util.List<? extends o.c0>]
     candidates:
      o.m0.i.Platform.a(int, java.lang.String, java.lang.Throwable):void
      o.m0.i.Platform.a(java.net.Socket, java.net.InetSocketAddress, int):void
      o.m0.i.Platform.a(javax.net.ssl.SSLSocket, java.lang.String, java.util.List<o.c0>):void */
    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list == null) {
            Intrinsics.a("protocols");
            throw null;
        } else if (Conscrypt.isConscrypt(sSLSocket)) {
            if (str != null) {
                Conscrypt.setUseSessionTickets(sSLSocket, true);
                Conscrypt.setHostname(sSLSocket, str);
            }
            Object[] array = Platform.c.a(list).toArray(new String[0]);
            if (array != null) {
                Conscrypt.setApplicationProtocols(sSLSocket, (String[]) array);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            super.a(sSLSocket, str, (List<c0>) list);
        }
    }

    public String b(SSLSocket sSLSocket) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (Conscrypt.isConscrypt(sSLSocket)) {
            return Conscrypt.getApplicationProtocol(sSLSocket);
        } else {
            super.b(sSLSocket);
            return null;
        }
    }

    public void a(SSLSocketFactory sSLSocketFactory) {
        if (sSLSocketFactory == null) {
            Intrinsics.a("socketFactory");
            throw null;
        } else if (Conscrypt.isConscrypt(sSLSocketFactory)) {
            Conscrypt.setUseEngineSocket(sSLSocketFactory, true);
        }
    }
}
