package o.m0.e;

import o.Request;
import o.Response;
import o.m0.d.RealConnection;
import p.Sink;
import p.Source;

/* compiled from: ExchangeCode.kt */
public interface ExchangeCode {
    long a(Response response);

    Response.a a(boolean z);

    RealConnection a();

    Sink a(Request request, long j2);

    void a(Request request);

    Source b(Response response);

    void b();

    void c();

    void cancel();
}
