package o.m0.k;

import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;

/* compiled from: BasicCertificateChainCleaner.kt */
public final class BasicCertificateChainCleaner extends CertificateChainCleaner {
    public final TrustRootIndex a;

    public BasicCertificateChainCleaner(TrustRootIndex trustRootIndex) {
        if (trustRootIndex != null) {
            this.a = trustRootIndex;
        } else {
            Intrinsics.a("trustRootIndex");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Iterator, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public List<Certificate> a(List<? extends Certificate> list, String str) {
        if (list == null) {
            Intrinsics.a("chain");
            throw null;
        } else if (str != null) {
            ArrayDeque arrayDeque = new ArrayDeque(list);
            ArrayList arrayList = new ArrayList();
            Object removeFirst = arrayDeque.removeFirst();
            Intrinsics.a(removeFirst, "queue.removeFirst()");
            arrayList.add(removeFirst);
            int i2 = 0;
            boolean z = false;
            while (i2 < 9) {
                Object obj = arrayList.get(arrayList.size() - 1);
                if (obj != null) {
                    X509Certificate x509Certificate = (X509Certificate) obj;
                    X509Certificate a2 = this.a.a(x509Certificate);
                    if (a2 != null) {
                        if (arrayList.size() > 1 || (!Intrinsics.a(x509Certificate, a2))) {
                            arrayList.add(a2);
                        }
                        if (a(a2, a2)) {
                            return arrayList;
                        }
                        z = true;
                    } else {
                        Iterator it = arrayDeque.iterator();
                        Intrinsics.a((Object) it, "queue.iterator()");
                        while (it.hasNext()) {
                            Object next = it.next();
                            if (next != null) {
                                X509Certificate x509Certificate2 = (X509Certificate) next;
                                if (a(x509Certificate, x509Certificate2)) {
                                    it.remove();
                                    arrayList.add(x509Certificate2);
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
                            }
                        }
                        if (z) {
                            return arrayList;
                        }
                        throw new SSLPeerUnverifiedException("Failed to find a trusted cert that signed " + x509Certificate);
                    }
                    i2++;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
                }
            }
            throw new SSLPeerUnverifiedException("Certificate chain too long: " + arrayList);
        } else {
            Intrinsics.a("hostname");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof BasicCertificateChainCleaner) && Intrinsics.a(((BasicCertificateChainCleaner) obj).a, this.a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public final boolean a(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        if (!Intrinsics.a(x509Certificate.getIssuerDN(), x509Certificate2.getSubjectDN())) {
            return false;
        }
        try {
            x509Certificate.verify(x509Certificate2.getPublicKey());
            return true;
        } catch (GeneralSecurityException unused) {
            return false;
        }
    }
}
