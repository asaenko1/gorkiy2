package o.m0.k;

import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import kotlin.TypeCastException;
import n.i.Collections2;
import n.n.c.Intrinsics;

/* compiled from: OkHostnameVerifier.kt */
public final class OkHostnameVerifier implements HostnameVerifier {
    public static final OkHostnameVerifier a = new OkHostnameVerifier();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x012b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r13, java.security.cert.X509Certificate r14) {
        /*
            r12 = this;
            r0 = 0
            if (r13 == 0) goto L_0x0132
            if (r14 == 0) goto L_0x012c
            boolean r0 = o.m0.Util.a(r13)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0036
            r0 = 7
            java.util.List r14 = r12.a(r14, r0)
            boolean r0 = r14 instanceof java.util.Collection
            if (r0 == 0) goto L_0x001e
            boolean r0 = r14.isEmpty()
            if (r0 == 0) goto L_0x001e
            goto L_0x012a
        L_0x001e:
            java.util.Iterator r14 = r14.iterator()
        L_0x0022:
            boolean r0 = r14.hasNext()
            if (r0 == 0) goto L_0x012a
            java.lang.Object r0 = r14.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = n.r.Indent.a(r13, r0, r1)
            if (r0 == 0) goto L_0x0022
            goto L_0x012b
        L_0x0036:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r3 = "Locale.US"
            n.n.c.Intrinsics.a(r0, r3)
            java.lang.String r13 = r13.toLowerCase(r0)
            java.lang.String r0 = "(this as java.lang.String).toLowerCase(locale)"
            n.n.c.Intrinsics.a(r13, r0)
            r4 = 2
            java.util.List r14 = r12.a(r14, r4)
            boolean r5 = r14 instanceof java.util.Collection
            if (r5 == 0) goto L_0x0057
            boolean r5 = r14.isEmpty()
            if (r5 == 0) goto L_0x0057
            goto L_0x012a
        L_0x0057:
            java.util.Iterator r14 = r14.iterator()
        L_0x005b:
            boolean r5 = r14.hasNext()
            if (r5 == 0) goto L_0x012a
            java.lang.Object r5 = r14.next()
            java.lang.String r5 = (java.lang.String) r5
            int r6 = r13.length()
            if (r6 != 0) goto L_0x006f
            r6 = 1
            goto L_0x0070
        L_0x006f:
            r6 = 0
        L_0x0070:
            if (r6 != 0) goto L_0x0126
            java.lang.String r6 = "."
            boolean r7 = n.r.Indent.b(r13, r6, r2, r4)
            if (r7 != 0) goto L_0x0126
            java.lang.String r7 = ".."
            boolean r8 = n.r.Indent.a(r13, r7, r2, r4)
            if (r8 == 0) goto L_0x0084
            goto L_0x0126
        L_0x0084:
            if (r5 == 0) goto L_0x008f
            int r8 = r5.length()
            if (r8 != 0) goto L_0x008d
            goto L_0x008f
        L_0x008d:
            r8 = 0
            goto L_0x0090
        L_0x008f:
            r8 = 1
        L_0x0090:
            if (r8 != 0) goto L_0x0126
            boolean r8 = n.r.Indent.b(r5, r6, r2, r4)
            if (r8 != 0) goto L_0x0126
            boolean r7 = n.r.Indent.a(r5, r7, r2, r4)
            if (r7 == 0) goto L_0x00a0
            goto L_0x0126
        L_0x00a0:
            boolean r7 = n.r.Indent.a(r13, r6, r2, r4)
            if (r7 != 0) goto L_0x00ab
            java.lang.String r7 = j.a.a.a.outline.a(r13, r6)
            goto L_0x00ac
        L_0x00ab:
            r7 = r13
        L_0x00ac:
            boolean r8 = n.r.Indent.a(r5, r6, r2, r4)
            if (r8 != 0) goto L_0x00b6
            java.lang.String r5 = j.a.a.a.outline.a(r5, r6)
        L_0x00b6:
            java.util.Locale r6 = java.util.Locale.US
            n.n.c.Intrinsics.a(r6, r3)
            if (r5 == 0) goto L_0x011e
            java.lang.String r5 = r5.toLowerCase(r6)
            n.n.c.Intrinsics.a(r5, r0)
            java.lang.String r6 = "*"
            boolean r6 = n.r.Indent.a(r5, r6, r2, r4)
            if (r6 != 0) goto L_0x00d1
            boolean r5 = n.n.c.Intrinsics.a(r7, r5)
            goto L_0x0127
        L_0x00d1:
            java.lang.String r6 = "*."
            boolean r8 = n.r.Indent.b(r5, r6, r2, r4)
            if (r8 == 0) goto L_0x0126
            r8 = 42
            r9 = 4
            int r8 = n.r.Indent.a(r5, r8, r1, r2, r9)
            r10 = -1
            if (r8 == r10) goto L_0x00e4
            goto L_0x0126
        L_0x00e4:
            int r8 = r7.length()
            int r11 = r5.length()
            if (r8 >= r11) goto L_0x00ef
            goto L_0x0126
        L_0x00ef:
            boolean r6 = n.n.c.Intrinsics.a(r6, r5)
            if (r6 == 0) goto L_0x00f6
            goto L_0x0126
        L_0x00f6:
            java.lang.String r5 = r5.substring(r1)
            java.lang.String r6 = "(this as java.lang.String).substring(startIndex)"
            n.n.c.Intrinsics.a(r5, r6)
            boolean r6 = n.r.Indent.a(r7, r5, r2, r4)
            if (r6 != 0) goto L_0x0106
            goto L_0x0126
        L_0x0106:
            int r6 = r7.length()
            int r5 = r5.length()
            int r6 = r6 - r5
            if (r6 <= 0) goto L_0x011c
            r5 = 46
            int r6 = r6 + -1
            int r5 = n.r.Indent.b(r7, r5, r6, r2, r9)
            if (r5 == r10) goto L_0x011c
            goto L_0x0126
        L_0x011c:
            r5 = 1
            goto L_0x0127
        L_0x011e:
            kotlin.TypeCastException r13 = new kotlin.TypeCastException
            java.lang.String r14 = "null cannot be cast to non-null type java.lang.String"
            r13.<init>(r14)
            throw r13
        L_0x0126:
            r5 = 0
        L_0x0127:
            if (r5 == 0) goto L_0x005b
            goto L_0x012b
        L_0x012a:
            r1 = 0
        L_0x012b:
            return r1
        L_0x012c:
            java.lang.String r13 = "certificate"
            n.n.c.Intrinsics.a(r13)
            throw r0
        L_0x0132:
            java.lang.String r13 = "host"
            n.n.c.Intrinsics.a(r13)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.k.OkHostnameVerifier.a(java.lang.String, java.security.cert.X509Certificate):boolean");
    }

    public boolean verify(String str, SSLSession sSLSession) {
        if (str == null) {
            Intrinsics.a("host");
            throw null;
        } else if (sSLSession != null) {
            try {
                Certificate certificate = sSLSession.getPeerCertificates()[0];
                if (certificate != null) {
                    return a(str, (X509Certificate) certificate);
                }
                throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
            } catch (SSLException unused) {
                return false;
            }
        } else {
            Intrinsics.a("session");
            throw null;
        }
    }

    public final List<String> a(X509Certificate x509Certificate, int i2) {
        try {
            Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
            if (subjectAlternativeNames == null) {
                return Collections2.b;
            }
            ArrayList arrayList = new ArrayList();
            for (List next : subjectAlternativeNames) {
                if (next != null) {
                    if (next.size() >= 2) {
                        if (!(!Intrinsics.a(next.get(0), Integer.valueOf(i2)))) {
                            Object obj = next.get(1);
                            if (obj != null) {
                                arrayList.add((String) obj);
                            }
                        }
                    }
                }
            }
            return arrayList;
        } catch (CertificateParsingException unused) {
            return Collections2.b;
        }
    }
}
