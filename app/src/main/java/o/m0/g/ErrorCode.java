package o.m0.g;

import n.n.c.DefaultConstructorMarker;

/* compiled from: ErrorCode.kt */
public enum ErrorCode {
    NO_ERROR(0),
    PROTOCOL_ERROR(1),
    INTERNAL_ERROR(2),
    FLOW_CONTROL_ERROR(3),
    REFUSED_STREAM(7),
    CANCEL(8),
    COMPRESSION_ERROR(9),
    CONNECT_ERROR(10),
    ENHANCE_YOUR_CALM(11),
    INADEQUATE_SECURITY(12),
    HTTP_1_1_REQUIRED(13);
    
    public static final a Companion = new a(null);
    public final int httpCode;

    /* compiled from: ErrorCode.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final ErrorCode a(int i2) {
            for (ErrorCode errorCode : ErrorCode.values()) {
                if (errorCode.httpCode == i2) {
                    return errorCode;
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: public */
    ErrorCode(int i2) {
        this.httpCode = i2;
    }
}
