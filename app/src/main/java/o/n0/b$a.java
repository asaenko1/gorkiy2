package o.n0;

import n.n.c.Intrinsics;
import o.m0.i.Platform;
import o.n0.HttpLoggingInterceptor;

/* compiled from: HttpLoggingInterceptor.kt */
public final class b$a implements HttpLoggingInterceptor.b {
    public void a(String str) {
        if (str != null) {
            Platform.a aVar = Platform.c;
            Platform.a.a(4, str, (Throwable) null);
            return;
        }
        Intrinsics.a("message");
        throw null;
    }
}
