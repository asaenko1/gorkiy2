package e.a.b.h.c;

import e.a.a.a.e.d;
import java.util.List;
import n.Tuples;

/* compiled from: IStepScreensConfig.kt */
public interface IStepScreensConfig {
    List<Tuples<d, String>> a();

    Tuples<d, String> a(int i2);

    List<Tuples<d, String>> b();

    int c();
}
