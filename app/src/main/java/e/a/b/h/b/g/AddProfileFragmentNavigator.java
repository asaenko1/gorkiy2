package e.a.b.h.b.g;

import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.b.h.c.IStepScreensConfig;
import e.a.b.h.c.StepsNestedFragmentCoordinator;
import n.n.c.Intrinsics;

/* compiled from: AddProfileFragmentNavigator.kt */
public final class AddProfileFragmentNavigator extends StepsNestedFragmentCoordinator {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AddProfileFragmentNavigator(NestedFragmentCiceroneHolder nestedFragmentCiceroneHolder, IStepScreensConfig iStepScreensConfig) {
        super(nestedFragmentCiceroneHolder, iStepScreensConfig);
        if (nestedFragmentCiceroneHolder == null) {
            Intrinsics.a("nestedFragmentCiceroneHolder");
            throw null;
        } else if (iStepScreensConfig != null) {
        } else {
            Intrinsics.a("screensConfig");
            throw null;
        }
    }
}
