package e.a.b.h.b.h.o;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.b.h.b.h.o.b;
import e.c.d.a.ViewAction;
import java.util.HashMap;
import java.util.List;
import n.i.Collections2;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.Children;

/* compiled from: ChildrenAdapter.kt */
public final class ChildrenAdapter0 extends RecyclerView.g<b.a> {
    public List<Children> c = Collections2.b;
    public final ViewAction<Children> d = new ViewAction<>();

    /* compiled from: ChildrenAdapter.kt */
    public final class a extends RecyclerView.d0 {

        /* renamed from: t  reason: collision with root package name */
        public final View f700t;
        public final /* synthetic */ ChildrenAdapter0 u;
        public HashMap v;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ChildrenAdapter0 childrenAdapter0, View view) {
            super(view);
            if (view != null) {
                this.u = childrenAdapter0;
                this.f700t = view;
                return;
            }
            Intrinsics.a("containerView");
            throw null;
        }

        public View c(int i2) {
            if (this.v == null) {
                this.v = new HashMap();
            }
            View view = (View) this.v.get(Integer.valueOf(i2));
            if (view != null) {
                return view;
            }
            View view2 = this.f700t;
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i2);
            this.v.put(Integer.valueOf(i2), findViewById);
            return findViewById;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(RecyclerView.d0 d0Var, int i2) {
        a aVar = (a) d0Var;
        if (aVar != null) {
            Children children = this.c.get(i2);
            if (children != null) {
                ((ImageView) aVar.c(e.a.b.b.item_filling_profile_child_iv_delete)).setOnClickListener(new ChildrenAdapter(aVar, children));
                TextView textView = (TextView) aVar.c(e.a.b.b.item_filling_profile_child_full_name);
                Intrinsics.a((Object) textView, "item_filling_profile_child_full_name");
                textView.setText(children.getFullName());
                return;
            }
            Intrinsics.a("item");
            throw null;
        }
        Intrinsics.a("holder");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public RecyclerView.d0 a(ViewGroup viewGroup, int i2) {
        if (viewGroup != null) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.item_filling_profile_child, viewGroup, false);
            Intrinsics.a((Object) inflate, "LayoutInflater.from(pare…, parent, false\n        )");
            return new a(this, inflate);
        }
        Intrinsics.a("parent");
        throw null;
    }

    public int a() {
        return this.c.size();
    }
}
