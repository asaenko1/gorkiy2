package e.a.b.h.b.d;

import e.a.a.g.a.d;
import e.a.a.g.a.g.f;
import e.a.a.g.b.b.a.b;
import e.a.a.j.a.c;
import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class AuthFragmentVm_Factory implements Factory<a> {
    public final Provider<c> a;
    public final Provider<b> b;
    public final Provider<d> c;
    public final Provider<a> d;

    /* renamed from: e  reason: collision with root package name */
    public final Provider<g> f670e;

    /* renamed from: f  reason: collision with root package name */
    public final Provider<e.a.a.a.e.o.c> f671f;
    public final Provider<f> g;

    public AuthFragmentVm_Factory(Provider<c> provider, Provider<b> provider2, Provider<d> provider3, Provider<a> provider4, Provider<g> provider5, Provider<e.a.a.a.e.o.c> provider6, Provider<f> provider7) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.f670e = provider5;
        this.f671f = provider6;
        this.g = provider7;
    }

    public Object get() {
        return new AuthFragmentVm(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.f670e.get(), this.f671f.get(), this.g.get());
    }
}
