package e.a.b.h.b.f;

import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.Statistic;

/* compiled from: MainFragmentViewState.kt */
public final class MainFragmentViewState {
    public final boolean a;
    public final Statistic b;

    public MainFragmentViewState() {
        this.a = false;
        this.b = null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MainFragmentViewState)) {
            return false;
        }
        MainFragmentViewState mainFragmentViewState = (MainFragmentViewState) obj;
        return this.a == mainFragmentViewState.a && Intrinsics.a(this.b, mainFragmentViewState.b);
    }

    public int hashCode() {
        boolean z = this.a;
        if (z) {
            z = true;
        }
        int i2 = (z ? 1 : 0) * true;
        Statistic statistic = this.b;
        return i2 + (statistic != null ? statistic.hashCode() : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("MainFragmentViewState(isLoading=");
        a2.append(this.a);
        a2.append(", statistic=");
        a2.append(this.b);
        a2.append(")");
        return a2.toString();
    }

    public MainFragmentViewState(boolean z, Statistic statistic) {
        this.a = z;
        this.b = statistic;
    }

    public /* synthetic */ MainFragmentViewState(boolean z, Statistic statistic, int i2) {
        z = (i2 & 1) != 0 ? false : z;
        statistic = (i2 & 2) != 0 ? null : statistic;
        this.a = z;
        this.b = statistic;
    }
}
