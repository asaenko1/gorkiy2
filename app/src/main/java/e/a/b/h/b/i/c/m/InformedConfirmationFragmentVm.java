package e.a.b.h.b.i.c.m;

import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.BaseStepValidatedFragmentVm;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentViewState0;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentViewState1;
import e.a.b.h.b.i.c.m.a;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState1;
import e.c.c.BaseViewState4;
import e.c.c.k;
import e.c.c.p;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: InformedConfirmationFragmentVm.kt */
public final class InformedConfirmationFragmentVm extends BaseStepValidatedFragmentVm<b> {

    /* renamed from: n  reason: collision with root package name */
    public final IProfileRepository f708n;

    /* renamed from: o  reason: collision with root package name */
    public final IMainCoordinator f709o;

    /* compiled from: InformedConfirmationFragmentVm.kt */
    public static final class a<T, R> implements Function<T, R> {
        public final /* synthetic */ InformedConfirmationFragmentVm a;

        public a(InformedConfirmationFragmentVm informedConfirmationFragmentVm) {
            this.a = informedConfirmationFragmentVm;
        }

        public Object a(Object obj) {
            InformedConfirmationFragmentViewState0.a aVar = (InformedConfirmationFragmentViewState0.a) obj;
            if (aVar != null) {
                this.a.f708n.f().setDataCorrectnessAccept(aVar.a);
                return new BaseMviVm0.a(new InformedConfirmationFragmentViewState1.a(aVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragmentVm.kt */
    public static final class b<T, R> implements Function<T, R> {
        public final /* synthetic */ InformedConfirmationFragmentVm a;

        public b(InformedConfirmationFragmentVm informedConfirmationFragmentVm) {
            this.a = informedConfirmationFragmentVm;
        }

        public Object a(Object obj) {
            InformedConfirmationFragmentViewState0.b bVar = (InformedConfirmationFragmentViewState0.b) obj;
            if (bVar != null) {
                this.a.f708n.f().setQuarantineViolationLiability(bVar.a);
                return new BaseMviVm0.a(new InformedConfirmationFragmentViewState1.c(bVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragmentVm.kt */
    public static final class c<T, R> implements Function<T, R> {
        public final /* synthetic */ InformedConfirmationFragmentVm a;

        public c(InformedConfirmationFragmentVm informedConfirmationFragmentVm) {
            this.a = informedConfirmationFragmentVm;
        }

        public Object a(Object obj) {
            InformedConfirmationFragmentViewState0.c cVar = (InformedConfirmationFragmentViewState0.c) obj;
            if (cVar != null) {
                this.a.f708n.f().setQuarantineRequirementsAccept(cVar.a);
                return new BaseMviVm0.a(new InformedConfirmationFragmentViewState1.d(cVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragmentVm.kt */
    public static final class d extends j implements Functions0<a.e, g> {
        public final /* synthetic */ InformedConfirmationFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(InformedConfirmationFragmentVm informedConfirmationFragmentVm) {
            super(1);
            this.c = informedConfirmationFragmentVm;
        }

        public Object a(Object obj) {
            if (((InformedConfirmationFragmentViewState0.e) obj) != null) {
                this.c.f709o.h();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragmentVm.kt */
    public static final class e extends j implements Functions0<a.d, g> {
        public final /* synthetic */ InformedConfirmationFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(InformedConfirmationFragmentVm informedConfirmationFragmentVm) {
            super(1);
            this.c = informedConfirmationFragmentVm;
        }

        public Object a(Object obj) {
            if (((InformedConfirmationFragmentViewState0.d) obj) != null) {
                this.c.f709o.j();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragmentVm.kt */
    public static final class f<T, R> implements Function<T, R> {
        public final /* synthetic */ InformedConfirmationFragmentVm a;

        public f(InformedConfirmationFragmentVm informedConfirmationFragmentVm) {
            this.a = informedConfirmationFragmentVm;
        }

        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                ProfileData f2 = this.a.f708n.f();
                return new BaseMviVm0.a(new InformedConfirmationFragmentViewState1.b(f2.getQuarantineRequirementsAccept(), f2.getQuarantineViolationLiability(), f2.getDataCorrectnessAccept()));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InformedConfirmationFragmentVm(IProfileRepository iProfileRepository, IMainCoordinator iMainCoordinator) {
        super(iMainCoordinator);
        if (iProfileRepository == null) {
            Intrinsics.a("profileRepository");
            throw null;
        } else if (iMainCoordinator != null) {
            this.f708n = iProfileRepository;
            this.f709o = iMainCoordinator;
        } else {
            Intrinsics.a("mainNavigator");
            throw null;
        }
    }

    public Object a(Object obj, BaseViewState1 baseViewState1) {
        InformedConfirmationFragmentViewState informedConfirmationFragmentViewState = (InformedConfirmationFragmentViewState) obj;
        if (informedConfirmationFragmentViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof InformedConfirmationFragmentViewState1.b) {
            InformedConfirmationFragmentViewState1.b bVar = (InformedConfirmationFragmentViewState1.b) baseViewState1;
            return new InformedConfirmationFragmentViewState(bVar.a, bVar.b, bVar.c);
        } else if (baseViewState1 instanceof InformedConfirmationFragmentViewState1.d) {
            return InformedConfirmationFragmentViewState.a(informedConfirmationFragmentViewState, ((InformedConfirmationFragmentViewState1.d) baseViewState1).a, false, false, 6);
        } else {
            if (baseViewState1 instanceof InformedConfirmationFragmentViewState1.c) {
                return InformedConfirmationFragmentViewState.a(informedConfirmationFragmentViewState, false, ((InformedConfirmationFragmentViewState1.c) baseViewState1).a, false, 5);
            }
            if (baseViewState1 instanceof InformedConfirmationFragmentViewState1.a) {
                return InformedConfirmationFragmentViewState.a(informedConfirmationFragmentViewState, false, false, ((InformedConfirmationFragmentViewState1.a) baseViewState1).a, 3);
            }
            super.a(informedConfirmationFragmentViewState, baseViewState1);
            return informedConfirmationFragmentViewState;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> b(Observable<p> observable) {
        if (observable != null) {
            Observable<R> c2 = observable.c((Function) new f(this));
            Intrinsics.a((Object) c2, "observable.map {\n       …)\n            )\n        }");
            return c2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    public Object d() {
        return new InformedConfirmationFragmentViewState(false, false, false, 7);
    }

    public boolean h() {
        return this.f708n.f().getQuarantineRequirementsAccept() && this.f708n.f().getDataCorrectnessAccept() && this.f708n.f().getQuarantineViolationLiability();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(InformedConfirmationFragmentViewState0.a.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(InformedConfirmationFragmentViewState0.b.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(InformedConfirmationFragmentViewState0.c.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(InformedConfirmationFragmentViewState0.e.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(InformedConfirmationFragmentViewState0.d.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a7 = Observable.a(super.a(observable), a2.c((Function) new a(this)), a3.c((Function) new b(this)), a4.c((Function) new c(this)), BaseMviVm.a(a5, new d(this)), BaseMviVm.a(a6, new e(this)));
            Intrinsics.a((Object) a7, "Observable.mergeArray(\n …)\n            }\n        )");
            return a7;
        }
        Intrinsics.a("o");
        throw null;
    }
}
