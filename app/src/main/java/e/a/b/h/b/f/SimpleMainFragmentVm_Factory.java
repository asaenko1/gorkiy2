package e.a.b.h.b.f;

import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class SimpleMainFragmentVm_Factory implements Factory<h> {
    public final Provider<g> a;

    public SimpleMainFragmentVm_Factory(Provider<g> provider) {
        this.a = provider;
    }

    public Object get() {
        return new SimpleMainFragmentVm(this.a.get());
    }
}
