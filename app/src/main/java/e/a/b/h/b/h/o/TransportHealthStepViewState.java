package e.a.b.h.b.h.o;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: TransportHealthStepViewState.kt */
public final class TransportHealthStepViewState implements BottomSheetSelectorNavigationDto {
    public static final Parcelable.Creator CREATOR = new a();
    public final boolean b;
    public final String c;
    public final boolean d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                boolean z = true;
                boolean z2 = parcel.readInt() != 0;
                String readString = parcel.readString();
                if (parcel.readInt() == 0) {
                    z = false;
                }
                return new TransportHealthStepViewState(z2, readString, z);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new TransportHealthStepViewState[i2];
        }
    }

    public TransportHealthStepViewState(boolean z, String str, boolean z2) {
        if (str != null) {
            this.b = z;
            this.c = str;
            this.d = z2;
            return;
        }
        Intrinsics.a("label");
        throw null;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TransportHealthStepViewState)) {
            return false;
        }
        TransportHealthStepViewState transportHealthStepViewState = (TransportHealthStepViewState) obj;
        return this.b == transportHealthStepViewState.b && Intrinsics.a(this.c, transportHealthStepViewState.c) && this.d == transportHealthStepViewState.d;
    }

    public String getLabel() {
        return this.c;
    }

    public int hashCode() {
        boolean z = this.b;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i2 = (z ? 1 : 0) * true;
        String str = this.c;
        int hashCode = (i2 + (str != null ? str.hashCode() : 0)) * 31;
        boolean z3 = this.d;
        if (!z3) {
            z2 = z3;
        }
        return hashCode + (z2 ? 1 : 0);
    }

    public boolean isSelected() {
        return this.d;
    }

    public String toString() {
        StringBuilder a2 = outline.a("HasSymptomsSelectorItem(item=");
        a2.append(this.b);
        a2.append(", label=");
        a2.append(this.c);
        a2.append(", isSelected=");
        return outline.a(a2, this.d, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeInt(this.b ? 1 : 0);
            parcel.writeString(this.c);
            parcel.writeInt(this.d ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
