package e.a.b.h.b.g;

import e.a.a.a.e.BaseScreens2;
import e.a.a.a.e.q.FragmentCoordinator;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.b.h.b.MainActivityVm;
import e.b.a.Screen;
import n.n.c.Intrinsics;

/* compiled from: MainCoordinator.kt */
public final class MainCoordinator extends FragmentCoordinator implements IMainCoordinator {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MainCoordinator(FragmentCiceroneHolder fragmentCiceroneHolder) {
        super(fragmentCiceroneHolder);
        if (fragmentCiceroneHolder != null) {
        } else {
            Intrinsics.a("fragmentCiceroneHolder");
            throw null;
        }
    }

    public void a() {
        n().b(Screens6.d);
    }

    public void b() {
        n().a((BaseScreens2) Screens2.d);
    }

    public void c() {
        n().a((Screen) Screens7.d);
    }

    public void d() {
        n().a((BaseScreens2) Screens1.d);
    }

    public void e() {
        n().a((Screen) Screens.d);
    }

    public void g() {
        n().a((BaseScreens2) Screens17.d);
    }

    public void h() {
        n().a((BaseScreens2) Screens14.d);
    }

    public void i() {
        n().a((BaseScreens2) Screens0.d);
    }

    public void j() {
        n().a((BaseScreens2) Screens12.d);
    }

    public void k() {
        n().b(Screens13.d);
    }

    public void l() {
        n().b(Screens10.d);
    }

    public void m() {
        n().a((BaseScreens2) Screens16.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String o() {
        String name = MainActivityVm.class.getName();
        Intrinsics.a((Object) name, "MainActivityVm::class.java.name");
        return name;
    }

    public void a(boolean z) {
        if (z) {
            n().b(Screens9.d);
        } else {
            n().a((Screen) Screens9.d);
        }
    }
}
