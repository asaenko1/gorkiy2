package e.a.b.h.b;

import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.b.h.b.g.IMainCoordinator;
import e.c.d.c.BaseVm;
import n.n.c.Intrinsics;

/* compiled from: MainActivityVm.kt */
public final class MainActivityVm extends BaseVm {
    public final ITopLevelCoordinator d;

    public MainActivityVm(ITopLevelCoordinator iTopLevelCoordinator, IMainCoordinator iMainCoordinator) {
        if (iTopLevelCoordinator == null) {
            Intrinsics.a("topLevelNavigator");
            throw null;
        } else if (iMainCoordinator != null) {
            this.d = iTopLevelCoordinator;
            iMainCoordinator.a();
        } else {
            Intrinsics.a("mainNavigator");
            throw null;
        }
    }

    public void c() {
    }
}
