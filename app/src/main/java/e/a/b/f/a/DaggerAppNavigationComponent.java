package e.a.b.f.a;

import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.o.c;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.b.f.c.AppNavigationModule;
import e.a.b.f.c.j;
import e.a.b.f.c.k;
import k.a.DoubleCheck;
import m.a.Provider;

public final class DaggerAppNavigationComponent implements AppNavigationComponent {
    public Provider<e.a.a.a.e.r.a> a;
    public Provider<c> b;

    public /* synthetic */ DaggerAppNavigationComponent(AppNavigationModule appNavigationModule, a aVar) {
        Provider<e.a.a.a.e.r.a> a2 = DoubleCheck.a(new j(appNavigationModule));
        this.a = a2;
        this.b = DoubleCheck.a(new k(appNavigationModule, a2));
    }

    public AppCiceroneHolder a() {
        return this.a.get();
    }

    public ITopLevelCoordinator b() {
        return this.b.get();
    }
}
