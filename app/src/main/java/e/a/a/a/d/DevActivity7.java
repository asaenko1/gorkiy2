package e.a.a.a.d;

import n.Unit;
import n.g;
import n.n.b.Functions1;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;
import ru.covid19.core.presentation.dev.DevActivity;

/* compiled from: DevActivity.kt */
public final /* synthetic */ class DevActivity7 extends h implements Functions1<String, Object, g> {
    public DevActivity7(DevActivity devActivity) {
        super(2, devActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.d.DevActivity7, n.n.c.CallableReference] */
    public Object a(Object obj, Object obj2) {
        String str = (String) obj;
        if (str != null) {
            DevActivity.a((DevActivity) this.c, str, obj2);
            return Unit.a;
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "showMessage";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(DevActivity.class);
    }

    public final String i() {
        return "showMessage(Ljava/lang/String;Ljava/lang/Object;)V";
    }
}
