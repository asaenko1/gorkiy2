package e.a.a.a.e.p;

import android.os.Parcelable;

/* compiled from: BottomSheetSelectorNavigationDto.kt */
public interface BottomSheetSelectorNavigationDto extends Parcelable {
    String getLabel();

    boolean isSelected();
}
