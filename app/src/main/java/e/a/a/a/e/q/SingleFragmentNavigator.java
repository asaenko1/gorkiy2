package e.a.a.a.e.q;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.a.a.e.Commands7;
import e.b.a.Navigator;
import e.b.a.Screen;
import e.b.a.h.Back;
import e.b.a.h.BackTo;
import e.b.a.h.Command;
import e.b.a.h.Forward;
import e.b.a.h.Replace;
import i.l.a.BackStackRecord;
import i.l.a.DialogFragment;
import i.l.a.FragmentManager;
import i.l.a.FragmentManagerImpl;
import i.l.a.FragmentTransaction;
import i.l.a.j;
import java.util.ArrayList;
import java.util.LinkedList;
import kotlin.TypeCastException;
import n.g;
import n.n.b.Functions;
import n.n.c.Intrinsics;

/* compiled from: SingleFragmentNavigator.kt */
public class SingleFragmentNavigator implements Navigator {
    public int a;
    public LinkedList<String> b;
    public final FragmentManager c;
    public final Functions<g> d;

    /* compiled from: SingleFragmentNavigator.kt */
    public static final class a implements Runnable {
        public final /* synthetic */ SingleFragmentNavigator b;
        public final /* synthetic */ Command[] c;

        public a(SingleFragmentNavigator singleFragmentNavigator, Command[] commandArr) {
            this.b = singleFragmentNavigator;
            this.c = commandArr;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [i.l.a.FragmentManager$a, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [i.l.a.BackStackRecord, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [e.b.a.Screen, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
         arg types: [i.l.a.FragmentManagerImpl$i, int]
         candidates:
          i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
          i.l.a.FragmentManagerImpl.a(int, boolean):void
          i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
          i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
          i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
          i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
          i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void */
        public final void run() {
            FragmentManagerImpl fragmentManagerImpl = (FragmentManagerImpl) this.b.c;
            fragmentManagerImpl.j();
            fragmentManagerImpl.k();
            SingleFragmentNavigator singleFragmentNavigator = this.b;
            if (singleFragmentNavigator != null) {
                singleFragmentNavigator.b = new LinkedList<>();
                ArrayList<i.l.a.a> arrayList = ((FragmentManagerImpl) singleFragmentNavigator.c).f1297i;
                int size = arrayList != null ? arrayList.size() : 0;
                int i2 = 0;
                while (i2 < size) {
                    LinkedList<String> linkedList = singleFragmentNavigator.b;
                    FragmentManager.a aVar = ((FragmentManagerImpl) singleFragmentNavigator.c).f1297i.get(i2);
                    Intrinsics.a((Object) aVar, "fragmentManager.getBackStackEntryAt(i)");
                    String a = aVar.a();
                    if (a != null) {
                        linkedList.add(a);
                        i2++;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                Command[] commandArr = this.c;
                int length = commandArr.length;
                int i3 = 0;
                while (i3 < length) {
                    Command command = commandArr[i3];
                    SingleFragmentNavigator singleFragmentNavigator2 = this.b;
                    if (singleFragmentNavigator2 == null) {
                        throw null;
                    } else if (command != null) {
                        if (command instanceof Forward) {
                            Forward forward = (Forward) command;
                            Screen screen = forward.a;
                            if (screen != null) {
                                Fragment a2 = singleFragmentNavigator2.a((BaseScreens2) screen);
                                FragmentManagerImpl fragmentManagerImpl2 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                if (fragmentManagerImpl2 != null) {
                                    BackStackRecord backStackRecord = new BackStackRecord(fragmentManagerImpl2);
                                    Intrinsics.a((Object) backStackRecord, "fragmentManager.beginTransaction()");
                                    singleFragmentNavigator2.c.a(singleFragmentNavigator2.a);
                                    singleFragmentNavigator2.a(forward, a2, backStackRecord);
                                    backStackRecord.a(singleFragmentNavigator2.a, a2);
                                    Screen screen2 = forward.a;
                                    Intrinsics.a((Object) screen2, "command.screen");
                                    backStackRecord.a(screen2.b);
                                    backStackRecord.b();
                                    LinkedList<String> linkedList2 = singleFragmentNavigator2.b;
                                    Screen screen3 = forward.a;
                                    Intrinsics.a((Object) screen3, "command.screen");
                                    linkedList2.add(screen3.b);
                                } else {
                                    throw null;
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type ru.covid19.core.presentation.navigation.FragmentScreen");
                            }
                        } else {
                            if (command instanceof Back) {
                                if (singleFragmentNavigator2.b.size() > 1) {
                                    FragmentManagerImpl fragmentManagerImpl3 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                    if (fragmentManagerImpl3 != null) {
                                        fragmentManagerImpl3.a((FragmentManagerImpl.h) new FragmentManagerImpl.i(null, -1, 0), false);
                                        singleFragmentNavigator2.b.pop();
                                        LinkedList<String> linkedList3 = singleFragmentNavigator2.b;
                                        if (linkedList3 != null) {
                                            String str = linkedList3.isEmpty() ? null : linkedList3.get(linkedList3.size() - 1);
                                        } else {
                                            Intrinsics.a("$this$lastOrNull");
                                            throw null;
                                        }
                                    } else {
                                        throw null;
                                    }
                                } else {
                                    singleFragmentNavigator2.d.b();
                                }
                            } else if (command instanceof Replace) {
                                Replace replace = (Replace) command;
                                Screen screen4 = replace.a;
                                if (screen4 != null) {
                                    Fragment a3 = singleFragmentNavigator2.a((BaseScreens2) screen4);
                                    if (singleFragmentNavigator2.b.size() > 0) {
                                        FragmentManagerImpl fragmentManagerImpl4 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                        if (fragmentManagerImpl4 != null) {
                                            fragmentManagerImpl4.a((FragmentManagerImpl.h) new FragmentManagerImpl.i(null, -1, 0), false);
                                            singleFragmentNavigator2.b.pop();
                                            FragmentManagerImpl fragmentManagerImpl5 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                            if (fragmentManagerImpl5 != null) {
                                                BackStackRecord backStackRecord2 = new BackStackRecord(fragmentManagerImpl5);
                                                Intrinsics.a((Object) backStackRecord2, "fragmentManager.beginTransaction()");
                                                singleFragmentNavigator2.c.a(singleFragmentNavigator2.a);
                                                singleFragmentNavigator2.a(replace, a3, backStackRecord2);
                                                backStackRecord2.a(singleFragmentNavigator2.a, a3);
                                                Screen screen5 = replace.a;
                                                Intrinsics.a((Object) screen5, "command.screen");
                                                backStackRecord2.a(screen5.b);
                                                backStackRecord2.b();
                                                LinkedList<String> linkedList4 = singleFragmentNavigator2.b;
                                                Screen screen6 = replace.a;
                                                Intrinsics.a((Object) screen6, "command.screen");
                                                linkedList4.add(screen6.b);
                                            } else {
                                                throw null;
                                            }
                                        } else {
                                            throw null;
                                        }
                                    } else {
                                        FragmentManagerImpl fragmentManagerImpl6 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                        if (fragmentManagerImpl6 != null) {
                                            BackStackRecord backStackRecord3 = new BackStackRecord(fragmentManagerImpl6);
                                            Intrinsics.a((Object) backStackRecord3, "fragmentManager.beginTransaction()");
                                            singleFragmentNavigator2.c.a(singleFragmentNavigator2.a);
                                            singleFragmentNavigator2.a(replace, a3, backStackRecord3);
                                            backStackRecord3.a(singleFragmentNavigator2.a, a3);
                                            Screen screen7 = replace.a;
                                            Intrinsics.a((Object) screen7, "command.screen");
                                            backStackRecord3.a(screen7.b);
                                            backStackRecord3.b();
                                            LinkedList<String> linkedList5 = singleFragmentNavigator2.b;
                                            Screen screen8 = replace.a;
                                            Intrinsics.a((Object) screen8, "command.screen");
                                            linkedList5.add(screen8.b);
                                        } else {
                                            throw null;
                                        }
                                    }
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type ru.covid19.core.presentation.navigation.FragmentScreen");
                                }
                            } else if (command instanceof BackTo) {
                                Screen screen9 = ((BackTo) command).a;
                                String str2 = screen9 != null ? screen9.b : null;
                                if (str2 == null) {
                                    singleFragmentNavigator2.a();
                                } else {
                                    int indexOf = singleFragmentNavigator2.b.indexOf(str2);
                                    int size2 = singleFragmentNavigator2.b.size();
                                    if (indexOf != -1) {
                                        int i4 = size2 - indexOf;
                                        for (int i5 = 1; i5 < i4; i5++) {
                                            singleFragmentNavigator2.b.pop();
                                        }
                                        FragmentManagerImpl fragmentManagerImpl7 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                        if (fragmentManagerImpl7 != null) {
                                            fragmentManagerImpl7.a((FragmentManagerImpl.h) new FragmentManagerImpl.i(str2, -1, 0), false);
                                        } else {
                                            throw null;
                                        }
                                    } else {
                                        singleFragmentNavigator2.a();
                                    }
                                }
                            } else if (command instanceof Commands7) {
                                Commands7 commands7 = (Commands7) command;
                                Fragment a4 = singleFragmentNavigator2.a(commands7.a);
                                if (a4 != null) {
                                    DialogFragment dialogFragment = (DialogFragment) a4;
                                    FragmentManagerImpl fragmentManagerImpl8 = (FragmentManagerImpl) singleFragmentNavigator2.c;
                                    if (fragmentManagerImpl8 != null) {
                                        BackStackRecord backStackRecord4 = new BackStackRecord(fragmentManagerImpl8);
                                        backStackRecord4.a(commands7.a.b);
                                        Intrinsics.a((Object) backStackRecord4, "fragmentManager.beginTra…command.screen.screenKey)");
                                        String str3 = commands7.a.b;
                                        dialogFragment.g0 = false;
                                        dialogFragment.h0 = true;
                                        backStackRecord4.a(0, dialogFragment, str3, 1);
                                        dialogFragment.f0 = false;
                                        dialogFragment.d0 = backStackRecord4.b();
                                        singleFragmentNavigator2.b.add(commands7.a.b);
                                    } else {
                                        throw null;
                                    }
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type androidx.fragment.app.DialogFragment");
                                }
                            } else {
                                continue;
                            }
                        }
                        i3++;
                    } else {
                        Intrinsics.a("command");
                        throw null;
                    }
                }
                return;
            }
            throw null;
        }
    }

    public SingleFragmentNavigator(j jVar, Functions<g> functions) {
        if (jVar == null) {
            Intrinsics.a("fragmentManager");
            throw null;
        } else if (functions != null) {
            this.c = jVar;
            this.d = functions;
            this.b = new LinkedList<>();
        } else {
            Intrinsics.a("exitAction");
            throw null;
        }
    }

    public void a(Command[] commandArr) {
        if (commandArr != null) {
            new Handler(Looper.getMainLooper()).post(new a(this, commandArr));
        } else {
            Intrinsics.a("commands");
            throw null;
        }
    }

    public final Fragment a(BaseScreens2 baseScreens2) {
        Fragment a2 = baseScreens2.a();
        Parcelable parcelable = baseScreens2.c;
        if (parcelable != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("EXTRA_SCREEN_DATA", parcelable);
            a2.f(bundle);
        }
        return a2;
    }

    public final void a(Command command, Fragment fragment, FragmentTransaction fragmentTransaction) {
        if (command == null) {
            Intrinsics.a("command");
            throw null;
        } else if (fragment == null) {
            Intrinsics.a("nextFragment");
            throw null;
        } else if (fragmentTransaction == null) {
            Intrinsics.a("fragmentTransaction");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
     arg types: [i.l.a.FragmentManagerImpl$i, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(int, boolean):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void */
    public final void a() {
        FragmentManagerImpl fragmentManagerImpl = (FragmentManagerImpl) this.c;
        if (fragmentManagerImpl != null) {
            fragmentManagerImpl.a((FragmentManagerImpl.h) new FragmentManagerImpl.i(null, -1, 1), false);
            this.b.clear();
            return;
        }
        throw null;
    }
}
