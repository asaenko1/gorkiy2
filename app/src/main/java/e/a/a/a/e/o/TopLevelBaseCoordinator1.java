package e.a.a.a.e.o;

import android.net.Uri;
import e.a.a.a.e.Commands;
import e.a.a.a.e.Commands0;
import e.a.a.a.e.Commands2;
import e.a.a.a.e.Commands8;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.a.e.s.ActivityRouter;
import e.b.a.CommandBuffer;
import e.b.a.Navigator;
import e.b.a.h.Command;
import l.b.Observable;
import l.b.t.Consumer;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableCreate;
import l.b.u.e.c.ObservableDoOnLifecycle;
import n.n.c.Intrinsics;

/* compiled from: TopLevelBaseCoordinator.kt */
public abstract class TopLevelBaseCoordinator1 implements ITopLevelBaseCoordinator {
    public final AppCiceroneHolder a;

    public TopLevelBaseCoordinator1(AppCiceroneHolder appCiceroneHolder) {
        if (appCiceroneHolder != null) {
            this.a = appCiceroneHolder;
        } else {
            Intrinsics.a("appCiceroneHolder");
            throw null;
        }
    }

    public void a(String str, Object obj) {
        if (str != null) {
            ActivityRouter activityRouter = this.a.b;
            if (activityRouter != null) {
                activityRouter.a.a(new Command[]{new Commands8(str, obj)});
                return;
            }
            throw null;
        }
        Intrinsics.a("message");
        throw null;
    }

    public void b() {
        ActivityRouter activityRouter = this.a.b;
        if (activityRouter != null) {
            activityRouter.a.a(new Command[]{Commands.a});
            return;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.o.TopLevelBaseCoordinator, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<java.lang.Object>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.o.TopLevelBaseCoordinator0, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservableDoOnLifecycle, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<Uri> c() {
        TopLevelBaseCoordinator topLevelBaseCoordinator = new TopLevelBaseCoordinator(this, 10);
        ObjectHelper.a((Object) topLevelBaseCoordinator, "source is null");
        ObservableCreate observableCreate = new ObservableCreate(topLevelBaseCoordinator);
        TopLevelBaseCoordinator0 topLevelBaseCoordinator0 = new TopLevelBaseCoordinator0(this, 10);
        Consumer<Object> consumer = Functions.d;
        ObjectHelper.a((Object) consumer, "onSubscribe is null");
        ObjectHelper.a((Object) topLevelBaseCoordinator0, "onDispose is null");
        ObservableDoOnLifecycle observableDoOnLifecycle = new ObservableDoOnLifecycle(observableCreate, consumer, topLevelBaseCoordinator0);
        Intrinsics.a((Object) observableDoOnLifecycle, "Observable.create<T> { s…tener(code)\n            }");
        return observableDoOnLifecycle;
    }

    public void a(Uri uri) {
        if (uri != null) {
            ActivityRouter activityRouter = this.a.b;
            if (activityRouter != null) {
                activityRouter.a.a(new Command[]{new Commands2(uri)});
                return;
            }
            throw null;
        }
        Intrinsics.a("uri");
        throw null;
    }

    public void a(boolean z) {
        ActivityRouter activityRouter = this.a.b;
        if (activityRouter != null) {
            Command[] commandArr = {new Commands0(z)};
            CommandBuffer commandBuffer = activityRouter.a;
            Navigator navigator = commandBuffer.a;
            if (navigator != null) {
                navigator.a(commandArr);
            } else {
                commandBuffer.b.add(commandArr);
            }
        } else {
            throw null;
        }
    }
}
