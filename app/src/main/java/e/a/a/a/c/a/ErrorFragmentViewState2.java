package e.a.a.a.c.a;

import com.crashlytics.android.core.CrashlyticsController;
import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ErrorFragmentViewState.kt */
public abstract class ErrorFragmentViewState2 extends BaseViewState1 {

    /* compiled from: ErrorFragmentViewState.kt */
    public static final class a extends ErrorFragmentViewState2 {
        public final ErrorFragmentViewState a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ErrorFragmentViewState errorFragmentViewState) {
            super(null);
            if (errorFragmentViewState != null) {
                this.a = errorFragmentViewState;
                return;
            }
            Intrinsics.a(CrashlyticsController.EVENT_TYPE_LOGGED);
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            ErrorFragmentViewState errorFragmentViewState = this.a;
            if (errorFragmentViewState != null) {
                return errorFragmentViewState.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitResult(error=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ ErrorFragmentViewState2(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
