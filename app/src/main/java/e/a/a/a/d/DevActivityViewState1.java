package e.a.a.a.d;

import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: DevActivityViewState.kt */
public abstract class DevActivityViewState1 extends BaseViewState1 {

    /* compiled from: DevActivityViewState.kt */
    public static final class a extends DevActivityViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("value");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("ChangeEndpointResult(value="), this.a, ")");
        }
    }

    /* compiled from: DevActivityViewState.kt */
    public static final class b extends DevActivityViewState1 {
        public final boolean a;

        public b(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && this.a == ((b) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("ToggleSecureActivityResult(toggled="), this.a, ")");
        }
    }

    public /* synthetic */ DevActivityViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
