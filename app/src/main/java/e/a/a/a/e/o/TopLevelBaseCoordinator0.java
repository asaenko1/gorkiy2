package e.a.a.a.e.o;

import l.b.t.Action;

/* compiled from: TopLevelBaseCoordinator.kt */
public final class TopLevelBaseCoordinator0 implements Action {
    public final /* synthetic */ TopLevelBaseCoordinator1 a;
    public final /* synthetic */ int b;

    public TopLevelBaseCoordinator0(TopLevelBaseCoordinator1 topLevelBaseCoordinator1, int i2) {
        this.a = topLevelBaseCoordinator1;
        this.b = i2;
    }

    public final void run() {
        TopLevelBaseCoordinator1 topLevelBaseCoordinator1 = this.a;
        topLevelBaseCoordinator1.a.b.b.remove(this.b);
    }
}
