package e.a.a.j.b;

import l.b.Completable;
import l.b.Single;

/* compiled from: IDevMenuRepository.kt */
public interface IDevMenuRepository0 {
    Completable a();

    Completable a(IDevMenuRepository iDevMenuRepository);

    void a(boolean z);

    boolean b();

    String c();

    String d();

    Single<a> e();

    String f();
}
