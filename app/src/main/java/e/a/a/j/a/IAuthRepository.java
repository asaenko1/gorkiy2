package e.a.a.j.a;

import android.os.Build;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.core.MetaDataStore;
import e.a.a.g.a.Session;
import e.a.a.g.a.f.EpguAuthApiService;
import e.a.a.g.a.f.EsiaAuthApiService;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.k.RxExtensions2;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.b.i.a.ICacheStorage;
import j.c.d.Gson;
import java.util.concurrent.Callable;
import l.b.Completable;
import l.b.Single;
import l.b.SingleSource;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.d.SingleFlatMap;
import l.b.u.e.d.SingleResumeNext;
import l.b.w.Schedulers;
import n.Tuples;
import n.Unit;
import n.i.Collections;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.CreateSessionRequestBody;
import ru.covid19.core.data.network.model.GetTokensResponse;
import ru.covid19.core.data.network.model.SaveTokensRequestBody;
import ru.covid19.core.data.network.model.SessionResponse;
import ru.covid19.core.data.network.model.SignResponse;

/* compiled from: IAuthRepository.kt */
public final class IAuthRepository implements IAuthRepository0 {
    public final EpguAuthApiService a;
    public final EsiaAuthApiService b;
    public final IAuthPrefs c;
    public final IPrefsStorage d;

    /* renamed from: e  reason: collision with root package name */
    public final Session f649e;

    /* renamed from: f  reason: collision with root package name */
    public final Gson f650f;
    public final ICacheStorage g;

    /* compiled from: IAuthRepository.kt */
    public static final class a<T, R> implements Function<T, R> {
        public final /* synthetic */ IAuthRepository a;
        public final /* synthetic */ boolean b;

        public a(IAuthRepository iAuthRepository, boolean z) {
            this.a = iAuthRepository;
            this.b = z;
        }

        public Object a(Object obj) {
            SessionResponse sessionResponse = (SessionResponse) obj;
            if (sessionResponse != null) {
                IAuthRepository.a(this.a, sessionResponse, this.b);
                return sessionResponse;
            }
            Intrinsics.a("sessionResp");
            throw null;
        }
    }

    /* compiled from: IAuthRepository.kt */
    public static final class b<T, R> implements Function<T, SingleSource<? extends R>> {
        public final /* synthetic */ IAuthRepository a;
        public final /* synthetic */ String b;
        public final /* synthetic */ String c;

        public b(IAuthRepository iAuthRepository, String str, String str2) {
            this.a = iAuthRepository;
            this.b = str;
            this.c = str2;
        }

        public Object a(Object obj) {
            SignResponse signResponse = (SignResponse) obj;
            if (signResponse != null) {
                return this.a.b.a(Collections.a(new Tuples("scope", "http://esia.gosuslugi.ru/usr_inf?mode=w"), new Tuples("client_id", "PGU"), new Tuples("grant_type", "password"), new Tuples("state", signResponse.getStateMarker()), new Tuples("client_secret", signResponse.getSecret()), new Tuples("timestamp", signResponse.getTime()), new Tuples("token_type", "Bearer"), new Tuples("instanceId", this.a.c.c()), new Tuples("terminal_name", Build.MODEL), new Tuples("terminal_type", "Android"), new Tuples("username", this.b), new Tuples("password", this.c)));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: IAuthRepository.kt */
    public static final class c<T, R> implements Function<T, SingleSource<? extends R>> {
        public final /* synthetic */ IAuthRepository a;
        public final /* synthetic */ boolean b;

        public c(IAuthRepository iAuthRepository, boolean z) {
            this.a = iAuthRepository;
            this.b = z;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Single<R>, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            GetTokensResponse getTokensResponse = (GetTokensResponse) obj;
            if (getTokensResponse != null) {
                IAuthRepository iAuthRepository = this.a;
                boolean z = this.b;
                iAuthRepository.f649e.a(getTokensResponse.getAccess_token());
                Session session = iAuthRepository.f649e;
                String oid = getTokensResponse.getOid();
                if (oid != null) {
                    session.b = oid;
                    iAuthRepository.c.c(iAuthRepository.f649e.b);
                    EpguAuthApiService epguAuthApiService = iAuthRepository.a;
                    int expires_in = getTokensResponse.getExpires_in();
                    String str = Build.MODEL;
                    Intrinsics.a((Object) str, "Build.MODEL");
                    Single<R> a2 = epguAuthApiService.a(new SaveTokensRequestBody(expires_in, str, iAuthRepository.c.d(), getTokensResponse.getAccess_token(), "Android", getTokensResponse.getOid(), getTokensResponse.getRefresh_token(), iAuthRepository.c.c())).a((Function) new IAuthRepository1(iAuthRepository, z));
                    Intrinsics.a((Object) a2, "epguAuthApiService.saveT…    sessionResp\n        }");
                    return a2;
                }
                Intrinsics.a("<set-?>");
                throw null;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: IAuthRepository.kt */
    public static final class d<V> implements Callable<Object> {
        public final /* synthetic */ IAuthRepository b;

        public d(IAuthRepository iAuthRepository) {
            this.b = iAuthRepository;
        }

        public Object call() {
            this.b.d.a();
            this.b.g.a();
            Session session = this.b.f649e;
            session.a = "";
            session.b = "";
            session.c.clear();
            session.d = "";
            return Unit.a;
        }
    }

    public IAuthRepository(EpguAuthApiService epguAuthApiService, EsiaAuthApiService esiaAuthApiService, IAuthPrefs iAuthPrefs, IPrefsStorage iPrefsStorage, Session session, Gson gson, ICacheStorage iCacheStorage) {
        if (epguAuthApiService == null) {
            Intrinsics.a("epguAuthApiService");
            throw null;
        } else if (esiaAuthApiService == null) {
            Intrinsics.a("esiaAuthApiService");
            throw null;
        } else if (iAuthPrefs == null) {
            Intrinsics.a("authPrefs");
            throw null;
        } else if (iPrefsStorage == null) {
            Intrinsics.a("prefsStorage");
            throw null;
        } else if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (gson == null) {
            Intrinsics.a("gson");
            throw null;
        } else if (iCacheStorage != null) {
            this.a = epguAuthApiService;
            this.b = esiaAuthApiService;
            this.c = iAuthPrefs;
            this.d = iPrefsStorage;
            this.f649e = session;
            this.f650f = gson;
            this.g = iCacheStorage;
        } else {
            Intrinsics.a("cacheStorage");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.j.a.IAuthRepository$b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.j.a.IAuthRepository$c, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.d.SingleFlatMap, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.k.RxExtensions2, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.d.SingleResumeNext, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<SessionResponse> a(String str, String str2, boolean z) {
        if (str == null) {
            Intrinsics.a(LoginEvent.TYPE);
            throw null;
        } else if (str2 != null) {
            Single<SignResponse> a2 = this.a.a(Collections.a(new Tuples("scope", "http://esia.gosuslugi.ru/usr_inf?mode=w"), new Tuples("clientId", "PGU")));
            b bVar = new b(this, str, str2);
            if (a2 != null) {
                ObjectHelper.a((Object) bVar, "mapper is null");
                SingleFlatMap singleFlatMap = new SingleFlatMap(a2, bVar);
                c cVar = new c(this, z);
                ObjectHelper.a((Object) cVar, "mapper is null");
                SingleFlatMap singleFlatMap2 = new SingleFlatMap(singleFlatMap, cVar);
                Intrinsics.a((Object) singleFlatMap2, "epguAuthApiService.sign(… useDefaultPin)\n        }");
                Gson gson = this.f650f;
                if (gson != null) {
                    RxExtensions2 rxExtensions2 = new RxExtensions2(gson);
                    ObjectHelper.a((Object) rxExtensions2, "resumeFunctionInCaseOfError is null");
                    SingleResumeNext singleResumeNext = new SingleResumeNext(singleFlatMap2, rxExtensions2);
                    Intrinsics.a((Object) singleResumeNext, "this.onErrorResumeNext {…  SingleError { t }\n    }");
                    return singleResumeNext;
                }
                Intrinsics.a("gson");
                throw null;
            }
            throw null;
        } else {
            Intrinsics.a("password");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Single<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<SessionResponse> b(String str, String str2, boolean z) {
        if (str != null) {
            this.f649e.b = str;
            EpguAuthApiService epguAuthApiService = this.a;
            IAuthPrefs iAuthPrefs = this.c;
            if (z) {
                str2 = "0000";
            } else if (str2 == null) {
                Intrinsics.a();
                throw null;
            }
            String b2 = iAuthPrefs.b(str2);
            String d2 = this.c.d();
            String str3 = Build.DEVICE;
            Intrinsics.a((Object) str3, "Build.DEVICE");
            Single<R> a2 = epguAuthApiService.a(new CreateSessionRequestBody(str, b2, d2, str3, "Android")).a((Function) new a(this, z));
            Intrinsics.a((Object) a2, "epguAuthApiService.creat…sessionResp\n            }");
            return a2;
        }
        Intrinsics.a(MetaDataStore.KEY_USER_ID);
        throw null;
    }

    public static final /* synthetic */ void a(IAuthRepository iAuthRepository, SessionResponse sessionResponse, boolean z) {
        Session session = iAuthRepository.f649e;
        String sessionId = sessionResponse.getSessionId();
        if (sessionId != null) {
            session.d = sessionId;
            iAuthRepository.f649e.a(sessionResponse.getAccessToken());
            if (z) {
                iAuthRepository.c.a(sessionResponse.getAccessToken());
                return;
            }
            return;
        }
        Intrinsics.a("<set-?>");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Completable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Completable a() {
        Completable a2 = Completable.a(new d(this)).a(Schedulers.b);
        Intrinsics.a((Object) a2, "Completable.fromCallable…scribeOn(Schedulers.io())");
        return a2;
    }
}
