package e.a.a.j.b;

import e.c.a.c.a.a.IPrefsStorage;
import java.util.concurrent.Callable;
import kotlin.NoWhenBranchMatchedException;
import l.b.Completable;
import l.b.Single;
import l.b.SingleOnSubscribe;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.d.SingleCreate;
import m.a.Provider;
import n.Unit;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: DevMenuRepository.kt */
public final class DevMenuRepository implements IDevMenuRepository0 {
    public final IPrefsStorage a;
    public final Provider<e.a.a.g.a.f.b> b;

    /* compiled from: DevMenuRepository.kt */
    public static final class a<T> implements SingleOnSubscribe<T> {
        public final /* synthetic */ DevMenuRepository a;

        public a(DevMenuRepository devMenuRepository) {
            this.a = devMenuRepository;
        }

        /* JADX WARN: Type inference failed for: r0v22, types: [java.lang.Boolean] */
        /* JADX WARN: Type inference failed for: r0v25, types: [java.lang.Long] */
        /* JADX WARN: Type inference failed for: r0v28, types: [java.lang.Float] */
        /* JADX WARN: Type inference failed for: r0v31, types: [java.lang.Integer] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(l.b.SingleEmitter<e.a.a.j.b.a> r6) {
            /*
                r5 = this;
                java.lang.Class<java.lang.String> r0 = java.lang.String.class
                r1 = 0
                if (r6 == 0) goto L_0x00be
                e.a.a.j.b.DevMenuRepository r2 = r5.a
                e.c.a.c.a.a.IPrefsStorage r2 = r2.a
                java.lang.String r3 = "API_CONFIG_PREFS"
                android.content.SharedPreferences r2 = r2.a(r3)
                java.lang.String r3 = "API_CONFIG_PREFS_CONTOUR"
                boolean r4 = r2.contains(r3)
                if (r4 != 0) goto L_0x0019
                goto L_0x009d
            L_0x0019:
                n.p.KClass r1 = n.n.c.Reflection.a(r0)
                n.p.KClass r0 = n.n.c.Reflection.a(r0)
                boolean r0 = n.n.c.Intrinsics.a(r1, r0)
                if (r0 == 0) goto L_0x0038
                java.lang.String r0 = ""
                java.lang.String r1 = r2.getString(r3, r0)
                if (r1 == 0) goto L_0x0030
                goto L_0x009d
            L_0x0030:
                kotlin.TypeCastException r6 = new kotlin.TypeCastException
                java.lang.String r0 = "null cannot be cast to non-null type kotlin.String"
                r6.<init>(r0)
                throw r6
            L_0x0038:
                java.lang.Class r0 = java.lang.Integer.TYPE
                n.p.KClass r0 = n.n.c.Reflection.a(r0)
                boolean r0 = n.n.c.Intrinsics.a(r1, r0)
                if (r0 == 0) goto L_0x0051
                r0 = -1
                int r0 = r2.getInt(r3, r0)
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                r1 = r0
                java.lang.String r1 = (java.lang.String) r1
                goto L_0x009d
            L_0x0051:
                java.lang.Class r0 = java.lang.Float.TYPE
                n.p.KClass r0 = n.n.c.Reflection.a(r0)
                boolean r0 = n.n.c.Intrinsics.a(r1, r0)
                if (r0 == 0) goto L_0x006b
                r0 = -1082130432(0xffffffffbf800000, float:-1.0)
                float r0 = r2.getFloat(r3, r0)
                java.lang.Float r0 = java.lang.Float.valueOf(r0)
                r1 = r0
                java.lang.String r1 = (java.lang.String) r1
                goto L_0x009d
            L_0x006b:
                java.lang.Class r0 = java.lang.Long.TYPE
                n.p.KClass r0 = n.n.c.Reflection.a(r0)
                boolean r0 = n.n.c.Intrinsics.a(r1, r0)
                if (r0 == 0) goto L_0x0085
                r0 = -1
                long r0 = r2.getLong(r3, r0)
                java.lang.Long r0 = java.lang.Long.valueOf(r0)
                r1 = r0
                java.lang.String r1 = (java.lang.String) r1
                goto L_0x009d
            L_0x0085:
                java.lang.Class r0 = java.lang.Boolean.TYPE
                n.p.KClass r0 = n.n.c.Reflection.a(r0)
                boolean r0 = n.n.c.Intrinsics.a(r1, r0)
                if (r0 == 0) goto L_0x00b6
                r0 = 0
                boolean r0 = r2.getBoolean(r3, r0)
                java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
                r1 = r0
                java.lang.String r1 = (java.lang.String) r1
            L_0x009d:
                if (r1 == 0) goto L_0x00a6
                e.a.a.j.b.IDevMenuRepository r0 = e.a.a.j.b.IDevMenuRepository.valueOf(r1)     // Catch:{ Exception -> 0x00af }
                if (r0 == 0) goto L_0x00a6
                goto L_0x00a8
            L_0x00a6:
                e.a.a.j.b.IDevMenuRepository r0 = e.a.a.j.b.IDevMenuRepository.Devfed     // Catch:{ Exception -> 0x00af }
            L_0x00a8:
                r1 = r6
                l.b.u.e.d.SingleCreate$a r1 = (l.b.u.e.d.SingleCreate.a) r1
                r1.a(r0)     // Catch:{ Exception -> 0x00af }
                goto L_0x00b5
            L_0x00af:
                r0 = move-exception
                l.b.u.e.d.SingleCreate$a r6 = (l.b.u.e.d.SingleCreate.a) r6
                r6.a(r0)
            L_0x00b5:
                return
            L_0x00b6:
                java.lang.UnsupportedOperationException r6 = new java.lang.UnsupportedOperationException
                java.lang.String r0 = "Not acceptable type"
                r6.<init>(r0)
                throw r6
            L_0x00be:
                java.lang.String r6 = "it"
                n.n.c.Intrinsics.a(r6)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: e.a.a.j.b.DevMenuRepository.a.a(l.b.SingleEmitter):void");
        }
    }

    /* compiled from: DevMenuRepository.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            IDevMenuRepository iDevMenuRepository = (IDevMenuRepository) obj;
            if (iDevMenuRepository != null) {
                switch (iDevMenuRepository.ordinal()) {
                    case 0:
                        return "https://lk.gosuslugi.ru/";
                    case 1:
                        return "https://lk-dev.test.gosuslugi.ru/";
                    case 2:
                        return "https://svcdev-betalk.test.gosuslugi.ru/";
                    case 3:
                        return "https://pgu-uat-betalk.test.gosuslugi.ru/";
                    case 4:
                        return "https://pgu-express-betalk.test.gosuslugi.ru/";
                    case 5:
                    case 6:
                        return "https://betarc-lk.test.gosuslugi.ru/";
                    default:
                        throw new NoWhenBranchMatchedException();
                }
            } else {
                Intrinsics.a("it");
                throw null;
            }
        }
    }

    /* compiled from: DevMenuRepository.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            IDevMenuRepository iDevMenuRepository = (IDevMenuRepository) obj;
            if (iDevMenuRepository != null) {
                switch (iDevMenuRepository.ordinal()) {
                    case 0:
                    case 6:
                        return "https://www.gosuslugi.ru/";
                    case 1:
                        return "https://pgu-devfed.test.gosuslugi.ru/";
                    case 2:
                        return "https://svcdev-pgu.test.gosuslugi.ru/";
                    case 3:
                        return "https://pgu-uat-fed.test.gosuslugi.ru/";
                    case 4:
                        return "https://pgu-express-fed.test.gosuslugi.ru/";
                    case 5:
                        return "https://www-demo1.gosuslugi.ru/";
                    default:
                        throw new NoWhenBranchMatchedException();
                }
            } else {
                Intrinsics.a("it");
                throw null;
            }
        }
    }

    /* compiled from: DevMenuRepository.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            IDevMenuRepository iDevMenuRepository = (IDevMenuRepository) obj;
            if (iDevMenuRepository != null) {
                int ordinal = iDevMenuRepository.ordinal();
                if (ordinal != 0) {
                    if (ordinal == 4) {
                        return "https://esia-uat.test.gosuslugi.ru/";
                    }
                    if (ordinal == 5) {
                        return "https://esia-portal2.test.gosuslugi.ru/";
                    }
                    if (ordinal != 6) {
                        return "https://esia-portal1.test.gosuslugi.ru/";
                    }
                }
                return "https://esia.gosuslugi.ru/";
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevMenuRepository.kt */
    public static final class e<V> implements Callable<Object> {
        public final /* synthetic */ DevMenuRepository b;
        public final /* synthetic */ IDevMenuRepository c;

        public e(DevMenuRepository devMenuRepository, IDevMenuRepository iDevMenuRepository) {
            this.b = devMenuRepository;
            this.c = iDevMenuRepository;
        }

        public final Object call() {
            if (this.c != null) {
                Collections.a(this.b.a.a("API_CONFIG_PREFS"), "API_CONFIG_PREFS_CONTOUR", this.c.name());
            } else {
                this.b.a.b("API_CONFIG_PREFS");
            }
            return Unit.a;
        }
    }

    public DevMenuRepository(e.c.a.c.a.a.a aVar, Provider<e.a.a.g.a.f.b> provider) {
        if (aVar == null) {
            Intrinsics.a("prefs");
            throw null;
        } else if (provider != null) {
            this.a = aVar;
            this.b = provider;
        } else {
            Intrinsics.a("epguDevApiService");
            throw null;
        }
    }

    public Completable a() {
        return this.b.get().a();
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v8, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v10, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b() {
        /*
            r8 = this;
            e.c.a.c.a.a.IPrefsStorage r0 = r8.a
            java.lang.String r1 = "DEV_PREFS"
            android.content.SharedPreferences r0 = r0.a(r1)
            r1 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r1)
            java.lang.String r3 = "SCREEN_SECURITY_OPTION"
            boolean r4 = r0.contains(r3)
            r5 = 0
            if (r4 != 0) goto L_0x0018
            goto L_0x00fe
        L_0x0018:
            java.lang.Class<java.lang.Boolean> r4 = java.lang.Boolean.class
            n.p.KClass r4 = n.n.c.Reflection.a(r4)
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            java.lang.String r7 = "null cannot be cast to non-null type kotlin.Boolean"
            if (r6 == 0) goto L_0x0044
            boolean r4 = r2 instanceof java.lang.String
            if (r4 != 0) goto L_0x0031
            r2 = r5
        L_0x0031:
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r0 = r0.getString(r3, r2)
            if (r0 == 0) goto L_0x003e
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fe
        L_0x003e:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0044:
            java.lang.Class r6 = java.lang.Integer.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x006c
            boolean r4 = r2 instanceof java.lang.Integer
            if (r4 != 0) goto L_0x0055
            r2 = r5
        L_0x0055:
            java.lang.Integer r2 = (java.lang.Integer) r2
            if (r2 == 0) goto L_0x005e
            int r2 = r2.intValue()
            goto L_0x005f
        L_0x005e:
            r2 = -1
        L_0x005f:
            int r0 = r0.getInt(r3, r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fe
        L_0x006c:
            java.lang.Class r6 = java.lang.Float.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x0095
            boolean r4 = r2 instanceof java.lang.Float
            if (r4 != 0) goto L_0x007d
            r2 = r5
        L_0x007d:
            java.lang.Float r2 = (java.lang.Float) r2
            if (r2 == 0) goto L_0x0086
            float r2 = r2.floatValue()
            goto L_0x0088
        L_0x0086:
            r2 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x0088:
            float r0 = r0.getFloat(r3, r2)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fe
        L_0x0095:
            java.lang.Class r6 = java.lang.Long.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x00bd
            boolean r4 = r2 instanceof java.lang.Long
            if (r4 != 0) goto L_0x00a6
            r2 = r5
        L_0x00a6:
            java.lang.Long r2 = (java.lang.Long) r2
            if (r2 == 0) goto L_0x00af
            long r4 = r2.longValue()
            goto L_0x00b1
        L_0x00af:
            r4 = -1
        L_0x00b1:
            long r2 = r0.getLong(r3, r4)
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fe
        L_0x00bd:
            java.lang.Class r6 = java.lang.Boolean.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x00da
            if (r2 == 0) goto L_0x00d0
            boolean r2 = r2.booleanValue()
            goto L_0x00d1
        L_0x00d0:
            r2 = 0
        L_0x00d1:
            boolean r0 = r0.getBoolean(r3, r2)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            goto L_0x00fe
        L_0x00da:
            java.lang.Class<java.util.Set> r6 = java.util.Set.class
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r4 = n.n.c.Intrinsics.a(r4, r6)
            if (r4 == 0) goto L_0x010b
            boolean r4 = r2 instanceof java.util.Set
            if (r4 != 0) goto L_0x00eb
            r2 = r5
        L_0x00eb:
            java.util.Set r2 = (java.util.Set) r2
            if (r2 == 0) goto L_0x00f0
            goto L_0x00f5
        L_0x00f0:
            java.util.LinkedHashSet r2 = new java.util.LinkedHashSet
            r2.<init>()
        L_0x00f5:
            java.util.Set r0 = r0.getStringSet(r3, r2)
            if (r0 == 0) goto L_0x0105
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
        L_0x00fe:
            if (r5 == 0) goto L_0x0104
            boolean r1 = r5.booleanValue()
        L_0x0104:
            return r1
        L_0x0105:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x010b:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            java.lang.String r1 = "Not acceptable type"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.j.b.DevMenuRepository.b():boolean");
    }

    public String c() {
        try {
            return (String) e().a((Function) d.a).b();
        } catch (Exception unused) {
            return null;
        }
    }

    public String d() {
        try {
            return (String) e().a((Function) b.a).b();
        } catch (Exception unused) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.j.b.DevMenuRepository$a, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.d.SingleCreate, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<a> e() {
        a aVar = new a(this);
        ObjectHelper.a((Object) aVar, "source is null");
        SingleCreate singleCreate = new SingleCreate(aVar);
        Intrinsics.a((Object) singleCreate, "Single.create<ApiContour…)\n            }\n        }");
        return singleCreate;
    }

    public String f() {
        try {
            return (String) e().a((Function) c.a).b();
        } catch (Exception unused) {
            return null;
        }
    }

    public void a(boolean z) {
        Collections.a(this.a.a("DEV_PREFS"), "SCREEN_SECURITY_OPTION", Boolean.valueOf(z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Completable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Completable a(IDevMenuRepository iDevMenuRepository) {
        Completable a2 = Completable.a(new e(this, iDevMenuRepository));
        Intrinsics.a((Object) a2, "Completable.fromCallable…I_CONFIG_PREFS)\n        }");
        return a2;
    }
}
