package e.a.a.h;

import k.a.Factory;

public final class DeeplinkActivityVm_Factory implements Factory<a> {

    public static final class a {
        public static final DeeplinkActivityVm_Factory a = new DeeplinkActivityVm_Factory();
    }

    public Object get() {
        return new DeeplinkActivityVm();
    }
}
