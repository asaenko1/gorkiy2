package e.a.a.i.c;

import e.a.a.g.a.Session;
import e.a.a.g.a.d;
import e.a.a.g.a.f.EpguAuthApiService;
import e.a.a.g.a.f.EsiaAuthApiService;
import e.a.a.g.a.f.a;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.g.b.b.a.b;
import e.a.a.j.a.IAuthRepository;
import e.a.a.j.a.c;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.b.i.a.ICacheStorage;
import j.c.d.Gson;
import j.c.d.k;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

public final class AuthModule_ProvideAuthRepositoryFactory implements Factory<c> {
    public final AuthModule a;
    public final Provider<a> b;
    public final Provider<e.a.a.g.a.f.c> c;
    public final Provider<b> d;

    /* renamed from: e  reason: collision with root package name */
    public final Provider<e.c.a.c.a.a.a> f645e;

    /* renamed from: f  reason: collision with root package name */
    public final Provider<d> f646f;
    public final Provider<k> g;
    public final Provider<e.c.b.i.a.a> h;

    public AuthModule_ProvideAuthRepositoryFactory(h hVar, Provider<a> provider, Provider<e.a.a.g.a.f.c> provider2, Provider<b> provider3, Provider<e.c.a.c.a.a.a> provider4, Provider<d> provider5, Provider<k> provider6, Provider<e.c.b.i.a.a> provider7) {
        this.a = hVar;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.f645e = provider4;
        this.f646f = provider5;
        this.g = provider6;
        this.h = provider7;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.j.a.IAuthRepository, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        AuthModule authModule = this.a;
        EpguAuthApiService epguAuthApiService = this.b.get();
        EsiaAuthApiService esiaAuthApiService = this.c.get();
        IAuthPrefs iAuthPrefs = this.d.get();
        IPrefsStorage iPrefsStorage = this.f645e.get();
        Session session = this.f646f.get();
        Gson gson = this.g.get();
        ICacheStorage iCacheStorage = this.h.get();
        if (authModule == null) {
            throw null;
        } else if (epguAuthApiService == null) {
            Intrinsics.a("epguAuthApiService");
            throw null;
        } else if (esiaAuthApiService == null) {
            Intrinsics.a("esiaAuthApiService");
            throw null;
        } else if (iAuthPrefs == null) {
            Intrinsics.a("authPrefs");
            throw null;
        } else if (iPrefsStorage == null) {
            Intrinsics.a("prefsStorage");
            throw null;
        } else if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (gson == null) {
            Intrinsics.a("gson");
            throw null;
        } else if (iCacheStorage != null) {
            IAuthRepository iAuthRepository = new IAuthRepository(epguAuthApiService, esiaAuthApiService, iAuthPrefs, iPrefsStorage, session, gson, iCacheStorage);
            j.c.a.a.c.n.c.a((Object) iAuthRepository, "Cannot return null from a non-@Nullable @Provides method");
            return iAuthRepository;
        } else {
            Intrinsics.a("cacheStorage");
            throw null;
        }
    }
}
