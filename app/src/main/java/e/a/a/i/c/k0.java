package e.a.a.i.c;

import android.content.Context;
import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.a.g.f;
import e.c.b.RetrofitFactory;
import e.c.b.h;
import e.c.b.j.IApiConfigRepository0;
import e.c.b.j.c;
import e.c.b.k.IConnectionChecker;
import e.c.b.k.b;
import e.c.b.l.HttpHeaderConfigurator;
import e.c.b.l.a;
import e.c.b.m.InterceptorsConfigurator;
import j.c.d.Gson;
import j.c.d.k;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: NetworkModule_ProvideRetrofitFactory$core_prodReleaseFactory */
public final class k0 implements Factory<h> {
    public final NetworkModule a;
    public final Provider<c> b;
    public final Provider<Context> c;
    public final Provider<k> d;

    /* renamed from: e  reason: collision with root package name */
    public final Provider<b> f647e;

    /* renamed from: f  reason: collision with root package name */
    public final Provider<a> f648f;
    public final Provider<e.c.b.m.a> g;
    public final Provider<f> h;

    public k0(y yVar, Provider<c> provider, Provider<Context> provider2, Provider<k> provider3, Provider<b> provider4, Provider<a> provider5, Provider<e.c.b.m.a> provider6, Provider<f> provider7) {
        this.a = yVar;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.f647e = provider4;
        this.f648f = provider5;
        this.g = provider6;
        this.h = provider7;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.c.b.RetrofitFactory, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        NetworkModule networkModule = this.a;
        IApiConfigRepository0 iApiConfigRepository0 = this.b.get();
        Context context = this.c.get();
        Gson gson = this.d.get();
        IConnectionChecker iConnectionChecker = this.f647e.get();
        HttpHeaderConfigurator httpHeaderConfigurator = this.f648f.get();
        InterceptorsConfigurator interceptorsConfigurator = this.g.get();
        IApiConfig iApiConfig = this.h.get();
        if (networkModule == null) {
            throw null;
        } else if (iApiConfigRepository0 == null) {
            Intrinsics.a("apiConfigRepository");
            throw null;
        } else if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (gson == null) {
            Intrinsics.a("gson");
            throw null;
        } else if (iConnectionChecker == null) {
            Intrinsics.a("connectionChecker");
            throw null;
        } else if (httpHeaderConfigurator == null) {
            Intrinsics.a("headersConfigurator");
            throw null;
        } else if (interceptorsConfigurator == null) {
            Intrinsics.a("interceptorsConfigurator");
            throw null;
        } else if (iApiConfig != null) {
            RetrofitFactory retrofitFactory = new RetrofitFactory(iApiConfigRepository0, context, gson, iConnectionChecker, httpHeaderConfigurator, interceptorsConfigurator, null, iApiConfig.f(), 0, 320);
            j.c.a.a.c.n.c.a((Object) retrofitFactory, "Cannot return null from a non-@Nullable @Provides method");
            return retrofitFactory;
        } else {
            Intrinsics.a("apiConfig");
            throw null;
        }
    }
}
