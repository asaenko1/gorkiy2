package e.a.a.i.c.m0;

import e.a.a.j.b.DevMenuRepository;
import e.a.a.j.b.c;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.a.c.a.a.a;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: DevRepositoryModule_ProvideDevMenuRepository$core_prodReleaseFactory */
public final class b implements Factory<c> {
    public final DevRepositoryModule a;
    public final Provider<a> b;
    public final Provider<e.a.a.g.a.f.b> c;

    public b(a aVar, Provider<a> provider, Provider<e.a.a.g.a.f.b> provider2) {
        this.a = aVar;
        this.b = provider;
        this.c = provider2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.j.b.DevMenuRepository, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        DevRepositoryModule devRepositoryModule = this.a;
        IPrefsStorage iPrefsStorage = this.b.get();
        Provider<e.a.a.g.a.f.b> provider = this.c;
        if (devRepositoryModule == null) {
            throw null;
        } else if (iPrefsStorage == null) {
            Intrinsics.a("prefs");
            throw null;
        } else if (provider != null) {
            DevMenuRepository devMenuRepository = new DevMenuRepository(iPrefsStorage, provider);
            j.c.a.a.c.n.c.a((Object) devMenuRepository, "Cannot return null from a non-@Nullable @Provides method");
            return devMenuRepository;
        } else {
            Intrinsics.a("epguDevApiService");
            throw null;
        }
    }
}
