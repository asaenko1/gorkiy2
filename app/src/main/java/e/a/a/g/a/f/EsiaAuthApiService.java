package e.a.a.g.a.f;

import java.util.Map;
import l.b.Single;
import r.l0.FieldMap;
import r.l0.FormUrlEncoded;
import r.l0.POST;
import ru.covid19.core.data.network.model.GetTokensResponse;

/* compiled from: EsiaAuthApiService.kt */
public interface EsiaAuthApiService {
    @POST("aas/oauth2/te")
    @FormUrlEncoded
    Single<GetTokensResponse> a(@FieldMap Map<String, String> map);
}
