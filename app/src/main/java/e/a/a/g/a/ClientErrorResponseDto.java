package e.a.a.g.a;

import j.c.d.a0.SerializedName;
import n.n.c.Intrinsics;

/* compiled from: ClientErrorResponseDto.kt */
public final class ClientErrorResponseDto {
    public final String a;
    @SerializedName("error_description")
    public final String b;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ClientErrorResponseDto)) {
            return false;
        }
        ClientErrorResponseDto clientErrorResponseDto = (ClientErrorResponseDto) obj;
        return Intrinsics.a(null, clientErrorResponseDto.a) && Intrinsics.a(null, clientErrorResponseDto.b);
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        return "ClientErrorResponseDto(error=null, message=null)";
    }
}
