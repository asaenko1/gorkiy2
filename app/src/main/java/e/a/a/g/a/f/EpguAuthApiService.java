package e.a.a.g.a.f;

import java.util.Map;
import l.b.Single;
import r.l0.Body;
import r.l0.POST;
import ru.covid19.core.data.network.model.CreateSessionRequestBody;
import ru.covid19.core.data.network.model.SaveTokensRequestBody;
import ru.covid19.core.data.network.model.SessionResponse;
import ru.covid19.core.data.network.model.SignResponse;

/* compiled from: EpguAuthApiService.kt */
public interface EpguAuthApiService {
    @POST("auth-provider/mobile/sign")
    Single<SignResponse> a(@Body Map<String, String> map);

    @POST("auth-provider/mobile/v2/createSession")
    Single<SessionResponse> a(@Body CreateSessionRequestBody createSessionRequestBody);

    @POST("auth-provider/mobile/saveToken")
    Single<SessionResponse> a(@Body SaveTokensRequestBody saveTokensRequestBody);
}
