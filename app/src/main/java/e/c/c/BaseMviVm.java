package e.c.c;

import e.c.c.BaseMviVm0;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableFromUnsafeSource;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: BaseMviVm.kt */
public final class BaseMviVm {
    public static final BaseMviVm0.a<n> a = new BaseMviVm0.a<>(BaseViewState5.a);

    /* compiled from: BaseMviVm.kt */
    public static final class a<T, R> implements Function<T, R> {
        public final /* synthetic */ Functions0 a;

        public a(Functions0 functions0) {
            this.a = functions0;
        }

        public Object a(Object obj) {
            BaseViewState0 baseViewState0 = (BaseViewState0) obj;
            if (baseViewState0 != null) {
                this.a.a(baseViewState0);
                return BaseMviVm.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T extends b> Observable<BaseMviVm0<? extends k>> a(Observable<T> observable, Functions0<? super T, g> functions0) {
        if (observable == null) {
            Intrinsics.a("$this$ignoreResult");
            throw null;
        } else if (functions0 != null) {
            Observable<R> c = observable.c((Function) new a(functions0));
            Intrinsics.a((Object) c, "this.map {\n        actio…)\n        IgnoreLce\n    }");
            return c;
        } else {
            Intrinsics.a("action");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.ObservableSource, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T> Observable<BaseMviVm0<? extends k>> b(Observable<T> observable, Functions0<? super Observable<T>, ? extends Observable<BaseMviVm0<? extends k>>> functions0) {
        Observable<BaseMviVm0<? extends k>> observable2;
        if (observable == null) {
            Intrinsics.a("$this$processWith");
            throw null;
        } else if (functions0 == null) {
            Intrinsics.a("transformer");
            throw null;
        } else if (observable != null) {
            ObservableSource observableSource = (ObservableSource) functions0.a(observable);
            ObjectHelper.a((Object) observableSource, "source is null");
            if (observableSource instanceof Observable) {
                observable2 = (Observable) observableSource;
            } else {
                observable2 = new ObservableFromUnsafeSource<>(observableSource);
            }
            Intrinsics.a((Object) observable2, "this.compose(transformer)");
            return observable2;
        } else {
            Intrinsics.a("p0");
            throw null;
        }
    }
}
