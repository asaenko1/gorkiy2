package e.c.c;

import android.os.Parcel;
import android.os.Parcelable;
import n.n.c.Intrinsics;

/* compiled from: BaseViewState.kt */
public final class BaseViewState3 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public static final BaseViewState3 b = new BaseViewState3();

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel == null) {
                Intrinsics.a("in");
                throw null;
            } else if (parcel.readInt() != 0) {
                return BaseViewState3.b;
            } else {
                return null;
            }
        }

        public final Object[] newArray(int i2) {
            return new BaseViewState3[i2];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeInt(1);
        } else {
            Intrinsics.a("parcel");
            throw null;
        }
    }
}
