package e.c.a.c.a.b;

import android.content.Context;
import android.content.SharedPreferences;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import e.c.a.a.ICryptoManager;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.TypeCastException;
import n.i.Collections;
import n.i.Sets;
import n.i._Arrays;
import n.n.c.Intrinsics;
import n.n.c.TypeIntrinsics;
import n.n.c.t.KMarkers;
import n.o.Ranges0;
import n.q.Iterables;
import n.r.Charsets;
import n.r.Indent;

/* compiled from: LegacyEncryptedSharedPreference.kt */
public final class LegacyEncryptedSharedPreference implements SharedPreferences {
    public final SharedPreferences a;
    public final ICryptoManager b;

    /* compiled from: LegacyEncryptedSharedPreference.kt */
    public static final class a implements SharedPreferences.Editor {
        public final List<String> a;
        public final AtomicBoolean b;
        public final LegacyEncryptedSharedPreference c;
        public final SharedPreferences.Editor d;

        /* renamed from: e  reason: collision with root package name */
        public final ICryptoManager f712e;

        public a(LegacyEncryptedSharedPreference legacyEncryptedSharedPreference, SharedPreferences.Editor editor, ICryptoManager iCryptoManager) {
            if (legacyEncryptedSharedPreference == null) {
                Intrinsics.a("storageSharedPreferences");
                throw null;
            } else if (editor == null) {
                Intrinsics.a("storageEditor");
                throw null;
            } else if (iCryptoManager != null) {
                this.c = legacyEncryptedSharedPreference;
                this.d = editor;
                this.f712e = iCryptoManager;
                this.a = new ArrayList();
                this.b = new AtomicBoolean(false);
            } else {
                Intrinsics.a("cryptoManager");
                throw null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [byte[], java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final <T> void a(String str, T t2) {
            byte[] bArr;
            if (t2 == null) {
                this.d.putString(str, null);
                return;
            }
            if (t2 instanceof String) {
                bArr = ((String) t2).getBytes(Charsets.a);
                Intrinsics.a((Object) bArr, "(this as java.lang.String).getBytes(charset)");
            } else if (t2 instanceof Integer) {
                bArr = ByteBuffer.allocate(4).putInt(((Number) t2).intValue()).array();
            } else if (t2 instanceof Boolean) {
                String obj = t2.toString();
                Charset charset = Charsets.a;
                if (obj != null) {
                    bArr = obj.getBytes(charset);
                    Intrinsics.a((Object) bArr, "(this as java.lang.String).getBytes(charset)");
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else if (t2 instanceof Long) {
                bArr = ByteBuffer.allocate(8).putLong(((Number) t2).longValue()).array();
            } else if (t2 instanceof Float) {
                bArr = ByteBuffer.allocate(4).putFloat(((Number) t2).floatValue()).array();
            } else if (t2 instanceof Double) {
                bArr = ByteBuffer.allocate(8).putDouble(((Number) t2).doubleValue()).array();
            } else if (t2 instanceof Set) {
                String a2 = _Arrays.a((Set) t2, ",", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, LegacyEncryptedSharedPreference0.c, 30);
                Charset charset2 = Charsets.a;
                if (a2 != null) {
                    bArr = a2.getBytes(charset2);
                    Intrinsics.a((Object) bArr, "(this as java.lang.String).getBytes(charset)");
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new UnsupportedOperationException("Not acceptable type");
            }
            ICryptoManager iCryptoManager = this.f712e;
            Charset charset3 = Charsets.a;
            if (str != null) {
                byte[] bytes = str.getBytes(charset3);
                Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                String a3 = iCryptoManager.a(bytes);
                if (a3 != null) {
                    this.a.add(a3);
                    this.d.putString(a3, this.f712e.a(bArr));
                    return;
                }
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        public void apply() {
            this.d.apply();
        }

        public SharedPreferences.Editor clear() {
            this.b.set(true);
            return this;
        }

        public boolean commit() {
            if (this.b.getAndSet(false)) {
                Set<String> keySet = this.c.getAll().keySet();
                ArrayList<String> arrayList = new ArrayList<>();
                for (T next : keySet) {
                    if (!this.a.contains((String) next)) {
                        arrayList.add(next);
                    }
                }
                for (String remove : arrayList) {
                    this.d.remove(remove);
                }
            }
            try {
                return this.d.commit();
            } finally {
                this.a.clear();
            }
        }

        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            if (str != null) {
                a(str, Boolean.valueOf(z));
                return this;
            }
            Intrinsics.a("key");
            throw null;
        }

        public SharedPreferences.Editor putFloat(String str, float f2) {
            if (str != null) {
                a(str, Float.valueOf(f2));
                return this;
            }
            Intrinsics.a("key");
            throw null;
        }

        public SharedPreferences.Editor putInt(String str, int i2) {
            if (str != null) {
                a(str, Integer.valueOf(i2));
                return this;
            }
            Intrinsics.a("key");
            throw null;
        }

        public SharedPreferences.Editor putLong(String str, long j2) {
            if (str != null) {
                a(str, Long.valueOf(j2));
                return this;
            }
            Intrinsics.a("key");
            throw null;
        }

        public SharedPreferences.Editor putString(String str, String str2) {
            if (str != null) {
                a(str, str2);
                return this;
            }
            Intrinsics.a("key");
            throw null;
        }

        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            if (str != null) {
                a(str, set);
                return this;
            }
            Intrinsics.a("key");
            throw null;
        }

        public SharedPreferences.Editor remove(String str) {
            this.d.remove(this.c.a(str));
            List<String> list = this.a;
            if (list == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableCollection<T>");
            } else if (!(list instanceof KMarkers)) {
                list.remove(str);
                return this;
            } else {
                TypeIntrinsics.a(list, "kotlin.collections.MutableCollection");
                throw null;
            }
        }
    }

    public LegacyEncryptedSharedPreference(Context context, String str, ICryptoManager iCryptoManager) {
        if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (iCryptoManager != null) {
            this.b = iCryptoManager;
            this.a = context.getSharedPreferences(str, 0);
        } else {
            Intrinsics.a("cryptoManager");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final String a(String str) {
        byte[] bArr;
        ICryptoManager iCryptoManager = this.b;
        if (str != null) {
            bArr = str.getBytes(Charsets.a);
            Intrinsics.a((Object) bArr, "(this as java.lang.String).getBytes(charset)");
        } else {
            bArr = null;
        }
        return iCryptoManager.a(bArr);
    }

    public boolean contains(String str) {
        return this.a.contains(a(str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.SharedPreferences$Editor, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public SharedPreferences.Editor edit() {
        SharedPreferences.Editor edit = this.a.edit();
        Intrinsics.a((Object) edit, "sharedPreferences.edit()");
        return new a(this, edit, this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.SharedPreferences, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<java.lang.String, ?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Map<String, ?> getAll() {
        SharedPreferences sharedPreferences = this.a;
        Intrinsics.a((Object) sharedPreferences, "sharedPreferences");
        Map<String, ?> all = sharedPreferences.getAll();
        Intrinsics.a((Object) all, "sharedPreferences.all");
        return all;
    }

    /* JADX WARN: Type inference failed for: r5v9, types: [java.lang.Double] */
    /* JADX WARN: Type inference failed for: r5v10, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r5v12, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r5v14, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.ByteBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getBoolean(java.lang.String r5, boolean r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x00f1
            android.content.SharedPreferences r1 = r4.a
            java.lang.String r5 = r4.a(r5)
            java.lang.String r2 = ""
            java.lang.String r5 = r1.getString(r5, r2)
            e.c.a.a.ICryptoManager r1 = r4.b
            byte[] r5 = r1.a(r5)
            if (r5 == 0) goto L_0x00ea
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r5)
            java.lang.Class<java.lang.Boolean> r1 = java.lang.Boolean.class
            n.p.KClass r1 = n.n.c.Reflection.a(r1)
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0038
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            goto L_0x00ea
        L_0x0038:
            java.lang.Class r2 = java.lang.Integer.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r3 = "bytesWrapper"
            if (r2 == 0) goto L_0x0056
            n.n.c.Intrinsics.a(r0, r3)
            int r5 = r0.getInt()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r0 = r5
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            goto L_0x00ea
        L_0x0056:
            java.lang.Class r2 = java.lang.Float.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0072
            n.n.c.Intrinsics.a(r0, r3)
            float r5 = r0.getFloat()
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            r0 = r5
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            goto L_0x00ea
        L_0x0072:
            java.lang.Class r2 = java.lang.Long.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x008d
            n.n.c.Intrinsics.a(r0, r3)
            long r0 = r0.getLong()
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r0 = r5
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            goto L_0x00ea
        L_0x008d:
            java.lang.Class r2 = java.lang.Double.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x00a8
            n.n.c.Intrinsics.a(r0, r3)
            double r0 = r0.getDouble()
            java.lang.Double r5 = java.lang.Double.valueOf(r0)
            r0 = r5
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            goto L_0x00ea
        L_0x00a8:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00c4
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            boolean r5 = java.lang.Boolean.parseBoolean(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)
            goto L_0x00ea
        L_0x00c4:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00e2
            java.util.Set r5 = a(r4, r5)
            if (r5 == 0) goto L_0x00da
            r0 = r5
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            goto L_0x00ea
        L_0x00da:
            kotlin.TypeCastException r5 = new kotlin.TypeCastException
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.Boolean"
            r5.<init>(r6)
            throw r5
        L_0x00e2:
            java.lang.UnsupportedOperationException r5 = new java.lang.UnsupportedOperationException
            java.lang.String r6 = "Not acceptable type"
            r5.<init>(r6)
            throw r5
        L_0x00ea:
            if (r0 == 0) goto L_0x00f0
            boolean r6 = r0.booleanValue()
        L_0x00f0:
            return r6
        L_0x00f1:
            java.lang.String r5 = "key"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.a.c.a.b.LegacyEncryptedSharedPreference.getBoolean(java.lang.String, boolean):boolean");
    }

    /* JADX WARN: Type inference failed for: r5v9, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r5v10, types: [java.lang.Double] */
    /* JADX WARN: Type inference failed for: r5v11, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r5v14, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.ByteBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float getFloat(java.lang.String r5, float r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x00f1
            android.content.SharedPreferences r1 = r4.a
            java.lang.String r5 = r4.a(r5)
            java.lang.String r2 = ""
            java.lang.String r5 = r1.getString(r5, r2)
            e.c.a.a.ICryptoManager r1 = r4.b
            byte[] r5 = r1.a(r5)
            if (r5 == 0) goto L_0x00ea
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r5)
            java.lang.Class<java.lang.Float> r1 = java.lang.Float.class
            n.p.KClass r1 = n.n.c.Reflection.a(r1)
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0038
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            java.lang.Float r0 = (java.lang.Float) r0
            goto L_0x00ea
        L_0x0038:
            java.lang.Class r2 = java.lang.Integer.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r3 = "bytesWrapper"
            if (r2 == 0) goto L_0x0056
            n.n.c.Intrinsics.a(r0, r3)
            int r5 = r0.getInt()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r0 = r5
            java.lang.Float r0 = (java.lang.Float) r0
            goto L_0x00ea
        L_0x0056:
            java.lang.Class r2 = java.lang.Float.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x006f
            n.n.c.Intrinsics.a(r0, r3)
            float r5 = r0.getFloat()
            java.lang.Float r0 = java.lang.Float.valueOf(r5)
            goto L_0x00ea
        L_0x006f:
            java.lang.Class r2 = java.lang.Long.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x008a
            n.n.c.Intrinsics.a(r0, r3)
            long r0 = r0.getLong()
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r0 = r5
            java.lang.Float r0 = (java.lang.Float) r0
            goto L_0x00ea
        L_0x008a:
            java.lang.Class r2 = java.lang.Double.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x00a5
            n.n.c.Intrinsics.a(r0, r3)
            double r0 = r0.getDouble()
            java.lang.Double r5 = java.lang.Double.valueOf(r0)
            r0 = r5
            java.lang.Float r0 = (java.lang.Float) r0
            goto L_0x00ea
        L_0x00a5:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00c4
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            boolean r5 = java.lang.Boolean.parseBoolean(r0)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0 = r5
            java.lang.Float r0 = (java.lang.Float) r0
            goto L_0x00ea
        L_0x00c4:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00e2
            java.util.Set r5 = a(r4, r5)
            if (r5 == 0) goto L_0x00da
            r0 = r5
            java.lang.Float r0 = (java.lang.Float) r0
            goto L_0x00ea
        L_0x00da:
            kotlin.TypeCastException r5 = new kotlin.TypeCastException
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.Float"
            r5.<init>(r6)
            throw r5
        L_0x00e2:
            java.lang.UnsupportedOperationException r5 = new java.lang.UnsupportedOperationException
            java.lang.String r6 = "Not acceptable type"
            r5.<init>(r6)
            throw r5
        L_0x00ea:
            if (r0 == 0) goto L_0x00f0
            float r6 = r0.floatValue()
        L_0x00f0:
            return r6
        L_0x00f1:
            java.lang.String r5 = "key"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.a.c.a.b.LegacyEncryptedSharedPreference.getFloat(java.lang.String, float):float");
    }

    /* JADX WARN: Type inference failed for: r5v9, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r5v10, types: [java.lang.Double] */
    /* JADX WARN: Type inference failed for: r5v11, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r5v13, types: [java.lang.Float] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.ByteBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getInt(java.lang.String r5, int r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x00f1
            android.content.SharedPreferences r1 = r4.a
            java.lang.String r5 = r4.a(r5)
            java.lang.String r2 = ""
            java.lang.String r5 = r1.getString(r5, r2)
            e.c.a.a.ICryptoManager r1 = r4.b
            byte[] r5 = r1.a(r5)
            if (r5 == 0) goto L_0x00ea
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r5)
            java.lang.Class<java.lang.Integer> r1 = java.lang.Integer.class
            n.p.KClass r1 = n.n.c.Reflection.a(r1)
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0038
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x00ea
        L_0x0038:
            java.lang.Class r2 = java.lang.Integer.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r3 = "bytesWrapper"
            if (r2 == 0) goto L_0x0053
            n.n.c.Intrinsics.a(r0, r3)
            int r5 = r0.getInt()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            goto L_0x00ea
        L_0x0053:
            java.lang.Class r2 = java.lang.Float.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x006f
            n.n.c.Intrinsics.a(r0, r3)
            float r5 = r0.getFloat()
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            r0 = r5
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x00ea
        L_0x006f:
            java.lang.Class r2 = java.lang.Long.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x008a
            n.n.c.Intrinsics.a(r0, r3)
            long r0 = r0.getLong()
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r0 = r5
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x00ea
        L_0x008a:
            java.lang.Class r2 = java.lang.Double.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x00a5
            n.n.c.Intrinsics.a(r0, r3)
            double r0 = r0.getDouble()
            java.lang.Double r5 = java.lang.Double.valueOf(r0)
            r0 = r5
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x00ea
        L_0x00a5:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00c4
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            boolean r5 = java.lang.Boolean.parseBoolean(r0)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0 = r5
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x00ea
        L_0x00c4:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00e2
            java.util.Set r5 = a(r4, r5)
            if (r5 == 0) goto L_0x00da
            r0 = r5
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x00ea
        L_0x00da:
            kotlin.TypeCastException r5 = new kotlin.TypeCastException
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.Int"
            r5.<init>(r6)
            throw r5
        L_0x00e2:
            java.lang.UnsupportedOperationException r5 = new java.lang.UnsupportedOperationException
            java.lang.String r6 = "Not acceptable type"
            r5.<init>(r6)
            throw r5
        L_0x00ea:
            if (r0 == 0) goto L_0x00f0
            int r6 = r0.intValue()
        L_0x00f0:
            return r6
        L_0x00f1:
            java.lang.String r5 = "key"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.a.c.a.b.LegacyEncryptedSharedPreference.getInt(java.lang.String, int):int");
    }

    /* JADX WARN: Type inference failed for: r5v9, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r5v10, types: [java.lang.Double] */
    /* JADX WARN: Type inference failed for: r5v12, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r5v14, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.ByteBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long getLong(java.lang.String r5, long r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x00f1
            android.content.SharedPreferences r1 = r4.a
            java.lang.String r5 = r4.a(r5)
            java.lang.String r2 = ""
            java.lang.String r5 = r1.getString(r5, r2)
            e.c.a.a.ICryptoManager r1 = r4.b
            byte[] r5 = r1.a(r5)
            if (r5 == 0) goto L_0x00ea
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r5)
            java.lang.Class<java.lang.Long> r1 = java.lang.Long.class
            n.p.KClass r1 = n.n.c.Reflection.a(r1)
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0038
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x00ea
        L_0x0038:
            java.lang.Class r2 = java.lang.Integer.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r3 = "bytesWrapper"
            if (r2 == 0) goto L_0x0056
            n.n.c.Intrinsics.a(r0, r3)
            int r5 = r0.getInt()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r0 = r5
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x00ea
        L_0x0056:
            java.lang.Class r2 = java.lang.Float.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0072
            n.n.c.Intrinsics.a(r0, r3)
            float r5 = r0.getFloat()
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            r0 = r5
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x00ea
        L_0x0072:
            java.lang.Class r2 = java.lang.Long.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x008a
            n.n.c.Intrinsics.a(r0, r3)
            long r0 = r0.getLong()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            goto L_0x00ea
        L_0x008a:
            java.lang.Class r2 = java.lang.Double.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x00a5
            n.n.c.Intrinsics.a(r0, r3)
            double r0 = r0.getDouble()
            java.lang.Double r5 = java.lang.Double.valueOf(r0)
            r0 = r5
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x00ea
        L_0x00a5:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00c4
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            boolean r5 = java.lang.Boolean.parseBoolean(r0)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0 = r5
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x00ea
        L_0x00c4:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00e2
            java.util.Set r5 = a(r4, r5)
            if (r5 == 0) goto L_0x00da
            r0 = r5
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x00ea
        L_0x00da:
            kotlin.TypeCastException r5 = new kotlin.TypeCastException
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.Long"
            r5.<init>(r6)
            throw r5
        L_0x00e2:
            java.lang.UnsupportedOperationException r5 = new java.lang.UnsupportedOperationException
            java.lang.String r6 = "Not acceptable type"
            r5.<init>(r6)
            throw r5
        L_0x00ea:
            if (r0 == 0) goto L_0x00f0
            long r6 = r0.longValue()
        L_0x00f0:
            return r6
        L_0x00f1:
            java.lang.String r5 = "key"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.a.c.a.b.LegacyEncryptedSharedPreference.getLong(java.lang.String, long):long");
    }

    /* JADX WARN: Type inference failed for: r6v7, types: [java.util.Set] */
    /* JADX WARN: Type inference failed for: r6v9, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r6v10, types: [java.lang.Double] */
    /* JADX WARN: Type inference failed for: r6v11, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r6v13, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r6v15, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.ByteBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getString(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r1 = 0
            if (r6 == 0) goto L_0x00ea
            android.content.SharedPreferences r2 = r5.a
            java.lang.String r6 = r5.a(r6)
            java.lang.String r3 = ""
            java.lang.String r6 = r2.getString(r6, r3)
            e.c.a.a.ICryptoManager r2 = r5.b
            byte[] r6 = r2.a(r6)
            if (r6 == 0) goto L_0x00df
            java.nio.ByteBuffer r2 = java.nio.ByteBuffer.wrap(r6)
            n.p.KClass r3 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0034
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r2 = n.r.Charsets.a
            r0.<init>(r6, r2)
            goto L_0x00e0
        L_0x0034:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            java.lang.String r4 = "bytesWrapper"
            if (r0 == 0) goto L_0x0052
            n.n.c.Intrinsics.a(r2, r4)
            int r6 = r2.getInt()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x00e0
        L_0x0052:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x006e
            n.n.c.Intrinsics.a(r2, r4)
            float r6 = r2.getFloat()
            java.lang.Float r6 = java.lang.Float.valueOf(r6)
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x00e0
        L_0x006e:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0089
            n.n.c.Intrinsics.a(r2, r4)
            long r2 = r2.getLong()
            java.lang.Long r6 = java.lang.Long.valueOf(r2)
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x00e0
        L_0x0089:
            java.lang.Class r0 = java.lang.Double.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00a4
            n.n.c.Intrinsics.a(r2, r4)
            double r2 = r2.getDouble()
            java.lang.Double r6 = java.lang.Double.valueOf(r2)
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x00e0
        L_0x00a4:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00c3
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r2 = n.r.Charsets.a
            r0.<init>(r6, r2)
            boolean r6 = java.lang.Boolean.parseBoolean(r0)
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x00e0
        L_0x00c3:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00d7
            java.util.Set r6 = a(r5, r6)
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x00e0
        L_0x00d7:
            java.lang.UnsupportedOperationException r6 = new java.lang.UnsupportedOperationException
            java.lang.String r7 = "Not acceptable type"
            r6.<init>(r7)
            throw r6
        L_0x00df:
            r0 = r1
        L_0x00e0:
            boolean r6 = r0 instanceof java.lang.String
            if (r6 != 0) goto L_0x00e5
            goto L_0x00e6
        L_0x00e5:
            r1 = r0
        L_0x00e6:
            if (r1 == 0) goto L_0x00e9
            r7 = r1
        L_0x00e9:
            return r7
        L_0x00ea:
            java.lang.String r6 = "key"
            n.n.c.Intrinsics.a(r6)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.a.c.a.b.LegacyEncryptedSharedPreference.getString(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r5v8, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r5v9, types: [java.lang.Double] */
    /* JADX WARN: Type inference failed for: r5v10, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r5v12, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r5v14, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.ByteBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Set<java.lang.String> getStringSet(java.lang.String r5, java.util.Set<java.lang.String> r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x00ee
            android.content.SharedPreferences r1 = r4.a
            java.lang.String r5 = r4.a(r5)
            java.lang.String r2 = ""
            java.lang.String r5 = r1.getString(r5, r2)
            e.c.a.a.ICryptoManager r1 = r4.b
            byte[] r5 = r1.a(r5)
            if (r5 == 0) goto L_0x00ea
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r5)
            java.lang.Class<java.util.Set> r1 = java.util.Set.class
            n.p.KClass r1 = n.n.c.Reflection.a(r1)
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0038
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            java.util.Set r0 = (java.util.Set) r0
            goto L_0x00ea
        L_0x0038:
            java.lang.Class r2 = java.lang.Integer.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r3 = "bytesWrapper"
            if (r2 == 0) goto L_0x0056
            n.n.c.Intrinsics.a(r0, r3)
            int r5 = r0.getInt()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r0 = r5
            java.util.Set r0 = (java.util.Set) r0
            goto L_0x00ea
        L_0x0056:
            java.lang.Class r2 = java.lang.Float.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x0072
            n.n.c.Intrinsics.a(r0, r3)
            float r5 = r0.getFloat()
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            r0 = r5
            java.util.Set r0 = (java.util.Set) r0
            goto L_0x00ea
        L_0x0072:
            java.lang.Class r2 = java.lang.Long.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x008d
            n.n.c.Intrinsics.a(r0, r3)
            long r0 = r0.getLong()
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
            r0 = r5
            java.util.Set r0 = (java.util.Set) r0
            goto L_0x00ea
        L_0x008d:
            java.lang.Class r2 = java.lang.Double.TYPE
            n.p.KClass r2 = n.n.c.Reflection.a(r2)
            boolean r2 = n.n.c.Intrinsics.a(r1, r2)
            if (r2 == 0) goto L_0x00a8
            n.n.c.Intrinsics.a(r0, r3)
            double r0 = r0.getDouble()
            java.lang.Double r5 = java.lang.Double.valueOf(r0)
            r0 = r5
            java.util.Set r0 = (java.util.Set) r0
            goto L_0x00ea
        L_0x00a8:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00c7
            java.lang.String r0 = new java.lang.String
            java.nio.charset.Charset r1 = n.r.Charsets.a
            r0.<init>(r5, r1)
            boolean r5 = java.lang.Boolean.parseBoolean(r0)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0 = r5
            java.util.Set r0 = (java.util.Set) r0
            goto L_0x00ea
        L_0x00c7:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r1, r0)
            if (r0 == 0) goto L_0x00e2
            java.util.Set r0 = a(r4, r5)
            if (r0 == 0) goto L_0x00da
            goto L_0x00ea
        L_0x00da:
            kotlin.TypeCastException r5 = new kotlin.TypeCastException
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.collections.Set<kotlin.String>"
            r5.<init>(r6)
            throw r5
        L_0x00e2:
            java.lang.UnsupportedOperationException r5 = new java.lang.UnsupportedOperationException
            java.lang.String r6 = "Not acceptable type"
            r5.<init>(r6)
            throw r5
        L_0x00ea:
            if (r0 == 0) goto L_0x00ed
            r6 = r0
        L_0x00ed:
            return r6
        L_0x00ee:
            java.lang.String r5 = "key"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.a.c.a.b.LegacyEncryptedSharedPreference.getStringSet(java.lang.String, java.util.Set):java.util.Set");
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.String[], int, boolean, int, int):n.q.Sequence
     arg types: [java.lang.String, java.lang.String[], int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, int, int, boolean, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.CharSequence, int, java.lang.CharSequence, int, int, boolean):boolean
      n.r.Indent.a(java.lang.String, int, java.lang.String, int, int, boolean):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], int, boolean, int, int):n.q.Sequence */
    public static final /* synthetic */ Set a(LegacyEncryptedSharedPreference legacyEncryptedSharedPreference, byte[] bArr) {
        List<String> list;
        if (legacyEncryptedSharedPreference != null) {
            String str = new String(bArr, Charsets.a);
            String[] strArr = {","};
            String str2 = strArr[0];
            if (!(str2.length() == 0)) {
                list = Indent.a((CharSequence) str, str2, false, 0);
            } else {
                Iterables iterables = new Iterables(Indent.a((CharSequence) str, strArr, 0, false, 0, 2));
                ArrayList arrayList = new ArrayList(Collections.a(iterables, 10));
                Iterator it = iterables.iterator();
                while (it.hasNext()) {
                    arrayList.add(Indent.a(str, (Ranges0) it.next()));
                }
                list = arrayList;
            }
            ArrayList arrayList2 = new ArrayList(Collections.a(list, 10));
            for (String decode : list) {
                arrayList2.add(URLDecoder.decode(decode, "UTF-8"));
            }
            Object[] array = arrayList2.toArray(new String[0]);
            if (array != null) {
                String[] strArr2 = (String[]) array;
                String[] strArr3 = (String[]) Arrays.copyOf(strArr2, strArr2.length);
                if (strArr3 == null) {
                    Intrinsics.a("elements");
                    throw null;
                } else if (strArr3.length <= 0) {
                    return Sets.b;
                } else {
                    int length = strArr3.length;
                    if (length == 0) {
                        return Sets.b;
                    }
                    if (length == 1) {
                        return Collections.a((Object) strArr3[0]);
                    }
                    LinkedHashSet linkedHashSet = new LinkedHashSet(Collections.a(strArr3.length));
                    for (String add : strArr3) {
                        linkedHashSet.add(add);
                    }
                    return linkedHashSet;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            throw null;
        }
    }
}
