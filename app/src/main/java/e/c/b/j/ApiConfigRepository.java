package e.c.b.j;

import java.util.Collections;
import java.util.Map;
import l.b.Observable;
import l.b.x.BehaviorSubject;
import n.n.c.Intrinsics;

/* compiled from: ApiConfigRepository.kt */
public final class ApiConfigRepository implements IApiConfigRepository0 {
    public final BehaviorSubject<Map<b, String>> a;
    public final BehaviorSubject<Boolean> b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.x.BehaviorSubject<java.util.Map<e.c.b.j.b, java.lang.String>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.x.BehaviorSubject<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public ApiConfigRepository(String str) {
        if (str != null) {
            BehaviorSubject<Map<b, String>> behaviorSubject = new BehaviorSubject<>();
            Map singletonMap = Collections.singletonMap(IApiConfigRepository1.a, str);
            Intrinsics.a((Object) singletonMap, "java.util.Collections.si…(pair.first, pair.second)");
            behaviorSubject.b(singletonMap);
            Intrinsics.a((Object) behaviorSubject, "BehaviorSubject.create<M…)\n            )\n        }");
            this.a = behaviorSubject;
            BehaviorSubject<Boolean> behaviorSubject2 = new BehaviorSubject<>();
            Intrinsics.a((Object) behaviorSubject2, "BehaviorSubject.create<Boolean>()");
            this.b = behaviorSubject2;
            return;
        }
        Intrinsics.a("mainUrl");
        throw null;
    }

    public Map<b, String> a() {
        return this.a.c();
    }

    public Observable<Boolean> b() {
        return this.b;
    }

    public Observable<Map<b, String>> c() {
        return this.a;
    }
}
