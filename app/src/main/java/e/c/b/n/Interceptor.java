package e.c.b.n;

import android.net.TrafficStats;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import e.c.b.k.IConnectionChecker;
import e.c.b.l.HttpHeaderConfigurator;
import java.util.LinkedHashMap;
import java.util.Map;
import n.Tuples;
import n.n.c.Intrinsics;
import o.Headers;
import o.HttpUrl;
import o.Interceptor;
import o.Request;
import o.RequestBody;
import o.Response;
import o.m0.Util;

/* compiled from: Interceptor.kt */
public final class Interceptor implements o.Interceptor {
    public final /* synthetic */ IConnectionChecker b;
    public final /* synthetic */ HttpHeaderConfigurator c;

    public Interceptor(IConnectionChecker iConnectionChecker, HttpHeaderConfigurator httpHeaderConfigurator) {
        this.b = iConnectionChecker;
        this.c = httpHeaderConfigurator;
    }

    public Response a(Interceptor.a aVar) {
        LinkedHashMap linkedHashMap;
        Request request = null;
        if (aVar != null) {
            TrafficStats.setThreadStatsTag(777777);
            this.b.a();
            HttpHeaderConfigurator httpHeaderConfigurator = this.c;
            if (httpHeaderConfigurator != null) {
                Request f2 = aVar.f();
                if (f2 != null) {
                    new LinkedHashMap();
                    HttpUrl httpUrl = f2.b;
                    String str = f2.c;
                    RequestBody requestBody = f2.f2896e;
                    if (f2.f2897f.isEmpty()) {
                        linkedHashMap = new LinkedHashMap();
                    } else {
                        Map<Class<?>, Object> map = f2.f2897f;
                        if (map != null) {
                            linkedHashMap = new LinkedHashMap(map);
                        } else {
                            Intrinsics.a("$this$toMutableMap");
                            throw null;
                        }
                    }
                    Headers.a c2 = f2.d.c();
                    for (Tuples tuples : httpHeaderConfigurator.a) {
                        String str2 = (String) tuples.b;
                        String str3 = (String) tuples.c;
                        if (str2 == null) {
                            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                            throw null;
                        } else if (str3 != null) {
                            c2.a(str2, str3);
                        } else {
                            Intrinsics.a("value");
                            throw null;
                        }
                    }
                    if (httpUrl != null) {
                        request = new Request(httpUrl, str, c2.a(), requestBody, Util.a(linkedHashMap));
                    } else {
                        throw new IllegalStateException("url == null".toString());
                    }
                } else {
                    Intrinsics.a("request");
                    throw null;
                }
            }
            if (request == null) {
                request = aVar.f();
            }
            return aVar.a(request);
        }
        Intrinsics.a("chain");
        throw null;
    }
}
