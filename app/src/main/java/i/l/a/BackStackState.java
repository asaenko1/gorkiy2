package i.l.a;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import i.l.a.FragmentTransaction;
import java.util.ArrayList;

@SuppressLint({"BanParcelableUsage"})
public final class BackStackState implements Parcelable {
    public static final Parcelable.Creator<b> CREATOR = new a();
    public final int[] b;
    public final ArrayList<String> c;
    public final int[] d;

    /* renamed from: e  reason: collision with root package name */
    public final int[] f1277e;

    /* renamed from: f  reason: collision with root package name */
    public final int f1278f;
    public final int g;
    public final String h;

    /* renamed from: i  reason: collision with root package name */
    public final int f1279i;

    /* renamed from: j  reason: collision with root package name */
    public final int f1280j;

    /* renamed from: k  reason: collision with root package name */
    public final CharSequence f1281k;

    /* renamed from: l  reason: collision with root package name */
    public final int f1282l;

    /* renamed from: m  reason: collision with root package name */
    public final CharSequence f1283m;

    /* renamed from: n  reason: collision with root package name */
    public final ArrayList<String> f1284n;

    /* renamed from: o  reason: collision with root package name */
    public final ArrayList<String> f1285o;

    /* renamed from: p  reason: collision with root package name */
    public final boolean f1286p;

    public static class a implements Parcelable.Creator<b> {
        public Object createFromParcel(Parcel parcel) {
            return new BackStackState(parcel);
        }

        public Object[] newArray(int i2) {
            return new BackStackState[i2];
        }
    }

    public BackStackState(BackStackRecord backStackRecord) {
        int size = backStackRecord.a.size();
        this.b = new int[(size * 5)];
        if (backStackRecord.h) {
            this.c = new ArrayList<>(size);
            this.d = new int[size];
            this.f1277e = new int[size];
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                FragmentTransaction.a aVar = backStackRecord.a.get(i2);
                int i4 = i3 + 1;
                this.b[i3] = aVar.a;
                ArrayList<String> arrayList = this.c;
                Fragment fragment = aVar.b;
                arrayList.add(fragment != null ? fragment.f221f : null);
                int[] iArr = this.b;
                int i5 = i4 + 1;
                iArr[i4] = aVar.c;
                int i6 = i5 + 1;
                iArr[i5] = aVar.d;
                int i7 = i6 + 1;
                iArr[i6] = aVar.f1336e;
                iArr[i7] = aVar.f1337f;
                this.d[i2] = aVar.g.ordinal();
                this.f1277e[i2] = aVar.h.ordinal();
                i2++;
                i3 = i7 + 1;
            }
            this.f1278f = backStackRecord.f1325f;
            this.g = backStackRecord.g;
            this.h = backStackRecord.f1327j;
            this.f1279i = backStackRecord.u;
            this.f1280j = backStackRecord.f1328k;
            this.f1281k = backStackRecord.f1329l;
            this.f1282l = backStackRecord.f1330m;
            this.f1283m = backStackRecord.f1331n;
            this.f1284n = backStackRecord.f1332o;
            this.f1285o = backStackRecord.f1333p;
            this.f1286p = backStackRecord.f1334q;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.b);
        parcel.writeStringList(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeIntArray(this.f1277e);
        parcel.writeInt(this.f1278f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        parcel.writeInt(this.f1279i);
        parcel.writeInt(this.f1280j);
        TextUtils.writeToParcel(this.f1281k, parcel, 0);
        parcel.writeInt(this.f1282l);
        TextUtils.writeToParcel(this.f1283m, parcel, 0);
        parcel.writeStringList(this.f1284n);
        parcel.writeStringList(this.f1285o);
        parcel.writeInt(this.f1286p ? 1 : 0);
    }

    public BackStackState(Parcel parcel) {
        this.b = parcel.createIntArray();
        this.c = parcel.createStringArrayList();
        this.d = parcel.createIntArray();
        this.f1277e = parcel.createIntArray();
        this.f1278f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readString();
        this.f1279i = parcel.readInt();
        this.f1280j = parcel.readInt();
        this.f1281k = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f1282l = parcel.readInt();
        this.f1283m = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f1284n = parcel.createStringArrayList();
        this.f1285o = parcel.createStringArrayList();
        this.f1286p = parcel.readInt() != 0;
    }
}
