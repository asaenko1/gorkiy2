package i.l.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.activity.ComponentActivity;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import i.a.c;
import i.b.k.ResourcesFlusher;
import i.e.ContainerHelpers;
import i.e.SparseArrayCompat;
import i.h.d.ActivityCompat;
import i.o.Lifecycle;
import i.o.LifecycleRegistry;
import i.o.ViewModelStore;
import i.o.ViewModelStoreOwner;
import i.o.y;
import i.p.a.LoaderManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class FragmentActivity extends ComponentActivity implements ActivityCompat.b, ActivityCompat.d {
    public final FragmentController g;
    public final LifecycleRegistry h = new LifecycleRegistry(this);

    /* renamed from: i  reason: collision with root package name */
    public boolean f1287i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f1288j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f1289k = true;

    /* renamed from: l  reason: collision with root package name */
    public boolean f1290l;

    /* renamed from: m  reason: collision with root package name */
    public int f1291m;

    /* renamed from: n  reason: collision with root package name */
    public SparseArrayCompat<String> f1292n;

    public class a extends FragmentHostCallback<e> implements y, c {
        public a() {
            super(FragmentActivity.this);
        }

        public Lifecycle a() {
            return FragmentActivity.this.h;
        }

        public boolean b() {
            Window window = FragmentActivity.this.getWindow();
            return (window == null || window.peekDecorView() == null) ? false : true;
        }

        public OnBackPressedDispatcher c() {
            return FragmentActivity.this.f2f;
        }

        public ViewModelStore f() {
            return FragmentActivity.this.f();
        }

        public View a(int i2) {
            return FragmentActivity.this.findViewById(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
     arg types: [i.l.a.FragmentActivity$a, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T */
    public FragmentActivity() {
        a aVar = new a();
        ResourcesFlusher.a((Object) aVar, (Object) "callbacks == null");
        this.g = new FragmentController(aVar);
    }

    public static void b(int i2) {
        if ((i2 & -65536) != 0) {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        }
    }

    public final void a(int i2) {
        if (i2 != -1) {
            b(i2);
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.f1287i);
        printWriter.print(" mResumed=");
        printWriter.print(this.f1288j);
        printWriter.print(" mStopped=");
        printWriter.print(this.f1289k);
        if (getApplication() != null) {
            LoaderManager.a(this).a(str2, fileDescriptor, printWriter, strArr);
        }
        this.g.a.f1294f.a(str, fileDescriptor, printWriter, strArr);
    }

    public FragmentManager j() {
        return this.g.a.f1294f;
    }

    public void k() {
        this.h.a(Lifecycle.a.ON_RESUME);
        FragmentManagerImpl fragmentManagerImpl = this.g.a.f1294f;
        fragmentManagerImpl.v = false;
        fragmentManagerImpl.w = false;
        fragmentManagerImpl.b(4);
    }

    @Deprecated
    public void l() {
        invalidateOptionsMenu();
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        this.g.a();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            String a2 = this.f1292n.a(i5);
            this.f1292n.c(i5);
            if (a2 == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
            } else if (this.g.a.f1294f.b(a2) == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for who: " + a2);
            }
        } else {
            ActivityCompat.a();
            super.onActivityResult(i2, i3, intent);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.g.a();
        this.g.a.f1294f.a(configuration);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [i.l.a.FragmentHostCallback<?>, i.l.a.FragmentHostCallback, i.l.a.FragmentContainer] */
    public void onCreate(Bundle bundle) {
        ? r0 = this.g.a;
        r0.f1294f.a((FragmentHostCallback) r0, (FragmentContainer) r0, (Fragment) null);
        if (bundle != null) {
            Parcelable parcelable = bundle.getParcelable("android:support:fragments");
            FragmentHostCallback<?> fragmentHostCallback = this.g.a;
            if (fragmentHostCallback instanceof ViewModelStoreOwner) {
                fragmentHostCallback.f1294f.a(parcelable);
                if (bundle.containsKey("android:support:next_request_index")) {
                    this.f1291m = bundle.getInt("android:support:next_request_index");
                    int[] intArray = bundle.getIntArray("android:support:request_indicies");
                    String[] stringArray = bundle.getStringArray("android:support:request_fragment_who");
                    if (intArray == null || stringArray == null || intArray.length != stringArray.length) {
                        Log.w("FragmentActivity", "Invalid requestCode mapping in savedInstanceState.");
                    } else {
                        this.f1292n = new SparseArrayCompat<>(intArray.length);
                        for (int i2 = 0; i2 < intArray.length; i2++) {
                            this.f1292n.c(intArray[i2], stringArray[i2]);
                        }
                    }
                }
            } else {
                throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
            }
        }
        if (this.f1292n == null) {
            this.f1292n = new SparseArrayCompat<>(10);
            this.f1291m = 0;
        }
        super.onCreate(bundle);
        this.h.a(Lifecycle.a.ON_CREATE);
        this.g.a.f1294f.f();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu);
        FragmentController fragmentController = this.g;
        return onCreatePanelMenu | fragmentController.a.f1294f.a(menu, getMenuInflater());
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView = this.g.a.f1294f.onCreateView(view, str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(view, str, context, attributeSet) : onCreateView;
    }

    public void onDestroy() {
        super.onDestroy();
        this.g.a.f1294f.g();
        this.h.a(Lifecycle.a.ON_DESTROY);
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.g.a.f1294f.h();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        if (i2 == 0) {
            return this.g.a.f1294f.b(menuItem);
        }
        if (i2 != 6) {
            return false;
        }
        return this.g.a.f1294f.a(menuItem);
    }

    public void onMultiWindowModeChanged(boolean z) {
        this.g.a.f1294f.a(z);
    }

    public void onNewIntent(@SuppressLint({"UnknownNullness"}) Intent intent) {
        super.onNewIntent(intent);
        this.g.a();
    }

    public void onPanelClosed(int i2, Menu menu) {
        if (i2 == 0) {
            this.g.a.f1294f.a(menu);
        }
        super.onPanelClosed(i2, menu);
    }

    public void onPause() {
        super.onPause();
        this.f1288j = false;
        this.g.a.f1294f.b(3);
        this.h.a(Lifecycle.a.ON_PAUSE);
    }

    public void onPictureInPictureModeChanged(boolean z) {
        this.g.a.f1294f.b(z);
    }

    public void onPostResume() {
        super.onPostResume();
        k();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        if (i2 == 0) {
            return super.onPreparePanel(0, view, menu) | this.g.a.f1294f.b(menu);
        }
        return super.onPreparePanel(i2, view, menu);
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        this.g.a();
        int i3 = (i2 >> 16) & 65535;
        if (i3 != 0) {
            int i4 = i3 - 1;
            String a2 = this.f1292n.a(i4);
            this.f1292n.c(i4);
            if (a2 == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
            } else if (this.g.a.f1294f.b(a2) == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for who: " + a2);
            }
        }
    }

    public void onResume() {
        super.onResume();
        this.f1288j = true;
        this.g.a();
        this.g.a.f1294f.j();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        do {
        } while (a(j(), Lifecycle.b.CREATED));
        this.h.a(Lifecycle.a.ON_STOP);
        Parcelable o2 = this.g.a.f1294f.o();
        if (o2 != null) {
            bundle.putParcelable("android:support:fragments", o2);
        }
        if (this.f1292n.c() > 0) {
            bundle.putInt("android:support:next_request_index", this.f1291m);
            int[] iArr = new int[this.f1292n.c()];
            String[] strArr = new String[this.f1292n.c()];
            for (int i2 = 0; i2 < this.f1292n.c(); i2++) {
                iArr[i2] = this.f1292n.b(i2);
                strArr[i2] = this.f1292n.d(i2);
            }
            bundle.putIntArray("android:support:request_indicies", iArr);
            bundle.putStringArray("android:support:request_fragment_who", strArr);
        }
    }

    public void onStart() {
        super.onStart();
        this.f1289k = false;
        if (!this.f1287i) {
            this.f1287i = true;
            FragmentManagerImpl fragmentManagerImpl = this.g.a.f1294f;
            fragmentManagerImpl.v = false;
            fragmentManagerImpl.w = false;
            fragmentManagerImpl.b(2);
        }
        this.g.a();
        this.g.a.f1294f.j();
        this.h.a(Lifecycle.a.ON_START);
        FragmentManagerImpl fragmentManagerImpl2 = this.g.a.f1294f;
        fragmentManagerImpl2.v = false;
        fragmentManagerImpl2.w = false;
        fragmentManagerImpl2.b(3);
    }

    public void onStateNotSaved() {
        this.g.a();
    }

    public void onStop() {
        super.onStop();
        this.f1289k = true;
        do {
        } while (a(j(), Lifecycle.b.CREATED));
        FragmentManagerImpl fragmentManagerImpl = this.g.a.f1294f;
        fragmentManagerImpl.w = true;
        fragmentManagerImpl.b(2);
        this.h.a(Lifecycle.a.ON_STOP);
    }

    public void startActivityForResult(@SuppressLint({"UnknownNullness"}) Intent intent, int i2) {
        if (!this.f1290l && i2 != -1) {
            b(i2);
        }
        super.startActivityForResult(intent, i2);
    }

    public void startIntentSenderForResult(@SuppressLint({"UnknownNullness"}) IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5) {
        if (i2 != -1) {
            b(i2);
        }
        super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5);
    }

    public final int a(Fragment fragment) {
        if (this.f1292n.c() < 65534) {
            while (true) {
                SparseArrayCompat<String> sparseArrayCompat = this.f1292n;
                int i2 = this.f1291m;
                if (sparseArrayCompat.b) {
                    sparseArrayCompat.b();
                }
                if (ContainerHelpers.a(sparseArrayCompat.c, sparseArrayCompat.f1073e, i2) >= 0) {
                    this.f1291m = (this.f1291m + 1) % 65534;
                } else {
                    int i3 = this.f1291m;
                    this.f1292n.c(i3, fragment.f221f);
                    this.f1291m = (this.f1291m + 1) % 65534;
                    return i3;
                }
            }
        } else {
            throw new IllegalStateException("Too many pending Fragment activity results.");
        }
    }

    public void startIntentSenderForResult(@SuppressLint({"UnknownNullness"}) IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) {
        if (i2 != -1) {
            b(i2);
        }
        super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5, bundle);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View onCreateView = this.g.a.f1294f.onCreateView(null, str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(str, context, attributeSet) : onCreateView;
    }

    public void startActivityForResult(@SuppressLint({"UnknownNullness"}) Intent intent, int i2, Bundle bundle) {
        if (!this.f1290l && i2 != -1) {
            b(i2);
        }
        super.startActivityForResult(intent, i2, bundle);
    }

    public static boolean a(FragmentManager fragmentManager, Lifecycle.b bVar) {
        FragmentActivity fragmentActivity;
        boolean z = false;
        for (Fragment next : fragmentManager.a()) {
            if (next != null) {
                if (next.S.b.a(Lifecycle.b.STARTED)) {
                    next.S.a(bVar);
                    z = true;
                }
                FragmentHostCallback fragmentHostCallback = next.f233t;
                if (fragmentHostCallback == null) {
                    fragmentActivity = null;
                } else {
                    fragmentActivity = FragmentActivity.this;
                }
                if (fragmentActivity != null) {
                    z |= a(next.o(), bVar);
                }
            }
        }
        return z;
    }
}
