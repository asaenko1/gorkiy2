package i.l.a;

import android.view.View;
import i.h.l.ViewCompat;
import java.util.ArrayList;
import java.util.Map;

public class FragmentTransitionImpl implements Runnable {
    public final /* synthetic */ ArrayList b;
    public final /* synthetic */ Map c;

    public FragmentTransitionImpl(FragmentTransitionImpl0 fragmentTransitionImpl0, ArrayList arrayList, Map map) {
        this.b = arrayList;
        this.c = map;
    }

    public void run() {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) this.b.get(i2);
            view.setTransitionName((String) this.c.get(ViewCompat.p(view)));
        }
    }
}
