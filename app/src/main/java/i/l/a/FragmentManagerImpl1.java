package i.l.a;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;

/* compiled from: FragmentManagerImpl */
public class FragmentManagerImpl1 extends AnimatorListenerAdapter {
    public final /* synthetic */ ViewGroup a;
    public final /* synthetic */ View b;
    public final /* synthetic */ Fragment c;
    public final /* synthetic */ FragmentManagerImpl d;

    public FragmentManagerImpl1(FragmentManagerImpl fragmentManagerImpl, ViewGroup viewGroup, View view, Fragment fragment) {
        this.d = fragmentManagerImpl;
        this.a = viewGroup;
        this.b = view;
        this.c = fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public void onAnimationEnd(Animator animator) {
        this.a.endViewTransition(this.b);
        Animator n2 = this.c.n();
        this.c.a((Animator) null);
        if (n2 != null && this.a.indexOfChild(this.b) < 0) {
            FragmentManagerImpl fragmentManagerImpl = this.d;
            Fragment fragment = this.c;
            fragmentManagerImpl.a(fragment, fragment.x(), 0, 0, false);
        }
    }
}
