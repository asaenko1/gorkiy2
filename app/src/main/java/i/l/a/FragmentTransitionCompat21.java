package i.l.a;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

public class FragmentTransitionCompat21 extends FragmentTransitionImpl0 {

    public class a extends Transition.EpicenterCallback {
        public final /* synthetic */ Rect a;

        public a(FragmentTransitionCompat21 fragmentTransitionCompat21, Rect rect) {
            this.a = rect;
        }

        public Rect onGetEpicenter(Transition transition) {
            return this.a;
        }
    }

    public class b implements Transition.TransitionListener {
        public final /* synthetic */ View a;
        public final /* synthetic */ ArrayList b;

        public b(FragmentTransitionCompat21 fragmentTransitionCompat21, View view, ArrayList arrayList) {
            this.a = view;
            this.b = arrayList;
        }

        public void onTransitionCancel(Transition transition) {
        }

        public void onTransitionEnd(Transition transition) {
            transition.removeListener(this);
            this.a.setVisibility(8);
            int size = this.b.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((View) this.b.get(i2)).setVisibility(0);
            }
        }

        public void onTransitionPause(Transition transition) {
        }

        public void onTransitionResume(Transition transition) {
        }

        public void onTransitionStart(Transition transition) {
        }
    }

    public class c implements Transition.TransitionListener {
        public final /* synthetic */ Object a;
        public final /* synthetic */ ArrayList b;
        public final /* synthetic */ Object c;
        public final /* synthetic */ ArrayList d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ Object f1352e;

        /* renamed from: f  reason: collision with root package name */
        public final /* synthetic */ ArrayList f1353f;

        public c(Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
            this.a = obj;
            this.b = arrayList;
            this.c = obj2;
            this.d = arrayList2;
            this.f1352e = obj3;
            this.f1353f = arrayList3;
        }

        public void onTransitionCancel(Transition transition) {
        }

        public void onTransitionEnd(Transition transition) {
            transition.removeListener(this);
        }

        public void onTransitionPause(Transition transition) {
        }

        public void onTransitionResume(Transition transition) {
        }

        public void onTransitionStart(Transition transition) {
            Object obj = this.a;
            if (obj != null) {
                FragmentTransitionCompat21.this.a(obj, (ArrayList<View>) this.b, (ArrayList<View>) null);
            }
            Object obj2 = this.c;
            if (obj2 != null) {
                FragmentTransitionCompat21.this.a(obj2, (ArrayList<View>) this.d, (ArrayList<View>) null);
            }
            Object obj3 = this.f1352e;
            if (obj3 != null) {
                FragmentTransitionCompat21.this.a(obj3, (ArrayList<View>) this.f1353f, (ArrayList<View>) null);
            }
        }
    }

    public class d extends Transition.EpicenterCallback {
        public final /* synthetic */ Rect a;

        public d(FragmentTransitionCompat21 fragmentTransitionCompat21, Rect rect) {
            this.a = rect;
        }

        public Rect onGetEpicenter(Transition transition) {
            Rect rect = this.a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.a;
        }
    }

    public boolean a(Object obj) {
        return obj instanceof Transition;
    }

    public Object b(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    public Object c(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition((Transition) obj);
        return transitionSet;
    }

    public void a(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            int i2 = 0;
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int transitionCount = transitionSet.getTransitionCount();
                while (i2 < transitionCount) {
                    a(transitionSet.getTransitionAt(i2), arrayList);
                    i2++;
                }
            } else if (!a(transition) && FragmentTransitionImpl0.a((List) transition.getTargets())) {
                int size = arrayList.size();
                while (i2 < size) {
                    transition.addTarget(arrayList.get(i2));
                    i2++;
                }
            }
        }
    }

    public void b(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> targets = transitionSet.getTargets();
        targets.clear();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            FragmentTransitionImpl0.a(targets, arrayList.get(i2));
        }
        targets.add(view);
        arrayList.add(view);
        a(transitionSet, arrayList);
    }

    public void c(Object obj, View view) {
        if (view != null) {
            Rect rect = new Rect();
            a(view, rect);
            ((Transition) obj).setEpicenterCallback(new a(this, rect));
        }
    }

    public Object b(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.addTransition((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.addTransition((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.addTransition((Transition) obj3);
        }
        return transitionSet;
    }

    public static boolean a(Transition transition) {
        return !FragmentTransitionImpl0.a(transition.getTargetIds()) || !FragmentTransitionImpl0.a(transition.getTargetNames()) || !FragmentTransitionImpl0.a(transition.getTargetTypes());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransitionCompat21.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
     arg types: [android.transition.TransitionSet, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>]
     candidates:
      i.l.a.FragmentTransitionCompat21.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.l.a.FragmentTransitionCompat21.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionImpl0.a(java.util.List<android.view.View>, android.view.View, int):boolean
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionCompat21.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void */
    public void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.getTargets().clear();
            transitionSet.getTargets().addAll(arrayList2);
            a((Object) transitionSet, arrayList, arrayList2);
        }
    }

    public void a(Object obj, View view, ArrayList<View> arrayList) {
        ((Transition) obj).addListener(new b(this, view, arrayList));
    }

    public Object a(Object obj, Object obj2, Object obj3) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition != null && transition2 != null) {
            transition = new TransitionSet().addTransition(transition).addTransition(transition2).setOrdering(1);
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 == null) {
            return transition;
        }
        TransitionSet transitionSet = new TransitionSet();
        if (transition != null) {
            transitionSet.addTransition(transition);
        }
        transitionSet.addTransition(transition3);
        return transitionSet;
    }

    public void b(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).removeTarget(view);
        }
    }

    public void a(ViewGroup viewGroup, Object obj) {
        TransitionManager.beginDelayedTransition(viewGroup, (Transition) obj);
    }

    public void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        ((Transition) obj).addListener(new c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransitionCompat21.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
     arg types: [android.transition.Transition, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>]
     candidates:
      i.l.a.FragmentTransitionCompat21.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.l.a.FragmentTransitionCompat21.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionImpl0.a(java.util.List<android.view.View>, android.view.View, int):boolean
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      i.l.a.FragmentTransitionImpl0.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      i.l.a.FragmentTransitionCompat21.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void */
    public void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        List<View> targets;
        int i2;
        Transition transition = (Transition) obj;
        int i3 = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int transitionCount = transitionSet.getTransitionCount();
            while (i3 < transitionCount) {
                a((Object) transitionSet.getTransitionAt(i3), arrayList, arrayList2);
                i3++;
            }
        } else if (!a(transition) && (targets = transition.getTargets()) != null && targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
            if (arrayList2 == null) {
                i2 = 0;
            } else {
                i2 = arrayList2.size();
            }
            while (i3 < i2) {
                transition.addTarget(arrayList2.get(i3));
                i3++;
            }
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                transition.removeTarget(arrayList.get(size));
            }
        }
    }

    public void a(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).addTarget(view);
        }
    }

    public void a(Object obj, Rect rect) {
        if (obj != null) {
            ((Transition) obj).setEpicenterCallback(new d(this, rect));
        }
    }
}
