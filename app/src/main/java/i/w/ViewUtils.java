package i.w;

import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;
import i.h.l.ViewCompat;

public class ViewUtils {
    public static final ViewUtilsBase a;
    public static final Property<View, Float> b = new a(Float.class, "translationAlpha");

    public static class a extends Property<View, Float> {
        public a(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            return Float.valueOf(ViewUtils.b((View) obj));
        }

        public void set(Object obj, Object obj2) {
            float floatValue = ((Float) obj2).floatValue();
            ViewUtils.a.a((View) obj, floatValue);
        }
    }

    public static class b extends Property<View, Rect> {
        public b(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            return ViewCompat.e((View) obj);
        }

        public void set(Object obj, Object obj2) {
            ViewCompat.a((View) obj, (Rect) obj2);
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 29) {
            a = new ViewUtilsApi29();
        } else if (i2 >= 23) {
            a = new ViewUtilsApi23();
        } else if (i2 >= 22) {
            a = new ViewUtilsApi22();
        } else {
            a = new ViewUtilsApi21();
        }
        new b(Rect.class, "clipBounds");
    }

    public static ViewOverlayImpl a(View view) {
        return new ViewOverlayApi18(view);
    }

    public static float b(View view) {
        return a.b(view);
    }

    public static WindowIdImpl c(View view) {
        return new WindowIdApi18(view);
    }

    public static void a(View view, int i2, int i3, int i4, int i5) {
        a.a(view, i2, i3, i4, i5);
    }
}
