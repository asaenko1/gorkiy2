package i.w;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;

public class ViewUtilsApi23 extends ViewUtilsApi22 {

    /* renamed from: i  reason: collision with root package name */
    public static boolean f1469i = true;

    @SuppressLint({"NewApi"})
    public void a(View view, int i2) {
        if (Build.VERSION.SDK_INT == 28) {
            super.a(view, i2);
        } else if (f1469i) {
            try {
                view.setTransitionVisibility(i2);
            } catch (NoSuchMethodError unused) {
                f1469i = false;
            }
        }
    }
}
