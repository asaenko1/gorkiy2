package i.w;

import android.util.SparseArray;
import android.view.View;
import i.e.ArrayMap;
import i.e.LongSparseArray;

public class TransitionValuesMaps {
    public final ArrayMap<View, q> a = new ArrayMap<>();
    public final SparseArray<View> b = new SparseArray<>();
    public final LongSparseArray<View> c = new LongSparseArray<>();
    public final ArrayMap<String, View> d = new ArrayMap<>();
}
