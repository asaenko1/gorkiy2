package i.v.a.e;

import i.v.a.SupportSQLiteOpenHelper;

public final class FrameworkSQLiteOpenHelperFactory implements SupportSQLiteOpenHelper.c {
    public SupportSQLiteOpenHelper a(SupportSQLiteOpenHelper.b bVar) {
        return new FrameworkSQLiteOpenHelper(bVar.a, bVar.b, bVar.c);
    }
}
