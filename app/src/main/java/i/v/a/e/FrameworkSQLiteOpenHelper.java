package i.v.a.e;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import e.a.b.d.c.a.DpDatabase_Impl;
import i.s.RoomOpenHelper;
import i.s.h;
import i.v.a.SimpleSQLiteQuery;
import i.v.a.SupportSQLiteDatabase;
import i.v.a.SupportSQLiteOpenHelper;
import j.a.a.a.outline;
import java.util.List;

public class FrameworkSQLiteOpenHelper implements SupportSQLiteOpenHelper {
    public final a a;

    public FrameworkSQLiteOpenHelper(Context context, String str, SupportSQLiteOpenHelper.a aVar) {
        this.a = new a(context, str, new FrameworkSQLiteDatabase0[1], aVar);
    }

    public void a(boolean z) {
        this.a.setWriteAheadLoggingEnabled(z);
    }

    public String b() {
        return this.a.getDatabaseName();
    }

    public SupportSQLiteDatabase a() {
        return this.a.a();
    }

    public static class a extends SQLiteOpenHelper {
        public final FrameworkSQLiteDatabase0[] b;
        public final SupportSQLiteOpenHelper.a c;
        public boolean d;

        /* renamed from: i.v.a.e.FrameworkSQLiteOpenHelper$a$a  reason: collision with other inner class name */
        public class C0009a implements DatabaseErrorHandler {
            public final /* synthetic */ SupportSQLiteOpenHelper.a a;
            public final /* synthetic */ FrameworkSQLiteDatabase0[] b;

            public C0009a(SupportSQLiteOpenHelper.a aVar, FrameworkSQLiteDatabase0[] frameworkSQLiteDatabase0Arr) {
                this.a = aVar;
                this.b = frameworkSQLiteDatabase0Arr;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:10:0x003e, code lost:
                if (r1 != null) goto L_0x0040;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
                r5 = r1.iterator();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:13:0x0048, code lost:
                if (r5.hasNext() != false) goto L_0x004a;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x004a, code lost:
                r0.a((java.lang.String) r5.next().second);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:15:0x0058, code lost:
                r0.a(r5.a());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:16:0x005f, code lost:
                throw r2;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:7:0x0038, code lost:
                r2 = move-exception;
             */
            /* JADX WARNING: Failed to process nested try/catch */
            /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x003a */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0063  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x007b  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x0038 A[ExcHandler: all (r2v5 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r1 10  PHI: (r1v8 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) = (r1v1 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r1v2 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r1v2 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) binds: [B:5:0x0031, B:8:0x003a, B:9:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0031] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onCorruption(android.database.sqlite.SQLiteDatabase r5) {
                /*
                    r4 = this;
                    i.v.a.SupportSQLiteOpenHelper$a r0 = r4.a
                    i.v.a.e.FrameworkSQLiteDatabase0[] r1 = r4.b
                    i.v.a.e.FrameworkSQLiteDatabase0 r5 = i.v.a.e.FrameworkSQLiteOpenHelper.a.a(r1, r5)
                    r1 = 0
                    if (r0 == 0) goto L_0x0083
                    java.lang.String r2 = "Corruption reported by sqlite on database: "
                    java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
                    java.lang.String r3 = r5.a()
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    java.lang.String r3 = "SupportSQLite"
                    android.util.Log.e(r3, r2)
                    android.database.sqlite.SQLiteDatabase r2 = r5.b
                    boolean r2 = r2.isOpen()
                    if (r2 != 0) goto L_0x0031
                    java.lang.String r5 = r5.a()
                    r0.a(r5)
                    goto L_0x0082
                L_0x0031:
                    android.database.sqlite.SQLiteDatabase r2 = r5.b     // Catch:{ SQLiteException -> 0x003a, all -> 0x0038 }
                    java.util.List r1 = r2.getAttachedDbs()     // Catch:{ SQLiteException -> 0x003a, all -> 0x0038 }
                    goto L_0x003a
                L_0x0038:
                    r2 = move-exception
                    goto L_0x003e
                L_0x003a:
                    r5.close()     // Catch:{ IOException -> 0x0060, all -> 0x0038 }
                    goto L_0x0061
                L_0x003e:
                    if (r1 == 0) goto L_0x0058
                    java.util.Iterator r5 = r1.iterator()
                L_0x0044:
                    boolean r1 = r5.hasNext()
                    if (r1 == 0) goto L_0x005f
                    java.lang.Object r1 = r5.next()
                    android.util.Pair r1 = (android.util.Pair) r1
                    java.lang.Object r1 = r1.second
                    java.lang.String r1 = (java.lang.String) r1
                    r0.a(r1)
                    goto L_0x0044
                L_0x0058:
                    java.lang.String r5 = r5.a()
                    r0.a(r5)
                L_0x005f:
                    throw r2
                L_0x0060:
                L_0x0061:
                    if (r1 == 0) goto L_0x007b
                    java.util.Iterator r5 = r1.iterator()
                L_0x0067:
                    boolean r1 = r5.hasNext()
                    if (r1 == 0) goto L_0x0082
                    java.lang.Object r1 = r5.next()
                    android.util.Pair r1 = (android.util.Pair) r1
                    java.lang.Object r1 = r1.second
                    java.lang.String r1 = (java.lang.String) r1
                    r0.a(r1)
                    goto L_0x0067
                L_0x007b:
                    java.lang.String r5 = r5.a()
                    r0.a(r5)
                L_0x0082:
                    return
                L_0x0083:
                    throw r1
                */
                throw new UnsupportedOperationException("Method not decompiled: i.v.a.e.FrameworkSQLiteOpenHelper.a.C0009a.onCorruption(android.database.sqlite.SQLiteDatabase):void");
            }
        }

        public a(Context context, String str, FrameworkSQLiteDatabase0[] frameworkSQLiteDatabase0Arr, SupportSQLiteOpenHelper.a aVar) {
            super(context, str, null, aVar.a, new C0009a(aVar, frameworkSQLiteDatabase0Arr));
            this.c = aVar;
            this.b = frameworkSQLiteDatabase0Arr;
        }

        public synchronized SupportSQLiteDatabase a() {
            this.d = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.d) {
                close();
                return a();
            }
            return a(writableDatabase);
        }

        public synchronized void close() {
            super.close();
            this.b[0] = null;
        }

        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            SupportSQLiteOpenHelper.a aVar = this.c;
            a(this.b, sQLiteDatabase);
            if (((RoomOpenHelper) aVar) == null) {
                throw null;
            }
        }

        /* JADX INFO: finally extract failed */
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            SupportSQLiteOpenHelper.a aVar = this.c;
            FrameworkSQLiteDatabase0 a = a(this.b, sQLiteDatabase);
            RoomOpenHelper roomOpenHelper = (RoomOpenHelper) aVar;
            if (roomOpenHelper != null) {
                Cursor b2 = a.b("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
                try {
                    boolean z = b2.moveToFirst() && b2.getInt(0) == 0;
                    b2.close();
                    roomOpenHelper.c.a(a);
                    if (!z) {
                        RoomOpenHelper.b b3 = roomOpenHelper.c.b(a);
                        if (!b3.a) {
                            StringBuilder a2 = outline.a("Pre-packaged database has an invalid schema: ");
                            a2.append(b3.b);
                            throw new IllegalStateException(a2.toString());
                        }
                    }
                    roomOpenHelper.a(a);
                    DpDatabase_Impl dpDatabase_Impl = (DpDatabase_Impl) roomOpenHelper.c;
                    if (dpDatabase_Impl.b.g != null) {
                        int size = dpDatabase_Impl.b.g.size();
                        for (int i2 = 0; i2 < size; i2++) {
                            dpDatabase_Impl.b.g.get(i2).a();
                        }
                    }
                } catch (Throwable th) {
                    b2.close();
                    throw th;
                }
            } else {
                throw null;
            }
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
            this.d = true;
            ((RoomOpenHelper) this.c).a(a(this.b, sQLiteDatabase), i2, i3);
        }

        /* JADX INFO: finally extract failed */
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.d) {
                SupportSQLiteOpenHelper.a aVar = this.c;
                FrameworkSQLiteDatabase0 a = a(this.b, sQLiteDatabase);
                RoomOpenHelper roomOpenHelper = (RoomOpenHelper) aVar;
                if (roomOpenHelper != null) {
                    Cursor b2 = a.b("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
                    try {
                        if (b2.moveToFirst() && b2.getInt(0) != 0) {
                            Cursor a2 = a.a(new SimpleSQLiteQuery("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
                            try {
                                String string = a2.moveToFirst() ? a2.getString(0) : null;
                                a2.close();
                                if (!roomOpenHelper.d.equals(string) && !roomOpenHelper.f1437e.equals(string)) {
                                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                                }
                            } catch (Throwable th) {
                                a2.close();
                                throw th;
                            }
                        } else {
                            RoomOpenHelper.b b3 = roomOpenHelper.c.b(a);
                            if (!b3.a) {
                                StringBuilder a3 = outline.a("Pre-packaged database has an invalid schema: ");
                                a3.append(b3.b);
                                throw new IllegalStateException(a3.toString());
                            } else if (((DpDatabase_Impl) roomOpenHelper.c) != null) {
                                roomOpenHelper.a(a);
                            } else {
                                throw null;
                            }
                        }
                        DpDatabase_Impl dpDatabase_Impl = (DpDatabase_Impl) roomOpenHelper.c;
                        dpDatabase_Impl.b.a = a;
                        dpDatabase_Impl.b.d.a(a);
                        List<h.a> list = dpDatabase_Impl.b.g;
                        if (list != null) {
                            int size = list.size();
                            for (int i2 = 0; i2 < size; i2++) {
                                dpDatabase_Impl.b.g.get(i2).c();
                            }
                        }
                        roomOpenHelper.b = null;
                    } finally {
                        b2.close();
                    }
                } else {
                    throw null;
                }
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
            this.d = true;
            this.c.a(a(this.b, sQLiteDatabase), i2, i3);
        }

        public FrameworkSQLiteDatabase0 a(SQLiteDatabase sQLiteDatabase) {
            return a(this.b, sQLiteDatabase);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x000c, code lost:
            if ((r1.b == r3) == false) goto L_0x000e;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static i.v.a.e.FrameworkSQLiteDatabase0 a(i.v.a.e.FrameworkSQLiteDatabase0[] r2, android.database.sqlite.SQLiteDatabase r3) {
            /*
                r0 = 0
                r1 = r2[r0]
                if (r1 == 0) goto L_0x000e
                android.database.sqlite.SQLiteDatabase r1 = r1.b
                if (r1 != r3) goto L_0x000b
                r1 = 1
                goto L_0x000c
            L_0x000b:
                r1 = 0
            L_0x000c:
                if (r1 != 0) goto L_0x0015
            L_0x000e:
                i.v.a.e.FrameworkSQLiteDatabase0 r1 = new i.v.a.e.FrameworkSQLiteDatabase0
                r1.<init>(r3)
                r2[r0] = r1
            L_0x0015:
                r2 = r2[r0]
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: i.v.a.e.FrameworkSQLiteOpenHelper.a.a(i.v.a.e.FrameworkSQLiteDatabase0[], android.database.sqlite.SQLiteDatabase):i.v.a.e.FrameworkSQLiteDatabase0");
        }
    }
}
