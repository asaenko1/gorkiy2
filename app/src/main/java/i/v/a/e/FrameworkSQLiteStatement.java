package i.v.a.e;

import android.database.sqlite.SQLiteStatement;
import i.v.a.SupportSQLiteProgram;

public class FrameworkSQLiteStatement extends FrameworkSQLiteProgram implements SupportSQLiteProgram {
    public final SQLiteStatement c;

    public FrameworkSQLiteStatement(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.c = sQLiteStatement;
    }
}
