package i.b.p.i;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;
import android.widget.PopupWindow;
import i.b.p.i.MenuPresenter;
import i.h.l.ViewCompat;

public class MenuPopupHelper {
    public final Context a;
    public final MenuBuilder b;
    public final boolean c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final int f911e;

    /* renamed from: f  reason: collision with root package name */
    public View f912f;
    public int g = 8388611;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public MenuPresenter.a f913i;

    /* renamed from: j  reason: collision with root package name */
    public MenuPopup f914j;

    /* renamed from: k  reason: collision with root package name */
    public PopupWindow.OnDismissListener f915k;

    /* renamed from: l  reason: collision with root package name */
    public final PopupWindow.OnDismissListener f916l = new a();

    public class a implements PopupWindow.OnDismissListener {
        public a() {
        }

        public void onDismiss() {
            MenuPopupHelper.this.c();
        }
    }

    public MenuPopupHelper(Context context, MenuBuilder menuBuilder, View view, boolean z, int i2, int i3) {
        this.a = context;
        this.b = menuBuilder;
        this.f912f = view;
        this.c = z;
        this.d = i2;
        this.f911e = i3;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v9, types: [i.b.p.i.MenuPresenter, i.b.p.i.MenuPopup] */
    /* JADX WARN: Type inference failed for: r7v1, types: [i.b.p.i.StandardMenuPopup] */
    /* JADX WARN: Type inference failed for: r1v13, types: [i.b.p.i.CascadingMenuPopup] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public i.b.p.i.MenuPopup a() {
        /*
            r14 = this;
            i.b.p.i.MenuPopup r0 = r14.f914j
            if (r0 != 0) goto L_0x0078
            android.content.Context r0 = r14.a
            java.lang.String r1 = "window"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.view.Display r0 = r0.getDefaultDisplay()
            android.graphics.Point r1 = new android.graphics.Point
            r1.<init>()
            r0.getRealSize(r1)
            int r0 = r1.x
            int r1 = r1.y
            int r0 = java.lang.Math.min(r0, r1)
            android.content.Context r1 = r14.a
            android.content.res.Resources r1 = r1.getResources()
            int r2 = i.b.d.abc_cascading_menus_min_smallest_width
            int r1 = r1.getDimensionPixelSize(r2)
            if (r0 < r1) goto L_0x0032
            r0 = 1
            goto L_0x0033
        L_0x0032:
            r0 = 0
        L_0x0033:
            if (r0 == 0) goto L_0x0046
            i.b.p.i.CascadingMenuPopup r0 = new i.b.p.i.CascadingMenuPopup
            android.content.Context r2 = r14.a
            android.view.View r3 = r14.f912f
            int r4 = r14.d
            int r5 = r14.f911e
            boolean r6 = r14.c
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6)
            goto L_0x0058
        L_0x0046:
            i.b.p.i.StandardMenuPopup r0 = new i.b.p.i.StandardMenuPopup
            android.content.Context r8 = r14.a
            i.b.p.i.MenuBuilder r9 = r14.b
            android.view.View r10 = r14.f912f
            int r11 = r14.d
            int r12 = r14.f911e
            boolean r13 = r14.c
            r7 = r0
            r7.<init>(r8, r9, r10, r11, r12, r13)
        L_0x0058:
            i.b.p.i.MenuBuilder r1 = r14.b
            r0.a(r1)
            android.widget.PopupWindow$OnDismissListener r1 = r14.f916l
            r0.a(r1)
            android.view.View r1 = r14.f912f
            r0.a(r1)
            i.b.p.i.MenuPresenter$a r1 = r14.f913i
            r0.a(r1)
            boolean r1 = r14.h
            r0.b(r1)
            int r1 = r14.g
            r0.a(r1)
            r14.f914j = r0
        L_0x0078:
            i.b.p.i.MenuPopup r0 = r14.f914j
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.p.i.MenuPopupHelper.a():i.b.p.i.MenuPopup");
    }

    public boolean b() {
        MenuPopup menuPopup = this.f914j;
        return menuPopup != null && menuPopup.b();
    }

    public void c() {
        this.f914j = null;
        PopupWindow.OnDismissListener onDismissListener = this.f915k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public boolean d() {
        if (b()) {
            return true;
        }
        if (this.f912f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    public final void a(int i2, int i3, boolean z, boolean z2) {
        MenuPopup a2 = a();
        a2.c(z2);
        if (z) {
            if ((Gravity.getAbsoluteGravity(this.g, ViewCompat.k(this.f912f)) & 7) == 5) {
                i2 -= this.f912f.getWidth();
            }
            a2.b(i2);
            a2.c(i3);
            int i4 = (int) ((this.a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            a2.b = new Rect(i2 - i4, i3 - i4, i2 + i4, i3 + i4);
        }
        a2.a();
    }

    public void a(MenuPresenter.a aVar) {
        this.f913i = aVar;
        MenuPopup menuPopup = this.f914j;
        if (menuPopup != null) {
            menuPopup.a(aVar);
        }
    }
}
