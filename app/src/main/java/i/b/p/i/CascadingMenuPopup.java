package i.b.p.i;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import i.b.g;
import i.b.p.i.MenuPresenter;
import i.b.p.i.d;
import i.b.q.MenuItemHoverListener;
import i.b.q.MenuPopupWindow;
import i.h.l.ViewCompat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class CascadingMenuPopup extends MenuPopup implements MenuPresenter, View.OnKeyListener, PopupWindow.OnDismissListener {
    public static final int C = g.abc_cascading_menu_item_layout;
    public PopupWindow.OnDismissListener A;
    public boolean B;
    public final Context c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final int f861e;

    /* renamed from: f  reason: collision with root package name */
    public final int f862f;
    public final boolean g;
    public final Handler h;

    /* renamed from: i  reason: collision with root package name */
    public final List<g> f863i = new ArrayList();

    /* renamed from: j  reason: collision with root package name */
    public final List<d.d> f864j = new ArrayList();

    /* renamed from: k  reason: collision with root package name */
    public final ViewTreeObserver.OnGlobalLayoutListener f865k = new a();

    /* renamed from: l  reason: collision with root package name */
    public final View.OnAttachStateChangeListener f866l = new b();

    /* renamed from: m  reason: collision with root package name */
    public final MenuItemHoverListener f867m = new c();

    /* renamed from: n  reason: collision with root package name */
    public int f868n;

    /* renamed from: o  reason: collision with root package name */
    public int f869o;

    /* renamed from: p  reason: collision with root package name */
    public View f870p;

    /* renamed from: q  reason: collision with root package name */
    public View f871q;

    /* renamed from: r  reason: collision with root package name */
    public int f872r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f873s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f874t;
    public int u;
    public int v;
    public boolean w;
    public boolean x;
    public MenuPresenter.a y;
    public ViewTreeObserver z;

    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public a() {
        }

        public void onGlobalLayout() {
            if (CascadingMenuPopup.this.b() && CascadingMenuPopup.this.f864j.size() > 0 && !CascadingMenuPopup.this.f864j.get(0).a.B) {
                View view = CascadingMenuPopup.this.f871q;
                if (view == null || !view.isShown()) {
                    CascadingMenuPopup.this.dismiss();
                    return;
                }
                Iterator<d.d> it = CascadingMenuPopup.this.f864j.iterator();
                while (it.hasNext()) {
                    it.next().a.a();
                }
            }
        }
    }

    public class b implements View.OnAttachStateChangeListener {
        public b() {
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = CascadingMenuPopup.this.z;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    CascadingMenuPopup.this.z = view.getViewTreeObserver();
                }
                CascadingMenuPopup cascadingMenuPopup = CascadingMenuPopup.this;
                cascadingMenuPopup.z.removeGlobalOnLayoutListener(cascadingMenuPopup.f865k);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    public class c implements MenuItemHoverListener {

        public class a implements Runnable {
            public final /* synthetic */ d b;
            public final /* synthetic */ MenuItem c;
            public final /* synthetic */ MenuBuilder d;

            public a(d dVar, MenuItem menuItem, MenuBuilder menuBuilder) {
                this.b = dVar;
                this.c = menuItem;
                this.d = menuBuilder;
            }

            public void run() {
                d dVar = this.b;
                if (dVar != null) {
                    CascadingMenuPopup.this.B = true;
                    dVar.b.a(false);
                    CascadingMenuPopup.this.B = false;
                }
                if (this.c.isEnabled() && this.c.hasSubMenu()) {
                    this.d.a(this.c, 4);
                }
            }
        }

        public c() {
        }

        /* JADX WARN: Type inference failed for: r0v11, types: [java.lang.Object] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(i.b.p.i.MenuBuilder r6, android.view.MenuItem r7) {
            /*
                r5 = this;
                i.b.p.i.CascadingMenuPopup r0 = i.b.p.i.CascadingMenuPopup.this
                android.os.Handler r0 = r0.h
                r1 = 0
                r0.removeCallbacksAndMessages(r1)
                i.b.p.i.CascadingMenuPopup r0 = i.b.p.i.CascadingMenuPopup.this
                java.util.List<i.b.p.i.d$d> r0 = r0.f864j
                int r0 = r0.size()
                r2 = 0
            L_0x0011:
                r3 = -1
                if (r2 >= r0) goto L_0x0026
                i.b.p.i.CascadingMenuPopup r4 = i.b.p.i.CascadingMenuPopup.this
                java.util.List<i.b.p.i.d$d> r4 = r4.f864j
                java.lang.Object r4 = r4.get(r2)
                i.b.p.i.CascadingMenuPopup$d r4 = (i.b.p.i.CascadingMenuPopup.d) r4
                i.b.p.i.MenuBuilder r4 = r4.b
                if (r6 != r4) goto L_0x0023
                goto L_0x0027
            L_0x0023:
                int r2 = r2 + 1
                goto L_0x0011
            L_0x0026:
                r2 = -1
            L_0x0027:
                if (r2 != r3) goto L_0x002a
                return
            L_0x002a:
                int r2 = r2 + 1
                i.b.p.i.CascadingMenuPopup r0 = i.b.p.i.CascadingMenuPopup.this
                java.util.List<i.b.p.i.d$d> r0 = r0.f864j
                int r0 = r0.size()
                if (r2 >= r0) goto L_0x0041
                i.b.p.i.CascadingMenuPopup r0 = i.b.p.i.CascadingMenuPopup.this
                java.util.List<i.b.p.i.d$d> r0 = r0.f864j
                java.lang.Object r0 = r0.get(r2)
                r1 = r0
                i.b.p.i.CascadingMenuPopup$d r1 = (i.b.p.i.CascadingMenuPopup.d) r1
            L_0x0041:
                i.b.p.i.CascadingMenuPopup$c$a r0 = new i.b.p.i.CascadingMenuPopup$c$a
                r0.<init>(r1, r7, r6)
                long r1 = android.os.SystemClock.uptimeMillis()
                r3 = 200(0xc8, double:9.9E-322)
                long r1 = r1 + r3
                i.b.p.i.CascadingMenuPopup r7 = i.b.p.i.CascadingMenuPopup.this
                android.os.Handler r7 = r7.h
                r7.postAtTime(r0, r6, r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: i.b.p.i.CascadingMenuPopup.c.a(i.b.p.i.MenuBuilder, android.view.MenuItem):void");
        }

        public void b(MenuBuilder menuBuilder, MenuItem menuItem) {
            CascadingMenuPopup.this.h.removeCallbacksAndMessages(menuBuilder);
        }
    }

    public static class d {
        public final MenuPopupWindow a;
        public final MenuBuilder b;
        public final int c;

        public d(MenuPopupWindow menuPopupWindow, MenuBuilder menuBuilder, int i2) {
            this.a = menuPopupWindow;
            this.b = menuBuilder;
            this.c = i2;
        }
    }

    public CascadingMenuPopup(Context context, View view, int i2, int i3, boolean z2) {
        int i4 = 0;
        this.f868n = 0;
        this.f869o = 0;
        this.c = context;
        this.f870p = view;
        this.f861e = i2;
        this.f862f = i3;
        this.g = z2;
        this.w = false;
        this.f872r = ViewCompat.k(view) != 1 ? 1 : i4;
        Resources resources = context.getResources();
        this.d = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(i.b.d.abc_config_prefDialogWidth));
        this.h = new Handler();
    }

    public void a() {
        if (!b()) {
            Iterator<g> it = this.f863i.iterator();
            while (it.hasNext()) {
                c(it.next());
            }
            this.f863i.clear();
            View view = this.f870p;
            this.f871q = view;
            if (view != null) {
                boolean z2 = this.z == null;
                ViewTreeObserver viewTreeObserver = this.f871q.getViewTreeObserver();
                this.z = viewTreeObserver;
                if (z2) {
                    viewTreeObserver.addOnGlobalLayoutListener(this.f865k);
                }
                this.f871q.addOnAttachStateChangeListener(this.f866l);
            }
        }
    }

    public void b(boolean z2) {
        this.w = z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, i.b.q.DropDownListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0149, code lost:
        if (((r9.getWidth() + r11[0]) + r4) > r10.right) goto L_0x0153;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x014f, code lost:
        if ((r11[0] - r4) < 0) goto L_0x0151;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0153, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(i.b.p.i.MenuBuilder r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            android.content.Context r2 = r0.c
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r2)
            i.b.p.i.MenuAdapter r3 = new i.b.p.i.MenuAdapter
            boolean r4 = r0.g
            int r5 = i.b.p.i.CascadingMenuPopup.C
            r3.<init>(r1, r2, r4, r5)
            boolean r4 = r16.b()
            r5 = 1
            if (r4 != 0) goto L_0x0021
            boolean r4 = r0.w
            if (r4 == 0) goto L_0x0021
            r3.d = r5
            goto L_0x002d
        L_0x0021:
            boolean r4 = r16.b()
            if (r4 == 0) goto L_0x002d
            boolean r4 = i.b.p.i.MenuPopup.b(r17)
            r3.d = r4
        L_0x002d:
            android.content.Context r4 = r0.c
            int r6 = r0.d
            r7 = 0
            int r4 = i.b.p.i.MenuPopup.a(r3, r7, r4, r6)
            i.b.q.MenuPopupWindow r6 = new i.b.q.MenuPopupWindow
            android.content.Context r8 = r0.c
            int r9 = r0.f861e
            int r10 = r0.f862f
            r6.<init>(r8, r7, r9, r10)
            i.b.q.MenuItemHoverListener r8 = r0.f867m
            r6.G = r8
            r6.f1006t = r0
            android.widget.PopupWindow r8 = r6.C
            r8.setOnDismissListener(r0)
            android.view.View r8 = r0.f870p
            r6.f1005s = r8
            int r8 = r0.f869o
            r6.f999m = r8
            r6.a(r5)
            android.widget.PopupWindow r8 = r6.C
            r9 = 2
            r8.setInputMethodMode(r9)
            r6.a(r3)
            r6.d(r4)
            int r3 = r0.f869o
            r6.f999m = r3
            java.util.List<i.b.p.i.d$d> r3 = r0.f864j
            int r3 = r3.size()
            r8 = 0
            if (r3 <= 0) goto L_0x00e5
            java.util.List<i.b.p.i.d$d> r3 = r0.f864j
            int r10 = r3.size()
            int r10 = r10 - r5
            java.lang.Object r3 = r3.get(r10)
            i.b.p.i.CascadingMenuPopup$d r3 = (i.b.p.i.CascadingMenuPopup.d) r3
            i.b.p.i.MenuBuilder r10 = r3.b
            int r11 = r10.size()
            r12 = 0
        L_0x0084:
            if (r12 >= r11) goto L_0x009a
            android.view.MenuItem r13 = r10.getItem(r12)
            boolean r14 = r13.hasSubMenu()
            if (r14 == 0) goto L_0x0097
            android.view.SubMenu r14 = r13.getSubMenu()
            if (r1 != r14) goto L_0x0097
            goto L_0x009b
        L_0x0097:
            int r12 = r12 + 1
            goto L_0x0084
        L_0x009a:
            r13 = r7
        L_0x009b:
            if (r13 != 0) goto L_0x009e
            goto L_0x00e7
        L_0x009e:
            i.b.q.MenuPopupWindow r10 = r3.a
            i.b.q.DropDownListView r10 = r10.d
            android.widget.ListAdapter r11 = r10.getAdapter()
            boolean r12 = r11 instanceof android.widget.HeaderViewListAdapter
            if (r12 == 0) goto L_0x00b7
            android.widget.HeaderViewListAdapter r11 = (android.widget.HeaderViewListAdapter) r11
            int r12 = r11.getHeadersCount()
            android.widget.ListAdapter r11 = r11.getWrappedAdapter()
            i.b.p.i.MenuAdapter r11 = (i.b.p.i.MenuAdapter) r11
            goto L_0x00ba
        L_0x00b7:
            i.b.p.i.MenuAdapter r11 = (i.b.p.i.MenuAdapter) r11
            r12 = 0
        L_0x00ba:
            int r14 = r11.getCount()
            r15 = 0
        L_0x00bf:
            r9 = -1
            if (r15 >= r14) goto L_0x00cd
            i.b.p.i.MenuItemImpl r7 = r11.getItem(r15)
            if (r13 != r7) goto L_0x00c9
            goto L_0x00ce
        L_0x00c9:
            int r15 = r15 + 1
            r7 = 0
            goto L_0x00bf
        L_0x00cd:
            r15 = -1
        L_0x00ce:
            if (r15 != r9) goto L_0x00d1
            goto L_0x00e6
        L_0x00d1:
            int r15 = r15 + r12
            int r7 = r10.getFirstVisiblePosition()
            int r15 = r15 - r7
            if (r15 < 0) goto L_0x00e6
            int r7 = r10.getChildCount()
            if (r15 < r7) goto L_0x00e0
            goto L_0x00e6
        L_0x00e0:
            android.view.View r7 = r10.getChildAt(r15)
            goto L_0x00e7
        L_0x00e5:
            r3 = 0
        L_0x00e6:
            r7 = 0
        L_0x00e7:
            if (r7 == 0) goto L_0x01b8
            int r9 = android.os.Build.VERSION.SDK_INT
            r10 = 28
            if (r9 > r10) goto L_0x0109
            java.lang.reflect.Method r9 = i.b.q.MenuPopupWindow.H
            if (r9 == 0) goto L_0x010e
            android.widget.PopupWindow r10 = r6.C     // Catch:{ Exception -> 0x0101 }
            java.lang.Object[] r11 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0101 }
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r8)     // Catch:{ Exception -> 0x0101 }
            r11[r8] = r12     // Catch:{ Exception -> 0x0101 }
            r9.invoke(r10, r11)     // Catch:{ Exception -> 0x0101 }
            goto L_0x010e
        L_0x0101:
            java.lang.String r9 = "MenuPopupWindow"
            java.lang.String r10 = "Could not invoke setTouchModal() on PopupWindow. Oh well."
            android.util.Log.i(r9, r10)
            goto L_0x010e
        L_0x0109:
            android.widget.PopupWindow r9 = r6.C
            r9.setTouchModal(r8)
        L_0x010e:
            int r9 = android.os.Build.VERSION.SDK_INT
            r10 = 23
            if (r9 < r10) goto L_0x011a
            android.widget.PopupWindow r9 = r6.C
            r10 = 0
            r9.setEnterTransition(r10)
        L_0x011a:
            java.util.List<i.b.p.i.d$d> r9 = r0.f864j
            int r10 = r9.size()
            int r10 = r10 - r5
            java.lang.Object r9 = r9.get(r10)
            i.b.p.i.CascadingMenuPopup$d r9 = (i.b.p.i.CascadingMenuPopup.d) r9
            i.b.q.MenuPopupWindow r9 = r9.a
            i.b.q.DropDownListView r9 = r9.d
            r10 = 2
            int[] r11 = new int[r10]
            r9.getLocationOnScreen(r11)
            android.graphics.Rect r10 = new android.graphics.Rect
            r10.<init>()
            android.view.View r12 = r0.f871q
            r12.getWindowVisibleDisplayFrame(r10)
            int r12 = r0.f872r
            if (r12 != r5) goto L_0x014c
            r11 = r11[r8]
            int r9 = r9.getWidth()
            int r9 = r9 + r11
            int r9 = r9 + r4
            int r10 = r10.right
            if (r9 <= r10) goto L_0x0151
            goto L_0x0153
        L_0x014c:
            r9 = r11[r8]
            int r9 = r9 - r4
            if (r9 >= 0) goto L_0x0153
        L_0x0151:
            r9 = 1
            goto L_0x0154
        L_0x0153:
            r9 = 0
        L_0x0154:
            if (r9 != r5) goto L_0x0158
            r10 = 1
            goto L_0x0159
        L_0x0158:
            r10 = 0
        L_0x0159:
            r0.f872r = r9
            int r9 = android.os.Build.VERSION.SDK_INT
            r11 = 26
            r12 = 5
            if (r9 < r11) goto L_0x0167
            r6.f1005s = r7
            r9 = 0
            r13 = 0
            goto L_0x0198
        L_0x0167:
            r9 = 2
            int[] r11 = new int[r9]
            android.view.View r13 = r0.f870p
            r13.getLocationOnScreen(r11)
            int[] r9 = new int[r9]
            r7.getLocationOnScreen(r9)
            int r13 = r0.f869o
            r13 = r13 & 7
            if (r13 != r12) goto L_0x018e
            r13 = r11[r8]
            android.view.View r14 = r0.f870p
            int r14 = r14.getWidth()
            int r14 = r14 + r13
            r11[r8] = r14
            r13 = r9[r8]
            int r14 = r7.getWidth()
            int r14 = r14 + r13
            r9[r8] = r14
        L_0x018e:
            r13 = r9[r8]
            r14 = r11[r8]
            int r13 = r13 - r14
            r9 = r9[r5]
            r11 = r11[r5]
            int r9 = r9 - r11
        L_0x0198:
            int r11 = r0.f869o
            r11 = r11 & r12
            if (r11 != r12) goto L_0x01a5
            if (r10 == 0) goto L_0x01a0
            goto L_0x01ab
        L_0x01a0:
            int r4 = r7.getWidth()
            goto L_0x01ad
        L_0x01a5:
            if (r10 == 0) goto L_0x01ad
            int r4 = r7.getWidth()
        L_0x01ab:
            int r13 = r13 + r4
            goto L_0x01ae
        L_0x01ad:
            int r13 = r13 - r4
        L_0x01ae:
            r6.g = r13
            r6.f998l = r5
            r6.f997k = r5
            r6.b(r9)
            goto L_0x01d6
        L_0x01b8:
            boolean r4 = r0.f873s
            if (r4 == 0) goto L_0x01c0
            int r4 = r0.u
            r6.g = r4
        L_0x01c0:
            boolean r4 = r0.f874t
            if (r4 == 0) goto L_0x01c9
            int r4 = r0.v
            r6.b(r4)
        L_0x01c9:
            android.graphics.Rect r4 = r0.b
            if (r4 == 0) goto L_0x01d3
            android.graphics.Rect r10 = new android.graphics.Rect
            r10.<init>(r4)
            goto L_0x01d4
        L_0x01d3:
            r10 = 0
        L_0x01d4:
            r6.A = r10
        L_0x01d6:
            i.b.p.i.CascadingMenuPopup$d r4 = new i.b.p.i.CascadingMenuPopup$d
            int r5 = r0.f872r
            r4.<init>(r6, r1, r5)
            java.util.List<i.b.p.i.d$d> r5 = r0.f864j
            r5.add(r4)
            r6.a()
            i.b.q.DropDownListView r4 = r6.d
            r4.setOnKeyListener(r0)
            if (r3 != 0) goto L_0x0214
            boolean r3 = r0.x
            if (r3 == 0) goto L_0x0214
            java.lang.CharSequence r3 = r1.f888m
            if (r3 == 0) goto L_0x0214
            int r3 = i.b.g.abc_popup_menu_header_item_layout
            android.view.View r2 = r2.inflate(r3, r4, r8)
            android.widget.FrameLayout r2 = (android.widget.FrameLayout) r2
            r3 = 16908310(0x1020016, float:2.387729E-38)
            android.view.View r3 = r2.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            r2.setEnabled(r8)
            java.lang.CharSequence r1 = r1.f888m
            r3.setText(r1)
            r1 = 0
            r4.addHeaderView(r2, r1, r8)
            r6.a()
        L_0x0214:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.p.i.CascadingMenuPopup.c(i.b.p.i.MenuBuilder):void");
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return false;
    }

    public void dismiss() {
        int size = this.f864j.size();
        if (size > 0) {
            d[] dVarArr = (d[]) this.f864j.toArray(new d[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                d dVar = dVarArr[i2];
                if (dVar.a.b()) {
                    dVar.a.dismiss();
                }
            }
        }
    }

    public ListView g() {
        if (this.f864j.isEmpty()) {
            return null;
        }
        List<d.d> list = this.f864j;
        return list.get(list.size() - 1).a.d;
    }

    public void onDismiss() {
        d dVar;
        int size = this.f864j.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                dVar = null;
                break;
            }
            dVar = (d) this.f864j.get(i2);
            if (!dVar.a.b()) {
                break;
            }
            i2++;
        }
        if (dVar != null) {
            dVar.b.a(false);
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public boolean b() {
        return this.f864j.size() > 0 && this.f864j.get(0).a.b();
    }

    public void b(int i2) {
        this.f873s = true;
        this.u = i2;
    }

    public void a(MenuBuilder menuBuilder) {
        menuBuilder.a(this, this.c);
        if (b()) {
            c(menuBuilder);
        } else {
            this.f863i.add(menuBuilder);
        }
    }

    public void a(boolean z2) {
        Iterator<d.d> it = this.f864j.iterator();
        while (it.hasNext()) {
            ListAdapter adapter = it.next().a.d.getAdapter();
            if (adapter instanceof HeaderViewListAdapter) {
                adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
            }
            ((MenuAdapter) adapter).notifyDataSetChanged();
        }
    }

    public void a(MenuPresenter.a aVar) {
        this.y = aVar;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        for (d next : this.f864j) {
            if (subMenuBuilder == next.b) {
                next.a.d.requestFocus();
                return true;
            }
        }
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        subMenuBuilder.a(this, this.c);
        if (b()) {
            c(subMenuBuilder);
        } else {
            this.f863i.add(subMenuBuilder);
        }
        MenuPresenter.a aVar = this.y;
        if (aVar != null) {
            aVar.a(subMenuBuilder);
        }
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z2) {
        int size = this.f864j.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                i2 = -1;
                break;
            } else if (menuBuilder == this.f864j.get(i2).b) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 >= 0) {
            int i3 = i2 + 1;
            if (i3 < this.f864j.size()) {
                this.f864j.get(i3).b.a(false);
            }
            d remove = this.f864j.remove(i2);
            remove.b.a(this);
            if (this.B) {
                MenuPopupWindow menuPopupWindow = remove.a;
                if (menuPopupWindow != null) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        menuPopupWindow.C.setExitTransition(null);
                    }
                    remove.a.C.setAnimationStyle(0);
                } else {
                    throw null;
                }
            }
            remove.a.dismiss();
            int size2 = this.f864j.size();
            if (size2 > 0) {
                this.f872r = this.f864j.get(size2 - 1).c;
            } else {
                this.f872r = ViewCompat.k(this.f870p) == 1 ? 0 : 1;
            }
            if (size2 == 0) {
                dismiss();
                MenuPresenter.a aVar = this.y;
                if (aVar != null) {
                    aVar.a(menuBuilder, true);
                }
                ViewTreeObserver viewTreeObserver = this.z;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.z.removeGlobalOnLayoutListener(this.f865k);
                    }
                    this.z = null;
                }
                this.f871q.removeOnAttachStateChangeListener(this.f866l);
                this.A.onDismiss();
            } else if (z2) {
                this.f864j.get(0).b.a(false);
            }
        }
    }

    public void a(int i2) {
        if (this.f868n != i2) {
            this.f868n = i2;
            this.f869o = Gravity.getAbsoluteGravity(i2, ViewCompat.k(this.f870p));
        }
    }

    public void a(View view) {
        if (this.f870p != view) {
            this.f870p = view;
            this.f869o = Gravity.getAbsoluteGravity(this.f868n, ViewCompat.k(view));
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.A = onDismissListener;
    }

    public void c(int i2) {
        this.f874t = true;
        this.v = i2;
    }

    public void c(boolean z2) {
        this.x = z2;
    }
}
