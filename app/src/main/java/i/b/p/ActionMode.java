package i.b.p;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public abstract class ActionMode {
    public Object b;
    public boolean c;

    public interface a {
        void a(ActionMode actionMode);

        boolean a(ActionMode actionMode, Menu menu);

        boolean a(ActionMode actionMode, MenuItem menuItem);

        boolean b(ActionMode actionMode, Menu menu);
    }

    public abstract void a();

    public abstract void a(int i2);

    public abstract void a(View view);

    public abstract void a(CharSequence charSequence);

    public abstract void a(boolean z);

    public abstract View b();

    public abstract void b(int i2);

    public abstract void b(CharSequence charSequence);

    public abstract Menu c();

    public abstract MenuInflater d();

    public abstract CharSequence e();

    public abstract CharSequence f();

    public abstract void g();

    public abstract boolean h();
}
