package i.b.q;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;
import i.b.j;
import i.b.k.ResourcesFlusher;
import i.h.l.ViewCompat;

public class AppCompatSeekBarHelper extends AppCompatProgressBarHelper {
    public final SeekBar d;

    /* renamed from: e  reason: collision with root package name */
    public Drawable f955e;

    /* renamed from: f  reason: collision with root package name */
    public ColorStateList f956f = null;
    public PorterDuff.Mode g = null;
    public boolean h = false;

    /* renamed from: i  reason: collision with root package name */
    public boolean f957i = false;

    public AppCompatSeekBarHelper(SeekBar seekBar) {
        super(seekBar);
        this.d = seekBar;
    }

    public void a(AttributeSet attributeSet, int i2) {
        super.a(attributeSet, i2);
        TintTypedArray a = TintTypedArray.a(this.d.getContext(), attributeSet, j.AppCompatSeekBar, i2, 0);
        Drawable c = a.c(j.AppCompatSeekBar_android_thumb);
        if (c != null) {
            this.d.setThumb(c);
        }
        Drawable b = a.b(j.AppCompatSeekBar_tickMark);
        Drawable drawable = this.f955e;
        if (drawable != null) {
            drawable.setCallback(null);
        }
        this.f955e = b;
        if (b != null) {
            b.setCallback(this.d);
            ResourcesFlusher.a(b, ViewCompat.k(this.d));
            if (b.isStateful()) {
                b.setState(this.d.getDrawableState());
            }
            a();
        }
        this.d.invalidate();
        if (a.f(j.AppCompatSeekBar_tickMarkTintMode)) {
            this.g = DrawableUtils.a(a.d(j.AppCompatSeekBar_tickMarkTintMode, -1), this.g);
            this.f957i = true;
        }
        if (a.f(j.AppCompatSeekBar_tickMarkTint)) {
            this.f956f = a.a(j.AppCompatSeekBar_tickMarkTint);
            this.h = true;
        }
        a.b.recycle();
        a();
    }

    public final void a() {
        if (this.f955e == null) {
            return;
        }
        if (this.h || this.f957i) {
            Drawable d2 = ResourcesFlusher.d(this.f955e.mutate());
            this.f955e = d2;
            if (this.h) {
                d2.setTintList(this.f956f);
            }
            if (this.f957i) {
                this.f955e.setTintMode(this.g);
            }
            if (this.f955e.isStateful()) {
                this.f955e.setState(this.d.getDrawableState());
            }
        }
    }

    public void a(Canvas canvas) {
        if (this.f955e != null) {
            int max = this.d.getMax();
            int i2 = 1;
            if (max > 1) {
                int intrinsicWidth = this.f955e.getIntrinsicWidth();
                int intrinsicHeight = this.f955e.getIntrinsicHeight();
                int i3 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
                if (intrinsicHeight >= 0) {
                    i2 = intrinsicHeight / 2;
                }
                this.f955e.setBounds(-i3, -i2, i3, i2);
                float width = ((float) ((this.d.getWidth() - this.d.getPaddingLeft()) - this.d.getPaddingRight())) / ((float) max);
                int save = canvas.save();
                canvas.translate((float) this.d.getPaddingLeft(), (float) (this.d.getHeight() / 2));
                for (int i4 = 0; i4 <= max; i4++) {
                    this.f955e.draw(canvas);
                    canvas.translate(width, 0.0f);
                }
                canvas.restoreToCount(save);
            }
        }
    }
}
