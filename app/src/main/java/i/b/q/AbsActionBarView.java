package i.b.q;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import androidx.recyclerview.widget.RecyclerView;
import i.b.j;
import i.b.p.i.MenuBuilder;
import i.h.l.ViewCompat;
import i.h.l.ViewPropertyAnimatorCompat;
import i.h.l.ViewPropertyAnimatorListener;

public abstract class AbsActionBarView extends ViewGroup {
    public final a b;
    public final Context c;
    public ActionMenuView d;

    /* renamed from: e  reason: collision with root package name */
    public ActionMenuPresenter f932e;

    /* renamed from: f  reason: collision with root package name */
    public int f933f;
    public ViewPropertyAnimatorCompat g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f934i;

    public class a implements ViewPropertyAnimatorListener {
        public boolean a = false;
        public int b;

        public a() {
        }

        public void a(View view) {
            if (!this.a) {
                AbsActionBarView absActionBarView = AbsActionBarView.this;
                absActionBarView.g = null;
                AbsActionBarView.super.setVisibility(this.b);
            }
        }

        public void b(View view) {
            AbsActionBarView.super.setVisibility(0);
            this.a = false;
        }

        public void c(View view) {
            this.a = true;
        }
    }

    public AbsActionBarView(Context context) {
        this(context, null);
    }

    public int getAnimatedVisibility() {
        if (this.g != null) {
            return this.b.b;
        }
        return getVisibility();
    }

    public int getContentHeight() {
        return this.f933f;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, j.ActionBar, i.b.a.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(j.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        ActionMenuPresenter actionMenuPresenter = this.f932e;
        if (actionMenuPresenter != null) {
            Configuration configuration2 = actionMenuPresenter.c.getResources().getConfiguration();
            int i2 = configuration2.screenWidthDp;
            int i3 = configuration2.screenHeightDp;
            actionMenuPresenter.f942q = (configuration2.smallestScreenWidthDp > 600 || i2 > 600 || (i2 > 960 && i3 > 720) || (i2 > 720 && i3 > 960)) ? 5 : (i2 >= 500 || (i2 > 640 && i3 > 480) || (i2 > 480 && i3 > 640)) ? 4 : i2 >= 360 ? 3 : 2;
            MenuBuilder menuBuilder = actionMenuPresenter.d;
            if (menuBuilder != null) {
                menuBuilder.b(true);
            }
        }
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.f934i = false;
        }
        if (!this.f934i) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.f934i = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.f934i = false;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.h = false;
        }
        if (!this.h) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.h = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.h = false;
        }
        return true;
    }

    public abstract void setContentHeight(int i2);

    public void setVisibility(int i2) {
        if (i2 != getVisibility()) {
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.g;
            if (viewPropertyAnimatorCompat != null) {
                viewPropertyAnimatorCompat.a();
            }
            super.setVisibility(i2);
        }
    }

    public AbsActionBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ViewPropertyAnimatorCompat a(int i2, long j2) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.g;
        if (viewPropertyAnimatorCompat != null) {
            viewPropertyAnimatorCompat.a();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                setAlpha(0.0f);
            }
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this);
            a2.a(1.0f);
            a2.a(j2);
            a aVar = this.b;
            AbsActionBarView.this.g = a2;
            aVar.b = i2;
            View view = a2.a.get();
            if (view != null) {
                a2.a(view, aVar);
            }
            return a2;
        }
        ViewPropertyAnimatorCompat a3 = ViewCompat.a(this);
        a3.a(0.0f);
        a3.a(j2);
        a aVar2 = this.b;
        AbsActionBarView.this.g = a3;
        aVar2.b = i2;
        View view2 = a3.a.get();
        if (view2 != null) {
            a3.a(view2, aVar2);
        }
        return a3;
    }

    public AbsActionBarView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = new a();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(i.b.a.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.c = context;
        } else {
            this.c = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    public int a(View view, int i2, int i3, int i4) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i2, RecyclerView.UNDEFINED_DURATION), i3);
        return Math.max(0, (i2 - view.getMeasuredWidth()) - i4);
    }

    public int a(View view, int i2, int i3, int i4, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = ((i4 - measuredHeight) / 2) + i3;
        if (z) {
            view.layout(i2 - measuredWidth, i5, i2, measuredHeight + i5);
        } else {
            view.layout(i2, i5, i2 + measuredWidth, measuredHeight + i5);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
