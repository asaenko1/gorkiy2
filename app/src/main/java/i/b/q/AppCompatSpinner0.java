package i.b.q;

import android.annotation.SuppressLint;
import android.view.View;
import i.b.p.i.ShowableListMenu;
import i.b.q.AppCompatSpinner;

/* compiled from: AppCompatSpinner */
public class AppCompatSpinner0 extends ForwardingListener {

    /* renamed from: k  reason: collision with root package name */
    public final /* synthetic */ AppCompatSpinner.d f963k;

    /* renamed from: l  reason: collision with root package name */
    public final /* synthetic */ AppCompatSpinner f964l;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AppCompatSpinner0(AppCompatSpinner appCompatSpinner, View view, AppCompatSpinner.d dVar) {
        super(view);
        this.f964l = appCompatSpinner;
        this.f963k = dVar;
    }

    public ShowableListMenu b() {
        return this.f963k;
    }

    @SuppressLint({"SyntheticAccessor"})
    public boolean c() {
        if (this.f964l.getInternalPopup().b()) {
            return true;
        }
        this.f964l.a();
        return true;
    }
}
