package i.b.q;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.RecyclerView;
import i.b.j;
import i.b.k.ResourcesFlusher;
import i.b.p.i.ShowableListMenu;
import i.h.l.ViewCompat;
import java.lang.reflect.Method;

/* compiled from: ListPopupWindow */
public class ListPopupWindow0 implements ShowableListMenu {
    public static Method D;
    public static Method E;
    public static Method F;
    public Rect A;
    public boolean B;
    public PopupWindow C;
    public Context b;
    public ListAdapter c;
    public DropDownListView d;

    /* renamed from: e  reason: collision with root package name */
    public int f993e = -2;

    /* renamed from: f  reason: collision with root package name */
    public int f994f = -2;
    public int g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f995i = 1002;

    /* renamed from: j  reason: collision with root package name */
    public boolean f996j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f997k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f998l;

    /* renamed from: m  reason: collision with root package name */
    public int f999m = 0;

    /* renamed from: n  reason: collision with root package name */
    public boolean f1000n = false;

    /* renamed from: o  reason: collision with root package name */
    public boolean f1001o = false;

    /* renamed from: p  reason: collision with root package name */
    public int f1002p = Integer.MAX_VALUE;

    /* renamed from: q  reason: collision with root package name */
    public int f1003q = 0;

    /* renamed from: r  reason: collision with root package name */
    public DataSetObserver f1004r;

    /* renamed from: s  reason: collision with root package name */
    public View f1005s;

    /* renamed from: t  reason: collision with root package name */
    public AdapterView.OnItemClickListener f1006t;
    public final e u = new e();
    public final d v = new d();
    public final c w = new c();
    public final a x = new a();
    public final Handler y;
    public final Rect z = new Rect();

    /* compiled from: ListPopupWindow */
    public class a implements Runnable {
        public a() {
        }

        public void run() {
            DropDownListView dropDownListView = ListPopupWindow0.this.d;
            if (dropDownListView != null) {
                dropDownListView.setListSelectionHidden(true);
                dropDownListView.requestLayout();
            }
        }
    }

    /* compiled from: ListPopupWindow */
    public class b extends DataSetObserver {
        public b() {
        }

        public void onChanged() {
            if (ListPopupWindow0.this.b()) {
                ListPopupWindow0.this.a();
            }
        }

        public void onInvalidated() {
            ListPopupWindow0.this.dismiss();
        }
    }

    /* compiled from: ListPopupWindow */
    public class c implements AbsListView.OnScrollListener {
        public c() {
        }

        public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        }

        public void onScrollStateChanged(AbsListView absListView, int i2) {
            boolean z = true;
            if (i2 == 1) {
                if (ListPopupWindow0.this.C.getInputMethodMode() != 2) {
                    z = false;
                }
                if (!z && ListPopupWindow0.this.C.getContentView() != null) {
                    ListPopupWindow0 listPopupWindow0 = ListPopupWindow0.this;
                    listPopupWindow0.y.removeCallbacks(listPopupWindow0.u);
                    ListPopupWindow0.this.u.run();
                }
            }
        }
    }

    /* compiled from: ListPopupWindow */
    public class d implements View.OnTouchListener {
        public d() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            PopupWindow popupWindow;
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && (popupWindow = ListPopupWindow0.this.C) != null && popupWindow.isShowing() && x >= 0 && x < ListPopupWindow0.this.C.getWidth() && y >= 0 && y < ListPopupWindow0.this.C.getHeight()) {
                ListPopupWindow0 listPopupWindow0 = ListPopupWindow0.this;
                listPopupWindow0.y.postDelayed(listPopupWindow0.u, 250);
                return false;
            } else if (action != 1) {
                return false;
            } else {
                ListPopupWindow0 listPopupWindow02 = ListPopupWindow0.this;
                listPopupWindow02.y.removeCallbacks(listPopupWindow02.u);
                return false;
            }
        }
    }

    /* compiled from: ListPopupWindow */
    public class e implements Runnable {
        public e() {
        }

        public void run() {
            DropDownListView dropDownListView = ListPopupWindow0.this.d;
            if (dropDownListView != null && ViewCompat.v(dropDownListView) && ListPopupWindow0.this.d.getCount() > ListPopupWindow0.this.d.getChildCount()) {
                int childCount = ListPopupWindow0.this.d.getChildCount();
                ListPopupWindow0 listPopupWindow0 = ListPopupWindow0.this;
                if (childCount <= listPopupWindow0.f1002p) {
                    listPopupWindow0.C.setInputMethodMode(2);
                    ListPopupWindow0.this.a();
                }
            }
        }
    }

    static {
        if (Build.VERSION.SDK_INT <= 28) {
            Class<PopupWindow> cls = PopupWindow.class;
            try {
                D = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
            } catch (NoSuchMethodException unused) {
                Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
            try {
                F = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
            } catch (NoSuchMethodException unused2) {
                Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
            }
        }
        if (Build.VERSION.SDK_INT <= 23) {
            Class<PopupWindow> cls2 = PopupWindow.class;
            try {
                E = cls2.getDeclaredMethod("getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE);
            } catch (NoSuchMethodException unused3) {
                Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
            }
        }
    }

    public ListPopupWindow0(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.b = context;
        this.y = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ListPopupWindow, i2, i3);
        this.g = obtainStyledAttributes.getDimensionPixelOffset(j.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(j.ListPopupWindow_android_dropDownVerticalOffset, 0);
        this.h = dimensionPixelOffset;
        if (dimensionPixelOffset != 0) {
            this.f996j = true;
        }
        obtainStyledAttributes.recycle();
        AppCompatPopupWindow appCompatPopupWindow = new AppCompatPopupWindow(context, attributeSet, i2, i3);
        this.C = appCompatPopupWindow;
        appCompatPopupWindow.setInputMethodMode(1);
    }

    public void a(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver = this.f1004r;
        if (dataSetObserver == null) {
            this.f1004r = new b();
        } else {
            ListAdapter listAdapter2 = this.c;
            if (listAdapter2 != null) {
                listAdapter2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.c = listAdapter;
        if (listAdapter != null) {
            listAdapter.registerDataSetObserver(this.f1004r);
        }
        DropDownListView dropDownListView = this.d;
        if (dropDownListView != null) {
            dropDownListView.setAdapter(this.c);
        }
    }

    public void b(int i2) {
        this.h = i2;
        this.f996j = true;
    }

    public int c() {
        return this.g;
    }

    public int d() {
        if (!this.f996j) {
            return 0;
        }
        return this.h;
    }

    public void dismiss() {
        this.C.dismiss();
        this.C.setContentView(null);
        this.d = null;
        this.y.removeCallbacks(this.u);
    }

    public Drawable e() {
        return this.C.getBackground();
    }

    public ListView g() {
        return this.d;
    }

    public boolean b() {
        return this.C.isShowing();
    }

    public void d(int i2) {
        Drawable background = this.C.getBackground();
        if (background != null) {
            background.getPadding(this.z);
            Rect rect = this.z;
            this.f994f = rect.left + rect.right + i2;
            return;
        }
        this.f994f = i2;
    }

    public void a(boolean z2) {
        this.B = z2;
        this.C.setFocusable(z2);
    }

    public void a(Drawable drawable) {
        this.C.setBackgroundDrawable(drawable);
    }

    public void a(int i2) {
        this.g = i2;
    }

    public DropDownListView a(Context context, boolean z2) {
        return new DropDownListView(context, z2);
    }

    public void a() {
        int i2;
        int i3;
        int i4;
        DropDownListView dropDownListView;
        int i5;
        boolean z2 = true;
        if (this.d == null) {
            DropDownListView a2 = a(this.b, !this.B);
            this.d = a2;
            a2.setAdapter(this.c);
            this.d.setOnItemClickListener(this.f1006t);
            this.d.setFocusable(true);
            this.d.setFocusableInTouchMode(true);
            this.d.setOnItemSelectedListener(new ListPopupWindow(this));
            this.d.setOnScrollListener(this.w);
            this.C.setContentView(this.d);
        } else {
            ViewGroup viewGroup = (ViewGroup) this.C.getContentView();
        }
        Drawable background = this.C.getBackground();
        if (background != null) {
            background.getPadding(this.z);
            Rect rect = this.z;
            int i6 = rect.top;
            i2 = rect.bottom + i6;
            if (!this.f996j) {
                this.h = -i6;
            }
        } else {
            this.z.setEmpty();
            i2 = 0;
        }
        boolean z3 = this.C.getInputMethodMode() == 2;
        View view = this.f1005s;
        int i7 = this.h;
        if (Build.VERSION.SDK_INT <= 23) {
            Method method = E;
            if (method != null) {
                try {
                    i3 = ((Integer) method.invoke(this.C, view, Integer.valueOf(i7), Boolean.valueOf(z3))).intValue();
                } catch (Exception unused) {
                    Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
                }
            }
            i3 = this.C.getMaxAvailableHeight(view, i7);
        } else {
            i3 = this.C.getMaxAvailableHeight(view, i7, z3);
        }
        if (this.f1000n || this.f993e == -1) {
            i4 = i3 + i2;
        } else {
            int i8 = this.f994f;
            if (i8 == -2) {
                int i9 = this.b.getResources().getDisplayMetrics().widthPixels;
                Rect rect2 = this.z;
                i5 = View.MeasureSpec.makeMeasureSpec(i9 - (rect2.left + rect2.right), RecyclerView.UNDEFINED_DURATION);
            } else if (i8 != -1) {
                i5 = View.MeasureSpec.makeMeasureSpec(i8, 1073741824);
            } else {
                int i10 = this.b.getResources().getDisplayMetrics().widthPixels;
                Rect rect3 = this.z;
                i5 = View.MeasureSpec.makeMeasureSpec(i10 - (rect3.left + rect3.right), 1073741824);
            }
            int a3 = this.d.a(i5, i3 - 0, -1);
            i4 = a3 + (a3 > 0 ? this.d.getPaddingBottom() + this.d.getPaddingTop() + i2 + 0 : 0);
        }
        boolean z4 = this.C.getInputMethodMode() == 2;
        ResourcesFlusher.a(this.C, this.f995i);
        if (!this.C.isShowing()) {
            int i11 = this.f994f;
            if (i11 == -1) {
                i11 = -1;
            } else if (i11 == -2) {
                i11 = this.f1005s.getWidth();
            }
            int i12 = this.f993e;
            if (i12 == -1) {
                i4 = -1;
            } else if (i12 != -2) {
                i4 = i12;
            }
            this.C.setWidth(i11);
            this.C.setHeight(i4);
            if (Build.VERSION.SDK_INT <= 28) {
                Method method2 = D;
                if (method2 != null) {
                    try {
                        method2.invoke(this.C, true);
                    } catch (Exception unused2) {
                        Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
                    }
                }
            } else {
                this.C.setIsClippedToScreen(true);
            }
            this.C.setOutsideTouchable(!this.f1001o && !this.f1000n);
            this.C.setTouchInterceptor(this.v);
            if (this.f998l) {
                ResourcesFlusher.a(this.C, this.f997k);
            }
            if (Build.VERSION.SDK_INT <= 28) {
                Method method3 = F;
                if (method3 != null) {
                    try {
                        method3.invoke(this.C, this.A);
                    } catch (Exception e2) {
                        Log.e("ListPopupWindow", "Could not invoke setEpicenterBounds on PopupWindow", e2);
                    }
                }
            } else {
                this.C.setEpicenterBounds(this.A);
            }
            this.C.showAsDropDown(this.f1005s, this.g, this.h, this.f999m);
            this.d.setSelection(-1);
            if ((!this.B || this.d.isInTouchMode()) && (dropDownListView = this.d) != null) {
                dropDownListView.setListSelectionHidden(true);
                dropDownListView.requestLayout();
            }
            if (!this.B) {
                this.y.post(this.x);
            }
        } else if (ViewCompat.v(this.f1005s)) {
            int i13 = this.f994f;
            if (i13 == -1) {
                i13 = -1;
            } else if (i13 == -2) {
                i13 = this.f1005s.getWidth();
            }
            int i14 = this.f993e;
            if (i14 == -1) {
                if (!z4) {
                    i4 = -1;
                }
                if (z4) {
                    this.C.setWidth(this.f994f == -1 ? -1 : 0);
                    this.C.setHeight(0);
                } else {
                    this.C.setWidth(this.f994f == -1 ? -1 : 0);
                    this.C.setHeight(-1);
                }
            } else if (i14 != -2) {
                i4 = i14;
            }
            PopupWindow popupWindow = this.C;
            if (this.f1001o || this.f1000n) {
                z2 = false;
            }
            popupWindow.setOutsideTouchable(z2);
            this.C.update(this.f1005s, this.g, this.h, i13 < 0 ? -1 : i13, i4 < 0 ? -1 : i4);
        }
    }
}
