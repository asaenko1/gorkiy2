package i.b.k;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AlertController;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.widget.NestedScrollView;
import i.b.f;
import i.h.l.ViewCompat;

public class AlertDialog extends AppCompatDialog implements DialogInterface {
    public final AlertController d = new AlertController(getContext(), super, getWindow());

    public static class a {
        public final AlertController.b a;
        public final int b;

        public a(Context context) {
            this(context, AlertDialog.a(context, 0));
        }

        public a a(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.b bVar = this.a;
            bVar.f22k = charSequence;
            bVar.f23l = onClickListener;
            return this;
        }

        public a b(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.b bVar = this.a;
            bVar.f20i = charSequence;
            bVar.f21j = onClickListener;
            return this;
        }

        public a(Context context, int i2) {
            this.a = new AlertController.b(new ContextThemeWrapper(context, AlertDialog.a(context, i2)));
            this.b = i2;
        }

        public AlertDialog a() {
            int i2;
            AlertDialog alertDialog = new AlertDialog(this.a.a, this.b);
            AlertController.b bVar = this.a;
            AlertController alertController = alertDialog.d;
            View view = bVar.g;
            if (view != null) {
                alertController.G = view;
            } else {
                CharSequence charSequence = bVar.f19f;
                if (charSequence != null) {
                    alertController.f4e = charSequence;
                    TextView textView = alertController.E;
                    if (textView != null) {
                        textView.setText(charSequence);
                    }
                }
                Drawable drawable = bVar.d;
                if (drawable != null) {
                    alertController.C = drawable;
                    alertController.B = 0;
                    ImageView imageView = alertController.D;
                    if (imageView != null) {
                        imageView.setVisibility(0);
                        alertController.D.setImageDrawable(drawable);
                    }
                }
                int i3 = bVar.c;
                if (i3 != 0) {
                    alertController.a(i3);
                }
                int i4 = bVar.f18e;
                if (i4 != 0) {
                    if (alertController != null) {
                        TypedValue typedValue = new TypedValue();
                        alertController.a.getTheme().resolveAttribute(i4, typedValue, true);
                        alertController.a(typedValue.resourceId);
                    } else {
                        throw null;
                    }
                }
            }
            CharSequence charSequence2 = bVar.h;
            if (charSequence2 != null) {
                alertController.f5f = charSequence2;
                TextView textView2 = alertController.F;
                if (textView2 != null) {
                    textView2.setText(charSequence2);
                }
            }
            CharSequence charSequence3 = bVar.f20i;
            if (charSequence3 != null) {
                alertController.a(-1, charSequence3, bVar.f21j, null, null);
            }
            CharSequence charSequence4 = bVar.f22k;
            if (charSequence4 != null) {
                alertController.a(-2, charSequence4, bVar.f23l, null, null);
            }
            if (bVar.f28q != null) {
                AlertController.RecycleListView recycleListView = (AlertController.RecycleListView) bVar.b.inflate(alertController.L, (ViewGroup) null);
                if (bVar.f31t) {
                    i2 = alertController.N;
                } else {
                    i2 = alertController.O;
                }
                ListAdapter listAdapter = bVar.f28q;
                if (listAdapter == null) {
                    listAdapter = new AlertController.d(bVar.a, i2, 16908308, null);
                }
                alertController.H = listAdapter;
                alertController.I = bVar.u;
                if (bVar.f29r != null) {
                    recycleListView.setOnItemClickListener(new AlertController2(bVar, alertController));
                }
                if (bVar.f31t) {
                    recycleListView.setChoiceMode(1);
                }
                alertController.g = recycleListView;
            }
            alertDialog.setCancelable(this.a.f24m);
            if (this.a.f24m) {
                alertDialog.setCanceledOnTouchOutside(true);
            }
            alertDialog.setOnCancelListener(this.a.f25n);
            alertDialog.setOnDismissListener(this.a.f26o);
            DialogInterface.OnKeyListener onKeyListener = this.a.f27p;
            if (onKeyListener != null) {
                alertDialog.setOnKeyListener(onKeyListener);
            }
            return alertDialog;
        }

        public AlertDialog b() {
            AlertDialog a2 = a();
            a2.show();
            return a2;
        }
    }

    public AlertDialog(Context context, int i2) {
        super(context, a(context, i2));
    }

    public static int a(Context context, int i2) {
        if (((i2 >>> 24) & 255) >= 1) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i.b.a.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onCreate(Bundle bundle) {
        boolean z;
        ListAdapter listAdapter;
        View view;
        View findViewById;
        super.onCreate(bundle);
        AlertController alertController = this.d;
        int i2 = alertController.K;
        if (i2 == 0) {
            i2 = alertController.J;
        } else if (alertController.Q != 1) {
            i2 = alertController.J;
        }
        alertController.b.setContentView(i2);
        View findViewById2 = alertController.c.findViewById(f.parentPanel);
        View findViewById3 = findViewById2.findViewById(f.topPanel);
        View findViewById4 = findViewById2.findViewById(f.contentPanel);
        View findViewById5 = findViewById2.findViewById(f.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById2.findViewById(f.customPanel);
        View view2 = alertController.h;
        View view3 = null;
        int i3 = 0;
        if (view2 == null) {
            view2 = alertController.f6i != 0 ? LayoutInflater.from(alertController.a).inflate(alertController.f6i, viewGroup, false) : null;
        }
        boolean z2 = view2 != null;
        if (!z2 || !AlertController.a(view2)) {
            alertController.c.setFlags(131072, 131072);
        }
        if (z2) {
            FrameLayout frameLayout = (FrameLayout) alertController.c.findViewById(f.custom);
            frameLayout.addView(view2, new ViewGroup.LayoutParams(-1, -1));
            if (alertController.f11n) {
                frameLayout.setPadding(alertController.f7j, alertController.f8k, alertController.f9l, alertController.f10m);
            }
            if (alertController.g != null) {
                ((LinearLayoutCompat.a) viewGroup.getLayoutParams()).a = 0.0f;
            }
        } else {
            viewGroup.setVisibility(8);
        }
        View findViewById6 = viewGroup.findViewById(f.topPanel);
        View findViewById7 = viewGroup.findViewById(f.contentPanel);
        View findViewById8 = viewGroup.findViewById(f.buttonPanel);
        ViewGroup a2 = alertController.a(findViewById6, findViewById3);
        ViewGroup a3 = alertController.a(findViewById7, findViewById4);
        ViewGroup a4 = alertController.a(findViewById8, findViewById5);
        NestedScrollView nestedScrollView = (NestedScrollView) alertController.c.findViewById(f.scrollView);
        alertController.A = nestedScrollView;
        nestedScrollView.setFocusable(false);
        alertController.A.setNestedScrollingEnabled(false);
        TextView textView = (TextView) a3.findViewById(16908299);
        alertController.F = textView;
        if (textView != null) {
            CharSequence charSequence = alertController.f5f;
            if (charSequence != null) {
                textView.setText(charSequence);
            } else {
                textView.setVisibility(8);
                alertController.A.removeView(alertController.F);
                if (alertController.g != null) {
                    ViewGroup viewGroup2 = (ViewGroup) alertController.A.getParent();
                    int indexOfChild = viewGroup2.indexOfChild(alertController.A);
                    viewGroup2.removeViewAt(indexOfChild);
                    viewGroup2.addView(alertController.g, indexOfChild, new ViewGroup.LayoutParams(-1, -1));
                } else {
                    a3.setVisibility(8);
                }
            }
        }
        Button button = (Button) a4.findViewById(16908313);
        alertController.f12o = button;
        button.setOnClickListener(alertController.S);
        if (!TextUtils.isEmpty(alertController.f13p) || alertController.f15r != null) {
            alertController.f12o.setText(alertController.f13p);
            Drawable drawable = alertController.f15r;
            if (drawable != null) {
                int i4 = alertController.d;
                drawable.setBounds(0, 0, i4, i4);
                alertController.f12o.setCompoundDrawables(alertController.f15r, null, null, null);
            }
            alertController.f12o.setVisibility(0);
            z = true;
        } else {
            alertController.f12o.setVisibility(8);
            z = false;
        }
        Button button2 = (Button) a4.findViewById(16908314);
        alertController.f16s = button2;
        button2.setOnClickListener(alertController.S);
        if (!TextUtils.isEmpty(alertController.f17t) || alertController.v != null) {
            alertController.f16s.setText(alertController.f17t);
            Drawable drawable2 = alertController.v;
            if (drawable2 != null) {
                int i5 = alertController.d;
                drawable2.setBounds(0, 0, i5, i5);
                alertController.f16s.setCompoundDrawables(alertController.v, null, null, null);
            }
            alertController.f16s.setVisibility(0);
            z |= true;
        } else {
            alertController.f16s.setVisibility(8);
        }
        Button button3 = (Button) a4.findViewById(16908315);
        alertController.w = button3;
        button3.setOnClickListener(alertController.S);
        if (!TextUtils.isEmpty(alertController.x) || alertController.z != null) {
            alertController.w.setText(alertController.x);
            Drawable drawable3 = alertController.f15r;
            if (drawable3 != null) {
                int i6 = alertController.d;
                drawable3.setBounds(0, 0, i6, i6);
                alertController.f12o.setCompoundDrawables(alertController.f15r, null, null, null);
            }
            alertController.w.setVisibility(0);
            z |= true;
        } else {
            alertController.w.setVisibility(8);
        }
        Context context = alertController.a;
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i.b.a.alertDialogCenterButtons, typedValue, true);
        if (typedValue.data != 0) {
            if (z) {
                alertController.a(alertController.f12o);
            } else if (z) {
                alertController.a(alertController.f16s);
            } else if (z) {
                alertController.a(alertController.w);
            }
        }
        if (!(z)) {
            a4.setVisibility(8);
        }
        if (alertController.G != null) {
            a2.addView(alertController.G, 0, new ViewGroup.LayoutParams(-1, -2));
            alertController.c.findViewById(f.title_template).setVisibility(8);
        } else {
            alertController.D = (ImageView) alertController.c.findViewById(16908294);
            if (!(!TextUtils.isEmpty(alertController.f4e)) || !alertController.P) {
                alertController.c.findViewById(f.title_template).setVisibility(8);
                alertController.D.setVisibility(8);
                a2.setVisibility(8);
            } else {
                TextView textView2 = (TextView) alertController.c.findViewById(f.alertTitle);
                alertController.E = textView2;
                textView2.setText(alertController.f4e);
                int i7 = alertController.B;
                if (i7 != 0) {
                    alertController.D.setImageResource(i7);
                } else {
                    Drawable drawable4 = alertController.C;
                    if (drawable4 != null) {
                        alertController.D.setImageDrawable(drawable4);
                    } else {
                        alertController.E.setPadding(alertController.D.getPaddingLeft(), alertController.D.getPaddingTop(), alertController.D.getPaddingRight(), alertController.D.getPaddingBottom());
                        alertController.D.setVisibility(8);
                    }
                }
            }
        }
        boolean z3 = viewGroup.getVisibility() != 8;
        int i8 = (a2 == null || a2.getVisibility() == 8) ? 0 : 1;
        boolean z4 = a4.getVisibility() != 8;
        if (!z4 && (findViewById = a3.findViewById(f.textSpacerNoButtons)) != null) {
            findViewById.setVisibility(0);
        }
        if (i8 != 0) {
            NestedScrollView nestedScrollView2 = alertController.A;
            if (nestedScrollView2 != null) {
                nestedScrollView2.setClipToPadding(true);
            }
            if (alertController.f5f == null && alertController.g == null) {
                view = null;
            } else {
                view = a2.findViewById(f.titleDividerNoCustom);
            }
            if (view != null) {
                view.setVisibility(0);
            }
        } else {
            View findViewById9 = a3.findViewById(f.textSpacerNoTitle);
            if (findViewById9 != null) {
                findViewById9.setVisibility(0);
            }
        }
        ListView listView = alertController.g;
        if (listView instanceof AlertController.RecycleListView) {
            AlertController.RecycleListView recycleListView = (AlertController.RecycleListView) listView;
            if (recycleListView == null) {
                throw null;
            } else if (!z4 || i8 == 0) {
                recycleListView.setPadding(recycleListView.getPaddingLeft(), i8 != 0 ? recycleListView.getPaddingTop() : recycleListView.b, recycleListView.getPaddingRight(), z4 ? recycleListView.getPaddingBottom() : recycleListView.c);
            }
        }
        if (!z3) {
            View view4 = alertController.g;
            if (view4 == null) {
                view4 = alertController.A;
            }
            if (view4 != null) {
                if (z4) {
                    i3 = 2;
                }
                int i9 = i8 | i3;
                View findViewById10 = alertController.c.findViewById(f.scrollIndicatorUp);
                View findViewById11 = alertController.c.findViewById(f.scrollIndicatorDown);
                if (Build.VERSION.SDK_INT >= 23) {
                    ViewCompat.a(view4, i9, 3);
                    if (findViewById10 != null) {
                        a3.removeView(findViewById10);
                    }
                    if (findViewById11 != null) {
                        a3.removeView(findViewById11);
                    }
                } else {
                    if (findViewById10 != null && (i9 & 1) == 0) {
                        a3.removeView(findViewById10);
                        findViewById10 = null;
                    }
                    if (findViewById11 == null || (i9 & 2) != 0) {
                        view3 = findViewById11;
                    } else {
                        a3.removeView(findViewById11);
                    }
                    if (!(findViewById10 == null && view3 == null)) {
                        if (alertController.f5f != null) {
                            alertController.A.setOnScrollChangeListener(new AlertController3(alertController, findViewById10, view3));
                            alertController.A.post(new AlertController(alertController, findViewById10, view3));
                        } else {
                            ListView listView2 = alertController.g;
                            if (listView2 != null) {
                                listView2.setOnScrollListener(new AlertController0(alertController, findViewById10, view3));
                                alertController.g.post(new AlertController1(alertController, findViewById10, view3));
                            } else {
                                if (findViewById10 != null) {
                                    a3.removeView(findViewById10);
                                }
                                if (view3 != null) {
                                    a3.removeView(view3);
                                }
                            }
                        }
                    }
                }
            }
        }
        ListView listView3 = alertController.g;
        if (listView3 != null && (listAdapter = alertController.H) != null) {
            listView3.setAdapter(listAdapter);
            int i10 = alertController.I;
            if (i10 > -1) {
                listView3.setItemChecked(i10, true);
                listView3.setSelection(i10);
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        NestedScrollView nestedScrollView = this.d.A;
        if (nestedScrollView != null && nestedScrollView.a(keyEvent)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        NestedScrollView nestedScrollView = this.d.A;
        if (nestedScrollView != null && nestedScrollView.a(keyEvent)) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        AlertController alertController = this.d;
        alertController.f4e = charSequence;
        TextView textView = alertController.E;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }
}
