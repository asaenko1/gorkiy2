package i.b.k;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeConverter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.InsetDrawable;
import android.icu.text.DecimalFormatSymbols;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.Property;
import android.util.TypedValue;
import android.util.Xml;
import android.view.ActionMode;
import android.view.InflateException;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.ImageHeaderParser;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader$ParseException;
import i.b.q.TooltipCompatHandler;
import i.b.q.WithHint;
import i.f.a.h.ConstraintAnchor;
import i.f.a.h.ConstraintWidget;
import i.f.a.h.ConstraintWidgetContainer;
import i.f.a.h.ConstraintWidgetGroup;
import i.f.a.h.Guideline;
import i.f.a.h.Helper;
import i.f.a.h.Optimizer;
import i.f.a.h.ResolutionAnchor;
import i.f.a.h.d;
import i.f.a.h.f;
import i.f.a.h.l;
import i.h.c;
import i.h.e.b.ComplexColorCompat;
import i.h.e.b.FontResourcesParserCompat;
import i.h.e.b.FontResourcesParserCompat0;
import i.h.e.b.FontResourcesParserCompat1;
import i.h.e.b.FontResourcesParserCompat2;
import i.h.f.PathParser;
import i.h.f.j.TintAwareDrawable;
import i.h.f.j.WrappedDrawable;
import i.h.f.j.WrappedDrawableApi21;
import i.h.i.FontRequest;
import i.h.j.PrecomputedTextCompat;
import i.h.m.TextViewCompat;
import i.l.a.FragmentActivity;
import i.o.ViewModelProvider;
import i.r.d.OrientationHelper;
import i.x.a.a.AndroidResources;
import i.x.a.a.AnimatorInflaterCompat;
import i.x.a.a.ArgbEvaluator;
import j.a.a.a.outline;
import j.b.a.m.m.b0.b;
import j.b.a.m.o.c.RecyclableBufferedInputStream;
import j.c.a.a.c.l.n;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ResourcesFlusher {
    public static Field a;
    public static boolean b;
    public static Class<?> c;
    public static boolean d;

    /* renamed from: e  reason: collision with root package name */
    public static Field f766e;

    /* renamed from: f  reason: collision with root package name */
    public static boolean f767f;
    public static Field g;
    public static boolean h;

    /* renamed from: i  reason: collision with root package name */
    public static Method f768i;

    /* renamed from: j  reason: collision with root package name */
    public static boolean f769j;

    /* renamed from: k  reason: collision with root package name */
    public static Method f770k;

    /* renamed from: l  reason: collision with root package name */
    public static boolean f771l;

    /* renamed from: m  reason: collision with root package name */
    public static Field f772m;

    /* renamed from: n  reason: collision with root package name */
    public static boolean f773n;

    /* renamed from: o  reason: collision with root package name */
    public static Method f774o;

    /* renamed from: p  reason: collision with root package name */
    public static boolean f775p;

    /* renamed from: q  reason: collision with root package name */
    public static Field f776q;

    /* renamed from: r  reason: collision with root package name */
    public static boolean f777r;

    public static int a(int i2, int i3, int i4) {
        return i2 < i3 ? i3 : i2 > i4 ? i4 : i2;
    }

    public static void a(String str) {
        if (!(Looper.getMainLooper() == Looper.myLooper())) {
            throw new IllegalStateException(str);
        }
    }

    public static boolean a(int i2, int i3) {
        return i2 != Integer.MIN_VALUE && i3 != Integer.MIN_VALUE && i2 <= 512 && i3 <= 384;
    }

    public static <T> T b(T t2) {
        if (t2 != null) {
            return t2;
        }
        throw new NullPointerException("null reference");
    }

    public static boolean b(int i2) {
        return i2 >= 28 && i2 <= 31;
    }

    public static void c(String str) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IllegalStateException(str);
        }
    }

    public static n d(Object obj) {
        return new n(obj, null);
    }

    public static Path e(String str) {
        Path path = new Path();
        PathParser[] d2 = d(str);
        if (d2 == null) {
            return null;
        }
        try {
            PathParser.a(d2, path);
            return path;
        } catch (RuntimeException e2) {
            throw new RuntimeException(outline.a("Error in parsing ", str), e2);
        }
    }

    public static int f(Parcel parcel, int i2) {
        b(parcel, i2, 4);
        return parcel.readInt();
    }

    public static long g(Parcel parcel, int i2) {
        b(parcel, i2, 8);
        return parcel.readLong();
    }

    public static int h(Parcel parcel, int i2) {
        return (i2 & -65536) != -65536 ? (i2 >> 16) & 65535 : parcel.readInt();
    }

    public static void i(Parcel parcel, int i2) {
        parcel.setDataPosition(parcel.dataPosition() + h(parcel, i2));
    }

    public static int j(Parcel parcel, int i2) {
        parcel.writeInt(i2 | -65536);
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    public static void k(Parcel parcel, int i2) {
        int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(i2 - 4);
        parcel.writeInt(dataPosition - i2);
        parcel.setDataPosition(dataPosition);
    }

    public static int b(Parcel parcel) {
        int readInt = parcel.readInt();
        int h2 = h(parcel, readInt);
        int dataPosition = parcel.dataPosition();
        if ((65535 & readInt) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(readInt));
            throw new SafeParcelReader$ParseException(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i2 = h2 + dataPosition;
        if (i2 >= dataPosition && i2 <= parcel.dataSize()) {
            return i2;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("Size read is invalid start=");
        sb.append(dataPosition);
        sb.append(" end=");
        sb.append(i2);
        throw new SafeParcelReader$ParseException(sb.toString(), parcel);
    }

    public static void d(Parcel parcel, int i2, int i3) {
        if (i3 >= 65535) {
            parcel.writeInt(i2 | -65536);
            parcel.writeInt(i3);
            return;
        }
        parcel.writeInt(i2 | (i3 << 16));
    }

    public static int a(Parcel parcel) {
        return j(parcel, 20293);
    }

    public static boolean c(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static void a(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void c(Parcel parcel, int i2, int i3) {
        if (i2 != i3) {
            String hexString = Integer.toHexString(i2);
            StringBuilder sb = new StringBuilder(outline.a(hexString, 46));
            sb.append("Expected size ");
            sb.append(i3);
            sb.append(" got ");
            sb.append(i2);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new SafeParcelReader$ParseException(sb.toString(), parcel);
        }
    }

    public static boolean a(Uri uri) {
        return uri != null && "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    public static boolean d(Parcel parcel, int i2) {
        b(parcel, i2, 4);
        return parcel.readInt() != 0;
    }

    public static IBinder e(Parcel parcel, int i2) {
        int h2 = h(parcel, i2);
        int dataPosition = parcel.dataPosition();
        if (h2 == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + h2);
        return readStrongBinder;
    }

    public static void c(Object obj) {
        if (!d) {
            try {
                c = Class.forName("android.content.res.ThemedResourceCache");
            } catch (ClassNotFoundException e2) {
                Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", e2);
            }
            d = true;
        }
        Class<?> cls = c;
        if (cls != null) {
            if (!f767f) {
                try {
                    Field declaredField = cls.getDeclaredField("mUnthemedEntries");
                    f766e = declaredField;
                    declaredField.setAccessible(true);
                } catch (NoSuchFieldException e3) {
                    Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", e3);
                }
                f767f = true;
            }
            Field field = f766e;
            if (field != null) {
                LongSparseArray longSparseArray = null;
                try {
                    longSparseArray = (LongSparseArray) field.get(obj);
                } catch (IllegalAccessException e4) {
                    Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", e4);
                }
                if (longSparseArray != null) {
                    longSparseArray.clear();
                }
            }
        }
    }

    public static void a(Parcel parcel, int i2, boolean z) {
        d(parcel, i2, 4);
        parcel.writeInt(z ? 1 : 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008b, code lost:
        if (r13 == false) goto L_0x008d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0098 A[Catch:{ NumberFormatException -> 0x00ba }, LOOP:3: B:25:0x006d->B:45:0x0098, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00d7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0097 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static i.h.f.PathParser[] d(java.lang.String r16) {
        /*
            r0 = r16
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            r3 = 1
            r4 = 0
        L_0x000e:
            int r5 = r16.length()
            if (r3 >= r5) goto L_0x00df
        L_0x0014:
            int r5 = r16.length()
            r6 = 69
            r7 = 101(0x65, float:1.42E-43)
            if (r3 >= r5) goto L_0x003a
            char r5 = r0.charAt(r3)
            int r8 = r5 + -65
            int r9 = r5 + -90
            int r9 = r9 * r8
            if (r9 <= 0) goto L_0x0032
            int r8 = r5 + -97
            int r9 = r5 + -122
            int r9 = r9 * r8
            if (r9 > 0) goto L_0x0037
        L_0x0032:
            if (r5 == r7) goto L_0x0037
            if (r5 == r6) goto L_0x0037
            goto L_0x003a
        L_0x0037:
            int r3 = r3 + 1
            goto L_0x0014
        L_0x003a:
            java.lang.String r4 = r0.substring(r4, r3)
            java.lang.String r4 = r4.trim()
            int r5 = r4.length()
            if (r5 <= 0) goto L_0x00d7
            char r5 = r4.charAt(r2)
            r8 = 122(0x7a, float:1.71E-43)
            if (r5 == r8) goto L_0x00c9
            char r5 = r4.charAt(r2)
            r8 = 90
            if (r5 != r8) goto L_0x005a
            goto L_0x00c9
        L_0x005a:
            int r5 = r4.length()     // Catch:{ NumberFormatException -> 0x00ba }
            float[] r5 = new float[r5]     // Catch:{ NumberFormatException -> 0x00ba }
            int r8 = r4.length()     // Catch:{ NumberFormatException -> 0x00ba }
            r9 = 1
            r10 = 0
        L_0x0066:
            if (r9 >= r8) goto L_0x00b2
            r2 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = r9
        L_0x006d:
            int r15 = r4.length()     // Catch:{ NumberFormatException -> 0x00ba }
            if (r14 >= r15) goto L_0x009b
            char r15 = r4.charAt(r14)     // Catch:{ NumberFormatException -> 0x00ba }
            r7 = 32
            if (r15 == r7) goto L_0x0091
            r7 = 101(0x65, float:1.42E-43)
            if (r15 == r6) goto L_0x008f
            if (r15 == r7) goto L_0x008f
            switch(r15) {
                case 44: goto L_0x0093;
                case 45: goto L_0x0089;
                case 46: goto L_0x0085;
                default: goto L_0x0084;
            }     // Catch:{ NumberFormatException -> 0x00ba }
        L_0x0084:
            goto L_0x0094
        L_0x0085:
            if (r12 != 0) goto L_0x008d
            r12 = 1
            goto L_0x0094
        L_0x0089:
            if (r14 == r9) goto L_0x0094
            if (r13 != 0) goto L_0x0094
        L_0x008d:
            r11 = 1
            goto L_0x0093
        L_0x008f:
            r13 = 1
            goto L_0x0095
        L_0x0091:
            r7 = 101(0x65, float:1.42E-43)
        L_0x0093:
            r2 = 1
        L_0x0094:
            r13 = 0
        L_0x0095:
            if (r2 == 0) goto L_0x0098
            goto L_0x009b
        L_0x0098:
            int r14 = r14 + 1
            goto L_0x006d
        L_0x009b:
            if (r9 >= r14) goto L_0x00aa
            int r2 = r10 + 1
            java.lang.String r9 = r4.substring(r9, r14)     // Catch:{ NumberFormatException -> 0x00ba }
            float r9 = java.lang.Float.parseFloat(r9)     // Catch:{ NumberFormatException -> 0x00ba }
            r5[r10] = r9     // Catch:{ NumberFormatException -> 0x00ba }
            r10 = r2
        L_0x00aa:
            if (r11 == 0) goto L_0x00ad
            goto L_0x00af
        L_0x00ad:
            int r14 = r14 + 1
        L_0x00af:
            r9 = r14
            r2 = 0
            goto L_0x0066
        L_0x00b2:
            float[] r2 = a(r5, r2, r10)     // Catch:{ NumberFormatException -> 0x00ba }
            r5 = 0
            r5 = r2
            r2 = 0
            goto L_0x00cb
        L_0x00ba:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "error in parsing \""
            java.lang.String r3 = "\""
            java.lang.String r2 = j.a.a.a.outline.a(r2, r4, r3)
            r1.<init>(r2, r0)
            throw r1
        L_0x00c9:
            float[] r5 = new float[r2]
        L_0x00cb:
            char r2 = r4.charAt(r2)
            i.h.f.PathParser r4 = new i.h.f.PathParser
            r4.<init>(r2, r5)
            r1.add(r4)
        L_0x00d7:
            int r2 = r3 + 1
            r4 = 0
            r4 = r3
            r3 = r2
            r2 = 0
            goto L_0x000e
        L_0x00df:
            int r3 = r3 - r4
            r2 = 1
            if (r3 != r2) goto L_0x00f8
            int r2 = r16.length()
            if (r4 >= r2) goto L_0x00f8
            char r0 = r0.charAt(r4)
            r2 = 0
            float[] r2 = new float[r2]
            i.h.f.PathParser r3 = new i.h.f.PathParser
            r3.<init>(r0, r2)
            r1.add(r3)
        L_0x00f8:
            int r0 = r1.size()
            i.h.f.PathParser[] r0 = new i.h.f.PathParser[r0]
            java.lang.Object[] r0 = r1.toArray(r0)
            i.h.f.PathParser[] r0 = (i.h.f.PathParser[]) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.d(java.lang.String):i.h.f.PathParser[]");
    }

    public static String b(String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }

    public static InputConnection a(InputConnection inputConnection, EditorInfo editorInfo, View view) {
        if (inputConnection != null && editorInfo.hintText == null) {
            ViewParent parent = view.getParent();
            while (true) {
                if (!(parent instanceof View)) {
                    break;
                } else if (parent instanceof WithHint) {
                    editorInfo.hintText = ((WithHint) parent).a();
                    break;
                } else {
                    parent = parent.getParent();
                }
            }
        }
        return inputConnection;
    }

    public static <T> T b(Object obj, Object obj2) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(String.valueOf(obj2));
    }

    public static void b(Parcel parcel, int i2, int i3) {
        int h2 = h(parcel, i2);
        if (h2 != i3) {
            String hexString = Integer.toHexString(h2);
            StringBuilder sb = new StringBuilder(outline.a(hexString, 46));
            sb.append("Expected size ");
            sb.append(i3);
            sb.append(" got ");
            sb.append(h2);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new SafeParcelReader$ParseException(sb.toString(), parcel);
        }
    }

    public static void b(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    public static <T> T a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    /* JADX INFO: finally extract failed */
    public static ImageHeaderParser.ImageType b(List<ImageHeaderParser> list, InputStream inputStream, b bVar) {
        if (inputStream == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        if (!inputStream.markSupported()) {
            inputStream = new RecyclableBufferedInputStream(inputStream, bVar);
        }
        inputStream.mark(5242880);
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            try {
                ImageHeaderParser.ImageType a2 = list.get(i2).a(inputStream);
                inputStream.reset();
                if (a2 != ImageHeaderParser.ImageType.UNKNOWN) {
                    return a2;
                }
                i2++;
            } catch (Throwable th) {
                inputStream.reset();
                throw th;
            }
        }
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    public static void a(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static <T> ObjectAnimator a(Object obj, Property property, Path path) {
        return ObjectAnimator.ofObject(obj, property, (TypeConverter) null, path);
    }

    public static int a(RecyclerView.a0 a0Var, OrientationHelper orientationHelper, View view, View view2, RecyclerView.o oVar, boolean z, boolean z2) {
        int i2;
        if (oVar.d() == 0 || a0Var.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(oVar.i(view), oVar.i(view2));
        int max = Math.max(oVar.i(view), oVar.i(view2));
        if (z2) {
            i2 = Math.max(0, (a0Var.a() - max) - 1);
        } else {
            i2 = Math.max(0, min);
        }
        if (!z) {
            return i2;
        }
        return Math.round((((float) i2) * (((float) Math.abs(orientationHelper.a(view2) - orientationHelper.d(view))) / ((float) (Math.abs(oVar.i(view) - oVar.i(view2)) + 1)))) + ((float) (orientationHelper.f() - orientationHelper.d(view))));
    }

    public static <T extends Drawable> T c(Drawable drawable) {
        return drawable instanceof WrappedDrawable ? ((WrappedDrawable) drawable).a() : drawable;
    }

    public static int c(int i2, Rect rect, Rect rect2) {
        if (i2 != 17) {
            if (i2 != 33) {
                if (i2 != 66) {
                    if (i2 != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                }
            }
            return Math.abs(((rect.width() / 2) + rect.left) - ((rect2.width() / 2) + rect2.left));
        }
        return Math.abs(((rect.height() / 2) + rect.top) - ((rect2.height() / 2) + rect2.top));
    }

    public static int b(RecyclerView.a0 a0Var, OrientationHelper orientationHelper, View view, View view2, RecyclerView.o oVar, boolean z) {
        if (oVar.d() == 0 || a0Var.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return a0Var.a();
        }
        return (int) ((((float) (orientationHelper.a(view2) - orientationHelper.d(view))) / ((float) (Math.abs(oVar.i(view) - oVar.i(view2)) + 1))) * ((float) a0Var.a()));
    }

    public static void d(TextView textView, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            textView.setTextAppearance(i2);
        } else {
            textView.setTextAppearance(textView.getContext(), i2);
        }
    }

    public static void c(Parcel parcel, int i2) {
        if (parcel.dataPosition() != i2) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(i2);
            throw new SafeParcelReader$ParseException(sb.toString(), parcel);
        }
    }

    public static void c(TextView textView, int i2) {
        a(i2);
        int fontMetricsInt = textView.getPaint().getFontMetricsInt(null);
        if (i2 != fontMetricsInt) {
            textView.setLineSpacing((float) (i2 - fontMetricsInt), 1.0f);
        }
    }

    public static Drawable d(Drawable drawable) {
        return (Build.VERSION.SDK_INT < 23 && !(drawable instanceof TintAwareDrawable)) ? new WrappedDrawableApi21(drawable) : drawable;
    }

    public static String b(Parcel parcel, int i2) {
        int h2 = h(parcel, i2);
        int dataPosition = parcel.dataPosition();
        if (h2 == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(dataPosition + h2);
        return readString;
    }

    public static void a(Object obj, StringBuilder sb) {
        int lastIndexOf;
        if (obj == null) {
            sb.append("null");
            return;
        }
        String simpleName = obj.getClass().getSimpleName();
        if (simpleName.length() <= 0 && (lastIndexOf = (simpleName = obj.getClass().getName()).lastIndexOf(46)) > 0) {
            simpleName = simpleName.substring(lastIndexOf + 1);
        }
        sb.append(simpleName);
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(obj)));
    }

    public static int b(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i2, int i3) {
        if (!a(xmlPullParser, str)) {
            return i3;
        }
        return typedArray.getInt(i2, i3);
    }

    public static void b(Drawable drawable, int i2) {
        drawable.setTint(i2);
    }

    public static String b(Context context, ComponentName componentName) {
        String string;
        PackageManager packageManager = context.getPackageManager();
        int i2 = Build.VERSION.SDK_INT;
        int i3 = 640;
        int i4 = Build.VERSION.SDK_INT;
        if (i4 >= 29) {
            i3 = 269222528;
        } else if (i4 >= 24) {
            i3 = 787072;
        }
        ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, i3);
        String str = activityInfo.parentActivityName;
        if (str != null) {
            return str;
        }
        Bundle bundle = activityInfo.metaData;
        if (bundle == null || (string = bundle.getString("android.support.PARENT_ACTIVITY")) == null) {
            return null;
        }
        if (string.charAt(0) != '.') {
            return string;
        }
        return context.getPackageName() + string;
    }

    public static void a(Parcel parcel, int i2, int i3) {
        d(parcel, i2, 4);
        parcel.writeInt(i3);
    }

    public static void a(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    public static void a(View view, CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 26) {
            view.setTooltipText(charSequence);
            return;
        }
        TooltipCompatHandler tooltipCompatHandler = TooltipCompatHandler.f1041k;
        if (tooltipCompatHandler != null && tooltipCompatHandler.b == view) {
            TooltipCompatHandler.a((TooltipCompatHandler) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            TooltipCompatHandler tooltipCompatHandler2 = TooltipCompatHandler.f1042l;
            if (tooltipCompatHandler2 != null && tooltipCompatHandler2.b == view) {
                tooltipCompatHandler2.b();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new TooltipCompatHandler(view, charSequence);
    }

    public static CharSequence[] b(TypedArray typedArray, int i2, int i3) {
        CharSequence[] textArray = typedArray.getTextArray(i2);
        return textArray == null ? typedArray.getTextArray(i3) : textArray;
    }

    public static int b(int i2, Rect rect, Rect rect2) {
        int i3;
        int i4;
        if (i2 == 17) {
            i4 = rect.left;
            i3 = rect2.right;
        } else if (i2 == 33) {
            i4 = rect.top;
            i3 = rect2.bottom;
        } else if (i2 == 66) {
            i4 = rect2.left;
            i3 = rect.right;
        } else if (i2 == 130) {
            i4 = rect2.top;
            i3 = rect.bottom;
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return Math.max(0, i4 - i3);
    }

    public static int b(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 23) {
            return drawable.getLayoutDirection();
        }
        if (!f771l) {
            try {
                Method declaredMethod = Drawable.class.getDeclaredMethod("getLayoutDirection", new Class[0]);
                f770k = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("DrawableCompat", "Failed to retrieve getLayoutDirection() method", e2);
            }
            f771l = true;
        }
        Method method = f770k;
        if (method != null) {
            try {
                return ((Integer) method.invoke(drawable, new Object[0])).intValue();
            } catch (Exception e3) {
                Log.i("DrawableCompat", "Failed to invoke getLayoutDirection() via reflection", e3);
                f770k = null;
            }
        }
        return 0;
    }

    public static void a(Parcel parcel, int i2, long j2) {
        d(parcel, i2, 8);
        parcel.writeLong(j2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0166, code lost:
        if (r12.f0 == 2) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x016a, code lost:
        r12 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e8, code lost:
        if (r4[r10].d.b == r15) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x014a, code lost:
        if (r12.e0 == 2) goto L_0x0168;
     */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0227  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00be  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(i.f.a.h.ConstraintWidgetContainer r27, i.f.a.LinearSystem r28, int r29) {
        /*
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = 2
            if (r2 != 0) goto L_0x000f
            int r5 = r0.s0
            i.f.a.h.ChainHead[] r6 = r0.v0
            r7 = 0
            goto L_0x0014
        L_0x000f:
            int r5 = r0.t0
            i.f.a.h.ChainHead[] r6 = r0.u0
            r7 = 2
        L_0x0014:
            r8 = 0
        L_0x0015:
            if (r8 >= r5) goto L_0x046c
            r9 = r6[r8]
            boolean r10 = r9.f1105q
            r11 = 0
            r12 = 8
            r14 = 1
            if (r10 != 0) goto L_0x0118
            int r10 = r9.f1100l
            int r10 = r10 * 2
            i.f.a.h.ConstraintWidget r15 = r9.a
            r4 = r15
            r16 = 0
        L_0x002a:
            if (r16 != 0) goto L_0x00fa
            int r13 = r9.f1097i
            int r13 = r13 + r14
            r9.f1097i = r13
            i.f.a.h.ConstraintWidget[] r13 = r15.i0
            int r14 = r9.f1100l
            r13[r14] = r11
            i.f.a.h.ConstraintWidget[] r13 = r15.h0
            r13[r14] = r11
            int r13 = r15.Y
            if (r13 == r12) goto L_0x00c6
            i.f.a.h.ConstraintWidget r13 = r9.b
            if (r13 != 0) goto L_0x0045
            r9.b = r15
        L_0x0045:
            r9.d = r15
            i.f.a.h.ConstraintWidget$a[] r13 = r15.C
            int r14 = r9.f1100l
            r13 = r13[r14]
            i.f.a.h.ConstraintWidget$a r11 = i.f.a.h.ConstraintWidget.a.MATCH_CONSTRAINT
            if (r13 != r11) goto L_0x00c6
            int[] r11 = r15.g
            r13 = r11[r14]
            r12 = 3
            if (r13 == 0) goto L_0x0060
            r13 = r11[r14]
            if (r13 == r12) goto L_0x0060
            r11 = r11[r14]
            if (r11 != r3) goto L_0x00c6
        L_0x0060:
            int r11 = r9.f1098j
            r13 = 1
            int r11 = r11 + r13
            r9.f1098j = r11
            float[] r11 = r15.g0
            int r13 = r9.f1100l
            r14 = r11[r13]
            r17 = 0
            int r19 = (r14 > r17 ? 1 : (r14 == r17 ? 0 : -1))
            if (r19 <= 0) goto L_0x0079
            float r3 = r9.f1099k
            r11 = r11[r13]
            float r3 = r3 + r11
            r9.f1099k = r3
        L_0x0079:
            int r3 = r9.f1100l
            int r11 = r15.Y
            r13 = 8
            if (r11 == r13) goto L_0x0095
            i.f.a.h.ConstraintWidget$a[] r11 = r15.C
            r11 = r11[r3]
            i.f.a.h.ConstraintWidget$a r13 = i.f.a.h.ConstraintWidget.a.MATCH_CONSTRAINT
            if (r11 != r13) goto L_0x0095
            int[] r11 = r15.g
            r13 = r11[r3]
            if (r13 == 0) goto L_0x0093
            r3 = r11[r3]
            if (r3 != r12) goto L_0x0095
        L_0x0093:
            r3 = 1
            goto L_0x0096
        L_0x0095:
            r3 = 0
        L_0x0096:
            if (r3 == 0) goto L_0x00b4
            r3 = 0
            int r11 = (r14 > r3 ? 1 : (r14 == r3 ? 0 : -1))
            if (r11 >= 0) goto L_0x00a1
            r3 = 1
            r9.f1102n = r3
            goto L_0x00a4
        L_0x00a1:
            r3 = 1
            r9.f1103o = r3
        L_0x00a4:
            java.util.ArrayList<i.f.a.h.d> r3 = r9.h
            if (r3 != 0) goto L_0x00af
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r9.h = r3
        L_0x00af:
            java.util.ArrayList<i.f.a.h.d> r3 = r9.h
            r3.add(r15)
        L_0x00b4:
            i.f.a.h.ConstraintWidget r3 = r9.f1096f
            if (r3 != 0) goto L_0x00ba
            r9.f1096f = r15
        L_0x00ba:
            i.f.a.h.ConstraintWidget r3 = r9.g
            if (r3 == 0) goto L_0x00c4
            i.f.a.h.ConstraintWidget[] r3 = r3.h0
            int r11 = r9.f1100l
            r3[r11] = r15
        L_0x00c4:
            r9.g = r15
        L_0x00c6:
            if (r4 == r15) goto L_0x00ce
            i.f.a.h.ConstraintWidget[] r3 = r4.i0
            int r4 = r9.f1100l
            r3[r4] = r15
        L_0x00ce:
            i.f.a.h.ConstraintAnchor[] r3 = r15.A
            int r4 = r10 + 1
            r3 = r3[r4]
            i.f.a.h.ConstraintAnchor r3 = r3.d
            if (r3 == 0) goto L_0x00ea
            i.f.a.h.ConstraintWidget r3 = r3.b
            i.f.a.h.ConstraintAnchor[] r4 = r3.A
            r11 = r4[r10]
            i.f.a.h.ConstraintAnchor r11 = r11.d
            if (r11 == 0) goto L_0x00ea
            r4 = r4[r10]
            i.f.a.h.ConstraintAnchor r4 = r4.d
            i.f.a.h.ConstraintWidget r4 = r4.b
            if (r4 == r15) goto L_0x00eb
        L_0x00ea:
            r3 = 0
        L_0x00eb:
            if (r3 == 0) goto L_0x00ee
            goto L_0x00f1
        L_0x00ee:
            r3 = r15
            r16 = 1
        L_0x00f1:
            r4 = r15
            r11 = 0
            r12 = 8
            r14 = 1
            r15 = r3
            r3 = 2
            goto L_0x002a
        L_0x00fa:
            r9.c = r15
            int r3 = r9.f1100l
            if (r3 != 0) goto L_0x0107
            boolean r3 = r9.f1101m
            if (r3 == 0) goto L_0x0107
            r9.f1095e = r15
            goto L_0x010b
        L_0x0107:
            i.f.a.h.ConstraintWidget r3 = r9.a
            r9.f1095e = r3
        L_0x010b:
            boolean r3 = r9.f1103o
            if (r3 == 0) goto L_0x0115
            boolean r3 = r9.f1102n
            if (r3 == 0) goto L_0x0115
            r3 = 1
            goto L_0x0116
        L_0x0115:
            r3 = 0
        L_0x0116:
            r9.f1104p = r3
        L_0x0118:
            r3 = 1
            r9.f1105q = r3
            r3 = 4
            boolean r3 = r0.g(r3)
            if (r3 == 0) goto L_0x0457
            i.f.a.h.ConstraintWidget r3 = r9.a
            i.f.a.h.ConstraintWidget r4 = r9.c
            i.f.a.h.ConstraintWidget r10 = r9.b
            i.f.a.h.ConstraintWidget r11 = r9.d
            i.f.a.h.ConstraintWidget r12 = r9.f1095e
            float r13 = r9.f1099k
            i.f.a.h.ConstraintWidget$a[] r14 = r0.C
            r14 = r14[r2]
            i.f.a.h.ConstraintWidget$a r14 = i.f.a.h.ConstraintWidget.a.WRAP_CONTENT
            if (r2 != 0) goto L_0x014d
            int r14 = r12.e0
            if (r14 != 0) goto L_0x013c
            r14 = 1
            goto L_0x013d
        L_0x013c:
            r14 = 0
        L_0x013d:
            int r15 = r12.e0
            r16 = r5
            r5 = 1
            if (r15 != r5) goto L_0x0146
            r5 = 1
            goto L_0x0147
        L_0x0146:
            r5 = 0
        L_0x0147:
            int r12 = r12.e0
            r15 = 2
            if (r12 != r15) goto L_0x016a
            goto L_0x0168
        L_0x014d:
            r16 = r5
            int r5 = r12.f0
            if (r5 != 0) goto L_0x0155
            r5 = 1
            goto L_0x0156
        L_0x0155:
            r5 = 0
        L_0x0156:
            int r14 = r12.f0
            r15 = 1
            if (r14 != r15) goto L_0x015d
            r14 = 1
            goto L_0x015e
        L_0x015d:
            r14 = 0
        L_0x015e:
            int r12 = r12.f0
            r15 = 2
            r26 = r14
            r14 = r5
            r5 = r26
            if (r12 != r15) goto L_0x016a
        L_0x0168:
            r12 = 1
            goto L_0x016b
        L_0x016a:
            r12 = 0
        L_0x016b:
            r15 = r3
            r22 = r6
            r23 = r8
            r6 = 0
            r8 = 0
            r19 = 0
            r20 = 0
            r21 = 0
        L_0x0178:
            if (r21 != 0) goto L_0x0236
            int r0 = r15.Y
            r24 = r9
            r9 = 8
            if (r0 == r9) goto L_0x01c6
            int r6 = r6 + 1
            if (r2 != 0) goto L_0x018b
            int r0 = r15.i()
            goto L_0x018f
        L_0x018b:
            int r0 = r15.d()
        L_0x018f:
            float r0 = (float) r0
            float r19 = r19 + r0
            if (r15 == r10) goto L_0x019f
            i.f.a.h.ConstraintAnchor[] r0 = r15.A
            r0 = r0[r7]
            int r0 = r0.a()
            float r0 = (float) r0
            float r19 = r19 + r0
        L_0x019f:
            if (r15 == r11) goto L_0x01ae
            i.f.a.h.ConstraintAnchor[] r0 = r15.A
            int r9 = r7 + 1
            r0 = r0[r9]
            int r0 = r0.a()
            float r0 = (float) r0
            float r19 = r19 + r0
        L_0x01ae:
            i.f.a.h.ConstraintAnchor[] r0 = r15.A
            r0 = r0[r7]
            int r0 = r0.a()
            float r0 = (float) r0
            float r20 = r20 + r0
            i.f.a.h.ConstraintAnchor[] r0 = r15.A
            int r9 = r7 + 1
            r0 = r0[r9]
            int r0 = r0.a()
            float r0 = (float) r0
            float r20 = r20 + r0
        L_0x01c6:
            i.f.a.h.ConstraintAnchor[] r0 = r15.A
            r0 = r0[r7]
            int r0 = r15.Y
            r9 = 8
            if (r0 == r9) goto L_0x0202
            i.f.a.h.ConstraintWidget$a[] r0 = r15.C
            r0 = r0[r2]
            i.f.a.h.ConstraintWidget$a r9 = i.f.a.h.ConstraintWidget.a.MATCH_CONSTRAINT
            if (r0 != r9) goto L_0x0202
            int r8 = r8 + 1
            if (r2 != 0) goto L_0x01ea
            int r0 = r15.f1109e
            if (r0 == 0) goto L_0x01e1
        L_0x01e0:
            goto L_0x01ee
        L_0x01e1:
            int r0 = r15.h
            if (r0 != 0) goto L_0x01ee
            int r0 = r15.f1111i
            if (r0 == 0) goto L_0x01fa
            goto L_0x01ee
        L_0x01ea:
            int r0 = r15.f1110f
            if (r0 == 0) goto L_0x01f1
        L_0x01ee:
            r13 = r1
            goto L_0x044c
        L_0x01f1:
            int r0 = r15.f1113k
            if (r0 != 0) goto L_0x01ee
            int r0 = r15.f1114l
            if (r0 == 0) goto L_0x01fa
            goto L_0x0201
        L_0x01fa:
            float r0 = r15.G
            r9 = 0
            int r0 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
            if (r0 == 0) goto L_0x0202
        L_0x0201:
            goto L_0x01e0
        L_0x0202:
            i.f.a.h.ConstraintAnchor[] r0 = r15.A
            int r9 = r7 + 1
            r0 = r0[r9]
            i.f.a.h.ConstraintAnchor r0 = r0.d
            if (r0 == 0) goto L_0x0224
            i.f.a.h.ConstraintWidget r0 = r0.b
            i.f.a.h.ConstraintAnchor[] r9 = r0.A
            r25 = r0
            r0 = r9[r7]
            i.f.a.h.ConstraintAnchor r0 = r0.d
            if (r0 == 0) goto L_0x0224
            r0 = r9[r7]
            i.f.a.h.ConstraintAnchor r0 = r0.d
            i.f.a.h.ConstraintWidget r0 = r0.b
            if (r0 == r15) goto L_0x0221
            goto L_0x0224
        L_0x0221:
            r0 = r25
            goto L_0x0225
        L_0x0224:
            r0 = 0
        L_0x0225:
            if (r0 == 0) goto L_0x022e
            r15 = r0
            r9 = r24
            r0 = r27
            goto L_0x0178
        L_0x022e:
            r21 = 1
            r0 = r27
            r9 = r24
            goto L_0x0178
        L_0x0236:
            r24 = r9
            i.f.a.h.ConstraintAnchor[] r0 = r3.A
            r0 = r0[r7]
            i.f.a.h.ResolutionAnchor r0 = r0.a
            i.f.a.h.ConstraintAnchor[] r9 = r4.A
            int r18 = r7 + 1
            r9 = r9[r18]
            i.f.a.h.ResolutionAnchor r9 = r9.a
            r21 = r3
            i.f.a.h.ResolutionAnchor r3 = r0.d
            if (r3 == 0) goto L_0x01ee
            i.f.a.h.ResolutionAnchor r1 = r9.d
            if (r1 != 0) goto L_0x0251
            goto L_0x0261
        L_0x0251:
            int r3 = r3.b
            r25 = r13
            r13 = 1
            if (r3 != r13) goto L_0x0261
            int r1 = r1.b
            if (r1 == r13) goto L_0x025d
            goto L_0x0261
        L_0x025d:
            if (r8 <= 0) goto L_0x0265
            if (r8 == r6) goto L_0x0265
        L_0x0261:
            r13 = r28
            goto L_0x044c
        L_0x0265:
            if (r12 != 0) goto L_0x026e
            if (r14 != 0) goto L_0x026e
            if (r5 == 0) goto L_0x026c
            goto L_0x026e
        L_0x026c:
            r1 = 0
            goto L_0x0287
        L_0x026e:
            if (r10 == 0) goto L_0x027a
            i.f.a.h.ConstraintAnchor[] r1 = r10.A
            r1 = r1[r7]
            int r1 = r1.a()
            float r1 = (float) r1
            goto L_0x027b
        L_0x027a:
            r1 = 0
        L_0x027b:
            if (r11 == 0) goto L_0x0287
            i.f.a.h.ConstraintAnchor[] r3 = r11.A
            r3 = r3[r18]
            int r3 = r3.a()
            float r3 = (float) r3
            float r1 = r1 + r3
        L_0x0287:
            i.f.a.h.ResolutionAnchor r3 = r0.d
            float r3 = r3.g
            i.f.a.h.ResolutionAnchor r9 = r9.d
            float r9 = r9.g
            int r11 = (r3 > r9 ? 1 : (r3 == r9 ? 0 : -1))
            if (r11 >= 0) goto L_0x0295
            float r9 = r9 - r3
            goto L_0x0297
        L_0x0295:
            float r9 = r3 - r9
        L_0x0297:
            float r9 = r9 - r19
            r11 = -1082130432(0xffffffffbf800000, float:-1.0)
            if (r8 <= 0) goto L_0x0326
            if (r8 != r6) goto L_0x0326
            i.f.a.h.ConstraintWidget r1 = r15.D
            if (r1 == 0) goto L_0x02ac
            i.f.a.h.ConstraintWidget$a[] r1 = r1.C
            r1 = r1[r2]
            i.f.a.h.ConstraintWidget$a r5 = i.f.a.h.ConstraintWidget.a.WRAP_CONTENT
            if (r1 != r5) goto L_0x02ac
            goto L_0x0261
        L_0x02ac:
            float r9 = r9 + r19
            float r9 = r9 - r20
            r1 = r3
            r3 = r21
        L_0x02b3:
            if (r3 == 0) goto L_0x0321
            i.f.a.h.ConstraintWidget[] r5 = r3.i0
            r5 = r5[r2]
            if (r5 != 0) goto L_0x02c1
            if (r3 != r4) goto L_0x02be
            goto L_0x02c1
        L_0x02be:
            r13 = r28
            goto L_0x031f
        L_0x02c1:
            float r6 = (float) r8
            float r6 = r9 / r6
            r10 = 0
            int r12 = (r25 > r10 ? 1 : (r25 == r10 ? 0 : -1))
            if (r12 <= 0) goto L_0x02d9
            float[] r6 = r3.g0
            r10 = r6[r2]
            int r10 = (r10 > r11 ? 1 : (r10 == r11 ? 0 : -1))
            if (r10 != 0) goto L_0x02d3
            r6 = 0
            goto L_0x02d9
        L_0x02d3:
            r6 = r6[r2]
            float r6 = r6 * r9
            float r6 = r6 / r25
        L_0x02d9:
            int r10 = r3.Y
            r12 = 8
            if (r10 != r12) goto L_0x02e0
            r6 = 0
        L_0x02e0:
            i.f.a.h.ConstraintAnchor[] r10 = r3.A
            r10 = r10[r7]
            int r10 = r10.a()
            float r10 = (float) r10
            float r1 = r1 + r10
            i.f.a.h.ConstraintAnchor[] r10 = r3.A
            r10 = r10[r7]
            i.f.a.h.ResolutionAnchor r10 = r10.a
            i.f.a.h.ResolutionAnchor r12 = r0.f1129f
            r10.a(r12, r1)
            i.f.a.h.ConstraintAnchor[] r10 = r3.A
            r10 = r10[r18]
            i.f.a.h.ResolutionAnchor r10 = r10.a
            i.f.a.h.ResolutionAnchor r12 = r0.f1129f
            float r1 = r1 + r6
            r10.a(r12, r1)
            i.f.a.h.ConstraintAnchor[] r6 = r3.A
            r6 = r6[r7]
            i.f.a.h.ResolutionAnchor r6 = r6.a
            r13 = r28
            r6.a(r13)
            i.f.a.h.ConstraintAnchor[] r6 = r3.A
            r6 = r6[r18]
            i.f.a.h.ResolutionAnchor r6 = r6.a
            r6.a(r13)
            i.f.a.h.ConstraintAnchor[] r3 = r3.A
            r3 = r3[r18]
            int r3 = r3.a()
            float r3 = (float) r3
            float r1 = r1 + r3
        L_0x031f:
            r3 = r5
            goto L_0x02b3
        L_0x0321:
            r13 = r28
        L_0x0323:
            r14 = 1
            goto L_0x044d
        L_0x0326:
            r13 = r28
            r8 = 0
            int r8 = (r9 > r8 ? 1 : (r9 == r8 ? 0 : -1))
            if (r8 >= 0) goto L_0x0330
            r5 = 0
            r12 = 1
            r14 = 0
        L_0x0330:
            if (r12 == 0) goto L_0x039a
            float r9 = r9 - r1
            if (r2 != 0) goto L_0x033a
            r8 = r21
            float r11 = r8.V
            goto L_0x0341
        L_0x033a:
            r8 = r21
            r1 = 1
            if (r2 != r1) goto L_0x0341
            float r11 = r8.W
        L_0x0341:
            float r9 = r9 * r11
            float r9 = r9 + r3
            r3 = r8
        L_0x0345:
            if (r3 == 0) goto L_0x03a1
            i.f.a.h.ConstraintWidget[] r1 = r3.i0
            r1 = r1[r2]
            if (r1 != 0) goto L_0x034f
            if (r3 != r4) goto L_0x0398
        L_0x034f:
            if (r2 != 0) goto L_0x0356
            int r5 = r3.i()
            goto L_0x035a
        L_0x0356:
            int r5 = r3.d()
        L_0x035a:
            float r5 = (float) r5
            i.f.a.h.ConstraintAnchor[] r6 = r3.A
            r6 = r6[r7]
            int r6 = r6.a()
            float r6 = (float) r6
            float r9 = r9 + r6
            i.f.a.h.ConstraintAnchor[] r6 = r3.A
            r6 = r6[r7]
            i.f.a.h.ResolutionAnchor r6 = r6.a
            i.f.a.h.ResolutionAnchor r8 = r0.f1129f
            r6.a(r8, r9)
            i.f.a.h.ConstraintAnchor[] r6 = r3.A
            r6 = r6[r18]
            i.f.a.h.ResolutionAnchor r6 = r6.a
            i.f.a.h.ResolutionAnchor r8 = r0.f1129f
            float r9 = r9 + r5
            r6.a(r8, r9)
            i.f.a.h.ConstraintAnchor[] r5 = r3.A
            r5 = r5[r7]
            i.f.a.h.ResolutionAnchor r5 = r5.a
            r5.a(r13)
            i.f.a.h.ConstraintAnchor[] r5 = r3.A
            r5 = r5[r18]
            i.f.a.h.ResolutionAnchor r5 = r5.a
            r5.a(r13)
            i.f.a.h.ConstraintAnchor[] r3 = r3.A
            r3 = r3[r18]
            int r3 = r3.a()
            float r3 = (float) r3
            float r9 = r9 + r3
        L_0x0398:
            r3 = r1
            goto L_0x0345
        L_0x039a:
            r8 = r21
            if (r14 != 0) goto L_0x03a3
            if (r5 == 0) goto L_0x03a1
            goto L_0x03a3
        L_0x03a1:
            r5 = 1
            goto L_0x0323
        L_0x03a3:
            if (r14 == 0) goto L_0x03a6
            goto L_0x03a8
        L_0x03a6:
            if (r5 == 0) goto L_0x03a9
        L_0x03a8:
            float r9 = r9 - r1
        L_0x03a9:
            int r1 = r6 + 1
            float r1 = (float) r1
            float r1 = r9 / r1
            if (r5 == 0) goto L_0x03bb
            r11 = 1
            if (r6 <= r11) goto L_0x03b7
            int r1 = r6 + -1
            float r1 = (float) r1
            goto L_0x03b9
        L_0x03b7:
            r1 = 1073741824(0x40000000, float:2.0)
        L_0x03b9:
            float r1 = r9 / r1
        L_0x03bb:
            int r9 = r8.Y
            r11 = 8
            if (r9 == r11) goto L_0x03c4
            float r9 = r3 + r1
            goto L_0x03c5
        L_0x03c4:
            r9 = r3
        L_0x03c5:
            if (r5 == 0) goto L_0x03d6
            r5 = 1
            if (r6 <= r5) goto L_0x03d7
            i.f.a.h.ConstraintAnchor[] r6 = r10.A
            r6 = r6[r7]
            int r6 = r6.a()
            float r6 = (float) r6
            float r9 = r6 + r3
            goto L_0x03d7
        L_0x03d6:
            r5 = 1
        L_0x03d7:
            if (r14 == 0) goto L_0x03e5
            if (r10 == 0) goto L_0x03e5
            i.f.a.h.ConstraintAnchor[] r3 = r10.A
            r3 = r3[r7]
            int r3 = r3.a()
            float r3 = (float) r3
            float r9 = r9 + r3
        L_0x03e5:
            r3 = r8
        L_0x03e6:
            if (r3 == 0) goto L_0x0323
            i.f.a.h.ConstraintWidget[] r6 = r3.i0
            r6 = r6[r2]
            if (r6 != 0) goto L_0x03f4
            if (r3 != r4) goto L_0x03f1
            goto L_0x03f4
        L_0x03f1:
            r8 = 8
            goto L_0x044a
        L_0x03f4:
            if (r2 != 0) goto L_0x03fb
            int r8 = r3.i()
            goto L_0x03ff
        L_0x03fb:
            int r8 = r3.d()
        L_0x03ff:
            float r8 = (float) r8
            if (r3 == r10) goto L_0x040c
            i.f.a.h.ConstraintAnchor[] r11 = r3.A
            r11 = r11[r7]
            int r11 = r11.a()
            float r11 = (float) r11
            float r9 = r9 + r11
        L_0x040c:
            i.f.a.h.ConstraintAnchor[] r11 = r3.A
            r11 = r11[r7]
            i.f.a.h.ResolutionAnchor r11 = r11.a
            i.f.a.h.ResolutionAnchor r12 = r0.f1129f
            r11.a(r12, r9)
            i.f.a.h.ConstraintAnchor[] r11 = r3.A
            r11 = r11[r18]
            i.f.a.h.ResolutionAnchor r11 = r11.a
            i.f.a.h.ResolutionAnchor r12 = r0.f1129f
            float r14 = r9 + r8
            r11.a(r12, r14)
            i.f.a.h.ConstraintAnchor[] r11 = r3.A
            r11 = r11[r7]
            i.f.a.h.ResolutionAnchor r11 = r11.a
            r11.a(r13)
            i.f.a.h.ConstraintAnchor[] r11 = r3.A
            r11 = r11[r18]
            i.f.a.h.ResolutionAnchor r11 = r11.a
            r11.a(r13)
            i.f.a.h.ConstraintAnchor[] r3 = r3.A
            r3 = r3[r18]
            int r3 = r3.a()
            float r3 = (float) r3
            float r8 = r8 + r3
            float r9 = r9 + r8
            if (r6 == 0) goto L_0x03f1
            int r3 = r6.Y
            r8 = 8
            if (r3 == r8) goto L_0x044a
            float r9 = r9 + r1
        L_0x044a:
            r3 = r6
            goto L_0x03e6
        L_0x044c:
            r14 = 0
        L_0x044d:
            r0 = r27
            if (r14 != 0) goto L_0x0462
            r1 = r24
            a(r0, r13, r2, r7, r1)
            goto L_0x0462
        L_0x0457:
            r13 = r1
            r16 = r5
            r22 = r6
            r23 = r8
            r1 = r9
            a(r0, r13, r2, r7, r1)
        L_0x0462:
            int r8 = r23 + 1
            r1 = r13
            r5 = r16
            r6 = r22
            r3 = 2
            goto L_0x0015
        L_0x046c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int):void");
    }

    public static <T> T[] b(Parcel parcel, int i2, Parcelable.Creator<T> creator) {
        int h2 = h(parcel, i2);
        int dataPosition = parcel.dataPosition();
        if (h2 == 0) {
            return null;
        }
        T[] createTypedArray = parcel.createTypedArray(creator);
        parcel.setDataPosition(dataPosition + h2);
        return createTypedArray;
    }

    public static void b(TextView textView, int i2) {
        int i3;
        a(i2);
        Paint.FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        if (textView.getIncludeFontPadding()) {
            i3 = fontMetricsInt.bottom;
        } else {
            i3 = fontMetricsInt.descent;
        }
        if (i2 > Math.abs(i3)) {
            textView.setPadding(textView.getPaddingLeft(), textView.getPaddingTop(), textView.getPaddingRight(), i2 - i3);
        }
    }

    public static ViewModelProvider a(Fragment fragment, ViewModelProvider.b bVar) {
        FragmentActivity l2 = fragment.l();
        if (l2 != null) {
            Application application = l2.getApplication();
            if (application != null) {
                if (bVar == null) {
                    if (ViewModelProvider.a.b == null) {
                        ViewModelProvider.a.b = new ViewModelProvider.a(application);
                    }
                    bVar = ViewModelProvider.a.b;
                }
                return new ViewModelProvider(fragment.f(), bVar);
            }
            throw new IllegalStateException("Your activity/fragment is not yet attached to Application. You can't request ViewModel before onCreate call.");
        }
        throw new IllegalStateException("Can't create ViewModelProvider for detached fragment");
    }

    public static void a(Handler handler) {
        if (Looper.myLooper() != handler.getLooper()) {
            throw new IllegalStateException("Must be called on the handler thread");
        }
    }

    public static float[] a(float[] fArr, int i2, int i3) {
        if (i2 <= i3) {
            int length = fArr.length;
            if (i2 < 0 || i2 > length) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i4 = i3 - i2;
            int min = Math.min(i4, length - i2);
            float[] fArr2 = new float[i4];
            System.arraycopy(fArr, i2, fArr2, 0, min);
            return fArr2;
        }
        throw new IllegalArgumentException();
    }

    public static boolean a(XmlPullParser xmlPullParser, String str) {
        return xmlPullParser.getAttributeValue("http://schemas.android.com/apk/res/android", str) != null;
    }

    public static int a(RecyclerView.a0 a0Var, OrientationHelper orientationHelper, View view, View view2, RecyclerView.o oVar, boolean z) {
        if (oVar.d() == 0 || a0Var.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(oVar.i(view) - oVar.i(view2)) + 1;
        }
        return Math.min(orientationHelper.g(), orientationHelper.a(view2) - orientationHelper.d(view));
    }

    public static File a(Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir == null) {
            return null;
        }
        StringBuilder a2 = outline.a(".font");
        a2.append(Process.myPid());
        a2.append("-");
        a2.append(Process.myTid());
        a2.append("-");
        String sb = a2.toString();
        int i2 = 0;
        while (i2 < 100) {
            File file = new File(cacheDir, outline.b(sb, i2));
            try {
                if (file.createNewFile()) {
                    return file;
                }
                i2++;
            } catch (IOException unused) {
            }
        }
        return null;
    }

    public static <T> T a(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw null;
    }

    public static int a(ConstraintWidgetGroup constraintWidgetGroup, int i2) {
        int i3 = i2 * 2;
        List<d> list = null;
        if (constraintWidgetGroup != null) {
            if (i2 == 0) {
                list = constraintWidgetGroup.f1124f;
            } else if (i2 == 1) {
                list = constraintWidgetGroup.g;
            }
            int size = list.size();
            int i4 = 0;
            for (int i5 = 0; i5 < size; i5++) {
                ConstraintWidget constraintWidget = list.get(i5);
                ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
                int i6 = i3 + 1;
                i4 = Math.max(i4, a(constraintWidget, i2, constraintAnchorArr[i6].d == null || !(constraintAnchorArr[i3].d == null || constraintAnchorArr[i6].d == null), 0));
            }
            constraintWidgetGroup.f1123e[i2] = i4;
            return i4;
        }
        throw null;
    }

    public static float a(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i2, float f2) {
        if (!a(xmlPullParser, str)) {
            return f2;
        }
        return typedArray.getFloat(i2, f2);
    }

    public static <T> T a(Object obj, Object obj2) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(String.valueOf(obj2));
    }

    /* JADX WARN: Type inference failed for: r4v9, types: [i.f.a.SolverVariable] */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        if (r2.e0 == 2) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0044, code lost:
        if (r2.f0 == 2) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0048, code lost:
        r5 = false;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x03cd  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x03f1  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x04bb  */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x04f0  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x0515  */
    /* JADX WARNING: Removed duplicated region for block: B:277:0x0518  */
    /* JADX WARNING: Removed duplicated region for block: B:280:0x051e  */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x0521  */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x0525  */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x0534  */
    /* JADX WARNING: Removed duplicated region for block: B:289:0x0537  */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x03ce A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0185  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(i.f.a.h.ConstraintWidgetContainer r33, i.f.a.LinearSystem r34, int r35, int r36, i.f.a.h.ChainHead r37) {
        /*
            r0 = r33
            r9 = r34
            r1 = r37
            i.f.a.h.ConstraintWidget r10 = r1.a
            i.f.a.h.ConstraintWidget r11 = r1.c
            i.f.a.h.ConstraintWidget r12 = r1.b
            i.f.a.h.ConstraintWidget r13 = r1.d
            i.f.a.h.ConstraintWidget r2 = r1.f1095e
            float r3 = r1.f1099k
            i.f.a.h.ConstraintWidget$a[] r4 = r0.C
            r4 = r4[r35]
            i.f.a.h.ConstraintWidget$a r5 = i.f.a.h.ConstraintWidget.a.WRAP_CONTENT
            r7 = 1
            if (r4 != r5) goto L_0x001d
            r4 = 1
            goto L_0x001e
        L_0x001d:
            r4 = 0
        L_0x001e:
            r5 = 2
            if (r35 != 0) goto L_0x0034
            int r8 = r2.e0
            if (r8 != 0) goto L_0x0027
            r8 = 1
            goto L_0x0028
        L_0x0027:
            r8 = 0
        L_0x0028:
            int r14 = r2.e0
            if (r14 != r7) goto L_0x002e
            r14 = 1
            goto L_0x002f
        L_0x002e:
            r14 = 0
        L_0x002f:
            int r15 = r2.e0
            if (r15 != r5) goto L_0x0048
            goto L_0x0046
        L_0x0034:
            int r8 = r2.f0
            if (r8 != 0) goto L_0x003a
            r8 = 1
            goto L_0x003b
        L_0x003a:
            r8 = 0
        L_0x003b:
            int r14 = r2.f0
            if (r14 != r7) goto L_0x0041
            r14 = 1
            goto L_0x0042
        L_0x0041:
            r14 = 0
        L_0x0042:
            int r15 = r2.f0
            if (r15 != r5) goto L_0x0048
        L_0x0046:
            r5 = 1
            goto L_0x0049
        L_0x0048:
            r5 = 0
        L_0x0049:
            r7 = r10
            r15 = r14
            r14 = r8
            r8 = 0
        L_0x004d:
            r21 = 0
            if (r8 != 0) goto L_0x0120
            i.f.a.h.ConstraintAnchor[] r6 = r7.A
            r6 = r6[r36]
            if (r4 != 0) goto L_0x005d
            if (r5 == 0) goto L_0x005a
            goto L_0x005d
        L_0x005a:
            r23 = 4
            goto L_0x005f
        L_0x005d:
            r23 = 1
        L_0x005f:
            int r24 = r6.a()
            r25 = r3
            i.f.a.h.ConstraintAnchor r3 = r6.d
            if (r3 == 0) goto L_0x0071
            if (r7 == r10) goto L_0x0071
            int r3 = r3.a()
            int r24 = r3 + r24
        L_0x0071:
            r3 = r24
            if (r5 == 0) goto L_0x007f
            if (r7 == r10) goto L_0x007f
            if (r7 == r12) goto L_0x007f
            r24 = r8
            r23 = r15
            r8 = 6
            goto L_0x008f
        L_0x007f:
            if (r14 == 0) goto L_0x0089
            if (r4 == 0) goto L_0x0089
            r24 = r8
            r23 = r15
            r8 = 4
            goto L_0x008f
        L_0x0089:
            r24 = r8
            r8 = r23
            r23 = r15
        L_0x008f:
            i.f.a.h.ConstraintAnchor r15 = r6.d
            if (r15 == 0) goto L_0x00b8
            if (r7 != r12) goto L_0x00a2
            r26 = r14
            i.f.a.SolverVariable r14 = r6.f1108i
            i.f.a.SolverVariable r15 = r15.f1108i
            r27 = r2
            r2 = 5
            r9.b(r14, r15, r3, r2)
            goto L_0x00ae
        L_0x00a2:
            r27 = r2
            r26 = r14
            i.f.a.SolverVariable r2 = r6.f1108i
            i.f.a.SolverVariable r14 = r15.f1108i
            r15 = 6
            r9.b(r2, r14, r3, r15)
        L_0x00ae:
            i.f.a.SolverVariable r2 = r6.f1108i
            i.f.a.h.ConstraintAnchor r6 = r6.d
            i.f.a.SolverVariable r6 = r6.f1108i
            r9.a(r2, r6, r3, r8)
            goto L_0x00bc
        L_0x00b8:
            r27 = r2
            r26 = r14
        L_0x00bc:
            if (r4 == 0) goto L_0x00ef
            int r2 = r7.Y
            r3 = 8
            if (r2 == r3) goto L_0x00de
            i.f.a.h.ConstraintWidget$a[] r2 = r7.C
            r2 = r2[r35]
            i.f.a.h.ConstraintWidget$a r3 = i.f.a.h.ConstraintWidget.a.MATCH_CONSTRAINT
            if (r2 != r3) goto L_0x00de
            i.f.a.h.ConstraintAnchor[] r2 = r7.A
            int r3 = r36 + 1
            r3 = r2[r3]
            i.f.a.SolverVariable r3 = r3.f1108i
            r2 = r2[r36]
            i.f.a.SolverVariable r2 = r2.f1108i
            r6 = 5
            r8 = 0
            r9.b(r3, r2, r8, r6)
            goto L_0x00df
        L_0x00de:
            r8 = 0
        L_0x00df:
            i.f.a.h.ConstraintAnchor[] r2 = r7.A
            r2 = r2[r36]
            i.f.a.SolverVariable r2 = r2.f1108i
            i.f.a.h.ConstraintAnchor[] r3 = r0.A
            r3 = r3[r36]
            i.f.a.SolverVariable r3 = r3.f1108i
            r6 = 6
            r9.b(r2, r3, r8, r6)
        L_0x00ef:
            i.f.a.h.ConstraintAnchor[] r2 = r7.A
            int r3 = r36 + 1
            r2 = r2[r3]
            i.f.a.h.ConstraintAnchor r2 = r2.d
            if (r2 == 0) goto L_0x010e
            i.f.a.h.ConstraintWidget r2 = r2.b
            i.f.a.h.ConstraintAnchor[] r3 = r2.A
            r6 = r3[r36]
            i.f.a.h.ConstraintAnchor r6 = r6.d
            if (r6 == 0) goto L_0x010e
            r3 = r3[r36]
            i.f.a.h.ConstraintAnchor r3 = r3.d
            i.f.a.h.ConstraintWidget r3 = r3.b
            if (r3 == r7) goto L_0x010c
            goto L_0x010e
        L_0x010c:
            r21 = r2
        L_0x010e:
            if (r21 == 0) goto L_0x0115
            r7 = r21
            r8 = r24
            goto L_0x0116
        L_0x0115:
            r8 = 1
        L_0x0116:
            r15 = r23
            r3 = r25
            r14 = r26
            r2 = r27
            goto L_0x004d
        L_0x0120:
            r27 = r2
            r25 = r3
            r26 = r14
            r23 = r15
            if (r13 == 0) goto L_0x014a
            i.f.a.h.ConstraintAnchor[] r2 = r11.A
            int r3 = r36 + 1
            r6 = r2[r3]
            i.f.a.h.ConstraintAnchor r6 = r6.d
            if (r6 == 0) goto L_0x014a
            i.f.a.h.ConstraintAnchor[] r6 = r13.A
            r6 = r6[r3]
            i.f.a.SolverVariable r7 = r6.f1108i
            r2 = r2[r3]
            i.f.a.h.ConstraintAnchor r2 = r2.d
            i.f.a.SolverVariable r2 = r2.f1108i
            int r3 = r6.a()
            int r3 = -r3
            r6 = 5
            r9.c(r7, r2, r3, r6)
            goto L_0x014b
        L_0x014a:
            r6 = 5
        L_0x014b:
            if (r4 == 0) goto L_0x0165
            i.f.a.h.ConstraintAnchor[] r0 = r0.A
            int r2 = r36 + 1
            r0 = r0[r2]
            i.f.a.SolverVariable r0 = r0.f1108i
            i.f.a.h.ConstraintAnchor[] r3 = r11.A
            r4 = r3[r2]
            i.f.a.SolverVariable r4 = r4.f1108i
            r2 = r3[r2]
            int r2 = r2.a()
            r3 = 6
            r9.b(r0, r4, r2, r3)
        L_0x0165:
            java.util.ArrayList<i.f.a.h.d> r0 = r1.h
            if (r0 == 0) goto L_0x0272
            int r2 = r0.size()
            r3 = 1
            if (r2 <= r3) goto L_0x0272
            boolean r4 = r1.f1102n
            if (r4 == 0) goto L_0x017c
            boolean r4 = r1.f1104p
            if (r4 != 0) goto L_0x017c
            int r4 = r1.f1098j
            float r4 = (float) r4
            goto L_0x017e
        L_0x017c:
            r4 = r25
        L_0x017e:
            r7 = 0
            r14 = r21
            r8 = 0
            r15 = 0
        L_0x0183:
            if (r8 >= r2) goto L_0x0272
            java.lang.Object r16 = r0.get(r8)
            r3 = r16
            i.f.a.h.ConstraintWidget r3 = (i.f.a.h.ConstraintWidget) r3
            float[] r6 = r3.g0
            r6 = r6[r35]
            r24 = r0
            int r25 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r25 >= 0) goto L_0x01b0
            boolean r6 = r1.f1104p
            if (r6 == 0) goto L_0x01ae
            i.f.a.h.ConstraintAnchor[] r0 = r3.A
            int r3 = r36 + 1
            r3 = r0[r3]
            i.f.a.SolverVariable r3 = r3.f1108i
            r0 = r0[r36]
            i.f.a.SolverVariable r0 = r0.f1108i
            r6 = 4
            r7 = 0
            r9.a(r3, r0, r7, r6)
            r6 = 6
            goto L_0x01c8
        L_0x01ae:
            r6 = 1065353216(0x3f800000, float:1.0)
        L_0x01b0:
            r7 = 4
            r20 = 0
            int r25 = (r6 > r20 ? 1 : (r6 == r20 ? 0 : -1))
            if (r25 != 0) goto L_0x01d0
            i.f.a.h.ConstraintAnchor[] r0 = r3.A
            int r3 = r36 + 1
            r3 = r0[r3]
            i.f.a.SolverVariable r3 = r3.f1108i
            r0 = r0[r36]
            i.f.a.SolverVariable r0 = r0.f1108i
            r6 = 6
            r7 = 0
            r9.a(r3, r0, r7, r6)
        L_0x01c8:
            r29 = r2
            r30 = r11
            r17 = 6
            goto L_0x0263
        L_0x01d0:
            r7 = 0
            r17 = 6
            if (r14 == 0) goto L_0x025a
            i.f.a.h.ConstraintAnchor[] r14 = r14.A
            r7 = r14[r36]
            i.f.a.SolverVariable r7 = r7.f1108i
            int r28 = r36 + 1
            r14 = r14[r28]
            i.f.a.SolverVariable r14 = r14.f1108i
            i.f.a.h.ConstraintAnchor[] r0 = r3.A
            r29 = r2
            r2 = r0[r36]
            i.f.a.SolverVariable r2 = r2.f1108i
            r0 = r0[r28]
            i.f.a.SolverVariable r0 = r0.f1108i
            r28 = r3
            i.f.a.ArrayRow r3 = r34.b()
            r1 = 0
            r3.b = r1
            r30 = r11
            r11 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r31 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r31 == 0) goto L_0x0240
            int r31 = (r15 > r6 ? 1 : (r15 == r6 ? 0 : -1))
            if (r31 != 0) goto L_0x0203
            goto L_0x0240
        L_0x0203:
            int r31 = (r15 > r1 ? 1 : (r15 == r1 ? 0 : -1))
            if (r31 != 0) goto L_0x0214
            i.f.a.ArrayLinkedVariables r0 = r3.d
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.a(r7, r2)
            i.f.a.ArrayLinkedVariables r0 = r3.d
            r0.a(r14, r11)
            goto L_0x0256
        L_0x0214:
            r1 = 1065353216(0x3f800000, float:1.0)
            if (r25 != 0) goto L_0x0223
            i.f.a.ArrayLinkedVariables r7 = r3.d
            r7.a(r2, r1)
            i.f.a.ArrayLinkedVariables r1 = r3.d
            r1.a(r0, r11)
            goto L_0x0256
        L_0x0223:
            float r15 = r15 / r4
            float r25 = r6 / r4
            float r15 = r15 / r25
            i.f.a.ArrayLinkedVariables r11 = r3.d
            r11.a(r7, r1)
            i.f.a.ArrayLinkedVariables r1 = r3.d
            r11 = -1082130432(0xffffffffbf800000, float:-1.0)
            r1.a(r14, r11)
            i.f.a.ArrayLinkedVariables r1 = r3.d
            r1.a(r0, r15)
            i.f.a.ArrayLinkedVariables r0 = r3.d
            float r1 = -r15
            r0.a(r2, r1)
            goto L_0x0256
        L_0x0240:
            r1 = 1065353216(0x3f800000, float:1.0)
            i.f.a.ArrayLinkedVariables r15 = r3.d
            r15.a(r7, r1)
            i.f.a.ArrayLinkedVariables r7 = r3.d
            r7.a(r14, r11)
            i.f.a.ArrayLinkedVariables r7 = r3.d
            r7.a(r0, r1)
            i.f.a.ArrayLinkedVariables r0 = r3.d
            r0.a(r2, r11)
        L_0x0256:
            r9.a(r3)
            goto L_0x0260
        L_0x025a:
            r29 = r2
            r28 = r3
            r30 = r11
        L_0x0260:
            r15 = r6
            r14 = r28
        L_0x0263:
            int r8 = r8 + 1
            r1 = r37
            r0 = r24
            r2 = r29
            r11 = r30
            r3 = 1
            r6 = 5
            r7 = 0
            goto L_0x0183
        L_0x0272:
            r30 = r11
            r17 = 6
            if (r12 == 0) goto L_0x02da
            if (r12 == r13) goto L_0x027c
            if (r5 == 0) goto L_0x02da
        L_0x027c:
            i.f.a.h.ConstraintAnchor[] r0 = r10.A
            r1 = r0[r36]
            r11 = r30
            i.f.a.h.ConstraintAnchor[] r2 = r11.A
            int r3 = r36 + 1
            r2 = r2[r3]
            r4 = r0[r36]
            i.f.a.h.ConstraintAnchor r4 = r4.d
            if (r4 == 0) goto L_0x0296
            r0 = r0[r36]
            i.f.a.h.ConstraintAnchor r0 = r0.d
            i.f.a.SolverVariable r0 = r0.f1108i
            r4 = r0
            goto L_0x0298
        L_0x0296:
            r4 = r21
        L_0x0298:
            i.f.a.h.ConstraintAnchor[] r0 = r11.A
            r5 = r0[r3]
            i.f.a.h.ConstraintAnchor r5 = r5.d
            if (r5 == 0) goto L_0x02a8
            r0 = r0[r3]
            i.f.a.h.ConstraintAnchor r0 = r0.d
            i.f.a.SolverVariable r0 = r0.f1108i
            r5 = r0
            goto L_0x02aa
        L_0x02a8:
            r5 = r21
        L_0x02aa:
            if (r12 != r13) goto L_0x02b2
            i.f.a.h.ConstraintAnchor[] r0 = r12.A
            r1 = r0[r36]
            r2 = r0[r3]
        L_0x02b2:
            if (r4 == 0) goto L_0x0501
            if (r5 == 0) goto L_0x0501
            if (r35 != 0) goto L_0x02bd
            r0 = r27
            float r0 = r0.V
            goto L_0x02c1
        L_0x02bd:
            r0 = r27
            float r0 = r0.W
        L_0x02c1:
            r6 = r0
            int r3 = r1.a()
            int r7 = r2.a()
            i.f.a.SolverVariable r1 = r1.f1108i
            i.f.a.SolverVariable r8 = r2.f1108i
            r10 = 5
            r0 = r34
            r2 = r4
            r4 = r6
            r6 = r8
            r8 = r10
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0501
        L_0x02da:
            r11 = r30
            if (r26 == 0) goto L_0x03d4
            if (r12 == 0) goto L_0x03d4
            r0 = r37
            int r1 = r0.f1098j
            if (r1 <= 0) goto L_0x02ed
            int r0 = r0.f1097i
            if (r0 != r1) goto L_0x02ed
            r19 = 1
            goto L_0x02ef
        L_0x02ed:
            r19 = 0
        L_0x02ef:
            r14 = r12
            r15 = r14
        L_0x02f1:
            if (r14 == 0) goto L_0x0501
            i.f.a.h.ConstraintWidget[] r0 = r14.i0
            r0 = r0[r35]
            r8 = r0
        L_0x02f8:
            if (r8 == 0) goto L_0x0305
            int r0 = r8.Y
            r6 = 8
            if (r0 != r6) goto L_0x0307
            i.f.a.h.ConstraintWidget[] r0 = r8.i0
            r8 = r0[r35]
            goto L_0x02f8
        L_0x0305:
            r6 = 8
        L_0x0307:
            if (r8 != 0) goto L_0x0314
            if (r14 != r13) goto L_0x030c
            goto L_0x0314
        L_0x030c:
            r16 = r8
            r17 = 4
            r18 = 6
            goto L_0x03c7
        L_0x0314:
            i.f.a.h.ConstraintAnchor[] r0 = r14.A
            r0 = r0[r36]
            i.f.a.SolverVariable r1 = r0.f1108i
            i.f.a.h.ConstraintAnchor r2 = r0.d
            if (r2 == 0) goto L_0x0321
            i.f.a.SolverVariable r2 = r2.f1108i
            goto L_0x0323
        L_0x0321:
            r2 = r21
        L_0x0323:
            if (r15 == r14) goto L_0x032e
            i.f.a.h.ConstraintAnchor[] r2 = r15.A
            int r3 = r36 + 1
            r2 = r2[r3]
            i.f.a.SolverVariable r2 = r2.f1108i
            goto L_0x0343
        L_0x032e:
            if (r14 != r12) goto L_0x0343
            if (r15 != r14) goto L_0x0343
            i.f.a.h.ConstraintAnchor[] r2 = r10.A
            r3 = r2[r36]
            i.f.a.h.ConstraintAnchor r3 = r3.d
            if (r3 == 0) goto L_0x0341
            r2 = r2[r36]
            i.f.a.h.ConstraintAnchor r2 = r2.d
            i.f.a.SolverVariable r2 = r2.f1108i
            goto L_0x0343
        L_0x0341:
            r2 = r21
        L_0x0343:
            int r0 = r0.a()
            i.f.a.h.ConstraintAnchor[] r3 = r14.A
            int r4 = r36 + 1
            r3 = r3[r4]
            int r3 = r3.a()
            if (r8 == 0) goto L_0x0365
            i.f.a.h.ConstraintAnchor[] r5 = r8.A
            r5 = r5[r36]
            i.f.a.SolverVariable r7 = r5.f1108i
            i.f.a.h.ConstraintAnchor[] r6 = r14.A
            r6 = r6[r4]
            i.f.a.SolverVariable r6 = r6.f1108i
            r32 = r7
            r7 = r6
            r6 = r32
            goto L_0x0378
        L_0x0365:
            i.f.a.h.ConstraintAnchor[] r5 = r11.A
            r5 = r5[r4]
            i.f.a.h.ConstraintAnchor r5 = r5.d
            if (r5 == 0) goto L_0x0370
            i.f.a.SolverVariable r6 = r5.f1108i
            goto L_0x0372
        L_0x0370:
            r6 = r21
        L_0x0372:
            i.f.a.h.ConstraintAnchor[] r7 = r14.A
            r7 = r7[r4]
            i.f.a.SolverVariable r7 = r7.f1108i
        L_0x0378:
            if (r5 == 0) goto L_0x037f
            int r5 = r5.a()
            int r3 = r3 + r5
        L_0x037f:
            if (r15 == 0) goto L_0x038a
            i.f.a.h.ConstraintAnchor[] r5 = r15.A
            r5 = r5[r4]
            int r5 = r5.a()
            int r0 = r0 + r5
        L_0x038a:
            if (r1 == 0) goto L_0x030c
            if (r2 == 0) goto L_0x030c
            if (r6 == 0) goto L_0x030c
            if (r7 == 0) goto L_0x030c
            if (r14 != r12) goto L_0x039c
            i.f.a.h.ConstraintAnchor[] r0 = r12.A
            r0 = r0[r36]
            int r0 = r0.a()
        L_0x039c:
            r5 = r0
            if (r14 != r13) goto L_0x03aa
            i.f.a.h.ConstraintAnchor[] r0 = r13.A
            r0 = r0[r4]
            int r0 = r0.a()
            r16 = r0
            goto L_0x03ac
        L_0x03aa:
            r16 = r3
        L_0x03ac:
            if (r19 == 0) goto L_0x03b1
            r22 = 6
            goto L_0x03b3
        L_0x03b1:
            r22 = 4
        L_0x03b3:
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r34
            r3 = r5
            r5 = r6
            r17 = 4
            r18 = 6
            r6 = r7
            r7 = r16
            r16 = r8
            r8 = r22
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x03c7:
            int r0 = r14.Y
            r8 = 8
            if (r0 == r8) goto L_0x03ce
            r15 = r14
        L_0x03ce:
            r14 = r16
            r17 = 6
            goto L_0x02f1
        L_0x03d4:
            r0 = r37
            r8 = 8
            r17 = 4
            r18 = 6
            if (r23 == 0) goto L_0x0501
            if (r12 == 0) goto L_0x0501
            int r1 = r0.f1098j
            if (r1 <= 0) goto L_0x03eb
            int r0 = r0.f1097i
            if (r0 != r1) goto L_0x03eb
            r19 = 1
            goto L_0x03ed
        L_0x03eb:
            r19 = 0
        L_0x03ed:
            r14 = r12
            r15 = r14
        L_0x03ef:
            if (r14 == 0) goto L_0x04a3
            i.f.a.h.ConstraintWidget[] r0 = r14.i0
            r0 = r0[r35]
        L_0x03f5:
            if (r0 == 0) goto L_0x0400
            int r1 = r0.Y
            if (r1 != r8) goto L_0x0400
            i.f.a.h.ConstraintWidget[] r0 = r0.i0
            r0 = r0[r35]
            goto L_0x03f5
        L_0x0400:
            if (r14 == r12) goto L_0x0492
            if (r14 == r13) goto L_0x0492
            if (r0 == 0) goto L_0x0492
            if (r0 != r13) goto L_0x040b
            r7 = r21
            goto L_0x040c
        L_0x040b:
            r7 = r0
        L_0x040c:
            i.f.a.h.ConstraintAnchor[] r0 = r14.A
            r0 = r0[r36]
            i.f.a.SolverVariable r1 = r0.f1108i
            i.f.a.h.ConstraintAnchor r2 = r0.d
            if (r2 == 0) goto L_0x0418
            i.f.a.SolverVariable r2 = r2.f1108i
        L_0x0418:
            i.f.a.h.ConstraintAnchor[] r2 = r15.A
            int r3 = r36 + 1
            r2 = r2[r3]
            i.f.a.SolverVariable r2 = r2.f1108i
            int r0 = r0.a()
            i.f.a.h.ConstraintAnchor[] r4 = r14.A
            r4 = r4[r3]
            int r4 = r4.a()
            if (r7 == 0) goto L_0x043e
            i.f.a.h.ConstraintAnchor[] r5 = r7.A
            r5 = r5[r36]
            i.f.a.SolverVariable r6 = r5.f1108i
            i.f.a.h.ConstraintAnchor r8 = r5.d
            if (r8 == 0) goto L_0x043b
            i.f.a.SolverVariable r8 = r8.f1108i
            goto L_0x0451
        L_0x043b:
            r8 = r21
            goto L_0x0451
        L_0x043e:
            i.f.a.h.ConstraintAnchor[] r5 = r14.A
            r5 = r5[r3]
            i.f.a.h.ConstraintAnchor r5 = r5.d
            if (r5 == 0) goto L_0x0449
            i.f.a.SolverVariable r6 = r5.f1108i
            goto L_0x044b
        L_0x0449:
            r6 = r21
        L_0x044b:
            i.f.a.h.ConstraintAnchor[] r8 = r14.A
            r8 = r8[r3]
            i.f.a.SolverVariable r8 = r8.f1108i
        L_0x0451:
            if (r5 == 0) goto L_0x045b
            int r5 = r5.a()
            int r5 = r5 + r4
            r20 = r5
            goto L_0x045d
        L_0x045b:
            r20 = r4
        L_0x045d:
            i.f.a.h.ConstraintAnchor[] r4 = r15.A
            r3 = r4[r3]
            int r3 = r3.a()
            int r3 = r3 + r0
            if (r19 == 0) goto L_0x046b
            r22 = 6
            goto L_0x046d
        L_0x046b:
            r22 = 4
        L_0x046d:
            if (r1 == 0) goto L_0x0489
            if (r2 == 0) goto L_0x0489
            if (r6 == 0) goto L_0x0489
            if (r8 == 0) goto L_0x0489
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r34
            r5 = r6
            r6 = r8
            r16 = r7
            r7 = r20
            r20 = r15
            r15 = 8
            r8 = r22
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x048f
        L_0x0489:
            r16 = r7
            r20 = r15
            r15 = 8
        L_0x048f:
            r0 = r16
            goto L_0x0496
        L_0x0492:
            r20 = r15
            r15 = 8
        L_0x0496:
            int r1 = r14.Y
            if (r1 == r15) goto L_0x049b
            goto L_0x049d
        L_0x049b:
            r14 = r20
        L_0x049d:
            r15 = r14
            r8 = 8
            r14 = r0
            goto L_0x03ef
        L_0x04a3:
            i.f.a.h.ConstraintAnchor[] r0 = r12.A
            r0 = r0[r36]
            i.f.a.h.ConstraintAnchor[] r1 = r10.A
            r1 = r1[r36]
            i.f.a.h.ConstraintAnchor r1 = r1.d
            i.f.a.h.ConstraintAnchor[] r2 = r13.A
            int r3 = r36 + 1
            r10 = r2[r3]
            i.f.a.h.ConstraintAnchor[] r2 = r11.A
            r2 = r2[r3]
            i.f.a.h.ConstraintAnchor r14 = r2.d
            if (r1 == 0) goto L_0x04f0
            if (r12 == r13) goto L_0x04ca
            i.f.a.SolverVariable r2 = r0.f1108i
            i.f.a.SolverVariable r1 = r1.f1108i
            int r0 = r0.a()
            r15 = 5
            r9.a(r2, r1, r0, r15)
            goto L_0x04f1
        L_0x04ca:
            r15 = 5
            if (r14 == 0) goto L_0x04f1
            i.f.a.SolverVariable r2 = r0.f1108i
            i.f.a.SolverVariable r3 = r1.f1108i
            int r4 = r0.a()
            r5 = 1056964608(0x3f000000, float:0.5)
            i.f.a.SolverVariable r6 = r10.f1108i
            i.f.a.SolverVariable r7 = r14.f1108i
            int r8 = r10.a()
            r16 = 5
            r0 = r34
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r16
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x04f1
        L_0x04f0:
            r15 = 5
        L_0x04f1:
            if (r14 == 0) goto L_0x0501
            if (r12 == r13) goto L_0x0501
            i.f.a.SolverVariable r0 = r10.f1108i
            i.f.a.SolverVariable r1 = r14.f1108i
            int r2 = r10.a()
            int r2 = -r2
            r9.a(r0, r1, r2, r15)
        L_0x0501:
            if (r26 != 0) goto L_0x0505
            if (r23 == 0) goto L_0x0563
        L_0x0505:
            if (r12 == 0) goto L_0x0563
            i.f.a.h.ConstraintAnchor[] r0 = r12.A
            r0 = r0[r36]
            i.f.a.h.ConstraintAnchor[] r1 = r13.A
            int r2 = r36 + 1
            r1 = r1[r2]
            i.f.a.h.ConstraintAnchor r3 = r0.d
            if (r3 == 0) goto L_0x0518
            i.f.a.SolverVariable r3 = r3.f1108i
            goto L_0x051a
        L_0x0518:
            r3 = r21
        L_0x051a:
            i.f.a.h.ConstraintAnchor r4 = r1.d
            if (r4 == 0) goto L_0x0521
            i.f.a.SolverVariable r4 = r4.f1108i
            goto L_0x0523
        L_0x0521:
            r4 = r21
        L_0x0523:
            if (r11 == r13) goto L_0x0534
            i.f.a.h.ConstraintAnchor[] r4 = r11.A
            r4 = r4[r2]
            i.f.a.h.ConstraintAnchor r4 = r4.d
            if (r4 == 0) goto L_0x0531
            i.f.a.SolverVariable r4 = r4.f1108i
            r21 = r4
        L_0x0531:
            r5 = r21
            goto L_0x0535
        L_0x0534:
            r5 = r4
        L_0x0535:
            if (r12 != r13) goto L_0x0542
            i.f.a.h.ConstraintAnchor[] r0 = r12.A
            r1 = r0[r36]
            r0 = r0[r2]
            r32 = r1
            r1 = r0
            r0 = r32
        L_0x0542:
            if (r3 == 0) goto L_0x0563
            if (r5 == 0) goto L_0x0563
            r4 = 1056964608(0x3f000000, float:0.5)
            int r6 = r0.a()
            i.f.a.h.ConstraintAnchor[] r7 = r13.A
            r2 = r7[r2]
            int r7 = r2.a()
            i.f.a.SolverVariable r2 = r0.f1108i
            i.f.a.SolverVariable r8 = r1.f1108i
            r10 = 5
            r0 = r34
            r1 = r2
            r2 = r3
            r3 = r6
            r6 = r8
            r8 = r10
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0563:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void");
    }

    public static ColorStateList a(Resources resources, XmlPullParser xmlPullParser, Resources.Theme theme) {
        int next;
        AttributeSet asAttributeSet = Xml.asAttributeSet(xmlPullParser);
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return a(resources, xmlPullParser, asAttributeSet, theme);
        }
        throw new XmlPullParserException("No start tag found");
    }

    public static void a(PopupWindow popupWindow, boolean z) {
        if (Build.VERSION.SDK_INT >= 23) {
            popupWindow.setOverlapAnchor(z);
            return;
        }
        if (!f777r) {
            try {
                Field declaredField = PopupWindow.class.getDeclaredField("mOverlapAnchor");
                f776q = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e2);
            }
            f777r = true;
        }
        Field field = f776q;
        if (field != null) {
            try {
                field.set(popupWindow, Boolean.valueOf(z));
            } catch (IllegalAccessException e3) {
                Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e3);
            }
        }
    }

    public static void a(Parcel parcel, int i2, String str, boolean z) {
        if (str != null) {
            int j2 = j(parcel, i2);
            parcel.writeString(str);
            k(parcel, j2);
        } else if (z) {
            d(parcel, i2, 0);
        }
    }

    public static <T extends Parcelable> T a(Parcel parcel, int i2, Parcelable.Creator creator) {
        int h2 = h(parcel, i2);
        int dataPosition = parcel.dataPosition();
        if (h2 == 0) {
            return null;
        }
        T t2 = (Parcelable) creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + h2);
        return t2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0035, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003a, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r9.addSuppressed(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004a, code lost:
        throw r9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.nio.ByteBuffer a(android.content.Context r7, android.os.CancellationSignal r8, android.net.Uri r9) {
        /*
            android.content.ContentResolver r7 = r7.getContentResolver()
            r0 = 0
            java.lang.String r1 = "r"
            android.os.ParcelFileDescriptor r7 = r7.openFileDescriptor(r9, r1, r8)     // Catch:{ IOException -> 0x004b }
            if (r7 != 0) goto L_0x0013
            if (r7 == 0) goto L_0x0012
            r7.close()     // Catch:{ IOException -> 0x004b }
        L_0x0012:
            return r0
        L_0x0013:
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ all -> 0x003f }
            java.io.FileDescriptor r9 = r7.getFileDescriptor()     // Catch:{ all -> 0x003f }
            r8.<init>(r9)     // Catch:{ all -> 0x003f }
            java.nio.channels.FileChannel r1 = r8.getChannel()     // Catch:{ all -> 0x0033 }
            long r5 = r1.size()     // Catch:{ all -> 0x0033 }
            java.nio.channels.FileChannel$MapMode r2 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x0033 }
            r3 = 0
            java.nio.MappedByteBuffer r9 = r1.map(r2, r3, r5)     // Catch:{ all -> 0x0033 }
            r8.close()     // Catch:{ all -> 0x003f }
            r7.close()     // Catch:{ IOException -> 0x004b }
            return r9
        L_0x0033:
            r9 = move-exception
            throw r9     // Catch:{ all -> 0x0035 }
        L_0x0035:
            r1 = move-exception
            r8.close()     // Catch:{ all -> 0x003a }
            goto L_0x003e
        L_0x003a:
            r8 = move-exception
            r9.addSuppressed(r8)     // Catch:{ all -> 0x003f }
        L_0x003e:
            throw r1     // Catch:{ all -> 0x003f }
        L_0x003f:
            r8 = move-exception
            throw r8     // Catch:{ all -> 0x0041 }
        L_0x0041:
            r9 = move-exception
            r7.close()     // Catch:{ all -> 0x0046 }
            goto L_0x004a
        L_0x0046:
            r7 = move-exception
            r8.addSuppressed(r7)     // Catch:{ IOException -> 0x004b }
        L_0x004a:
            throw r9     // Catch:{ IOException -> 0x004b }
        L_0x004b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(android.content.Context, android.os.CancellationSignal, android.net.Uri):java.nio.ByteBuffer");
    }

    public static Bundle a(Parcel parcel, int i2) {
        int h2 = h(parcel, i2);
        int dataPosition = parcel.dataPosition();
        if (h2 == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + h2);
        return readBundle;
    }

    public static void a(Parcel parcel, int i2, IBinder iBinder, boolean z) {
        if (iBinder != null) {
            int j2 = j(parcel, i2);
            parcel.writeStrongBinder(iBinder);
            k(parcel, j2);
        } else if (z) {
            d(parcel, i2, 0);
        }
    }

    /* JADX WARN: Type inference failed for: r8v16, types: [java.lang.Object[], java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.res.ColorStateList a(android.content.res.Resources r17, org.xmlpull.v1.XmlPullParser r18, android.util.AttributeSet r19, android.content.res.Resources.Theme r20) {
        /*
            r0 = r19
            r1 = r20
            java.lang.String r2 = r18.getName()
            java.lang.String r3 = "selector"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x010b
            int r2 = r18.getDepth()
            r3 = 1
            int r2 = r2 + r3
            r4 = 20
            int[][] r5 = new int[r4][]
            int[] r4 = new int[r4]
            r6 = 0
            r7 = 0
        L_0x001e:
            int r8 = r18.next()
            if (r8 == r3) goto L_0x00fb
            int r9 = r18.getDepth()
            if (r9 >= r2) goto L_0x002d
            r10 = 3
            if (r8 == r10) goto L_0x00fb
        L_0x002d:
            r10 = 2
            if (r8 != r10) goto L_0x00f6
            if (r9 > r2) goto L_0x00f6
            java.lang.String r8 = r18.getName()
            java.lang.String r9 = "item"
            boolean r8 = r8.equals(r9)
            if (r8 != 0) goto L_0x0040
            goto L_0x00f6
        L_0x0040:
            int[] r8 = i.h.c.ColorStateListItem
            if (r1 != 0) goto L_0x004b
            r9 = r17
            android.content.res.TypedArray r8 = r9.obtainAttributes(r0, r8)
            goto L_0x0051
        L_0x004b:
            r9 = r17
            android.content.res.TypedArray r8 = r1.obtainStyledAttributes(r0, r8, r6, r6)
        L_0x0051:
            int r10 = i.h.c.ColorStateListItem_android_color
            r11 = -65281(0xffffffffffff00ff, float:NaN)
            int r10 = r8.getColor(r10, r11)
            r11 = 1065353216(0x3f800000, float:1.0)
            int r12 = i.h.c.ColorStateListItem_android_alpha
            boolean r12 = r8.hasValue(r12)
            if (r12 == 0) goto L_0x006b
            int r12 = i.h.c.ColorStateListItem_android_alpha
            float r11 = r8.getFloat(r12, r11)
            goto L_0x0079
        L_0x006b:
            int r12 = i.h.c.ColorStateListItem_alpha
            boolean r12 = r8.hasValue(r12)
            if (r12 == 0) goto L_0x0079
            int r12 = i.h.c.ColorStateListItem_alpha
            float r11 = r8.getFloat(r12, r11)
        L_0x0079:
            r8.recycle()
            int r8 = r19.getAttributeCount()
            int[] r12 = new int[r8]
            r13 = 0
            r14 = 0
        L_0x0084:
            if (r13 >= r8) goto L_0x00a9
            int r15 = r0.getAttributeNameResource(r13)
            r3 = 16843173(0x10101a5, float:2.3694738E-38)
            if (r15 == r3) goto L_0x00a5
            r3 = 16843551(0x101031f, float:2.3695797E-38)
            if (r15 == r3) goto L_0x00a5
            int r3 = i.h.a.alpha
            if (r15 == r3) goto L_0x00a5
            int r3 = r14 + 1
            boolean r16 = r0.getAttributeBooleanValue(r13, r6)
            if (r16 == 0) goto L_0x00a1
            goto L_0x00a2
        L_0x00a1:
            int r15 = -r15
        L_0x00a2:
            r12[r14] = r15
            r14 = r3
        L_0x00a5:
            int r13 = r13 + 1
            r3 = 1
            goto L_0x0084
        L_0x00a9:
            int[] r3 = android.util.StateSet.trimStateSet(r12, r14)
            int r8 = android.graphics.Color.alpha(r10)
            float r8 = (float) r8
            float r8 = r8 * r11
            int r8 = java.lang.Math.round(r8)
            r11 = 16777215(0xffffff, float:2.3509886E-38)
            r10 = r10 & r11
            int r8 = r8 << 24
            r8 = r8 | r10
            int r10 = r7 + 1
            int r11 = r4.length
            r12 = 4
            r13 = 8
            if (r10 <= r11) goto L_0x00d4
            if (r7 > r12) goto L_0x00cc
            r11 = 8
            goto L_0x00ce
        L_0x00cc:
            int r11 = r7 * 2
        L_0x00ce:
            int[] r11 = new int[r11]
            java.lang.System.arraycopy(r4, r6, r11, r6, r7)
            r4 = r11
        L_0x00d4:
            r4[r7] = r8
            int r8 = r5.length
            if (r10 <= r8) goto L_0x00f0
            java.lang.Class r8 = r5.getClass()
            java.lang.Class r8 = r8.getComponentType()
            if (r7 > r12) goto L_0x00e4
            goto L_0x00e6
        L_0x00e4:
            int r13 = r7 * 2
        L_0x00e6:
            java.lang.Object r8 = java.lang.reflect.Array.newInstance(r8, r13)
            java.lang.Object[] r8 = (java.lang.Object[]) r8
            java.lang.System.arraycopy(r5, r6, r8, r6, r7)
            r5 = r8
        L_0x00f0:
            r5[r7] = r3
            int[][] r5 = (int[][]) r5
            r7 = r10
            goto L_0x00f8
        L_0x00f6:
            r9 = r17
        L_0x00f8:
            r3 = 1
            goto L_0x001e
        L_0x00fb:
            int[] r0 = new int[r7]
            int[][] r1 = new int[r7][]
            java.lang.System.arraycopy(r4, r6, r0, r6, r7)
            java.lang.System.arraycopy(r5, r6, r1, r6, r7)
            android.content.res.ColorStateList r2 = new android.content.res.ColorStateList
            r2.<init>(r1, r0)
            return r2
        L_0x010b:
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r18.getPositionDescription()
            r1.append(r3)
            java.lang.String r3 = ": invalid color state list tag "
            r1.append(r3)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList");
    }

    public static void a(Parcel parcel, int i2, Parcelable parcelable, int i3, boolean z) {
        if (parcelable != null) {
            int j2 = j(parcel, i2);
            parcelable.writeToParcel(parcel, i3);
            k(parcel, j2);
        } else if (z) {
            d(parcel, i2, 0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
     arg types: [i.f.a.h.ConstraintWidget, i.f.a.h.ConstraintWidgetGroup, java.util.List<i.f.a.h.f>, boolean]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean */
    public static void a(ConstraintWidgetContainer constraintWidgetContainer) {
        if ((constraintWidgetContainer.C0 & 32) != 32) {
            constraintWidgetContainer.w0.clear();
            constraintWidgetContainer.w0.add(0, new ConstraintWidgetGroup(constraintWidgetContainer.k0));
            return;
        }
        constraintWidgetContainer.D0 = true;
        constraintWidgetContainer.x0 = false;
        constraintWidgetContainer.y0 = false;
        constraintWidgetContainer.z0 = false;
        ArrayList<d> arrayList = constraintWidgetContainer.k0;
        List<f> list = constraintWidgetContainer.w0;
        boolean z = constraintWidgetContainer.e() == ConstraintWidget.a.WRAP_CONTENT;
        boolean z2 = constraintWidgetContainer.h() == ConstraintWidget.a.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (ConstraintWidget next : arrayList) {
            next.f1118p = null;
            next.d0 = false;
            next.l();
        }
        for (ConstraintWidget next2 : arrayList) {
            if (next2.f1118p == null) {
                ConstraintWidgetGroup constraintWidgetGroup = new ConstraintWidgetGroup(new ArrayList(), true);
                list.add(constraintWidgetGroup);
                if (!a((d) next2, (f) constraintWidgetGroup, list, z3)) {
                    constraintWidgetContainer.w0.clear();
                    constraintWidgetContainer.w0.add(0, new ConstraintWidgetGroup(constraintWidgetContainer.k0));
                    constraintWidgetContainer.D0 = false;
                    return;
                }
            }
        }
        int i2 = 0;
        int i3 = 0;
        for (ConstraintWidgetGroup next3 : list) {
            i2 = Math.max(i2, a(next3, 0));
            i3 = Math.max(i3, a(next3, 1));
        }
        if (z) {
            constraintWidgetContainer.a(ConstraintWidget.a.FIXED);
            constraintWidgetContainer.f(i2);
            constraintWidgetContainer.x0 = true;
            constraintWidgetContainer.y0 = true;
            constraintWidgetContainer.A0 = i2;
        }
        if (z2) {
            constraintWidgetContainer.b(ConstraintWidget.a.FIXED);
            constraintWidgetContainer.e(i3);
            constraintWidgetContainer.x0 = true;
            constraintWidgetContainer.z0 = true;
            constraintWidgetContainer.B0 = i3;
        }
        a(list, 0, constraintWidgetContainer.i());
        a(list, 1, constraintWidgetContainer.d());
    }

    public static int a(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i2, int i3) {
        if (!a(xmlPullParser, str)) {
            return i3;
        }
        return typedArray.getColor(i2, i3);
    }

    public static void a(Parcel parcel, int i2, Bundle bundle, boolean z) {
        if (bundle != null) {
            int j2 = j(parcel, i2);
            parcel.writeBundle(bundle);
            k(parcel, j2);
        } else if (z) {
            d(parcel, i2, 0);
        }
    }

    public static PathParser[] a(PathParser[] pathParserArr) {
        if (pathParserArr == null) {
            return null;
        }
        PathParser[] pathParserArr2 = new PathParser[pathParserArr.length];
        for (int i2 = 0; i2 < pathParserArr.length; i2++) {
            pathParserArr2[i2] = new PathParser(pathParserArr[i2]);
        }
        return pathParserArr2;
    }

    public static Drawable a(CompoundButton compoundButton) {
        if (Build.VERSION.SDK_INT >= 23) {
            return compoundButton.getButtonDrawable();
        }
        if (!f773n) {
            try {
                Field declaredField = CompoundButton.class.getDeclaredField("mButtonDrawable");
                f772m = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.i("CompoundButtonCompat", "Failed to retrieve mButtonDrawable field", e2);
            }
            f773n = true;
        }
        Field field = f772m;
        if (field != null) {
            try {
                return (Drawable) field.get(compoundButton);
            } catch (IllegalAccessException e3) {
                Log.i("CompoundButtonCompat", "Failed to get button drawable via reflection", e3);
                f772m = null;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
     arg types: [i.f.a.h.ConstraintWidget, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean */
    public static boolean a(d dVar, f fVar, List<f> list, boolean z) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        ConstraintWidget constraintWidget;
        ConstraintAnchor constraintAnchor4;
        ConstraintAnchor constraintAnchor5;
        ConstraintAnchor constraintAnchor6;
        ConstraintAnchor constraintAnchor7;
        ConstraintWidget constraintWidget2;
        ConstraintAnchor constraintAnchor8;
        if (dVar == null) {
            return true;
        }
        dVar.c0 = false;
        ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer) dVar.D;
        ConstraintWidgetGroup constraintWidgetGroup = dVar.f1118p;
        if (constraintWidgetGroup == null) {
            dVar.b0 = true;
            fVar.a.add(dVar);
            dVar.f1118p = fVar;
            if (dVar.f1121s.d == null && dVar.u.d == null && dVar.f1122t.d == null && dVar.v.d == null && dVar.w.d == null && dVar.z.d == null) {
                fVar.d = false;
                constraintWidgetContainer.D0 = false;
                dVar.b0 = false;
                if (z) {
                    return false;
                }
            }
            if (!(dVar.f1122t.d == null || dVar.v.d == null)) {
                ConstraintWidget.a h2 = constraintWidgetContainer.h();
                ConstraintWidget.a aVar = ConstraintWidget.a.WRAP_CONTENT;
                if (z) {
                    fVar.d = false;
                    constraintWidgetContainer.D0 = false;
                    dVar.b0 = false;
                    return false;
                }
                ConstraintWidget constraintWidget3 = dVar.f1122t.d.b;
                ConstraintWidget constraintWidget4 = dVar.D;
                if (!(constraintWidget3 == constraintWidget4 && dVar.v.d.b == constraintWidget4)) {
                    fVar.d = false;
                    constraintWidgetContainer.D0 = false;
                    dVar.b0 = false;
                }
            }
            if (!(dVar.f1121s.d == null || dVar.u.d == null)) {
                ConstraintWidget.a e2 = constraintWidgetContainer.e();
                ConstraintWidget.a aVar2 = ConstraintWidget.a.WRAP_CONTENT;
                if (z) {
                    fVar.d = false;
                    constraintWidgetContainer.D0 = false;
                    dVar.b0 = false;
                    return false;
                }
                ConstraintWidget constraintWidget5 = dVar.f1121s.d.b;
                ConstraintWidget constraintWidget6 = dVar.D;
                if (!(constraintWidget5 == constraintWidget6 && dVar.u.d.b == constraintWidget6)) {
                    fVar.d = false;
                    constraintWidgetContainer.D0 = false;
                    dVar.b0 = false;
                }
            }
            if (((dVar.e() == ConstraintWidget.a.MATCH_CONSTRAINT) ^ (dVar.h() == ConstraintWidget.a.MATCH_CONSTRAINT)) && dVar.G != 0.0f) {
                a((ConstraintWidget) dVar);
            } else if (dVar.e() == ConstraintWidget.a.MATCH_CONSTRAINT || dVar.h() == ConstraintWidget.a.MATCH_CONSTRAINT) {
                fVar.d = false;
                constraintWidgetContainer.D0 = false;
                dVar.b0 = false;
                if (z) {
                    return false;
                }
            }
            if (((dVar.f1121s.d == null && dVar.u.d == null) || (((constraintAnchor5 = dVar.f1121s.d) != null && constraintAnchor5.b == dVar.D && dVar.u.d == null) || (((constraintAnchor6 = dVar.u.d) != null && constraintAnchor6.b == dVar.D && dVar.f1121s.d == null) || ((constraintAnchor7 = dVar.f1121s.d) != null && constraintAnchor7.b == (constraintWidget2 = dVar.D) && (constraintAnchor8 = dVar.u.d) != null && constraintAnchor8.b == constraintWidget2)))) && dVar.z.d == null && !(dVar instanceof Guideline) && !(dVar instanceof Helper)) {
                fVar.f1124f.add(dVar);
            }
            if (((dVar.f1122t.d == null && dVar.v.d == null) || (((constraintAnchor = dVar.f1122t.d) != null && constraintAnchor.b == dVar.D && dVar.v.d == null) || (((constraintAnchor2 = dVar.v.d) != null && constraintAnchor2.b == dVar.D && dVar.f1122t.d == null) || ((constraintAnchor3 = dVar.f1122t.d) != null && constraintAnchor3.b == (constraintWidget = dVar.D) && (constraintAnchor4 = dVar.v.d) != null && constraintAnchor4.b == constraintWidget)))) && dVar.z.d == null && dVar.w.d == null && !(dVar instanceof Guideline) && !(dVar instanceof Helper)) {
                fVar.g.add(dVar);
            }
            if (dVar instanceof Helper) {
                fVar.d = false;
                constraintWidgetContainer.D0 = false;
                dVar.b0 = false;
                if (z) {
                    return false;
                }
                Helper helper = (Helper) dVar;
                for (int i2 = 0; i2 < helper.l0; i2++) {
                    if (!a((d) helper.k0[i2], fVar, list, z)) {
                        return false;
                    }
                }
            }
            for (ConstraintAnchor constraintAnchor9 : dVar.A) {
                ConstraintAnchor constraintAnchor10 = constraintAnchor9.d;
                if (!(constraintAnchor10 == null || constraintAnchor10.b == dVar.D)) {
                    if (constraintAnchor9.c == ConstraintAnchor.c.CENTER) {
                        fVar.d = false;
                        constraintWidgetContainer.D0 = false;
                        dVar.b0 = false;
                        if (z) {
                            return false;
                        }
                    } else {
                        ResolutionAnchor resolutionAnchor = constraintAnchor9.a;
                        if (!(constraintAnchor10 == null || constraintAnchor10.d == constraintAnchor9)) {
                            constraintAnchor10.a.a.add(resolutionAnchor);
                        }
                    }
                    if (!a((d) constraintAnchor9.d.b, fVar, list, z)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if (constraintWidgetGroup != fVar) {
            fVar.a.addAll(constraintWidgetGroup.a);
            fVar.f1124f.addAll(dVar.f1118p.f1124f);
            fVar.g.addAll(dVar.f1118p.g);
            if (!dVar.f1118p.d) {
                fVar.d = false;
            }
            list.remove(dVar.f1118p);
            Iterator<d> it = dVar.f1118p.a.iterator();
            while (it.hasNext()) {
                it.next().f1118p = fVar;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.b(android.content.Context, android.content.ComponentName):java.lang.String
     arg types: [android.app.Activity, android.content.ComponentName]
     candidates:
      i.b.k.ResourcesFlusher.b(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.b(android.os.Parcel, int):java.lang.String
      i.b.k.ResourcesFlusher.b(android.graphics.drawable.Drawable, int):void
      i.b.k.ResourcesFlusher.b(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.b(android.content.Context, android.content.ComponentName):java.lang.String */
    public static Intent a(Activity activity) {
        Intent parentActivityIntent = activity.getParentActivityIntent();
        if (parentActivityIntent != null) {
            return parentActivityIntent;
        }
        try {
            String b2 = b((Context) activity, activity.getComponentName());
            if (b2 == null) {
                return null;
            }
            ComponentName componentName = new ComponentName(activity, b2);
            try {
                if (b((Context) activity, componentName) == null) {
                    return Intent.makeMainActivity(componentName);
                }
                return new Intent().setComponent(componentName);
            } catch (PackageManager.NameNotFoundException unused) {
                Log.e("NavUtils", "getParentActivityIntent: bad parentActivityName '" + b2 + "' in manifest");
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    public static int a(Context context, String str) {
        int myPid = Process.myPid();
        int myUid = Process.myUid();
        String packageName = context.getPackageName();
        if (context.checkPermission(str, myPid, myUid) == -1) {
            return -1;
        }
        String permissionToOp = Build.VERSION.SDK_INT >= 23 ? AppOpsManager.permissionToOp(str) : null;
        if (permissionToOp != null) {
            if (packageName == null) {
                String[] packagesForUid = context.getPackageManager().getPackagesForUid(myUid);
                if (packagesForUid == null || packagesForUid.length <= 0) {
                    return -1;
                }
                packageName = packagesForUid[0];
            }
            if ((Build.VERSION.SDK_INT >= 23 ? ((AppOpsManager) context.getSystemService(AppOpsManager.class)).noteProxyOpNoThrow(permissionToOp, packageName) : 1) != 0) {
                return -2;
            }
        }
        return 0;
    }

    public static int a(List<ImageHeaderParser> list, InputStream inputStream, b bVar) {
        if (inputStream == null) {
            return -1;
        }
        if (!inputStream.markSupported()) {
            inputStream = new RecyclableBufferedInputStream(inputStream, bVar);
        }
        inputStream.mark(5242880);
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            try {
                int a2 = list.get(i2).a(inputStream, bVar);
                if (a2 != -1) {
                    return a2;
                }
                i2++;
            } finally {
                inputStream.reset();
            }
        }
        return -1;
    }

    public static int a(int i2) {
        if (i2 >= 0) {
            return i2;
        }
        throw new IllegalArgumentException();
    }

    public static ComplexColorCompat a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme, String str, int i2, int i3) {
        ComplexColorCompat complexColorCompat;
        if (a(xmlPullParser, str)) {
            TypedValue typedValue = new TypedValue();
            typedArray.getValue(i2, typedValue);
            int i4 = typedValue.type;
            if (i4 >= 28 && i4 <= 31) {
                return new ComplexColorCompat(null, null, typedValue.data);
            }
            try {
                complexColorCompat = ComplexColorCompat.a(typedArray.getResources(), typedArray.getResourceId(i2, 0), theme);
            } catch (Exception e2) {
                Log.e("ComplexColorCompat", "Failed to inflate ComplexColor.", e2);
                complexColorCompat = null;
            }
            if (complexColorCompat != null) {
                return complexColorCompat;
            }
        }
        return new ComplexColorCompat(null, null, i3);
    }

    public static boolean a(PathParser[] pathParserArr, PathParser[] pathParserArr2) {
        if (pathParserArr == null || pathParserArr2 == null || pathParserArr.length != pathParserArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < pathParserArr.length; i2++) {
            if (pathParserArr[i2].a != pathParserArr2[i2].a || pathParserArr[i2].b.length != pathParserArr2[i2].b.length) {
                return false;
            }
        }
        return true;
    }

    public static int a(File file) {
        FileChannel fileChannel = null;
        try {
            ByteBuffer allocate = ByteBuffer.allocate(4);
            fileChannel = new FileInputStream(file).getChannel();
            fileChannel.tryLock(60, 4, true);
            fileChannel.position(60L);
            if (fileChannel.read(allocate) == 4) {
                allocate.rewind();
                int i2 = allocate.getInt();
                fileChannel.close();
                return i2;
            }
            throw new IOException("Bad database header, unable to read 4 bytes at offset 60");
        } catch (Throwable th) {
            if (fileChannel != null) {
                fileChannel.close();
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0047 A[SYNTHETIC, Splitter:B:25:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0050 A[SYNTHETIC, Splitter:B:31:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r5, java.io.InputStream r6) {
        /*
            android.os.StrictMode$ThreadPolicy r0 = android.os.StrictMode.allowThreadDiskWrites()
            r1 = 0
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002a }
            r3.<init>(r5, r1)     // Catch:{ IOException -> 0x002a }
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r5]     // Catch:{ IOException -> 0x0025, all -> 0x0022 }
        L_0x000f:
            int r2 = r6.read(r5)     // Catch:{ IOException -> 0x0025, all -> 0x0022 }
            r4 = -1
            if (r2 == r4) goto L_0x001a
            r3.write(r5, r1, r2)     // Catch:{ IOException -> 0x0025, all -> 0x0022 }
            goto L_0x000f
        L_0x001a:
            r5 = 1
            r3.close()     // Catch:{ IOException -> 0x001e }
        L_0x001e:
            android.os.StrictMode.setThreadPolicy(r0)
            return r5
        L_0x0022:
            r5 = move-exception
            r2 = r3
            goto L_0x004e
        L_0x0025:
            r5 = move-exception
            r2 = r3
            goto L_0x002b
        L_0x0028:
            r5 = move-exception
            goto L_0x004e
        L_0x002a:
            r5 = move-exception
        L_0x002b:
            java.lang.String r6 = "TypefaceCompatUtil"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0028 }
            r3.<init>()     // Catch:{ all -> 0x0028 }
            java.lang.String r4 = "Error copying resource contents to temp file: "
            r3.append(r4)     // Catch:{ all -> 0x0028 }
            java.lang.String r5 = r5.getMessage()     // Catch:{ all -> 0x0028 }
            r3.append(r5)     // Catch:{ all -> 0x0028 }
            java.lang.String r5 = r3.toString()     // Catch:{ all -> 0x0028 }
            android.util.Log.e(r6, r5)     // Catch:{ all -> 0x0028 }
            if (r2 == 0) goto L_0x004a
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x004a:
            android.os.StrictMode.setThreadPolicy(r0)
            return r1
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0053 }
        L_0x0053:
            android.os.StrictMode.setThreadPolicy(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean");
    }

    public static void a(Drawable drawable, ColorStateList colorStateList) {
        drawable.setTintList(colorStateList);
    }

    public static void a(PopupWindow popupWindow, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            popupWindow.setWindowLayoutType(i2);
            return;
        }
        if (!f775p) {
            Class<PopupWindow> cls = PopupWindow.class;
            try {
                Method declaredMethod = cls.getDeclaredMethod("setWindowLayoutType", Integer.TYPE);
                f774o = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (Exception unused) {
            }
            f775p = true;
        }
        Method method = f774o;
        if (method != null) {
            try {
                method.invoke(popupWindow, Integer.valueOf(i2));
            } catch (Exception unused2) {
            }
        }
    }

    public static void a(Drawable drawable, PorterDuff.Mode mode) {
        drawable.setTintMode(mode);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0014 A[SYNTHETIC, Splitter:B:13:0x0014] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r0, android.content.res.Resources r1, int r2) {
        /*
            java.io.InputStream r1 = r1.openRawResource(r2)     // Catch:{ all -> 0x0010 }
            boolean r0 = a(r0, r1)     // Catch:{ all -> 0x000e }
            if (r1 == 0) goto L_0x000d
            r1.close()     // Catch:{ IOException -> 0x000d }
        L_0x000d:
            return r0
        L_0x000e:
            r0 = move-exception
            goto L_0x0012
        L_0x0010:
            r0 = move-exception
            r1 = 0
        L_0x0012:
            if (r1 == 0) goto L_0x0017
            r1.close()     // Catch:{ IOException -> 0x0017 }
        L_0x0017:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(java.io.File, android.content.res.Resources, int):boolean");
    }

    public static FontResourcesParserCompat a(XmlPullParser xmlPullParser, Resources resources) {
        int next;
        Resources resources2 = resources;
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            xmlPullParser.require(2, null, "font-family");
            if (xmlPullParser.getName().equals("font-family")) {
                TypedArray obtainAttributes = resources2.obtainAttributes(Xml.asAttributeSet(xmlPullParser), c.FontFamily);
                String string = obtainAttributes.getString(c.FontFamily_fontProviderAuthority);
                String string2 = obtainAttributes.getString(c.FontFamily_fontProviderPackage);
                String string3 = obtainAttributes.getString(c.FontFamily_fontProviderQuery);
                int resourceId = obtainAttributes.getResourceId(c.FontFamily_fontProviderCerts, 0);
                int integer = obtainAttributes.getInteger(c.FontFamily_fontProviderFetchStrategy, 1);
                int integer2 = obtainAttributes.getInteger(c.FontFamily_fontProviderFetchTimeout, 500);
                obtainAttributes.recycle();
                if (string == null || string2 == null || string3 == null) {
                    ArrayList arrayList = new ArrayList();
                    while (xmlPullParser.next() != 3) {
                        if (xmlPullParser.getEventType() == 2) {
                            if (xmlPullParser.getName().equals("font")) {
                                TypedArray obtainAttributes2 = resources2.obtainAttributes(Xml.asAttributeSet(xmlPullParser), c.FontFamilyFont);
                                int i2 = obtainAttributes2.getInt(obtainAttributes2.hasValue(c.FontFamilyFont_fontWeight) ? c.FontFamilyFont_fontWeight : c.FontFamilyFont_android_fontWeight, 400);
                                boolean z = 1 == obtainAttributes2.getInt(obtainAttributes2.hasValue(c.FontFamilyFont_fontStyle) ? c.FontFamilyFont_fontStyle : c.FontFamilyFont_android_fontStyle, 0);
                                int i3 = obtainAttributes2.hasValue(c.FontFamilyFont_ttcIndex) ? c.FontFamilyFont_ttcIndex : c.FontFamilyFont_android_ttcIndex;
                                String string4 = obtainAttributes2.getString(obtainAttributes2.hasValue(c.FontFamilyFont_fontVariationSettings) ? c.FontFamilyFont_fontVariationSettings : c.FontFamilyFont_android_fontVariationSettings);
                                int i4 = obtainAttributes2.getInt(i3, 0);
                                int i5 = obtainAttributes2.hasValue(c.FontFamilyFont_font) ? c.FontFamilyFont_font : c.FontFamilyFont_android_font;
                                int resourceId2 = obtainAttributes2.getResourceId(i5, 0);
                                String string5 = obtainAttributes2.getString(i5);
                                obtainAttributes2.recycle();
                                while (xmlPullParser.next() != 3) {
                                    a(xmlPullParser);
                                }
                                arrayList.add(new FontResourcesParserCompat0(string5, i2, z, string4, i4, resourceId2));
                            } else {
                                a(xmlPullParser);
                            }
                        }
                    }
                    if (arrayList.isEmpty()) {
                        return null;
                    }
                    return new FontResourcesParserCompat1((FontResourcesParserCompat0[]) arrayList.toArray(new FontResourcesParserCompat0[arrayList.size()]));
                }
                while (xmlPullParser.next() != 3) {
                    a(xmlPullParser);
                }
                return new FontResourcesParserCompat2(new FontRequest(string, string2, string3, a(resources2, resourceId)), integer, integer2);
            }
            a(xmlPullParser);
            return null;
        }
        throw new XmlPullParserException("No start tag found");
    }

    public static Intent a(Context context, ComponentName componentName) {
        String b2 = b(context, componentName);
        if (b2 == null) {
            return null;
        }
        ComponentName componentName2 = new ComponentName(componentName.getPackageName(), b2);
        if (b(context, componentName2) == null) {
            return Intent.makeMainActivity(componentName2);
        }
        return new Intent().setComponent(componentName2);
    }

    public static PropertyValuesHolder a(TypedArray typedArray, int i2, int i3, int i4, String str) {
        PropertyValuesHolder propertyValuesHolder;
        int i5;
        int i6;
        int i7;
        float f2;
        float f3;
        float f4;
        PropertyValuesHolder propertyValuesHolder2;
        TypedValue peekValue = typedArray.peekValue(i3);
        boolean z = peekValue != null;
        int i8 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i4);
        boolean z2 = peekValue2 != null;
        int i9 = z2 ? peekValue2.type : 0;
        if (i2 == 4) {
            i2 = ((!z || !b(i8)) && (!z2 || !b(i9))) ? 0 : 3;
        }
        boolean z3 = i2 == 0;
        PropertyValuesHolder propertyValuesHolder3 = null;
        if (i2 == 2) {
            String string = typedArray.getString(i3);
            String string2 = typedArray.getString(i4);
            PathParser[] d2 = d(string);
            PathParser[] d3 = d(string2);
            if (d2 == null && d3 == null) {
                return null;
            }
            if (d2 != null) {
                AnimatorInflaterCompat animatorInflaterCompat = new AnimatorInflaterCompat();
                if (d3 == null) {
                    propertyValuesHolder2 = PropertyValuesHolder.ofObject(str, animatorInflaterCompat, d2);
                } else if (a(d2, d3)) {
                    propertyValuesHolder2 = PropertyValuesHolder.ofObject(str, animatorInflaterCompat, d2, d3);
                } else {
                    throw new InflateException(" Can't morph from " + string + " to " + string2);
                }
                return propertyValuesHolder2;
            } else if (d3 == null) {
                return null;
            } else {
                return PropertyValuesHolder.ofObject(str, new AnimatorInflaterCompat(), d3);
            }
        } else {
            ArgbEvaluator argbEvaluator = i2 == 3 ? ArgbEvaluator.a : null;
            if (z3) {
                if (z) {
                    if (i8 == 5) {
                        f3 = typedArray.getDimension(i3, 0.0f);
                    } else {
                        f3 = typedArray.getFloat(i3, 0.0f);
                    }
                    if (z2) {
                        if (i9 == 5) {
                            f4 = typedArray.getDimension(i4, 0.0f);
                        } else {
                            f4 = typedArray.getFloat(i4, 0.0f);
                        }
                        propertyValuesHolder = PropertyValuesHolder.ofFloat(str, f3, f4);
                    } else {
                        propertyValuesHolder = PropertyValuesHolder.ofFloat(str, f3);
                    }
                } else {
                    if (i9 == 5) {
                        f2 = typedArray.getDimension(i4, 0.0f);
                    } else {
                        f2 = typedArray.getFloat(i4, 0.0f);
                    }
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, f2);
                }
            } else if (z) {
                if (i8 == 5) {
                    i6 = (int) typedArray.getDimension(i3, 0.0f);
                } else if (b(i8)) {
                    i6 = typedArray.getColor(i3, 0);
                } else {
                    i6 = typedArray.getInt(i3, 0);
                }
                if (z2) {
                    if (i9 == 5) {
                        i7 = (int) typedArray.getDimension(i4, 0.0f);
                    } else if (b(i9)) {
                        i7 = typedArray.getColor(i4, 0);
                    } else {
                        i7 = typedArray.getInt(i4, 0);
                    }
                    propertyValuesHolder = PropertyValuesHolder.ofInt(str, i6, i7);
                } else {
                    propertyValuesHolder = PropertyValuesHolder.ofInt(str, i6);
                }
            } else {
                if (z2) {
                    if (i9 == 5) {
                        i5 = (int) typedArray.getDimension(i4, 0.0f);
                    } else if (b(i9)) {
                        i5 = typedArray.getColor(i4, 0);
                    } else {
                        i5 = typedArray.getInt(i4, 0);
                    }
                    propertyValuesHolder = PropertyValuesHolder.ofInt(str, i5);
                }
                if (propertyValuesHolder3 == null && argbEvaluator != null) {
                    propertyValuesHolder3.setEvaluator(argbEvaluator);
                    return propertyValuesHolder3;
                }
            }
            propertyValuesHolder3 = propertyValuesHolder;
            return propertyValuesHolder3 == null ? propertyValuesHolder3 : propertyValuesHolder3;
        }
    }

    public static void a(Drawable drawable) {
        DrawableContainer.DrawableContainerState drawableContainerState;
        if (Build.VERSION.SDK_INT >= 23) {
            drawable.clearColorFilter();
            return;
        }
        drawable.clearColorFilter();
        if (drawable instanceof InsetDrawable) {
            a(((InsetDrawable) drawable).getDrawable());
        } else if (drawable instanceof WrappedDrawable) {
            a(((WrappedDrawable) drawable).a());
        } else if ((drawable instanceof DrawableContainer) && (drawableContainerState = (DrawableContainer.DrawableContainerState) ((DrawableContainer) drawable).getConstantState()) != null) {
            int childCount = drawableContainerState.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                Drawable child = drawableContainerState.getChild(i2);
                if (child != null) {
                    a(child);
                }
            }
        }
    }

    public static String a(TypedArray typedArray, XmlPullParser xmlPullParser, String str, int i2) {
        if (!a(xmlPullParser, str)) {
            return null;
        }
        return typedArray.getString(i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        if (r10.right <= r12.left) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003d, code lost:
        if (r10.top >= r12.bottom) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0044, code lost:
        if (r10.left >= r12.right) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0026, code lost:
        if (r10.bottom <= r12.top) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(int r9, android.graphics.Rect r10, android.graphics.Rect r11, android.graphics.Rect r12) {
        /*
            boolean r0 = a(r9, r10, r11)
            boolean r1 = a(r9, r10, r12)
            r2 = 0
            if (r1 != 0) goto L_0x007e
            if (r0 != 0) goto L_0x000f
            goto L_0x007e
        L_0x000f:
            java.lang.String r0 = "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."
            r1 = 130(0x82, float:1.82E-43)
            r3 = 33
            r4 = 66
            r5 = 17
            r6 = 1
            if (r9 == r5) goto L_0x0040
            if (r9 == r3) goto L_0x0039
            if (r9 == r4) goto L_0x0032
            if (r9 != r1) goto L_0x002c
            int r7 = r10.bottom
            int r8 = r12.top
            if (r7 > r8) goto L_0x002a
        L_0x0028:
            r7 = 1
            goto L_0x0047
        L_0x002a:
            r7 = 0
            goto L_0x0047
        L_0x002c:
            java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
            r9.<init>(r0)
            throw r9
        L_0x0032:
            int r7 = r10.right
            int r8 = r12.left
            if (r7 > r8) goto L_0x002a
            goto L_0x0028
        L_0x0039:
            int r7 = r10.top
            int r8 = r12.bottom
            if (r7 < r8) goto L_0x002a
            goto L_0x0028
        L_0x0040:
            int r7 = r10.left
            int r8 = r12.right
            if (r7 < r8) goto L_0x002a
            goto L_0x0028
        L_0x0047:
            if (r7 != 0) goto L_0x004a
            return r6
        L_0x004a:
            if (r9 == r5) goto L_0x007d
            if (r9 != r4) goto L_0x004f
            goto L_0x007d
        L_0x004f:
            int r11 = b(r9, r10, r11)
            if (r9 == r5) goto L_0x0070
            if (r9 == r3) goto L_0x006b
            if (r9 == r4) goto L_0x0066
            if (r9 != r1) goto L_0x0060
            int r9 = r12.bottom
            int r10 = r10.bottom
            goto L_0x0074
        L_0x0060:
            java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
            r9.<init>(r0)
            throw r9
        L_0x0066:
            int r9 = r12.right
            int r10 = r10.right
            goto L_0x0074
        L_0x006b:
            int r9 = r10.top
            int r10 = r12.top
            goto L_0x0074
        L_0x0070:
            int r9 = r10.left
            int r10 = r12.left
        L_0x0074:
            int r9 = r9 - r10
            int r9 = java.lang.Math.max(r6, r9)
            if (r11 >= r9) goto L_0x007c
            r2 = 1
        L_0x007c:
            return r2
        L_0x007d:
            return r6
        L_0x007e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean");
    }

    public static List<List<byte[]>> a(Resources resources, int i2) {
        if (i2 == 0) {
            return Collections.emptyList();
        }
        TypedArray obtainTypedArray = resources.obtainTypedArray(i2);
        try {
            if (obtainTypedArray.length() == 0) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            if (obtainTypedArray.getType(0) == 1) {
                for (int i3 = 0; i3 < obtainTypedArray.length(); i3++) {
                    int resourceId = obtainTypedArray.getResourceId(i3, 0);
                    if (resourceId != 0) {
                        arrayList.add(a(resources.getStringArray(resourceId)));
                    }
                }
            } else {
                arrayList.add(a(resources.getStringArray(i2)));
            }
            obtainTypedArray.recycle();
            return arrayList;
        } finally {
            obtainTypedArray.recycle();
        }
    }

    public static TypedArray a(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    public static boolean a(TypedArray typedArray, int i2, int i3, boolean z) {
        return typedArray.getBoolean(i2, typedArray.getBoolean(i3, z));
    }

    public static List<byte[]> a(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String decode : strArr) {
            arrayList.add(Base64.decode(decode, 0));
        }
        return arrayList;
    }

    public static int a(TypedArray typedArray, int i2, int i3, int i4) {
        return typedArray.getResourceId(i2, typedArray.getResourceId(i3, i4));
    }

    public static int a(ConstraintWidget constraintWidget, int i2, boolean z, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        ConstraintWidget constraintWidget2;
        int i12;
        ConstraintWidget constraintWidget3 = constraintWidget;
        int i13 = i2;
        boolean z2 = z;
        int i14 = 0;
        if (!constraintWidget3.b0) {
            return 0;
        }
        boolean z3 = constraintWidget3.w.d != null && i13 == 1;
        if (z2) {
            i7 = constraintWidget3.Q;
            i6 = constraintWidget.d() - constraintWidget3.Q;
            i5 = i13 * 2;
            i4 = i5 + 1;
        } else {
            int d2 = constraintWidget.d();
            i6 = constraintWidget3.Q;
            i7 = d2 - i6;
            i4 = i13 * 2;
            i5 = i4 + 1;
        }
        ConstraintAnchor[] constraintAnchorArr = constraintWidget3.A;
        if (constraintAnchorArr[i4].d == null || constraintAnchorArr[i5].d != null) {
            i8 = 1;
        } else {
            i8 = -1;
            int i15 = i4;
            i4 = i5;
            i5 = i15;
        }
        int i16 = z3 ? i3 - i7 : i3;
        int a2 = a(constraintWidget, i2) + (constraintWidget3.A[i5].a() * i8);
        int i17 = i16 + a2;
        int i18 = (i13 == 0 ? constraintWidget.i() : constraintWidget.d()) * i8;
        Iterator<l> it = constraintWidget3.A[i5].a.a.iterator();
        while (it.hasNext()) {
            i14 = Math.max(i14, a(((ResolutionAnchor) it.next()).c.b, i13, z2, i17));
        }
        int i19 = 0;
        for (Iterator<l> it2 = constraintWidget3.A[i4].a.a.iterator(); it2.hasNext(); it2 = it2) {
            i19 = Math.max(i19, a(((ResolutionAnchor) it2.next()).c.b, i13, z2, i18 + i17));
        }
        if (z3) {
            i14 -= i7;
            i9 = i19 + i6;
        } else {
            i9 = i19 + ((i13 == 0 ? constraintWidget.i() : constraintWidget.d()) * i8);
        }
        if (i13 == 1) {
            Iterator<l> it3 = constraintWidget3.w.a.a.iterator();
            int i20 = 0;
            while (it3.hasNext()) {
                Iterator<l> it4 = it3;
                ResolutionAnchor resolutionAnchor = (ResolutionAnchor) it3.next();
                int i21 = i4;
                if (i8 == 1) {
                    i12 = Math.max(i20, a(resolutionAnchor.c.b, i13, z2, i7 + i17));
                } else {
                    i12 = Math.max(i20, a(resolutionAnchor.c.b, i13, z2, (i6 * i8) + i17));
                }
                i20 = i12;
                it3 = it4;
                i4 = i21;
            }
            i10 = i4;
            i11 = (constraintWidget3.w.a.a.size() <= 0 || z3) ? i20 : i8 == 1 ? i20 + i7 : i20 - i6;
        } else {
            i10 = i4;
            i11 = 0;
        }
        int max = Math.max(i14, Math.max(i9, i11)) + a2;
        int i22 = i18 + i17;
        if (i8 == -1) {
            int i23 = i22;
            i22 = i17;
            i17 = i23;
        }
        if (z2) {
            Optimizer.a(constraintWidget3, i13, i17);
            constraintWidget3.a(i17, i22, i13);
        } else {
            constraintWidget3.f1118p.a(constraintWidget3, i13);
            if (i13 == 0) {
                constraintWidget3.K = i17;
            } else if (i13 == 1) {
                constraintWidget3.L = i17;
            }
        }
        if (constraintWidget.b(i2) == ConstraintWidget.a.MATCH_CONSTRAINT && constraintWidget3.G != 0.0f) {
            constraintWidget3.f1118p.a(constraintWidget3, i13);
        }
        ConstraintAnchor[] constraintAnchorArr2 = constraintWidget3.A;
        if (constraintAnchorArr2[i5].d != null && constraintAnchorArr2[i10].d != null && constraintAnchorArr2[i5].d.b == (constraintWidget2 = constraintWidget3.D) && constraintAnchorArr2[i10].d.b == constraintWidget2) {
            constraintWidget3.f1118p.a(constraintWidget3, i13);
        }
        return max;
    }

    public static boolean a(Rect rect, Rect rect2, int i2) {
        if (i2 == 17) {
            int i3 = rect.right;
            int i4 = rect2.right;
            if ((i3 > i4 || rect.left >= i4) && rect.left > rect2.left) {
                return true;
            }
            return false;
        } else if (i2 == 33) {
            int i5 = rect.bottom;
            int i6 = rect2.bottom;
            if ((i5 > i6 || rect.top >= i6) && rect.top > rect2.top) {
                return true;
            }
            return false;
        } else if (i2 == 66) {
            int i7 = rect.left;
            int i8 = rect2.left;
            if ((i7 < i8 || rect.right <= i8) && rect.right < rect2.right) {
                return true;
            }
            return false;
        } else if (i2 == 130) {
            int i9 = rect.top;
            int i10 = rect2.top;
            return (i9 < i10 || rect.bottom <= i10) && rect.bottom < rect2.bottom;
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
    }

    public static String a(TypedArray typedArray, int i2, int i3) {
        String string = typedArray.getString(i2);
        return string == null ? typedArray.getString(i3) : string;
    }

    public static <T extends Parcelable> void a(Parcel parcel, int i2, T[] tArr, int i3, boolean z) {
        if (tArr != null) {
            int j2 = j(parcel, i2);
            parcel.writeInt(r7);
            for (T t2 : tArr) {
                if (t2 == null) {
                    parcel.writeInt(0);
                } else {
                    a(parcel, t2, i3);
                }
            }
            k(parcel, j2);
        } else if (z) {
            d(parcel, i2, 0);
        }
    }

    public static void a(XmlPullParser xmlPullParser) {
        int i2 = 1;
        while (i2 > 0) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i2++;
            } else if (next == 3) {
                i2--;
            }
        }
    }

    public static boolean a(int i2, Rect rect, Rect rect2) {
        if (i2 != 17) {
            if (i2 != 33) {
                if (i2 != 66) {
                    if (i2 != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                }
            }
            if (rect2.right < rect.left || rect2.left > rect.right) {
                return false;
            }
            return true;
        }
        if (rect2.bottom < rect.top || rect2.top > rect.bottom) {
            return false;
        }
        return true;
    }

    public static <T extends Parcelable> void a(Parcel parcel, Parcelable parcelable, int i2) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        parcelable.writeToParcel(parcel, i2);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }

    public static int a(Context context, int i2, int i3) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i2, typedValue, true);
        return typedValue.resourceId != 0 ? i2 : i3;
    }

    public static boolean a(Drawable drawable, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            return drawable.setLayoutDirection(i2);
        }
        if (!f769j) {
            Class<Drawable> cls = Drawable.class;
            try {
                Method declaredMethod = cls.getDeclaredMethod("setLayoutDirection", Integer.TYPE);
                f768i = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("DrawableCompat", "Failed to retrieve setLayoutDirection(int) method", e2);
            }
            f769j = true;
        }
        Method method = f768i;
        if (method != null) {
            try {
                method.invoke(drawable, Integer.valueOf(i2));
                return true;
            } catch (Exception e3) {
                Log.i("DrawableCompat", "Failed to invoke setLayoutDirection(int) via reflection", e3);
                f768i = null;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
     arg types: [android.content.res.XmlResourceParser, android.content.res.Resources]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b9 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Typeface a(android.content.Context r16, int r17, android.util.TypedValue r18, int r19, i.h.e.b.ResourcesCompat r20, android.os.Handler r21, boolean r22) {
        /*
            r9 = r17
            r0 = r18
            r5 = r19
            r10 = r20
            r11 = r21
            android.content.res.Resources r3 = r16.getResources()
            r1 = 1
            r3.getValue(r9, r0, r1)
            java.lang.String r12 = "ResourcesCompat"
            java.lang.CharSequence r1 = r0.string
            if (r1 == 0) goto L_0x00d9
            java.lang.String r13 = r1.toString()
            java.lang.String r0 = "res/"
            boolean r0 = r13.startsWith(r0)
            r14 = 0
            r15 = -3
            if (r0 != 0) goto L_0x002d
            if (r10 == 0) goto L_0x00b7
            r10.a(r15, r11)
            goto L_0x00b7
        L_0x002d:
            i.e.LruCache<java.lang.String, android.graphics.Typeface> r0 = i.h.f.TypefaceCompat.b
            java.lang.String r1 = i.h.f.TypefaceCompat.a(r3, r9, r5)
            java.lang.Object r0 = r0.a(r1)
            android.graphics.Typeface r0 = (android.graphics.Typeface) r0
            if (r0 == 0) goto L_0x0043
            if (r10 == 0) goto L_0x0040
            r10.a(r0, r11)
        L_0x0040:
            r14 = r0
            goto L_0x00b7
        L_0x0043:
            java.lang.String r0 = r13.toLowerCase()     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            java.lang.String r1 = ".xml"
            boolean r0 = r0.endsWith(r1)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            if (r0 == 0) goto L_0x0075
            android.content.res.XmlResourceParser r0 = r3.getXml(r9)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            i.h.e.b.FontResourcesParserCompat r2 = a(r0, r3)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            if (r2 != 0) goto L_0x0064
            java.lang.String r0 = "Failed to find font-family tag"
            android.util.Log.e(r12, r0)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            if (r10 == 0) goto L_0x00b7
            r10.a(r15, r11)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            goto L_0x00b7
        L_0x0064:
            r1 = r16
            r4 = r17
            r5 = r19
            r6 = r20
            r7 = r21
            r8 = r22
            android.graphics.Typeface r14 = i.h.f.TypefaceCompat.a(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            goto L_0x00b7
        L_0x0075:
            r0 = r16
            android.graphics.Typeface r0 = i.h.f.TypefaceCompat.a(r0, r3, r9, r13, r5)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            if (r10 == 0) goto L_0x0040
            if (r0 == 0) goto L_0x0083
            r10.a(r0, r11)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            goto L_0x0040
        L_0x0083:
            r10.a(r15, r11)     // Catch:{ XmlPullParserException -> 0x009d, IOException -> 0x0087 }
            goto L_0x0040
        L_0x0087:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to read xml resource "
            r1.append(r2)
            r1.append(r13)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r12, r1, r0)
            goto L_0x00b2
        L_0x009d:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to parse xml resource "
            r1.append(r2)
            r1.append(r13)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r12, r1, r0)
        L_0x00b2:
            if (r10 == 0) goto L_0x00b7
            r10.a(r15, r11)
        L_0x00b7:
            if (r14 != 0) goto L_0x00d8
            if (r10 == 0) goto L_0x00bc
            goto L_0x00d8
        L_0x00bc:
            android.content.res.Resources$NotFoundException r0 = new android.content.res.Resources$NotFoundException
            java.lang.String r1 = "Font resource ID #0x"
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            java.lang.String r2 = java.lang.Integer.toHexString(r17)
            r1.append(r2)
            java.lang.String r2 = " could not be retrieved."
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00d8:
            return r14
        L_0x00d9:
            android.content.res.Resources$NotFoundException r1 = new android.content.res.Resources$NotFoundException
            java.lang.String r2 = "Resource \""
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.String r3 = r3.getResourceName(r9)
            r2.append(r3)
            java.lang.String r3 = "\" ("
            r2.append(r3)
            java.lang.String r3 = java.lang.Integer.toHexString(r17)
            r2.append(r3)
            java.lang.String r3 = ") is not a Font: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(android.content.Context, int, android.util.TypedValue, int, i.h.e.b.ResourcesCompat, android.os.Handler, boolean):android.graphics.Typeface");
    }

    public static void a(List<f> list, int i2, int i3) {
        int i4;
        int size = list.size();
        int i5 = 0;
        while (i5 < size) {
            ConstraintWidgetGroup constraintWidgetGroup = list.get(i5);
            HashSet<d> hashSet = null;
            if (constraintWidgetGroup != null) {
                if (i2 == 0) {
                    hashSet = constraintWidgetGroup.h;
                } else if (i2 == 1) {
                    hashSet = constraintWidgetGroup.f1125i;
                }
                for (ConstraintWidget next : hashSet) {
                    if (next.b0) {
                        int i6 = i2 * 2;
                        ConstraintAnchor[] constraintAnchorArr = next.A;
                        ConstraintAnchor constraintAnchor = constraintAnchorArr[i6];
                        ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i6 + 1];
                        if ((constraintAnchor.d == null || constraintAnchor2.d == null) ? false : true) {
                            Optimizer.a(next, i2, constraintAnchor.a() + a(next, i2));
                        } else if (next.G == 0.0f || next.b(i2) != ConstraintWidget.a.MATCH_CONSTRAINT) {
                            if (i2 == 0) {
                                i4 = next.K;
                            } else {
                                i4 = i2 == 1 ? next.L : 0;
                            }
                            int i7 = i3 - i4;
                            int c2 = i7 - next.c(i2);
                            next.a(c2, i7, i2);
                            Optimizer.a(next, i2, c2);
                        } else {
                            int a2 = a(next);
                            int i8 = (int) next.A[i6].a.g;
                            ResolutionAnchor resolutionAnchor = constraintAnchor2.a;
                            resolutionAnchor.f1129f = constraintAnchor.a;
                            resolutionAnchor.g = (float) a2;
                            resolutionAnchor.b = 1;
                            next.a(i8, i8 + a2, i2);
                        }
                    }
                }
                i5++;
            } else {
                throw null;
            }
        }
    }

    public static int a(ConstraintWidget constraintWidget, int i2) {
        ConstraintWidget constraintWidget2;
        ConstraintAnchor constraintAnchor;
        int i3 = i2 * 2;
        ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
        ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i3];
        ConstraintAnchor constraintAnchor3 = constraintAnchorArr[i3 + 1];
        ConstraintAnchor constraintAnchor4 = constraintAnchor2.d;
        if (constraintAnchor4 == null || constraintAnchor4.b != (constraintWidget2 = constraintWidget.D) || (constraintAnchor = constraintAnchor3.d) == null || constraintAnchor.b != constraintWidget2) {
            return 0;
        }
        return (int) (((float) (((constraintWidget2.c(i2) - constraintAnchor2.a()) - constraintAnchor3.a()) - constraintWidget.c(i2))) * (i2 == 0 ? constraintWidget.V : constraintWidget.W));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
     arg types: [android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int, java.lang.String):android.animation.PropertyValuesHolder
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0306  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.animation.Animator a(android.content.Context r22, android.content.res.Resources r23, android.content.res.Resources.Theme r24, org.xmlpull.v1.XmlPullParser r25, android.util.AttributeSet r26, android.animation.AnimatorSet r27, int r28, float r29) {
        /*
            r8 = r23
            r9 = r24
            r10 = r25
            r11 = r27
            int r12 = r25.getDepth()
            r0 = 0
            r1 = 0
            r13 = r1
        L_0x000f:
            int r1 = r25.next()
            r2 = 3
            r14 = 0
            if (r1 != r2) goto L_0x001d
            int r3 = r25.getDepth()
            if (r3 <= r12) goto L_0x032d
        L_0x001d:
            r3 = 1
            if (r1 == r3) goto L_0x032d
            r4 = 2
            if (r1 == r4) goto L_0x0024
            goto L_0x000f
        L_0x0024:
            java.lang.String r1 = r25.getName()
            java.lang.String r5 = "objectAnimator"
            boolean r5 = r1.equals(r5)
            if (r5 == 0) goto L_0x0046
            android.animation.ObjectAnimator r7 = new android.animation.ObjectAnimator
            r7.<init>()
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r26
            r4 = r7
            r5 = r29
            r6 = r25
            a(r0, r1, r2, r3, r4, r5, r6)
            goto L_0x005f
        L_0x0046:
            java.lang.String r5 = "animator"
            boolean r5 = r1.equals(r5)
            if (r5 == 0) goto L_0x0064
            r4 = 0
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r26
            r5 = r29
            r6 = r25
            android.animation.ValueAnimator r7 = a(r0, r1, r2, r3, r4, r5, r6)
        L_0x005f:
            r0 = r7
            r19 = r12
            goto L_0x0300
        L_0x0064:
            java.lang.String r5 = "set"
            boolean r5 = r1.equals(r5)
            if (r5 == 0) goto L_0x009b
            android.animation.AnimatorSet r15 = new android.animation.AnimatorSet
            r15.<init>()
            int[] r0 = i.x.a.a.AndroidResources.h
            r7 = r26
            android.content.res.TypedArray r6 = a(r8, r9, r7, r0)
            java.lang.String r0 = "ordering"
            int r16 = b(r6, r10, r0, r14, r14)
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r25
            r4 = r26
            r5 = r15
            r17 = r6
            r6 = r16
            r7 = r29
            a(r0, r1, r2, r3, r4, r5, r6, r7)
            r17.recycle()
            r19 = r12
            r0 = r15
            goto L_0x0300
        L_0x009b:
            java.lang.String r5 = "propertyValuesHolder"
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x0316
            android.util.AttributeSet r1 = android.util.Xml.asAttributeSet(r25)
            r6 = 0
        L_0x00a8:
            int r7 = r25.getEventType()
            if (r7 == r2) goto L_0x02da
            if (r7 == r3) goto L_0x02da
            if (r7 == r4) goto L_0x00b6
            r25.next()
            goto L_0x00a8
        L_0x00b6:
            java.lang.String r3 = r25.getName()
            boolean r3 = r3.equals(r5)
            if (r3 == 0) goto L_0x02c3
            int[] r3 = i.x.a.a.AndroidResources.f1476i
            android.content.res.TypedArray r3 = a(r8, r9, r1, r3)
            java.lang.String r7 = "propertyName"
            java.lang.String r7 = a(r3, r10, r7, r2)
            r14 = 4
            java.lang.String r15 = "valueType"
            int r4 = b(r3, r10, r15, r4, r14)
            r14 = 0
            r16 = r1
            r15 = r4
        L_0x00d7:
            int r1 = r25.next()
            if (r1 == r2) goto L_0x01ca
            r17 = r2
            r2 = 1
            if (r1 == r2) goto L_0x01ca
            java.lang.String r1 = r25.getName()
            java.lang.String r2 = "keyframe"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x01c0
            java.lang.String r1 = "value"
            r2 = 4
            if (r15 != r2) goto L_0x0121
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r25)
            int[] r15 = i.x.a.a.AndroidResources.f1477j
            android.content.res.TypedArray r2 = a(r8, r9, r2, r15)
            boolean r15 = a(r10, r1)
            if (r15 != 0) goto L_0x0105
            r15 = 0
            goto L_0x010a
        L_0x0105:
            r15 = 0
            android.util.TypedValue r15 = r2.peekValue(r15)
        L_0x010a:
            if (r15 == 0) goto L_0x010f
            r17 = 1
            goto L_0x0111
        L_0x010f:
            r17 = 0
        L_0x0111:
            if (r17 == 0) goto L_0x011d
            int r15 = r15.type
            boolean r15 = b(r15)
            if (r15 == 0) goto L_0x011d
            r15 = 3
            goto L_0x011e
        L_0x011d:
            r15 = 0
        L_0x011e:
            r2.recycle()
        L_0x0121:
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r25)
            r18 = r5
            int[] r5 = i.x.a.a.AndroidResources.f1477j
            android.content.res.TypedArray r2 = a(r8, r9, r2, r5)
            r5 = -1082130432(0xffffffffbf800000, float:-1.0)
            java.lang.String r8 = "fraction"
            r9 = 3
            float r5 = a(r2, r10, r8, r9, r5)
            boolean r8 = a(r10, r1)
            if (r8 != 0) goto L_0x013e
            r8 = 0
            goto L_0x0143
        L_0x013e:
            r8 = 0
            android.util.TypedValue r8 = r2.peekValue(r8)
        L_0x0143:
            if (r8 == 0) goto L_0x0147
            r9 = 1
            goto L_0x0148
        L_0x0147:
            r9 = 0
        L_0x0148:
            r19 = r12
            r12 = 4
            if (r15 != r12) goto L_0x015b
            if (r9 == 0) goto L_0x0159
            int r8 = r8.type
            boolean r8 = b(r8)
            if (r8 == 0) goto L_0x0159
            r8 = 3
            goto L_0x015c
        L_0x0159:
            r8 = 0
            goto L_0x015c
        L_0x015b:
            r8 = r15
        L_0x015c:
            if (r9 == 0) goto L_0x017d
            if (r8 == 0) goto L_0x0172
            r9 = 1
            if (r8 == r9) goto L_0x0168
            r9 = 3
            if (r8 == r9) goto L_0x0168
            r1 = 0
            goto L_0x0188
        L_0x0168:
            r8 = 0
            int r1 = b(r2, r10, r1, r8, r8)
            android.animation.Keyframe r1 = android.animation.Keyframe.ofInt(r5, r1)
            goto L_0x0188
        L_0x0172:
            r8 = 0
            r9 = 0
            float r1 = a(r2, r10, r1, r8, r9)
            android.animation.Keyframe r1 = android.animation.Keyframe.ofFloat(r5, r1)
            goto L_0x0188
        L_0x017d:
            if (r8 != 0) goto L_0x0184
            android.animation.Keyframe r1 = android.animation.Keyframe.ofFloat(r5)
            goto L_0x0188
        L_0x0184:
            android.animation.Keyframe r1 = android.animation.Keyframe.ofInt(r5)
        L_0x0188:
            java.lang.String r5 = "interpolator"
            boolean r5 = a(r10, r5)
            if (r5 != 0) goto L_0x0192
            r5 = 0
            goto L_0x0198
        L_0x0192:
            r5 = 0
            r8 = 1
            int r5 = r2.getResourceId(r8, r5)
        L_0x0198:
            r8 = r22
            if (r5 <= 0) goto L_0x01a3
            android.view.animation.Interpolator r5 = android.view.animation.AnimationUtils.loadInterpolator(r8, r5)
            r1.setInterpolator(r5)
        L_0x01a3:
            r2.recycle()
            if (r1 == 0) goto L_0x01b2
            if (r14 != 0) goto L_0x01af
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
        L_0x01af:
            r14.add(r1)
        L_0x01b2:
            r25.next()
            r2 = 3
            r8 = r23
            r9 = r24
            r5 = r18
            r12 = r19
            goto L_0x00d7
        L_0x01c0:
            r8 = r22
            r8 = r23
            r9 = r24
            r2 = r17
            goto L_0x00d7
        L_0x01ca:
            r8 = r22
            r18 = r5
            r19 = r12
            if (r14 == 0) goto L_0x02a8
            int r1 = r14.size()
            if (r1 <= 0) goto L_0x02a8
            r2 = 0
            java.lang.Object r2 = r14.get(r2)
            android.animation.Keyframe r2 = (android.animation.Keyframe) r2
            int r5 = r1 + -1
            java.lang.Object r5 = r14.get(r5)
            android.animation.Keyframe r5 = (android.animation.Keyframe) r5
            float r9 = r5.getFraction()
            r12 = 1065353216(0x3f800000, float:1.0)
            int r17 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            if (r17 >= 0) goto L_0x0208
            r17 = 0
            int r9 = (r9 > r17 ? 1 : (r9 == r17 ? 0 : -1))
            if (r9 >= 0) goto L_0x01fb
            r5.setFraction(r12)
            goto L_0x0208
        L_0x01fb:
            int r9 = r14.size()
            android.animation.Keyframe r5 = a(r5, r12)
            r14.add(r9, r5)
            int r1 = r1 + 1
        L_0x0208:
            float r5 = r2.getFraction()
            r9 = 0
            int r12 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r12 == 0) goto L_0x0223
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 >= 0) goto L_0x0219
            r2.setFraction(r9)
            goto L_0x0223
        L_0x0219:
            android.animation.Keyframe r2 = a(r2, r9)
            r5 = 0
            r14.add(r5, r2)
            int r1 = r1 + 1
        L_0x0223:
            android.animation.Keyframe[] r2 = new android.animation.Keyframe[r1]
            r14.toArray(r2)
            r5 = 0
        L_0x0229:
            if (r5 >= r1) goto L_0x029b
            r9 = r2[r5]
            float r12 = r9.getFraction()
            r14 = 0
            int r12 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r12 >= 0) goto L_0x0292
            if (r5 != 0) goto L_0x023c
            r9.setFraction(r14)
            goto L_0x0292
        L_0x023c:
            int r12 = r1 + -1
            if (r5 != r12) goto L_0x0246
            r12 = 1065353216(0x3f800000, float:1.0)
            r9.setFraction(r12)
            goto L_0x0292
        L_0x0246:
            int r9 = r5 + 1
            r14 = r5
        L_0x0249:
            if (r9 >= r12) goto L_0x0260
            r17 = r2[r9]
            float r17 = r17.getFraction()
            r20 = 0
            int r17 = (r17 > r20 ? 1 : (r17 == r20 ? 0 : -1))
            if (r17 < 0) goto L_0x0258
            goto L_0x0260
        L_0x0258:
            int r14 = r9 + 1
            r21 = r14
            r14 = r9
            r9 = r21
            goto L_0x0249
        L_0x0260:
            int r9 = r14 + 1
            r9 = r2[r9]
            float r9 = r9.getFraction()
            int r12 = r5 + -1
            r12 = r2[r12]
            float r12 = r12.getFraction()
            float r9 = r9 - r12
            int r12 = r14 - r5
            int r12 = r12 + 2
            float r12 = (float) r12
            float r9 = r9 / r12
            r12 = r5
        L_0x0278:
            if (r12 > r14) goto L_0x0292
            r17 = r1
            r1 = r2[r12]
            int r20 = r12 + -1
            r20 = r2[r20]
            float r20 = r20.getFraction()
            float r8 = r20 + r9
            r1.setFraction(r8)
            int r12 = r12 + 1
            r8 = r22
            r1 = r17
            goto L_0x0278
        L_0x0292:
            r17 = r1
            int r5 = r5 + 1
            r8 = r22
            r1 = r17
            goto L_0x0229
        L_0x029b:
            android.animation.PropertyValuesHolder r1 = android.animation.PropertyValuesHolder.ofKeyframe(r7, r2)
            r2 = 3
            if (r15 != r2) goto L_0x02aa
            i.x.a.a.ArgbEvaluator r5 = i.x.a.a.ArgbEvaluator.a
            r1.setEvaluator(r5)
            goto L_0x02aa
        L_0x02a8:
            r2 = 3
            r1 = 0
        L_0x02aa:
            r5 = 0
            r8 = 1
            if (r1 != 0) goto L_0x02b2
            android.animation.PropertyValuesHolder r1 = a(r3, r4, r5, r8, r7)
        L_0x02b2:
            if (r1 == 0) goto L_0x02bf
            if (r6 != 0) goto L_0x02bc
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r6 = r4
        L_0x02bc:
            r6.add(r1)
        L_0x02bf:
            r3.recycle()
            goto L_0x02c9
        L_0x02c3:
            r16 = r1
            r18 = r5
            r19 = r12
        L_0x02c9:
            r25.next()
            r3 = 1
            r4 = 2
            r8 = r23
            r9 = r24
            r1 = r16
            r5 = r18
            r12 = r19
            goto L_0x00a8
        L_0x02da:
            r19 = r12
            if (r6 == 0) goto L_0x02f2
            int r1 = r6.size()
            android.animation.PropertyValuesHolder[] r2 = new android.animation.PropertyValuesHolder[r1]
            r3 = 0
        L_0x02e5:
            if (r3 >= r1) goto L_0x02f3
            java.lang.Object r4 = r6.get(r3)
            android.animation.PropertyValuesHolder r4 = (android.animation.PropertyValuesHolder) r4
            r2[r3] = r4
            int r3 = r3 + 1
            goto L_0x02e5
        L_0x02f2:
            r2 = 0
        L_0x02f3:
            if (r2 == 0) goto L_0x02ff
            boolean r1 = r0 instanceof android.animation.ValueAnimator
            if (r1 == 0) goto L_0x02ff
            r1 = r0
            android.animation.ValueAnimator r1 = (android.animation.ValueAnimator) r1
            r1.setValues(r2)
        L_0x02ff:
            r14 = 1
        L_0x0300:
            if (r11 == 0) goto L_0x030e
            if (r14 != 0) goto L_0x030e
            if (r13 != 0) goto L_0x030b
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
        L_0x030b:
            r13.add(r0)
        L_0x030e:
            r8 = r23
            r9 = r24
            r12 = r19
            goto L_0x000f
        L_0x0316:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Unknown animator name: "
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            java.lang.String r2 = r25.getName()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x032d:
            if (r11 == 0) goto L_0x0357
            if (r13 == 0) goto L_0x0357
            int r1 = r13.size()
            android.animation.Animator[] r1 = new android.animation.Animator[r1]
            java.util.Iterator r2 = r13.iterator()
            r3 = 0
        L_0x033c:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x034e
            java.lang.Object r4 = r2.next()
            android.animation.Animator r4 = (android.animation.Animator) r4
            int r5 = r3 + 1
            r1[r3] = r4
            r3 = r5
            goto L_0x033c
        L_0x034e:
            if (r28 != 0) goto L_0x0354
            r11.playTogether(r1)
            goto L_0x0357
        L_0x0354:
            r11.playSequentially(r1)
        L_0x0357:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.ResourcesFlusher.a(android.content.Context, android.content.res.Resources, android.content.res.Resources$Theme, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.animation.AnimatorSet, int, float):android.animation.Animator");
    }

    public static ActionMode.Callback a(TextView textView, ActionMode.Callback callback) {
        int i2 = Build.VERSION.SDK_INT;
        return (i2 < 26 || i2 > 27 || (callback instanceof TextViewCompat)) ? callback : new TextViewCompat(callback, textView);
    }

    public static int a(ConstraintWidget constraintWidget) {
        float f2;
        float f3;
        if (constraintWidget.e() == ConstraintWidget.a.MATCH_CONSTRAINT) {
            if (constraintWidget.H == 0) {
                f3 = ((float) constraintWidget.d()) * constraintWidget.G;
            } else {
                f3 = ((float) constraintWidget.d()) / constraintWidget.G;
            }
            int i2 = (int) f3;
            constraintWidget.f(i2);
            return i2;
        } else if (constraintWidget.h() != ConstraintWidget.a.MATCH_CONSTRAINT) {
            return -1;
        } else {
            if (constraintWidget.H == 1) {
                f2 = ((float) constraintWidget.i()) * constraintWidget.G;
            } else {
                f2 = ((float) constraintWidget.i()) / constraintWidget.G;
            }
            int i3 = (int) f2;
            constraintWidget.e(i3);
            return i3;
        }
    }

    public static void a(TextView textView, int i2) {
        int i3;
        a(i2);
        if (Build.VERSION.SDK_INT >= 28) {
            textView.setFirstBaselineToTopHeight(i2);
            return;
        }
        Paint.FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        if (textView.getIncludeFontPadding()) {
            i3 = fontMetricsInt.top;
        } else {
            i3 = fontMetricsInt.ascent;
        }
        if (i2 > Math.abs(i3)) {
            textView.setPadding(textView.getPaddingLeft(), i2 + i3, textView.getPaddingRight(), textView.getPaddingBottom());
        }
    }

    public static Keyframe a(Keyframe keyframe, float f2) {
        if (keyframe.getType() == Float.TYPE) {
            return Keyframe.ofFloat(f2);
        }
        if (keyframe.getType() == Integer.TYPE) {
            return Keyframe.ofInt(f2);
        }
        return Keyframe.ofObject(f2);
    }

    public static PrecomputedTextCompat.a a(TextView textView) {
        int i2;
        int i3;
        TextDirectionHeuristic textDirectionHeuristic;
        if (Build.VERSION.SDK_INT >= 28) {
            return new PrecomputedTextCompat.a(textView.getTextMetricsParams());
        }
        TextPaint textPaint = new TextPaint(textView.getPaint());
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 23) {
            i3 = 1;
            i2 = 1;
        } else {
            i3 = 0;
            i2 = 0;
        }
        TextDirectionHeuristic textDirectionHeuristic2 = TextDirectionHeuristics.FIRSTSTRONG_LTR;
        if (Build.VERSION.SDK_INT >= 23) {
            i3 = textView.getBreakStrategy();
            i2 = textView.getHyphenationFrequency();
        }
        if (textView.getTransformationMethod() instanceof PasswordTransformationMethod) {
            textDirectionHeuristic = TextDirectionHeuristics.LTR;
        } else if (Build.VERSION.SDK_INT < 28 || (textView.getInputType() & 15) != 3) {
            if (textView.getLayoutDirection() == 1) {
                z = true;
            }
            switch (textView.getTextDirection()) {
                case 2:
                    textDirectionHeuristic = TextDirectionHeuristics.ANYRTL_LTR;
                    break;
                case 3:
                    textDirectionHeuristic = TextDirectionHeuristics.LTR;
                    break;
                case 4:
                    textDirectionHeuristic = TextDirectionHeuristics.RTL;
                    break;
                case 5:
                    textDirectionHeuristic = TextDirectionHeuristics.LOCALE;
                    break;
                case 6:
                    textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                    break;
                case 7:
                    textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL;
                    break;
                default:
                    if (!z) {
                        textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                        break;
                    } else {
                        textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL;
                        break;
                    }
            }
        } else {
            byte directionality = Character.getDirectionality(DecimalFormatSymbols.getInstance(textView.getTextLocale()).getDigitStrings()[0].codePointAt(0));
            if (directionality == 1 || directionality == 2) {
                textDirectionHeuristic = TextDirectionHeuristics.RTL;
            } else {
                textDirectionHeuristic = TextDirectionHeuristics.LTR;
            }
        }
        return new PrecomputedTextCompat.a(textPaint, textDirectionHeuristic, i3, i2);
    }

    public static void a(TextView textView, PrecomputedTextCompat precomputedTextCompat) {
        if (Build.VERSION.SDK_INT < 29) {
            PrecomputedTextCompat.a a2 = a(textView);
            if (precomputedTextCompat == null) {
                throw null;
            } else if (a2.a(null)) {
                textView.setText(precomputedTextCompat);
            } else {
                throw new IllegalArgumentException("Given text can not be applied to TextView.");
            }
        } else if (precomputedTextCompat != null) {
            textView.setText((CharSequence) null);
        } else {
            throw null;
        }
    }

    public static ValueAnimator a(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, ValueAnimator valueAnimator, float f2, XmlPullParser xmlPullParser) {
        TypedArray typedArray;
        ValueAnimator valueAnimator2;
        TypedArray typedArray2;
        ValueAnimator valueAnimator3;
        PropertyValuesHolder propertyValuesHolder;
        Resources resources2 = resources;
        Resources.Theme theme2 = theme;
        AttributeSet attributeSet2 = attributeSet;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        TypedArray a2 = a(resources2, theme2, attributeSet2, AndroidResources.g);
        TypedArray a3 = a(resources2, theme2, attributeSet2, AndroidResources.f1478k);
        ValueAnimator valueAnimator4 = valueAnimator == null ? new ValueAnimator() : valueAnimator;
        long b2 = (long) b(a2, xmlPullParser2, "duration", 1, 300);
        int i2 = 0;
        long b3 = (long) b(a2, xmlPullParser2, "startOffset", 2, 0);
        int b4 = b(a2, xmlPullParser2, "valueType", 7, 4);
        if (a(xmlPullParser2, "valueFrom") && a(xmlPullParser2, "valueTo")) {
            if (b4 == 4) {
                TypedValue peekValue = a2.peekValue(5);
                boolean z = peekValue != null;
                int i3 = z ? peekValue.type : 0;
                TypedValue peekValue2 = a2.peekValue(6);
                boolean z2 = peekValue2 != null;
                b4 = ((!z || !b(i3)) && (!z2 || !b(z2 ? peekValue2.type : 0))) ? 0 : 3;
            }
            PropertyValuesHolder a4 = a(a2, b4, 5, 6, "");
            if (a4 != null) {
                valueAnimator4.setValues(a4);
            }
        }
        valueAnimator4.setDuration(b2);
        valueAnimator4.setStartDelay(b3);
        valueAnimator4.setRepeatCount(b(a2, xmlPullParser2, "repeatCount", 3, 0));
        valueAnimator4.setRepeatMode(b(a2, xmlPullParser2, "repeatMode", 4, 1));
        if (a3 != null) {
            ObjectAnimator objectAnimator = (ObjectAnimator) valueAnimator4;
            String a5 = a(a3, xmlPullParser2, "pathData", 1);
            if (a5 != null) {
                String a6 = a(a3, xmlPullParser2, "propertyXName", 2);
                String a7 = a(a3, xmlPullParser2, "propertyYName", 3);
                if (a6 == null && a7 == null) {
                    throw new InflateException(a3.getPositionDescription() + " propertyXName or propertyYName is needed for PathData");
                }
                Path e2 = e(a5);
                float f3 = 0.5f * f2;
                PathMeasure pathMeasure = new PathMeasure(e2, false);
                ArrayList arrayList = new ArrayList();
                arrayList.add(Float.valueOf(0.0f));
                float f4 = 0.0f;
                while (true) {
                    f4 += pathMeasure.getLength();
                    arrayList.add(Float.valueOf(f4));
                    if (!pathMeasure.nextContour()) {
                        break;
                    }
                }
                PathMeasure pathMeasure2 = new PathMeasure(e2, false);
                int min = Math.min(100, ((int) (f4 / f3)) + 1);
                float[] fArr = new float[min];
                float[] fArr2 = new float[min];
                float[] fArr3 = new float[2];
                float f5 = f4 / ((float) (min - 1));
                valueAnimator2 = valueAnimator4;
                typedArray = a2;
                int i4 = 0;
                float f6 = 0.0f;
                while (true) {
                    propertyValuesHolder = null;
                    if (i2 >= min) {
                        break;
                    }
                    int i5 = min;
                    pathMeasure2.getPosTan(f6 - ((Float) arrayList.get(i4)).floatValue(), fArr3, null);
                    fArr[i2] = fArr3[0];
                    fArr2[i2] = fArr3[1];
                    f6 += f5;
                    int i6 = i4 + 1;
                    if (i6 < arrayList.size() && f6 > ((Float) arrayList.get(i6)).floatValue()) {
                        pathMeasure2.nextContour();
                        i4 = i6;
                    }
                    i2++;
                    min = i5;
                }
                PropertyValuesHolder ofFloat = a6 != null ? PropertyValuesHolder.ofFloat(a6, fArr) : null;
                if (a7 != null) {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(a7, fArr2);
                }
                if (ofFloat == null) {
                    i2 = 0;
                    objectAnimator.setValues(propertyValuesHolder);
                } else {
                    i2 = 0;
                    if (propertyValuesHolder == null) {
                        objectAnimator.setValues(ofFloat);
                    } else {
                        objectAnimator.setValues(ofFloat, propertyValuesHolder);
                    }
                }
            } else {
                valueAnimator2 = valueAnimator4;
                typedArray = a2;
                objectAnimator.setPropertyName(a(a3, xmlPullParser2, "propertyName", 0));
            }
        } else {
            valueAnimator2 = valueAnimator4;
            typedArray = a2;
        }
        if (!a(xmlPullParser2, "interpolator")) {
            typedArray2 = typedArray;
        } else {
            typedArray2 = typedArray;
            i2 = typedArray2.getResourceId(i2, i2);
        }
        if (i2 > 0) {
            valueAnimator3 = valueAnimator2;
            valueAnimator3.setInterpolator(AnimationUtils.loadInterpolator(context, i2));
        } else {
            valueAnimator3 = valueAnimator2;
        }
        typedArray2.recycle();
        if (a3 != null) {
            a3.recycle();
        }
        return valueAnimator3;
    }
}
