package i.b.k;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatViewInflater;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import com.crashlytics.android.core.CodedOutputStream;
import i.b.k.TwilightManager;
import i.b.l.a.AppCompatResources;
import i.b.p.ActionMode;
import i.b.p.ContextThemeWrapper;
import i.b.p.WindowCallbackWrapper;
import i.b.p.i.ListMenuPresenter;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuPresenter;
import i.b.q.AppCompatDrawableManager;
import i.b.q.DecorContentParent;
import i.b.q.TintTypedArray;
import i.b.q.ViewUtils;
import i.e.ArrayMap;
import i.h.l.ViewCompat;
import i.h.l.ViewPropertyAnimatorCompat;
import i.h.l.ViewPropertyAnimatorListener;
import i.h.l.ViewPropertyAnimatorListenerAdapter;
import j.a.a.a.outline;
import java.lang.Thread;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class AppCompatDelegateImpl extends AppCompatDelegate implements MenuBuilder.a, LayoutInflater.Factory2 {
    public static final Map<Class<?>, Integer> a0 = new ArrayMap();
    public static final boolean b0 = false;
    public static final int[] c0 = {16842836};
    public static boolean d0 = true;
    public static final boolean e0;
    public boolean A;
    public boolean B;
    public boolean C;
    public boolean D;
    public boolean E;
    public boolean F;
    public j[] G;
    public j H;
    public boolean I;
    public boolean J;
    public boolean K;
    public boolean L;
    public boolean M;
    public int N = -100;
    public int O;
    public boolean P;
    public boolean Q;
    public g R;
    public g S;
    public boolean T;
    public int U;
    public final Runnable V = new b();
    public boolean W;
    public Rect X;
    public Rect Y;
    public AppCompatViewInflater Z;
    public final Object d;

    /* renamed from: e  reason: collision with root package name */
    public final Context f738e;

    /* renamed from: f  reason: collision with root package name */
    public Window f739f;
    public e g;
    public final AppCompatCallback h;

    /* renamed from: i  reason: collision with root package name */
    public ActionBar f740i;

    /* renamed from: j  reason: collision with root package name */
    public MenuInflater f741j;

    /* renamed from: k  reason: collision with root package name */
    public CharSequence f742k;

    /* renamed from: l  reason: collision with root package name */
    public DecorContentParent f743l;

    /* renamed from: m  reason: collision with root package name */
    public c f744m;

    /* renamed from: n  reason: collision with root package name */
    public k f745n;

    /* renamed from: o  reason: collision with root package name */
    public ActionMode f746o;

    /* renamed from: p  reason: collision with root package name */
    public ActionBarContextView f747p;

    /* renamed from: q  reason: collision with root package name */
    public PopupWindow f748q;

    /* renamed from: r  reason: collision with root package name */
    public Runnable f749r;

    /* renamed from: s  reason: collision with root package name */
    public ViewPropertyAnimatorCompat f750s = null;

    /* renamed from: t  reason: collision with root package name */
    public boolean f751t = true;
    public boolean u;
    public ViewGroup v;
    public TextView w;
    public View x;
    public boolean y;
    public boolean z;

    public static class a implements Thread.UncaughtExceptionHandler {
        public final /* synthetic */ Thread.UncaughtExceptionHandler a;

        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.a = uncaughtExceptionHandler;
        }

        public void uncaughtException(Thread thread, Throwable th) {
            String message;
            boolean z = false;
            if ((th instanceof Resources.NotFoundException) && (message = th.getMessage()) != null && (message.contains("drawable") || message.contains("Drawable"))) {
                z = true;
            }
            if (z) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.a.uncaughtException(thread, notFoundException);
                return;
            }
            this.a.uncaughtException(thread, th);
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if ((appCompatDelegateImpl.U & 1) != 0) {
                appCompatDelegateImpl.c(0);
            }
            AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
            if ((appCompatDelegateImpl2.U & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0) {
                appCompatDelegateImpl2.c(108);
            }
            AppCompatDelegateImpl appCompatDelegateImpl3 = AppCompatDelegateImpl.this;
            appCompatDelegateImpl3.T = false;
            appCompatDelegateImpl3.U = 0;
        }
    }

    public class d implements ActionMode.a {
        public ActionMode.a a;

        public class a extends ViewPropertyAnimatorListenerAdapter {
            public a() {
            }

            public void a(View view) {
                AppCompatDelegateImpl.this.f747p.setVisibility(8);
                AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
                PopupWindow popupWindow = appCompatDelegateImpl.f748q;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (appCompatDelegateImpl.f747p.getParent() instanceof View) {
                    ViewCompat.B((View) AppCompatDelegateImpl.this.f747p.getParent());
                }
                AppCompatDelegateImpl.this.f747p.removeAllViews();
                AppCompatDelegateImpl.this.f750s.a((ViewPropertyAnimatorListener) null);
                AppCompatDelegateImpl.this.f750s = null;
            }
        }

        public d(ActionMode.a aVar) {
            this.a = aVar;
        }

        public boolean a(ActionMode actionMode, Menu menu) {
            return this.a.a(actionMode, menu);
        }

        public boolean b(ActionMode actionMode, Menu menu) {
            return this.a.b(actionMode, menu);
        }

        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.a.a(actionMode, menuItem);
        }

        public void a(ActionMode actionMode) {
            this.a.a(actionMode);
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl.f748q != null) {
                appCompatDelegateImpl.f739f.getDecorView().removeCallbacks(AppCompatDelegateImpl.this.f749r);
            }
            AppCompatDelegateImpl appCompatDelegateImpl2 = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl2.f747p != null) {
                appCompatDelegateImpl2.e();
                AppCompatDelegateImpl appCompatDelegateImpl3 = AppCompatDelegateImpl.this;
                ViewPropertyAnimatorCompat a2 = ViewCompat.a(appCompatDelegateImpl3.f747p);
                a2.a(0.0f);
                appCompatDelegateImpl3.f750s = a2;
                ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = AppCompatDelegateImpl.this.f750s;
                a aVar = new a();
                View view = viewPropertyAnimatorCompat.a.get();
                if (view != null) {
                    viewPropertyAnimatorCompat.a(view, aVar);
                }
            }
            AppCompatDelegateImpl appCompatDelegateImpl4 = AppCompatDelegateImpl.this;
            AppCompatCallback appCompatCallback = appCompatDelegateImpl4.h;
            if (appCompatCallback != null) {
                appCompatCallback.b(appCompatDelegateImpl4.f746o);
            }
            AppCompatDelegateImpl.this.f746o = null;
        }
    }

    public class f extends g {
        public final PowerManager c;

        public f(Context context) {
            super();
            this.c = (PowerManager) context.getSystemService("power");
        }

        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }

        public int c() {
            return this.c.isPowerSaveMode() ? 2 : 1;
        }

        public void d() {
            AppCompatDelegateImpl.this.d();
        }
    }

    public abstract class g {
        public BroadcastReceiver a;

        public class a extends BroadcastReceiver {
            public a() {
            }

            public void onReceive(Context context, Intent intent) {
                g.this.d();
            }
        }

        public g() {
        }

        public void a() {
            BroadcastReceiver broadcastReceiver = this.a;
            if (broadcastReceiver != null) {
                try {
                    AppCompatDelegateImpl.this.f738e.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
                this.a = null;
            }
        }

        public abstract IntentFilter b();

        public abstract int c();

        public abstract void d();

        public void e() {
            a();
            IntentFilter b2 = b();
            if (b2 != null && b2.countActions() != 0) {
                if (this.a == null) {
                    this.a = new a();
                }
                AppCompatDelegateImpl.this.f738e.registerReceiver(this.a, b2);
            }
        }
    }

    public class h extends g {
        public final TwilightManager c;

        public h(TwilightManager twilightManager) {
            super();
            this.c = twilightManager;
        }

        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        public int c() {
            boolean z;
            long j2;
            TwilightManager twilightManager = this.c;
            TwilightManager.a aVar = twilightManager.c;
            boolean z2 = false;
            if (aVar.b > System.currentTimeMillis()) {
                z = aVar.a;
            } else {
                Location location = null;
                Location a = ResourcesFlusher.a(twilightManager.a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? twilightManager.a("network") : null;
                if (ResourcesFlusher.a(twilightManager.a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
                    location = twilightManager.a("gps");
                }
                if (location == null || a == null ? location != null : location.getTime() > a.getTime()) {
                    a = location;
                }
                if (a != null) {
                    TwilightManager.a aVar2 = twilightManager.c;
                    long currentTimeMillis = System.currentTimeMillis();
                    if (TwilightCalculator.d == null) {
                        TwilightCalculator.d = new TwilightCalculator();
                    }
                    TwilightCalculator twilightCalculator = TwilightCalculator.d;
                    TwilightCalculator twilightCalculator2 = twilightCalculator;
                    twilightCalculator2.a(currentTimeMillis - 86400000, a.getLatitude(), a.getLongitude());
                    twilightCalculator2.a(currentTimeMillis, a.getLatitude(), a.getLongitude());
                    if (twilightCalculator.c == 1) {
                        z2 = true;
                    }
                    long j3 = twilightCalculator.b;
                    long j4 = twilightCalculator.a;
                    long j5 = j3;
                    twilightCalculator.a(currentTimeMillis + 86400000, a.getLatitude(), a.getLongitude());
                    long j6 = twilightCalculator.b;
                    if (j5 == -1 || j4 == -1) {
                        j2 = 43200000 + currentTimeMillis;
                    } else {
                        j2 = (currentTimeMillis > j4 ? j6 + 0 : currentTimeMillis > j5 ? j4 + 0 : j5 + 0) + 60000;
                    }
                    aVar2.a = z2;
                    aVar2.b = j2;
                    z = aVar.a;
                } else {
                    Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
                    int i2 = Calendar.getInstance().get(11);
                    if (i2 < 6 || i2 >= 22) {
                        z2 = true;
                    }
                    z = z2;
                }
            }
            if (z) {
                return 2;
            }
            return 1;
        }

        public void d() {
            AppCompatDelegateImpl.this.d();
        }
    }

    public class i extends ContentFrameLayout {
        public i(Context context) {
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
         arg types: [i.b.k.AppCompatDelegateImpl$j, int]
         candidates:
          i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
          i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
          i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
          i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
          i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
          i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() == 0) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if (x < -5 || y < -5 || x > getWidth() + 5 || y > getHeight() + 5) {
                    AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
                    appCompatDelegateImpl.a(appCompatDelegateImpl.d(0), true);
                    return true;
                }
            }
            return super.onInterceptTouchEvent(motionEvent);
        }

        public void setBackgroundResource(int i2) {
            setBackgroundDrawable(AppCompatResources.c(getContext(), i2));
        }
    }

    public static final class j {
        public int a;
        public int b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f753e;

        /* renamed from: f  reason: collision with root package name */
        public int f754f;
        public ViewGroup g;
        public View h;

        /* renamed from: i  reason: collision with root package name */
        public View f755i;

        /* renamed from: j  reason: collision with root package name */
        public MenuBuilder f756j;

        /* renamed from: k  reason: collision with root package name */
        public ListMenuPresenter f757k;

        /* renamed from: l  reason: collision with root package name */
        public Context f758l;

        /* renamed from: m  reason: collision with root package name */
        public boolean f759m;

        /* renamed from: n  reason: collision with root package name */
        public boolean f760n;

        /* renamed from: o  reason: collision with root package name */
        public boolean f761o;

        /* renamed from: p  reason: collision with root package name */
        public boolean f762p;

        /* renamed from: q  reason: collision with root package name */
        public boolean f763q = false;

        /* renamed from: r  reason: collision with root package name */
        public boolean f764r;

        /* renamed from: s  reason: collision with root package name */
        public Bundle f765s;

        public j(int i2) {
            this.a = i2;
        }

        public void a(MenuBuilder menuBuilder) {
            ListMenuPresenter listMenuPresenter;
            MenuBuilder menuBuilder2 = this.f756j;
            if (menuBuilder != menuBuilder2) {
                if (menuBuilder2 != null) {
                    menuBuilder2.a(this.f757k);
                }
                this.f756j = menuBuilder;
                if (menuBuilder != null && (listMenuPresenter = this.f757k) != null) {
                    menuBuilder.a(listMenuPresenter, menuBuilder.a);
                }
            }
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        boolean z2 = false;
        if (i2 <= 25) {
            z2 = true;
        }
        e0 = z2;
        if (b0 && !d0) {
            Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }

    public AppCompatDelegateImpl(Context context, Window window, AppCompatCallback appCompatCallback, Object obj) {
        Integer num;
        AppCompatActivity appCompatActivity = null;
        this.f738e = context;
        this.h = appCompatCallback;
        this.d = obj;
        if (this.N == -100 && (obj instanceof Dialog)) {
            while (true) {
                if (context != null) {
                    if (!(context instanceof AppCompatActivity)) {
                        if (!(context instanceof ContextWrapper)) {
                            break;
                        }
                        context = ((ContextWrapper) context).getBaseContext();
                    } else {
                        appCompatActivity = (AppCompatActivity) context;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (appCompatActivity != null) {
                this.N = ((AppCompatDelegateImpl) appCompatActivity.m()).N;
            }
        }
        if (this.N == -100 && (num = a0.get(this.d.getClass())) != null) {
            this.N = num.intValue();
            a0.remove(this.d.getClass());
        }
        if (window != null) {
            a(window);
        }
        AppCompatDrawableManager.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.b(android.content.Context, android.content.ComponentName):java.lang.String
     arg types: [android.app.Activity, android.content.ComponentName]
     candidates:
      i.b.k.ResourcesFlusher.b(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.b(android.os.Parcel, int):java.lang.String
      i.b.k.ResourcesFlusher.b(android.graphics.drawable.Drawable, int):void
      i.b.k.ResourcesFlusher.b(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.b(android.content.Context, android.content.ComponentName):java.lang.String */
    public void a(Bundle bundle) {
        this.J = true;
        a(false);
        g();
        Object obj = this.d;
        if (obj instanceof Activity) {
            String str = null;
            try {
                Activity activity = (Activity) obj;
                str = ResourcesFlusher.b((Context) activity, activity.getComponentName());
            } catch (PackageManager.NameNotFoundException e2) {
                throw new IllegalArgumentException(e2);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                ActionBar actionBar = this.f740i;
                if (actionBar == null) {
                    this.W = true;
                } else {
                    actionBar.b(true);
                }
            }
        }
        this.K = true;
    }

    public void b() {
        j();
        ActionBar actionBar = this.f740i;
        if (actionBar == null || !actionBar.e()) {
            e(0);
        }
    }

    public void c() {
        this.L = false;
        AppCompatDelegate.b(super);
        j();
        ActionBar actionBar = this.f740i;
        if (actionBar != null) {
            actionBar.d(false);
        }
        if (this.d instanceof Dialog) {
            g gVar = this.R;
            if (gVar != null) {
                gVar.a();
            }
            g gVar2 = this.S;
            if (gVar2 != null) {
                gVar2.a();
            }
        }
    }

    public j d(int i2) {
        j[] jVarArr = this.G;
        if (jVarArr == null || jVarArr.length <= i2) {
            j[] jVarArr2 = new j[(i2 + 1)];
            if (jVarArr != null) {
                System.arraycopy(jVarArr, 0, jVarArr2, 0, jVarArr.length);
            }
            this.G = jVarArr2;
            jVarArr = jVarArr2;
        }
        j jVar = jVarArr[i2];
        if (jVar != null) {
            return jVar;
        }
        j jVar2 = new j(i2);
        jVarArr[i2] = jVar2;
        return jVar2;
    }

    public void e() {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.f750s;
        if (viewPropertyAnimatorCompat != null) {
            viewPropertyAnimatorCompat.a();
        }
    }

    public final void f() {
        ViewGroup viewGroup;
        CharSequence charSequence;
        Context context;
        if (!this.u) {
            TypedArray obtainStyledAttributes = this.f738e.obtainStyledAttributes(i.b.j.AppCompatTheme);
            if (obtainStyledAttributes.hasValue(i.b.j.AppCompatTheme_windowActionBar)) {
                if (obtainStyledAttributes.getBoolean(i.b.j.AppCompatTheme_windowNoTitle, false)) {
                    a(1);
                } else if (obtainStyledAttributes.getBoolean(i.b.j.AppCompatTheme_windowActionBar, false)) {
                    a(108);
                }
                if (obtainStyledAttributes.getBoolean(i.b.j.AppCompatTheme_windowActionBarOverlay, false)) {
                    a(109);
                }
                if (obtainStyledAttributes.getBoolean(i.b.j.AppCompatTheme_windowActionModeOverlay, false)) {
                    a(10);
                }
                this.D = obtainStyledAttributes.getBoolean(i.b.j.AppCompatTheme_android_windowIsFloating, false);
                obtainStyledAttributes.recycle();
                g();
                this.f739f.getDecorView();
                LayoutInflater from = LayoutInflater.from(this.f738e);
                if (this.E) {
                    if (this.C) {
                        viewGroup = (ViewGroup) from.inflate(i.b.g.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                    } else {
                        viewGroup = (ViewGroup) from.inflate(i.b.g.abc_screen_simple, (ViewGroup) null);
                    }
                    ViewCompat.a(viewGroup, new AppCompatDelegateImpl1(this));
                } else if (this.D) {
                    viewGroup = (ViewGroup) from.inflate(i.b.g.abc_dialog_title_material, (ViewGroup) null);
                    this.B = false;
                    this.A = false;
                } else if (this.A) {
                    TypedValue typedValue = new TypedValue();
                    this.f738e.getTheme().resolveAttribute(i.b.a.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        context = new ContextThemeWrapper(this.f738e, typedValue.resourceId);
                    } else {
                        context = this.f738e;
                    }
                    viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(i.b.g.abc_screen_toolbar, (ViewGroup) null);
                    DecorContentParent decorContentParent = (DecorContentParent) viewGroup.findViewById(i.b.f.decor_content_parent);
                    this.f743l = decorContentParent;
                    decorContentParent.setWindowCallback(i());
                    if (this.B) {
                        this.f743l.a(109);
                    }
                    if (this.y) {
                        this.f743l.a(2);
                    }
                    if (this.z) {
                        this.f743l.a(5);
                    }
                } else {
                    viewGroup = null;
                }
                if (viewGroup != null) {
                    if (this.f743l == null) {
                        this.w = (TextView) viewGroup.findViewById(i.b.f.title);
                    }
                    ViewUtils.b(viewGroup);
                    ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(i.b.f.action_bar_activity_content);
                    ViewGroup viewGroup2 = (ViewGroup) this.f739f.findViewById(16908290);
                    if (viewGroup2 != null) {
                        while (viewGroup2.getChildCount() > 0) {
                            View childAt = viewGroup2.getChildAt(0);
                            viewGroup2.removeViewAt(0);
                            contentFrameLayout.addView(childAt);
                        }
                        viewGroup2.setId(-1);
                        contentFrameLayout.setId(16908290);
                        if (viewGroup2 instanceof FrameLayout) {
                            ((FrameLayout) viewGroup2).setForeground(null);
                        }
                    }
                    this.f739f.setContentView(viewGroup);
                    contentFrameLayout.setAttachListener(new AppCompatDelegateImpl3(this));
                    this.v = viewGroup;
                    Object obj = this.d;
                    if (obj instanceof Activity) {
                        charSequence = ((Activity) obj).getTitle();
                    } else {
                        charSequence = this.f742k;
                    }
                    if (!TextUtils.isEmpty(charSequence)) {
                        DecorContentParent decorContentParent2 = this.f743l;
                        if (decorContentParent2 != null) {
                            decorContentParent2.setWindowTitle(charSequence);
                        } else {
                            ActionBar actionBar = this.f740i;
                            if (actionBar != null) {
                                actionBar.a(charSequence);
                            } else {
                                TextView textView = this.w;
                                if (textView != null) {
                                    textView.setText(charSequence);
                                }
                            }
                        }
                    }
                    ContentFrameLayout contentFrameLayout2 = (ContentFrameLayout) this.v.findViewById(16908290);
                    View decorView = this.f739f.getDecorView();
                    contentFrameLayout2.h.set(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
                    if (ViewCompat.w(contentFrameLayout2)) {
                        contentFrameLayout2.requestLayout();
                    }
                    TypedArray obtainStyledAttributes2 = this.f738e.obtainStyledAttributes(i.b.j.AppCompatTheme);
                    obtainStyledAttributes2.getValue(i.b.j.AppCompatTheme_windowMinWidthMajor, contentFrameLayout2.getMinWidthMajor());
                    obtainStyledAttributes2.getValue(i.b.j.AppCompatTheme_windowMinWidthMinor, contentFrameLayout2.getMinWidthMinor());
                    if (obtainStyledAttributes2.hasValue(i.b.j.AppCompatTheme_windowFixedWidthMajor)) {
                        obtainStyledAttributes2.getValue(i.b.j.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout2.getFixedWidthMajor());
                    }
                    if (obtainStyledAttributes2.hasValue(i.b.j.AppCompatTheme_windowFixedWidthMinor)) {
                        obtainStyledAttributes2.getValue(i.b.j.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout2.getFixedWidthMinor());
                    }
                    if (obtainStyledAttributes2.hasValue(i.b.j.AppCompatTheme_windowFixedHeightMajor)) {
                        obtainStyledAttributes2.getValue(i.b.j.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout2.getFixedHeightMajor());
                    }
                    if (obtainStyledAttributes2.hasValue(i.b.j.AppCompatTheme_windowFixedHeightMinor)) {
                        obtainStyledAttributes2.getValue(i.b.j.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout2.getFixedHeightMinor());
                    }
                    obtainStyledAttributes2.recycle();
                    contentFrameLayout2.requestLayout();
                    this.u = true;
                    j d2 = d(0);
                    if (this.M) {
                        return;
                    }
                    if (d2 == null || d2.f756j == null) {
                        e(108);
                        return;
                    }
                    return;
                }
                StringBuilder a2 = outline.a("AppCompat does not support the current theme features: { windowActionBar: ");
                a2.append(this.A);
                a2.append(", windowActionBarOverlay: ");
                a2.append(this.B);
                a2.append(", android:windowIsFloating: ");
                a2.append(this.D);
                a2.append(", windowActionModeOverlay: ");
                a2.append(this.C);
                a2.append(", windowNoTitle: ");
                throw new IllegalArgumentException(outline.a(a2, this.E, " }"));
            }
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
    }

    public final void g() {
        if (this.f739f == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                a(((Activity) obj).getWindow());
            }
        }
        if (this.f739f == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    public final g h() {
        if (this.R == null) {
            Context context = this.f738e;
            if (TwilightManager.d == null) {
                Context applicationContext = context.getApplicationContext();
                TwilightManager.d = new TwilightManager(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
            }
            this.R = new h(TwilightManager.d);
        }
        return this.R;
    }

    public final Window.Callback i() {
        return this.f739f.getCallback();
    }

    public final void j() {
        f();
        if (this.A && this.f740i == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                this.f740i = new WindowDecorActionBar((Activity) this.d, this.B);
            } else if (obj instanceof Dialog) {
                this.f740i = new WindowDecorActionBar((Dialog) this.d);
            }
            ActionBar actionBar = this.f740i;
            if (actionBar != null) {
                actionBar.b(this.W);
            }
        }
    }

    public final boolean k() {
        ViewGroup viewGroup;
        return this.u && (viewGroup = this.v) != null && ViewCompat.w(viewGroup);
    }

    public final void l() {
        if (this.u) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    /* JADX WARN: Type inference failed for: r5v1, types: [java.lang.Throwable, java.lang.String] */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r9v5, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r10v6, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r10v7, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r5v3 */
    /* JADX WARN: Type inference failed for: r9v8, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r9v9, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r5v4 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0072, code lost:
        if (((org.xmlpull.v1.XmlPullParser) r12).getDepth() > 1) goto L_0x0083;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0291  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a4  */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View onCreateView(android.view.View r9, java.lang.String r10, android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r8 = this;
            androidx.appcompat.app.AppCompatViewInflater r0 = r8.Z
            r1 = 0
            if (r0 != 0) goto L_0x0062
            android.content.Context r0 = r8.f738e
            int[] r2 = i.b.j.AppCompatTheme
            android.content.res.TypedArray r0 = r0.obtainStyledAttributes(r2)
            int r2 = i.b.j.AppCompatTheme_viewInflaterClass
            java.lang.String r0 = r0.getString(r2)
            if (r0 == 0) goto L_0x005b
            java.lang.Class<androidx.appcompat.app.AppCompatViewInflater> r2 = androidx.appcompat.app.AppCompatViewInflater.class
            java.lang.String r2 = r2.getName()
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0022
            goto L_0x005b
        L_0x0022:
            java.lang.Class r2 = java.lang.Class.forName(r0)     // Catch:{ all -> 0x0037 }
            java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch:{ all -> 0x0037 }
            java.lang.reflect.Constructor r2 = r2.getDeclaredConstructor(r3)     // Catch:{ all -> 0x0037 }
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x0037 }
            java.lang.Object r2 = r2.newInstance(r3)     // Catch:{ all -> 0x0037 }
            androidx.appcompat.app.AppCompatViewInflater r2 = (androidx.appcompat.app.AppCompatViewInflater) r2     // Catch:{ all -> 0x0037 }
            r8.Z = r2     // Catch:{ all -> 0x0037 }
            goto L_0x0062
        L_0x0037:
            r2 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to instantiate custom view inflater "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = ". Falling back to default."
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            java.lang.String r3 = "AppCompatDelegate"
            android.util.Log.i(r3, r0, r2)
            androidx.appcompat.app.AppCompatViewInflater r0 = new androidx.appcompat.app.AppCompatViewInflater
            r0.<init>()
            r8.Z = r0
            goto L_0x0062
        L_0x005b:
            androidx.appcompat.app.AppCompatViewInflater r0 = new androidx.appcompat.app.AppCompatViewInflater
            r0.<init>()
            r8.Z = r0
        L_0x0062:
            boolean r0 = i.b.k.AppCompatDelegateImpl.b0
            r2 = 1
            if (r0 == 0) goto L_0x009a
            boolean r0 = r12 instanceof org.xmlpull.v1.XmlPullParser
            if (r0 == 0) goto L_0x0075
            r0 = r12
            org.xmlpull.v1.XmlPullParser r0 = (org.xmlpull.v1.XmlPullParser) r0
            int r0 = r0.getDepth()
            if (r0 <= r2) goto L_0x009a
            goto L_0x0083
        L_0x0075:
            r0 = r9
            android.view.ViewParent r0 = (android.view.ViewParent) r0
            if (r0 != 0) goto L_0x007b
            goto L_0x009a
        L_0x007b:
            android.view.Window r3 = r8.f739f
            android.view.View r3 = r3.getDecorView()
        L_0x0081:
            if (r0 != 0) goto L_0x0085
        L_0x0083:
            r0 = 1
            goto L_0x009b
        L_0x0085:
            if (r0 == r3) goto L_0x009a
            boolean r4 = r0 instanceof android.view.View
            if (r4 == 0) goto L_0x009a
            r4 = r0
            android.view.View r4 = (android.view.View) r4
            boolean r4 = i.h.l.ViewCompat.v(r4)
            if (r4 == 0) goto L_0x0095
            goto L_0x009a
        L_0x0095:
            android.view.ViewParent r0 = r0.getParent()
            goto L_0x0081
        L_0x009a:
            r0 = 0
        L_0x009b:
            androidx.appcompat.app.AppCompatViewInflater r3 = r8.Z
            boolean r4 = i.b.k.AppCompatDelegateImpl.b0
            boolean r5 = i.b.q.VectorEnabledTintResources.a
            r5 = 0
            if (r3 == 0) goto L_0x0291
            if (r0 == 0) goto L_0x00ad
            if (r9 == 0) goto L_0x00ad
            android.content.Context r9 = r9.getContext()
            goto L_0x00ae
        L_0x00ad:
            r9 = r11
        L_0x00ae:
            int[] r0 = i.b.j.View
            android.content.res.TypedArray r0 = r9.obtainStyledAttributes(r12, r0, r1, r1)
            if (r4 == 0) goto L_0x00bd
            int r4 = i.b.j.View_android_theme
            int r4 = r0.getResourceId(r4, r1)
            goto L_0x00be
        L_0x00bd:
            r4 = 0
        L_0x00be:
            if (r4 != 0) goto L_0x00cf
            int r4 = i.b.j.View_theme
            int r4 = r0.getResourceId(r4, r1)
            if (r4 == 0) goto L_0x00cf
            java.lang.String r6 = "AppCompatViewInflater"
            java.lang.String r7 = "app:theme is now deprecated. Please move to using android:theme instead."
            android.util.Log.i(r6, r7)
        L_0x00cf:
            r0.recycle()
            if (r4 == 0) goto L_0x00e5
            boolean r0 = r9 instanceof i.b.p.ContextThemeWrapper
            if (r0 == 0) goto L_0x00df
            r0 = r9
            i.b.p.ContextThemeWrapper r0 = (i.b.p.ContextThemeWrapper) r0
            int r0 = r0.a
            if (r0 == r4) goto L_0x00e5
        L_0x00df:
            i.b.p.ContextThemeWrapper r0 = new i.b.p.ContextThemeWrapper
            r0.<init>(r9, r4)
            r9 = r0
        L_0x00e5:
            int r0 = r10.hashCode()
            r4 = -1
            switch(r0) {
                case -1946472170: goto L_0x017a;
                case -1455429095: goto L_0x016f;
                case -1346021293: goto L_0x0164;
                case -938935918: goto L_0x015a;
                case -937446323: goto L_0x0150;
                case -658531749: goto L_0x0145;
                case -339785223: goto L_0x013b;
                case 776382189: goto L_0x0131;
                case 799298502: goto L_0x0126;
                case 1125864064: goto L_0x011c;
                case 1413872058: goto L_0x0110;
                case 1601505219: goto L_0x0105;
                case 1666676343: goto L_0x00fa;
                case 2001146706: goto L_0x00ef;
                default: goto L_0x00ed;
            }
        L_0x00ed:
            goto L_0x0185
        L_0x00ef:
            java.lang.String r0 = "Button"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 2
            goto L_0x0186
        L_0x00fa:
            java.lang.String r0 = "EditText"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 3
            goto L_0x0186
        L_0x0105:
            java.lang.String r0 = "CheckBox"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 6
            goto L_0x0186
        L_0x0110:
            java.lang.String r0 = "AutoCompleteTextView"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 9
            goto L_0x0186
        L_0x011c:
            java.lang.String r0 = "ImageView"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 1
            goto L_0x0186
        L_0x0126:
            java.lang.String r0 = "ToggleButton"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 13
            goto L_0x0186
        L_0x0131:
            java.lang.String r0 = "RadioButton"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 7
            goto L_0x0186
        L_0x013b:
            java.lang.String r0 = "Spinner"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 4
            goto L_0x0186
        L_0x0145:
            java.lang.String r0 = "SeekBar"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 12
            goto L_0x0186
        L_0x0150:
            java.lang.String r0 = "ImageButton"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 5
            goto L_0x0186
        L_0x015a:
            java.lang.String r0 = "TextView"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 0
            goto L_0x0186
        L_0x0164:
            java.lang.String r0 = "MultiAutoCompleteTextView"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 10
            goto L_0x0186
        L_0x016f:
            java.lang.String r0 = "CheckedTextView"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 8
            goto L_0x0186
        L_0x017a:
            java.lang.String r0 = "RatingBar"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0185
            r0 = 11
            goto L_0x0186
        L_0x0185:
            r0 = -1
        L_0x0186:
            switch(r0) {
                case 0: goto L_0x0200;
                case 1: goto L_0x01f7;
                case 2: goto L_0x01ef;
                case 3: goto L_0x01e6;
                case 4: goto L_0x01db;
                case 5: goto L_0x01d2;
                case 6: goto L_0x01ca;
                case 7: goto L_0x01c2;
                case 8: goto L_0x01b9;
                case 9: goto L_0x01b1;
                case 10: goto L_0x01a8;
                case 11: goto L_0x019f;
                case 12: goto L_0x0195;
                case 13: goto L_0x018b;
                default: goto L_0x0189;
            }
        L_0x0189:
            goto L_0x0208
        L_0x018b:
            i.b.q.AppCompatToggleButton r0 = new i.b.q.AppCompatToggleButton
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x0195:
            i.b.q.AppCompatSeekBar r0 = new i.b.q.AppCompatSeekBar
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x019f:
            i.b.q.AppCompatRatingBar r0 = new i.b.q.AppCompatRatingBar
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01a8:
            i.b.q.AppCompatMultiAutoCompleteTextView r0 = new i.b.q.AppCompatMultiAutoCompleteTextView
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01b1:
            i.b.q.AppCompatAutoCompleteTextView r0 = r3.a(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01b9:
            i.b.q.AppCompatCheckedTextView r0 = new i.b.q.AppCompatCheckedTextView
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01c2:
            i.b.q.AppCompatRadioButton r0 = r3.d(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01ca:
            androidx.appcompat.widget.AppCompatCheckBox r0 = r3.c(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01d2:
            i.b.q.AppCompatImageButton r0 = new i.b.q.AppCompatImageButton
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01db:
            i.b.q.AppCompatSpinner r0 = new i.b.q.AppCompatSpinner
            int r6 = i.b.a.spinnerStyle
            r0.<init>(r9, r12, r6)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01e6:
            i.b.q.AppCompatEditText r0 = new i.b.q.AppCompatEditText
            r0.<init>(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01ef:
            i.b.q.AppCompatButton r0 = r3.b(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x01f7:
            i.b.q.AppCompatImageView r0 = new i.b.q.AppCompatImageView
            r0.<init>(r9, r12, r1)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x0200:
            i.b.q.AppCompatTextView r0 = r3.e(r9, r12)
            r3.a(r0, r10)
            goto L_0x0209
        L_0x0208:
            r0 = r5
        L_0x0209:
            if (r0 != 0) goto L_0x0268
            if (r11 == r9) goto L_0x0268
            java.lang.String r11 = "view"
            boolean r11 = r10.equals(r11)
            if (r11 == 0) goto L_0x021b
            java.lang.String r10 = "class"
            java.lang.String r10 = r12.getAttributeValue(r5, r10)
        L_0x021b:
            java.lang.Object[] r11 = r3.a     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            r11[r1] = r9     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            java.lang.Object[] r11 = r3.a     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            r11[r2] = r12     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            r11 = 46
            int r11 = r10.indexOf(r11)     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            if (r4 != r11) goto L_0x024d
            r11 = 0
        L_0x022c:
            java.lang.String[] r0 = androidx.appcompat.app.AppCompatViewInflater.d     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            int r0 = r0.length     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            if (r11 >= r0) goto L_0x0246
            java.lang.String[] r0 = androidx.appcompat.app.AppCompatViewInflater.d     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            r0 = r0[r11]     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            android.view.View r0 = r3.a(r9, r10, r0)     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            if (r0 == 0) goto L_0x0243
            java.lang.Object[] r9 = r3.a
            r9[r1] = r5
            r9[r2] = r5
            r5 = r0
            goto L_0x0267
        L_0x0243:
            int r11 = r11 + 1
            goto L_0x022c
        L_0x0246:
            java.lang.Object[] r9 = r3.a
            r9[r1] = r5
            r9[r2] = r5
            goto L_0x0267
        L_0x024d:
            android.view.View r9 = r3.a(r9, r10, r5)     // Catch:{ Exception -> 0x0261, all -> 0x0259 }
            java.lang.Object[] r10 = r3.a
            r10[r1] = r5
            r10[r2] = r5
            r5 = r9
            goto L_0x0267
        L_0x0259:
            r9 = move-exception
            java.lang.Object[] r10 = r3.a
            r10[r1] = r5
            r10[r2] = r5
            throw r9
        L_0x0261:
            java.lang.Object[] r9 = r3.a
            r9[r1] = r5
            r9[r2] = r5
        L_0x0267:
            r0 = r5
        L_0x0268:
            if (r0 == 0) goto L_0x0290
            android.content.Context r9 = r0.getContext()
            boolean r10 = r9 instanceof android.content.ContextWrapper
            if (r10 == 0) goto L_0x0290
            boolean r10 = i.h.l.ViewCompat.s(r0)
            if (r10 != 0) goto L_0x0279
            goto L_0x0290
        L_0x0279:
            int[] r10 = androidx.appcompat.app.AppCompatViewInflater.c
            android.content.res.TypedArray r9 = r9.obtainStyledAttributes(r12, r10)
            java.lang.String r10 = r9.getString(r1)
            if (r10 == 0) goto L_0x028d
            androidx.appcompat.app.AppCompatViewInflater$a r11 = new androidx.appcompat.app.AppCompatViewInflater$a
            r11.<init>(r0, r10)
            r0.setOnClickListener(r11)
        L_0x028d:
            r9.recycle()
        L_0x0290:
            return r0
        L_0x0291:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDelegateImpl.onCreateView(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    public final class c implements MenuPresenter.a {
        public c() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback i2 = AppCompatDelegateImpl.this.i();
            if (i2 == null) {
                return true;
            }
            i2.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            AppCompatDelegateImpl.this.b(menuBuilder);
        }
    }

    public final void e(int i2) {
        this.U = (1 << i2) | this.U;
        if (!this.T) {
            ViewCompat.a(this.f739f.getDecorView(), this.V);
            this.T = true;
        }
    }

    public class e extends WindowCallbackWrapper {
        public e(Window.Callback callback) {
            super(callback);
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x004e  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0052  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final android.view.ActionMode a(android.view.ActionMode.Callback r10) {
            /*
                r9 = this;
                i.b.p.SupportActionModeWrapper$a r0 = new i.b.p.SupportActionModeWrapper$a
                i.b.k.AppCompatDelegateImpl r1 = i.b.k.AppCompatDelegateImpl.this
                android.content.Context r1 = r1.f738e
                r0.<init>(r1, r10)
                i.b.k.AppCompatDelegateImpl r10 = i.b.k.AppCompatDelegateImpl.this
                r1 = 0
                if (r10 == 0) goto L_0x01ba
                i.b.p.ActionMode r2 = r10.f746o
                if (r2 == 0) goto L_0x0015
                r2.a()
            L_0x0015:
                i.b.k.AppCompatDelegateImpl$d r2 = new i.b.k.AppCompatDelegateImpl$d
                r2.<init>(r0)
                r10.j()
                i.b.k.ActionBar r3 = r10.f740i
                if (r3 == 0) goto L_0x0030
                i.b.p.ActionMode r3 = r3.a(r2)
                r10.f746o = r3
                if (r3 == 0) goto L_0x0030
                i.b.k.AppCompatCallback r4 = r10.h
                if (r4 == 0) goto L_0x0030
                r4.a(r3)
            L_0x0030:
                i.b.p.ActionMode r3 = r10.f746o
                if (r3 != 0) goto L_0x01b0
                r10.e()
                i.b.p.ActionMode r3 = r10.f746o
                if (r3 == 0) goto L_0x003e
                r3.a()
            L_0x003e:
                i.b.k.AppCompatCallback r3 = r10.h
                if (r3 == 0) goto L_0x004b
                boolean r4 = r10.M
                if (r4 != 0) goto L_0x004b
                i.b.p.ActionMode r3 = r3.a(r2)     // Catch:{ AbstractMethodError -> 0x004b }
                goto L_0x004c
            L_0x004b:
                r3 = r1
            L_0x004c:
                if (r3 == 0) goto L_0x0052
                r10.f746o = r3
                goto L_0x01a1
            L_0x0052:
                androidx.appcompat.widget.ActionBarContextView r3 = r10.f747p
                r4 = 1
                r5 = 0
                if (r3 != 0) goto L_0x010a
                boolean r3 = r10.D
                if (r3 == 0) goto L_0x00de
                android.util.TypedValue r3 = new android.util.TypedValue
                r3.<init>()
                android.content.Context r6 = r10.f738e
                android.content.res.Resources$Theme r6 = r6.getTheme()
                int r7 = i.b.a.actionBarTheme
                r6.resolveAttribute(r7, r3, r4)
                int r7 = r3.resourceId
                if (r7 == 0) goto L_0x0091
                android.content.Context r7 = r10.f738e
                android.content.res.Resources r7 = r7.getResources()
                android.content.res.Resources$Theme r7 = r7.newTheme()
                r7.setTo(r6)
                int r6 = r3.resourceId
                r7.applyStyle(r6, r4)
                i.b.p.ContextThemeWrapper r6 = new i.b.p.ContextThemeWrapper
                android.content.Context r8 = r10.f738e
                r6.<init>(r8, r5)
                android.content.res.Resources$Theme r8 = r6.getTheme()
                r8.setTo(r7)
                goto L_0x0093
            L_0x0091:
                android.content.Context r6 = r10.f738e
            L_0x0093:
                androidx.appcompat.widget.ActionBarContextView r7 = new androidx.appcompat.widget.ActionBarContextView
                r7.<init>(r6)
                r10.f747p = r7
                android.widget.PopupWindow r7 = new android.widget.PopupWindow
                int r8 = i.b.a.actionModePopupWindowStyle
                r7.<init>(r6, r1, r8)
                r10.f748q = r7
                r8 = 2
                i.b.k.ResourcesFlusher.a(r7, r8)
                android.widget.PopupWindow r7 = r10.f748q
                androidx.appcompat.widget.ActionBarContextView r8 = r10.f747p
                r7.setContentView(r8)
                android.widget.PopupWindow r7 = r10.f748q
                r8 = -1
                r7.setWidth(r8)
                android.content.res.Resources$Theme r7 = r6.getTheme()
                int r8 = i.b.a.actionBarSize
                r7.resolveAttribute(r8, r3, r4)
                int r3 = r3.data
                android.content.res.Resources r6 = r6.getResources()
                android.util.DisplayMetrics r6 = r6.getDisplayMetrics()
                int r3 = android.util.TypedValue.complexToDimensionPixelSize(r3, r6)
                androidx.appcompat.widget.ActionBarContextView r6 = r10.f747p
                r6.setContentHeight(r3)
                android.widget.PopupWindow r3 = r10.f748q
                r6 = -2
                r3.setHeight(r6)
                i.b.k.AppCompatDelegateImpl0 r3 = new i.b.k.AppCompatDelegateImpl0
                r3.<init>(r10)
                r10.f749r = r3
                goto L_0x010a
            L_0x00de:
                android.view.ViewGroup r3 = r10.v
                int r6 = i.b.f.action_mode_bar_stub
                android.view.View r3 = r3.findViewById(r6)
                androidx.appcompat.widget.ViewStubCompat r3 = (androidx.appcompat.widget.ViewStubCompat) r3
                if (r3 == 0) goto L_0x010a
                r10.j()
                i.b.k.ActionBar r6 = r10.f740i
                if (r6 == 0) goto L_0x00f6
                android.content.Context r6 = r6.d()
                goto L_0x00f7
            L_0x00f6:
                r6 = r1
            L_0x00f7:
                if (r6 != 0) goto L_0x00fb
                android.content.Context r6 = r10.f738e
            L_0x00fb:
                android.view.LayoutInflater r6 = android.view.LayoutInflater.from(r6)
                r3.setLayoutInflater(r6)
                android.view.View r3 = r3.a()
                androidx.appcompat.widget.ActionBarContextView r3 = (androidx.appcompat.widget.ActionBarContextView) r3
                r10.f747p = r3
            L_0x010a:
                androidx.appcompat.widget.ActionBarContextView r3 = r10.f747p
                if (r3 == 0) goto L_0x01a1
                r10.e()
                androidx.appcompat.widget.ActionBarContextView r3 = r10.f747p
                r3.b()
                i.b.p.StandaloneActionMode r3 = new i.b.p.StandaloneActionMode
                androidx.appcompat.widget.ActionBarContextView r6 = r10.f747p
                android.content.Context r6 = r6.getContext()
                androidx.appcompat.widget.ActionBarContextView r7 = r10.f747p
                android.widget.PopupWindow r8 = r10.f748q
                if (r8 != 0) goto L_0x0125
                goto L_0x0126
            L_0x0125:
                r4 = 0
            L_0x0126:
                r3.<init>(r6, r7, r2, r4)
                i.b.p.i.MenuBuilder r4 = r3.f826i
                i.b.p.ActionMode$a r2 = r2.a
                boolean r2 = r2.b(r3, r4)
                if (r2 == 0) goto L_0x019f
                r3.g()
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                r2.a(r3)
                r10.f746o = r3
                boolean r2 = r10.k()
                r3 = 1065353216(0x3f800000, float:1.0)
                if (r2 == 0) goto L_0x0169
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                r4 = 0
                r2.setAlpha(r4)
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                i.h.l.ViewPropertyAnimatorCompat r2 = i.h.l.ViewCompat.a(r2)
                r2.a(r3)
                r10.f750s = r2
                i.b.k.AppCompatDelegateImpl4 r3 = new i.b.k.AppCompatDelegateImpl4
                r3.<init>(r10)
                java.lang.ref.WeakReference<android.view.View> r4 = r2.a
                java.lang.Object r4 = r4.get()
                android.view.View r4 = (android.view.View) r4
                if (r4 == 0) goto L_0x018f
                r2.a(r4, r3)
                goto L_0x018f
            L_0x0169:
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                r2.setAlpha(r3)
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                r2.setVisibility(r5)
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                r3 = 32
                r2.sendAccessibilityEvent(r3)
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                android.view.ViewParent r2 = r2.getParent()
                boolean r2 = r2 instanceof android.view.View
                if (r2 == 0) goto L_0x018f
                androidx.appcompat.widget.ActionBarContextView r2 = r10.f747p
                android.view.ViewParent r2 = r2.getParent()
                android.view.View r2 = (android.view.View) r2
                r2.requestApplyInsets()
            L_0x018f:
                android.widget.PopupWindow r2 = r10.f748q
                if (r2 == 0) goto L_0x01a1
                android.view.Window r2 = r10.f739f
                android.view.View r2 = r2.getDecorView()
                java.lang.Runnable r3 = r10.f749r
                r2.post(r3)
                goto L_0x01a1
            L_0x019f:
                r10.f746o = r1
            L_0x01a1:
                i.b.p.ActionMode r2 = r10.f746o
                if (r2 == 0) goto L_0x01ac
                i.b.k.AppCompatCallback r3 = r10.h
                if (r3 == 0) goto L_0x01ac
                r3.a(r2)
            L_0x01ac:
                i.b.p.ActionMode r2 = r10.f746o
                r10.f746o = r2
            L_0x01b0:
                i.b.p.ActionMode r10 = r10.f746o
                if (r10 == 0) goto L_0x01b9
                android.view.ActionMode r10 = r0.b(r10)
                return r10
            L_0x01b9:
                return r1
            L_0x01ba:
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDelegateImpl.e.a(android.view.ActionMode$Callback):android.view.ActionMode");
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImpl.this.a(keyEvent) || super.b.dispatchKeyEvent(keyEvent);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
            if (r6 != false) goto L_0x001d;
         */
        /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean dispatchKeyShortcutEvent(android.view.KeyEvent r6) {
            /*
                r5 = this;
                android.view.Window$Callback r0 = r5.b
                boolean r0 = r0.dispatchKeyShortcutEvent(r6)
                r1 = 0
                r2 = 1
                if (r0 != 0) goto L_0x004f
                i.b.k.AppCompatDelegateImpl r0 = i.b.k.AppCompatDelegateImpl.this
                int r3 = r6.getKeyCode()
                r0.j()
                i.b.k.ActionBar r4 = r0.f740i
                if (r4 == 0) goto L_0x001f
                boolean r3 = r4.a(r3, r6)
                if (r3 == 0) goto L_0x001f
            L_0x001d:
                r6 = 1
                goto L_0x004d
            L_0x001f:
                i.b.k.AppCompatDelegateImpl$j r3 = r0.H
                if (r3 == 0) goto L_0x0034
                int r4 = r6.getKeyCode()
                boolean r3 = r0.a(r3, r4, r6, r2)
                if (r3 == 0) goto L_0x0034
                i.b.k.AppCompatDelegateImpl$j r6 = r0.H
                if (r6 == 0) goto L_0x001d
                r6.f760n = r2
                goto L_0x001d
            L_0x0034:
                i.b.k.AppCompatDelegateImpl$j r3 = r0.H
                if (r3 != 0) goto L_0x004c
                i.b.k.AppCompatDelegateImpl$j r3 = r0.d(r1)
                r0.b(r3, r6)
                int r4 = r6.getKeyCode()
                boolean r6 = r0.a(r3, r4, r6, r2)
                r3.f759m = r1
                if (r6 == 0) goto L_0x004c
                goto L_0x001d
            L_0x004c:
                r6 = 0
            L_0x004d:
                if (r6 == 0) goto L_0x0050
            L_0x004f:
                r1 = 1
            L_0x0050:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDelegateImpl.e.dispatchKeyShortcutEvent(android.view.KeyEvent):boolean");
        }

        public void onContentChanged() {
        }

        public boolean onCreatePanelMenu(int i2, Menu menu) {
            if (i2 != 0 || (menu instanceof MenuBuilder)) {
                return super.b.onCreatePanelMenu(i2, menu);
            }
            return false;
        }

        public boolean onMenuOpened(int i2, Menu menu) {
            super.b.onMenuOpened(i2, menu);
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl != null) {
                if (i2 == 108) {
                    appCompatDelegateImpl.j();
                    ActionBar actionBar = appCompatDelegateImpl.f740i;
                    if (actionBar != null) {
                        actionBar.a(true);
                    }
                }
                return true;
            }
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
         arg types: [i.b.k.AppCompatDelegateImpl$j, int]
         candidates:
          i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
          i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
          i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
          i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
          i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
          i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
        public void onPanelClosed(int i2, Menu menu) {
            super.b.onPanelClosed(i2, menu);
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (appCompatDelegateImpl == null) {
                throw null;
            } else if (i2 == 108) {
                appCompatDelegateImpl.j();
                ActionBar actionBar = appCompatDelegateImpl.f740i;
                if (actionBar != null) {
                    actionBar.a(false);
                }
            } else if (i2 == 0) {
                j d = appCompatDelegateImpl.d(i2);
                if (d.f761o) {
                    appCompatDelegateImpl.a(d, false);
                }
            }
        }

        public boolean onPreparePanel(int i2, View view, Menu menu) {
            MenuBuilder menuBuilder = menu instanceof MenuBuilder ? (MenuBuilder) menu : null;
            if (i2 == 0 && menuBuilder == null) {
                return false;
            }
            if (menuBuilder != null) {
                menuBuilder.y = true;
            }
            boolean onPreparePanel = super.b.onPreparePanel(i2, view, menu);
            if (menuBuilder != null) {
                menuBuilder.y = false;
            }
            return onPreparePanel;
        }

        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i2) {
            MenuBuilder menuBuilder;
            j d = AppCompatDelegateImpl.this.d(0);
            if (d == null || (menuBuilder = d.f756j) == null) {
                super.b.onProvideKeyboardShortcuts(list, menu, i2);
            } else {
                super.b.onProvideKeyboardShortcuts(list, menuBuilder, i2);
            }
        }

        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (AppCompatDelegateImpl.this.f751t) {
                return a(callback);
            }
            return super.b.onWindowStartingActionMode(callback);
        }

        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i2) {
            if (!AppCompatDelegateImpl.this.f751t || i2 != 0) {
                return super.b.onWindowStartingActionMode(callback, i2);
            }
            return a(callback);
        }
    }

    public final class k implements MenuPresenter.a {
        public k() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
         arg types: [i.b.k.AppCompatDelegateImpl$j, int]
         candidates:
          i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
          i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
          i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
          i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
          i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
          i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
        public void a(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder c = menuBuilder.c();
            boolean z2 = c != menuBuilder;
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (z2) {
                menuBuilder = c;
            }
            j a = appCompatDelegateImpl.a((Menu) menuBuilder);
            if (a == null) {
                return;
            }
            if (z2) {
                AppCompatDelegateImpl.this.a(a.a, a, c);
                AppCompatDelegateImpl.this.a(a, true);
                return;
            }
            AppCompatDelegateImpl.this.a(a, z);
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback i2;
            if (menuBuilder != null) {
                return true;
            }
            AppCompatDelegateImpl appCompatDelegateImpl = AppCompatDelegateImpl.this;
            if (!appCompatDelegateImpl.A || (i2 = appCompatDelegateImpl.i()) == null || AppCompatDelegateImpl.this.M) {
                return true;
            }
            i2.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    public void b(int i2) {
        f();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f738e).inflate(i2, viewGroup);
        this.g.b.onContentChanged();
    }

    public boolean d() {
        return a(true);
    }

    public void c(int i2) {
        j d2;
        j d3 = d(i2);
        if (d3.f756j != null) {
            Bundle bundle = new Bundle();
            d3.f756j.b(bundle);
            if (bundle.size() > 0) {
                d3.f765s = bundle;
            }
            d3.f756j.j();
            d3.f756j.clear();
        }
        d3.f764r = true;
        d3.f763q = true;
        if ((i2 == 108 || i2 == 0) && this.f743l != null && (d2 = d(0)) != null) {
            d2.f759m = false;
            b(d2, (KeyEvent) null);
        }
    }

    public void a(View view) {
        f();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.g.b.onContentChanged();
    }

    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        f();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.g.b.onContentChanged();
    }

    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        f();
        ((ViewGroup) this.v.findViewById(16908290)).addView(view, layoutParams);
        this.g.b.onContentChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
     arg types: [i.b.k.AppCompatDelegateImpl$j, int]
     candidates:
      i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
      i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
    public final boolean b(j jVar, KeyEvent keyEvent) {
        DecorContentParent decorContentParent;
        DecorContentParent decorContentParent2;
        Resources.Theme theme;
        DecorContentParent decorContentParent3;
        if (this.M) {
            return false;
        }
        if (jVar.f759m) {
            return true;
        }
        j jVar2 = this.H;
        if (!(jVar2 == null || jVar2 == jVar)) {
            a(jVar2, false);
        }
        Window.Callback i2 = i();
        if (i2 != null) {
            jVar.f755i = i2.onCreatePanelView(jVar.a);
        }
        int i3 = jVar.a;
        boolean z2 = i3 == 0 || i3 == 108;
        if (z2 && (decorContentParent3 = this.f743l) != null) {
            decorContentParent3.f();
        }
        if (jVar.f755i == null && (!z2 || !(this.f740i instanceof ToolbarActionBar))) {
            if (jVar.f756j == null || jVar.f764r) {
                if (jVar.f756j == null) {
                    Context context = this.f738e;
                    int i4 = jVar.a;
                    if ((i4 == 0 || i4 == 108) && this.f743l != null) {
                        TypedValue typedValue = new TypedValue();
                        Resources.Theme theme2 = context.getTheme();
                        theme2.resolveAttribute(i.b.a.actionBarTheme, typedValue, true);
                        if (typedValue.resourceId != 0) {
                            theme = context.getResources().newTheme();
                            theme.setTo(theme2);
                            theme.applyStyle(typedValue.resourceId, true);
                            theme.resolveAttribute(i.b.a.actionBarWidgetTheme, typedValue, true);
                        } else {
                            theme2.resolveAttribute(i.b.a.actionBarWidgetTheme, typedValue, true);
                            theme = null;
                        }
                        if (typedValue.resourceId != 0) {
                            if (theme == null) {
                                theme = context.getResources().newTheme();
                                theme.setTo(theme2);
                            }
                            theme.applyStyle(typedValue.resourceId, true);
                        }
                        if (theme != null) {
                            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, 0);
                            contextThemeWrapper.getTheme().setTo(theme);
                            context = contextThemeWrapper;
                        }
                    }
                    MenuBuilder menuBuilder = new MenuBuilder(context);
                    menuBuilder.f882e = this;
                    jVar.a(menuBuilder);
                    if (jVar.f756j == null) {
                        return false;
                    }
                }
                if (z2 && this.f743l != null) {
                    if (this.f744m == null) {
                        this.f744m = new c();
                    }
                    this.f743l.a(jVar.f756j, this.f744m);
                }
                jVar.f756j.j();
                if (!i2.onCreatePanelMenu(jVar.a, jVar.f756j)) {
                    jVar.a(null);
                    if (z2 && (decorContentParent2 = this.f743l) != null) {
                        decorContentParent2.a(null, this.f744m);
                    }
                    return false;
                }
                jVar.f764r = false;
            }
            jVar.f756j.j();
            Bundle bundle = jVar.f765s;
            if (bundle != null) {
                jVar.f756j.a(bundle);
                jVar.f765s = null;
            }
            if (!i2.onPreparePanel(0, jVar.f755i, jVar.f756j)) {
                if (z2 && (decorContentParent = this.f743l) != null) {
                    decorContentParent.a(null, this.f744m);
                }
                jVar.f756j.i();
                return false;
            }
            boolean z3 = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            jVar.f762p = z3;
            jVar.f756j.setQwertyMode(z3);
            jVar.f756j.i();
        }
        jVar.f759m = true;
        jVar.f760n = false;
        this.H = jVar;
        return true;
    }

    public final void a(Window window) {
        if (this.f739f == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof e)) {
                e eVar = new e(callback);
                this.g = eVar;
                window.setCallback(eVar);
                TintTypedArray a2 = TintTypedArray.a(this.f738e, (AttributeSet) null, c0);
                Drawable c2 = a2.c(0);
                if (c2 != null) {
                    window.setBackgroundDrawable(c2);
                }
                a2.b.recycle();
                this.f739f = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    public final void a(CharSequence charSequence) {
        this.f742k = charSequence;
        DecorContentParent decorContentParent = this.f743l;
        if (decorContentParent != null) {
            decorContentParent.setWindowTitle(charSequence);
            return;
        }
        ActionBar actionBar = this.f740i;
        if (actionBar != null) {
            actionBar.a(charSequence);
            return;
        }
        TextView textView = this.w;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        j a2;
        Window.Callback i2 = i();
        if (i2 == null || this.M || (a2 = a((Menu) menuBuilder.c())) == null) {
            return false;
        }
        return i2.onMenuItemSelected(a2.a, menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
     arg types: [i.b.k.AppCompatDelegateImpl$j, int]
     candidates:
      i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
      i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.KeyEvent r7) {
        /*
            r6 = this;
            java.lang.Object r0 = r6.d
            boolean r1 = r0 instanceof i.h.l.KeyEventDispatcher.a
            r2 = 1
            if (r1 != 0) goto L_0x000b
            boolean r0 = r0 instanceof i.b.k.AppCompatDialog
            if (r0 == 0) goto L_0x001a
        L_0x000b:
            android.view.Window r0 = r6.f739f
            android.view.View r0 = r0.getDecorView()
            if (r0 == 0) goto L_0x001a
            boolean r0 = i.h.l.ViewCompat.b(r0, r7)
            if (r0 == 0) goto L_0x001a
            return r2
        L_0x001a:
            int r0 = r7.getKeyCode()
            r1 = 82
            if (r0 != r1) goto L_0x002d
            i.b.k.AppCompatDelegateImpl$e r0 = r6.g
            android.view.Window$Callback r0 = r0.b
            boolean r0 = r0.dispatchKeyEvent(r7)
            if (r0 == 0) goto L_0x002d
            return r2
        L_0x002d:
            int r0 = r7.getKeyCode()
            int r3 = r7.getAction()
            r4 = 0
            if (r3 != 0) goto L_0x003a
            r3 = 1
            goto L_0x003b
        L_0x003a:
            r3 = 0
        L_0x003b:
            r5 = 4
            if (r3 == 0) goto L_0x0065
            if (r0 == r5) goto L_0x0056
            if (r0 == r1) goto L_0x0043
            goto L_0x0062
        L_0x0043:
            int r0 = r7.getRepeatCount()
            if (r0 != 0) goto L_0x0119
            i.b.k.AppCompatDelegateImpl$j r0 = r6.d(r4)
            boolean r1 = r0.f761o
            if (r1 != 0) goto L_0x0119
            r6.b(r0, r7)
            goto L_0x0119
        L_0x0056:
            int r7 = r7.getFlags()
            r7 = r7 & 128(0x80, float:1.794E-43)
            if (r7 == 0) goto L_0x005f
            goto L_0x0060
        L_0x005f:
            r2 = 0
        L_0x0060:
            r6.I = r2
        L_0x0062:
            r2 = 0
            goto L_0x0119
        L_0x0065:
            if (r0 == r5) goto L_0x00eb
            if (r0 == r1) goto L_0x006a
            goto L_0x0062
        L_0x006a:
            i.b.p.ActionMode r0 = r6.f746o
            if (r0 == 0) goto L_0x0070
            goto L_0x0119
        L_0x0070:
            i.b.k.AppCompatDelegateImpl$j r0 = r6.d(r4)
            i.b.q.DecorContentParent r1 = r6.f743l
            if (r1 == 0) goto L_0x00aa
            boolean r1 = r1.g()
            if (r1 == 0) goto L_0x00aa
            android.content.Context r1 = r6.f738e
            android.view.ViewConfiguration r1 = android.view.ViewConfiguration.get(r1)
            boolean r1 = r1.hasPermanentMenuKey()
            if (r1 != 0) goto L_0x00aa
            i.b.q.DecorContentParent r1 = r6.f743l
            boolean r1 = r1.b()
            if (r1 != 0) goto L_0x00a3
            boolean r1 = r6.M
            if (r1 != 0) goto L_0x00ca
            boolean r7 = r6.b(r0, r7)
            if (r7 == 0) goto L_0x00ca
            i.b.q.DecorContentParent r7 = r6.f743l
            boolean r7 = r7.e()
            goto L_0x00d1
        L_0x00a3:
            i.b.q.DecorContentParent r7 = r6.f743l
            boolean r7 = r7.d()
            goto L_0x00d1
        L_0x00aa:
            boolean r1 = r0.f761o
            if (r1 != 0) goto L_0x00cc
            boolean r1 = r0.f760n
            if (r1 == 0) goto L_0x00b3
            goto L_0x00cc
        L_0x00b3:
            boolean r1 = r0.f759m
            if (r1 == 0) goto L_0x00ca
            boolean r1 = r0.f764r
            if (r1 == 0) goto L_0x00c2
            r0.f759m = r4
            boolean r1 = r6.b(r0, r7)
            goto L_0x00c3
        L_0x00c2:
            r1 = 1
        L_0x00c3:
            if (r1 == 0) goto L_0x00ca
            r6.a(r0, r7)
            r7 = 1
            goto L_0x00d1
        L_0x00ca:
            r7 = 0
            goto L_0x00d1
        L_0x00cc:
            boolean r7 = r0.f761o
            r6.a(r0, r2)
        L_0x00d1:
            if (r7 == 0) goto L_0x0119
            android.content.Context r7 = r6.f738e
            java.lang.String r0 = "audio"
            java.lang.Object r7 = r7.getSystemService(r0)
            android.media.AudioManager r7 = (android.media.AudioManager) r7
            if (r7 == 0) goto L_0x00e3
            r7.playSoundEffect(r4)
            goto L_0x0119
        L_0x00e3:
            java.lang.String r7 = "AppCompatDelegate"
            java.lang.String r0 = "Couldn't get audio manager"
            android.util.Log.w(r7, r0)
            goto L_0x0119
        L_0x00eb:
            boolean r7 = r6.I
            r6.I = r4
            i.b.k.AppCompatDelegateImpl$j r0 = r6.d(r4)
            if (r0 == 0) goto L_0x00ff
            boolean r1 = r0.f761o
            if (r1 == 0) goto L_0x00ff
            if (r7 != 0) goto L_0x0119
            r6.a(r0, r2)
            goto L_0x0119
        L_0x00ff:
            i.b.p.ActionMode r7 = r6.f746o
            if (r7 == 0) goto L_0x0107
            r7.a()
            goto L_0x0114
        L_0x0107:
            r6.j()
            i.b.k.ActionBar r7 = r6.f740i
            if (r7 == 0) goto L_0x0116
            boolean r7 = r7.b()
            if (r7 == 0) goto L_0x0116
        L_0x0114:
            r7 = 1
            goto L_0x0117
        L_0x0116:
            r7 = 0
        L_0x0117:
            if (r7 == 0) goto L_0x0062
        L_0x0119:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDelegateImpl.a(android.view.KeyEvent):boolean");
    }

    public void b(MenuBuilder menuBuilder) {
        if (!this.F) {
            this.F = true;
            this.f743l.h();
            Window.Callback i2 = i();
            if (i2 != null && !this.M) {
                i2.onPanelClosed(108, menuBuilder);
            }
            this.F = false;
        }
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    public void a() {
        LayoutInflater from = LayoutInflater.from(this.f738e);
        if (from.getFactory() == null) {
            from.setFactory2(this);
        } else if (!(from.getFactory2() instanceof AppCompatDelegateImpl)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
     arg types: [i.b.k.AppCompatDelegateImpl$j, int]
     candidates:
      i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
      i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x015c, code lost:
        if (r4 != null) goto L_0x015e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:103:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0163  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(i.b.k.AppCompatDelegateImpl.j r13, android.view.KeyEvent r14) {
        /*
            r12 = this;
            boolean r0 = r13.f761o
            if (r0 != 0) goto L_0x01da
            boolean r0 = r12.M
            if (r0 == 0) goto L_0x000a
            goto L_0x01da
        L_0x000a:
            int r0 = r13.a
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0027
            android.content.Context r0 = r12.f738e
            android.content.res.Resources r0 = r0.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r0 = r0.screenLayout
            r0 = r0 & 15
            r3 = 4
            if (r0 != r3) goto L_0x0023
            r0 = 1
            goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 == 0) goto L_0x0027
            return
        L_0x0027:
            android.view.Window$Callback r0 = r12.i()
            if (r0 == 0) goto L_0x003b
            int r3 = r13.a
            i.b.p.i.MenuBuilder r4 = r13.f756j
            boolean r0 = r0.onMenuOpened(r3, r4)
            if (r0 != 0) goto L_0x003b
            r12.a(r13, r1)
            return
        L_0x003b:
            android.content.Context r0 = r12.f738e
            java.lang.String r3 = "window"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            if (r0 != 0) goto L_0x0048
            return
        L_0x0048:
            boolean r14 = r12.b(r13, r14)
            if (r14 != 0) goto L_0x004f
            return
        L_0x004f:
            android.view.ViewGroup r14 = r13.g
            r3 = -2
            r4 = -1
            if (r14 == 0) goto L_0x006b
            boolean r14 = r13.f763q
            if (r14 == 0) goto L_0x005a
            goto L_0x006b
        L_0x005a:
            android.view.View r14 = r13.f755i
            if (r14 == 0) goto L_0x01b8
            android.view.ViewGroup$LayoutParams r14 = r14.getLayoutParams()
            if (r14 == 0) goto L_0x01b8
            int r14 = r14.width
            if (r14 != r4) goto L_0x01b8
            r5 = -1
            goto L_0x01b9
        L_0x006b:
            android.view.ViewGroup r14 = r13.g
            r4 = 0
            if (r14 != 0) goto L_0x00e8
            r12.j()
            i.b.k.ActionBar r14 = r12.f740i
            if (r14 == 0) goto L_0x007c
            android.content.Context r14 = r14.d()
            goto L_0x007d
        L_0x007c:
            r14 = r4
        L_0x007d:
            if (r14 != 0) goto L_0x0081
            android.content.Context r14 = r12.f738e
        L_0x0081:
            android.util.TypedValue r5 = new android.util.TypedValue
            r5.<init>()
            android.content.res.Resources r6 = r14.getResources()
            android.content.res.Resources$Theme r6 = r6.newTheme()
            android.content.res.Resources$Theme r7 = r14.getTheme()
            r6.setTo(r7)
            int r7 = i.b.a.actionBarPopupTheme
            r6.resolveAttribute(r7, r5, r1)
            int r7 = r5.resourceId
            if (r7 == 0) goto L_0x00a1
            r6.applyStyle(r7, r1)
        L_0x00a1:
            int r7 = i.b.a.panelMenuListTheme
            r6.resolveAttribute(r7, r5, r1)
            int r5 = r5.resourceId
            if (r5 == 0) goto L_0x00ae
            r6.applyStyle(r5, r1)
            goto L_0x00b3
        L_0x00ae:
            int r5 = i.b.i.Theme_AppCompat_CompactMenu
            r6.applyStyle(r5, r1)
        L_0x00b3:
            i.b.p.ContextThemeWrapper r5 = new i.b.p.ContextThemeWrapper
            r5.<init>(r14, r2)
            android.content.res.Resources$Theme r14 = r5.getTheme()
            r14.setTo(r6)
            r13.f758l = r5
            int[] r14 = i.b.j.AppCompatTheme
            android.content.res.TypedArray r14 = r5.obtainStyledAttributes(r14)
            int r5 = i.b.j.AppCompatTheme_panelBackground
            int r5 = r14.getResourceId(r5, r2)
            r13.b = r5
            int r5 = i.b.j.AppCompatTheme_android_windowAnimationStyle
            int r5 = r14.getResourceId(r5, r2)
            r13.f754f = r5
            r14.recycle()
            i.b.k.AppCompatDelegateImpl$i r14 = new i.b.k.AppCompatDelegateImpl$i
            android.content.Context r5 = r13.f758l
            r14.<init>(r5)
            r13.g = r14
            r14 = 81
            r13.c = r14
            goto L_0x00f7
        L_0x00e8:
            boolean r5 = r13.f763q
            if (r5 == 0) goto L_0x00f7
            int r14 = r14.getChildCount()
            if (r14 <= 0) goto L_0x00f7
            android.view.ViewGroup r14 = r13.g
            r14.removeAllViews()
        L_0x00f7:
            android.view.View r14 = r13.f755i
            if (r14 == 0) goto L_0x00fe
            r13.h = r14
            goto L_0x015e
        L_0x00fe:
            i.b.p.i.MenuBuilder r14 = r13.f756j
            if (r14 != 0) goto L_0x0103
            goto L_0x0160
        L_0x0103:
            i.b.k.AppCompatDelegateImpl$k r14 = r12.f745n
            if (r14 != 0) goto L_0x010e
            i.b.k.AppCompatDelegateImpl$k r14 = new i.b.k.AppCompatDelegateImpl$k
            r14.<init>()
            r12.f745n = r14
        L_0x010e:
            i.b.k.AppCompatDelegateImpl$k r14 = r12.f745n
            i.b.p.i.MenuBuilder r5 = r13.f756j
            if (r5 != 0) goto L_0x0115
            goto L_0x015a
        L_0x0115:
            i.b.p.i.ListMenuPresenter r4 = r13.f757k
            if (r4 != 0) goto L_0x012d
            i.b.p.i.ListMenuPresenter r4 = new i.b.p.i.ListMenuPresenter
            android.content.Context r5 = r13.f758l
            int r6 = i.b.g.abc_list_menu_item_layout
            r4.<init>(r5, r6)
            r13.f757k = r4
            r4.f878i = r14
            i.b.p.i.MenuBuilder r14 = r13.f756j
            android.content.Context r5 = r14.a
            r14.a(r4, r5)
        L_0x012d:
            i.b.p.i.ListMenuPresenter r14 = r13.f757k
            android.view.ViewGroup r4 = r13.g
            androidx.appcompat.view.menu.ExpandedMenuView r5 = r14.f876e
            if (r5 != 0) goto L_0x0158
            android.view.LayoutInflater r5 = r14.c
            int r6 = i.b.g.abc_expanded_menu_layout
            android.view.View r4 = r5.inflate(r6, r4, r2)
            androidx.appcompat.view.menu.ExpandedMenuView r4 = (androidx.appcompat.view.menu.ExpandedMenuView) r4
            r14.f876e = r4
            i.b.p.i.ListMenuPresenter$a r4 = r14.f879j
            if (r4 != 0) goto L_0x014c
            i.b.p.i.ListMenuPresenter$a r4 = new i.b.p.i.ListMenuPresenter$a
            r4.<init>()
            r14.f879j = r4
        L_0x014c:
            androidx.appcompat.view.menu.ExpandedMenuView r4 = r14.f876e
            i.b.p.i.ListMenuPresenter$a r5 = r14.f879j
            r4.setAdapter(r5)
            androidx.appcompat.view.menu.ExpandedMenuView r4 = r14.f876e
            r4.setOnItemClickListener(r14)
        L_0x0158:
            androidx.appcompat.view.menu.ExpandedMenuView r4 = r14.f876e
        L_0x015a:
            r13.h = r4
            if (r4 == 0) goto L_0x0160
        L_0x015e:
            r14 = 1
            goto L_0x0161
        L_0x0160:
            r14 = 0
        L_0x0161:
            if (r14 == 0) goto L_0x01da
            android.view.View r14 = r13.h
            if (r14 != 0) goto L_0x0168
            goto L_0x017b
        L_0x0168:
            android.view.View r14 = r13.f755i
            if (r14 == 0) goto L_0x016d
            goto L_0x0179
        L_0x016d:
            i.b.p.i.ListMenuPresenter r14 = r13.f757k
            android.widget.ListAdapter r14 = r14.a()
            int r14 = r14.getCount()
            if (r14 <= 0) goto L_0x017b
        L_0x0179:
            r14 = 1
            goto L_0x017c
        L_0x017b:
            r14 = 0
        L_0x017c:
            if (r14 != 0) goto L_0x017f
            goto L_0x01da
        L_0x017f:
            android.view.View r14 = r13.h
            android.view.ViewGroup$LayoutParams r14 = r14.getLayoutParams()
            if (r14 != 0) goto L_0x018c
            android.view.ViewGroup$LayoutParams r14 = new android.view.ViewGroup$LayoutParams
            r14.<init>(r3, r3)
        L_0x018c:
            int r4 = r13.b
            android.view.ViewGroup r5 = r13.g
            r5.setBackgroundResource(r4)
            android.view.View r4 = r13.h
            android.view.ViewParent r4 = r4.getParent()
            boolean r5 = r4 instanceof android.view.ViewGroup
            if (r5 == 0) goto L_0x01a4
            android.view.ViewGroup r4 = (android.view.ViewGroup) r4
            android.view.View r5 = r13.h
            r4.removeView(r5)
        L_0x01a4:
            android.view.ViewGroup r4 = r13.g
            android.view.View r5 = r13.h
            r4.addView(r5, r14)
            android.view.View r14 = r13.h
            boolean r14 = r14.hasFocus()
            if (r14 != 0) goto L_0x01b8
            android.view.View r14 = r13.h
            r14.requestFocus()
        L_0x01b8:
            r5 = -2
        L_0x01b9:
            r13.f760n = r2
            android.view.WindowManager$LayoutParams r14 = new android.view.WindowManager$LayoutParams
            r6 = -2
            int r7 = r13.d
            int r8 = r13.f753e
            r9 = 1002(0x3ea, float:1.404E-42)
            r10 = 8519680(0x820000, float:1.1938615E-38)
            r11 = -3
            r4 = r14
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            int r2 = r13.c
            r14.gravity = r2
            int r2 = r13.f754f
            r14.windowAnimations = r2
            android.view.ViewGroup r2 = r13.g
            r0.addView(r2, r14)
            r13.f761o = r1
        L_0x01da:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void");
    }

    public int f(int i2) {
        boolean z2;
        boolean z3;
        ActionBarContextView actionBarContextView = this.f747p;
        int i3 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f747p.getLayoutParams();
            boolean z4 = true;
            if (this.f747p.isShown()) {
                if (this.X == null) {
                    this.X = new Rect();
                    this.Y = new Rect();
                }
                Rect rect = this.X;
                Rect rect2 = this.Y;
                rect.set(0, i2, 0, 0);
                ViewUtils.a(this.v, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    View view = this.x;
                    if (view == null) {
                        View view2 = new View(this.f738e);
                        this.x = view2;
                        view2.setBackgroundColor(this.f738e.getResources().getColor(i.b.c.abc_input_method_navigation_guard));
                        this.v.addView(this.x, -1, new ViewGroup.LayoutParams(-1, i2));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.x.setLayoutParams(layoutParams);
                        }
                    }
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (this.x == null) {
                    z4 = false;
                }
                if (!this.C && z4) {
                    i2 = 0;
                }
                boolean z5 = z4;
                z4 = z3;
                z2 = z5;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z2 = false;
            } else {
                z2 = false;
                z4 = false;
            }
            if (z4) {
                this.f747p.setLayoutParams(marginLayoutParams);
            }
        }
        View view3 = this.x;
        if (view3 != null) {
            if (!z2) {
                i3 = 8;
            }
            view3.setVisibility(i3);
        }
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
     arg types: [i.b.k.AppCompatDelegateImpl$j, int]
     candidates:
      i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
      i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
    public void a(MenuBuilder menuBuilder) {
        DecorContentParent decorContentParent = this.f743l;
        if (decorContentParent == null || !decorContentParent.g() || (ViewConfiguration.get(this.f738e).hasPermanentMenuKey() && !this.f743l.a())) {
            j d2 = d(0);
            d2.f763q = true;
            a(d2, false);
            a(d2, (KeyEvent) null);
            return;
        }
        Window.Callback i2 = i();
        if (this.f743l.b()) {
            this.f743l.d();
            if (!this.M) {
                i2.onPanelClosed(108, d(0).f756j);
            }
        } else if (i2 != null && !this.M) {
            if (this.T && (1 & this.U) != 0) {
                this.f739f.getDecorView().removeCallbacks(this.V);
                this.V.run();
            }
            j d3 = d(0);
            MenuBuilder menuBuilder2 = d3.f756j;
            if (menuBuilder2 != null && !d3.f764r && i2.onPreparePanel(0, d3.f755i, menuBuilder2)) {
                i2.onMenuOpened(108, d3.f756j);
                this.f743l.e();
            }
        }
    }

    public void a(j jVar, boolean z2) {
        ViewGroup viewGroup;
        DecorContentParent decorContentParent;
        if (!z2 || jVar.a != 0 || (decorContentParent = this.f743l) == null || !decorContentParent.b()) {
            WindowManager windowManager = (WindowManager) this.f738e.getSystemService("window");
            if (!(windowManager == null || !jVar.f761o || (viewGroup = jVar.g) == null)) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    a(jVar.a, jVar, null);
                }
            }
            jVar.f759m = false;
            jVar.f760n = false;
            jVar.f761o = false;
            jVar.h = null;
            jVar.f763q = true;
            if (this.H == jVar) {
                this.H = null;
                return;
            }
            return;
        }
        b(jVar.f756j);
    }

    public void a(int i2, j jVar, Menu menu) {
        if (menu == null) {
            if (jVar == null && i2 >= 0) {
                j[] jVarArr = this.G;
                if (i2 < jVarArr.length) {
                    jVar = jVarArr[i2];
                }
            }
            if (jVar != null) {
                menu = jVar.f756j;
            }
        }
        if ((jVar == null || jVar.f761o) && !this.M) {
            this.g.b.onPanelClosed(i2, menu);
        }
    }

    public j a(Menu menu) {
        j[] jVarArr = this.G;
        int length = jVarArr != null ? jVarArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            j jVar = jVarArr[i2];
            if (jVar != null && jVar.f756j == menu) {
                return jVar;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void
     arg types: [i.b.k.AppCompatDelegateImpl$j, int]
     candidates:
      i.b.k.AppCompatDelegateImpl.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, android.view.KeyEvent):void
      i.b.k.AppCompatDelegateImpl.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegate.a(android.app.Activity, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.app.Dialog, i.b.k.AppCompatCallback):i.b.k.AppCompatDelegate
      i.b.k.AppCompatDelegate.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      i.b.p.i.MenuBuilder.a.a(i.b.p.i.MenuBuilder, android.view.MenuItem):boolean
      i.b.k.AppCompatDelegateImpl.a(i.b.k.AppCompatDelegateImpl$j, boolean):void */
    public final boolean a(j jVar, int i2, KeyEvent keyEvent, int i3) {
        MenuBuilder menuBuilder;
        boolean z2 = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((jVar.f759m || b(jVar, keyEvent)) && (menuBuilder = jVar.f756j) != null) {
            z2 = menuBuilder.performShortcut(i2, keyEvent, i3);
        }
        if (z2 && (i3 & 1) == 0 && this.f743l == null) {
            a(jVar, true);
        }
        return z2;
    }

    public boolean a(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            i2 = 108;
        } else if (i2 == 9) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            i2 = 109;
        }
        if (this.E && i2 == 108) {
            return false;
        }
        if (this.A && i2 == 1) {
            this.A = false;
        }
        if (i2 == 1) {
            l();
            this.E = true;
            return true;
        } else if (i2 == 2) {
            l();
            this.y = true;
            return true;
        } else if (i2 == 5) {
            l();
            this.z = true;
            return true;
        } else if (i2 == 10) {
            l();
            this.C = true;
            return true;
        } else if (i2 == 108) {
            l();
            this.A = true;
            return true;
        } else if (i2 != 109) {
            return this.f739f.requestFeature(i2);
        } else {
            l();
            this.B = true;
            return true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:112:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x01d0  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0255  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x025d  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0266  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x0279  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(boolean r13) {
        /*
            r12 = this;
            boolean r0 = r12.M
            r1 = 0
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            int r0 = r12.N
            r2 = -100
            if (r0 == r2) goto L_0x000d
            goto L_0x000f
        L_0x000d:
            r0 = -100
        L_0x000f:
            r3 = -1
            r4 = 3
            r5 = 2
            r6 = 23
            r7 = 1
            if (r0 == r2) goto L_0x005c
            if (r0 == r3) goto L_0x005b
            if (r0 == 0) goto L_0x003d
            if (r0 == r7) goto L_0x005b
            if (r0 == r5) goto L_0x005b
            if (r0 != r4) goto L_0x0035
            i.b.k.AppCompatDelegateImpl$g r2 = r12.S
            if (r2 != 0) goto L_0x002e
            i.b.k.AppCompatDelegateImpl$f r2 = new i.b.k.AppCompatDelegateImpl$f
            android.content.Context r3 = r12.f738e
            r2.<init>(r3)
            r12.S = r2
        L_0x002e:
            i.b.k.AppCompatDelegateImpl$g r2 = r12.S
            int r3 = r2.c()
            goto L_0x005c
        L_0x0035:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate."
            r13.<init>(r0)
            throw r13
        L_0x003d:
            int r2 = android.os.Build.VERSION.SDK_INT
            if (r2 < r6) goto L_0x0052
            android.content.Context r2 = r12.f738e
            java.lang.Class<android.app.UiModeManager> r8 = android.app.UiModeManager.class
            java.lang.Object r2 = r2.getSystemService(r8)
            android.app.UiModeManager r2 = (android.app.UiModeManager) r2
            int r2 = r2.getNightMode()
            if (r2 != 0) goto L_0x0052
            goto L_0x005c
        L_0x0052:
            i.b.k.AppCompatDelegateImpl$g r2 = r12.h()
            int r3 = r2.c()
            goto L_0x005c
        L_0x005b:
            r3 = r0
        L_0x005c:
            android.content.Context r2 = r12.f738e
            android.content.Context r2 = r2.getApplicationContext()
            android.content.res.Resources r2 = r2.getResources()
            android.content.res.Configuration r2 = r2.getConfiguration()
            int r2 = r2.uiMode
            r2 = r2 & 48
            if (r3 == r7) goto L_0x0077
            if (r3 == r5) goto L_0x0074
            r3 = r2
            goto L_0x0079
        L_0x0074:
            r3 = 32
            goto L_0x0079
        L_0x0077:
            r3 = 16
        L_0x0079:
            boolean r5 = r12.Q
            java.lang.String r8 = "AppCompatDelegate"
            if (r5 != 0) goto L_0x00b6
            java.lang.Object r5 = r12.d
            boolean r5 = r5 instanceof android.app.Activity
            if (r5 == 0) goto L_0x00b6
            android.content.Context r5 = r12.f738e
            android.content.pm.PackageManager r5 = r5.getPackageManager()
            if (r5 != 0) goto L_0x008f
            r5 = 0
            goto L_0x00ba
        L_0x008f:
            android.content.ComponentName r9 = new android.content.ComponentName     // Catch:{ NameNotFoundException -> 0x00ae }
            android.content.Context r10 = r12.f738e     // Catch:{ NameNotFoundException -> 0x00ae }
            java.lang.Object r11 = r12.d     // Catch:{ NameNotFoundException -> 0x00ae }
            java.lang.Class r11 = r11.getClass()     // Catch:{ NameNotFoundException -> 0x00ae }
            r9.<init>(r10, r11)     // Catch:{ NameNotFoundException -> 0x00ae }
            android.content.pm.ActivityInfo r5 = r5.getActivityInfo(r9, r1)     // Catch:{ NameNotFoundException -> 0x00ae }
            if (r5 == 0) goto L_0x00aa
            int r5 = r5.configChanges     // Catch:{ NameNotFoundException -> 0x00ae }
            r5 = r5 & 512(0x200, float:7.175E-43)
            if (r5 == 0) goto L_0x00aa
            r5 = 1
            goto L_0x00ab
        L_0x00aa:
            r5 = 0
        L_0x00ab:
            r12.P = r5     // Catch:{ NameNotFoundException -> 0x00ae }
            goto L_0x00b6
        L_0x00ae:
            r5 = move-exception
            java.lang.String r9 = "Exception while getting ActivityInfo"
            android.util.Log.d(r8, r9, r5)
            r12.P = r1
        L_0x00b6:
            r12.Q = r7
            boolean r5 = r12.P
        L_0x00ba:
            boolean r9 = i.b.k.AppCompatDelegateImpl.e0
            if (r9 != 0) goto L_0x00c0
            if (r3 == r2) goto L_0x00e7
        L_0x00c0:
            if (r5 != 0) goto L_0x00e7
            boolean r2 = r12.J
            if (r2 != 0) goto L_0x00e7
            java.lang.Object r2 = r12.d
            boolean r2 = r2 instanceof android.view.ContextThemeWrapper
            if (r2 == 0) goto L_0x00e7
            android.content.res.Configuration r2 = new android.content.res.Configuration
            r2.<init>()
            int r9 = r2.uiMode
            r9 = r9 & -49
            r9 = r9 | r3
            r2.uiMode = r9
            java.lang.Object r9 = r12.d     // Catch:{ IllegalStateException -> 0x00e1 }
            android.view.ContextThemeWrapper r9 = (android.view.ContextThemeWrapper) r9     // Catch:{ IllegalStateException -> 0x00e1 }
            r9.applyOverrideConfiguration(r2)     // Catch:{ IllegalStateException -> 0x00e1 }
            r1 = 1
            goto L_0x00e7
        L_0x00e1:
            r2 = move-exception
            java.lang.String r9 = "updateForNightMode. Calling applyOverrideConfiguration() failed with an exception. Will fall back to using Resources.updateConfiguration()"
            android.util.Log.e(r8, r9, r2)
        L_0x00e7:
            android.content.Context r2 = r12.f738e
            android.content.res.Resources r2 = r2.getResources()
            android.content.res.Configuration r2 = r2.getConfiguration()
            int r2 = r2.uiMode
            r2 = r2 & 48
            if (r1 != 0) goto L_0x010d
            if (r2 == r3) goto L_0x010d
            if (r13 == 0) goto L_0x010d
            if (r5 != 0) goto L_0x010d
            boolean r13 = r12.J
            if (r13 == 0) goto L_0x010d
            java.lang.Object r13 = r12.d
            boolean r8 = r13 instanceof android.app.Activity
            if (r8 == 0) goto L_0x010d
            android.app.Activity r13 = (android.app.Activity) r13
            i.h.d.ActivityCompat.b(r13)
            r1 = 1
        L_0x010d:
            r13 = 0
            if (r1 != 0) goto L_0x0244
            if (r2 == r3) goto L_0x0244
            android.content.Context r1 = r12.f738e
            android.content.res.Resources r1 = r1.getResources()
            android.content.res.Configuration r2 = new android.content.res.Configuration
            android.content.res.Configuration r8 = r1.getConfiguration()
            r2.<init>(r8)
            android.content.res.Configuration r8 = r1.getConfiguration()
            int r8 = r8.uiMode
            r8 = r8 & -49
            r3 = r3 | r8
            r2.uiMode = r3
            r1.updateConfiguration(r2, r13)
            int r3 = android.os.Build.VERSION.SDK_INT
            r8 = 26
            if (r3 >= r8) goto L_0x01ff
            r8 = 28
            if (r3 < r8) goto L_0x013b
            goto L_0x01ff
        L_0x013b:
            r8 = 24
            java.lang.String r9 = "mDrawableCache"
            java.lang.String r10 = "ResourcesFlusher"
            if (r3 < r8) goto L_0x01a3
            boolean r3 = i.b.k.ResourcesFlusher.h
            if (r3 != 0) goto L_0x015d
            java.lang.Class<android.content.res.Resources> r3 = android.content.res.Resources.class
            java.lang.String r8 = "mResourcesImpl"
            java.lang.reflect.Field r3 = r3.getDeclaredField(r8)     // Catch:{ NoSuchFieldException -> 0x0155 }
            i.b.k.ResourcesFlusher.g = r3     // Catch:{ NoSuchFieldException -> 0x0155 }
            r3.setAccessible(r7)     // Catch:{ NoSuchFieldException -> 0x0155 }
            goto L_0x015b
        L_0x0155:
            r3 = move-exception
            java.lang.String r8 = "Could not retrieve Resources#mResourcesImpl field"
            android.util.Log.e(r10, r8, r3)
        L_0x015b:
            i.b.k.ResourcesFlusher.h = r7
        L_0x015d:
            java.lang.reflect.Field r3 = i.b.k.ResourcesFlusher.g
            if (r3 != 0) goto L_0x0163
            goto L_0x01ff
        L_0x0163:
            java.lang.Object r1 = r3.get(r1)     // Catch:{ IllegalAccessException -> 0x0168 }
            goto L_0x016f
        L_0x0168:
            r1 = move-exception
            java.lang.String r3 = "Could not retrieve value from Resources#mResourcesImpl"
            android.util.Log.e(r10, r3, r1)
            r1 = r13
        L_0x016f:
            if (r1 != 0) goto L_0x0173
            goto L_0x01ff
        L_0x0173:
            boolean r3 = i.b.k.ResourcesFlusher.b
            if (r3 != 0) goto L_0x018d
            java.lang.Class r3 = r1.getClass()     // Catch:{ NoSuchFieldException -> 0x0185 }
            java.lang.reflect.Field r3 = r3.getDeclaredField(r9)     // Catch:{ NoSuchFieldException -> 0x0185 }
            i.b.k.ResourcesFlusher.a = r3     // Catch:{ NoSuchFieldException -> 0x0185 }
            r3.setAccessible(r7)     // Catch:{ NoSuchFieldException -> 0x0185 }
            goto L_0x018b
        L_0x0185:
            r3 = move-exception
            java.lang.String r8 = "Could not retrieve ResourcesImpl#mDrawableCache field"
            android.util.Log.e(r10, r8, r3)
        L_0x018b:
            i.b.k.ResourcesFlusher.b = r7
        L_0x018d:
            java.lang.reflect.Field r3 = i.b.k.ResourcesFlusher.a
            if (r3 == 0) goto L_0x019c
            java.lang.Object r1 = r3.get(r1)     // Catch:{ IllegalAccessException -> 0x0196 }
            goto L_0x019d
        L_0x0196:
            r1 = move-exception
            java.lang.String r3 = "Could not retrieve value from ResourcesImpl#mDrawableCache"
            android.util.Log.e(r10, r3, r1)
        L_0x019c:
            r1 = r13
        L_0x019d:
            if (r1 == 0) goto L_0x01ff
            i.b.k.ResourcesFlusher.c(r1)
            goto L_0x01ff
        L_0x01a3:
            java.lang.String r8 = "Could not retrieve Resources#mDrawableCache field"
            java.lang.String r11 = "Could not retrieve value from Resources#mDrawableCache"
            if (r3 < r6) goto L_0x01d4
            boolean r3 = i.b.k.ResourcesFlusher.b
            if (r3 != 0) goto L_0x01bf
            java.lang.Class<android.content.res.Resources> r3 = android.content.res.Resources.class
            java.lang.reflect.Field r3 = r3.getDeclaredField(r9)     // Catch:{ NoSuchFieldException -> 0x01b9 }
            i.b.k.ResourcesFlusher.a = r3     // Catch:{ NoSuchFieldException -> 0x01b9 }
            r3.setAccessible(r7)     // Catch:{ NoSuchFieldException -> 0x01b9 }
            goto L_0x01bd
        L_0x01b9:
            r3 = move-exception
            android.util.Log.e(r10, r8, r3)
        L_0x01bd:
            i.b.k.ResourcesFlusher.b = r7
        L_0x01bf:
            java.lang.reflect.Field r3 = i.b.k.ResourcesFlusher.a
            if (r3 == 0) goto L_0x01cc
            java.lang.Object r1 = r3.get(r1)     // Catch:{ IllegalAccessException -> 0x01c8 }
            goto L_0x01cd
        L_0x01c8:
            r1 = move-exception
            android.util.Log.e(r10, r11, r1)
        L_0x01cc:
            r1 = r13
        L_0x01cd:
            if (r1 != 0) goto L_0x01d0
            goto L_0x01ff
        L_0x01d0:
            i.b.k.ResourcesFlusher.c(r1)
            goto L_0x01ff
        L_0x01d4:
            boolean r3 = i.b.k.ResourcesFlusher.b
            if (r3 != 0) goto L_0x01ea
            java.lang.Class<android.content.res.Resources> r3 = android.content.res.Resources.class
            java.lang.reflect.Field r3 = r3.getDeclaredField(r9)     // Catch:{ NoSuchFieldException -> 0x01e4 }
            i.b.k.ResourcesFlusher.a = r3     // Catch:{ NoSuchFieldException -> 0x01e4 }
            r3.setAccessible(r7)     // Catch:{ NoSuchFieldException -> 0x01e4 }
            goto L_0x01e8
        L_0x01e4:
            r3 = move-exception
            android.util.Log.e(r10, r8, r3)
        L_0x01e8:
            i.b.k.ResourcesFlusher.b = r7
        L_0x01ea:
            java.lang.reflect.Field r3 = i.b.k.ResourcesFlusher.a
            if (r3 == 0) goto L_0x01ff
            java.lang.Object r1 = r3.get(r1)     // Catch:{ IllegalAccessException -> 0x01f5 }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ IllegalAccessException -> 0x01f5 }
            goto L_0x01fa
        L_0x01f5:
            r1 = move-exception
            android.util.Log.e(r10, r11, r1)
            r1 = r13
        L_0x01fa:
            if (r1 == 0) goto L_0x01ff
            r1.clear()
        L_0x01ff:
            int r1 = r12.O
            if (r1 == 0) goto L_0x0217
            android.content.Context r3 = r12.f738e
            r3.setTheme(r1)
            int r1 = android.os.Build.VERSION.SDK_INT
            if (r1 < r6) goto L_0x0217
            android.content.Context r1 = r12.f738e
            android.content.res.Resources$Theme r1 = r1.getTheme()
            int r3 = r12.O
            r1.applyStyle(r3, r7)
        L_0x0217:
            if (r5 == 0) goto L_0x0245
            java.lang.Object r1 = r12.d
            boolean r3 = r1 instanceof android.app.Activity
            if (r3 == 0) goto L_0x0245
            android.app.Activity r1 = (android.app.Activity) r1
            boolean r3 = r1 instanceof i.o.LifecycleOwner
            if (r3 == 0) goto L_0x023c
            r3 = r1
            i.o.LifecycleOwner r3 = (i.o.LifecycleOwner) r3
            i.o.Lifecycle r3 = r3.a()
            i.o.LifecycleRegistry r3 = (i.o.LifecycleRegistry) r3
            i.o.Lifecycle$b r3 = r3.b
            i.o.Lifecycle$b r5 = i.o.Lifecycle.b.STARTED
            boolean r3 = r3.a(r5)
            if (r3 == 0) goto L_0x0245
            r1.onConfigurationChanged(r2)
            goto L_0x0245
        L_0x023c:
            boolean r3 = r12.L
            if (r3 == 0) goto L_0x0245
            r1.onConfigurationChanged(r2)
            goto L_0x0245
        L_0x0244:
            r7 = r1
        L_0x0245:
            if (r7 == 0) goto L_0x0253
            java.lang.Object r1 = r12.d
            boolean r2 = r1 instanceof i.b.k.AppCompatActivity
            if (r2 == 0) goto L_0x0253
            i.b.k.AppCompatActivity r1 = (i.b.k.AppCompatActivity) r1
            if (r1 == 0) goto L_0x0252
            goto L_0x0253
        L_0x0252:
            throw r13
        L_0x0253:
            if (r0 != 0) goto L_0x025d
            i.b.k.AppCompatDelegateImpl$g r13 = r12.h()
            r13.e()
            goto L_0x0264
        L_0x025d:
            i.b.k.AppCompatDelegateImpl$g r13 = r12.R
            if (r13 == 0) goto L_0x0264
            r13.a()
        L_0x0264:
            if (r0 != r4) goto L_0x0279
            i.b.k.AppCompatDelegateImpl$g r13 = r12.S
            if (r13 != 0) goto L_0x0273
            i.b.k.AppCompatDelegateImpl$f r13 = new i.b.k.AppCompatDelegateImpl$f
            android.content.Context r0 = r12.f738e
            r13.<init>(r0)
            r12.S = r13
        L_0x0273:
            i.b.k.AppCompatDelegateImpl$g r13 = r12.S
            r13.e()
            goto L_0x0280
        L_0x0279:
            i.b.k.AppCompatDelegateImpl$g r13 = r12.S
            if (r13 == 0) goto L_0x0280
            r13.a()
        L_0x0280:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDelegateImpl.a(boolean):boolean");
    }
}
