package i.c.a.b;

import i.c.a.b.SafeIterableMap;
import java.util.HashMap;

public class FastSafeIterableMap<K, V> extends SafeIterableMap<K, V> {

    /* renamed from: f  reason: collision with root package name */
    public HashMap<K, SafeIterableMap.c<K, V>> f1049f = new HashMap<>();

    public SafeIterableMap.c<K, V> a(K k2) {
        return this.f1049f.get(k2);
    }

    public V b(K k2, V v) {
        SafeIterableMap.c cVar = this.f1049f.get(k2);
        if (cVar != null) {
            return cVar.c;
        }
        this.f1049f.put(k2, a(k2, v));
        return null;
    }

    public boolean contains(K k2) {
        return this.f1049f.containsKey(k2);
    }

    public V remove(K k2) {
        V remove = super.remove(k2);
        this.f1049f.remove(k2);
        return remove;
    }
}
