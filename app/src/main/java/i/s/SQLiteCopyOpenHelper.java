package i.s;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.core.CodedOutputStream;
import i.b.k.ResourcesFlusher;
import i.s.m.CopyLock;
import i.v.a.SupportSQLiteDatabase;
import i.v.a.SupportSQLiteOpenHelper;
import j.a.a.a.outline;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

public class SQLiteCopyOpenHelper implements SupportSQLiteOpenHelper {
    public final Context a;
    public final String b;
    public final File c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final SupportSQLiteOpenHelper f1438e;

    /* renamed from: f  reason: collision with root package name */
    public DatabaseConfiguration f1439f;
    public boolean g;

    public void a(boolean z) {
        this.f1438e.a(z);
    }

    public String b() {
        return this.f1438e.b();
    }

    public final void c() {
        String b2 = this.f1438e.b();
        File databasePath = this.a.getDatabasePath(b2);
        DatabaseConfiguration databaseConfiguration = this.f1439f;
        CopyLock copyLock = new CopyLock(b2, this.a.getFilesDir(), databaseConfiguration == null || databaseConfiguration.f1418j);
        try {
            copyLock.b.lock();
            if (copyLock.c) {
                FileChannel channel = new FileOutputStream(copyLock.a).getChannel();
                copyLock.d = channel;
                channel.lock();
            }
            if (!databasePath.exists()) {
                a(databasePath);
                copyLock.a();
            } else if (this.f1439f == null) {
                copyLock.a();
            } else {
                try {
                    int a2 = ResourcesFlusher.a(databasePath);
                    if (a2 == this.d) {
                        copyLock.a();
                    } else if (this.f1439f.a(a2, this.d)) {
                        copyLock.a();
                    } else {
                        if (this.a.deleteDatabase(b2)) {
                            try {
                                a(databasePath);
                            } catch (IOException e2) {
                                Log.w("ROOM", "Unable to copy database file.", e2);
                            }
                        } else {
                            Log.w("ROOM", "Failed to delete database file (" + b2 + ") for a copy destructive migration.");
                        }
                        copyLock.a();
                    }
                } catch (IOException e3) {
                    Log.w("ROOM", "Unable to read database version.", e3);
                    copyLock.a();
                }
            }
        } catch (IOException e4) {
            throw new RuntimeException("Unable to copy database file.", e4);
        } catch (IOException e5) {
            throw new IllegalStateException("Unable to grab copy lock.", e5);
        } catch (Throwable th) {
            copyLock.a();
            throw th;
        }
    }

    public synchronized SupportSQLiteDatabase a() {
        if (!this.g) {
            c();
            this.g = true;
        }
        return this.f1438e.a();
    }

    /* JADX INFO: finally extract failed */
    public final void a(File file) {
        ReadableByteChannel readableByteChannel;
        if (this.b != null) {
            readableByteChannel = Channels.newChannel(this.a.getAssets().open(this.b));
        } else if (this.c != null) {
            readableByteChannel = new FileInputStream(this.c).getChannel();
        } else {
            throw new IllegalStateException("copyFromAssetPath and copyFromFile == null!");
        }
        File createTempFile = File.createTempFile("room-copy-helper", ".tmp", this.a.getCacheDir());
        createTempFile.deleteOnExit();
        FileChannel channel = new FileOutputStream(createTempFile).getChannel();
        try {
            if (Build.VERSION.SDK_INT <= 23) {
                InputStream newInputStream = Channels.newInputStream(readableByteChannel);
                OutputStream newOutputStream = Channels.newOutputStream(channel);
                byte[] bArr = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
                while (true) {
                    int read = newInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    newOutputStream.write(bArr, 0, read);
                }
            } else {
                channel.transferFrom(readableByteChannel, 0, RecyclerView.FOREVER_NS);
            }
            channel.force(false);
            readableByteChannel.close();
            channel.close();
            File parentFile = file.getParentFile();
            if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
                StringBuilder a2 = outline.a("Failed to create directories for ");
                a2.append(file.getAbsolutePath());
                throw new IOException(a2.toString());
            } else if (!createTempFile.renameTo(file)) {
                StringBuilder a3 = outline.a("Failed to move intermediate file (");
                a3.append(createTempFile.getAbsolutePath());
                a3.append(") to destination (");
                a3.append(file.getAbsolutePath());
                a3.append(").");
                throw new IOException(a3.toString());
            }
        } catch (Throwable th) {
            readableByteChannel.close();
            channel.close();
            throw th;
        }
    }
}
