package i.f.b;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import i.f.a.h.ConstraintWidget;
import i.f.a.h.Helper;
import java.util.Arrays;

public abstract class ConstraintHelper extends View {
    public int[] b = new int[32];
    public int c;
    public Context d;

    /* renamed from: e  reason: collision with root package name */
    public Helper f1137e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1138f = false;
    public String g;

    public ConstraintHelper(Context context) {
        super(context);
        this.d = context;
        a((AttributeSet) null);
    }

    private void setIds(String str) {
        if (str != null) {
            int i2 = 0;
            while (true) {
                int indexOf = str.indexOf(44, i2);
                if (indexOf == -1) {
                    a(str.substring(i2));
                    return;
                } else {
                    a(str.substring(i2, indexOf));
                    i2 = indexOf + 1;
                }
            }
        }
    }

    public void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, g.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i2 = 0; i2 < indexCount; i2++) {
                int index = obtainStyledAttributes.getIndex(i2);
                if (index == g.ConstraintLayout_Layout_constraint_referenced_ids) {
                    String string = obtainStyledAttributes.getString(index);
                    this.g = string;
                    setIds(string);
                }
            }
        }
    }

    public void a(ConstraintLayout constraintLayout) {
    }

    public void b(ConstraintLayout constraintLayout) {
        if (isInEditMode()) {
            setIds(this.g);
        }
        Helper helper = this.f1137e;
        if (helper != null) {
            helper.l0 = 0;
            for (int i2 = 0; i2 < this.c; i2++) {
                View view = constraintLayout.b.get(this.b[i2]);
                if (view != null) {
                    Helper helper2 = this.f1137e;
                    ConstraintWidget a = constraintLayout.a(super);
                    int i3 = helper2.l0 + 1;
                    ConstraintWidget[] constraintWidgetArr = helper2.k0;
                    if (i3 > constraintWidgetArr.length) {
                        helper2.k0 = (ConstraintWidget[]) Arrays.copyOf(constraintWidgetArr, constraintWidgetArr.length * 2);
                    }
                    ConstraintWidget[] constraintWidgetArr2 = helper2.k0;
                    int i4 = helper2.l0;
                    constraintWidgetArr2[i4] = a;
                    helper2.l0 = i4 + 1;
                }
            }
        }
    }

    public int[] getReferencedIds() {
        return Arrays.copyOf(this.b, this.c);
    }

    public void onDraw(Canvas canvas) {
    }

    public void onMeasure(int i2, int i3) {
        if (this.f1138f) {
            super.onMeasure(i2, i3);
        } else {
            setMeasuredDimension(0, 0);
        }
    }

    public void setReferencedIds(int[] iArr) {
        this.c = 0;
        for (int tag : iArr) {
            setTag(tag, null);
        }
    }

    public void setTag(int i2, Object obj) {
        int i3 = this.c + 1;
        int[] iArr = this.b;
        if (i3 > iArr.length) {
            this.b = Arrays.copyOf(iArr, iArr.length * 2);
        }
        int[] iArr2 = this.b;
        int i4 = this.c;
        iArr2[i4] = i2;
        this.c = i4 + 1;
    }

    public ConstraintHelper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        a(attributeSet);
    }

    public void a() {
        if (this.f1137e != null) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams instanceof ConstraintLayout.a) {
                ((ConstraintLayout.a) layoutParams).k0 = this.f1137e;
            }
        }
    }

    public ConstraintHelper(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = context;
        a(attributeSet);
    }

    public final void a(String str) {
        int i2;
        Object a;
        if (str != null && this.d != null) {
            String trim = str.trim();
            try {
                i2 = f.class.getField(trim).getInt(null);
            } catch (Exception unused) {
                i2 = 0;
            }
            if (i2 == 0) {
                i2 = this.d.getResources().getIdentifier(trim, "id", this.d.getPackageName());
            }
            if (i2 == 0 && isInEditMode() && (getParent() instanceof ConstraintLayout) && (a = ((ConstraintLayout) getParent()).a(0, trim)) != null && (a instanceof Integer)) {
                i2 = ((Integer) a).intValue();
            }
            if (i2 != 0) {
                setTag(i2, null);
                return;
            }
            Log.w("ConstraintHelper", "Could not find id of \"" + trim + "\"");
        }
    }
}
