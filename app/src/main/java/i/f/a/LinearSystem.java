package i.f.a;

import i.f.a.SolverVariable;
import i.f.a.h.ConstraintAnchor;
import java.util.Arrays;
import java.util.HashMap;

public class LinearSystem {

    /* renamed from: p  reason: collision with root package name */
    public static int f1080p = 1000;
    public int a;
    public HashMap<String, g> b;
    public a c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f1081e;

    /* renamed from: f  reason: collision with root package name */
    public ArrayRow[] f1082f;
    public boolean g;
    public boolean[] h;

    /* renamed from: i  reason: collision with root package name */
    public int f1083i;

    /* renamed from: j  reason: collision with root package name */
    public int f1084j;

    /* renamed from: k  reason: collision with root package name */
    public int f1085k;

    /* renamed from: l  reason: collision with root package name */
    public final Cache f1086l;

    /* renamed from: m  reason: collision with root package name */
    public SolverVariable[] f1087m;

    /* renamed from: n  reason: collision with root package name */
    public int f1088n;

    /* renamed from: o  reason: collision with root package name */
    public final a f1089o;

    public interface a {
        void a(SolverVariable solverVariable);
    }

    public LinearSystem() {
        this.a = 0;
        this.b = null;
        this.d = 32;
        this.f1081e = 32;
        this.f1082f = null;
        this.g = false;
        this.h = new boolean[32];
        this.f1083i = 1;
        this.f1084j = 0;
        this.f1085k = 32;
        this.f1087m = new SolverVariable[f1080p];
        this.f1088n = 0;
        this.f1082f = new ArrayRow[32];
        e();
        Cache cache = new Cache();
        this.f1086l = cache;
        this.c = new GoalRow(cache);
        this.f1089o = new ArrayRow(this.f1086l);
    }

    public SolverVariable a(Object obj) {
        SolverVariable solverVariable = null;
        if (obj == null) {
            return null;
        }
        if (this.f1083i + 1 >= this.f1081e) {
            d();
        }
        if (obj instanceof ConstraintAnchor) {
            ConstraintAnchor constraintAnchor = (ConstraintAnchor) obj;
            solverVariable = constraintAnchor.f1108i;
            if (solverVariable == null) {
                constraintAnchor.d();
                solverVariable = constraintAnchor.f1108i;
            }
            int i2 = solverVariable.b;
            if (i2 == -1 || i2 > this.a || this.f1086l.c[i2] == null) {
                if (solverVariable.b != -1) {
                    solverVariable.a();
                }
                int i3 = this.a + 1;
                this.a = i3;
                this.f1083i++;
                solverVariable.b = i3;
                solverVariable.g = SolverVariable.a.UNRESTRICTED;
                this.f1086l.c[i3] = solverVariable;
            }
        }
        return solverVariable;
    }

    public ArrayRow b() {
        ArrayRow a2 = this.f1086l.a.a();
        if (a2 == null) {
            a2 = new ArrayRow(this.f1086l);
        } else {
            a2.a = null;
            a2.d.a();
            a2.b = 0.0f;
            a2.f1079e = false;
        }
        SolverVariable.f1090k++;
        return a2;
    }

    public SolverVariable c() {
        if (this.f1083i + 1 >= this.f1081e) {
            d();
        }
        SolverVariable a2 = a(SolverVariable.a.SLACK, (String) null);
        int i2 = this.a + 1;
        this.a = i2;
        this.f1083i++;
        a2.b = i2;
        this.f1086l.c[i2] = a2;
        return a2;
    }

    public final void d() {
        int i2 = this.d * 2;
        this.d = i2;
        this.f1082f = (ArrayRow[]) Arrays.copyOf(this.f1082f, i2);
        Cache cache = this.f1086l;
        cache.c = (SolverVariable[]) Arrays.copyOf(cache.c, this.d);
        int i3 = this.d;
        this.h = new boolean[i3];
        this.f1081e = i3;
        this.f1085k = i3;
    }

    public final void e() {
        int i2 = 0;
        while (true) {
            ArrayRow[] arrayRowArr = this.f1082f;
            if (i2 < arrayRowArr.length) {
                ArrayRow arrayRow = arrayRowArr[i2];
                if (arrayRow != null) {
                    this.f1086l.a.a(arrayRow);
                }
                this.f1082f[i2] = null;
                i2++;
            } else {
                return;
            }
        }
    }

    public void f() {
        Cache cache;
        int i2 = 0;
        while (true) {
            cache = this.f1086l;
            SolverVariable[] solverVariableArr = cache.c;
            if (i2 >= solverVariableArr.length) {
                break;
            }
            SolverVariable solverVariable = solverVariableArr[i2];
            if (solverVariable != null) {
                solverVariable.a();
            }
            i2++;
        }
        Pools<g> pools = cache.b;
        SolverVariable[] solverVariableArr2 = this.f1087m;
        int i3 = this.f1088n;
        if (pools != null) {
            if (i3 > solverVariableArr2.length) {
                i3 = solverVariableArr2.length;
            }
            for (int i4 = 0; i4 < i3; i4++) {
                SolverVariable solverVariable2 = solverVariableArr2[i4];
                int i5 = pools.b;
                Object[] objArr = pools.a;
                if (i5 < objArr.length) {
                    objArr[i5] = solverVariable2;
                    pools.b = i5 + 1;
                }
            }
            this.f1088n = 0;
            Arrays.fill(this.f1086l.c, (Object) null);
            HashMap<String, g> hashMap = this.b;
            if (hashMap != null) {
                hashMap.clear();
            }
            this.a = 0;
            ArrayRow arrayRow = (ArrayRow) this.c;
            arrayRow.d.a();
            arrayRow.a = null;
            arrayRow.b = 0.0f;
            this.f1083i = 1;
            for (int i6 = 0; i6 < this.f1084j; i6++) {
                this.f1082f[i6].c = false;
            }
            e();
            this.f1084j = 0;
            return;
        }
        throw null;
    }

    public int b(Object obj) {
        SolverVariable solverVariable = ((ConstraintAnchor) obj).f1108i;
        if (solverVariable != null) {
            return (int) (solverVariable.f1091e + 0.5f);
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float
     arg types: [i.f.a.SolverVariable, int]
     candidates:
      i.f.a.ArrayLinkedVariables.a(boolean[], i.f.a.SolverVariable):i.f.a.SolverVariable
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, float):void
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float */
    public final void c(ArrayRow arrayRow) {
        if (this.f1084j > 0) {
            ArrayLinkedVariables arrayLinkedVariables = arrayRow.d;
            ArrayRow[] arrayRowArr = this.f1082f;
            int i2 = arrayLinkedVariables.f1076i;
            loop0:
            while (true) {
                int i3 = 0;
                int i4 = 0;
                while (i2 != -1 && i4 < arrayLinkedVariables.a) {
                    SolverVariable solverVariable = arrayLinkedVariables.c.c[arrayLinkedVariables.f1075f[i2]];
                    if (solverVariable.c != -1) {
                        float f2 = arrayLinkedVariables.h[i2];
                        arrayLinkedVariables.a(solverVariable, true);
                        ArrayRow arrayRow2 = arrayRowArr[solverVariable.c];
                        if (!arrayRow2.f1079e) {
                            ArrayLinkedVariables arrayLinkedVariables2 = arrayRow2.d;
                            int i5 = arrayLinkedVariables2.f1076i;
                            while (i5 != -1 && i3 < arrayLinkedVariables2.a) {
                                arrayLinkedVariables.a(arrayLinkedVariables.c.c[arrayLinkedVariables2.f1075f[i5]], arrayLinkedVariables2.h[i5] * f2, true);
                                i5 = arrayLinkedVariables2.g[i5];
                                i3++;
                            }
                        }
                        arrayRow.b = (arrayRow2.b * f2) + arrayRow.b;
                        arrayRow2.a.b(arrayRow);
                        i2 = arrayLinkedVariables.f1076i;
                    } else {
                        i2 = arrayLinkedVariables.g[i2];
                        i4++;
                    }
                }
            }
            if (arrayRow.d.a == 0) {
                arrayRow.f1079e = true;
            }
        }
    }

    public final void b(ArrayRow arrayRow) {
        ArrayRow[] arrayRowArr = this.f1082f;
        int i2 = this.f1084j;
        if (arrayRowArr[i2] != null) {
            this.f1086l.a.a(arrayRowArr[i2]);
        }
        ArrayRow[] arrayRowArr2 = this.f1082f;
        int i3 = this.f1084j;
        arrayRowArr2[i3] = arrayRow;
        SolverVariable solverVariable = arrayRow.a;
        solverVariable.c = i3;
        this.f1084j = i3 + 1;
        solverVariable.c(arrayRow);
    }

    public SolverVariable a(int i2, String str) {
        if (this.f1083i + 1 >= this.f1081e) {
            d();
        }
        SolverVariable a2 = a(SolverVariable.a.ERROR, str);
        int i3 = this.a + 1;
        this.a = i3;
        this.f1083i++;
        a2.b = i3;
        a2.d = i2;
        this.f1086l.c[i3] = a2;
        this.c.a(a2);
        return a2;
    }

    public final int b(a aVar) {
        boolean z;
        for (int i2 = 0; i2 < this.f1083i; i2++) {
            this.h[i2] = false;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            i3++;
            if (i3 >= this.f1083i * 2) {
                return i3;
            }
            ArrayRow arrayRow = (ArrayRow) aVar;
            SolverVariable solverVariable = arrayRow.a;
            if (solverVariable != null) {
                this.h[solverVariable.b] = true;
            }
            SolverVariable a2 = arrayRow.d.a(this.h, (SolverVariable) null);
            if (a2 != null) {
                boolean[] zArr = this.h;
                int i4 = a2.b;
                if (zArr[i4]) {
                    return i3;
                }
                zArr[i4] = true;
            }
            if (a2 != null) {
                float f2 = Float.MAX_VALUE;
                int i5 = -1;
                for (int i6 = 0; i6 < this.f1084j; i6++) {
                    ArrayRow arrayRow2 = this.f1082f[i6];
                    if (arrayRow2.a.g != SolverVariable.a.UNRESTRICTED && !arrayRow2.f1079e) {
                        ArrayLinkedVariables arrayLinkedVariables = arrayRow2.d;
                        int i7 = arrayLinkedVariables.f1076i;
                        if (i7 != -1) {
                            int i8 = 0;
                            while (true) {
                                if (i7 == -1 || i8 >= arrayLinkedVariables.a) {
                                    break;
                                } else if (arrayLinkedVariables.f1075f[i7] == a2.b) {
                                    z = true;
                                    break;
                                } else {
                                    i7 = arrayLinkedVariables.g[i7];
                                    i8++;
                                }
                            }
                        }
                        z = false;
                        if (z) {
                            float a3 = arrayRow2.d.a(a2);
                            if (a3 < 0.0f) {
                                float f3 = (-arrayRow2.b) / a3;
                                if (f3 < f2) {
                                    i5 = i6;
                                    f2 = f3;
                                }
                            }
                        }
                    }
                }
                if (i5 > -1) {
                    ArrayRow arrayRow3 = this.f1082f[i5];
                    arrayRow3.a.c = -1;
                    arrayRow3.b(a2);
                    SolverVariable solverVariable2 = arrayRow3.a;
                    solverVariable2.c = i5;
                    solverVariable2.c(arrayRow3);
                }
            }
            z2 = true;
        }
        return i3;
    }

    public final SolverVariable a(SolverVariable.a aVar, String str) {
        SolverVariable a2 = this.f1086l.b.a();
        if (a2 == null) {
            a2 = new SolverVariable(aVar);
            a2.g = aVar;
        } else {
            a2.a();
            a2.g = aVar;
        }
        int i2 = this.f1088n;
        int i3 = f1080p;
        if (i2 >= i3) {
            int i4 = i3 * 2;
            f1080p = i4;
            this.f1087m = (SolverVariable[]) Arrays.copyOf(this.f1087m, i4);
        }
        SolverVariable[] solverVariableArr = this.f1087m;
        int i5 = this.f1088n;
        this.f1088n = i5 + 1;
        solverVariableArr[i5] = a2;
        return a2;
    }

    public void c(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        ArrayRow b2 = b();
        SolverVariable c2 = c();
        c2.d = 0;
        b2.b(solverVariable, solverVariable2, c2, i2);
        if (i3 != 6) {
            b2.d.a(a(i3, (String) null), (float) ((int) (b2.d.a(c2) * -1.0f)));
        }
        a(b2);
    }

    public void a(a aVar) {
        float f2;
        boolean z;
        c((ArrayRow) aVar);
        int i2 = 0;
        while (true) {
            f2 = 0.0f;
            if (i2 >= this.f1084j) {
                z = false;
                break;
            }
            ArrayRow[] arrayRowArr = this.f1082f;
            if (arrayRowArr[i2].a.g != SolverVariable.a.UNRESTRICTED && arrayRowArr[i2].b < 0.0f) {
                z = true;
                break;
            }
            i2++;
        }
        if (z) {
            boolean z2 = false;
            int i3 = 0;
            while (!z2) {
                i3++;
                float f3 = Float.MAX_VALUE;
                int i4 = 0;
                int i5 = -1;
                int i6 = -1;
                int i7 = 0;
                while (i4 < this.f1084j) {
                    ArrayRow arrayRow = this.f1082f[i4];
                    if (arrayRow.a.g != SolverVariable.a.UNRESTRICTED && !arrayRow.f1079e && arrayRow.b < f2) {
                        int i8 = 1;
                        while (i8 < this.f1083i) {
                            SolverVariable solverVariable = this.f1086l.c[i8];
                            float a2 = arrayRow.d.a(solverVariable);
                            if (a2 > f2) {
                                for (int i9 = 0; i9 < 7; i9++) {
                                    float f4 = solverVariable.f1092f[i9] / a2;
                                    if ((f4 < f3 && i9 == i7) || i9 > i7) {
                                        i7 = i9;
                                        f3 = f4;
                                        i5 = i4;
                                        i6 = i8;
                                    }
                                }
                            }
                            i8++;
                            f2 = 0.0f;
                        }
                    }
                    i4++;
                    f2 = 0.0f;
                }
                if (i5 != -1) {
                    ArrayRow arrayRow2 = this.f1082f[i5];
                    arrayRow2.a.c = -1;
                    arrayRow2.b(this.f1086l.c[i6]);
                    SolverVariable solverVariable2 = arrayRow2.a;
                    solverVariable2.c = i5;
                    solverVariable2.c(arrayRow2);
                } else {
                    z2 = true;
                }
                if (i3 > this.f1083i / 2) {
                    z2 = true;
                }
                f2 = 0.0f;
            }
        }
        b(aVar);
        a();
    }

    public void b(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        ArrayRow b2 = b();
        SolverVariable c2 = c();
        c2.d = 0;
        b2.a(solverVariable, solverVariable2, c2, i2);
        if (i3 != 6) {
            b2.d.a(a(i3, (String) null), (float) ((int) (b2.d.a(c2) * -1.0f)));
        }
        a(b2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:121:0x00f3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(i.f.a.ArrayRow r19) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            if (r1 != 0) goto L_0x0007
            return
        L_0x0007:
            int r2 = r0.f1084j
            r3 = 1
            int r2 = r2 + r3
            int r4 = r0.f1085k
            if (r2 >= r4) goto L_0x0016
            int r2 = r0.f1083i
            int r2 = r2 + r3
            int r4 = r0.f1081e
            if (r2 < r4) goto L_0x0019
        L_0x0016:
            r18.d()
        L_0x0019:
            boolean r2 = r1.f1079e
            if (r2 != 0) goto L_0x01a4
            r18.c(r19)
            i.f.a.SolverVariable r2 = r1.a
            r5 = 0
            if (r2 != 0) goto L_0x0033
            float r2 = r1.b
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 != 0) goto L_0x0033
            i.f.a.ArrayLinkedVariables r2 = r1.d
            int r2 = r2.a
            if (r2 != 0) goto L_0x0033
            r2 = 1
            goto L_0x0034
        L_0x0033:
            r2 = 0
        L_0x0034:
            if (r2 == 0) goto L_0x0037
            return
        L_0x0037:
            float r2 = r1.b
            r6 = -1
            int r7 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r7 >= 0) goto L_0x005e
            r7 = -1082130432(0xffffffffbf800000, float:-1.0)
            float r2 = r2 * r7
            r1.b = r2
            i.f.a.ArrayLinkedVariables r2 = r1.d
            int r8 = r2.f1076i
            r9 = 0
        L_0x0049:
            if (r8 == r6) goto L_0x005e
            int r10 = r2.a
            if (r9 >= r10) goto L_0x005e
            float[] r10 = r2.h
            r11 = r10[r8]
            float r11 = r11 * r7
            r10[r8] = r11
            int[] r10 = r2.g
            r8 = r10[r8]
            int r9 = r9 + 1
            goto L_0x0049
        L_0x005e:
            i.f.a.ArrayLinkedVariables r2 = r1.d
            int r7 = r2.f1076i
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
        L_0x0069:
            if (r7 == r6) goto L_0x00fc
            int r4 = r2.a
            if (r9 >= r4) goto L_0x00fc
            float[] r4 = r2.h
            r16 = r4[r7]
            r17 = 981668463(0x3a83126f, float:0.001)
            i.f.a.Cache r6 = r2.c
            i.f.a.SolverVariable[] r6 = r6.c
            int[] r8 = r2.f1075f
            r8 = r8[r7]
            r6 = r6[r8]
            int r8 = (r16 > r5 ? 1 : (r16 == r5 ? 0 : -1))
            if (r8 >= 0) goto L_0x0093
            r8 = -1165815185(0xffffffffba83126f, float:-0.001)
            int r8 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r8 <= 0) goto L_0x00a0
            r4[r7] = r5
            i.f.a.ArrayRow r4 = r2.b
            r6.b(r4)
            goto L_0x009e
        L_0x0093:
            int r8 = (r16 > r17 ? 1 : (r16 == r17 ? 0 : -1))
            if (r8 >= 0) goto L_0x00a0
            r4[r7] = r5
            i.f.a.ArrayRow r4 = r2.b
            r6.b(r4)
        L_0x009e:
            r16 = 0
        L_0x00a0:
            int r4 = (r16 > r5 ? 1 : (r16 == r5 ? 0 : -1))
            if (r4 == 0) goto L_0x00f3
            i.f.a.SolverVariable$a r4 = r6.g
            i.f.a.SolverVariable$a r8 = i.f.a.SolverVariable.a.UNRESTRICTED
            if (r4 != r8) goto L_0x00cc
            if (r11 != 0) goto L_0x00b2
            boolean r4 = r2.b(r6)
        L_0x00b0:
            r13 = r4
            goto L_0x00bb
        L_0x00b2:
            int r4 = (r12 > r16 ? 1 : (r12 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x00bf
            boolean r4 = r2.b(r6)
            goto L_0x00b0
        L_0x00bb:
            r11 = r6
            r12 = r16
            goto L_0x00f3
        L_0x00bf:
            if (r13 != 0) goto L_0x00f3
            boolean r4 = r2.b(r6)
            if (r4 == 0) goto L_0x00f3
            r11 = r6
            r12 = r16
            r13 = 1
            goto L_0x00f3
        L_0x00cc:
            if (r11 != 0) goto L_0x00f3
            int r4 = (r16 > r5 ? 1 : (r16 == r5 ? 0 : -1))
            if (r4 >= 0) goto L_0x00f3
            if (r10 != 0) goto L_0x00da
            boolean r4 = r2.b(r6)
        L_0x00d8:
            r15 = r4
            goto L_0x00e3
        L_0x00da:
            int r4 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x00e7
            boolean r4 = r2.b(r6)
            goto L_0x00d8
        L_0x00e3:
            r10 = r6
            r14 = r16
            goto L_0x00f3
        L_0x00e7:
            if (r15 != 0) goto L_0x00f3
            boolean r4 = r2.b(r6)
            if (r4 == 0) goto L_0x00f3
            r10 = r6
            r14 = r16
            r15 = 1
        L_0x00f3:
            int[] r4 = r2.g
            r7 = r4[r7]
            int r9 = r9 + 1
            r6 = -1
            goto L_0x0069
        L_0x00fc:
            if (r11 == 0) goto L_0x00ff
            r10 = r11
        L_0x00ff:
            if (r10 != 0) goto L_0x0103
            r2 = 1
            goto L_0x0107
        L_0x0103:
            r1.b(r10)
            r2 = 0
        L_0x0107:
            i.f.a.ArrayLinkedVariables r4 = r1.d
            int r4 = r4.a
            if (r4 != 0) goto L_0x010f
            r1.f1079e = r3
        L_0x010f:
            if (r2 == 0) goto L_0x018c
            int r2 = r0.f1083i
            int r2 = r2 + r3
            int r4 = r0.f1081e
            if (r2 < r4) goto L_0x011b
            r18.d()
        L_0x011b:
            i.f.a.SolverVariable$a r2 = i.f.a.SolverVariable.a.SLACK
            r4 = 0
            i.f.a.SolverVariable r2 = r0.a(r2, r4)
            int r4 = r0.a
            int r4 = r4 + r3
            r0.a = r4
            int r6 = r0.f1083i
            int r6 = r6 + r3
            r0.f1083i = r6
            r2.b = r4
            i.f.a.Cache r6 = r0.f1086l
            i.f.a.SolverVariable[] r6 = r6.c
            r6[r4] = r2
            r1.a = r2
            r18.b(r19)
            i.f.a.LinearSystem$a r4 = r0.f1089o
            i.f.a.ArrayRow r4 = (i.f.a.ArrayRow) r4
            if (r4 == 0) goto L_0x018a
            r6 = 0
            r4.a = r6
            i.f.a.ArrayLinkedVariables r6 = r4.d
            r6.a()
            r6 = 0
        L_0x0148:
            i.f.a.ArrayLinkedVariables r7 = r1.d
            int r8 = r7.a
            if (r6 >= r8) goto L_0x0160
            i.f.a.SolverVariable r7 = r7.a(r6)
            i.f.a.ArrayLinkedVariables r8 = r1.d
            float r8 = r8.b(r6)
            i.f.a.ArrayLinkedVariables r9 = r4.d
            r9.a(r7, r8, r3)
            int r6 = r6 + 1
            goto L_0x0148
        L_0x0160:
            i.f.a.LinearSystem$a r4 = r0.f1089o
            r0.b(r4)
            int r4 = r2.c
            r6 = -1
            if (r4 != r6) goto L_0x0188
            i.f.a.SolverVariable r4 = r1.a
            if (r4 != r2) goto L_0x017a
            i.f.a.ArrayLinkedVariables r4 = r1.d
            r6 = 0
            i.f.a.SolverVariable r2 = r4.a(r6, r2)
            if (r2 == 0) goto L_0x017a
            r1.b(r2)
        L_0x017a:
            boolean r2 = r1.f1079e
            if (r2 != 0) goto L_0x0183
            i.f.a.SolverVariable r2 = r1.a
            r2.c(r1)
        L_0x0183:
            int r2 = r0.f1084j
            int r2 = r2 - r3
            r0.f1084j = r2
        L_0x0188:
            r2 = 1
            goto L_0x018d
        L_0x018a:
            r2 = 0
            throw r2
        L_0x018c:
            r2 = 0
        L_0x018d:
            i.f.a.SolverVariable r4 = r1.a
            if (r4 == 0) goto L_0x019e
            i.f.a.SolverVariable$a r4 = r4.g
            i.f.a.SolverVariable$a r6 = i.f.a.SolverVariable.a.UNRESTRICTED
            if (r4 == r6) goto L_0x019f
            float r4 = r1.b
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 < 0) goto L_0x019e
            goto L_0x019f
        L_0x019e:
            r3 = 0
        L_0x019f:
            if (r3 != 0) goto L_0x01a2
            return
        L_0x01a2:
            r4 = r2
            goto L_0x01a5
        L_0x01a4:
            r4 = 0
        L_0x01a5:
            if (r4 != 0) goto L_0x01aa
            r18.b(r19)
        L_0x01aa:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.f.a.LinearSystem.a(i.f.a.ArrayRow):void");
    }

    public final void a() {
        for (int i2 = 0; i2 < this.f1084j; i2++) {
            ArrayRow arrayRow = this.f1082f[i2];
            arrayRow.a.f1091e = arrayRow.b;
        }
    }

    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, float f2, SolverVariable solverVariable3, SolverVariable solverVariable4, int i3, int i4) {
        ArrayRow b2 = b();
        if (solverVariable2 == solverVariable3) {
            b2.d.a(solverVariable, 1.0f);
            b2.d.a(solverVariable4, 1.0f);
            b2.d.a(solverVariable2, -2.0f);
        } else if (f2 == 0.5f) {
            b2.d.a(solverVariable, 1.0f);
            b2.d.a(solverVariable2, -1.0f);
            b2.d.a(solverVariable3, -1.0f);
            b2.d.a(solverVariable4, 1.0f);
            if (i2 > 0 || i3 > 0) {
                b2.b = (float) ((-i2) + i3);
            }
        } else if (f2 <= 0.0f) {
            b2.d.a(solverVariable, -1.0f);
            b2.d.a(solverVariable2, 1.0f);
            b2.b = (float) i2;
        } else if (f2 >= 1.0f) {
            b2.d.a(solverVariable3, -1.0f);
            b2.d.a(solverVariable4, 1.0f);
            b2.b = (float) i3;
        } else {
            float f3 = 1.0f - f2;
            b2.d.a(solverVariable, f3 * 1.0f);
            b2.d.a(solverVariable2, f3 * -1.0f);
            b2.d.a(solverVariable3, -1.0f * f2);
            b2.d.a(solverVariable4, 1.0f * f2);
            if (i2 > 0 || i3 > 0) {
                b2.b = (((float) i3) * f2) + (((float) (-i2)) * f3);
            }
        }
        if (i4 != 6) {
            b2.a(this, i4);
        }
        a(b2);
    }

    public void a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f2, int i2) {
        ArrayRow b2 = b();
        b2.a(solverVariable, solverVariable2, solverVariable3, solverVariable4, f2);
        if (i2 != 6) {
            b2.a(this, i2);
        }
        a(b2);
    }

    public ArrayRow a(SolverVariable solverVariable, SolverVariable solverVariable2, int i2, int i3) {
        ArrayRow b2 = b();
        boolean z = false;
        if (i2 != 0) {
            if (i2 < 0) {
                i2 *= -1;
                z = true;
            }
            b2.b = (float) i2;
        }
        if (!z) {
            b2.d.a(solverVariable, -1.0f);
            b2.d.a(solverVariable2, 1.0f);
        } else {
            b2.d.a(solverVariable, 1.0f);
            b2.d.a(solverVariable2, -1.0f);
        }
        if (i3 != 6) {
            b2.a(this, i3);
        }
        a(b2);
        return b2;
    }

    public void a(SolverVariable solverVariable, int i2) {
        int i3 = solverVariable.c;
        if (i3 != -1) {
            ArrayRow arrayRow = this.f1082f[i3];
            if (arrayRow.f1079e) {
                arrayRow.b = (float) i2;
            } else if (arrayRow.d.a == 0) {
                arrayRow.f1079e = true;
                arrayRow.b = (float) i2;
            } else {
                ArrayRow b2 = b();
                if (i2 < 0) {
                    b2.b = (float) (i2 * -1);
                    b2.d.a(solverVariable, 1.0f);
                } else {
                    b2.b = (float) i2;
                    b2.d.a(solverVariable, -1.0f);
                }
                a(b2);
            }
        } else {
            ArrayRow b3 = b();
            b3.a = solverVariable;
            float f2 = (float) i2;
            solverVariable.f1091e = f2;
            b3.b = f2;
            b3.f1079e = true;
            a(b3);
        }
    }
}
