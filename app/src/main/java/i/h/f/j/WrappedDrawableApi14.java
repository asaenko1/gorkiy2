package i.h.f.j;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

public class WrappedDrawableApi14 extends Drawable implements Drawable.Callback, WrappedDrawable, TintAwareDrawable {
    public static final PorterDuff.Mode h = PorterDuff.Mode.SRC_IN;
    public int b;
    public PorterDuff.Mode c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public WrappedDrawableState f1185e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1186f;
    public Drawable g;

    public WrappedDrawableApi14(WrappedDrawableState wrappedDrawableState, Resources resources) {
        Drawable.ConstantState constantState;
        this.f1185e = wrappedDrawableState;
        if (wrappedDrawableState != null && (constantState = wrappedDrawableState.b) != null) {
            a(constantState.newDrawable(resources));
        }
    }

    public final boolean a(int[] iArr) {
        if (!b()) {
            return false;
        }
        WrappedDrawableState wrappedDrawableState = this.f1185e;
        ColorStateList colorStateList = wrappedDrawableState.c;
        PorterDuff.Mode mode = wrappedDrawableState.d;
        if (colorStateList == null || mode == null) {
            this.d = false;
            clearColorFilter();
        } else {
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (!(this.d && colorForState == this.b && mode == this.c)) {
                setColorFilter(colorForState, mode);
                this.b = colorForState;
                this.c = mode;
                this.d = true;
                return true;
            }
        }
        return false;
    }

    public boolean b() {
        throw null;
    }

    public void draw(Canvas canvas) {
        this.g.draw(canvas);
    }

    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        WrappedDrawableState wrappedDrawableState = this.f1185e;
        return changingConfigurations | (wrappedDrawableState != null ? wrappedDrawableState.getChangingConfigurations() : 0) | this.g.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState() {
        WrappedDrawableState wrappedDrawableState = this.f1185e;
        if (wrappedDrawableState == null) {
            return null;
        }
        if (!(wrappedDrawableState.b != null)) {
            return null;
        }
        this.f1185e.a = getChangingConfigurations();
        return this.f1185e;
    }

    public Drawable getCurrent() {
        return this.g.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.g.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.g.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.g.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.g.getMinimumWidth();
    }

    public int getOpacity() {
        return this.g.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.g.getPadding(rect);
    }

    public int[] getState() {
        return this.g.getState();
    }

    public Region getTransparentRegion() {
        return this.g.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isAutoMirrored() {
        return this.g.isAutoMirrored();
    }

    public boolean isStateful() {
        WrappedDrawableState wrappedDrawableState;
        ColorStateList colorStateList = (!b() || (wrappedDrawableState = this.f1185e) == null) ? null : wrappedDrawableState.c;
        return (colorStateList != null && colorStateList.isStateful()) || this.g.isStateful();
    }

    public void jumpToCurrentState() {
        this.g.jumpToCurrentState();
    }

    public Drawable mutate() {
        if (!this.f1186f && super.mutate() == this) {
            this.f1185e = new WrappedDrawableState(this.f1185e);
            Drawable drawable = this.g;
            if (drawable != null) {
                super.mutate();
            }
            WrappedDrawableState wrappedDrawableState = this.f1185e;
            if (wrappedDrawableState != null) {
                Drawable drawable2 = this.g;
                wrappedDrawableState.b = drawable2 != null ? super.getConstantState() : null;
            }
            this.f1186f = true;
        }
        return super;
    }

    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.g;
        if (drawable != null) {
            super.setBounds(rect);
        }
    }

    public boolean onLevelChange(int i2) {
        return this.g.setLevel(i2);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        scheduleSelf(runnable, j2);
    }

    public void setAlpha(int i2) {
        this.g.setAlpha(i2);
    }

    public void setAutoMirrored(boolean z) {
        this.g.setAutoMirrored(z);
    }

    public void setChangingConfigurations(int i2) {
        this.g.setChangingConfigurations(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.g.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.g.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.g.setFilterBitmap(z);
    }

    public boolean setState(int[] iArr) {
        return a(iArr) || this.g.setState(iArr);
    }

    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    public void setTintList(ColorStateList colorStateList) {
        throw null;
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.g.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public WrappedDrawableApi14(Drawable drawable) {
        this.f1185e = new WrappedDrawableState(this.f1185e);
        a(super);
    }

    public final Drawable a() {
        return this.g;
    }

    public final void a(Drawable drawable) {
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            super.setCallback(null);
        }
        this.g = super;
        if (drawable != null) {
            super.setCallback(this);
            setVisible(super.isVisible(), true);
            setState(super.getState());
            setLevel(super.getLevel());
            setBounds(super.getBounds());
            WrappedDrawableState wrappedDrawableState = this.f1185e;
            if (wrappedDrawableState != null) {
                wrappedDrawableState.b = super.getConstantState();
            }
        }
        invalidateSelf();
    }
}
