package i.h.m;

import android.widget.ListView;

public class ListViewAutoScrollHelper extends AutoScrollHelper {

    /* renamed from: s  reason: collision with root package name */
    public final ListView f1225s;

    public ListViewAutoScrollHelper(ListView listView) {
        super(listView);
        this.f1225s = listView;
    }
}
