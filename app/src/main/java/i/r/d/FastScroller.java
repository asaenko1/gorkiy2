package i.r.d;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import i.h.l.ViewCompat;

public class FastScroller extends RecyclerView.n implements RecyclerView.s {
    public static final int[] D = {16842919};
    public static final int[] E = new int[0];
    public int A = 0;
    public final Runnable B = new a();
    public final RecyclerView.t C = new b();
    public final int a;
    public final int b;
    public final StateListDrawable c;
    public final Drawable d;

    /* renamed from: e  reason: collision with root package name */
    public final int f1382e;

    /* renamed from: f  reason: collision with root package name */
    public final int f1383f;
    public final StateListDrawable g;
    public final Drawable h;

    /* renamed from: i  reason: collision with root package name */
    public final int f1384i;

    /* renamed from: j  reason: collision with root package name */
    public final int f1385j;

    /* renamed from: k  reason: collision with root package name */
    public int f1386k;

    /* renamed from: l  reason: collision with root package name */
    public int f1387l;

    /* renamed from: m  reason: collision with root package name */
    public float f1388m;

    /* renamed from: n  reason: collision with root package name */
    public int f1389n;

    /* renamed from: o  reason: collision with root package name */
    public int f1390o;

    /* renamed from: p  reason: collision with root package name */
    public float f1391p;

    /* renamed from: q  reason: collision with root package name */
    public int f1392q = 0;

    /* renamed from: r  reason: collision with root package name */
    public int f1393r = 0;

    /* renamed from: s  reason: collision with root package name */
    public RecyclerView f1394s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f1395t = false;
    public boolean u = false;
    public int v = 0;
    public int w = 0;
    public final int[] x = new int[2];
    public final int[] y = new int[2];
    public final ValueAnimator z = ValueAnimator.ofFloat(0.0f, 1.0f);

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            FastScroller fastScroller = FastScroller.this;
            int i2 = fastScroller.A;
            if (i2 == 1) {
                fastScroller.z.cancel();
            } else if (i2 != 2) {
                return;
            }
            fastScroller.A = 3;
            ValueAnimator valueAnimator = fastScroller.z;
            valueAnimator.setFloatValues(((Float) valueAnimator.getAnimatedValue()).floatValue(), 0.0f);
            fastScroller.z.setDuration((long) 500);
            fastScroller.z.start();
        }
    }

    public class b extends RecyclerView.t {
        public b() {
        }

        public void a(RecyclerView recyclerView, int i2, int i3) {
            FastScroller fastScroller = FastScroller.this;
            int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset();
            int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset();
            int computeVerticalScrollRange = fastScroller.f1394s.computeVerticalScrollRange();
            int i4 = fastScroller.f1393r;
            fastScroller.f1395t = computeVerticalScrollRange - i4 > 0 && i4 >= fastScroller.a;
            int computeHorizontalScrollRange = fastScroller.f1394s.computeHorizontalScrollRange();
            int i5 = fastScroller.f1392q;
            boolean z = computeHorizontalScrollRange - i5 > 0 && i5 >= fastScroller.a;
            fastScroller.u = z;
            if (fastScroller.f1395t || z) {
                if (fastScroller.f1395t) {
                    float f2 = (float) i4;
                    fastScroller.f1387l = (int) ((((f2 / 2.0f) + ((float) computeVerticalScrollOffset)) * f2) / ((float) computeVerticalScrollRange));
                    fastScroller.f1386k = Math.min(i4, (i4 * i4) / computeVerticalScrollRange);
                }
                if (fastScroller.u) {
                    float f3 = (float) computeHorizontalScrollOffset;
                    float f4 = (float) i5;
                    fastScroller.f1390o = (int) ((((f4 / 2.0f) + f3) * f4) / ((float) computeHorizontalScrollRange));
                    fastScroller.f1389n = Math.min(i5, (i5 * i5) / computeHorizontalScrollRange);
                }
                int i6 = fastScroller.v;
                if (i6 == 0 || i6 == 1) {
                    fastScroller.a(1);
                }
            } else if (fastScroller.v != 0) {
                fastScroller.a(0);
            }
        }
    }

    public class c extends AnimatorListenerAdapter {
        public boolean a = false;

        public c() {
        }

        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        public void onAnimationEnd(Animator animator) {
            if (this.a) {
                this.a = false;
            } else if (((Float) FastScroller.this.z.getAnimatedValue()).floatValue() == 0.0f) {
                FastScroller fastScroller = FastScroller.this;
                fastScroller.A = 0;
                fastScroller.a(0);
            } else {
                FastScroller fastScroller2 = FastScroller.this;
                fastScroller2.A = 2;
                fastScroller2.f1394s.invalidate();
            }
        }
    }

    public class d implements ValueAnimator.AnimatorUpdateListener {
        public d() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int floatValue = (int) (((Float) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
            FastScroller.this.c.setAlpha(floatValue);
            FastScroller.this.d.setAlpha(floatValue);
            FastScroller.this.f1394s.invalidate();
        }
    }

    public FastScroller(RecyclerView recyclerView, StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2, int i2, int i3, int i4) {
        this.c = stateListDrawable;
        this.d = drawable;
        this.g = stateListDrawable2;
        this.h = drawable2;
        this.f1382e = Math.max(i2, stateListDrawable.getIntrinsicWidth());
        this.f1383f = Math.max(i2, drawable.getIntrinsicWidth());
        this.f1384i = Math.max(i2, stateListDrawable2.getIntrinsicWidth());
        this.f1385j = Math.max(i2, drawable2.getIntrinsicWidth());
        this.a = i3;
        this.b = i4;
        this.c.setAlpha(255);
        this.d.setAlpha(255);
        this.z.addListener(new c());
        this.z.addUpdateListener(new d());
        RecyclerView recyclerView2 = this.f1394s;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                recyclerView2.removeItemDecoration(super);
                this.f1394s.removeOnItemTouchListener(this);
                this.f1394s.removeOnScrollListener(this.C);
                a();
            }
            this.f1394s = recyclerView;
            if (recyclerView != null) {
                recyclerView.addItemDecoration(super);
                this.f1394s.addOnItemTouchListener(this);
                this.f1394s.addOnScrollListener(this.C);
            }
        }
    }

    public void a(int i2) {
        if (i2 == 2 && this.v != 2) {
            this.c.setState(D);
            a();
        }
        if (i2 == 0) {
            this.f1394s.invalidate();
        } else {
            b();
        }
        if (this.v == 2 && i2 != 2) {
            this.c.setState(E);
            a();
            this.f1394s.postDelayed(this.B, (long) 1200);
        } else if (i2 == 1) {
            a();
            this.f1394s.postDelayed(this.B, (long) 1500);
        }
        this.v = i2;
    }

    public void a(boolean z2) {
    }

    public boolean b(float f2, float f3) {
        if (ViewCompat.k(this.f1394s) == 1) {
            if (f2 > ((float) (this.f1382e / 2))) {
                return false;
            }
        } else if (f2 < ((float) (this.f1392q - this.f1382e))) {
            return false;
        }
        int i2 = this.f1387l;
        int i3 = this.f1386k / 2;
        if (f3 < ((float) (i2 - i3)) || f3 > ((float) (i3 + i2))) {
            return false;
        }
        return true;
    }

    public void b() {
        int i2 = this.A;
        if (i2 != 0) {
            if (i2 == 3) {
                this.z.cancel();
            } else {
                return;
            }
        }
        this.A = 1;
        ValueAnimator valueAnimator = this.z;
        valueAnimator.setFloatValues(((Float) valueAnimator.getAnimatedValue()).floatValue(), 1.0f);
        this.z.setDuration(500L);
        this.z.setStartDelay(0);
        this.z.start();
    }

    public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var) {
        if (this.f1392q != this.f1394s.getWidth() || this.f1393r != this.f1394s.getHeight()) {
            this.f1392q = this.f1394s.getWidth();
            this.f1393r = this.f1394s.getHeight();
            a(0);
        } else if (this.A != 0) {
            if (this.f1395t) {
                int i2 = this.f1392q;
                int i3 = this.f1382e;
                int i4 = i2 - i3;
                int i5 = this.f1387l;
                int i6 = this.f1386k;
                int i7 = i5 - (i6 / 2);
                this.c.setBounds(0, 0, i3, i6);
                this.d.setBounds(0, 0, this.f1383f, this.f1393r);
                boolean z2 = true;
                if (ViewCompat.k(this.f1394s) != 1) {
                    z2 = false;
                }
                if (z2) {
                    this.d.draw(canvas);
                    canvas.translate((float) this.f1382e, (float) i7);
                    canvas.scale(-1.0f, 1.0f);
                    this.c.draw(canvas);
                    canvas.scale(1.0f, 1.0f);
                    canvas.translate((float) (-this.f1382e), (float) (-i7));
                } else {
                    canvas.translate((float) i4, 0.0f);
                    this.d.draw(canvas);
                    canvas.translate(0.0f, (float) i7);
                    this.c.draw(canvas);
                    canvas.translate((float) (-i4), (float) (-i7));
                }
            }
            if (this.u) {
                int i8 = this.f1393r;
                int i9 = this.f1384i;
                int i10 = i8 - i9;
                int i11 = this.f1390o;
                int i12 = this.f1389n;
                int i13 = i11 - (i12 / 2);
                this.g.setBounds(0, 0, i12, i9);
                this.h.setBounds(0, 0, this.f1392q, this.f1385j);
                canvas.translate(0.0f, (float) i10);
                this.h.draw(canvas);
                canvas.translate((float) i13, 0.0f);
                this.g.draw(canvas);
                canvas.translate((float) (-i13), (float) (-i10));
            }
        }
    }

    public final void a() {
        this.f1394s.removeCallbacks(this.B);
    }

    public boolean a(RecyclerView recyclerView, MotionEvent motionEvent) {
        int i2 = this.v;
        if (i2 == 1) {
            boolean b2 = b(motionEvent.getX(), motionEvent.getY());
            boolean a2 = a(motionEvent.getX(), motionEvent.getY());
            if (motionEvent.getAction() != 0) {
                return false;
            }
            if (!b2 && !a2) {
                return false;
            }
            if (a2) {
                this.w = 1;
                this.f1391p = (float) ((int) motionEvent.getX());
            } else if (b2) {
                this.w = 2;
                this.f1388m = (float) ((int) motionEvent.getY());
            }
            a(2);
        } else if (i2 == 2) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    public final int a(float f2, float f3, int[] iArr, int i2, int i3, int i4) {
        int i5 = iArr[1] - iArr[0];
        if (i5 == 0) {
            return 0;
        }
        int i6 = i2 - i4;
        int i7 = (int) (((f3 - f2) / ((float) i5)) * ((float) i6));
        int i8 = i3 + i7;
        if (i8 >= i6 || i8 < 0) {
            return 0;
        }
        return i7;
    }

    public boolean a(float f2, float f3) {
        if (f3 >= ((float) (this.f1393r - this.f1384i))) {
            int i2 = this.f1390o;
            int i3 = this.f1389n;
            return f2 >= ((float) (i2 - (i3 / 2))) && f2 <= ((float) ((i3 / 2) + i2));
        }
    }

    public void b(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (this.v != 0) {
            if (motionEvent.getAction() == 0) {
                boolean b2 = b(motionEvent.getX(), motionEvent.getY());
                boolean a2 = a(motionEvent.getX(), motionEvent.getY());
                if (b2 || a2) {
                    if (a2) {
                        this.w = 1;
                        this.f1391p = (float) ((int) motionEvent.getX());
                    } else if (b2) {
                        this.w = 2;
                        this.f1388m = (float) ((int) motionEvent.getY());
                    }
                    a(2);
                }
            } else if (motionEvent.getAction() == 1 && this.v == 2) {
                this.f1388m = 0.0f;
                this.f1391p = 0.0f;
                a(1);
                this.w = 0;
            } else if (motionEvent.getAction() == 2 && this.v == 2) {
                b();
                if (this.w == 1) {
                    float x2 = motionEvent.getX();
                    int[] iArr = this.y;
                    int i2 = this.b;
                    iArr[0] = i2;
                    iArr[1] = this.f1392q - i2;
                    float max = Math.max((float) iArr[0], Math.min((float) iArr[1], x2));
                    if (Math.abs(((float) this.f1390o) - max) >= 2.0f) {
                        int a3 = a(this.f1391p, max, iArr, this.f1394s.computeHorizontalScrollRange(), this.f1394s.computeHorizontalScrollOffset(), this.f1392q);
                        if (a3 != 0) {
                            this.f1394s.scrollBy(a3, 0);
                        }
                        this.f1391p = max;
                    }
                }
                if (this.w == 2) {
                    float y2 = motionEvent.getY();
                    int[] iArr2 = this.x;
                    int i3 = this.b;
                    iArr2[0] = i3;
                    iArr2[1] = this.f1393r - i3;
                    float max2 = Math.max((float) iArr2[0], Math.min((float) iArr2[1], y2));
                    if (Math.abs(((float) this.f1387l) - max2) >= 2.0f) {
                        int a4 = a(this.f1388m, max2, iArr2, this.f1394s.computeVerticalScrollRange(), this.f1394s.computeVerticalScrollOffset(), this.f1393r);
                        if (a4 != 0) {
                            this.f1394s.scrollBy(0, a4);
                        }
                        this.f1388m = max2;
                    }
                }
            }
        }
    }
}
