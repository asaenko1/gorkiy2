package i.r.d;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: OrientationHelper */
public final class OrientationHelper1 extends OrientationHelper {
    public OrientationHelper1(RecyclerView.o oVar) {
        super(oVar, null);
    }

    public int a() {
        return super.a.f327r;
    }

    public int b() {
        RecyclerView.o oVar = super.a;
        return oVar.f327r - oVar.i();
    }

    public int c(View view) {
        RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
        return super.a.f(view) + pVar.leftMargin + pVar.rightMargin;
    }

    public int d(View view) {
        return super.a.h(view) - ((RecyclerView.p) view.getLayoutParams()).topMargin;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
    public int e(View view) {
        super.a.a(view, true, super.c);
        return super.c.bottom;
    }

    public int f() {
        return super.a.l();
    }

    public int g() {
        RecyclerView.o oVar = super.a;
        return (oVar.f327r - oVar.l()) - super.a.i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
    public int f(View view) {
        super.a.a(view, true, super.c);
        return super.c.top;
    }

    public void a(int i2) {
        super.a.e(i2);
    }

    public int c() {
        return super.a.i();
    }

    public int d() {
        return super.a.f325p;
    }

    public int e() {
        return super.a.f324o;
    }

    public int a(View view) {
        return super.a.c(view) + ((RecyclerView.p) view.getLayoutParams()).bottomMargin;
    }

    public int b(View view) {
        RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
        return super.a.e(view) + pVar.topMargin + pVar.bottomMargin;
    }
}
