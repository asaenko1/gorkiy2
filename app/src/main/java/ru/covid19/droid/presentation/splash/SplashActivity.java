package ru.covid19.droid.presentation.splash;

import android.os.Bundle;
import e.a.a.a.b.BaseDpActivity4;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerSplashComponent;
import e.a.b.f.a.SplashComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.h.d.SplashActivityVm;
import e.a.b.h.d.a;
import i.o.ViewModelProvider;
import j.c.a.a.c.n.c;
import n.n.c.Intrinsics;

/* compiled from: SplashActivity.kt */
public final class SplashActivity extends BaseDpActivity4<a> {
    public void onCreate(Bundle bundle) {
        if (AppComponentsHolder.a == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent != null) {
                c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.a = new DaggerSplashComponent(coreComponent, null);
            } else {
                Intrinsics.b("coreComponent");
                throw null;
            }
        }
        SplashComponent splashComponent = AppComponentsHolder.a;
        if (splashComponent != null) {
            splashComponent.a(super.u);
            splashComponent.a(super.v);
            super.onCreate(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [ru.covid19.droid.presentation.splash.SplashActivity, i.b.k.AppCompatActivity] */
    public void onDestroy() {
        AppComponentsHolder.a = null;
        SplashActivity.super.onDestroy();
    }

    public Class<a> p() {
        return SplashActivityVm.class;
    }

    public ViewModelProvider.b q() {
        return super.u.a();
    }
}
