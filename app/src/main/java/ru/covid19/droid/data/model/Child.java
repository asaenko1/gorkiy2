package ru.covid19.droid.data.model;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: Child.kt */
public final class Child {
    public final String name;
    public final String patronymic;
    public final String surname;

    public Child(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("surname");
            throw null;
        } else if (str3 != null) {
            this.name = str;
            this.surname = str2;
            this.patronymic = str3;
        } else {
            Intrinsics.a("patronymic");
            throw null;
        }
    }

    public static /* synthetic */ Child copy$default(Child child, String str, String str2, String str3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = child.name;
        }
        if ((i2 & 2) != 0) {
            str2 = child.surname;
        }
        if ((i2 & 4) != 0) {
            str3 = child.patronymic;
        }
        return child.copy(str, str2, str3);
    }

    public final String component1() {
        return this.name;
    }

    public final String component2() {
        return this.surname;
    }

    public final String component3() {
        return this.patronymic;
    }

    public final Child copy(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("surname");
            throw null;
        } else if (str3 != null) {
            return new Child(str, str2, str3);
        } else {
            Intrinsics.a("patronymic");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Child)) {
            return false;
        }
        Child child = (Child) obj;
        return Intrinsics.a(this.name, child.name) && Intrinsics.a(this.surname, child.surname) && Intrinsics.a(this.patronymic, child.patronymic);
    }

    public final String getName() {
        return this.name;
    }

    public final String getPatronymic() {
        return this.patronymic;
    }

    public final String getSurname() {
        return this.surname;
    }

    public int hashCode() {
        String str = this.name;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.surname;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.patronymic;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("Child(name=");
        a.append(this.name);
        a.append(", surname=");
        a.append(this.surname);
        a.append(", patronymic=");
        return outline.a(a, this.patronymic, ")");
    }
}
