package ru.covid19.core.deeplink;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import e.a.a.a.b.BaseDpActivity4;
import e.a.a.a.e.BaseScreens1;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.a.e.s.ActivityRouter;
import e.a.a.h.DeeplinkActivityVm;
import e.a.a.h.IDeeplinkManager;
import e.a.a.h.a;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.b.DaggerDeeplinkComponent;
import e.a.a.i.b.DeeplinkComponent;
import e.a.a.i.e.DeeplinkManagerWrapper;
import e.a.a.i.e.NavigationWrapper;
import e.b.a.Screen;
import e.b.a.f;
import i.o.ViewModelProvider;
import j.c.a.a.c.n.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;

/* compiled from: DeeplinkActivity.kt */
public final class DeeplinkActivity extends BaseDpActivity4<a> {
    public final DeeplinkManagerWrapper x = new DeeplinkManagerWrapper();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.FragmentCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.AppCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.h.IDeeplinkManager, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public void onCreate(Bundle bundle) {
        if (CoreComponentsHolder.g == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent != null) {
                c.a(coreComponent, CoreComponent.class);
                CoreComponentsHolder.g = new DaggerDeeplinkComponent(coreComponent, null);
            } else {
                Intrinsics.b("coreComponent");
                throw null;
            }
        }
        DeeplinkComponent deeplinkComponent = CoreComponentsHolder.g;
        if (deeplinkComponent != null) {
            super.u.a = ((DaggerDeeplinkComponent) deeplinkComponent).d.get();
            NavigationWrapper navigationWrapper = super.v;
            DaggerDeeplinkComponent daggerDeeplinkComponent = (DaggerDeeplinkComponent) deeplinkComponent;
            FragmentCiceroneHolder m2 = daggerDeeplinkComponent.a.m();
            c.a((Object) m2, "Cannot return null from a non-@Nullable component method");
            navigationWrapper.a = m2;
            AppCiceroneHolder a = daggerDeeplinkComponent.a.a();
            c.a((Object) a, "Cannot return null from a non-@Nullable component method");
            navigationWrapper.b = a;
            DeeplinkManagerWrapper deeplinkManagerWrapper = this.x;
            IDeeplinkManager j2 = daggerDeeplinkComponent.a.j();
            c.a((Object) j2, "Cannot return null from a non-@Nullable component method");
            deeplinkManagerWrapper.a = j2;
            super.onCreate(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [ru.covid19.core.deeplink.DeeplinkActivity, e.a.a.a.b.BaseDpActivity4, android.app.Activity] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Intent, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Intrinsics.a((Object) intent, "intent");
        Uri data = intent.getData();
        if (data != null) {
            IDeeplinkManager iDeeplinkManager = this.x.a;
            if (iDeeplinkManager != null) {
                List<f> a = iDeeplinkManager.a(data);
                if (!a.isEmpty()) {
                    ActivityRouter activityRouter = r().b;
                    ArrayList arrayList = new ArrayList();
                    for (T next : a) {
                        if (next instanceof BaseScreens1) {
                            arrayList.add(next);
                        }
                    }
                    Object[] array = arrayList.toArray(new BaseScreens1[0]);
                    if (array != null) {
                        Screen[] screenArr = (Screen[]) array;
                        activityRouter.a((Screen[]) Arrays.copyOf(screenArr, screenArr.length));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
            } else {
                Intrinsics.b("deeplinkManager");
                throw null;
            }
        }
        finish();
    }

    public Class<a> p() {
        return DeeplinkActivityVm.class;
    }

    public ViewModelProvider.b q() {
        return super.u.a();
    }
}
