package ru.covid19.core.data.network.model;

import n.n.c.Intrinsics;

/* compiled from: GuidForSharingResponse.kt */
public final class GuidForSharingResponse {
    public final String guid;

    public GuidForSharingResponse(String str) {
        if (str != null) {
            this.guid = str;
        } else {
            Intrinsics.a("guid");
            throw null;
        }
    }

    public final String getGuid() {
        return this.guid;
    }
}
