package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: GetTokensResponse.kt */
public final class GetTokensResponse extends BaseResponse {
    public final String access_token;
    public final int expires_in;
    public final String id_token;
    public final String oid;
    public final String refresh_token;
    public final String state;
    public final String token_type;

    public GetTokensResponse(String str, String str2, int i2, String str3, String str4, String str5, String str6) {
        if (str == null) {
            Intrinsics.a("id_token");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("access_token");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("state");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a("token_type");
            throw null;
        } else if (str5 == null) {
            Intrinsics.a("refresh_token");
            throw null;
        } else if (str6 != null) {
            this.id_token = str;
            this.access_token = str2;
            this.expires_in = i2;
            this.state = str3;
            this.token_type = str4;
            this.refresh_token = str5;
            this.oid = str6;
        } else {
            Intrinsics.a("oid");
            throw null;
        }
    }

    public static /* synthetic */ GetTokensResponse copy$default(GetTokensResponse getTokensResponse, String str, String str2, int i2, String str3, String str4, String str5, String str6, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = getTokensResponse.id_token;
        }
        if ((i3 & 2) != 0) {
            str2 = getTokensResponse.access_token;
        }
        String str7 = str2;
        if ((i3 & 4) != 0) {
            i2 = getTokensResponse.expires_in;
        }
        int i4 = i2;
        if ((i3 & 8) != 0) {
            str3 = getTokensResponse.state;
        }
        String str8 = str3;
        if ((i3 & 16) != 0) {
            str4 = getTokensResponse.token_type;
        }
        String str9 = str4;
        if ((i3 & 32) != 0) {
            str5 = getTokensResponse.refresh_token;
        }
        String str10 = str5;
        if ((i3 & 64) != 0) {
            str6 = getTokensResponse.oid;
        }
        return getTokensResponse.copy(str, str7, i4, str8, str9, str10, str6);
    }

    public final String component1() {
        return this.id_token;
    }

    public final String component2() {
        return this.access_token;
    }

    public final int component3() {
        return this.expires_in;
    }

    public final String component4() {
        return this.state;
    }

    public final String component5() {
        return this.token_type;
    }

    public final String component6() {
        return this.refresh_token;
    }

    public final String component7() {
        return this.oid;
    }

    public final GetTokensResponse copy(String str, String str2, int i2, String str3, String str4, String str5, String str6) {
        if (str == null) {
            Intrinsics.a("id_token");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("access_token");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("state");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a("token_type");
            throw null;
        } else if (str5 == null) {
            Intrinsics.a("refresh_token");
            throw null;
        } else if (str6 != null) {
            return new GetTokensResponse(str, str2, i2, str3, str4, str5, str6);
        } else {
            Intrinsics.a("oid");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GetTokensResponse)) {
            return false;
        }
        GetTokensResponse getTokensResponse = (GetTokensResponse) obj;
        return Intrinsics.a(this.id_token, getTokensResponse.id_token) && Intrinsics.a(this.access_token, getTokensResponse.access_token) && this.expires_in == getTokensResponse.expires_in && Intrinsics.a(this.state, getTokensResponse.state) && Intrinsics.a(this.token_type, getTokensResponse.token_type) && Intrinsics.a(this.refresh_token, getTokensResponse.refresh_token) && Intrinsics.a(this.oid, getTokensResponse.oid);
    }

    public final String getAccess_token() {
        return this.access_token;
    }

    public final int getExpires_in() {
        return this.expires_in;
    }

    public final String getId_token() {
        return this.id_token;
    }

    public final String getOid() {
        return this.oid;
    }

    public final String getRefresh_token() {
        return this.refresh_token;
    }

    public final String getState() {
        return this.state;
    }

    public final String getToken_type() {
        return this.token_type;
    }

    public int hashCode() {
        String str = this.id_token;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.access_token;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.expires_in) * 31;
        String str3 = this.state;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.token_type;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.refresh_token;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.oid;
        if (str6 != null) {
            i2 = str6.hashCode();
        }
        return hashCode5 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("GetTokensResponse(id_token=");
        a.append(this.id_token);
        a.append(", access_token=");
        a.append(this.access_token);
        a.append(", expires_in=");
        a.append(this.expires_in);
        a.append(", state=");
        a.append(this.state);
        a.append(", token_type=");
        a.append(this.token_type);
        a.append(", refresh_token=");
        a.append(this.refresh_token);
        a.append(", oid=");
        return outline.a(a, this.oid, ")");
    }
}
