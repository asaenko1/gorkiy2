package ru.covid19.core.data.network.model;

import e.a.a.f;

/* compiled from: PersonalResponse.kt */
public enum PersonalResponse5 {
    PS(f.profile_account_simplified),
    PR(f.profile_account_simplified_ready_to_confirm),
    PVD(f.profile_account_standard),
    PCD(f.profile_account_confirmed);
    
    public final int resId;

    /* access modifiers changed from: public */
    PersonalResponse5(int i2) {
        this.resId = i2;
    }

    public final int getResId() {
        return this.resId;
    }
}
