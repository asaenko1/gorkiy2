package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import j.c.d.a0.SerializedName;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: CitizenshipResponse.kt */
public final class CitizenshipResponse1 extends BaseResponse {
    @SerializedName("values")
    public final List<Citizenship> citizenshipsList;
    public final int size;

    public CitizenshipResponse1(int i2, List<Citizenship> list) {
        if (list != null) {
            this.size = i2;
            this.citizenshipsList = list;
            return;
        }
        Intrinsics.a("citizenshipsList");
        throw null;
    }

    public static /* synthetic */ CitizenshipResponse1 copy$default(CitizenshipResponse1 citizenshipResponse1, int i2, List list, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i2 = citizenshipResponse1.size;
        }
        if ((i3 & 2) != 0) {
            list = citizenshipResponse1.citizenshipsList;
        }
        return citizenshipResponse1.copy(i2, list);
    }

    public final int component1() {
        return this.size;
    }

    public final List<Citizenship> component2() {
        return this.citizenshipsList;
    }

    public final CitizenshipsResponse copy(int i2, List<Citizenship> list) {
        if (list != null) {
            return new CitizenshipResponse1(i2, list);
        }
        Intrinsics.a("citizenshipsList");
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CitizenshipResponse1)) {
            return false;
        }
        CitizenshipResponse1 citizenshipResponse1 = (CitizenshipResponse1) obj;
        return this.size == citizenshipResponse1.size && Intrinsics.a(this.citizenshipsList, citizenshipResponse1.citizenshipsList);
    }

    public final List<Citizenship> getCitizenshipsList() {
        return this.citizenshipsList;
    }

    public final int getSize() {
        return this.size;
    }

    public int hashCode() {
        int i2 = this.size * 31;
        List<Citizenship> list = this.citizenshipsList;
        return i2 + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        StringBuilder a = outline.a("CitizenshipsResponse(size=");
        a.append(this.size);
        a.append(", citizenshipsList=");
        a.append(this.citizenshipsList);
        a.append(")");
        return a.toString();
    }
}
