package ru.covid19.core.data.network.model;

/* compiled from: BaseResponse.kt */
public interface BaseResponse0 {

    /* compiled from: BaseResponse.kt */
    public static final class DefaultImpls {
        public static boolean getHasError(BaseResponse0 baseResponse0) {
            ApiError error = baseResponse0.getError();
            return error != null && error.getHasError();
        }
    }

    ApiError getError();

    boolean getHasError();

    void setError(ApiError apiError);
}
