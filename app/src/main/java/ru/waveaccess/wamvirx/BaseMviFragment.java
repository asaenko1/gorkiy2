package ru.waveaccess.wamvirx;

import android.os.Bundle;
import e.c.c.BaseMviView1;
import e.c.c.BaseMviVm1;
import e.c.c.BaseViewState2;
import e.c.c.b;
import e.c.d.b.c.BaseMvvmFragment;
import j.c.a.a.c.n.c;
import j.e.b.PublishRelay;
import java.util.List;
import l.b.Observable;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: BaseMviFragment.kt */
public abstract class BaseMviFragment<VS, VM extends BaseMviVm1<VS>> extends BaseMvvmFragment<VM> implements BaseMviView1<VS, VM> {
    public final PublishRelay<b> c0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay<e.c.c.b>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public BaseMviFragment() {
        PublishRelay<b> publishRelay = new PublishRelay<>();
        Intrinsics.a((Object) publishRelay, "PublishRelay.create()");
        this.c0 = publishRelay;
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void E() {
        this.F = true;
        super.Z.a();
        ((BaseMviVm1) h()).f722f.a();
    }

    public void N() {
    }

    public void O() {
        Collections.a((BaseMviView1) this);
    }

    public void a(BaseViewState2 baseViewState2) {
        if (baseViewState2 == null) {
            Intrinsics.a("ve");
            throw null;
        }
    }

    public void a(Object obj) {
    }

    public PublishRelay<b> b() {
        return this.c0;
    }

    public void d(Bundle bundle) {
        if (bundle != null) {
            ((BaseMviVm1) h()).b(bundle);
        } else {
            Intrinsics.a("outState");
            throw null;
        }
    }

    public List<Observable<? extends b>> g() {
        return c.c(b());
    }

    public void b(Bundle bundle) {
        ((BaseMviVm1) h()).a(bundle);
        super.b(bundle);
    }
}
