package q.b.a.t;

import java.text.DateFormatSymbols;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import q.b.a.t.k;
import q.b.a.v.ChronoField;
import q.b.a.v.IsoFields;
import q.b.a.v.TemporalField;
import q.b.a.v.j;

public final class SimpleDateTimeTextProvider extends DateTimeTextProvider {
    public static final Comparator<Map.Entry<String, Long>> c = new a();
    public final ConcurrentMap<Map.Entry<j, Locale>, Object> b = new ConcurrentHashMap(16, 0.75f, 2);

    public class a implements Comparator<Map.Entry<String, Long>> {
        public int compare(Object obj, Object obj2) {
            return ((String) ((Map.Entry) obj2).getKey()).length() - ((String) ((Map.Entry) obj).getKey()).length();
        }
    }

    public static final class b {
        public final Map<l, Map<Long, String>> a;
        public final Map<l, List<Map.Entry<String, Long>>> b;

        public b(Map<l, Map<Long, String>> map) {
            this.a = map;
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList();
            for (TextStyle next : map.keySet()) {
                HashMap hashMap2 = new HashMap();
                for (Map.Entry entry : map.get(next).entrySet()) {
                    hashMap2.put(entry.getValue(), SimpleDateTimeTextProvider.a(entry.getValue(), entry.getKey()));
                }
                ArrayList arrayList2 = new ArrayList(hashMap2.values());
                Collections.sort(arrayList2, SimpleDateTimeTextProvider.c);
                hashMap.put(next, arrayList2);
                arrayList.addAll(arrayList2);
                hashMap.put(null, arrayList);
            }
            Collections.sort(arrayList, SimpleDateTimeTextProvider.c);
            this.b = hashMap;
        }

        public String a(long j2, TextStyle textStyle) {
            Map map = this.a.get(textStyle);
            if (map != null) {
                return (String) map.get(Long.valueOf(j2));
            }
            return null;
        }
    }

    public String a(TemporalField temporalField, long j2, TextStyle textStyle, Locale locale) {
        Object a2 = a(temporalField, locale);
        if (a2 instanceof b) {
            return ((b) a2).a(j2, textStyle);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.t.SimpleDateTimeTextProvider.a(q.b.a.v.TemporalField, java.util.Locale):java.lang.Object
     arg types: [q.b.a.v.j, java.util.Locale]
     candidates:
      q.b.a.t.SimpleDateTimeTextProvider.a(java.lang.Object, java.lang.Object):java.util.Map$Entry
      q.b.a.t.SimpleDateTimeTextProvider.a(q.b.a.v.TemporalField, java.util.Locale):java.lang.Object */
    public Iterator<Map.Entry<String, Long>> a(j jVar, l lVar, Locale locale) {
        List list;
        Object a2 = a((TemporalField) jVar, locale);
        if (!(a2 instanceof b) || (list = ((b) a2).b.get(lVar)) == null) {
            return null;
        }
        return list.iterator();
    }

    public static /* synthetic */ Map.Entry a(Object obj, Object obj2) {
        return new AbstractMap.SimpleImmutableEntry(obj, obj2);
    }

    public final Object a(TemporalField temporalField, Locale locale) {
        AbstractMap.SimpleImmutableEntry simpleImmutableEntry;
        Object obj;
        TemporalField temporalField2 = temporalField;
        AbstractMap.SimpleImmutableEntry simpleImmutableEntry2 = new AbstractMap.SimpleImmutableEntry(temporalField2, locale);
        Object obj2 = this.b.get(simpleImmutableEntry2);
        if (obj2 != null) {
            return obj2;
        }
        if (temporalField2 == ChronoField.MONTH_OF_YEAR) {
            DateFormatSymbols instance = DateFormatSymbols.getInstance(locale);
            HashMap hashMap = new HashMap();
            String[] months = instance.getMonths();
            simpleImmutableEntry = simpleImmutableEntry2;
            HashMap hashMap2 = new HashMap();
            hashMap2.put(1L, months[0]);
            hashMap2.put(2L, months[1]);
            hashMap2.put(3L, months[2]);
            hashMap2.put(4L, months[3]);
            hashMap2.put(5L, months[4]);
            hashMap2.put(6L, months[5]);
            hashMap2.put(7L, months[6]);
            hashMap2.put(8L, months[7]);
            hashMap2.put(9L, months[8]);
            hashMap2.put(10L, months[9]);
            hashMap2.put(11L, months[10]);
            hashMap2.put(12L, months[11]);
            hashMap.put(TextStyle.FULL, hashMap2);
            HashMap hashMap3 = new HashMap();
            hashMap3.put(1L, months[0].substring(0, 1));
            hashMap3.put(2L, months[1].substring(0, 1));
            hashMap3.put(3L, months[2].substring(0, 1));
            hashMap3.put(4L, months[3].substring(0, 1));
            hashMap3.put(5L, months[4].substring(0, 1));
            hashMap3.put(6L, months[5].substring(0, 1));
            hashMap3.put(7L, months[6].substring(0, 1));
            hashMap3.put(8L, months[7].substring(0, 1));
            hashMap3.put(9L, months[8].substring(0, 1));
            hashMap3.put(10L, months[9].substring(0, 1));
            hashMap3.put(11L, months[10].substring(0, 1));
            hashMap3.put(12L, months[11].substring(0, 1));
            HashMap hashMap4 = hashMap;
            hashMap4.put(TextStyle.NARROW, hashMap3);
            String[] shortMonths = instance.getShortMonths();
            HashMap hashMap5 = new HashMap();
            hashMap5.put(1L, shortMonths[0]);
            hashMap5.put(2, shortMonths[1]);
            hashMap5.put(3L, shortMonths[2]);
            hashMap5.put(4L, shortMonths[3]);
            hashMap5.put(5L, shortMonths[4]);
            hashMap5.put(6L, shortMonths[5]);
            hashMap5.put(7L, shortMonths[6]);
            hashMap5.put(8L, shortMonths[7]);
            hashMap5.put(9L, shortMonths[8]);
            hashMap5.put(10L, shortMonths[9]);
            hashMap5.put(11L, shortMonths[10]);
            hashMap5.put(12L, shortMonths[11]);
            hashMap4.put(TextStyle.SHORT, hashMap5);
            obj = a(hashMap4);
        } else {
            simpleImmutableEntry = simpleImmutableEntry2;
            if (temporalField2 == ChronoField.DAY_OF_WEEK) {
                DateFormatSymbols instance2 = DateFormatSymbols.getInstance(locale);
                HashMap hashMap6 = new HashMap();
                String[] weekdays = instance2.getWeekdays();
                HashMap hashMap7 = new HashMap();
                hashMap7.put(1L, weekdays[2]);
                hashMap7.put(2, weekdays[3]);
                hashMap7.put(3L, weekdays[4]);
                hashMap7.put(4L, weekdays[5]);
                hashMap7.put(5L, weekdays[6]);
                hashMap7.put(6L, weekdays[7]);
                hashMap7.put(7L, weekdays[1]);
                hashMap6.put(TextStyle.FULL, hashMap7);
                HashMap hashMap8 = new HashMap();
                hashMap8.put(1L, weekdays[2].substring(0, 1));
                hashMap8.put(2, weekdays[3].substring(0, 1));
                hashMap8.put(3L, weekdays[4].substring(0, 1));
                hashMap8.put(4L, weekdays[5].substring(0, 1));
                hashMap8.put(5L, weekdays[6].substring(0, 1));
                hashMap8.put(6L, weekdays[7].substring(0, 1));
                hashMap8.put(7L, weekdays[1].substring(0, 1));
                hashMap6.put(TextStyle.NARROW, hashMap8);
                String[] shortWeekdays = instance2.getShortWeekdays();
                HashMap hashMap9 = new HashMap();
                hashMap9.put(1L, shortWeekdays[2]);
                hashMap9.put(2, shortWeekdays[3]);
                hashMap9.put(3L, shortWeekdays[4]);
                hashMap9.put(4L, shortWeekdays[5]);
                hashMap9.put(5L, shortWeekdays[6]);
                hashMap9.put(6L, shortWeekdays[7]);
                hashMap9.put(7L, shortWeekdays[1]);
                hashMap6.put(TextStyle.SHORT, hashMap9);
                obj = a(hashMap6);
            } else if (temporalField2 == ChronoField.AMPM_OF_DAY) {
                DateFormatSymbols instance3 = DateFormatSymbols.getInstance(locale);
                HashMap hashMap10 = new HashMap();
                String[] amPmStrings = instance3.getAmPmStrings();
                HashMap hashMap11 = new HashMap();
                hashMap11.put(0L, amPmStrings[0]);
                hashMap11.put(1L, amPmStrings[1]);
                hashMap10.put(TextStyle.FULL, hashMap11);
                hashMap10.put(TextStyle.SHORT, hashMap11);
                obj = a(hashMap10);
            } else if (temporalField2 == ChronoField.ERA) {
                DateFormatSymbols instance4 = DateFormatSymbols.getInstance(locale);
                HashMap hashMap12 = new HashMap();
                String[] eras = instance4.getEras();
                HashMap hashMap13 = new HashMap();
                hashMap13.put(0L, eras[0]);
                hashMap13.put(1L, eras[1]);
                hashMap12.put(TextStyle.SHORT, hashMap13);
                if (locale.getLanguage().equals(Locale.ENGLISH.getLanguage())) {
                    HashMap hashMap14 = new HashMap();
                    hashMap14.put(0L, "Before Christ");
                    hashMap14.put(1L, "Anno Domini");
                    hashMap12.put(TextStyle.FULL, hashMap14);
                } else {
                    hashMap12.put(TextStyle.FULL, hashMap13);
                }
                HashMap hashMap15 = new HashMap();
                hashMap15.put(0L, eras[0].substring(0, 1));
                hashMap15.put(1L, eras[1].substring(0, 1));
                hashMap12.put(TextStyle.NARROW, hashMap15);
                obj = a(hashMap12);
            } else if (temporalField2 == IsoFields.a) {
                HashMap hashMap16 = new HashMap();
                HashMap hashMap17 = new HashMap();
                hashMap17.put(1L, "Q1");
                hashMap17.put(2, "Q2");
                hashMap17.put(3L, "Q3");
                hashMap17.put(4L, "Q4");
                hashMap16.put(TextStyle.SHORT, hashMap17);
                HashMap hashMap18 = new HashMap();
                hashMap18.put(1L, "1st quarter");
                hashMap18.put(2, "2nd quarter");
                hashMap18.put(3L, "3rd quarter");
                hashMap18.put(4L, "4th quarter");
                hashMap16.put(TextStyle.FULL, hashMap18);
                obj = a(hashMap16);
            } else {
                obj = "";
            }
        }
        AbstractMap.SimpleImmutableEntry simpleImmutableEntry3 = simpleImmutableEntry;
        this.b.putIfAbsent(simpleImmutableEntry3, obj);
        return this.b.get(simpleImmutableEntry3);
    }

    public static k.b a(Map<l, Map<Long, String>> map) {
        map.put(TextStyle.FULL_STANDALONE, map.get(TextStyle.FULL));
        map.put(TextStyle.SHORT_STANDALONE, map.get(TextStyle.SHORT));
        if (map.containsKey(TextStyle.NARROW) && !map.containsKey(TextStyle.NARROW_STANDALONE)) {
            map.put(TextStyle.NARROW_STANDALONE, map.get(TextStyle.NARROW));
        }
        return new b(map);
    }
}
