package q.b.a.t;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import j.a.a.a.outline;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import l.a.a.a.o.b.AbstractSpiCall;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.LocalDate;
import q.b.a.LocalDateTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.t.DateTimeParseContext;
import q.b.a.t.SimpleDateTimeTextProvider;
import q.b.a.t.c;
import q.b.a.t.d;
import q.b.a.v.ChronoField;
import q.b.a.v.IsoFields;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;
import q.b.a.v.WeekFields;
import q.b.a.w.ZoneRulesProvider;

public final class DateTimeFormatterBuilder {
    public static final TemporalQuery<q.b.a.o> h = new a();

    /* renamed from: i  reason: collision with root package name */
    public static final Map<Character, q.b.a.v.j> f3133i;

    /* renamed from: j  reason: collision with root package name */
    public static final Comparator<String> f3134j = new c();
    public DateTimeFormatterBuilder a;
    public final DateTimeFormatterBuilder b;
    public final List<c.f> c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public int f3135e;

    /* renamed from: f  reason: collision with root package name */
    public char f3136f;
    public int g;

    public class a implements TemporalQuery<q.b.a.o> {
        public Object a(TemporalAccessor temporalAccessor) {
            ZoneId zoneId = (ZoneId) temporalAccessor.a(TemporalQueries.a);
            if (zoneId == null || (zoneId instanceof ZoneOffset)) {
                return null;
            }
            return zoneId;
        }
    }

    public class b extends DateTimeTextProvider {
        public final /* synthetic */ SimpleDateTimeTextProvider.b b;

        public b(DateTimeFormatterBuilder dateTimeFormatterBuilder, SimpleDateTimeTextProvider.b bVar) {
            this.b = bVar;
        }

        public String a(TemporalField temporalField, long j2, TextStyle textStyle, Locale locale) {
            return this.b.a(j2, textStyle);
        }

        public Iterator<Map.Entry<String, Long>> a(q.b.a.v.j jVar, l lVar, Locale locale) {
            List list = this.b.b.get(lVar);
            if (list != null) {
                return list.iterator();
            }
            return null;
        }
    }

    public class c implements Comparator<String> {
        public int compare(Object obj, Object obj2) {
            String str = (String) obj;
            String str2 = (String) obj2;
            return str.length() == str2.length() ? str.compareTo(str2) : str.length() - str2.length();
        }
    }

    public static final class d implements f {
        public final char b;

        public d(char c) {
            this.b = c;
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            sb.append(this.b);
            return true;
        }

        public String toString() {
            if (this.b == '\'') {
                return "''";
            }
            StringBuilder a = outline.a("'");
            a.append(this.b);
            a.append("'");
            return a.toString();
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            if (i2 == charSequence.length()) {
                return ~i2;
            }
            return !dateTimeParseContext.a(this.b, charSequence.charAt(i2)) ? ~i2 : i2 + 1;
        }
    }

    public interface f {
        int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2);

        boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb);
    }

    public enum n implements c.f {
        SENSITIVE,
        INSENSITIVE,
        STRICT,
        LENIENT;

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            int ordinal = ordinal();
            if (ordinal == 0) {
                dateTimeParseContext.f3144e = true;
            } else if (ordinal == 1) {
                dateTimeParseContext.f3144e = false;
            } else if (ordinal == 2) {
                dateTimeParseContext.f3145f = true;
            } else if (ordinal == 3) {
                dateTimeParseContext.f3145f = false;
            }
            return i2;
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            return true;
        }

        public String toString() {
            int ordinal = ordinal();
            if (ordinal == 0) {
                return "ParseCaseSensitive(true)";
            }
            if (ordinal == 1) {
                return "ParseCaseSensitive(false)";
            }
            if (ordinal == 2) {
                return "ParseStrict(true)";
            }
            if (ordinal == 3) {
                return "ParseStrict(false)";
            }
            throw new IllegalStateException("Unreachable");
        }
    }

    public static final class o implements f {
        public final String b;

        public o(String str) {
            this.b = str;
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            sb.append(this.b);
            return true;
        }

        public String toString() {
            return outline.a("'", this.b.replace("'", "''"), "'");
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            if (i2 > charSequence.length() || i2 < 0) {
                throw new IndexOutOfBoundsException();
            }
            String str = this.b;
            if (!dateTimeParseContext.a(charSequence, i2, str, 0, str.length())) {
                return ~i2;
            }
            return this.b.length() + i2;
        }
    }

    static {
        HashMap hashMap = new HashMap();
        f3133i = hashMap;
        hashMap.put('G', ChronoField.ERA);
        f3133i.put('y', ChronoField.YEAR_OF_ERA);
        f3133i.put('u', ChronoField.YEAR);
        f3133i.put('Q', IsoFields.a);
        f3133i.put('q', IsoFields.a);
        f3133i.put('M', ChronoField.MONTH_OF_YEAR);
        f3133i.put('L', ChronoField.MONTH_OF_YEAR);
        f3133i.put('D', ChronoField.DAY_OF_YEAR);
        f3133i.put('d', ChronoField.DAY_OF_MONTH);
        f3133i.put('F', ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH);
        f3133i.put('E', ChronoField.DAY_OF_WEEK);
        f3133i.put('c', ChronoField.DAY_OF_WEEK);
        f3133i.put('e', ChronoField.DAY_OF_WEEK);
        f3133i.put('a', ChronoField.AMPM_OF_DAY);
        f3133i.put('H', ChronoField.HOUR_OF_DAY);
        f3133i.put('k', ChronoField.CLOCK_HOUR_OF_DAY);
        f3133i.put('K', ChronoField.HOUR_OF_AMPM);
        f3133i.put('h', ChronoField.CLOCK_HOUR_OF_AMPM);
        f3133i.put('m', ChronoField.MINUTE_OF_HOUR);
        f3133i.put('s', ChronoField.SECOND_OF_MINUTE);
        f3133i.put('S', ChronoField.NANO_OF_SECOND);
        f3133i.put('A', ChronoField.MILLI_OF_DAY);
        f3133i.put('n', ChronoField.NANO_OF_SECOND);
        f3133i.put('N', ChronoField.NANO_OF_DAY);
    }

    public DateTimeFormatterBuilder() {
        this.a = this;
        this.c = new ArrayList();
        this.g = -1;
        this.b = null;
        this.d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatterBuilder a(TemporalField temporalField) {
        Collections.a((Object) temporalField, "field");
        a(new j(temporalField, 1, 19, SignStyle.NORMAL));
        return this;
    }

    /* JADX WARN: Type inference failed for: r4v12, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDate, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, boolean):q.b.a.t.DateTimeFormatterBuilder
     arg types: [?, int, int, int]
     candidates:
      q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, q.b.a.t.SignStyle):q.b.a.t.DateTimeFormatterBuilder
      q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, boolean):q.b.a.t.DateTimeFormatterBuilder */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x020e  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0213  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x03c9  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x03e2 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q.b.a.t.DateTimeFormatterBuilder b(java.lang.String r18) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            java.lang.String r2 = "pattern"
            n.i.Collections.a(r1, r2)
            r2 = 0
            r3 = 0
        L_0x000b:
            int r4 = r18.length()
            if (r3 >= r4) goto L_0x0438
            char r4 = r1.charAt(r3)
            r5 = 65
            r6 = 122(0x7a, float:1.71E-43)
            r7 = 90
            r8 = 97
            r9 = 1
            if (r4 < r5) goto L_0x0022
            if (r4 <= r7) goto L_0x0026
        L_0x0022:
            if (r4 < r8) goto L_0x039d
            if (r4 > r6) goto L_0x039d
        L_0x0026:
            int r10 = r3 + 1
        L_0x0028:
            int r11 = r18.length()
            if (r10 >= r11) goto L_0x0037
            char r11 = r1.charAt(r10)
            if (r11 != r4) goto L_0x0037
            int r10 = r10 + 1
            goto L_0x0028
        L_0x0037:
            int r3 = r10 - r3
            r11 = 112(0x70, float:1.57E-43)
            r12 = -1
            if (r4 != r11) goto L_0x0091
            int r11 = r18.length()
            if (r10 >= r11) goto L_0x0064
            char r4 = r1.charAt(r10)
            if (r4 < r5) goto L_0x004c
            if (r4 <= r7) goto L_0x0050
        L_0x004c:
            if (r4 < r8) goto L_0x0064
            if (r4 > r6) goto L_0x0064
        L_0x0050:
            int r5 = r10 + 1
        L_0x0052:
            int r11 = r18.length()
            if (r5 >= r11) goto L_0x0061
            char r11 = r1.charAt(r5)
            if (r11 != r4) goto L_0x0061
            int r5 = r5 + 1
            goto L_0x0052
        L_0x0061:
            int r10 = r5 - r10
            goto L_0x0068
        L_0x0064:
            r5 = 0
            r5 = r10
            r10 = r3
            r3 = 0
        L_0x0068:
            if (r3 == 0) goto L_0x0085
            if (r3 < r9) goto L_0x0079
            q.b.a.t.DateTimeFormatterBuilder r11 = r0.a
            r11.f3135e = r3
            r3 = 32
            r11.f3136f = r3
            r11.g = r12
            r3 = r10
            r10 = r5
            goto L_0x0091
        L_0x0079:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "The pad width must be at least one but was "
            java.lang.String r2 = j.a.a.a.outline.b(r2, r3)
            r1.<init>(r2)
            throw r1
        L_0x0085:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Pad letter 'p' must be followed by valid pad pattern: "
            java.lang.String r1 = j.a.a.a.outline.a(r3, r1)
            r2.<init>(r1)
            throw r2
        L_0x0091:
            java.util.Map<java.lang.Character, q.b.a.v.j> r5 = q.b.a.t.DateTimeFormatterBuilder.f3133i
            java.lang.Character r11 = java.lang.Character.valueOf(r4)
            java.lang.Object r5 = r5.get(r11)
            r12 = r5
            q.b.a.v.TemporalField r12 = (q.b.a.v.TemporalField) r12
            r5 = 5
            r11 = 4
            java.lang.String r13 = "Too many pattern letters: "
            r14 = 2
            if (r12 == 0) goto L_0x0271
            r6 = 81
            r7 = 3
            if (r4 == r6) goto L_0x023e
            r6 = 83
            if (r4 == r6) goto L_0x0237
            if (r4 == r8) goto L_0x0224
            r6 = 104(0x68, float:1.46E-43)
            if (r4 == r6) goto L_0x020c
            r6 = 107(0x6b, float:1.5E-43)
            if (r4 == r6) goto L_0x020c
            r6 = 109(0x6d, float:1.53E-43)
            if (r4 == r6) goto L_0x020c
            r6 = 113(0x71, float:1.58E-43)
            if (r4 == r6) goto L_0x01d9
            r6 = 115(0x73, float:1.61E-43)
            if (r4 == r6) goto L_0x020c
            r6 = 117(0x75, float:1.64E-43)
            if (r4 == r6) goto L_0x01a9
            r6 = 121(0x79, float:1.7E-43)
            if (r4 == r6) goto L_0x01a9
            switch(r4) {
                case 68: goto L_0x0116;
                case 69: goto L_0x00ed;
                case 70: goto L_0x00dc;
                case 71: goto L_0x00ed;
                case 72: goto L_0x020c;
                default: goto L_0x00cf;
            }
        L_0x00cf:
            switch(r4) {
                case 75: goto L_0x020c;
                case 76: goto L_0x01d9;
                case 77: goto L_0x023e;
                default: goto L_0x00d2;
            }
        L_0x00d2:
            switch(r4) {
                case 99: goto L_0x0163;
                case 100: goto L_0x020c;
                case 101: goto L_0x012e;
                default: goto L_0x00d5;
            }
        L_0x00d5:
            if (r3 != r9) goto L_0x01a4
            r0.a(r12)
            goto L_0x038d
        L_0x00dc:
            if (r3 != r9) goto L_0x00e3
            r0.a(r12)
            goto L_0x038d
        L_0x00e3:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x00ed:
            if (r3 == r9) goto L_0x010f
            if (r3 == r14) goto L_0x010f
            if (r3 == r7) goto L_0x010f
            if (r3 == r11) goto L_0x0108
            if (r3 != r5) goto L_0x00fe
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.NARROW
            r0.a(r12, r3)
            goto L_0x038d
        L_0x00fe:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0108:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL
            r0.a(r12, r3)
            goto L_0x038d
        L_0x010f:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0116:
            if (r3 != r9) goto L_0x011d
            r0.a(r12)
            goto L_0x038d
        L_0x011d:
            if (r3 > r7) goto L_0x0124
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0124:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x012e:
            if (r3 == r9) goto L_0x0157
            if (r3 == r14) goto L_0x0157
            if (r3 == r7) goto L_0x0150
            if (r3 == r11) goto L_0x0149
            if (r3 != r5) goto L_0x013f
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.NARROW
            r0.a(r12, r3)
            goto L_0x038d
        L_0x013f:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0149:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0150:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0157:
            q.b.a.t.DateTimeFormatterBuilder$q r4 = new q.b.a.t.DateTimeFormatterBuilder$q
            r5 = 101(0x65, float:1.42E-43)
            r4.<init>(r5, r3)
            r0.a(r4)
            goto L_0x038d
        L_0x0163:
            if (r3 == r9) goto L_0x0198
            if (r3 == r14) goto L_0x018c
            if (r3 == r7) goto L_0x0185
            if (r3 == r11) goto L_0x017e
            if (r3 != r5) goto L_0x0174
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.NARROW_STANDALONE
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0174:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x017e:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL_STANDALONE
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0185:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT_STANDALONE
            r0.a(r12, r3)
            goto L_0x038d
        L_0x018c:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Invalid number of pattern letters: "
            java.lang.String r2 = j.a.a.a.outline.a(r2, r4)
            r1.<init>(r2)
            throw r1
        L_0x0198:
            q.b.a.t.DateTimeFormatterBuilder$q r4 = new q.b.a.t.DateTimeFormatterBuilder$q
            r5 = 99
            r4.<init>(r5, r3)
            r0.a(r4)
            goto L_0x038d
        L_0x01a4:
            r0.a(r12, r3)
            goto L_0x038d
        L_0x01a9:
            if (r3 != r14) goto L_0x01c7
            r13 = 2
            r14 = 2
            q.b.a.LocalDate r3 = q.b.a.t.DateTimeFormatterBuilder.m.f3141j
            java.lang.String r4 = "field"
            n.i.Collections.a(r12, r4)
            java.lang.String r4 = "baseDate"
            n.i.Collections.a(r3, r4)
            q.b.a.t.DateTimeFormatterBuilder$m r4 = new q.b.a.t.DateTimeFormatterBuilder$m
            r15 = 0
            r11 = r4
            r16 = r3
            r11.<init>(r12, r13, r14, r15, r16)
            r0.a(r4)
            goto L_0x038d
        L_0x01c7:
            r4 = 19
            if (r3 >= r11) goto L_0x01d2
            q.b.a.t.SignStyle r5 = q.b.a.t.SignStyle.NORMAL
            r0.a(r12, r3, r4, r5)
            goto L_0x038d
        L_0x01d2:
            q.b.a.t.SignStyle r5 = q.b.a.t.SignStyle.EXCEEDS_PAD
            r0.a(r12, r3, r4, r5)
            goto L_0x038d
        L_0x01d9:
            if (r3 == r9) goto L_0x0207
            if (r3 == r14) goto L_0x0202
            if (r3 == r7) goto L_0x01fb
            if (r3 == r11) goto L_0x01f4
            if (r3 != r5) goto L_0x01ea
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.NARROW_STANDALONE
            r0.a(r12, r3)
            goto L_0x038d
        L_0x01ea:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x01f4:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL_STANDALONE
            r0.a(r12, r3)
            goto L_0x038d
        L_0x01fb:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT_STANDALONE
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0202:
            r0.a(r12, r14)
            goto L_0x038d
        L_0x0207:
            r0.a(r12)
            goto L_0x038d
        L_0x020c:
            if (r3 != r9) goto L_0x0213
            r0.a(r12)
            goto L_0x038d
        L_0x0213:
            if (r3 != r14) goto L_0x021a
            r0.a(r12, r3)
            goto L_0x038d
        L_0x021a:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0224:
            if (r3 != r9) goto L_0x022d
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT
            r0.a(r12, r3)
            goto L_0x038d
        L_0x022d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0237:
            q.b.a.v.ChronoField r4 = q.b.a.v.ChronoField.NANO_OF_SECOND
            r0.a(r4, r3, r3, r2)
            goto L_0x038d
        L_0x023e:
            if (r3 == r9) goto L_0x026c
            if (r3 == r14) goto L_0x0267
            if (r3 == r7) goto L_0x0260
            if (r3 == r11) goto L_0x0259
            if (r3 != r5) goto L_0x024f
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.NARROW
            r0.a(r12, r3)
            goto L_0x038d
        L_0x024f:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0259:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0260:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT
            r0.a(r12, r3)
            goto L_0x038d
        L_0x0267:
            r0.a(r12, r14)
            goto L_0x038d
        L_0x026c:
            r0.a(r12)
            goto L_0x038d
        L_0x0271:
            if (r4 != r6) goto L_0x0299
            if (r3 > r11) goto L_0x028f
            if (r3 != r11) goto L_0x0283
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL
            q.b.a.t.DateTimeFormatterBuilder$s r4 = new q.b.a.t.DateTimeFormatterBuilder$s
            r4.<init>(r3)
            r0.a(r4)
            goto L_0x038d
        L_0x0283:
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT
            q.b.a.t.DateTimeFormatterBuilder$s r4 = new q.b.a.t.DateTimeFormatterBuilder$s
            r4.<init>(r3)
            r0.a(r4)
            goto L_0x038d
        L_0x028f:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0299:
            r6 = 86
            if (r4 != r6) goto L_0x02b9
            if (r3 != r14) goto L_0x02ad
            q.b.a.t.DateTimeFormatterBuilder$r r3 = new q.b.a.t.DateTimeFormatterBuilder$r
            q.b.a.v.TemporalQuery<q.b.a.o> r4 = q.b.a.v.TemporalQueries.a
            java.lang.String r5 = "ZoneId()"
            r3.<init>(r4, r5)
            r0.a(r3)
            goto L_0x038d
        L_0x02ad:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Pattern letter count must be 2: "
            java.lang.String r2 = j.a.a.a.outline.a(r2, r4)
            r1.<init>(r2)
            throw r1
        L_0x02b9:
            java.lang.String r6 = "Z"
            java.lang.String r8 = "+0000"
            if (r4 != r7) goto L_0x02e4
            if (r3 >= r11) goto L_0x02c8
            java.lang.String r3 = "+HHMM"
            r0.a(r3, r8)
            goto L_0x038d
        L_0x02c8:
            if (r3 != r11) goto L_0x02d1
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL
            r0.a(r3)
            goto L_0x038d
        L_0x02d1:
            if (r3 != r5) goto L_0x02da
            java.lang.String r3 = "+HH:MM:ss"
            r0.a(r3, r6)
            goto L_0x038d
        L_0x02da:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x02e4:
            r7 = 79
            if (r4 != r7) goto L_0x0306
            if (r3 != r9) goto L_0x02f1
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.SHORT
            r0.a(r3)
            goto L_0x038d
        L_0x02f1:
            if (r3 != r11) goto L_0x02fa
            q.b.a.t.TextStyle r3 = q.b.a.t.TextStyle.FULL
            r0.a(r3)
            goto L_0x038d
        L_0x02fa:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Pattern letter count must be 1 or 4: "
            java.lang.String r2 = j.a.a.a.outline.a(r2, r4)
            r1.<init>(r2)
            throw r1
        L_0x0306:
            r7 = 88
            if (r4 != r7) goto L_0x0325
            if (r3 > r5) goto L_0x031b
            java.lang.String[] r4 = q.b.a.t.DateTimeFormatterBuilder.k.d
            if (r3 != r9) goto L_0x0312
            r5 = 0
            goto L_0x0313
        L_0x0312:
            r5 = 1
        L_0x0313:
            int r3 = r3 + r5
            r3 = r4[r3]
            r0.a(r3, r6)
            goto L_0x038d
        L_0x031b:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0325:
            r6 = 120(0x78, float:1.68E-43)
            if (r4 != r6) goto L_0x034f
            if (r3 > r5) goto L_0x0345
            if (r3 != r9) goto L_0x0330
            java.lang.String r8 = "+00"
            goto L_0x0337
        L_0x0330:
            int r4 = r3 % 2
            if (r4 != 0) goto L_0x0335
            goto L_0x0337
        L_0x0335:
            java.lang.String r8 = "+00:00"
        L_0x0337:
            java.lang.String[] r4 = q.b.a.t.DateTimeFormatterBuilder.k.d
            if (r3 != r9) goto L_0x033d
            r5 = 0
            goto L_0x033e
        L_0x033d:
            r5 = 1
        L_0x033e:
            int r3 = r3 + r5
            r3 = r4[r3]
            r0.a(r3, r8)
            goto L_0x038d
        L_0x0345:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x034f:
            r5 = 87
            if (r4 != r5) goto L_0x0368
            if (r3 > r9) goto L_0x035e
            q.b.a.t.DateTimeFormatterBuilder$q r4 = new q.b.a.t.DateTimeFormatterBuilder$q
            r4.<init>(r5, r3)
            r0.a(r4)
            goto L_0x038d
        L_0x035e:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0368:
            r5 = 119(0x77, float:1.67E-43)
            if (r4 != r5) goto L_0x0381
            if (r3 > r14) goto L_0x0377
            q.b.a.t.DateTimeFormatterBuilder$q r4 = new q.b.a.t.DateTimeFormatterBuilder$q
            r4.<init>(r5, r3)
            r0.a(r4)
            goto L_0x038d
        L_0x0377:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = j.a.a.a.outline.a(r13, r4)
            r1.<init>(r2)
            throw r1
        L_0x0381:
            r5 = 89
            if (r4 != r5) goto L_0x0391
            q.b.a.t.DateTimeFormatterBuilder$q r4 = new q.b.a.t.DateTimeFormatterBuilder$q
            r4.<init>(r5, r3)
            r0.a(r4)
        L_0x038d:
            int r3 = r10 + -1
            goto L_0x041b
        L_0x0391:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Unknown pattern letter: "
            java.lang.String r2 = j.a.a.a.outline.a(r2, r4)
            r1.<init>(r2)
            throw r1
        L_0x039d:
            java.lang.String r5 = "'"
            r6 = 39
            if (r4 != r6) goto L_0x03ee
            int r3 = r3 + 1
            r4 = r3
        L_0x03a6:
            int r7 = r18.length()
            if (r4 >= r7) goto L_0x03c3
            char r7 = r1.charAt(r4)
            if (r7 != r6) goto L_0x03c1
            int r7 = r4 + 1
            int r8 = r18.length()
            if (r7 >= r8) goto L_0x03c3
            char r8 = r1.charAt(r7)
            if (r8 != r6) goto L_0x03c3
            r4 = r7
        L_0x03c1:
            int r4 = r4 + r9
            goto L_0x03a6
        L_0x03c3:
            int r7 = r18.length()
            if (r4 >= r7) goto L_0x03e2
            java.lang.String r3 = r1.substring(r3, r4)
            int r7 = r3.length()
            if (r7 != 0) goto L_0x03d7
            r0.a(r6)
            goto L_0x03e0
        L_0x03d7:
            java.lang.String r6 = "''"
            java.lang.String r3 = r3.replace(r6, r5)
            r0.a(r3)
        L_0x03e0:
            r3 = r4
            goto L_0x041b
        L_0x03e2:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Pattern ends with an incomplete string literal: "
            java.lang.String r1 = j.a.a.a.outline.a(r3, r1)
            r2.<init>(r1)
            throw r2
        L_0x03ee:
            r6 = 91
            if (r4 != r6) goto L_0x03f6
            r17.b()
            goto L_0x041b
        L_0x03f6:
            r6 = 93
            if (r4 != r6) goto L_0x040c
            q.b.a.t.DateTimeFormatterBuilder r4 = r0.a
            q.b.a.t.DateTimeFormatterBuilder r4 = r4.b
            if (r4 == 0) goto L_0x0404
            r17.a()
            goto L_0x041b
        L_0x0404:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Pattern invalid as it contains ] without previous ["
            r1.<init>(r2)
            throw r1
        L_0x040c:
            r6 = 123(0x7b, float:1.72E-43)
            if (r4 == r6) goto L_0x041e
            r6 = 125(0x7d, float:1.75E-43)
            if (r4 == r6) goto L_0x041e
            r6 = 35
            if (r4 == r6) goto L_0x041e
            r0.a(r4)
        L_0x041b:
            int r3 = r3 + r9
            goto L_0x000b
        L_0x041e:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Pattern includes reserved character: '"
            r2.append(r3)
            r2.append(r4)
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0438:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.b(java.lang.String):q.b.a.t.DateTimeFormatterBuilder");
    }

    public DateTimeFormatter c() {
        return a(Locale.getDefault());
    }

    public static class j implements f {
        public static final int[] g = {0, 10, 100, AnswersRetryFilesSender.BACKOFF_MS, AbstractSpiCall.DEFAULT_TIMEOUT, 100000, 1000000, 10000000, 100000000, 1000000000};
        public final TemporalField b;
        public final int c;
        public final int d;

        /* renamed from: e  reason: collision with root package name */
        public final SignStyle f3138e;

        /* renamed from: f  reason: collision with root package name */
        public final int f3139f;

        public j(TemporalField temporalField, int i2, int i3, SignStyle signStyle) {
            this.b = temporalField;
            this.c = i2;
            this.d = i3;
            this.f3138e = signStyle;
            this.f3139f = 0;
        }

        public long a(DateTimePrintContext dateTimePrintContext, long j2) {
            return j2;
        }

        public j a() {
            if (this.f3139f == -1) {
                return this;
            }
            return new j(this.b, this.c, this.d, this.f3138e, -1);
        }

        public String toString() {
            if (this.c == 1 && this.d == 19 && this.f3138e == SignStyle.NORMAL) {
                StringBuilder a = outline.a("Value(");
                a.append(this.b);
                a.append(")");
                return a.toString();
            } else if (this.c == this.d && this.f3138e == SignStyle.NOT_NEGATIVE) {
                StringBuilder a2 = outline.a("Value(");
                a2.append(this.b);
                a2.append(",");
                a2.append(this.c);
                a2.append(")");
                return a2.toString();
            } else {
                StringBuilder a3 = outline.a("Value(");
                a3.append(this.b);
                a3.append(",");
                a3.append(this.c);
                a3.append(",");
                a3.append(this.d);
                a3.append(",");
                a3.append(this.f3138e);
                a3.append(")");
                return a3.toString();
            }
        }

        public j a(int i2) {
            return new j(this.b, this.c, this.d, this.f3138e, this.f3139f + i2);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0070, code lost:
            if (r4 != 4) goto L_0x0096;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(q.b.a.t.DateTimePrintContext r12, java.lang.StringBuilder r13) {
            /*
                r11 = this;
                q.b.a.v.TemporalField r0 = r11.b
                java.lang.Long r0 = r12.a(r0)
                r1 = 0
                if (r0 != 0) goto L_0x000a
                return r1
            L_0x000a:
                long r2 = r0.longValue()
                long r2 = r11.a(r12, r2)
                q.b.a.t.DecimalStyle r12 = r12.c
                r4 = -9223372036854775808
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 != 0) goto L_0x001d
                java.lang.String r0 = "9223372036854775808"
                goto L_0x0025
            L_0x001d:
                long r4 = java.lang.Math.abs(r2)
                java.lang.String r0 = java.lang.Long.toString(r4)
            L_0x0025:
                int r4 = r0.length()
                int r5 = r11.d
                java.lang.String r6 = " cannot be printed as the value "
                java.lang.String r7 = "Field "
                if (r4 > r5) goto L_0x00ab
                java.lang.String r0 = r12.a(r0)
                r4 = 0
                r8 = 4
                r9 = 1
                int r10 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r10 < 0) goto L_0x0063
                q.b.a.t.SignStyle r4 = r11.f3138e
                int r4 = r4.ordinal()
                if (r4 == r9) goto L_0x005d
                if (r4 == r8) goto L_0x0048
                goto L_0x0096
            L_0x0048:
                int r4 = r11.c
                r5 = 19
                if (r4 >= r5) goto L_0x0096
                int[] r5 = q.b.a.t.DateTimeFormatterBuilder.j.g
                r4 = r5[r4]
                long r4 = (long) r4
                int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r6 < 0) goto L_0x0096
                char r2 = r12.b
                r13.append(r2)
                goto L_0x0096
            L_0x005d:
                char r2 = r12.b
                r13.append(r2)
                goto L_0x0096
            L_0x0063:
                q.b.a.t.SignStyle r4 = r11.f3138e
                int r4 = r4.ordinal()
                if (r4 == 0) goto L_0x0091
                if (r4 == r9) goto L_0x0091
                r5 = 3
                if (r4 == r5) goto L_0x0073
                if (r4 == r8) goto L_0x0091
                goto L_0x0096
            L_0x0073:
                org.threeten.bp.DateTimeException r12 = new org.threeten.bp.DateTimeException
                java.lang.StringBuilder r13 = j.a.a.a.outline.a(r7)
                q.b.a.v.TemporalField r0 = r11.b
                r13.append(r0)
                r13.append(r6)
                r13.append(r2)
                java.lang.String r0 = " cannot be negative according to the SignStyle"
                r13.append(r0)
                java.lang.String r13 = r13.toString()
                r12.<init>(r13)
                throw r12
            L_0x0091:
                char r2 = r12.c
                r13.append(r2)
            L_0x0096:
                int r2 = r11.c
                int r3 = r0.length()
                int r2 = r2 - r3
                if (r1 >= r2) goto L_0x00a7
                char r2 = r12.a
                r13.append(r2)
                int r1 = r1 + 1
                goto L_0x0096
            L_0x00a7:
                r13.append(r0)
                return r9
            L_0x00ab:
                org.threeten.bp.DateTimeException r12 = new org.threeten.bp.DateTimeException
                java.lang.StringBuilder r13 = j.a.a.a.outline.a(r7)
                q.b.a.v.TemporalField r0 = r11.b
                r13.append(r0)
                r13.append(r6)
                r13.append(r2)
                java.lang.String r0 = " exceeds the maximum print width of "
                r13.append(r0)
                int r0 = r11.d
                r13.append(r0)
                java.lang.String r13 = r13.toString()
                r12.<init>(r13)
                throw r12
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.j.a(q.b.a.t.DateTimePrintContext, java.lang.StringBuilder):boolean");
        }

        public j(TemporalField temporalField, int i2, int i3, SignStyle signStyle, int i4) {
            this.b = temporalField;
            this.c = i2;
            this.d = i3;
            this.f3138e = signStyle;
            this.f3139f = i4;
        }

        public /* synthetic */ j(TemporalField temporalField, int i2, int i3, SignStyle signStyle, int i4, a aVar) {
            this.b = temporalField;
            this.c = i2;
            this.d = i3;
            this.f3138e = signStyle;
            this.f3139f = i4;
        }

        public boolean a(DateTimeParseContext dateTimeParseContext) {
            int i2 = this.f3139f;
            return i2 == -1 || (i2 > 0 && this.c == this.d && this.f3138e == SignStyle.NOT_NEGATIVE);
        }

        /* JADX WARNING: Removed duplicated region for block: B:110:0x0149  */
        /* JADX WARNING: Removed duplicated region for block: B:115:0x0166  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int a(q.b.a.t.DateTimeParseContext r20, java.lang.CharSequence r21, int r22) {
            /*
                r19 = this;
                r6 = r19
                r1 = r20
                r0 = r22
                int r2 = r21.length()
                if (r0 != r2) goto L_0x000e
                int r0 = ~r0
                return r0
            L_0x000e:
                char r3 = r21.charAt(r22)
                q.b.a.t.DecimalStyle r4 = r1.b
                char r5 = r4.b
                r7 = 0
                r8 = 1
                if (r3 != r5) goto L_0x0034
                q.b.a.t.SignStyle r3 = r6.f3138e
                boolean r4 = r1.f3145f
                int r5 = r6.c
                int r9 = r6.d
                if (r5 != r9) goto L_0x0026
                r5 = 1
                goto L_0x0027
            L_0x0026:
                r5 = 0
            L_0x0027:
                boolean r3 = r3.a(r8, r4, r5)
                if (r3 != 0) goto L_0x002f
                int r0 = ~r0
                return r0
            L_0x002f:
                int r0 = r0 + r8
                r4 = r0
                r0 = 1
                r3 = 0
                goto L_0x0060
            L_0x0034:
                char r4 = r4.c
                if (r3 != r4) goto L_0x0051
                q.b.a.t.SignStyle r3 = r6.f3138e
                boolean r4 = r1.f3145f
                int r5 = r6.c
                int r9 = r6.d
                if (r5 != r9) goto L_0x0044
                r5 = 1
                goto L_0x0045
            L_0x0044:
                r5 = 0
            L_0x0045:
                boolean r3 = r3.a(r7, r4, r5)
                if (r3 != 0) goto L_0x004d
                int r0 = ~r0
                return r0
            L_0x004d:
                int r0 = r0 + 1
                r3 = 1
                goto L_0x005e
            L_0x0051:
                q.b.a.t.SignStyle r3 = r6.f3138e
                q.b.a.t.SignStyle r4 = q.b.a.t.SignStyle.ALWAYS
                if (r3 != r4) goto L_0x005d
                boolean r3 = r1.f3145f
                if (r3 == 0) goto L_0x005d
                int r0 = ~r0
                return r0
            L_0x005d:
                r3 = 0
            L_0x005e:
                r4 = r0
                r0 = 0
            L_0x0060:
                boolean r5 = r1.f3145f
                if (r5 != 0) goto L_0x006d
                boolean r5 = r19.a(r20)
                if (r5 == 0) goto L_0x006b
                goto L_0x006d
            L_0x006b:
                r5 = 1
                goto L_0x006f
            L_0x006d:
                int r5 = r6.c
            L_0x006f:
                int r9 = r4 + r5
                if (r9 <= r2) goto L_0x0075
                int r0 = ~r4
                return r0
            L_0x0075:
                boolean r10 = r1.f3145f
                r11 = 9
                if (r10 != 0) goto L_0x0085
                boolean r10 = r19.a(r20)
                if (r10 == 0) goto L_0x0082
                goto L_0x0085
            L_0x0082:
                r10 = 9
                goto L_0x0087
            L_0x0085:
                int r10 = r6.d
            L_0x0087:
                int r12 = r6.f3139f
                int r12 = java.lang.Math.max(r12, r7)
                int r12 = r12 + r10
            L_0x008e:
                r10 = 0
                r13 = 2
                if (r7 >= r13) goto L_0x00fd
                int r12 = r12 + r4
                int r12 = java.lang.Math.min(r12, r2)
                r13 = r4
                r16 = 0
            L_0x009a:
                if (r13 >= r12) goto L_0x00e3
                int r18 = r13 + 1
                r14 = r21
                char r13 = r14.charAt(r13)
                q.b.a.t.DecimalStyle r15 = r1.b
                char r15 = r15.a
                int r13 = r13 - r15
                if (r13 < 0) goto L_0x00ae
                if (r13 > r11) goto L_0x00ae
                goto L_0x00af
            L_0x00ae:
                r13 = -1
            L_0x00af:
                if (r13 >= 0) goto L_0x00b7
                int r13 = r18 + -1
                if (r13 >= r9) goto L_0x00e5
                int r0 = ~r4
                return r0
            L_0x00b7:
                int r15 = r18 - r4
                r11 = 18
                if (r15 <= r11) goto L_0x00d4
                if (r10 != 0) goto L_0x00c3
                java.math.BigInteger r10 = java.math.BigInteger.valueOf(r16)
            L_0x00c3:
                java.math.BigInteger r11 = java.math.BigInteger.TEN
                java.math.BigInteger r10 = r10.multiply(r11)
                r15 = r9
                long r8 = (long) r13
                java.math.BigInteger r8 = java.math.BigInteger.valueOf(r8)
                java.math.BigInteger r10 = r10.add(r8)
                goto L_0x00dc
            L_0x00d4:
                r15 = r9
                r8 = 10
                long r16 = r16 * r8
                long r8 = (long) r13
                long r16 = r16 + r8
            L_0x00dc:
                r9 = r15
                r13 = r18
                r8 = 1
                r11 = 9
                goto L_0x009a
            L_0x00e3:
                r14 = r21
            L_0x00e5:
                r15 = r9
                int r8 = r6.f3139f
                if (r8 <= 0) goto L_0x00f9
                if (r7 != 0) goto L_0x00f9
                int r13 = r13 - r4
                int r13 = r13 - r8
                int r12 = java.lang.Math.max(r5, r13)
                int r7 = r7 + 1
                r9 = r15
                r8 = 1
                r11 = 9
                goto L_0x008e
            L_0x00f9:
                r5 = r13
                r7 = r16
                goto L_0x0100
            L_0x00fd:
                r5 = r4
                r7 = 0
            L_0x0100:
                if (r3 == 0) goto L_0x012a
                if (r10 == 0) goto L_0x011a
                java.math.BigInteger r0 = java.math.BigInteger.ZERO
                boolean r0 = r10.equals(r0)
                if (r0 == 0) goto L_0x0114
                boolean r0 = r1.f3145f
                if (r0 == 0) goto L_0x0114
                r0 = 1
                int r4 = r4 - r0
                int r0 = ~r4
                return r0
            L_0x0114:
                java.math.BigInteger r0 = r10.negate()
                r10 = r0
                goto L_0x0146
            L_0x011a:
                r0 = 1
                r2 = 0
                int r9 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
                if (r9 != 0) goto L_0x0128
                boolean r2 = r1.f3145f
                if (r2 == 0) goto L_0x0128
                int r4 = r4 - r0
                int r0 = ~r4
                return r0
            L_0x0128:
                long r2 = -r7
                goto L_0x0147
            L_0x012a:
                q.b.a.t.SignStyle r2 = r6.f3138e
                q.b.a.t.SignStyle r3 = q.b.a.t.SignStyle.EXCEEDS_PAD
                if (r2 != r3) goto L_0x0146
                boolean r2 = r1.f3145f
                if (r2 == 0) goto L_0x0146
                int r2 = r5 - r4
                if (r0 == 0) goto L_0x0140
                int r0 = r6.c
                if (r2 > r0) goto L_0x0146
                r0 = 1
                int r4 = r4 - r0
                int r0 = ~r4
                return r0
            L_0x0140:
                int r0 = r6.c
                if (r2 <= r0) goto L_0x0146
                int r0 = ~r4
                return r0
            L_0x0146:
                r2 = r7
            L_0x0147:
                if (r10 == 0) goto L_0x0166
                int r0 = r10.bitLength()
                r2 = 63
                if (r0 <= r2) goto L_0x0159
                java.math.BigInteger r0 = java.math.BigInteger.TEN
                java.math.BigInteger r10 = r10.divide(r0)
                int r5 = r5 + -1
            L_0x0159:
                long r2 = r10.longValue()
                r0 = r19
                r1 = r20
                int r0 = r0.a(r1, r2, r4, r5)
                return r0
            L_0x0166:
                r0 = r19
                r1 = r20
                int r0 = r0.a(r1, r2, r4, r5)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.j.a(q.b.a.t.DateTimeParseContext, java.lang.CharSequence, int):int");
        }

        public int a(DateTimeParseContext dateTimeParseContext, long j2, int i2, int i3) {
            return dateTimeParseContext.a(this.b, j2, i2, i3);
        }
    }

    public static final class m extends j {

        /* renamed from: j  reason: collision with root package name */
        public static final LocalDate f3141j = LocalDate.a((int) RecyclerView.MAX_SCROLL_DURATION, 1, 1);
        public final int h;

        /* renamed from: i  reason: collision with root package name */
        public final ChronoLocalDate f3142i;

        public m(TemporalField temporalField, int i2, int i3, int i4, ChronoLocalDate chronoLocalDate) {
            super(temporalField, i2, i3, SignStyle.NOT_NEGATIVE);
            if (i2 < 1 || i2 > 10) {
                throw new IllegalArgumentException(outline.b("The width must be from 1 to 10 inclusive but was ", i2));
            } else if (i3 < 1 || i3 > 10) {
                throw new IllegalArgumentException(outline.b("The maxWidth must be from 1 to 10 inclusive but was ", i3));
            } else if (i3 >= i2) {
                if (chronoLocalDate == null) {
                    long j2 = (long) i4;
                    if (!temporalField.g().a(j2)) {
                        throw new IllegalArgumentException("The base value must be within the range of the field");
                    } else if (j2 + ((long) j.g[i2]) > 2147483647L) {
                        throw new DateTimeException("Unable to add printer-parser as the range exceeds the capacity of an int");
                    }
                }
                this.h = i4;
                this.f3142i = chronoLocalDate;
            } else {
                throw new IllegalArgumentException("The maxWidth must be greater than the width");
            }
        }

        public boolean a(DateTimeParseContext dateTimeParseContext) {
            if (!dateTimeParseContext.f3145f) {
                return false;
            }
            return super.a(dateTimeParseContext);
        }

        public String toString() {
            StringBuilder a = outline.a("ReducedValue(");
            a.append(super.b);
            a.append(",");
            a.append(super.c);
            a.append(",");
            a.append(super.d);
            a.append(",");
            Object obj = this.f3142i;
            if (obj == null) {
                obj = Integer.valueOf(this.h);
            }
            a.append(obj);
            a.append(")");
            return a.toString();
        }

        /* JADX WARN: Type inference failed for: r2v4, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r7v10, types: [q.b.a.s.ChronoLocalDate, q.b.a.u.DefaultInterfaceTemporalAccessor] */
        public long a(DateTimePrintContext dateTimePrintContext, long j2) {
            long abs = Math.abs(j2);
            int i2 = this.h;
            if (this.f3142i != null) {
                i2 = Chronology.c(dateTimePrintContext.a).a((TemporalAccessor) this.f3142i).b(super.b);
            }
            if (j2 >= ((long) i2)) {
                int[] iArr = j.g;
                int i3 = super.c;
                if (j2 < ((long) (i2 + iArr[i3]))) {
                    return abs % ((long) iArr[i3]);
                }
            }
            return abs % ((long) j.g[super.d]);
        }

        public m(TemporalField temporalField, int i2, int i3, int i4, ChronoLocalDate chronoLocalDate, int i5) {
            super(temporalField, i2, i3, SignStyle.NOT_NEGATIVE, i5, null);
            this.h = i4;
            this.f3142i = chronoLocalDate;
        }

        /* JADX WARN: Type inference failed for: r1v7, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
        /* JADX WARN: Type inference failed for: r0v7, types: [q.b.a.s.ChronoLocalDate, q.b.a.u.DefaultInterfaceTemporalAccessor] */
        public int a(DateTimeParseContext dateTimeParseContext, long j2, int i2, int i3) {
            int i4 = this.h;
            if (this.f3142i != null) {
                Chronology chronology = dateTimeParseContext.a().b;
                if (chronology == null && (chronology = dateTimeParseContext.c) == null) {
                    chronology = IsoChronology.d;
                }
                i4 = chronology.a((TemporalAccessor) this.f3142i).b(super.b);
                DateTimeParseContext.a a = dateTimeParseContext.a();
                if (a.g == null) {
                    a.g = new ArrayList(2);
                }
                a.g.add(new Object[]{this, Long.valueOf(j2), Integer.valueOf(i2), Integer.valueOf(i3)});
            }
            int i5 = i3 - i2;
            int i6 = super.c;
            if (i5 == i6 && j2 >= 0) {
                long j3 = (long) j.g[i6];
                long j4 = (long) i4;
                long j5 = j4 - (j4 % j3);
                j2 = i4 > 0 ? j5 + j2 : j5 - j2;
                if (j2 < j4) {
                    j2 += j3;
                }
            }
            return dateTimeParseContext.a(super.b, j2, i2, i3);
        }

        public j a() {
            if (super.f3139f == -1) {
                return super;
            }
            return new m(super.b, super.c, super.d, this.h, this.f3142i, -1);
        }

        public j a(int i2) {
            return new m(super.b, super.c, super.d, this.h, this.f3142i, super.f3139f + i2);
        }
    }

    public static final class r implements f {
        public static volatile Map.Entry<Integer, c.r.a> d;
        public final TemporalQuery<q.b.a.o> b;
        public final String c;

        public r(TemporalQuery<q.b.a.o> temporalQuery, String str) {
            this.b = temporalQuery;
            this.c = str;
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            ZoneId zoneId = (ZoneId) dateTimePrintContext.a(this.b);
            if (zoneId == null) {
                return false;
            }
            sb.append(zoneId.f());
            return true;
        }

        public String toString() {
            return this.c;
        }

        /* JADX WARN: Type inference failed for: r13v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r13v4, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
        /* JADX WARN: Type inference failed for: r12v9, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            c.r.a aVar;
            int i3;
            int length = charSequence.length();
            if (i2 > length) {
                throw new IndexOutOfBoundsException();
            } else if (i2 == length) {
                return ~i2;
            } else {
                char charAt = charSequence.charAt(i2);
                String str = null;
                if (charAt != '+' && charAt != '-') {
                    int i4 = i2 + 2;
                    if (length >= i4) {
                        char charAt2 = charSequence.charAt(i2 + 1);
                        if (dateTimeParseContext.a(charAt, 'U') && dateTimeParseContext.a(charAt2, 'T')) {
                            int i5 = i2 + 3;
                            if (length < i5 || !dateTimeParseContext.a(charSequence.charAt(i4), 'C')) {
                                return a(dateTimeParseContext, charSequence, i2, i4);
                            }
                            return a(dateTimeParseContext, charSequence, i2, i5);
                        } else if (dateTimeParseContext.a(charAt, 'G') && length >= (i3 = i2 + 3) && dateTimeParseContext.a(charAt2, 'M') && dateTimeParseContext.a(charSequence.charAt(i4), 'T')) {
                            return a(dateTimeParseContext, charSequence, i2, i3);
                        }
                    }
                    Set unmodifiableSet = java.util.Collections.unmodifiableSet(ZoneRulesProvider.b.keySet());
                    int size = unmodifiableSet.size();
                    Map.Entry<Integer, c.r.a> entry = d;
                    if (entry == null || entry.getKey().intValue() != size) {
                        synchronized (this) {
                            entry = d;
                            if (entry == null || entry.getKey().intValue() != size) {
                                Integer valueOf = Integer.valueOf(size);
                                ArrayList arrayList = new ArrayList(unmodifiableSet);
                                java.util.Collections.sort(arrayList, DateTimeFormatterBuilder.f3134j);
                                a aVar2 = new a(((String) arrayList.get(0)).length(), null);
                                Iterator it = arrayList.iterator();
                                while (it.hasNext()) {
                                    aVar2.a((String) it.next());
                                }
                                entry = new AbstractMap.SimpleImmutableEntry<>(valueOf, aVar2);
                                d = entry;
                            }
                        }
                    }
                    a value = entry.getValue();
                    String str2 = null;
                    while (value != null) {
                        int i6 = value.a + i2;
                        if (i6 > length) {
                            break;
                        }
                        String charSequence2 = charSequence.subSequence(i2, i6).toString();
                        if (dateTimeParseContext.f3144e) {
                            aVar = value.b.get(charSequence2);
                        } else {
                            aVar = value.c.get(charSequence2.toString().toLowerCase(Locale.ENGLISH));
                        }
                        value = (a) aVar;
                        String str3 = charSequence2;
                        str2 = str;
                        str = str3;
                    }
                    ZoneId a2 = a(unmodifiableSet, str, dateTimeParseContext.f3144e);
                    if (a2 == null) {
                        a2 = a(unmodifiableSet, str2, dateTimeParseContext.f3144e);
                        if (a2 != null) {
                            str = str2;
                        } else if (!dateTimeParseContext.a(charAt, 'Z')) {
                            return ~i2;
                        } else {
                            dateTimeParseContext.a((ZoneId) ZoneOffset.g);
                            return i2 + 1;
                        }
                    }
                    dateTimeParseContext.a(a2);
                    return str.length() + i2;
                } else if (dateTimeParseContext != null) {
                    DateTimeParseContext dateTimeParseContext2 = new DateTimeParseContext(dateTimeParseContext);
                    int a3 = k.f3140e.a(dateTimeParseContext2, charSequence, i2);
                    if (a3 < 0) {
                        return a3;
                    }
                    dateTimeParseContext.a((ZoneId) ZoneOffset.a((int) dateTimeParseContext2.a((TemporalField) ChronoField.OFFSET_SECONDS).longValue()));
                    return a3;
                } else {
                    throw null;
                }
            }
        }

        public static final class a {
            public final int a;
            public final Map<CharSequence, c.r.a> b = new HashMap();
            public final Map<String, c.r.a> c = new HashMap();

            public a(int i2) {
                this.a = i2;
            }

            public final void a(String str) {
                int length = str.length();
                int i2 = this.a;
                if (length == i2) {
                    this.b.put(str, null);
                    this.c.put(str.toLowerCase(Locale.ENGLISH), null);
                } else if (length > i2) {
                    String substring = str.substring(0, i2);
                    a aVar = this.b.get(substring);
                    if (aVar == null) {
                        aVar = new a(length);
                        this.b.put(substring, aVar);
                        this.c.put(substring.toLowerCase(Locale.ENGLISH), aVar);
                    }
                    aVar.a(str);
                }
            }

            public /* synthetic */ a(int i2, a aVar) {
                this.a = i2;
            }
        }

        public final q.b.a.o a(Set<String> set, String str, boolean z) {
            if (str == null) {
                return null;
            }
            if (!z) {
                for (String next : set) {
                    if (next.equalsIgnoreCase(str)) {
                        return ZoneId.a(next);
                    }
                }
                return null;
            } else if (set.contains(str)) {
                return ZoneId.a(str);
            } else {
                return null;
            }
        }

        /* JADX WARN: Type inference failed for: r7v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public final int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2, int i3) {
            String upperCase = charSequence.subSequence(i2, i3).toString().toUpperCase();
            if (dateTimeParseContext != null) {
                DateTimeParseContext dateTimeParseContext2 = new DateTimeParseContext(dateTimeParseContext);
                if (i3 >= charSequence.length() || !dateTimeParseContext.a(charSequence.charAt(i3), 'Z')) {
                    int a2 = k.f3140e.a(dateTimeParseContext2, charSequence, i3);
                    if (a2 < 0) {
                        dateTimeParseContext.a(ZoneId.a(upperCase, ZoneOffset.g));
                        return i3;
                    }
                    dateTimeParseContext.a(ZoneId.a(upperCase, ZoneOffset.a((int) dateTimeParseContext2.a((TemporalField) ChronoField.OFFSET_SECONDS).longValue())));
                    return a2;
                }
                dateTimeParseContext.a(ZoneId.a(upperCase, ZoneOffset.g));
                return i3;
            }
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatterBuilder a(TemporalField temporalField, int i2) {
        Collections.a((Object) temporalField, "field");
        if (i2 < 1 || i2 > 19) {
            throw new IllegalArgumentException(outline.b("The width must be from 1 to 19 inclusive but was ", i2));
        }
        a(new j(temporalField, i2, i2, SignStyle.NOT_NEGATIVE));
        return this;
    }

    public static final class e implements f {
        public final f[] b;
        public final boolean c;

        public e(List<c.f> list, boolean z) {
            this.b = (f[]) list.toArray(new f[list.size()]);
            this.c = z;
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            int length = sb.length();
            if (this.c) {
                dateTimePrintContext.d++;
            }
            try {
                for (f a : this.b) {
                    if (!a.a(dateTimePrintContext, sb)) {
                        sb.setLength(length);
                        return true;
                    }
                }
                if (this.c) {
                    dateTimePrintContext.a();
                }
                return true;
            } finally {
                if (this.c) {
                    dateTimePrintContext.a();
                }
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            if (this.b != null) {
                sb.append(this.c ? "[" : "(");
                for (f append : this.b) {
                    sb.append(append);
                }
                sb.append(this.c ? "]" : ")");
            }
            return sb.toString();
        }

        public e(f[] fVarArr, boolean z) {
            this.b = fVarArr;
            this.c = z;
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            if (this.c) {
                ArrayList<d.a> arrayList = dateTimeParseContext.g;
                DateTimeParseContext.a a = dateTimeParseContext.a();
                DateTimeParseContext.a aVar = new DateTimeParseContext.a();
                aVar.b = a.b;
                aVar.c = a.c;
                aVar.d.putAll(a.d);
                aVar.f3146e = a.f3146e;
                arrayList.add(aVar);
                int i3 = i2;
                for (f a2 : this.b) {
                    i3 = a2.a(dateTimeParseContext, charSequence, i3);
                    if (i3 < 0) {
                        dateTimeParseContext.a(false);
                        return i2;
                    }
                }
                dateTimeParseContext.a(true);
                return i3;
            }
            for (f a3 : this.b) {
                i2 = a3.a(dateTimeParseContext, charSequence, i2);
                if (i2 < 0) {
                    break;
                }
            }
            return i2;
        }
    }

    public static final class q implements f {
        public final char b;
        public final int c;

        public q(char c2, int i2) {
            this.b = c2;
            this.c = i2;
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            return a(WeekFields.a(dateTimeParseContext.a)).a(dateTimeParseContext, charSequence, i2);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(30);
            sb.append("Localized(");
            char c2 = this.b;
            if (c2 == 'Y') {
                int i2 = this.c;
                if (i2 == 1) {
                    sb.append("WeekBasedYear");
                } else if (i2 == 2) {
                    sb.append("ReducedValue(WeekBasedYear,2,2,2000-01-01)");
                } else {
                    sb.append("WeekBasedYear,");
                    sb.append(this.c);
                    sb.append(",");
                    sb.append(19);
                    sb.append(",");
                    sb.append(this.c < 4 ? SignStyle.NORMAL : SignStyle.EXCEEDS_PAD);
                }
            } else {
                if (c2 == 'c' || c2 == 'e') {
                    sb.append("DayOfWeek");
                } else if (c2 == 'w') {
                    sb.append("WeekOfWeekBasedYear");
                } else if (c2 == 'W') {
                    sb.append("WeekOfMonth");
                }
                sb.append(",");
                sb.append(this.c);
            }
            sb.append(")");
            return sb.toString();
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            return a(WeekFields.a(dateTimePrintContext.b)).a(dateTimePrintContext, sb);
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r0v1 */
        /* JADX WARN: Type inference failed for: r0v8 */
        /* JADX WARN: Type inference failed for: r4v1, types: [q.b.a.t.DateTimeFormatterBuilder$j] */
        /* JADX WARN: Type inference failed for: r7v2, types: [q.b.a.t.DateTimeFormatterBuilder$m] */
        /* JADX WARN: Type inference failed for: r0v9 */
        /* JADX WARN: Type inference failed for: r0v10 */
        /* JADX WARN: Type inference failed for: r0v11 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final q.b.a.t.DateTimeFormatterBuilder.f a(q.b.a.v.WeekFields r14) {
            /*
                r13 = this;
                char r0 = r13.b
                r1 = 87
                r2 = 2
                if (r0 == r1) goto L_0x0065
                r1 = 89
                if (r0 == r1) goto L_0x003d
                r1 = 99
                if (r0 == r1) goto L_0x0031
                r1 = 101(0x65, float:1.42E-43)
                if (r0 == r1) goto L_0x0025
                r1 = 119(0x77, float:1.67E-43)
                if (r0 == r1) goto L_0x0019
                r14 = 0
                goto L_0x0070
            L_0x0019:
                q.b.a.t.DateTimeFormatterBuilder$j r0 = new q.b.a.t.DateTimeFormatterBuilder$j
                q.b.a.v.TemporalField r14 = r14.f3154f
                int r1 = r13.c
                q.b.a.t.SignStyle r3 = q.b.a.t.SignStyle.NOT_NEGATIVE
                r0.<init>(r14, r1, r2, r3)
                goto L_0x006f
            L_0x0025:
                q.b.a.t.DateTimeFormatterBuilder$j r0 = new q.b.a.t.DateTimeFormatterBuilder$j
                q.b.a.v.TemporalField r14 = r14.d
                int r1 = r13.c
                q.b.a.t.SignStyle r3 = q.b.a.t.SignStyle.NOT_NEGATIVE
                r0.<init>(r14, r1, r2, r3)
                goto L_0x006f
            L_0x0031:
                q.b.a.t.DateTimeFormatterBuilder$j r0 = new q.b.a.t.DateTimeFormatterBuilder$j
                q.b.a.v.TemporalField r14 = r14.d
                int r1 = r13.c
                q.b.a.t.SignStyle r3 = q.b.a.t.SignStyle.NOT_NEGATIVE
                r0.<init>(r14, r1, r2, r3)
                goto L_0x006f
            L_0x003d:
                int r6 = r13.c
                if (r6 != r2) goto L_0x004f
                q.b.a.t.DateTimeFormatterBuilder$m r0 = new q.b.a.t.DateTimeFormatterBuilder$m
                q.b.a.v.TemporalField r8 = r14.g
                r9 = 2
                r10 = 2
                r11 = 0
                q.b.a.LocalDate r12 = q.b.a.t.DateTimeFormatterBuilder.m.f3141j
                r7 = r0
                r7.<init>(r8, r9, r10, r11, r12)
                goto L_0x006f
            L_0x004f:
                q.b.a.t.DateTimeFormatterBuilder$j r0 = new q.b.a.t.DateTimeFormatterBuilder$j
                q.b.a.v.TemporalField r5 = r14.g
                r7 = 19
                r14 = 4
                if (r6 >= r14) goto L_0x005b
                q.b.a.t.SignStyle r14 = q.b.a.t.SignStyle.NORMAL
                goto L_0x005d
            L_0x005b:
                q.b.a.t.SignStyle r14 = q.b.a.t.SignStyle.EXCEEDS_PAD
            L_0x005d:
                r8 = r14
                r9 = -1
                r10 = 0
                r4 = r0
                r4.<init>(r5, r6, r7, r8, r9, r10)
                goto L_0x006f
            L_0x0065:
                q.b.a.t.DateTimeFormatterBuilder$j r0 = new q.b.a.t.DateTimeFormatterBuilder$j
                q.b.a.v.TemporalField r14 = r14.f3153e
                r1 = 1
                q.b.a.t.SignStyle r3 = q.b.a.t.SignStyle.NOT_NEGATIVE
                r0.<init>(r14, r1, r2, r3)
            L_0x006f:
                r14 = r0
            L_0x0070:
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.q.a(q.b.a.v.WeekFields):q.b.a.t.DateTimeFormatterBuilder$f");
        }
    }

    public static final class p implements f {
        public final TemporalField b;
        public final TextStyle c;
        public final DateTimeTextProvider d;

        /* renamed from: e  reason: collision with root package name */
        public volatile j f3143e;

        public p(TemporalField temporalField, TextStyle textStyle, DateTimeTextProvider dateTimeTextProvider) {
            this.b = temporalField;
            this.c = textStyle;
            this.d = dateTimeTextProvider;
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            Long a = dateTimePrintContext.a(this.b);
            if (a == null) {
                return false;
            }
            String a2 = this.d.a(this.b, a.longValue(), this.c, dateTimePrintContext.b);
            if (a2 == null) {
                return a().a(dateTimePrintContext, sb);
            }
            sb.append(a2);
            return true;
        }

        public String toString() {
            if (this.c == TextStyle.FULL) {
                StringBuilder a = outline.a("Text(");
                a.append(this.b);
                a.append(")");
                return a.toString();
            }
            StringBuilder a2 = outline.a("Text(");
            a2.append(this.b);
            a2.append(",");
            a2.append(this.c);
            a2.append(")");
            return a2.toString();
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            int length = charSequence.length();
            if (i2 < 0 || i2 > length) {
                throw new IndexOutOfBoundsException();
            }
            Iterator<Map.Entry<String, Long>> a = this.d.a(this.b, dateTimeParseContext.f3145f ? this.c : null, dateTimeParseContext.a);
            if (a != null) {
                while (a.hasNext()) {
                    Map.Entry next = a.next();
                    String str = (String) next.getKey();
                    if (dateTimeParseContext.a(str, 0, charSequence, i2, str.length())) {
                        return dateTimeParseContext.a(this.b, ((Long) next.getValue()).longValue(), i2, str.length() + i2);
                    }
                }
                if (dateTimeParseContext.f3145f) {
                    return ~i2;
                }
            }
            return a().a(dateTimeParseContext, charSequence, i2);
        }

        public final j a() {
            if (this.f3143e == null) {
                this.f3143e = new j(this.b, 1, 19, SignStyle.NORMAL);
            }
            return this.f3143e;
        }
    }

    public DateTimeFormatterBuilder(DateTimeFormatterBuilder dateTimeFormatterBuilder, boolean z) {
        this.a = this;
        this.c = new ArrayList();
        this.g = -1;
        this.b = dateTimeFormatterBuilder;
        this.d = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.SignStyle, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatterBuilder a(TemporalField temporalField, int i2, int i3, SignStyle signStyle) {
        if (i2 == i3 && signStyle == SignStyle.NOT_NEGATIVE) {
            a(temporalField, i3);
            return this;
        }
        Collections.a((Object) temporalField, "field");
        Collections.a((Object) signStyle, "signStyle");
        if (i2 < 1 || i2 > 19) {
            throw new IllegalArgumentException(outline.b("The minimum width must be from 1 to 19 inclusive but was ", i2));
        } else if (i3 < 1 || i3 > 19) {
            throw new IllegalArgumentException(outline.b("The maximum width must be from 1 to 19 inclusive but was ", i3));
        } else if (i3 >= i2) {
            a(new j(temporalField, i2, i3, signStyle));
            return this;
        } else {
            throw new IllegalArgumentException(outline.a("The maximum width must exceed or equal the minimum width but ", i3, " < ", i2));
        }
    }

    public static final class l implements f {
        public final f b;
        public final int c;
        public final char d;

        public l(f fVar, int i2, char c2) {
            this.b = fVar;
            this.c = i2;
            this.d = c2;
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            boolean z = dateTimeParseContext.f3145f;
            boolean z2 = dateTimeParseContext.f3144e;
            if (i2 > charSequence.length()) {
                throw new IndexOutOfBoundsException();
            } else if (i2 == charSequence.length()) {
                return ~i2;
            } else {
                int i3 = this.c + i2;
                if (i3 > charSequence.length()) {
                    if (z) {
                        return ~i2;
                    }
                    i3 = charSequence.length();
                }
                int i4 = i2;
                while (i4 < i3) {
                    if (!z2) {
                        if (!dateTimeParseContext.a(charSequence.charAt(i4), this.d)) {
                            break;
                        }
                    } else if (charSequence.charAt(i4) != this.d) {
                        break;
                    }
                    i4++;
                }
                int a = this.b.a(dateTimeParseContext, charSequence.subSequence(0, i3), i4);
                return (a == i3 || !z) ? a : ~(i2 + i4);
            }
        }

        public String toString() {
            String str;
            StringBuilder a = outline.a("Pad(");
            a.append(this.b);
            a.append(",");
            a.append(this.c);
            if (this.d == ' ') {
                str = ")";
            } else {
                StringBuilder a2 = outline.a(",'");
                a2.append(this.d);
                a2.append("')");
                str = a2.toString();
            }
            a.append(str);
            return a.toString();
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            int length = sb.length();
            if (!this.b.a(dateTimePrintContext, sb)) {
                return false;
            }
            int length2 = sb.length() - length;
            if (length2 <= this.c) {
                for (int i2 = 0; i2 < this.c - length2; i2++) {
                    sb.insert(length, this.d);
                }
                return true;
            }
            StringBuilder a = outline.a("Cannot print as output of ", length2, " characters exceeds pad width of ");
            a.append(this.c);
            throw new DateTimeException(a.toString());
        }
    }

    public static final class i implements f {
        public final TextStyle b;

        public i(TextStyle textStyle) {
            this.b = textStyle;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            Long a = dateTimePrintContext.a((TemporalField) ChronoField.OFFSET_SECONDS);
            if (a == null) {
                return false;
            }
            sb.append("GMT");
            if (this.b == TextStyle.FULL) {
                return new k("", "+HH:MM:ss").a(dateTimePrintContext, sb);
            }
            int a2 = Collections.a(a.longValue());
            if (a2 == 0) {
                return true;
            }
            int abs = Math.abs((a2 / 3600) % 100);
            int abs2 = Math.abs((a2 / 60) % 60);
            int abs3 = Math.abs(a2 % 60);
            sb.append(a2 < 0 ? "-" : "+");
            sb.append(abs);
            if (abs2 <= 0 && abs3 <= 0) {
                return true;
            }
            sb.append(":");
            sb.append((char) ((abs2 / 10) + 48));
            sb.append((char) ((abs2 % 10) + 48));
            if (abs3 <= 0) {
                return true;
            }
            sb.append(":");
            sb.append((char) ((abs3 / 10) + 48));
            sb.append((char) ((abs3 % 10) + 48));
            return true;
        }

        /* JADX WARN: Type inference failed for: r9v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r9v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r9v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.v.TemporalField] */
        /* JADX WARN: Type inference failed for: r2v4, types: [q.b.a.v.TemporalField] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int a(q.b.a.t.DateTimeParseContext r15, java.lang.CharSequence r16, int r17) {
            /*
                r14 = this;
                r6 = r16
                r7 = r17
                java.lang.String r3 = "GMT"
                r4 = 0
                r5 = 3
                r0 = r15
                r1 = r16
                r2 = r17
                boolean r0 = r0.a(r1, r2, r3, r4, r5)
                if (r0 != 0) goto L_0x0015
                int r0 = ~r7
                return r0
            L_0x0015:
                int r0 = r7 + 3
                r7 = r14
                q.b.a.t.TextStyle r1 = r7.b
                q.b.a.t.TextStyle r2 = q.b.a.t.TextStyle.FULL
                if (r1 != r2) goto L_0x002d
                q.b.a.t.DateTimeFormatterBuilder$k r1 = new q.b.a.t.DateTimeFormatterBuilder$k
                java.lang.String r2 = ""
                java.lang.String r3 = "+HH:MM:ss"
                r1.<init>(r2, r3)
                r2 = r15
                int r0 = r1.a(r15, r6, r0)
                return r0
            L_0x002d:
                r2 = r15
                int r1 = r16.length()
                if (r0 != r1) goto L_0x0042
                q.b.a.v.ChronoField r3 = q.b.a.v.ChronoField.OFFSET_SECONDS
                r4 = 0
                r1 = r15
                r2 = r3
                r3 = r4
                r5 = r0
                r6 = r0
                int r0 = r1.a(r2, r3, r5, r6)
                return r0
            L_0x0042:
                char r3 = r6.charAt(r0)
                r4 = 43
                r5 = 45
                if (r3 == r4) goto L_0x005c
                if (r3 == r5) goto L_0x005c
                q.b.a.v.ChronoField r3 = q.b.a.v.ChronoField.OFFSET_SECONDS
                r4 = 0
                r1 = r15
                r2 = r3
                r3 = r4
                r5 = r0
                r6 = r0
                int r0 = r1.a(r2, r3, r5, r6)
                return r0
            L_0x005c:
                r4 = 1
                if (r3 != r5) goto L_0x0061
                r3 = -1
                goto L_0x0062
            L_0x0061:
                r3 = 1
            L_0x0062:
                if (r0 != r1) goto L_0x0066
                int r0 = ~r0
                return r0
            L_0x0066:
                int r0 = r0 + r4
                char r5 = r6.charAt(r0)
                r8 = 48
                if (r5 < r8) goto L_0x0125
                r9 = 57
                if (r5 <= r9) goto L_0x0075
                goto L_0x0125
            L_0x0075:
                int r0 = r0 + r4
                int r5 = r5 - r8
                if (r0 == r1) goto L_0x008d
                char r10 = r6.charAt(r0)
                if (r10 < r8) goto L_0x008d
                if (r10 > r9) goto L_0x008d
                int r5 = r5 * 10
                int r10 = r10 - r8
                int r5 = r5 + r10
                r10 = 23
                if (r5 <= r10) goto L_0x008b
                int r0 = ~r0
                return r0
            L_0x008b:
                int r0 = r0 + 1
            L_0x008d:
                r13 = r0
                if (r13 == r1) goto L_0x0117
                char r0 = r6.charAt(r13)
                r10 = 58
                if (r0 == r10) goto L_0x009a
                goto L_0x0117
            L_0x009a:
                int r13 = r13 + r4
                int r0 = r1 + -2
                if (r13 <= r0) goto L_0x00a1
                int r0 = ~r13
                return r0
            L_0x00a1:
                char r11 = r6.charAt(r13)
                if (r11 < r8) goto L_0x0115
                if (r11 <= r9) goto L_0x00ab
                goto L_0x0115
            L_0x00ab:
                int r13 = r13 + r4
                int r11 = r11 - r8
                char r12 = r6.charAt(r13)
                if (r12 < r8) goto L_0x0113
                if (r12 <= r9) goto L_0x00b6
                goto L_0x0113
            L_0x00b6:
                int r13 = r13 + r4
                int r11 = r11 * 10
                int r12 = r12 - r8
                int r12 = r12 + r11
                r11 = 59
                if (r12 <= r11) goto L_0x00c1
                int r0 = ~r13
                return r0
            L_0x00c1:
                if (r13 == r1) goto L_0x0102
                char r1 = r6.charAt(r13)
                if (r1 == r10) goto L_0x00ca
                goto L_0x0102
            L_0x00ca:
                int r13 = r13 + r4
                if (r13 <= r0) goto L_0x00cf
                int r0 = ~r13
                return r0
            L_0x00cf:
                char r0 = r6.charAt(r13)
                if (r0 < r8) goto L_0x0100
                if (r0 <= r9) goto L_0x00d8
                goto L_0x0100
            L_0x00d8:
                int r13 = r13 + r4
                int r0 = r0 - r8
                char r1 = r6.charAt(r13)
                if (r1 < r8) goto L_0x00fe
                if (r1 <= r9) goto L_0x00e3
                goto L_0x00fe
            L_0x00e3:
                int r13 = r13 + r4
                int r0 = r0 * 10
                int r1 = r1 - r8
                int r1 = r1 + r0
                if (r1 <= r11) goto L_0x00ec
                int r0 = ~r13
                return r0
            L_0x00ec:
                int r5 = r5 * 3600
                int r12 = r12 * 60
                int r12 = r12 + r5
                int r12 = r12 + r1
                int r12 = r12 * r3
                q.b.a.v.ChronoField r9 = q.b.a.v.ChronoField.OFFSET_SECONDS
                long r10 = (long) r12
                r8 = r15
                r12 = r13
                int r0 = r8.a(r9, r10, r12, r13)
                return r0
            L_0x00fe:
                int r0 = ~r13
                return r0
            L_0x0100:
                int r0 = ~r13
                return r0
            L_0x0102:
                int r5 = r5 * 3600
                int r12 = r12 * 60
                int r12 = r12 + r5
                int r12 = r12 * r3
                q.b.a.v.ChronoField r9 = q.b.a.v.ChronoField.OFFSET_SECONDS
                long r10 = (long) r12
                r8 = r15
                r12 = r13
                int r0 = r8.a(r9, r10, r12, r13)
                return r0
            L_0x0113:
                int r0 = ~r13
                return r0
            L_0x0115:
                int r0 = ~r13
                return r0
            L_0x0117:
                int r3 = r3 * 3600
                int r3 = r3 * r5
                q.b.a.v.ChronoField r9 = q.b.a.v.ChronoField.OFFSET_SECONDS
                long r10 = (long) r3
                r8 = r15
                r12 = r13
                int r0 = r8.a(r9, r10, r12, r13)
                return r0
            L_0x0125:
                int r0 = ~r0
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.i.a(q.b.a.t.DateTimeParseContext, java.lang.CharSequence, int):int");
        }
    }

    public final DateTimeFormatterBuilder a(j jVar) {
        j jVar2;
        DateTimeFormatterBuilder dateTimeFormatterBuilder = this.a;
        int i2 = dateTimeFormatterBuilder.g;
        if (i2 < 0 || !(dateTimeFormatterBuilder.c.get(i2) instanceof j)) {
            this.a.g = a((f) jVar);
        } else {
            DateTimeFormatterBuilder dateTimeFormatterBuilder2 = this.a;
            int i3 = dateTimeFormatterBuilder2.g;
            j jVar3 = dateTimeFormatterBuilder2.c.get(i3);
            int i4 = jVar.c;
            int i5 = jVar.d;
            if (i4 == i5 && jVar.f3138e == SignStyle.NOT_NEGATIVE) {
                jVar2 = jVar3.a(i5);
                a((f) jVar.a());
                this.a.g = i3;
            } else {
                jVar2 = jVar3.a();
                this.a.g = a((f) jVar);
            }
            this.a.c.set(i3, jVar2);
        }
        return this;
    }

    public static final class k implements f {
        public static final String[] d = {"+HH", "+HHmm", "+HH:mm", "+HHMM", "+HH:MM", "+HHMMss", "+HH:MM:ss", "+HHMMSS", "+HH:MM:SS"};

        /* renamed from: e  reason: collision with root package name */
        public static final k f3140e = new k("Z", "+HH:MM:ss");
        public final String b;
        public final int c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Object, java.lang.String):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Object, java.lang.String):T */
        public k(String str, String str2) {
            Collections.a((Object) str, "noOffsetText");
            Collections.a((Object) str2, "pattern");
            this.b = str;
            int i2 = 0;
            while (true) {
                String[] strArr = d;
                if (i2 >= strArr.length) {
                    throw new IllegalArgumentException(outline.a("Invalid zone offset pattern: ", str2));
                } else if (strArr[i2].equals(str2)) {
                    this.c = i2;
                    return;
                } else {
                    i2++;
                }
            }
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            Long a = dateTimePrintContext.a((TemporalField) ChronoField.OFFSET_SECONDS);
            if (a == null) {
                return false;
            }
            int a2 = Collections.a(a.longValue());
            if (a2 == 0) {
                sb.append(this.b);
            } else {
                int abs = Math.abs((a2 / 3600) % 100);
                int abs2 = Math.abs((a2 / 60) % 60);
                int abs3 = Math.abs(a2 % 60);
                int length = sb.length();
                sb.append(a2 < 0 ? "-" : "+");
                sb.append((char) ((abs / 10) + 48));
                sb.append((char) ((abs % 10) + 48));
                int i2 = this.c;
                if (i2 >= 3 || (i2 >= 1 && abs2 > 0)) {
                    String str = ":";
                    sb.append(this.c % 2 == 0 ? str : "");
                    sb.append((char) ((abs2 / 10) + 48));
                    sb.append((char) ((abs2 % 10) + 48));
                    abs += abs2;
                    int i3 = this.c;
                    if (i3 >= 7 || (i3 >= 5 && abs3 > 0)) {
                        if (this.c % 2 != 0) {
                            str = "";
                        }
                        sb.append(str);
                        sb.append((char) ((abs3 / 10) + 48));
                        sb.append((char) ((abs3 % 10) + 48));
                        abs += abs3;
                    }
                }
                if (abs == 0) {
                    sb.setLength(length);
                    sb.append(this.b);
                }
            }
            return true;
        }

        public String toString() {
            String replace = this.b.replace("'", "''");
            StringBuilder a = outline.a("Offset(");
            a.append(d[this.c]);
            a.append(",'");
            a.append(replace);
            a.append("')");
            return a.toString();
        }

        /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v7, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0080  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int a(q.b.a.t.DateTimeParseContext r16, java.lang.CharSequence r17, int r18) {
            /*
                r15 = this;
                r0 = r15
                r7 = r17
                r8 = r18
                int r1 = r17.length()
                java.lang.String r2 = r0.b
                int r9 = r2.length()
                if (r9 != 0) goto L_0x0022
                if (r8 != r1) goto L_0x0045
                q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.OFFSET_SECONDS
                r3 = 0
                r1 = r16
                r5 = r18
                r6 = r18
                int r1 = r1.a(r2, r3, r5, r6)
                return r1
            L_0x0022:
                if (r8 != r1) goto L_0x0026
                int r1 = ~r8
                return r1
            L_0x0026:
                java.lang.String r4 = r0.b
                r5 = 0
                r1 = r16
                r2 = r17
                r3 = r18
                r6 = r9
                boolean r1 = r1.a(r2, r3, r4, r5, r6)
                if (r1 == 0) goto L_0x0045
                q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.OFFSET_SECONDS
                r3 = 0
                int r6 = r8 + r9
                r1 = r16
                r5 = r18
                int r1 = r1.a(r2, r3, r5, r6)
                return r1
            L_0x0045:
                char r1 = r17.charAt(r18)
                r2 = 43
                r3 = 45
                if (r1 == r2) goto L_0x0051
                if (r1 != r3) goto L_0x00a5
            L_0x0051:
                r2 = 1
                if (r1 != r3) goto L_0x0056
                r1 = -1
                goto L_0x0057
            L_0x0056:
                r1 = 1
            L_0x0057:
                r3 = 4
                int[] r3 = new int[r3]
                int r4 = r8 + 1
                r5 = 0
                r3[r5] = r4
                boolean r4 = r15.a(r3, r2, r7, r2)
                r6 = 2
                r10 = 3
                if (r4 != 0) goto L_0x007d
                int r4 = r0.c
                if (r4 < r10) goto L_0x006d
                r4 = 1
                goto L_0x006e
            L_0x006d:
                r4 = 0
            L_0x006e:
                boolean r4 = r15.a(r3, r6, r7, r4)
                if (r4 != 0) goto L_0x007d
                boolean r4 = r15.a(r3, r10, r7, r5)
                if (r4 == 0) goto L_0x007b
                goto L_0x007d
            L_0x007b:
                r4 = 0
                goto L_0x007e
            L_0x007d:
                r4 = 1
            L_0x007e:
                if (r4 != 0) goto L_0x00a5
                long r11 = (long) r1
                r1 = r3[r2]
                long r1 = (long) r1
                r13 = 3600(0xe10, double:1.7786E-320)
                long r1 = r1 * r13
                r4 = r3[r6]
                long r6 = (long) r4
                r13 = 60
                long r6 = r6 * r13
                long r6 = r6 + r1
                r1 = r3[r10]
                long r1 = (long) r1
                long r6 = r6 + r1
                long r6 = r6 * r11
                q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.OFFSET_SECONDS
                r9 = r3[r5]
                r1 = r16
                r3 = r6
                r5 = r18
                r6 = r9
                int r1 = r1.a(r2, r3, r5, r6)
                return r1
            L_0x00a5:
                if (r9 != 0) goto L_0x00b6
                q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.OFFSET_SECONDS
                r3 = 0
                int r6 = r8 + r9
                r1 = r16
                r5 = r18
                int r1 = r1.a(r2, r3, r5, r6)
                return r1
            L_0x00b6:
                int r1 = ~r8
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.k.a(q.b.a.t.DateTimeParseContext, java.lang.CharSequence, int):int");
        }

        public final boolean a(int[] iArr, int i2, CharSequence charSequence, boolean z) {
            int i3;
            int i4 = this.c;
            if ((i4 + 3) / 2 < i2) {
                return false;
            }
            int i5 = iArr[0];
            if (i4 % 2 == 0 && i2 > 1) {
                int i6 = i5 + 1;
                if (i6 > charSequence.length() || charSequence.charAt(i5) != ':') {
                    return z;
                }
                i5 = i6;
            }
            if (i5 + 2 > charSequence.length()) {
                return z;
            }
            int i7 = i5 + 1;
            char charAt = charSequence.charAt(i5);
            int i8 = i7 + 1;
            char charAt2 = charSequence.charAt(i7);
            if (charAt < '0' || charAt > '9' || charAt2 < '0' || charAt2 > '9' || (i3 = (charAt2 - '0') + ((charAt - '0') * 10)) < 0 || i3 > 59) {
                return z;
            }
            iArr[i2] = i3;
            iArr[0] = i8;
            return false;
        }
    }

    public static final class s implements f {
        public static final Comparator<String> c = new a();
        public final TextStyle b;

        public class a implements Comparator<String> {
            public int compare(Object obj, Object obj2) {
                String str = (String) obj;
                String str2 = (String) obj2;
                int length = str2.length() - str.length();
                return length == 0 ? str.compareTo(str2) : length;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Object, java.lang.String):T
         arg types: [q.b.a.t.TextStyle, java.lang.String]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Object, java.lang.String):T */
        public s(TextStyle textStyle) {
            Collections.a((Object) textStyle, "textStyle");
            this.b = textStyle;
        }

        /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r4v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x002b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(q.b.a.t.DateTimePrintContext r7, java.lang.StringBuilder r8) {
            /*
                r6 = this;
                q.b.a.v.TemporalQuery<q.b.a.o> r0 = q.b.a.v.TemporalQueries.a
                java.lang.Object r0 = r7.a(r0)
                q.b.a.ZoneId r0 = (q.b.a.ZoneId) r0
                r1 = 0
                if (r0 != 0) goto L_0x000c
                return r1
            L_0x000c:
                q.b.a.w.ZoneRules r2 = r0.g()     // Catch:{ ZoneRulesException -> 0x001d }
                boolean r3 = r2.a()     // Catch:{ ZoneRulesException -> 0x001d }
                if (r3 == 0) goto L_0x001d
                q.b.a.Instant r3 = q.b.a.Instant.d     // Catch:{ ZoneRulesException -> 0x001d }
                q.b.a.ZoneOffset r2 = r2.a(r3)     // Catch:{ ZoneRulesException -> 0x001d }
                goto L_0x001e
            L_0x001d:
                r2 = r0
            L_0x001e:
                boolean r2 = r2 instanceof q.b.a.ZoneOffset
                r3 = 1
                if (r2 == 0) goto L_0x002b
                java.lang.String r7 = r0.f()
                r8.append(r7)
                return r3
            L_0x002b:
                q.b.a.v.TemporalAccessor r2 = r7.a
                q.b.a.v.ChronoField r4 = q.b.a.v.ChronoField.INSTANT_SECONDS
                boolean r4 = r2.c(r4)
                if (r4 == 0) goto L_0x0048
                q.b.a.v.ChronoField r4 = q.b.a.v.ChronoField.INSTANT_SECONDS
                long r4 = r2.d(r4)
                q.b.a.Instant r2 = q.b.a.Instant.a(r4, r1)
                q.b.a.w.ZoneRules r4 = r0.g()
                boolean r2 = r4.b(r2)
                goto L_0x0049
            L_0x0048:
                r2 = 0
            L_0x0049:
                java.lang.String r0 = r0.f()
                java.util.TimeZone r0 = java.util.TimeZone.getTimeZone(r0)
                q.b.a.t.TextStyle r4 = r6.b
                if (r4 == 0) goto L_0x0070
                q.b.a.t.TextStyle[] r5 = q.b.a.t.TextStyle.values()
                int r4 = r4.ordinal()
                r4 = r4 & -2
                r4 = r5[r4]
                q.b.a.t.TextStyle r5 = q.b.a.t.TextStyle.FULL
                if (r4 != r5) goto L_0x0066
                r1 = 1
            L_0x0066:
                java.util.Locale r7 = r7.b
                java.lang.String r7 = r0.getDisplayName(r2, r1, r7)
                r8.append(r7)
                return r3
            L_0x0070:
                r7 = 0
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeFormatterBuilder.s.a(q.b.a.t.DateTimePrintContext, java.lang.StringBuilder):boolean");
        }

        public String toString() {
            StringBuilder a2 = outline.a("ZoneText(");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            TreeMap treeMap = new TreeMap(c);
            Iterator it = ((HashSet) ZoneId.h()).iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                treeMap.put(str, str);
                TimeZone timeZone = TimeZone.getTimeZone(str);
                TextStyle textStyle = this.b;
                if (textStyle != null) {
                    int i3 = TextStyle.values()[textStyle.ordinal() & -2] == TextStyle.FULL ? 1 : 0;
                    String displayName = timeZone.getDisplayName(false, i3, dateTimeParseContext.a);
                    if (str.startsWith("Etc/") || (!displayName.startsWith("GMT+") && !displayName.startsWith("GMT+"))) {
                        treeMap.put(displayName, str);
                    }
                    String displayName2 = timeZone.getDisplayName(true, i3, dateTimeParseContext.a);
                    if (str.startsWith("Etc/") || (!displayName2.startsWith("GMT+") && !displayName2.startsWith("GMT+"))) {
                        treeMap.put(displayName2, str);
                    }
                } else {
                    throw null;
                }
            }
            for (Map.Entry entry : treeMap.entrySet()) {
                String str2 = (String) entry.getKey();
                if (dateTimeParseContext.a(charSequence, i2, str2, 0, str2.length())) {
                    dateTimeParseContext.a(ZoneId.a((String) entry.getValue()));
                    return str2.length() + i2;
                }
            }
            return ~i2;
        }
    }

    public static final class g implements f {
        public final TemporalField b;
        public final int c;
        public final int d;

        /* renamed from: e  reason: collision with root package name */
        public final boolean f3137e;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Object, java.lang.String):T
         arg types: [q.b.a.v.TemporalField, java.lang.String]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Object, java.lang.String):T */
        public g(TemporalField temporalField, int i2, int i3, boolean z) {
            Collections.a((Object) temporalField, "field");
            ValueRange g = temporalField.g();
            if (!(g.b == g.c && g.d == g.f3152e)) {
                throw new IllegalArgumentException(outline.a("Field must have a fixed set of values: ", temporalField));
            } else if (i2 < 0 || i2 > 9) {
                throw new IllegalArgumentException(outline.b("Minimum width must be from 0 to 9 inclusive but was ", i2));
            } else if (i3 < 1 || i3 > 9) {
                throw new IllegalArgumentException(outline.b("Maximum width must be from 1 to 9 inclusive but was ", i3));
            } else if (i3 >= i2) {
                this.b = temporalField;
                this.c = i2;
                this.d = i3;
                this.f3137e = z;
            } else {
                throw new IllegalArgumentException(outline.a("Maximum width must exceed or equal the minimum width but ", i3, " < ", i2));
            }
        }

        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            int i3;
            int i4 = dateTimeParseContext.f3145f ? this.c : 0;
            int i5 = dateTimeParseContext.f3145f ? this.d : 9;
            int length = charSequence.length();
            if (i2 == length) {
                return i4 > 0 ? ~i2 : i2;
            }
            if (this.f3137e) {
                if (charSequence.charAt(i2) != dateTimeParseContext.b.d) {
                    return i4 > 0 ? ~i2 : i2;
                }
                i2++;
            }
            int i6 = i2;
            int i7 = i4 + i6;
            if (i7 > length) {
                return ~i6;
            }
            int min = Math.min(i5 + i6, length);
            int i8 = i6;
            int i9 = 0;
            while (true) {
                if (i8 >= min) {
                    i3 = i8;
                    break;
                }
                int i10 = i8 + 1;
                int charAt = charSequence.charAt(i8) - dateTimeParseContext.b.a;
                if (charAt < 0 || charAt > 9) {
                    charAt = -1;
                }
                if (charAt >= 0) {
                    i9 = (i9 * 10) + charAt;
                    i8 = i10;
                } else if (i10 < i7) {
                    return ~i6;
                } else {
                    i3 = i10 - 1;
                }
            }
            BigDecimal movePointLeft = new BigDecimal(i9).movePointLeft(i3 - i6);
            ValueRange g = this.b.g();
            BigDecimal valueOf = BigDecimal.valueOf(g.b);
            return dateTimeParseContext.a(this.b, movePointLeft.multiply(BigDecimal.valueOf(g.f3152e).subtract(valueOf).add(BigDecimal.ONE)).setScale(0, RoundingMode.FLOOR).add(valueOf).longValueExact(), i6, i3);
        }

        public String toString() {
            String str = this.f3137e ? ",DecimalPoint" : "";
            StringBuilder a = outline.a("Fraction(");
            a.append(this.b);
            a.append(",");
            a.append(this.c);
            a.append(",");
            a.append(this.d);
            a.append(str);
            a.append(")");
            return a.toString();
        }

        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            Long a = dateTimePrintContext.a(this.b);
            if (a == null) {
                return false;
            }
            DecimalStyle decimalStyle = dateTimePrintContext.c;
            long longValue = a.longValue();
            ValueRange g = this.b.g();
            g.b(longValue, this.b);
            BigDecimal valueOf = BigDecimal.valueOf(g.b);
            BigDecimal divide = BigDecimal.valueOf(longValue).subtract(valueOf).divide(BigDecimal.valueOf(g.f3152e).subtract(valueOf).add(BigDecimal.ONE), 9, RoundingMode.FLOOR);
            BigDecimal stripTrailingZeros = divide.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : divide.stripTrailingZeros();
            if (stripTrailingZeros.scale() != 0) {
                String a2 = decimalStyle.a(stripTrailingZeros.setScale(Math.min(Math.max(stripTrailingZeros.scale(), this.c), this.d), RoundingMode.FLOOR).toPlainString().substring(2));
                if (this.f3137e) {
                    sb.append(decimalStyle.d);
                }
                sb.append(a2);
                return true;
            } else if (this.c <= 0) {
                return true;
            } else {
                if (this.f3137e) {
                    sb.append(decimalStyle.d);
                }
                for (int i2 = 0; i2 < this.c; i2++) {
                    sb.append(decimalStyle.a);
                }
                return true;
            }
        }
    }

    public DateTimeFormatterBuilder a(TemporalField temporalField, int i2, int i3, boolean z) {
        a(new g(temporalField, i2, i3, z));
        return this;
    }

    public static final class h implements f {
        public final int b;

        public h(int i2) {
            this.b = i2;
        }

        /* JADX WARN: Type inference failed for: r6v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r10v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r6v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r6v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v10, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v12, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r10v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r10v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r13v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v19, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r2v20, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, boolean):q.b.a.t.DateTimeFormatterBuilder
         arg types: [?, int, int, int]
         candidates:
          q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, q.b.a.t.SignStyle):q.b.a.t.DateTimeFormatterBuilder
          q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, boolean):q.b.a.t.DateTimeFormatterBuilder */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: q.b.a.t.DateTimeFormatterBuilder.e.<init>(q.b.a.t.DateTimeFormatterBuilder$f[], boolean):void
         arg types: [q.b.a.t.DateTimeFormatterBuilder$f[], int]
         candidates:
          q.b.a.t.DateTimeFormatterBuilder.e.<init>(java.util.List<q.b.a.t.c$f>, boolean):void
          q.b.a.t.DateTimeFormatterBuilder.e.<init>(q.b.a.t.DateTimeFormatterBuilder$f[], boolean):void */
        public int a(DateTimeParseContext dateTimeParseContext, CharSequence charSequence, int i2) {
            int i3;
            int i4;
            DateTimeParseContext dateTimeParseContext2 = dateTimeParseContext;
            int i5 = i2;
            if (dateTimeParseContext2 != null) {
                DateTimeParseContext dateTimeParseContext3 = new DateTimeParseContext(dateTimeParseContext2);
                int i6 = this.b;
                if (i6 < 0) {
                    i6 = 0;
                }
                int i7 = this.b;
                if (i7 < 0) {
                    i7 = 9;
                }
                DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
                dateTimeFormatterBuilder.a(DateTimeFormatter.h);
                dateTimeFormatterBuilder.a('T');
                dateTimeFormatterBuilder.a((TemporalField) ChronoField.HOUR_OF_DAY, 2);
                dateTimeFormatterBuilder.a(':');
                dateTimeFormatterBuilder.a((TemporalField) ChronoField.MINUTE_OF_HOUR, 2);
                dateTimeFormatterBuilder.a(':');
                dateTimeFormatterBuilder.a((TemporalField) ChronoField.SECOND_OF_MINUTE, 2);
                dateTimeFormatterBuilder.a((TemporalField) ChronoField.NANO_OF_SECOND, i6, i7, true);
                dateTimeFormatterBuilder.a('Z');
                e eVar = dateTimeFormatterBuilder.c().a;
                if (eVar.c) {
                    eVar = new e(eVar.b, false);
                }
                int a = eVar.a(dateTimeParseContext3, charSequence, i5);
                if (a < 0) {
                    return a;
                }
                long longValue = dateTimeParseContext3.a((TemporalField) ChronoField.YEAR).longValue();
                int intValue = dateTimeParseContext3.a((TemporalField) ChronoField.MONTH_OF_YEAR).intValue();
                int intValue2 = dateTimeParseContext3.a((TemporalField) ChronoField.DAY_OF_MONTH).intValue();
                int intValue3 = dateTimeParseContext3.a((TemporalField) ChronoField.HOUR_OF_DAY).intValue();
                int intValue4 = dateTimeParseContext3.a((TemporalField) ChronoField.MINUTE_OF_HOUR).intValue();
                Long a2 = dateTimeParseContext3.a((TemporalField) ChronoField.SECOND_OF_MINUTE);
                Long a3 = dateTimeParseContext3.a((TemporalField) ChronoField.NANO_OF_SECOND);
                int intValue5 = a2 != null ? a2.intValue() : 0;
                int intValue6 = a3 != null ? a3.intValue() : 0;
                int i8 = ((int) longValue) % AbstractSpiCall.DEFAULT_TIMEOUT;
                if (intValue3 == 24 && intValue4 == 0 && intValue5 == 0 && intValue6 == 0) {
                    i3 = intValue5;
                    intValue3 = 0;
                    i4 = 1;
                } else if (intValue3 == 23 && intValue4 == 59 && intValue5 == 60) {
                    dateTimeParseContext.a().f3146e = true;
                    i4 = 0;
                    i3 = 59;
                } else {
                    i3 = intValue5;
                    i4 = 0;
                }
                try {
                    int i9 = intValue6;
                    DateTimeParseContext dateTimeParseContext4 = dateTimeParseContext;
                    int i10 = i2;
                    return dateTimeParseContext4.a(ChronoField.NANO_OF_SECOND, (long) i9, i10, dateTimeParseContext4.a(ChronoField.INSTANT_SECONDS, LocalDateTime.a(i8, intValue, intValue2, intValue3, intValue4, i3, 0).a((long) i4).a(ZoneOffset.g) + Collections.e(longValue / 10000, 315569520000L), i10, a));
                } catch (RuntimeException unused) {
                    return ~i5;
                }
            } else {
                throw null;
            }
        }

        public String toString() {
            return "Instant()";
        }

        /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r8v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX WARN: Type inference failed for: r6v12, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.b(long, long):long
         arg types: [long, int]
         candidates:
          n.i.Collections.b(int, int):int
          n.i.Collections.b(long, int):long
          n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.b(long, long):long */
        public boolean a(DateTimePrintContext dateTimePrintContext, StringBuilder sb) {
            DateTimePrintContext dateTimePrintContext2 = dateTimePrintContext;
            StringBuilder sb2 = sb;
            Long a = dateTimePrintContext2.a((TemporalField) ChronoField.INSTANT_SECONDS);
            Long l2 = 0L;
            if (dateTimePrintContext2.a.c(ChronoField.NANO_OF_SECOND)) {
                l2 = Long.valueOf(dateTimePrintContext2.a.d(ChronoField.NANO_OF_SECOND));
            }
            int i2 = 0;
            if (a == null) {
                return false;
            }
            long longValue = a.longValue();
            int a2 = ChronoField.NANO_OF_SECOND.a(l2.longValue());
            if (longValue >= -62167219200L) {
                long j2 = (longValue - 315569520000L) + 62167219200L;
                long b2 = Collections.b(j2, 315569520000L) + 1;
                LocalDateTime a3 = LocalDateTime.a(Collections.c(j2, 315569520000L) - 62167219200L, 0, ZoneOffset.g);
                if (b2 > 0) {
                    sb2.append('+');
                    sb2.append(b2);
                }
                sb2.append(a3);
                if (a3.c.d == 0) {
                    sb2.append(":00");
                }
            } else {
                long j3 = longValue + 62167219200L;
                long j4 = j3 / 315569520000L;
                long j5 = j3 % 315569520000L;
                LocalDateTime a4 = LocalDateTime.a(j5 - 62167219200L, 0, ZoneOffset.g);
                int length = sb.length();
                sb2.append(a4);
                if (a4.c.d == 0) {
                    sb2.append(":00");
                }
                if (j4 < 0) {
                    if (a4.b.b == -10000) {
                        sb2.replace(length, length + 2, Long.toString(j4 - 1));
                    } else if (j5 == 0) {
                        sb2.insert(length, j4);
                    } else {
                        sb2.insert(length + 1, Math.abs(j4));
                    }
                }
            }
            int i3 = this.b;
            if (i3 == -2) {
                if (a2 != 0) {
                    sb2.append('.');
                    if (a2 % 1000000 == 0) {
                        sb2.append(Integer.toString((a2 / 1000000) + AnswersRetryFilesSender.BACKOFF_MS).substring(1));
                    } else if (a2 % AnswersRetryFilesSender.BACKOFF_MS == 0) {
                        sb2.append(Integer.toString((a2 / AnswersRetryFilesSender.BACKOFF_MS) + 1000000).substring(1));
                    } else {
                        sb2.append(Integer.toString(a2 + 1000000000).substring(1));
                    }
                }
            } else if (i3 > 0 || (i3 == -1 && a2 > 0)) {
                sb2.append('.');
                int i4 = 100000000;
                while (true) {
                    if ((this.b != -1 || a2 <= 0) && i2 >= this.b) {
                        break;
                    }
                    int i5 = a2 / i4;
                    sb2.append((char) (i5 + 48));
                    a2 -= i5 * i4;
                    i4 /= 10;
                    i2++;
                }
            }
            sb2.append('Z');
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.TextStyle, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatterBuilder a(TemporalField temporalField, TextStyle textStyle) {
        Collections.a((Object) temporalField, "field");
        Collections.a((Object) textStyle, "textStyle");
        a(new p(temporalField, textStyle, DateTimeTextProvider.a()));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.j, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.Map<java.lang.Long, java.lang.String>, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public c a(q.b.a.v.j jVar, Map<Long, String> map) {
        Collections.a((Object) jVar, "field");
        Collections.a((Object) map, "textLookup");
        a(new p(jVar, TextStyle.FULL, new b(this, new SimpleDateTimeTextProvider.b(java.util.Collections.singletonMap(TextStyle.FULL, new LinkedHashMap(map))))));
        return this;
    }

    public DateTimeFormatterBuilder a(String str, String str2) {
        a(new k(str2, str));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.TextStyle, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatterBuilder a(TextStyle textStyle) {
        Collections.a((Object) textStyle, "style");
        if (textStyle == TextStyle.FULL || textStyle == TextStyle.SHORT) {
            a(new i(textStyle));
            return this;
        }
        throw new IllegalArgumentException("Style must be either full or short");
    }

    public DateTimeFormatterBuilder a(char c2) {
        a(new d(c2));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatterBuilder a(String str) {
        Collections.a((Object) str, "literal");
        if (str.length() > 0) {
            if (str.length() == 1) {
                a(new d(str.charAt(0)));
            } else {
                a(new o(str));
            }
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.t.DateTimeFormatterBuilder.e.<init>(q.b.a.t.DateTimeFormatterBuilder$f[], boolean):void
     arg types: [q.b.a.t.DateTimeFormatterBuilder$f[], int]
     candidates:
      q.b.a.t.DateTimeFormatterBuilder.e.<init>(java.util.List<q.b.a.t.c$f>, boolean):void
      q.b.a.t.DateTimeFormatterBuilder.e.<init>(q.b.a.t.DateTimeFormatterBuilder$f[], boolean):void */
    public DateTimeFormatterBuilder a(DateTimeFormatter dateTimeFormatter) {
        Collections.a((Object) dateTimeFormatter, "formatter");
        e eVar = dateTimeFormatter.a;
        if (eVar.c) {
            eVar = new e(eVar.b, false);
        }
        a(eVar);
        return this;
    }

    public DateTimeFormatterBuilder a() {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = this.a;
        if (dateTimeFormatterBuilder.b != null) {
            if (dateTimeFormatterBuilder.c.size() > 0) {
                DateTimeFormatterBuilder dateTimeFormatterBuilder2 = this.a;
                e eVar = new e(dateTimeFormatterBuilder2.c, dateTimeFormatterBuilder2.d);
                this.a = this.a.b;
                a(eVar);
            } else {
                this.a = this.a.b;
            }
            return this;
        }
        throw new IllegalStateException("Cannot call optionalEnd() as there was no previous call to optionalStart()");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatterBuilder$f, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public final int a(f fVar) {
        Collections.a((Object) fVar, "pp");
        DateTimeFormatterBuilder dateTimeFormatterBuilder = this.a;
        int i2 = dateTimeFormatterBuilder.f3135e;
        if (i2 > 0) {
            l lVar = new l(fVar, i2, dateTimeFormatterBuilder.f3136f);
            DateTimeFormatterBuilder dateTimeFormatterBuilder2 = this.a;
            dateTimeFormatterBuilder2.f3135e = 0;
            dateTimeFormatterBuilder2.f3136f = 0;
            fVar = lVar;
        }
        this.a.c.add(fVar);
        DateTimeFormatterBuilder dateTimeFormatterBuilder3 = this.a;
        dateTimeFormatterBuilder3.g = -1;
        return dateTimeFormatterBuilder3.c.size() - 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.t.DateTimeFormatterBuilder.e.<init>(java.util.List<q.b.a.t.c$f>, boolean):void
     arg types: [java.util.List<q.b.a.t.c$f>, int]
     candidates:
      q.b.a.t.DateTimeFormatterBuilder.e.<init>(q.b.a.t.DateTimeFormatterBuilder$f[], boolean):void
      q.b.a.t.DateTimeFormatterBuilder.e.<init>(java.util.List<q.b.a.t.c$f>, boolean):void */
    public DateTimeFormatter a(Locale locale) {
        Collections.a((Object) locale, "locale");
        while (this.a.b != null) {
            a();
        }
        return new DateTimeFormatter(new e(this.c, false), locale, DecimalStyle.f3149e, ResolverStyle.SMART, null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.ResolverStyle, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
     arg types: [q.b.a.t.ResolverStyle, q.b.a.t.ResolverStyle]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean */
    public DateTimeFormatter a(ResolverStyle resolverStyle) {
        DateTimeFormatter c2 = c();
        if (c2 != null) {
            Collections.a((Object) resolverStyle, "resolverStyle");
            if (Collections.a((Object) c2.d, (Object) resolverStyle)) {
                return c2;
            }
            return new DateTimeFormatter(c2.a, c2.b, c2.c, resolverStyle, c2.f3131e, c2.f3132f, c2.g);
        }
        throw null;
    }

    public DateTimeFormatterBuilder b() {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = this.a;
        dateTimeFormatterBuilder.g = -1;
        this.a = new DateTimeFormatterBuilder(dateTimeFormatterBuilder, true);
        return this;
    }
}
