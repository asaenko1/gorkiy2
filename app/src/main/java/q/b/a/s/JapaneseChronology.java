package q.b.a.s;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import q.b.a.LocalDate;
import q.b.a.c;
import q.b.a.o;
import q.b.a.v.ChronoField;
import q.b.a.v.ValueRange;
import q.b.a.v.e;

public final class JapaneseChronology extends Chronology implements Serializable {
    public static final Locale d = new Locale("ja", "JP", "JP");

    /* renamed from: e  reason: collision with root package name */
    public static final JapaneseChronology f3116e = new JapaneseChronology();

    /* renamed from: f  reason: collision with root package name */
    public static final Map<String, String[]> f3117f = new HashMap();
    public static final Map<String, String[]> g = new HashMap();
    public static final Map<String, String[]> h = new HashMap();

    static {
        f3117f.put("en", new String[]{"Unknown", "K", "M", "T", "S", "H"});
        f3117f.put("ja", new String[]{"Unknown", "K", "M", "T", "S", "H"});
        g.put("en", new String[]{"Unknown", "K", "M", "T", "S", "H"});
        g.put("ja", new String[]{"Unknown", "慶", "明", "大", "昭", "平"});
        h.put("en", new String[]{"Unknown", "Keio", "Meiji", "Taisho", "Showa", "Heisei"});
        h.put("ja", new String[]{"Unknown", "慶応", "明治", "大正", "昭和", "平成"});
    }

    private Object readResolve() {
        return f3116e;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    public ChronoLocalDate a(int i2, int i3, int i4) {
        return new JapaneseDate(LocalDate.a(i2, i3, i4));
    }

    public ChronoLocalDateTime<p> b(e eVar) {
        return super.b(eVar);
    }

    public String f() {
        return "japanese";
    }

    public String g() {
        return "Japanese";
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.s.ChronoLocalDate] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q.b.a.s.ChronoLocalDate a(q.b.a.v.TemporalAccessor r2) {
        /*
            r1 = this;
            boolean r0 = r2 instanceof q.b.a.s.JapaneseDate
            if (r0 == 0) goto L_0x0007
            q.b.a.s.JapaneseDate r2 = (q.b.a.s.JapaneseDate) r2
            goto L_0x0011
        L_0x0007:
            q.b.a.s.JapaneseDate r0 = new q.b.a.s.JapaneseDate
            q.b.a.LocalDate r2 = q.b.a.LocalDate.a(r2)
            r0.<init>(r2)
            r2 = r0
        L_0x0011:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.s.JapaneseChronology.a(q.b.a.v.TemporalAccessor):q.b.a.s.ChronoLocalDate");
    }

    public Era a(int i2) {
        return JapaneseEra.a(i2);
    }

    public ValueRange a(ChronoField chronoField) {
        int ordinal = chronoField.ordinal();
        if (!(ordinal == 15 || ordinal == 18 || ordinal == 20 || ordinal == 24)) {
            switch (ordinal) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                    break;
                default:
                    Calendar instance = Calendar.getInstance(d);
                    int ordinal2 = chronoField.ordinal();
                    int i2 = 0;
                    if (ordinal2 == 19) {
                        JapaneseEra[] j2 = JapaneseEra.j();
                        int i3 = 366;
                        while (i2 < j2.length) {
                            i3 = Math.min(i3, ((j2[i2].c.q() ? 366 : 365) - j2[i2].c.p()) + 1);
                            i2++;
                        }
                        return ValueRange.a(1, (long) i3, 366);
                    } else if (ordinal2 == 23) {
                        return ValueRange.a((long) (instance.getMinimum(2) + 1), (long) (instance.getGreatestMinimum(2) + 1), (long) (instance.getLeastMaximum(2) + 1), (long) (instance.getMaximum(2) + 1));
                    } else {
                        switch (ordinal2) {
                            case 25:
                                JapaneseEra[] j3 = JapaneseEra.j();
                                int i4 = (j3[j3.length - 1].i().b - j3[j3.length - 1].c.b) + 1;
                                int i5 = Integer.MAX_VALUE;
                                while (i2 < j3.length) {
                                    i5 = Math.min(i5, (j3[i2].i().b - j3[i2].c.b) + 1);
                                    i2++;
                                }
                                return ValueRange.a(1, 6, (long) i5, (long) i4);
                            case 26:
                                JapaneseEra[] j4 = JapaneseEra.j();
                                return ValueRange.a((long) JapaneseDate.f3118e.b, (long) j4[j4.length - 1].i().b);
                            case 27:
                                JapaneseEra[] j5 = JapaneseEra.j();
                                return ValueRange.a((long) j5[0].b, (long) j5[j5.length - 1].b);
                            default:
                                throw new UnsupportedOperationException("Unimplementable field: " + chronoField);
                        }
                    }
            }
        }
        return chronoField.range;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
     arg types: [q.b.a.s.JapaneseChronology, q.b.a.c, q.b.a.o]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R> */
    public ChronoZonedDateTime<p> a(c cVar, o oVar) {
        return ChronoZonedDateTimeImpl.a((h) super, cVar, oVar);
    }
}
