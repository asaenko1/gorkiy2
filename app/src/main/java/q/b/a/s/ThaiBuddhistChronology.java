package q.b.a.s;

import java.io.Serializable;
import java.util.HashMap;
import q.b.a.LocalDate;
import q.b.a.c;
import q.b.a.o;
import q.b.a.v.ChronoField;
import q.b.a.v.ValueRange;
import q.b.a.v.e;

public final class ThaiBuddhistChronology extends Chronology implements Serializable {
    public static final ThaiBuddhistChronology d = new ThaiBuddhistChronology();

    /* renamed from: e  reason: collision with root package name */
    public static final HashMap<String, String[]> f3122e = new HashMap<>();

    /* renamed from: f  reason: collision with root package name */
    public static final HashMap<String, String[]> f3123f = new HashMap<>();
    public static final HashMap<String, String[]> g = new HashMap<>();

    static {
        f3122e.put("en", new String[]{"BB", "BE"});
        f3122e.put("th", new String[]{"BB", "BE"});
        f3123f.put("en", new String[]{"B.B.", "B.E."});
        f3123f.put("th", new String[]{"พ.ศ.", "ปีก่อนคริสต์กาลที่"});
        g.put("en", new String[]{"Before Buddhist", "Budhhist Era"});
        g.put("th", new String[]{"พุทธศักราช", "ปีก่อนคริสต์กาลที่"});
    }

    private Object readResolve() {
        return d;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.s.ThaiBuddhistDate, q.b.a.s.ChronoLocalDate] */
    public ChronoLocalDate a(int i2, int i3, int i4) {
        return new ThaiBuddhistDate(LocalDate.a(i2 - 543, i3, i4));
    }

    public ChronoLocalDateTime<w> b(e eVar) {
        return super.b(eVar);
    }

    public String f() {
        return "buddhist";
    }

    public String g() {
        return "ThaiBuddhist";
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.s.ChronoLocalDate] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ThaiBuddhistDate, q.b.a.s.ChronoLocalDate] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q.b.a.s.ChronoLocalDate a(q.b.a.v.TemporalAccessor r2) {
        /*
            r1 = this;
            boolean r0 = r2 instanceof q.b.a.s.ThaiBuddhistDate
            if (r0 == 0) goto L_0x0007
            q.b.a.s.ThaiBuddhistDate r2 = (q.b.a.s.ThaiBuddhistDate) r2
            goto L_0x0011
        L_0x0007:
            q.b.a.s.ThaiBuddhistDate r0 = new q.b.a.s.ThaiBuddhistDate
            q.b.a.LocalDate r2 = q.b.a.LocalDate.a(r2)
            r0.<init>(r2)
            r2 = r0
        L_0x0011:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.s.ThaiBuddhistChronology.a(q.b.a.v.TemporalAccessor):q.b.a.s.ChronoLocalDate");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.s.Era, q.b.a.s.ThaiBuddhistEra] */
    public Era a(int i2) {
        return ThaiBuddhistEra.a(i2);
    }

    public ValueRange a(ChronoField chronoField) {
        switch (chronoField.ordinal()) {
            case 24:
                ValueRange valueRange = ChronoField.PROLEPTIC_MONTH.range;
                return ValueRange.a(valueRange.b + 6516, valueRange.f3152e + 6516);
            case 25:
                ValueRange valueRange2 = ChronoField.YEAR.range;
                return ValueRange.a(1, (-(valueRange2.b + 543)) + 1, valueRange2.f3152e + 543);
            case 26:
                ValueRange valueRange3 = ChronoField.YEAR.range;
                return ValueRange.a(valueRange3.b + 543, valueRange3.f3152e + 543);
            default:
                return chronoField.range;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
     arg types: [q.b.a.s.ThaiBuddhistChronology, q.b.a.c, q.b.a.o]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R> */
    public ChronoZonedDateTime<w> a(c cVar, o oVar) {
        return ChronoZonedDateTimeImpl.a((h) super, cVar, oVar);
    }
}
