package q.b.a.s;

import j.a.a.a.outline;
import java.io.DataInput;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;

public enum HijrahEra implements i {
    BEFORE_AH,
    AH;

    public static HijrahEra a(int i2) {
        if (i2 == 0) {
            return BEFORE_AH;
        }
        if (i2 == 1) {
            return AH;
        }
        throw new DateTimeException("HijrahEra not valid");
    }

    private Object writeReplace() {
        return new Ser((byte) 4, this);
    }

    public int b(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return ordinal();
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.HijrahEra] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.ERA) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, java.lang.Enum, q.b.a.s.HijrahEra] */
    public long d(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return (long) ordinal();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public int getValue() {
        return ordinal();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.HijrahEra] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return ValueRange.a(1, 1);
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.ERA, (long) ordinal());
    }

    public static HijrahEra a(DataInput dataInput) {
        return a(dataInput.readByte());
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.HijrahEra] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.ERAS;
        }
        if (temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g) {
            return null;
        }
        return temporalQuery.a(this);
    }
}
