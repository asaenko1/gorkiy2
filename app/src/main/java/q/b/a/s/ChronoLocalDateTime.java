package q.b.a.s;

import n.i.Collections;
import q.b.a.LocalDate;
import q.b.a.LocalTime;
import q.b.a.ZoneOffset;
import q.b.a.o;
import q.b.a.s.b;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.d;
import q.b.a.v.f;
import q.b.a.v.j;
import q.b.a.v.m;

public abstract class ChronoLocalDateTime<D extends b> extends q.b.a.u.b implements d, f, Comparable<ChronoLocalDateTime<?>> {
    public abstract ChronoLocalDateTime<D> a(j jVar, long j2);

    public abstract ChronoZonedDateTime<D> a(o oVar);

    public abstract ChronoLocalDateTime<D> b(long j2, m mVar);

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChronoLocalDateTime) || compareTo((ChronoLocalDateTime) obj) != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return j().hashCode() ^ l().hashCode();
    }

    public Chronology i() {
        return j().i();
    }

    public abstract D j();

    public abstract LocalTime l();

    public String toString() {
        return j().toString() + 'T' + l().toString();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.s.ChronoLocalDateTime] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return i();
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.NANOS;
        }
        if (temporalQuery == TemporalQueries.f3151f) {
            return LocalDate.e(j().l());
        }
        if (temporalQuery == TemporalQueries.g) {
            return l();
        }
        if (temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return ChronoLocalDateTime.super.a(temporalQuery);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.Temporal, q.b.a.s.ChronoLocalDateTime] */
    public ChronoLocalDateTime<D> a(f fVar) {
        return j().i().b((d) fVar.a(this));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDateTime, q.b.a.u.DefaultInterfaceTemporal] */
    public ChronoLocalDateTime<D> a(long j2, m mVar) {
        return j().i().b((d) ChronoLocalDateTime.super.a(j2, mVar));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.EPOCH_DAY, j().l()).a((TemporalField) ChronoField.NANO_OF_DAY, l().i());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public long a(ZoneOffset zoneOffset) {
        Collections.a((Object) zoneOffset, "offset");
        return ((j().l() * 86400) + ((long) l().j())) - ((long) zoneOffset.c);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDate] */
    /* renamed from: a */
    public int compareTo(ChronoLocalDateTime<?> chronoLocalDateTime) {
        int a = j().compareTo((ChronoLocalDate) chronoLocalDateTime.j());
        if (a != 0) {
            return a;
        }
        int a2 = l().compareTo(chronoLocalDateTime.l());
        return a2 == 0 ? i().compareTo(chronoLocalDateTime.i()) : a2;
    }
}
