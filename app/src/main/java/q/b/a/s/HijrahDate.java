package q.b.a.s;

import j.a.a.a.outline;
import java.io.DataInput;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.StringTokenizer;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.DayOfWeek;
import q.b.a.f;
import q.b.a.v.ChronoField;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAmount;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.m;

public final class HijrahDate extends ChronoDateImpl<k> implements Serializable {
    public static final Integer[] A = new Integer[f3102i.length];
    public static final Integer[] B = new Integer[f3103j.length];
    public static final Integer[] C = new Integer[f3104k.length];
    public static final Integer[] D = new Integer[f3105l.length];
    public static final Integer[] E = new Integer[f3109p.length];

    /* renamed from: i  reason: collision with root package name */
    public static final int[] f3102i = {0, 30, 59, 89, 118, 148, 177, 207, 236, 266, 295, 325};

    /* renamed from: j  reason: collision with root package name */
    public static final int[] f3103j = {0, 30, 59, 89, 118, 148, 177, 207, 236, 266, 295, 325};

    /* renamed from: k  reason: collision with root package name */
    public static final int[] f3104k = {30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29};

    /* renamed from: l  reason: collision with root package name */
    public static final int[] f3105l = {30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 30};

    /* renamed from: m  reason: collision with root package name */
    public static final int[] f3106m = {0, 1, 0, 1, 0, 1, 1};

    /* renamed from: n  reason: collision with root package name */
    public static final int[] f3107n = {1, 9999, 11, 51, 5, 29, 354};

    /* renamed from: o  reason: collision with root package name */
    public static final int[] f3108o = {1, 9999, 11, 52, 6, 30, 355};

    /* renamed from: p  reason: collision with root package name */
    public static final int[] f3109p = {0, 354, 709, 1063, 1417, 1772, 2126, 2481, 2835, 3189, 3544, 3898, 4252, 4607, 4961, 5315, 5670, 6024, 6379, 6733, 7087, 7442, 7796, 8150, 8505, 8859, 9214, 9568, 9922, 10277};

    /* renamed from: q  reason: collision with root package name */
    public static final char f3110q = File.separatorChar;

    /* renamed from: r  reason: collision with root package name */
    public static final String f3111r = File.pathSeparator;

    /* renamed from: s  reason: collision with root package name */
    public static final String f3112s;

    /* renamed from: t  reason: collision with root package name */
    public static final HashMap<Integer, Integer[]> f3113t = new HashMap<>();
    public static final HashMap<Integer, Integer[]> u = new HashMap<>();
    public static final HashMap<Integer, Integer[]> v = new HashMap<>();
    public static final Long[] w = new Long[334];
    public static final Integer[] x = new Integer[f3106m.length];
    public static final Integer[] y = new Integer[f3107n.length];
    public static final Integer[] z = new Integer[f3108o.length];
    public final transient HijrahEra b;
    public final transient int c;
    public final transient int d;

    /* renamed from: e  reason: collision with root package name */
    public final transient int f3114e;

    /* renamed from: f  reason: collision with root package name */
    public final transient int f3115f;
    public final transient DayOfWeek g;
    public final long h;

    static {
        StringBuilder a = outline.a("org");
        a.append(f3110q);
        a.append("threeten");
        a.append(f3110q);
        a.append("bp");
        a.append(f3110q);
        a.append("chrono");
        f3112s = a.toString();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int[] iArr = f3102i;
            if (i3 >= iArr.length) {
                break;
            }
            A[i3] = Integer.valueOf(iArr[i3]);
            i3++;
        }
        int i4 = 0;
        while (true) {
            int[] iArr2 = f3103j;
            if (i4 >= iArr2.length) {
                break;
            }
            B[i4] = Integer.valueOf(iArr2[i4]);
            i4++;
        }
        int i5 = 0;
        while (true) {
            int[] iArr3 = f3104k;
            if (i5 >= iArr3.length) {
                break;
            }
            C[i5] = Integer.valueOf(iArr3[i5]);
            i5++;
        }
        int i6 = 0;
        while (true) {
            int[] iArr4 = f3105l;
            if (i6 >= iArr4.length) {
                break;
            }
            D[i6] = Integer.valueOf(iArr4[i6]);
            i6++;
        }
        int i7 = 0;
        while (true) {
            int[] iArr5 = f3109p;
            if (i7 >= iArr5.length) {
                break;
            }
            E[i7] = Integer.valueOf(iArr5[i7]);
            i7++;
        }
        int i8 = 0;
        while (true) {
            Long[] lArr = w;
            if (i8 >= lArr.length) {
                break;
            }
            lArr[i8] = Long.valueOf((long) (i8 * 10631));
            i8++;
        }
        int i9 = 0;
        while (true) {
            int[] iArr6 = f3106m;
            if (i9 >= iArr6.length) {
                break;
            }
            x[i9] = Integer.valueOf(iArr6[i9]);
            i9++;
        }
        int i10 = 0;
        while (true) {
            int[] iArr7 = f3107n;
            if (i10 >= iArr7.length) {
                break;
            }
            y[i10] = Integer.valueOf(iArr7[i10]);
            i10++;
        }
        while (true) {
            int[] iArr8 = f3108o;
            if (i2 < iArr8.length) {
                z[i2] = Integer.valueOf(iArr8[i2]);
                i2++;
            } else {
                try {
                    o();
                    return;
                } catch (IOException | ParseException unused) {
                    return;
                }
            }
        }
    }

    public HijrahDate(long j2) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        Long l2;
        int i8;
        long j3 = j2 - -492148;
        if (j3 >= 0) {
            Long[] lArr = w;
            int i9 = 0;
            while (i9 < lArr.length) {
                try {
                    if (j3 < lArr[i9].longValue()) {
                        i7 = i9 - 1;
                        break;
                    }
                    i9++;
                } catch (ArrayIndexOutOfBoundsException unused) {
                    i8 = ((int) j3) / 10631;
                }
            }
            i8 = ((int) j3) / 10631;
            i7 = i8;
            try {
                l2 = w[i7];
            } catch (ArrayIndexOutOfBoundsException unused2) {
                l2 = null;
            }
            int longValue = (int) (j3 - (l2 == null ? Long.valueOf((long) (i7 * 10631)) : l2).longValue());
            int a = a(i7, (long) longValue);
            i6 = b(i7, longValue, a);
            i5 = (i7 * 30) + a + 1;
            i4 = a(i6, i5);
            i3 = a(i6, i4, i5) + 1;
            HijrahEra hijrahEra = HijrahEra.AH;
            i2 = 1;
        } else {
            int i10 = (int) j3;
            int i11 = i10 / 10631;
            int i12 = i10 % 10631;
            if (i12 == 0) {
                i12 = -10631;
                i11++;
            }
            int a2 = a(i11, (long) i12);
            int b2 = b(i11, i12, a2);
            i5 = 1 - ((i11 * 30) - a2);
            i6 = d((long) i5) ? b2 + 355 : b2 + 354;
            i4 = a(i6, i5);
            i3 = a(i6, i4, i5) + 1;
            HijrahEra hijrahEra2 = HijrahEra.BEFORE_AH;
            i2 = 0;
        }
        int i13 = (int) ((j3 + 5) % 7);
        int[] iArr = {i2, i5, i4 + 1, i3, i6 + 1, i13 + (i13 <= 0 ? 7 : 0)};
        int i14 = iArr[1];
        if (i14 < 1 || i14 > 9999) {
            throw new DateTimeException("Invalid year of Hijrah Era");
        }
        int i15 = iArr[2];
        if (i15 < 1 || i15 > 12) {
            throw new DateTimeException("Invalid month of Hijrah date");
        }
        a(iArr[3]);
        int i16 = iArr[4];
        if (i16 < 1 || i16 > z[6].intValue()) {
            throw new DateTimeException("Invalid day of year of Hijrah date");
        }
        this.b = HijrahEra.a(iArr[0]);
        this.c = iArr[1];
        this.d = iArr[2];
        this.f3114e = iArr[3];
        this.f3115f = iArr[4];
        this.g = DayOfWeek.a(iArr[5]);
        this.h = j2;
        d((long) this.c);
    }

    public static HijrahDate d(int i2, int i3, int i4) {
        if (i2 >= 1) {
            return a(HijrahEra.AH, i2, i3, i4);
        }
        return a(HijrahEra.BEFORE_AH, 1 - i2, i3, i4);
    }

    public static HijrahDate e(int i2, int i3, int i4) {
        int intValue = c(i2)[i3 - 1].intValue();
        if (i4 > intValue) {
            i4 = intValue;
        }
        return d(i2, i3, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x010c A[SYNTHETIC, Splitter:B:50:0x010c] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void o() {
        /*
            java.lang.String r0 = "org.threeten.bp.i18n.HijrahDate.deviationConfigFile"
            java.lang.String r0 = java.lang.System.getProperty(r0)
            if (r0 != 0) goto L_0x000a
            java.lang.String r0 = "hijrah_deviation.cfg"
        L_0x000a:
            java.lang.String r1 = "org.threeten.bp.i18n.HijrahDate.deviationConfigDir"
            java.lang.String r1 = java.lang.System.getProperty(r1)
            r2 = 0
            if (r1 == 0) goto L_0x0058
            int r3 = r1.length()
            java.lang.String r4 = "file.separator"
            if (r3 != 0) goto L_0x0025
            java.lang.String r3 = java.lang.System.getProperty(r4)
            boolean r3 = r1.endsWith(r3)
            if (r3 != 0) goto L_0x0034
        L_0x0025:
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            java.lang.String r3 = java.lang.System.getProperty(r4)
            r1.append(r3)
            java.lang.String r1 = r1.toString()
        L_0x0034:
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            char r4 = q.b.a.s.HijrahDate.f3110q
            r1.append(r4)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r3.<init>(r0)
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x0109
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0056 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0056 }
            goto L_0x010a
        L_0x0056:
            r0 = move-exception
            throw r0
        L_0x0058:
            java.lang.String r1 = "java.class.path"
            java.lang.String r1 = java.lang.System.getProperty(r1)
            java.util.StringTokenizer r3 = new java.util.StringTokenizer
            java.lang.String r4 = q.b.a.s.HijrahDate.f3111r
            r3.<init>(r1, r4)
        L_0x0065:
            boolean r1 = r3.hasMoreTokens()
            if (r1 == 0) goto L_0x0109
            java.lang.String r1 = r3.nextToken()
            java.io.File r4 = new java.io.File
            r4.<init>(r1)
            boolean r5 = r4.exists()
            if (r5 == 0) goto L_0x0065
            boolean r5 = r4.isDirectory()
            if (r5 == 0) goto L_0x00c4
            java.io.File r4 = new java.io.File
            java.lang.StringBuilder r5 = j.a.a.a.outline.a(r1)
            char r6 = q.b.a.s.HijrahDate.f3110q
            r5.append(r6)
            java.lang.String r6 = q.b.a.s.HijrahDate.f3112s
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.<init>(r5, r0)
            boolean r4 = r4.exists()
            if (r4 == 0) goto L_0x0065
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00c2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00c2 }
            r4.<init>()     // Catch:{ IOException -> 0x00c2 }
            r4.append(r1)     // Catch:{ IOException -> 0x00c2 }
            char r1 = q.b.a.s.HijrahDate.f3110q     // Catch:{ IOException -> 0x00c2 }
            r4.append(r1)     // Catch:{ IOException -> 0x00c2 }
            java.lang.String r1 = q.b.a.s.HijrahDate.f3112s     // Catch:{ IOException -> 0x00c2 }
            r4.append(r1)     // Catch:{ IOException -> 0x00c2 }
            char r1 = q.b.a.s.HijrahDate.f3110q     // Catch:{ IOException -> 0x00c2 }
            r4.append(r1)     // Catch:{ IOException -> 0x00c2 }
            r4.append(r0)     // Catch:{ IOException -> 0x00c2 }
            java.lang.String r0 = r4.toString()     // Catch:{ IOException -> 0x00c2 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x00c2 }
            r0 = r3
            goto L_0x010a
        L_0x00c2:
            r0 = move-exception
            throw r0
        L_0x00c4:
            java.util.zip.ZipFile r1 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x00ca }
            r1.<init>(r4)     // Catch:{ IOException -> 0x00ca }
            goto L_0x00cb
        L_0x00ca:
            r1 = r2
        L_0x00cb:
            if (r1 == 0) goto L_0x0065
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = q.b.a.s.HijrahDate.f3112s
            r4.append(r5)
            char r5 = q.b.a.s.HijrahDate.f3110q
            r4.append(r5)
            r4.append(r0)
            java.lang.String r4 = r4.toString()
            java.util.zip.ZipEntry r5 = r1.getEntry(r4)
            if (r5 != 0) goto L_0x0100
            char r5 = q.b.a.s.HijrahDate.f3110q
            r6 = 92
            r7 = 47
            if (r5 != r7) goto L_0x00f6
            java.lang.String r4 = r4.replace(r7, r6)
            goto L_0x00fc
        L_0x00f6:
            if (r5 != r6) goto L_0x00fc
            java.lang.String r4 = r4.replace(r6, r7)
        L_0x00fc:
            java.util.zip.ZipEntry r5 = r1.getEntry(r4)
        L_0x0100:
            if (r5 == 0) goto L_0x0065
            java.io.InputStream r0 = r1.getInputStream(r5)     // Catch:{ IOException -> 0x0107 }
            goto L_0x010a
        L_0x0107:
            r0 = move-exception
            throw r0
        L_0x0109:
            r0 = r2
        L_0x010a:
            if (r0 == 0) goto L_0x0135
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x012e }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x012e }
            r3.<init>(r0)     // Catch:{ all -> 0x012e }
            r1.<init>(r3)     // Catch:{ all -> 0x012e }
            r0 = 0
        L_0x0117:
            java.lang.String r2 = r1.readLine()     // Catch:{ all -> 0x012b }
            if (r2 == 0) goto L_0x0127
            int r0 = r0 + 1
            java.lang.String r2 = r2.trim()     // Catch:{ all -> 0x012b }
            a(r2, r0)     // Catch:{ all -> 0x012b }
            goto L_0x0117
        L_0x0127:
            r1.close()
            goto L_0x0135
        L_0x012b:
            r0 = move-exception
            r2 = r1
            goto L_0x012f
        L_0x012e:
            r0 = move-exception
        L_0x012f:
            if (r2 == 0) goto L_0x0134
            r2.close()
        L_0x0134:
            throw r0
        L_0x0135:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.s.HijrahDate.o():void");
    }

    private Object readResolve() {
        return new HijrahDate(this.h);
    }

    private Object writeReplace() {
        return new Ser((byte) 3, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.HijrahDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.HijrahDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    public ChronoDateImpl b(long j2, TemporalUnit temporalUnit) {
        return (HijrahDate) super.b(j2, (m) temporalUnit);
    }

    public ChronoDateImpl c(long j2) {
        if (j2 == 0) {
            return super;
        }
        return a(this.b, Collections.d(this.c, (int) j2), this.d, this.f3114e);
    }

    public Chronology i() {
        return HijrahChronology.d;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.s.Era, q.b.a.s.HijrahEra] */
    public Era j() {
        return this.b;
    }

    public long l() {
        return c(this.c, this.d, this.f3114e);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.HijrahDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.HijrahDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    /* renamed from: b  reason: collision with other method in class */
    public ChronoLocalDate m18b(long j2, TemporalUnit temporalUnit) {
        return (HijrahDate) super.b(j2, (m) temporalUnit);
    }

    public static long c(int i2, int i3, int i4) {
        Long l2;
        int i5 = i2 - 1;
        int i6 = i5 / 30;
        int i7 = i5 % 30;
        int intValue = b(i6)[Math.abs(i7)].intValue();
        if (i7 < 0) {
            intValue = -intValue;
        }
        try {
            l2 = w[i6];
        } catch (ArrayIndexOutOfBoundsException unused) {
            l2 = null;
        }
        if (l2 == null) {
            l2 = Long.valueOf((long) (i6 * 10631));
        }
        return (((l2.longValue() + ((long) intValue)) - 492148) - 1) + ((long) c(i2)[i3 - 1].intValue()) + ((long) i4);
    }

    public final ChronoLocalDateTime<k> a(f fVar) {
        return new ChronoLocalDateTimeImpl(this, fVar);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.Temporal, q.b.a.s.HijrahDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.HijrahDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.HijrahDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    /* renamed from: b  reason: collision with other method in class */
    public Temporal m19b(long j2, TemporalUnit temporalUnit) {
        return (HijrahDate) super.b(j2, (m) temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.HijrahDate] */
    public long d(TemporalField temporalField) {
        int i2;
        int i3;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        switch (((ChronoField) temporalField).ordinal()) {
            case 15:
                i2 = this.g.getValue();
                break;
            case 16:
                i3 = (this.f3114e - 1) % 7;
                i2 = i3 + 1;
                break;
            case 17:
                i3 = (this.f3115f - 1) % 7;
                i2 = i3 + 1;
                break;
            case 18:
                i2 = this.f3114e;
                break;
            case 19:
                i2 = this.f3115f;
                break;
            case 20:
                return l();
            case 21:
                i3 = (this.f3114e - 1) / 7;
                i2 = i3 + 1;
                break;
            case 22:
                i3 = (this.f3115f - 1) / 7;
                i2 = i3 + 1;
                break;
            case 23:
                i2 = this.d;
                break;
            case 24:
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
            case 25:
                i2 = this.c;
                break;
            case 26:
                i2 = this.c;
                break;
            case 27:
                return (long) this.b.ordinal();
        }
        return (long) i2;
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    public ChronoLocalDate a(TemporalAdjuster temporalAdjuster) {
        return (HijrahDate) i().a((d) temporalAdjuster.a(this));
    }

    public ChronoDateImpl b(long j2) {
        if (j2 == 0) {
            return super;
        }
        int i2 = (this.d - 1) + ((int) j2);
        int i3 = i2 / 12;
        int i4 = i2 % 12;
        while (i4 < 0) {
            i4 += 12;
            i3 = Collections.e(i3, 1);
        }
        return a(this.b, Collections.d(this.c, i3), i4 + 1, this.f3114e);
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.v.Temporal, q.b.a.s.HijrahDate] */
    /* renamed from: a  reason: collision with other method in class */
    public Temporal m17a(TemporalAdjuster temporalAdjuster) {
        return (HijrahDate) i().a((d) temporalAdjuster.a(this));
    }

    public static Integer[] b(int i2) {
        Integer[] numArr;
        try {
            numArr = v.get(Integer.valueOf(i2));
        } catch (ArrayIndexOutOfBoundsException unused) {
            numArr = null;
        }
        return numArr == null ? E : numArr;
    }

    public static int b(int i2, int i3, int i4) {
        Integer[] b2 = b(i2);
        if (i3 > 0) {
            return i3 - b2[i4].intValue();
        }
        return b2[i4].intValue() + i3;
    }

    public static Integer[] c(int i2) {
        Integer[] numArr;
        try {
            numArr = f3113t.get(Integer.valueOf(i2));
        } catch (ArrayIndexOutOfBoundsException unused) {
            numArr = null;
        }
        if (numArr != null) {
            return numArr;
        }
        if (d((long) i2)) {
            return B;
        }
        return A;
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    public ChronoLocalDate a(TemporalAmount temporalAmount) {
        return (HijrahDate) i().a((d) temporalAmount.a(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.s.HijrahEra, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static HijrahDate a(HijrahEra hijrahEra, int i2, int i3, int i4) {
        Collections.a((Object) hijrahEra, "era");
        if (i2 < 1 || i2 > 9999) {
            throw new DateTimeException("Invalid year of Hijrah Era");
        } else if (i3 < 1 || i3 > 12) {
            throw new DateTimeException("Invalid month of Hijrah date");
        } else {
            a(i4);
            if (hijrahEra != HijrahEra.AH) {
                i2 = 1 - i2;
            }
            return new HijrahDate(c(i2, i3, i4));
        }
    }

    public static boolean d(long j2) {
        if (j2 <= 0) {
            j2 = -j2;
        }
        return ((j2 * 11) + 14) % 30 < 11;
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    public ValueRange a(TemporalField temporalField) {
        int i2;
        int i3;
        int i4;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (c(temporalField)) {
            ChronoField chronoField = (ChronoField) temporalField;
            int ordinal = chronoField.ordinal();
            Integer[] numArr = null;
            if (ordinal == 18) {
                int i5 = this.d - 1;
                int i6 = this.c;
                try {
                    numArr = u.get(Integer.valueOf(i6));
                } catch (ArrayIndexOutOfBoundsException unused) {
                }
                if (numArr == null) {
                    if (d((long) i6)) {
                        numArr = D;
                    } else {
                        numArr = C;
                    }
                }
                return ValueRange.a(1, (long) numArr[i5].intValue());
            } else if (ordinal == 19) {
                int i7 = this.c;
                int i8 = i7 - 1;
                int i9 = i8 / 30;
                try {
                    numArr = v.get(Integer.valueOf(i9));
                } catch (ArrayIndexOutOfBoundsException unused2) {
                }
                if (numArr != null) {
                    int i10 = i8 % 30;
                    if (i10 == 29) {
                        i3 = w[i9 + 1].intValue() - w[i9].intValue();
                        i4 = numArr[i10].intValue();
                    } else {
                        i3 = numArr[i10 + 1].intValue();
                        i4 = numArr[i10].intValue();
                    }
                    i2 = i3 - i4;
                } else {
                    i2 = d((long) i7) ? 355 : 354;
                }
                return ValueRange.a(1, (long) i2);
            } else if (ordinal == 21) {
                return ValueRange.a(1, 5);
            } else {
                if (ordinal == 25) {
                    return ValueRange.a(1, 1000);
                }
                HijrahChronology hijrahChronology = HijrahChronology.d;
                return chronoField.range;
            }
        } else {
            throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v15, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v17, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public HijrahDate a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (HijrahDate) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        r0.range.b(j2, r0);
        int i2 = (int) j2;
        switch (r0.ordinal()) {
            case 15:
                return m15a(j2 - ((long) this.g.getValue()));
            case 16:
                return m15a(j2 - d((TemporalField) ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH));
            case 17:
                return m15a(j2 - d((TemporalField) ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR));
            case 18:
                return e(this.c, this.d, i2);
            case 19:
                int i3 = i2 - 1;
                return e(this.c, (i3 / 30) + 1, (i3 % 30) + 1);
            case 20:
                return new HijrahDate((long) i2);
            case 21:
                return m15a((j2 - d((TemporalField) ChronoField.ALIGNED_WEEK_OF_MONTH)) * 7);
            case 22:
                return m15a((j2 - d((TemporalField) ChronoField.ALIGNED_WEEK_OF_YEAR)) * 7);
            case 23:
                return e(this.c, i2, this.f3114e);
            case 24:
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
            case 25:
                if (this.c < 1) {
                    i2 = 1 - i2;
                }
                return e(i2, this.d, this.f3114e);
            case 26:
                return e(i2, this.d, this.f3114e);
            case 27:
                return e(1 - this.c, this.d, this.f3114e);
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.HijrahDate.a(int, int):int
      q.b.a.s.HijrahDate.a(int, long):int
      q.b.a.s.HijrahDate.a(java.lang.String, int):void
      q.b.a.s.HijrahDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.HijrahDate.a(q.b.a.v.TemporalField, long):q.b.a.s.HijrahDate
      q.b.a.s.HijrahDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.HijrahDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
    public ChronoLocalDate a(long j2, TemporalUnit temporalUnit) {
        return (HijrahDate) HijrahDate.super.a(j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.Temporal, q.b.a.s.HijrahDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.HijrahDate.a(int, int):int
      q.b.a.s.HijrahDate.a(int, long):int
      q.b.a.s.HijrahDate.a(java.lang.String, int):void
      q.b.a.s.HijrahDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.HijrahDate.a(q.b.a.v.TemporalField, long):q.b.a.s.HijrahDate
      q.b.a.s.HijrahDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.HijrahDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
    /* renamed from: a  reason: collision with other method in class */
    public Temporal m16a(long j2, TemporalUnit temporalUnit) {
        return (HijrahDate) HijrahDate.super.a(j2, temporalUnit);
    }

    public ChronoDateImpl a(long j2) {
        return new HijrahDate(this.h + j2);
    }

    /* renamed from: a  reason: collision with other method in class */
    public HijrahDate m15a(long j2) {
        return new HijrahDate(this.h + j2);
    }

    public static int a(int i2, long j2) {
        Integer[] b2 = b(i2);
        int i3 = 0;
        int i4 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i4 == 0) {
            return 0;
        }
        if (i4 > 0) {
            while (i3 < b2.length) {
                if (j2 < ((long) b2[i3].intValue())) {
                    return i3 - 1;
                }
                i3++;
            }
            return 29;
        }
        long j3 = -j2;
        while (i3 < b2.length) {
            if (j3 <= ((long) b2[i3].intValue())) {
                return i3 - 1;
            }
            i3++;
        }
        return 29;
    }

    public static int a(int i2, int i3) {
        Integer[] c2 = c(i3);
        int i4 = 0;
        if (i2 >= 0) {
            while (i4 < c2.length) {
                if (i2 < c2[i4].intValue()) {
                    return i4 - 1;
                }
                i4++;
            }
            return 11;
        }
        int i5 = d((long) i3) ? i2 + 355 : i2 + 354;
        while (i4 < c2.length) {
            if (i5 < c2[i4].intValue()) {
                return i4 - 1;
            }
            i4++;
        }
        return 11;
    }

    public static int a(int i2, int i3, int i4) {
        int intValue;
        Integer[] c2 = c(i4);
        if (i2 < 0) {
            i2 = d((long) i4) ? i2 + 355 : i2 + 354;
            if (i3 <= 0) {
                return i2;
            }
            intValue = c2[i3].intValue();
        } else if (i3 <= 0) {
            return i2;
        } else {
            intValue = c2[i3].intValue();
        }
        return i2 - intValue;
    }

    public static void a(int i2) {
        if (i2 < 1 || i2 > z[5].intValue()) {
            StringBuilder a = outline.a("Invalid day of month of Hijrah date, day ", i2, " greater than ");
            a.append(z[5].intValue());
            a.append(" or less than 1");
            throw new DateTimeException(a.toString());
        }
    }

    public static void a(String str, int i2) {
        StringTokenizer stringTokenizer;
        int i3 = i2;
        StringTokenizer stringTokenizer2 = new StringTokenizer(str, ";");
        while (stringTokenizer2.hasMoreTokens()) {
            String nextToken = stringTokenizer2.nextToken();
            int indexOf = nextToken.indexOf(58);
            if (indexOf != -1) {
                try {
                    int parseInt = Integer.parseInt(nextToken.substring(indexOf + 1, nextToken.length()));
                    int indexOf2 = nextToken.indexOf(45);
                    if (indexOf2 != -1) {
                        String substring = nextToken.substring(0, indexOf2);
                        String substring2 = nextToken.substring(indexOf2 + 1, indexOf);
                        int indexOf3 = substring.indexOf(47);
                        int indexOf4 = substring2.indexOf(47);
                        if (indexOf3 != -1) {
                            String substring3 = substring.substring(0, indexOf3);
                            String substring4 = substring.substring(indexOf3 + 1, substring.length());
                            try {
                                int parseInt2 = Integer.parseInt(substring3);
                                try {
                                    int parseInt3 = Integer.parseInt(substring4);
                                    if (indexOf4 != -1) {
                                        String substring5 = substring2.substring(0, indexOf4);
                                        String substring6 = substring2.substring(indexOf4 + 1, substring2.length());
                                        try {
                                            int parseInt4 = Integer.parseInt(substring5);
                                            try {
                                                int parseInt5 = Integer.parseInt(substring6);
                                                if (parseInt2 == -1 || parseInt3 == -1 || parseInt4 == -1 || parseInt5 == -1) {
                                                    int i4 = i2;
                                                    throw new ParseException(outline.b("Unknown error at line ", i4, "."), i4);
                                                } else if (parseInt2 < 1) {
                                                    throw new IllegalArgumentException("startYear < 1");
                                                } else if (parseInt4 < 1) {
                                                    throw new IllegalArgumentException("endYear < 1");
                                                } else if (parseInt3 < 0 || parseInt3 > 11) {
                                                    throw new IllegalArgumentException("startMonth < 0 || startMonth > 11");
                                                } else if (parseInt5 < 0 || parseInt5 > 11) {
                                                    throw new IllegalArgumentException("endMonth < 0 || endMonth > 11");
                                                } else if (parseInt4 > 9999) {
                                                    throw new IllegalArgumentException("endYear > 9999");
                                                } else if (parseInt4 < parseInt2) {
                                                    throw new IllegalArgumentException("startYear > endYear");
                                                } else if (parseInt4 != parseInt2 || parseInt5 >= parseInt3) {
                                                    boolean d2 = d((long) parseInt2);
                                                    Integer[] numArr = f3113t.get(Integer.valueOf(parseInt2));
                                                    if (numArr == null) {
                                                        if (!d2) {
                                                            numArr = new Integer[f3102i.length];
                                                            int i5 = 0;
                                                            while (true) {
                                                                int[] iArr = f3102i;
                                                                if (i5 >= iArr.length) {
                                                                    break;
                                                                }
                                                                numArr[i5] = Integer.valueOf(iArr[i5]);
                                                                i5++;
                                                            }
                                                        } else {
                                                            numArr = new Integer[f3103j.length];
                                                            int i6 = 0;
                                                            while (true) {
                                                                int[] iArr2 = f3103j;
                                                                if (i6 >= iArr2.length) {
                                                                    break;
                                                                }
                                                                numArr[i6] = Integer.valueOf(iArr2[i6]);
                                                                i6++;
                                                            }
                                                        }
                                                    }
                                                    Integer[] numArr2 = new Integer[numArr.length];
                                                    for (int i7 = 0; i7 < 12; i7++) {
                                                        if (i7 > parseInt3) {
                                                            numArr2[i7] = Integer.valueOf(numArr[i7].intValue() - parseInt);
                                                        } else {
                                                            numArr2[i7] = Integer.valueOf(numArr[i7].intValue());
                                                        }
                                                    }
                                                    f3113t.put(Integer.valueOf(parseInt2), numArr2);
                                                    Integer[] numArr3 = u.get(Integer.valueOf(parseInt2));
                                                    if (numArr3 == null) {
                                                        if (!d2) {
                                                            numArr3 = new Integer[f3104k.length];
                                                            int i8 = 0;
                                                            while (true) {
                                                                int[] iArr3 = f3104k;
                                                                if (i8 >= iArr3.length) {
                                                                    break;
                                                                }
                                                                numArr3[i8] = Integer.valueOf(iArr3[i8]);
                                                                i8++;
                                                            }
                                                        } else {
                                                            numArr3 = new Integer[f3105l.length];
                                                            int i9 = 0;
                                                            while (true) {
                                                                int[] iArr4 = f3105l;
                                                                if (i9 >= iArr4.length) {
                                                                    break;
                                                                }
                                                                numArr3[i9] = Integer.valueOf(iArr4[i9]);
                                                                i9++;
                                                            }
                                                        }
                                                    }
                                                    Integer[] numArr4 = new Integer[numArr3.length];
                                                    for (int i10 = 0; i10 < 12; i10++) {
                                                        if (i10 == parseInt3) {
                                                            numArr4[i10] = Integer.valueOf(numArr3[i10].intValue() - parseInt);
                                                        } else {
                                                            numArr4[i10] = Integer.valueOf(numArr3[i10].intValue());
                                                        }
                                                    }
                                                    u.put(Integer.valueOf(parseInt2), numArr4);
                                                    if (parseInt2 != parseInt4) {
                                                        int i11 = parseInt2 - 1;
                                                        int i12 = i11 / 30;
                                                        int i13 = i11 % 30;
                                                        Integer[] numArr5 = v.get(Integer.valueOf(i12));
                                                        if (numArr5 == null) {
                                                            int length = f3109p.length;
                                                            Integer[] numArr6 = new Integer[length];
                                                            for (int i14 = 0; i14 < length; i14++) {
                                                                numArr6[i14] = Integer.valueOf(f3109p[i14]);
                                                            }
                                                            numArr5 = numArr6;
                                                        }
                                                        for (int i15 = i13 + 1; i15 < f3109p.length; i15++) {
                                                            numArr5[i15] = Integer.valueOf(numArr5[i15].intValue() - parseInt);
                                                        }
                                                        v.put(Integer.valueOf(i12), numArr5);
                                                        int i16 = parseInt4 - 1;
                                                        int i17 = i16 / 30;
                                                        if (i12 != i17) {
                                                            int i18 = i12 + 1;
                                                            while (true) {
                                                                Long[] lArr = w;
                                                                if (i18 >= lArr.length) {
                                                                    break;
                                                                }
                                                                lArr[i18] = Long.valueOf(lArr[i18].longValue() - ((long) parseInt));
                                                                i18++;
                                                                stringTokenizer2 = stringTokenizer2;
                                                            }
                                                            StringTokenizer stringTokenizer3 = stringTokenizer2;
                                                            int i19 = i17 + 1;
                                                            while (true) {
                                                                Long[] lArr2 = w;
                                                                if (i19 >= lArr2.length) {
                                                                    break;
                                                                }
                                                                lArr2[i19] = Long.valueOf(lArr2[i19].longValue() + ((long) parseInt));
                                                                i19++;
                                                                stringTokenizer3 = stringTokenizer3;
                                                            }
                                                            stringTokenizer = stringTokenizer3;
                                                        } else {
                                                            stringTokenizer = stringTokenizer2;
                                                        }
                                                        int i20 = i16 % 30;
                                                        Integer[] numArr7 = v.get(Integer.valueOf(i17));
                                                        if (numArr7 == null) {
                                                            int length2 = f3109p.length;
                                                            Integer[] numArr8 = new Integer[length2];
                                                            for (int i21 = 0; i21 < length2; i21++) {
                                                                numArr8[i21] = Integer.valueOf(f3109p[i21]);
                                                            }
                                                            numArr7 = numArr8;
                                                        }
                                                        while (true) {
                                                            i20++;
                                                            if (i20 >= f3109p.length) {
                                                                break;
                                                            }
                                                            numArr7[i20] = Integer.valueOf(numArr7[i20].intValue() + parseInt);
                                                        }
                                                        v.put(Integer.valueOf(i17), numArr7);
                                                    } else {
                                                        stringTokenizer = stringTokenizer2;
                                                    }
                                                    boolean d3 = d((long) parseInt4);
                                                    Integer[] numArr9 = f3113t.get(Integer.valueOf(parseInt4));
                                                    if (numArr9 == null) {
                                                        if (!d3) {
                                                            numArr9 = new Integer[f3102i.length];
                                                            int i22 = 0;
                                                            while (true) {
                                                                int[] iArr5 = f3102i;
                                                                if (i22 >= iArr5.length) {
                                                                    break;
                                                                }
                                                                numArr9[i22] = Integer.valueOf(iArr5[i22]);
                                                                i22++;
                                                            }
                                                        } else {
                                                            numArr9 = new Integer[f3103j.length];
                                                            int i23 = 0;
                                                            while (true) {
                                                                int[] iArr6 = f3103j;
                                                                if (i23 >= iArr6.length) {
                                                                    break;
                                                                }
                                                                numArr9[i23] = Integer.valueOf(iArr6[i23]);
                                                                i23++;
                                                            }
                                                        }
                                                    }
                                                    Integer[] numArr10 = new Integer[numArr9.length];
                                                    for (int i24 = 0; i24 < 12; i24++) {
                                                        if (i24 > parseInt5) {
                                                            numArr10[i24] = Integer.valueOf(numArr9[i24].intValue() + parseInt);
                                                        } else {
                                                            numArr10[i24] = Integer.valueOf(numArr9[i24].intValue());
                                                        }
                                                    }
                                                    f3113t.put(Integer.valueOf(parseInt4), numArr10);
                                                    Integer[] numArr11 = u.get(Integer.valueOf(parseInt4));
                                                    if (numArr11 == null) {
                                                        if (!d3) {
                                                            numArr11 = new Integer[f3104k.length];
                                                            int i25 = 0;
                                                            while (true) {
                                                                int[] iArr7 = f3104k;
                                                                if (i25 >= iArr7.length) {
                                                                    break;
                                                                }
                                                                numArr11[i25] = Integer.valueOf(iArr7[i25]);
                                                                i25++;
                                                            }
                                                        } else {
                                                            numArr11 = new Integer[f3105l.length];
                                                            int i26 = 0;
                                                            while (true) {
                                                                int[] iArr8 = f3105l;
                                                                if (i26 >= iArr8.length) {
                                                                    break;
                                                                }
                                                                numArr11[i26] = Integer.valueOf(iArr8[i26]);
                                                                i26++;
                                                            }
                                                        }
                                                    }
                                                    Integer[] numArr12 = new Integer[numArr11.length];
                                                    for (int i27 = 0; i27 < 12; i27++) {
                                                        if (i27 == parseInt5) {
                                                            numArr12[i27] = Integer.valueOf(numArr11[i27].intValue() + parseInt);
                                                        } else {
                                                            numArr12[i27] = Integer.valueOf(numArr11[i27].intValue());
                                                        }
                                                    }
                                                    u.put(Integer.valueOf(parseInt4), numArr12);
                                                    Integer[] numArr13 = u.get(Integer.valueOf(parseInt2));
                                                    Integer[] numArr14 = u.get(Integer.valueOf(parseInt4));
                                                    int intValue = numArr13[parseInt3].intValue();
                                                    int intValue2 = numArr14[parseInt5].intValue();
                                                    int intValue3 = numArr13[11].intValue() + f3113t.get(Integer.valueOf(parseInt2))[11].intValue();
                                                    int intValue4 = numArr14[11].intValue() + f3113t.get(Integer.valueOf(parseInt4))[11].intValue();
                                                    int intValue5 = z[5].intValue();
                                                    int intValue6 = y[5].intValue();
                                                    if (intValue5 < intValue) {
                                                        intValue5 = intValue;
                                                    }
                                                    if (intValue5 < intValue2) {
                                                        intValue5 = intValue2;
                                                    }
                                                    z[5] = Integer.valueOf(intValue5);
                                                    if (intValue6 <= intValue) {
                                                        intValue = intValue6;
                                                    }
                                                    if (intValue <= intValue2) {
                                                        intValue2 = intValue;
                                                    }
                                                    y[5] = Integer.valueOf(intValue2);
                                                    int intValue7 = z[6].intValue();
                                                    int intValue8 = y[6].intValue();
                                                    if (intValue7 < intValue3) {
                                                        intValue7 = intValue3;
                                                    }
                                                    if (intValue7 < intValue4) {
                                                        intValue7 = intValue4;
                                                    }
                                                    z[6] = Integer.valueOf(intValue7);
                                                    if (intValue8 <= intValue3) {
                                                        intValue3 = intValue8;
                                                    }
                                                    if (intValue3 <= intValue4) {
                                                        intValue4 = intValue3;
                                                    }
                                                    y[6] = Integer.valueOf(intValue4);
                                                    stringTokenizer2 = stringTokenizer;
                                                    i3 = i2;
                                                } else {
                                                    throw new IllegalArgumentException("startYear == endYear && endMonth < startMonth");
                                                }
                                            } catch (NumberFormatException unused) {
                                                int i28 = i3;
                                                throw new ParseException(outline.b("End month is not properly set at line ", i28, "."), i28);
                                            }
                                        } catch (NumberFormatException unused2) {
                                            int i29 = i3;
                                            throw new ParseException(outline.b("End year is not properly set at line ", i29, "."), i29);
                                        }
                                    } else {
                                        int i30 = i3;
                                        throw new ParseException(outline.b("End year/month has incorrect format at line ", i30, "."), i30);
                                    }
                                } catch (NumberFormatException unused3) {
                                    int i31 = i3;
                                    throw new ParseException(outline.b("Start month is not properly set at line ", i31, "."), i31);
                                }
                            } catch (NumberFormatException unused4) {
                                int i32 = i3;
                                throw new ParseException(outline.b("Start year is not properly set at line ", i32, "."), i32);
                            }
                        } else {
                            int i33 = i3;
                            throw new ParseException(outline.b("Start year/month has incorrect format at line ", i33, "."), i33);
                        }
                    } else {
                        int i34 = i3;
                        throw new ParseException(outline.b("Start and end year/month has incorrect format at line ", i34, "."), i34);
                    }
                } catch (NumberFormatException unused5) {
                    int i35 = i3;
                    throw new ParseException(outline.b("Offset is not properly set at line ", i35, "."), i35);
                }
            } else {
                int i36 = i3;
                throw new ParseException(outline.b("Offset has incorrect format at line ", i36, "."), i36);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.HijrahDate] */
    public static ChronoLocalDate a(DataInput dataInput) {
        int readInt = dataInput.readInt();
        byte readByte = dataInput.readByte();
        byte readByte2 = dataInput.readByte();
        HijrahChronology hijrahChronology = HijrahChronology.d;
        return d(readInt, readByte, readByte2);
    }
}
