package q.b.a.s;

import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import org.threeten.bp.DateTimeException;
import q.b.a.LocalDate;
import q.b.a.u.DefaultInterfaceEra;
import q.b.a.v.ChronoField;
import q.b.a.v.TemporalField;
import q.b.a.v.ValueRange;

public final class JapaneseEra extends DefaultInterfaceEra implements Serializable {

    /* renamed from: e  reason: collision with root package name */
    public static final JapaneseEra f3119e = new JapaneseEra(-1, LocalDate.a(1868, 9, 8), "Meiji");

    /* renamed from: f  reason: collision with root package name */
    public static final JapaneseEra f3120f = new JapaneseEra(0, LocalDate.a(1912, 7, 30), "Taisho");
    public static final JapaneseEra g = new JapaneseEra(1, LocalDate.a(1926, 12, 25), "Showa");
    public static final JapaneseEra h;

    /* renamed from: i  reason: collision with root package name */
    public static final AtomicReference<q[]> f3121i;
    public final int b;
    public final transient LocalDate c;
    public final transient String d;

    static {
        JapaneseEra japaneseEra = new JapaneseEra(2, LocalDate.a(1989, 1, 8), "Heisei");
        h = japaneseEra;
        f3121i = new AtomicReference<>(new JapaneseEra[]{f3119e, f3120f, g, japaneseEra});
    }

    public JapaneseEra(int i2, LocalDate localDate, String str) {
        this.b = i2;
        this.c = localDate;
        this.d = str;
    }

    public static JapaneseEra a(int i2) {
        JapaneseEra[] japaneseEraArr = f3121i.get();
        if (i2 >= f3119e.b && i2 <= japaneseEraArr[japaneseEraArr.length - 1].b) {
            return japaneseEraArr[i2 + 1];
        }
        throw new DateTimeException("japaneseEra is invalid");
    }

    public static JapaneseEra[] j() {
        JapaneseEra[] japaneseEraArr = f3121i.get();
        return (JapaneseEra[]) Arrays.copyOf(japaneseEraArr, japaneseEraArr.length);
    }

    private Object readResolve() {
        try {
            return a(this.b);
        } catch (DateTimeException e2) {
            InvalidObjectException invalidObjectException = new InvalidObjectException("Invalid era");
            invalidObjectException.initCause(e2);
            throw invalidObjectException;
        }
    }

    private Object writeReplace() {
        return new Ser((byte) 2, this);
    }

    public int getValue() {
        return this.b;
    }

    public LocalDate i() {
        int i2 = this.b + 1;
        JapaneseEra[] j2 = j();
        if (i2 >= j2.length - 1) {
            return LocalDate.f3089f;
        }
        return j2[i2 + 1].c.a(-1L);
    }

    public String toString() {
        return this.d;
    }

    public static JapaneseEra a(LocalDate localDate) {
        if (!localDate.b(f3119e.c)) {
            JapaneseEra[] japaneseEraArr = f3121i.get();
            for (int length = japaneseEraArr.length - 1; length >= 0; length--) {
                JapaneseEra japaneseEra = japaneseEraArr[length];
                if (localDate.compareTo((ChronoLocalDate) japaneseEra.c) >= 0) {
                    return japaneseEra;
                }
            }
            return null;
        }
        throw new DateTimeException("Date too early: " + localDate);
    }

    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return JapaneseChronology.f3116e.a(ChronoField.ERA);
        }
        return super.a(temporalField);
    }

    public static JapaneseEra a(DataInput dataInput) {
        return a(dataInput.readByte());
    }
}
