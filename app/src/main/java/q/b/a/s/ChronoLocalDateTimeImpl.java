package q.b.a.s;

import com.crashlytics.android.answers.RetryManager;
import java.io.Serializable;
import n.i.Collections;
import q.b.a.LocalTime;
import q.b.a.o;
import q.b.a.p;
import q.b.a.s.b;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.TemporalField;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;
import q.b.a.v.j;
import q.b.a.v.m;

public final class ChronoLocalDateTimeImpl<D extends b> extends ChronoLocalDateTime<D> implements d, f, Serializable {
    public final D b;
    public final LocalTime c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [D, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.f, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public ChronoLocalDateTimeImpl(D d, q.b.a.f fVar) {
        Collections.a((Object) d, "date");
        Collections.a((Object) fVar, "time");
        this.b = d;
        this.c = fVar;
    }

    private Object writeReplace() {
        return new Ser((byte) 12, this);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.TemporalAccessor] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField.f() || temporalField.h()) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.TemporalAccessor] */
    public long d(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h() ? this.c.d(temporalField) : this.b.d(temporalField);
        }
        return temporalField.b(this);
    }

    public D j() {
        return this.b;
    }

    public LocalTime l() {
        return this.c;
    }

    public int b(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h() ? this.c.b(temporalField) : this.b.b(temporalField);
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    public final ChronoLocalDateTimeImpl<D> a(d dVar, q.b.a.f fVar) {
        if (this.b == dVar && this.c == fVar) {
            return this;
        }
        return new ChronoLocalDateTimeImpl<>(this.b.i().a(dVar), fVar);
    }

    /* JADX WARN: Type inference failed for: r0v21, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [?, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    public ChronoLocalDateTimeImpl<D> b(long j2, m mVar) {
        long j3 = j2;
        m mVar2 = mVar;
        if (!(mVar2 instanceof ChronoUnit)) {
            return this.b.i().b((d) mVar2.a(this, j3));
        }
        switch (((ChronoUnit) mVar2).ordinal()) {
            case 0:
                return b(j2);
            case 1:
                return a(j3 / 86400000000L).b((j3 % 86400000000L) * 1000);
            case 2:
                return a(j3 / 86400000).b((j3 % 86400000) * RetryManager.NANOSECONDS_IN_MS);
            case 3:
                return a(this.b, 0, 0, j2, 0);
            case 4:
                return a(this.b, 0, j2, 0, 0);
            case 5:
                return a(this.b, j2, 0, 0, 0);
            case 6:
                ChronoLocalDateTimeImpl a = a(j3 / 256);
                return a.a(a.b, (j3 % 256) * 12, 0, 0, 0);
            default:
                return a((d) this.b.b(j3, mVar2), (q.b.a.f) this.c);
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.TemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h() ? this.c.a(temporalField) : this.b.a(temporalField);
        }
        return temporalField.c(this);
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r2v4, types: [q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r2v8, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [?, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [D, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    public ChronoLocalDateTimeImpl<D> a(f fVar) {
        if (fVar instanceof ChronoLocalDate) {
            return a((d) ((ChronoLocalDate) fVar), (q.b.a.f) this.c);
        }
        if (fVar instanceof LocalTime) {
            return a((d) this.b, (q.b.a.f) ((LocalTime) fVar));
        }
        if (fVar instanceof ChronoLocalDateTimeImpl) {
            return this.b.i().b((d) ((ChronoLocalDateTimeImpl) fVar));
        }
        return this.b.i().b((d) ((ChronoLocalDateTimeImpl) fVar.a(this)));
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [D, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalTime
     arg types: [q.b.a.v.j, long]
     candidates:
      q.b.a.LocalTime.a(int, int):q.b.a.LocalTime
      q.b.a.LocalTime.a(long, int):q.b.a.LocalTime
      q.b.a.LocalTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [?, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    public ChronoLocalDateTimeImpl<D> a(j jVar, long j2) {
        if (!(jVar instanceof ChronoField)) {
            return this.b.i().b((d) jVar.a(this, j2));
        }
        if (jVar.h()) {
            return a((d) this.b, (q.b.a.f) this.c.a((TemporalField) jVar, j2));
        }
        return a((d) this.b.a(jVar, j2), (q.b.a.f) this.c);
    }

    public final ChronoLocalDateTimeImpl<D> b(long j2) {
        return a(this.b, 0, 0, 0, j2);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r3v1, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [?, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    public final ChronoLocalDateTimeImpl<D> a(long j2) {
        return a((d) this.b.b(j2, ChronoUnit.DAYS), (q.b.a.f) this.c);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.s.ChronoLocalDate, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [D, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D>
     arg types: [?, q.b.a.LocalTime]
     candidates:
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTimeImpl<D>
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTimeImpl.a(q.b.a.v.d, q.b.a.f):q.b.a.s.ChronoLocalDateTimeImpl<D> */
    public final ChronoLocalDateTimeImpl<D> a(D d, long j2, long j3, long j4, long j5) {
        D d2 = d;
        if ((j2 | j3 | j4 | j5) == 0) {
            return a((d) d2, (q.b.a.f) this.c);
        }
        long j6 = j2 / 24;
        long j7 = ((j2 % 24) * 3600000000000L) + ((j3 % 1440) * 60000000000L) + ((j4 % 86400) * 1000000000) + (j5 % 86400000000000L);
        long i2 = this.c.i();
        long j8 = j7 + i2;
        long b2 = Collections.b(j8, 86400000000000L) + j6 + (j3 / 1440) + (j4 / 86400) + (j5 / 86400000000000L);
        long c2 = Collections.c(j8, 86400000000000L);
        return a((d) d2.b(b2, ChronoUnit.DAYS), (q.b.a.f) (c2 == i2 ? this.c : LocalTime.e(c2)));
    }

    public ChronoZonedDateTime<D> a(o oVar) {
        return ChronoZonedDateTimeImpl.a(this, oVar, (p) null);
    }
}
