package q.b.a.w;

import j.a.a.a.outline;
import java.io.Serializable;
import n.i.Collections;
import q.b.a.Instant;
import q.b.a.LocalDateTime;
import q.b.a.ZoneOffset;

public final class ZoneOffsetTransition implements Comparable<d>, Serializable {
    public final LocalDateTime b;
    public final ZoneOffset c;
    public final ZoneOffset d;

    public ZoneOffsetTransition(LocalDateTime localDateTime, ZoneOffset zoneOffset, ZoneOffset zoneOffset2) {
        this.b = localDateTime;
        this.c = zoneOffset;
        this.d = zoneOffset2;
    }

    private Object writeReplace() {
        return new Ser((byte) 2, this);
    }

    public int compareTo(Object obj) {
        Instant g = g();
        Instant g2 = ((ZoneOffsetTransition) obj).g();
        int a = Collections.a(g.b, g2.b);
        return a != 0 ? a : g.c - g2.c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ZoneOffsetTransition)) {
            return false;
        }
        ZoneOffsetTransition zoneOffsetTransition = (ZoneOffsetTransition) obj;
        if (!this.b.equals(zoneOffsetTransition.b) || !this.c.equals(zoneOffsetTransition.c) || !this.d.equals(zoneOffsetTransition.d)) {
            return false;
        }
        return true;
    }

    public LocalDateTime f() {
        return this.b.c((long) (this.d.c - this.c.c));
    }

    public Instant g() {
        LocalDateTime localDateTime = this.b;
        return Instant.b(localDateTime.a(this.c), (long) localDateTime.c.f3094e);
    }

    public boolean h() {
        return this.d.c > this.c.c;
    }

    public int hashCode() {
        return (this.b.hashCode() ^ this.c.c) ^ Integer.rotateLeft(this.d.c, 16);
    }

    public String toString() {
        StringBuilder a = outline.a("Transition[");
        a.append(h() ? "Gap" : "Overlap");
        a.append(" at ");
        a.append(this.b);
        a.append(this.c);
        a.append(" to ");
        a.append(this.d);
        a.append(']');
        return a.toString();
    }

    public ZoneOffsetTransition(long j2, ZoneOffset zoneOffset, ZoneOffset zoneOffset2) {
        this.b = LocalDateTime.a(j2, 0, zoneOffset);
        this.c = zoneOffset;
        this.d = zoneOffset2;
    }
}
