package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.t.DateTimeFormatterBuilder;
import q.b.a.t.SignStyle;
import q.b.a.u.c;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class Year extends c implements d, f, Comparable<m>, Serializable {
    public final int b;

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    static {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD);
        dateTimeFormatterBuilder.c();
    }

    public Year(int i2) {
        this.b = i2;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 67, this);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.Year] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.YEAR || temporalField == ChronoField.YEAR_OF_ERA || temporalField == ChronoField.ERA) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public int compareTo(Object obj) {
        return this.b - ((Year) obj).b;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.Year] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int i2 = 1;
        switch (((ChronoField) temporalField).ordinal()) {
            case 25:
                int i3 = this.b;
                if (i3 < 1) {
                    i3 = 1 - i3;
                }
                return (long) i3;
            case 26:
                return (long) this.b;
            case 27:
                if (this.b < 1) {
                    i2 = 0;
                }
                return (long) i2;
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Year) || this.b != ((Year) obj).b) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.b;
    }

    public String toString() {
        return Integer.toString(this.b);
    }

    public static boolean b(long j2) {
        return (3 & j2) == 0 && (j2 % 100 != 0 || j2 % 400 == 0);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.Year, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return IsoChronology.d;
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.YEARS;
        }
        if (temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return Year.super.a(temporalQuery);
    }

    public int b(TemporalField temporalField) {
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r5v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Year.a(q.b.a.v.TemporalField, long):q.b.a.Year
     arg types: [?, long]
     candidates:
      q.b.a.Year.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Year.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.Year.a(q.b.a.v.TemporalField, long):q.b.a.Year */
    public Year b(long j2, TemporalUnit temporalUnit) {
        if (!(temporalUnit instanceof ChronoUnit)) {
            return (Year) temporalUnit.a(this, j2);
        }
        switch (((ChronoUnit) temporalUnit).ordinal()) {
            case 10:
                return a(j2);
            case 11:
                return a(Collections.b(j2, 10));
            case 12:
                return a(Collections.b(j2, 100));
            case 13:
                return a(Collections.b(j2, (int) AnswersRetryFilesSender.BACKOFF_MS));
            case 14:
                ? r5 = ChronoField.ERA;
                return a((TemporalField) r5, Collections.d(d(r5), j2));
            default:
                throw new UnsupportedTemporalTypeException("Unsupported unit: " + temporalUnit);
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static Year a(int i2) {
        ? r0 = ChronoField.YEAR;
        r0.range.b((long) i2, r0);
        return new Year(i2);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.Year, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField != ChronoField.YEAR_OF_ERA) {
            return Year.super.a(temporalField);
        }
        return ValueRange.a(1, this.b <= 0 ? 1000000000 : 999999999);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.Year, q.b.a.v.Temporal] */
    public Temporal a(TemporalAdjuster temporalAdjuster) {
        return (Year) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Year a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (Year) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        r0.range.b(j2, r0);
        switch (r0.ordinal()) {
            case 25:
                if (this.b < 1) {
                    j2 = 1 - j2;
                }
                return a((int) j2);
            case 26:
                return a((int) j2);
            case 27:
                return d(ChronoField.ERA) == j2 ? this : a(1 - this.b);
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    public Year a(long j2) {
        return j2 == 0 ? this : a(ChronoField.YEAR.a(((long) this.b) + j2));
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.Year, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.Year, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.Year
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.Year */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.Year
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.Year */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.Year
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.Year.b(long, q.b.a.v.TemporalUnit):q.b.a.Year */
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        if (Chronology.c(temporal).equals(IsoChronology.d)) {
            return temporal.a((TemporalField) ChronoField.YEAR, (long) this.b);
        }
        throw new DateTimeException("Adjustment only supported on ISO date-time");
    }

    public static Year a(DataInput dataInput) {
        return a(dataInput.readInt());
    }
}
