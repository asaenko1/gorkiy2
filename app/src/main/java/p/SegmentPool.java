package p;

import n.n.c.Intrinsics;

/* compiled from: SegmentPool.kt */
public final class SegmentPool {
    public static Segment a;
    public static long b;
    public static final SegmentPool c = new SegmentPool();

    public static final Segment a() {
        synchronized (c) {
            Segment segment = a;
            if (segment == null) {
                return new Segment();
            }
            a = segment.f3071f;
            segment.f3071f = null;
            b -= (long) 8192;
            return segment;
        }
    }

    public static final void a(Segment segment) {
        if (segment != null) {
            if (!(segment.f3071f == null && segment.g == null)) {
                throw new IllegalArgumentException("Failed requirement.".toString());
            } else if (!segment.d) {
                synchronized (c) {
                    long j2 = (long) 8192;
                    if (b + j2 <= 65536) {
                        b += j2;
                        segment.f3071f = a;
                        segment.c = 0;
                        segment.b = 0;
                        a = segment;
                    }
                }
            }
        } else {
            Intrinsics.a("segment");
            throw null;
        }
    }
}
