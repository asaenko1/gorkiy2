package p;

import j.a.a.a.outline;
import java.io.OutputStream;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: Okio.kt */
public final class Okio1 implements Sink {
    public final OutputStream b;
    public final Timeout c;

    public Okio1(OutputStream outputStream, Timeout timeout) {
        if (outputStream == null) {
            Intrinsics.a("out");
            throw null;
        } else if (timeout != null) {
            this.b = outputStream;
            this.c = timeout;
        } else {
            Intrinsics.a("timeout");
            throw null;
        }
    }

    public void a(Buffer buffer, long j2) {
        if (buffer != null) {
            Collections.a(buffer.c, 0, j2);
            while (j2 > 0) {
                this.c.e();
                Segment segment = buffer.b;
                if (segment != null) {
                    int min = (int) Math.min(j2, (long) (segment.c - segment.b));
                    this.b.write(segment.a, segment.b, min);
                    int i2 = segment.b + min;
                    segment.b = i2;
                    long j3 = (long) min;
                    j2 -= j3;
                    buffer.c -= j3;
                    if (i2 == segment.c) {
                        buffer.b = segment.a();
                        SegmentPool.a(segment);
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            return;
        }
        Intrinsics.a("source");
        throw null;
    }

    public Timeout b() {
        return this.c;
    }

    public void close() {
        this.b.close();
    }

    public void flush() {
        this.b.flush();
    }

    public String toString() {
        StringBuilder a = outline.a("sink(");
        a.append(this.b);
        a.append(')');
        return a.toString();
    }
}
