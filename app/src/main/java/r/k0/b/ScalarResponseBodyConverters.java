package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

public final class ScalarResponseBodyConverters implements Converter<i0, Boolean> {
    public static final ScalarResponseBodyConverters a = new ScalarResponseBodyConverters();

    public Object a(Object obj) {
        return Boolean.valueOf(((ResponseBody) obj).h());
    }
}
