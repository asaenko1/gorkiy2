package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters3 implements Converter<i0, Float> {
    public static final ScalarResponseBodyConverters3 a = new ScalarResponseBodyConverters3();

    public Object a(Object obj) {
        return Float.valueOf(((ResponseBody) obj).h());
    }
}
