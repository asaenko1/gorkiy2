package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters4 implements Converter<i0, Integer> {
    public static final ScalarResponseBodyConverters4 a = new ScalarResponseBodyConverters4();

    public Object a(Object obj) {
        return Integer.valueOf(((ResponseBody) obj).h());
    }
}
