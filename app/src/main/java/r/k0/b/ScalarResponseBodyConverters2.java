package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters2 implements Converter<i0, Double> {
    public static final ScalarResponseBodyConverters2 a = new ScalarResponseBodyConverters2();

    public Object a(Object obj) {
        return Double.valueOf(((ResponseBody) obj).h());
    }
}
