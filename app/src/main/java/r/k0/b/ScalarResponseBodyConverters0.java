package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters0 implements Converter<i0, Byte> {
    public static final ScalarResponseBodyConverters0 a = new ScalarResponseBodyConverters0();

    public Object a(Object obj) {
        return Byte.valueOf(((ResponseBody) obj).h());
    }
}
