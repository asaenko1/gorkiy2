package r.k0.a;

import j.c.d.Gson;
import j.c.d.c0.TypeToken;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import o.g0;
import o.i0;
import r.Converter;
import r.e0;

public final class GsonConverterFactory extends Converter.a {
    public final Gson a;

    public GsonConverterFactory(Gson gson) {
        this.a = gson;
    }

    public Converter<i0, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        return new GsonResponseBodyConverter(this.a, this.a.a(new TypeToken(type)));
    }

    public Converter<?, g0> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, e0 e0Var) {
        return new GsonRequestBodyConverter(this.a, this.a.a(new TypeToken(type)));
    }
}
