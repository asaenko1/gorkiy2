package r.j0.a;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import r.Call;
import r.Response;

public final class CallExecuteObservable<T> extends Observable<Response<T>> {
    public final Call<T> b;

    public static final class a implements Disposable {
        public final Call<?> b;
        public volatile boolean c;

        public a(Call<?> call) {
            this.b = call;
        }

        public void f() {
            this.c = true;
            this.b.cancel();
        }

        public boolean g() {
            return this.c;
        }
    }

    public CallExecuteObservable(Call<T> call) {
        this.b = call;
    }

    public void b(Observer<? super Response<T>> observer) {
        boolean z;
        Call<T> clone = this.b.clone();
        a aVar = new a(clone);
        observer.a(aVar);
        if (!aVar.c) {
            try {
                Response<T> g = clone.g();
                if (!aVar.c) {
                    observer.b(g);
                }
                if (!aVar.c) {
                    try {
                        observer.a();
                    } catch (Throwable th) {
                        th = th;
                        z = true;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                z = false;
                c.c(th);
                if (z) {
                    c.b(th);
                } else if (!aVar.c) {
                    try {
                        observer.a(th);
                    } catch (Throwable th3) {
                        c.c(th3);
                        c.b((Throwable) new CompositeException(th, th3));
                    }
                }
            }
        }
    }
}
