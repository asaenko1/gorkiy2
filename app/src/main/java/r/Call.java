package r;

import o.Request;

public interface Call<T> extends Cloneable {
    void a(Callback<T> callback);

    void cancel();

    Call<T> clone();

    Request f();

    Response<T> g();

    boolean h();
}
