package r;

import n.l.Continuation;
import n.l.e.a.ContinuationImpl1;
import n.l.e.a.DebugMetadata;

@DebugMetadata(c = "retrofit2/KotlinExtensions", f = "KotlinExtensions.kt", l = {100, 102}, m = "yieldAndThrow")
/* compiled from: KotlinExtensions.kt */
public final class KotlinExtensions5 extends ContinuationImpl1 {

    /* renamed from: e  reason: collision with root package name */
    public /* synthetic */ Object f3167e;

    /* renamed from: f  reason: collision with root package name */
    public int f3168f;
    public Object g;

    public KotlinExtensions5(Continuation continuation) {
        super(continuation);
    }
}
