package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import i.n.a.WakefulBroadcastReceiver;
import j.c.a.a.c.n.c;
import j.c.c.g.f0;
import j.c.c.g.w;
import javax.annotation.concurrent.GuardedBy;

public final class FirebaseInstanceIdReceiver extends WakefulBroadcastReceiver {
    @GuardedBy("FirebaseInstanceIdReceiver.class")
    public static f0 c;

    public static int a(BroadcastReceiver broadcastReceiver, Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Binding to service");
        }
        if (broadcastReceiver.isOrderedBroadcast()) {
            broadcastReceiver.setResultCode(-1);
        }
        a(context, "com.google.firebase.MESSAGING_EVENT").a(intent, broadcastReceiver.goAsync());
        return -1;
    }

    public final void c(Context context, Intent intent) {
        ComponentName componentName;
        intent.setComponent(null);
        intent.setPackage(context.getPackageName());
        int i2 = -1;
        if ("google.com/iid".equals(intent.getStringExtra("from"))) {
            String stringExtra = intent.getStringExtra("CMD");
            if (stringExtra != null) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(intent.getExtras());
                    StringBuilder sb = new StringBuilder(valueOf.length() + stringExtra.length() + 21);
                    sb.append("Received command: ");
                    sb.append(stringExtra);
                    sb.append(" - ");
                    sb.append(valueOf);
                    Log.d("FirebaseInstanceId", sb.toString());
                }
                if ("RST".equals(stringExtra) || "RST_FULL".equals(stringExtra)) {
                    FirebaseInstanceId.f().e();
                } else if ("SYNC".equals(stringExtra)) {
                    FirebaseInstanceId f2 = FirebaseInstanceId.f();
                    if (f2 != null) {
                        FirebaseInstanceId.f556j.c("");
                        f2.a();
                    } else {
                        throw null;
                    }
                }
            }
        } else {
            String stringExtra2 = intent.getStringExtra("gcm.rawData64");
            boolean z = false;
            if (stringExtra2 != null) {
                intent.putExtra("rawData", Base64.decode(stringExtra2, 0));
                intent.removeExtra("gcm.rawData64");
            }
            boolean z2 = c.e() && context.getApplicationInfo().targetSdkVersion >= 26;
            if ((intent.getFlags() & 268435456) != 0) {
                z = true;
            }
            if (!z2 || z) {
                w a = w.a();
                if (a != null) {
                    if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        Log.d("FirebaseInstanceId", "Starting service");
                    }
                    a.d.offer(intent);
                    Intent intent2 = new Intent("com.google.firebase.MESSAGING_EVENT");
                    intent2.setPackage(context.getPackageName());
                    String a2 = a.a(context, intent2);
                    if (a2 != null) {
                        if (Log.isLoggable("FirebaseInstanceId", 3)) {
                            Log.d("FirebaseInstanceId", a2.length() != 0 ? "Restricting intent to a specific service: ".concat(a2) : new String("Restricting intent to a specific service: "));
                        }
                        intent2.setClassName(context.getPackageName(), a2);
                    }
                    try {
                        if (a.a(context)) {
                            componentName = WakefulBroadcastReceiver.b(context, intent2);
                        } else {
                            componentName = context.startService(intent2);
                            Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
                        }
                        if (componentName == null) {
                            Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
                            i2 = 404;
                        }
                    } catch (SecurityException e2) {
                        Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e2);
                        i2 = 401;
                    } catch (IllegalStateException e3) {
                        String valueOf2 = String.valueOf(e3);
                        StringBuilder sb2 = new StringBuilder(valueOf2.length() + 45);
                        sb2.append("Failed to start service while in background: ");
                        sb2.append(valueOf2);
                        Log.e("FirebaseInstanceId", sb2.toString());
                        i2 = 402;
                    }
                    if (c.e() && i2 == 402) {
                        a(this, context, intent);
                        i2 = 403;
                    }
                } else {
                    throw null;
                }
            } else {
                a(this, context, intent);
            }
        }
        if (isOrderedBroadcast()) {
            setResultCode(i2);
        }
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
            Intent intent2 = parcelableExtra instanceof Intent ? (Intent) parcelableExtra : null;
            if (intent2 != null) {
                c(context, intent2);
            } else {
                c(context, intent);
            }
        }
    }

    public static synchronized f0 a(Context context, String str) {
        f0 f0Var;
        synchronized (FirebaseInstanceIdReceiver.class) {
            if (c == null) {
                c = new f0(context, str);
            }
            f0Var = c;
        }
        return f0Var;
    }
}
