package com.gun0912.tedpermission;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import androidx.appcompat.app.AlertController;
import i.b.k.AlertDialog;
import i.b.k.AppCompatActivity;
import i.h.d.ActivityCompat;
import i.h.e.ContextCompat;
import j.d.a.PermissionListener;
import j.d.a.TedPermission;
import j.d.a.TedPermissionActivity0;
import j.d.a.TedPermissionActivity1;
import j.d.a.TedPermissionActivity2;
import j.d.a.TedPermissionActivity3;
import j.d.a.TedPermissionActivity4;
import j.d.a.a;
import j.d.a.b;
import j.d.a.c;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;

public class TedPermissionActivity extends AppCompatActivity {
    public static Deque<a> C;
    public boolean A;
    public int B;

    /* renamed from: q  reason: collision with root package name */
    public CharSequence f562q;

    /* renamed from: r  reason: collision with root package name */
    public CharSequence f563r;

    /* renamed from: s  reason: collision with root package name */
    public CharSequence f564s;

    /* renamed from: t  reason: collision with root package name */
    public CharSequence f565t;
    public String[] u;
    public String v;
    public boolean w;
    public String x;
    public String y;
    public String z;

    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @TargetApi(23)
    public final boolean o() {
        return Settings.canDrawOverlays(getApplicationContext());
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 30) {
            if (i2 == 31) {
                a(false);
            } else if (i2 != 2000) {
                super.onActivityResult(i2, i3, intent);
            } else {
                a(true);
            }
        } else if (o() || TextUtils.isEmpty(this.f565t)) {
            a(false);
        } else {
            AlertDialog.a aVar = new AlertDialog.a(this, c.Theme_AppCompat_Light_Dialog_Alert);
            CharSequence charSequence = this.f565t;
            AlertController.b bVar = aVar.a;
            bVar.h = charSequence;
            bVar.f24m = false;
            aVar.a(this.y, new TedPermissionActivity3(this));
            if (this.w) {
                if (TextUtils.isEmpty(this.x)) {
                    this.x = getString(b.tedpermission_setting);
                }
                aVar.b(this.x, new TedPermissionActivity4(this));
            }
            aVar.b();
        }
    }

    public void onCreate(Bundle bundle) {
        boolean z2;
        overridePendingTransition(0, 0);
        super.onCreate(bundle);
        getWindow().addFlags(16);
        if (bundle != null) {
            this.u = bundle.getStringArray("permissions");
            this.f562q = bundle.getCharSequence("rationale_title");
            this.f563r = bundle.getCharSequence("rationale_message");
            this.f564s = bundle.getCharSequence("deny_title");
            this.f565t = bundle.getCharSequence("deny_message");
            this.v = bundle.getString("package_name");
            this.w = bundle.getBoolean("setting_button", true);
            this.z = bundle.getString("rationale_confirm_text");
            this.y = bundle.getString("denied_dialog_close_text");
            this.x = bundle.getString("setting_button_text");
            this.B = bundle.getInt("screen_orientation", -1);
        } else {
            Intent intent = getIntent();
            this.u = intent.getStringArrayExtra("permissions");
            this.f562q = intent.getCharSequenceExtra("rationale_title");
            this.f563r = intent.getCharSequenceExtra("rationale_message");
            this.f564s = intent.getCharSequenceExtra("deny_title");
            this.f565t = intent.getCharSequenceExtra("deny_message");
            this.v = intent.getStringExtra("package_name");
            this.w = intent.getBooleanExtra("setting_button", true);
            this.z = intent.getStringExtra("rationale_confirm_text");
            this.y = intent.getStringExtra("denied_dialog_close_text");
            this.x = intent.getStringExtra("setting_button_text");
            this.B = intent.getIntExtra("screen_orientation", -1);
        }
        String[] strArr = this.u;
        int length = strArr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                z2 = false;
                break;
            } else if (strArr[i2].equals("android.permission.SYSTEM_ALERT_WINDOW")) {
                z2 = !o();
                break;
            } else {
                i2++;
            }
        }
        if (z2) {
            Intent intent2 = new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.fromParts("package", this.v, null));
            if (!TextUtils.isEmpty(this.f563r)) {
                AlertDialog.a aVar = new AlertDialog.a(this, c.Theme_AppCompat_Light_Dialog_Alert);
                CharSequence charSequence = this.f563r;
                AlertController.b bVar = aVar.a;
                bVar.h = charSequence;
                bVar.f24m = false;
                aVar.a(this.z, new j.d.a.TedPermissionActivity(this, intent2));
                aVar.b();
                this.A = true;
            } else {
                startActivityForResult(intent2, 30);
            }
        } else {
            a(false);
        }
        setRequestedOrientation(this.B);
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (!(ContextCompat.a(this, str) == 0)) {
                arrayList.add(str);
            }
        }
        if (arrayList.isEmpty()) {
            a((List<String>) null);
        } else if (TextUtils.isEmpty(this.f565t)) {
            a(arrayList);
        } else {
            AlertDialog.a aVar = new AlertDialog.a(this, c.Theme_AppCompat_Light_Dialog_Alert);
            CharSequence charSequence = this.f564s;
            AlertController.b bVar = aVar.a;
            bVar.f19f = charSequence;
            bVar.h = this.f565t;
            bVar.f24m = false;
            aVar.a(this.y, new TedPermissionActivity1(this, arrayList));
            if (this.w) {
                if (TextUtils.isEmpty(this.x)) {
                    this.x = getString(b.tedpermission_setting);
                }
                aVar.b(this.x, new TedPermissionActivity2(this));
            }
            aVar.b();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putStringArray("permissions", this.u);
        bundle.putCharSequence("rationale_title", this.f562q);
        bundle.putCharSequence("rationale_message", this.f563r);
        bundle.putCharSequence("deny_title", this.f564s);
        bundle.putCharSequence("deny_message", this.f565t);
        bundle.putString("package_name", this.v);
        bundle.putBoolean("setting_button", this.w);
        bundle.putString("denied_dialog_close_text", this.y);
        bundle.putString("rationale_confirm_text", this.z);
        bundle.putString("setting_button_text", this.x);
        super.onSaveInstanceState(bundle);
    }

    public final void a(boolean z2) {
        ArrayList arrayList = new ArrayList();
        for (String str : this.u) {
            if (!str.equals("android.permission.SYSTEM_ALERT_WINDOW")) {
                if (!(ContextCompat.a(this, str) == 0)) {
                    arrayList.add(str);
                }
            } else if (!o()) {
                arrayList.add(str);
            }
        }
        if (arrayList.isEmpty()) {
            a((List<String>) null);
        } else if (z2) {
            a(arrayList);
        } else if (arrayList.size() == 1 && arrayList.contains("android.permission.SYSTEM_ALERT_WINDOW")) {
            a(arrayList);
        } else if (this.A || TextUtils.isEmpty(this.f563r)) {
            ActivityCompat.a(this, (String[]) arrayList.toArray(new String[arrayList.size()]), 10);
        } else {
            AlertDialog.a aVar = new AlertDialog.a(this, c.Theme_AppCompat_Light_Dialog_Alert);
            CharSequence charSequence = this.f562q;
            AlertController.b bVar = aVar.a;
            bVar.f19f = charSequence;
            bVar.h = this.f563r;
            bVar.f24m = false;
            aVar.a(this.z, new TedPermissionActivity0(this, arrayList));
            aVar.b();
            this.A = true;
        }
    }

    public final void a(List<String> list) {
        String str = TedPermission.a;
        Log.v(str, "permissionResult(): " + list);
        finish();
        overridePendingTransition(0, 0);
        Deque<a> deque = C;
        if (deque != null) {
            PermissionListener pop = deque.pop();
            boolean z2 = true;
            if (list != null && (!(list instanceof String) || ((String) list).trim().length() != 0)) {
                z2 = list instanceof Map ? ((Map) list).isEmpty() : list.isEmpty();
            }
            if (z2) {
                pop.a();
            } else {
                pop.a(list);
            }
            if (C.size() == 0) {
                C = null;
            }
        }
    }
}
