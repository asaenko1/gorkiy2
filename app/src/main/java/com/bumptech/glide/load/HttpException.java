package com.bumptech.glide.load;

import j.a.a.a.outline;
import java.io.IOException;

public final class HttpException extends IOException {
    public HttpException(int i2) {
        super(outline.b("Http request failed with status code: ", i2), null);
    }

    public HttpException(String str) {
        super(str, null);
    }

    public HttpException(String str, int i2) {
        super(str, null);
    }
}
