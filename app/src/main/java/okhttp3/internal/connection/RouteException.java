package okhttp3.internal.connection;

import java.io.IOException;
import n.n.c.Intrinsics;

/* compiled from: RouteException.kt */
public final class RouteException extends RuntimeException {
    public IOException b;
    public final IOException c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RouteException(IOException iOException) {
        super(iOException);
        if (iOException != null) {
            this.c = iOException;
            this.b = iOException;
            return;
        }
        Intrinsics.a("firstConnectException");
        throw null;
    }
}
