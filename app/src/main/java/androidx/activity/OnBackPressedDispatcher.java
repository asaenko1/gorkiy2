package androidx.activity;

import i.a.Cancellable;
import i.a.OnBackPressedCallback;
import i.a.b;
import i.l.a.FragmentManagerImpl;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;
import java.util.ArrayDeque;
import java.util.Iterator;

public final class OnBackPressedDispatcher {
    public final Runnable a;
    public final ArrayDeque<b> b = new ArrayDeque<>();

    public class LifecycleOnBackPressedCancellable implements LifecycleEventObserver, Cancellable {
        public final Lifecycle a;
        public final OnBackPressedCallback b;
        public Cancellable c;

        public LifecycleOnBackPressedCancellable(Lifecycle lifecycle, OnBackPressedCallback onBackPressedCallback) {
            this.a = lifecycle;
            this.b = onBackPressedCallback;
            lifecycle.a(this);
        }

        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
            if (aVar == Lifecycle.a.ON_START) {
                OnBackPressedDispatcher onBackPressedDispatcher = OnBackPressedDispatcher.this;
                OnBackPressedCallback onBackPressedCallback = this.b;
                onBackPressedDispatcher.b.add(onBackPressedCallback);
                a aVar2 = new a(onBackPressedCallback);
                onBackPressedCallback.b.add(aVar2);
                this.c = aVar2;
            } else if (aVar == Lifecycle.a.ON_STOP) {
                Cancellable cancellable = this.c;
                if (cancellable != null) {
                    cancellable.cancel();
                }
            } else if (aVar == Lifecycle.a.ON_DESTROY) {
                cancel();
            }
        }

        public void cancel() {
            ((LifecycleRegistry) this.a).a.remove(this);
            this.b.b.remove(this);
            Cancellable cancellable = this.c;
            if (cancellable != null) {
                cancellable.cancel();
                this.c = null;
            }
        }
    }

    public class a implements Cancellable {
        public final OnBackPressedCallback a;

        public a(OnBackPressedCallback onBackPressedCallback) {
            this.a = onBackPressedCallback;
        }

        public void cancel() {
            OnBackPressedDispatcher.this.b.remove(this.a);
            this.a.b.remove(this);
        }
    }

    public OnBackPressedDispatcher(Runnable runnable) {
        this.a = runnable;
    }

    public void a() {
        Iterator<b> descendingIterator = this.b.descendingIterator();
        while (descendingIterator.hasNext()) {
            OnBackPressedCallback next = descendingIterator.next();
            if (next.a) {
                FragmentManagerImpl fragmentManagerImpl = FragmentManagerImpl.this;
                fragmentManagerImpl.j();
                if (fragmentManagerImpl.f1300l.a) {
                    fragmentManagerImpl.b();
                    return;
                } else {
                    fragmentManagerImpl.f1299k.a();
                    return;
                }
            }
        }
        Runnable runnable = this.a;
        if (runnable != null) {
            runnable.run();
        }
    }
}
