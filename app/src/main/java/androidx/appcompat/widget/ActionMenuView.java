package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuPresenter;
import i.b.p.i.MenuView;
import i.b.q.ActionMenuPresenter;
import i.b.q.ViewUtils;
import j.a.a.a.outline;

public class ActionMenuView extends LinearLayoutCompat implements MenuBuilder.b, MenuView {
    public int A;
    public e B;

    /* renamed from: q  reason: collision with root package name */
    public MenuBuilder f86q;

    /* renamed from: r  reason: collision with root package name */
    public Context f87r;

    /* renamed from: s  reason: collision with root package name */
    public int f88s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f89t;
    public ActionMenuPresenter u;
    public MenuPresenter.a v;
    public MenuBuilder.a w;
    public boolean x;
    public int y;
    public int z;

    public interface a {
        boolean a();

        boolean b();
    }

    public static class b implements MenuPresenter.a {
        public void a(MenuBuilder menuBuilder, boolean z) {
        }

        public boolean a(MenuBuilder menuBuilder) {
            return false;
        }
    }

    public static class c extends LinearLayoutCompat.a {
        @ViewDebug.ExportedProperty
        public boolean c;
        @ViewDebug.ExportedProperty
        public int d;
        @ViewDebug.ExportedProperty

        /* renamed from: e  reason: collision with root package name */
        public int f90e;
        @ViewDebug.ExportedProperty

        /* renamed from: f  reason: collision with root package name */
        public boolean f91f;
        @ViewDebug.ExportedProperty
        public boolean g;
        public boolean h;

        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public c(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public c(c cVar) {
            super(cVar);
            this.c = cVar.c;
        }

        public c(int i2, int i3) {
            super(i2, i3);
            this.c = false;
        }
    }

    public interface e {
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public static int b(View view, int i2, int i3, int i4, int i5) {
        c cVar = (c) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i4) - i5, View.MeasureSpec.getMode(i4));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        boolean z2 = true;
        boolean z3 = actionMenuItemView != null && actionMenuItemView.d();
        int i6 = 2;
        if (i3 <= 0 || (z3 && i3 < 2)) {
            i6 = 0;
        } else {
            view.measure(View.MeasureSpec.makeMeasureSpec(i3 * i2, RecyclerView.UNDEFINED_DURATION), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            int i7 = measuredWidth / i2;
            if (measuredWidth % i2 != 0) {
                i7++;
            }
            if (!z3 || i7 >= 2) {
                i6 = i7;
            }
        }
        if (cVar.c || !z3) {
            z2 = false;
        }
        cVar.f91f = z2;
        cVar.d = i6;
        view.measure(View.MeasureSpec.makeMeasureSpec(i2 * i6, 1073741824), makeMeasureSpec);
        return i6;
    }

    public boolean a(MenuItemImpl menuItemImpl) {
        return this.f86q.a(menuItemImpl, (MenuPresenter) null, 0);
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof c;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public Menu getMenu() {
        if (this.f86q == null) {
            Context context = getContext();
            MenuBuilder menuBuilder = new MenuBuilder(context);
            this.f86q = menuBuilder;
            menuBuilder.f882e = new d();
            ActionMenuPresenter actionMenuPresenter = new ActionMenuPresenter(context);
            this.u = actionMenuPresenter;
            actionMenuPresenter.f938m = true;
            actionMenuPresenter.f939n = true;
            MenuPresenter.a aVar = this.v;
            if (aVar == null) {
                aVar = new b();
            }
            actionMenuPresenter.f859f = aVar;
            this.f86q.a(this.u, this.f87r);
            ActionMenuPresenter actionMenuPresenter2 = this.u;
            actionMenuPresenter2.f860i = this;
            this.f86q = actionMenuPresenter2.d;
        }
        return this.f86q;
    }

    public Drawable getOverflowIcon() {
        getMenu();
        ActionMenuPresenter actionMenuPresenter = this.u;
        ActionMenuPresenter.d dVar = actionMenuPresenter.f935j;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (actionMenuPresenter.f937l) {
            return actionMenuPresenter.f936k;
        }
        return null;
    }

    public int getPopupTheme() {
        return this.f88s;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        ActionMenuPresenter actionMenuPresenter = this.u;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.a(false);
            if (this.u.e()) {
                this.u.b();
                this.u.f();
            }
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ActionMenuPresenter actionMenuPresenter = this.u;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.a();
        }
    }

    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        if (!this.x) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int childCount = getChildCount();
        int i8 = (i5 - i3) / 2;
        int dividerWidth = getDividerWidth();
        int i9 = i4 - i2;
        int paddingRight = (i9 - getPaddingRight()) - getPaddingLeft();
        boolean a2 = ViewUtils.a(this);
        int i10 = 0;
        int i11 = 0;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() != 8) {
                c cVar = (c) childAt.getLayoutParams();
                if (cVar.c) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (b(i12)) {
                        measuredWidth += dividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a2) {
                        i6 = getPaddingLeft() + cVar.leftMargin;
                        i7 = i6 + measuredWidth;
                    } else {
                        i7 = (getWidth() - getPaddingRight()) - cVar.rightMargin;
                        i6 = i7 - measuredWidth;
                    }
                    int i13 = i8 - (measuredHeight / 2);
                    childAt.layout(i6, i13, i7, measuredHeight + i13);
                    paddingRight -= measuredWidth;
                    i10 = 1;
                } else {
                    paddingRight -= (childAt.getMeasuredWidth() + cVar.leftMargin) + cVar.rightMargin;
                    b(i12);
                    i11++;
                }
            }
        }
        if (childCount == 1 && i10 == 0) {
            View childAt2 = getChildAt(0);
            int measuredWidth2 = childAt2.getMeasuredWidth();
            int measuredHeight2 = childAt2.getMeasuredHeight();
            int i14 = (i9 / 2) - (measuredWidth2 / 2);
            int i15 = i8 - (measuredHeight2 / 2);
            childAt2.layout(i14, i15, measuredWidth2 + i14, measuredHeight2 + i15);
            return;
        }
        int i16 = i11 - (i10 ^ 1);
        int max = Math.max(0, i16 > 0 ? paddingRight / i16 : 0);
        if (a2) {
            int width = getWidth() - getPaddingRight();
            for (int i17 = 0; i17 < childCount; i17++) {
                View childAt3 = getChildAt(i17);
                c cVar2 = (c) childAt3.getLayoutParams();
                if (childAt3.getVisibility() != 8 && !cVar2.c) {
                    int i18 = width - cVar2.rightMargin;
                    int measuredWidth3 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i19 = i8 - (measuredHeight3 / 2);
                    childAt3.layout(i18 - measuredWidth3, i19, i18, measuredHeight3 + i19);
                    width = i18 - ((measuredWidth3 + cVar2.leftMargin) + max);
                }
            }
            return;
        }
        int paddingLeft = getPaddingLeft();
        for (int i20 = 0; i20 < childCount; i20++) {
            View childAt4 = getChildAt(i20);
            c cVar3 = (c) childAt4.getLayoutParams();
            if (childAt4.getVisibility() != 8 && !cVar3.c) {
                int i21 = paddingLeft + cVar3.leftMargin;
                int measuredWidth4 = childAt4.getMeasuredWidth();
                int measuredHeight4 = childAt4.getMeasuredHeight();
                int i22 = i8 - (measuredHeight4 / 2);
                childAt4.layout(i21, i22, i21 + measuredWidth4, measuredHeight4 + i22);
                paddingLeft = outline.a(measuredWidth4, cVar3.rightMargin, max, i21);
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        boolean z2;
        int i5;
        int i6;
        boolean z3;
        int i7;
        boolean z4;
        MenuBuilder menuBuilder;
        boolean z5 = this.x;
        boolean z6 = View.MeasureSpec.getMode(i2) == 1073741824;
        this.x = z6;
        if (z5 != z6) {
            this.y = 0;
        }
        int size = View.MeasureSpec.getSize(i2);
        if (!(!this.x || (menuBuilder = this.f86q) == null || size == this.y)) {
            this.y = size;
            menuBuilder.b(true);
        }
        int childCount = getChildCount();
        if (!this.x || childCount <= 0) {
            for (int i8 = 0; i8 < childCount; i8++) {
                c cVar = (c) getChildAt(i8).getLayoutParams();
                cVar.rightMargin = 0;
                cVar.leftMargin = 0;
            }
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i2);
        int size3 = View.MeasureSpec.getSize(i3);
        int paddingRight = getPaddingRight() + getPaddingLeft();
        int paddingBottom = getPaddingBottom() + getPaddingTop();
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(i3, paddingBottom, -2);
        int i9 = size2 - paddingRight;
        int i10 = this.z;
        int i11 = i9 / i10;
        int i12 = i9 % i10;
        if (i11 == 0) {
            setMeasuredDimension(i9, 0);
            return;
        }
        int i13 = (i12 / i11) + i10;
        int childCount2 = getChildCount();
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        boolean z7 = false;
        long j2 = 0;
        while (i18 < childCount2) {
            View childAt = getChildAt(i18);
            int i19 = size3;
            int i20 = i9;
            if (childAt.getVisibility() != 8) {
                boolean z8 = childAt instanceof ActionMenuItemView;
                int i21 = i14 + 1;
                if (z8) {
                    int i22 = this.A;
                    i7 = i21;
                    z4 = false;
                    childAt.setPadding(i22, 0, i22, 0);
                } else {
                    i7 = i21;
                    z4 = false;
                }
                c cVar2 = (c) childAt.getLayoutParams();
                cVar2.h = z4;
                cVar2.f90e = z4 ? 1 : 0;
                cVar2.d = z4;
                cVar2.f91f = z4;
                cVar2.leftMargin = z4;
                cVar2.rightMargin = z4;
                cVar2.g = z8 && ((ActionMenuItemView) childAt).d();
                int b2 = b(childAt, i13, cVar2.c ? 1 : i11, childMeasureSpec, paddingBottom);
                i16 = Math.max(i16, b2);
                if (cVar2.f91f) {
                    i17++;
                }
                if (cVar2.c) {
                    z7 = true;
                }
                i11 -= b2;
                i15 = Math.max(i15, childAt.getMeasuredHeight());
                if (b2 == 1) {
                    j2 |= (long) (1 << i18);
                }
                i14 = i7;
            }
            i18++;
            size3 = i19;
            i9 = i20;
        }
        int i23 = i9;
        int i24 = size3;
        boolean z9 = z7 && i14 == 2;
        boolean z10 = false;
        while (true) {
            if (i17 <= 0 || i11 <= 0) {
                i4 = i15;
                z2 = z10;
            } else {
                int i25 = Integer.MAX_VALUE;
                int i26 = 0;
                int i27 = 0;
                long j3 = 0;
                while (i26 < childCount2) {
                    int i28 = i15;
                    c cVar3 = (c) getChildAt(i26).getLayoutParams();
                    boolean z11 = z10;
                    if (cVar3.f91f) {
                        int i29 = cVar3.d;
                        if (i29 < i25) {
                            j3 = 1 << i26;
                            i25 = i29;
                            i27 = 1;
                        } else if (i29 == i25) {
                            i27++;
                            j3 |= 1 << i26;
                        }
                    }
                    i26++;
                    z10 = z11;
                    i15 = i28;
                }
                i4 = i15;
                z2 = z10;
                j2 |= j3;
                if (i27 > i11) {
                    break;
                }
                int i30 = i25 + 1;
                int i31 = 0;
                while (i31 < childCount2) {
                    View childAt2 = getChildAt(i31);
                    c cVar4 = (c) childAt2.getLayoutParams();
                    int i32 = i17;
                    long j4 = (long) (1 << i31);
                    if ((j3 & j4) == 0) {
                        if (cVar4.d == i30) {
                            j2 |= j4;
                        }
                        z3 = z9;
                    } else {
                        if (!z9 || !cVar4.g || i11 != 1) {
                            z3 = z9;
                        } else {
                            int i33 = this.A;
                            z3 = z9;
                            childAt2.setPadding(i33 + i13, 0, i33, 0);
                        }
                        cVar4.d++;
                        cVar4.h = true;
                        i11--;
                    }
                    i31++;
                    i17 = i32;
                    z9 = z3;
                }
                i15 = i4;
                z10 = true;
            }
        }
        i4 = i15;
        z2 = z10;
        boolean z12 = !z7 && i14 == 1;
        if (i11 > 0 && j2 != 0 && (i11 < i14 - 1 || z12 || i16 > 1)) {
            float bitCount = (float) Long.bitCount(j2);
            if (!z12) {
                if ((j2 & 1) != 0 && !((c) getChildAt(0).getLayoutParams()).g) {
                    bitCount -= 0.5f;
                }
                int i34 = childCount2 - 1;
                if ((j2 & ((long) (1 << i34))) != 0 && !((c) getChildAt(i34).getLayoutParams()).g) {
                    bitCount -= 0.5f;
                }
            }
            int i35 = bitCount > 0.0f ? (int) (((float) (i11 * i13)) / bitCount) : 0;
            for (int i36 = 0; i36 < childCount2; i36++) {
                if ((j2 & ((long) (1 << i36))) != 0) {
                    View childAt3 = getChildAt(i36);
                    c cVar5 = (c) childAt3.getLayoutParams();
                    if (childAt3 instanceof ActionMenuItemView) {
                        cVar5.f90e = i35;
                        cVar5.h = true;
                        if (i36 == 0 && !cVar5.g) {
                            cVar5.leftMargin = (-i35) / 2;
                        }
                    } else if (cVar5.c) {
                        cVar5.f90e = i35;
                        cVar5.h = true;
                        cVar5.rightMargin = (-i35) / 2;
                    } else {
                        if (i36 != 0) {
                            cVar5.leftMargin = i35 / 2;
                        }
                        if (i36 != childCount2 - 1) {
                            cVar5.rightMargin = i35 / 2;
                        }
                    }
                    z2 = true;
                }
            }
        }
        if (z2) {
            for (int i37 = 0; i37 < childCount2; i37++) {
                View childAt4 = getChildAt(i37);
                c cVar6 = (c) childAt4.getLayoutParams();
                if (cVar6.h) {
                    childAt4.measure(View.MeasureSpec.makeMeasureSpec((cVar6.d * i13) + cVar6.f90e, 1073741824), childMeasureSpec);
                }
            }
        }
        if (mode != 1073741824) {
            i6 = i23;
            i5 = i4;
        } else {
            i5 = i24;
            i6 = i23;
        }
        setMeasuredDimension(i6, i5);
    }

    public void setExpandedActionViewsExclusive(boolean z2) {
        this.u.f943r = z2;
    }

    public void setOnMenuItemClickListener(e eVar) {
        this.B = eVar;
    }

    public void setOverflowIcon(Drawable drawable) {
        getMenu();
        ActionMenuPresenter actionMenuPresenter = this.u;
        ActionMenuPresenter.d dVar = actionMenuPresenter.f935j;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        actionMenuPresenter.f937l = true;
        actionMenuPresenter.f936k = drawable;
    }

    public void setOverflowReserved(boolean z2) {
        this.f89t = z2;
    }

    public void setPopupTheme(int i2) {
        if (this.f88s != i2) {
            this.f88s = i2;
            if (i2 == 0) {
                this.f87r = getContext();
            } else {
                this.f87r = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.u = actionMenuPresenter;
        actionMenuPresenter.f860i = this;
        this.f86q = actionMenuPresenter.d;
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.z = (int) (56.0f * f2);
        this.A = (int) (f2 * 4.0f);
        this.f87r = context;
        this.f88s = 0;
    }

    public void a(MenuBuilder menuBuilder) {
        this.f86q = menuBuilder;
    }

    public c generateDefaultLayoutParams() {
        c cVar = new c(-2, -2);
        cVar.b = 16;
        return cVar;
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    public class d implements MenuBuilder.a {
        public d() {
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            e eVar = ActionMenuView.this.B;
            if (eVar == null) {
                return false;
            }
            Toolbar.f fVar = Toolbar.this.H;
            if (fVar != null ? fVar.onMenuItemClick(menuItem) : false) {
                return true;
            }
            return false;
        }

        public void a(MenuBuilder menuBuilder) {
            MenuBuilder.a aVar = ActionMenuView.this.w;
            if (aVar != null) {
                aVar.a(menuBuilder);
            }
        }
    }

    /* renamed from: generateLayoutParams  reason: collision with other method in class */
    public LinearLayoutCompat.a m0generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    public c generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return generateDefaultLayoutParams();
        }
        c cVar = layoutParams instanceof c ? new c((c) layoutParams) : new c(layoutParams);
        if (cVar.b <= 0) {
            cVar.b = 16;
        }
        return cVar;
    }

    public boolean b(int i2) {
        boolean z2 = false;
        if (i2 == 0) {
            return false;
        }
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        if (i2 < getChildCount() && (childAt instanceof a)) {
            z2 = false | ((a) childAt).a();
        }
        return (i2 <= 0 || !(childAt2 instanceof a)) ? z2 : z2 | ((a) childAt2).b();
    }
}
