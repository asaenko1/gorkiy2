package androidx.lifecycle;

import i.o.GeneratedAdapter;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;
import i.o.MethodCallsLogger;

public class CompositeGeneratedAdaptersObserver implements LifecycleEventObserver {
    public final GeneratedAdapter[] a;

    public CompositeGeneratedAdaptersObserver(GeneratedAdapter[] generatedAdapterArr) {
        this.a = generatedAdapterArr;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
        MethodCallsLogger methodCallsLogger = new MethodCallsLogger();
        for (GeneratedAdapter a2 : this.a) {
            a2.a(lifecycleOwner, aVar, false, methodCallsLogger);
        }
        for (GeneratedAdapter a3 : this.a) {
            a3.a(lifecycleOwner, aVar, true, methodCallsLogger);
        }
    }
}
