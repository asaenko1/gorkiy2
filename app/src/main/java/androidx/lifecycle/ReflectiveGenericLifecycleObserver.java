package androidx.lifecycle;

import i.o.ClassesInfoCache;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;

public class ReflectiveGenericLifecycleObserver implements LifecycleEventObserver {
    public final Object a;
    public final ClassesInfoCache.a b;

    public ReflectiveGenericLifecycleObserver(Object obj) {
        this.a = obj;
        this.b = ClassesInfoCache.c.a(obj.getClass());
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
        ClassesInfoCache.a aVar2 = this.b;
        Object obj = this.a;
        ClassesInfoCache.a.a(aVar2.a.get(aVar), lifecycleOwner, aVar, obj);
        ClassesInfoCache.a.a(aVar2.a.get(Lifecycle.a.ON_ANY), lifecycleOwner, aVar, obj);
    }
}
