package androidx.preference;

import android.content.res.TypedArray;

public class SeekBarPreference extends Preference {

    /* renamed from: p  reason: collision with root package name */
    public int f268p;

    /* renamed from: q  reason: collision with root package name */
    public int f269q;

    /* renamed from: r  reason: collision with root package name */
    public int f270r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f271s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f272t;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SeekBarPreference(android.content.Context r4, android.util.AttributeSet r5) {
        /*
            r3 = this;
            int r0 = i.q.a.seekBarPreferenceStyle
            r1 = 0
            r3.<init>(r4, r5, r0, r1)
            int[] r2 = i.q.d.SeekBarPreference
            android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r2, r0, r1)
            int r5 = i.q.d.SeekBarPreference_min
            int r5 = r4.getInt(r5, r1)
            r3.f268p = r5
            int r5 = i.q.d.SeekBarPreference_android_max
            r0 = 100
            int r5 = r4.getInt(r5, r0)
            int r0 = r3.f268p
            if (r5 >= r0) goto L_0x0021
            r5 = r0
        L_0x0021:
            int r0 = r3.f269q
            if (r5 == r0) goto L_0x0027
            r3.f269q = r5
        L_0x0027:
            int r5 = i.q.d.SeekBarPreference_seekBarIncrement
            int r5 = r4.getInt(r5, r1)
            int r0 = r3.f270r
            if (r5 == r0) goto L_0x0040
            int r0 = r3.f269q
            int r2 = r3.f268p
            int r0 = r0 - r2
            int r5 = java.lang.Math.abs(r5)
            int r5 = java.lang.Math.min(r0, r5)
            r3.f270r = r5
        L_0x0040:
            int r5 = i.q.d.SeekBarPreference_adjustable
            r0 = 1
            boolean r5 = r4.getBoolean(r5, r0)
            r3.f271s = r5
            int r5 = i.q.d.SeekBarPreference_showSeekBarValue
            r4.getBoolean(r5, r1)
            int r5 = i.q.d.SeekBarPreference_updatesContinuously
            boolean r5 = r4.getBoolean(r5, r1)
            r3.f272t = r5
            r4.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.SeekBarPreference.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public Object a(TypedArray typedArray, int i2) {
        return Integer.valueOf(typedArray.getInt(i2, 0));
    }
}
