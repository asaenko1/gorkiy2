package androidx.constraintlayout.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import i.f.b.ConstraintHelper;
import i.f.b.g;

public class Barrier extends ConstraintHelper {
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f148i;

    /* renamed from: j  reason: collision with root package name */
    public i.f.a.h.Barrier f149j;

    public Barrier(Context context) {
        super(context);
        super.setVisibility(8);
    }

    public void a(AttributeSet attributeSet) {
        super.a(attributeSet);
        this.f149j = new i.f.a.h.Barrier();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, g.ConstraintLayout_Layout);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i2 = 0; i2 < indexCount; i2++) {
                int index = obtainStyledAttributes.getIndex(i2);
                if (index == g.ConstraintLayout_Layout_barrierDirection) {
                    setType(obtainStyledAttributes.getInt(index, 0));
                } else if (index == g.ConstraintLayout_Layout_barrierAllowsGoneWidgets) {
                    this.f149j.o0 = obtainStyledAttributes.getBoolean(index, true);
                }
            }
        }
        super.f1137e = this.f149j;
        a();
    }

    public int getType() {
        return this.h;
    }

    public void setAllowsGoneWidget(boolean z) {
        this.f149j.o0 = z;
    }

    public void setType(int i2) {
        this.h = i2;
        this.f148i = i2;
        if (1 == getResources().getConfiguration().getLayoutDirection()) {
            int i3 = this.h;
            if (i3 == 5) {
                this.f148i = 1;
            } else if (i3 == 6) {
                this.f148i = 0;
            }
        } else {
            int i4 = this.h;
            if (i4 == 5) {
                this.f148i = 0;
            } else if (i4 == 6) {
                this.f148i = 1;
            }
        }
        this.f149j.m0 = this.f148i;
    }

    public Barrier(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        super.setVisibility(8);
    }

    public Barrier(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        super.setVisibility(8);
    }
}
