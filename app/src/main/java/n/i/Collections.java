package n.i;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import e.a.a.a.e.d;
import e.a.a.a.e.o.ITopLevelBaseCoordinator;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.b.g.EditTextExtensions;
import e.a.b.g.EditTextExtensions0;
import e.a.b.h.c.a;
import e.c.c.BaseMviView;
import e.c.c.BaseMviView0;
import e.c.c.BaseMviView1;
import e.c.c.BaseMviVm1;
import e.c.d.a.ViewAction;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.net.IDN;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import kotlin.TypeCastException;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import l.b.Observable;
import l.b.l;
import l.b.r.b.AndroidSchedulers;
import l.b.s.Disposable;
import l.b.s.b;
import n.Tuples;
import n.l.Continuation;
import n.l.ContinuationInterceptor;
import n.l.e.a.ContinuationImpl1;
import n.m.PlatformImplementations0;
import n.n.b.Functions0;
import n.n.c.ClassBasedDeclarationContainer;
import n.n.c.Intrinsics;
import n.n.c.SpreadBuilder;
import n.p.KClass;
import n.q.Sequence;
import n.q.Sequences2;
import n.r.Indent;
import o.m0.Util;
import org.threeten.bp.DateTimeException;
import p.AsyncTimeout0;
import p.AsyncTimeout1;
import p.Buffer;
import p.BufferedSink;
import p.BufferedSource;
import p.Okio0;
import p.Okio1;
import p.Okio2;
import p.RealBufferedSink;
import p.RealBufferedSource;
import p.Sink;
import p.Source;
import p.Timeout;
import q.b.a.DayOfWeek;
import q.b.a.Instant;
import q.b.a.LocalDate;
import q.b.a.ZoneId;
import q.b.a.ZonedDateTime;
import q.b.a.t.DateTimeFormatter;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAdjusters0;
import r.Call;
import r.KotlinExtensions;
import r.KotlinExtensions0;
import r.KotlinExtensions1;
import r.KotlinExtensions2;
import r.KotlinExtensions3;
import r.KotlinExtensions4;
import r.Response;
import ru.covid19.core.data.network.model.Citizenship;
import ru.covid19.core.data.network.model.CitizenshipResponse;

/* compiled from: Collections.kt */
public class Collections {
    public static int a(int i2, int i3) {
        if (i2 < i3) {
            return -1;
        }
        return i2 > i3 ? 1 : 0;
    }

    public static int a(long j2, long j3) {
        int i2 = (j2 > j3 ? 1 : (j2 == j3 ? 0 : -1));
        if (i2 < 0) {
            return -1;
        }
        return i2 > 0 ? 1 : 0;
    }

    public static int a(byte[] bArr, int i2) {
        int i3 = i2 + 1;
        int i4 = i3 + 1;
        return (bArr[i4 + 1] << 24) | (bArr[i2] & 255) | ((bArr[i3] & 255) << 8) | ((bArr[i4] & 255) << 16);
    }

    public static void a(int i2, byte[] bArr, int i3) {
        bArr[i3] = (byte) (i2 >>> 24);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i2 >>> 16);
        int i5 = i4 + 1;
        bArr[i5] = (byte) (i2 >>> 8);
        bArr[i5 + 1] = (byte) i2;
    }

    public static final void a(CompoundButton compoundButton, boolean z) {
        if (compoundButton == null) {
            Intrinsics.a("$this$setCheckedIfNotEqual");
            throw null;
        } else if (compoundButton.isChecked() != z) {
            compoundButton.setChecked(z);
        }
    }

    public static final void b(View view) {
        if (view != null) {
            view.setVisibility(0);
        } else {
            Intrinsics.a("$this$toVisible");
            throw null;
        }
    }

    public static final int c(int i2, int i3) {
        int i4 = i2 % i3;
        return i4 >= 0 ? i4 : i4 + i3;
    }

    public static int d(int i2, int i3) {
        int i4 = i2 + i3;
        if ((i2 ^ i4) >= 0 || (i2 ^ i3) < 0) {
            return i4;
        }
        throw new ArithmeticException(outline.a("Addition overflows an int: ", i2, " + ", i3));
    }

    public static int e(int i2, int i3) {
        int i4 = i2 - i3;
        if ((i2 ^ i4) >= 0 || (i2 ^ i3) >= 0) {
            return i4;
        }
        throw new ArithmeticException(outline.a("Subtraction overflows an int: ", i2, " - ", i3));
    }

    public static long f(long j2, long j3) {
        long j4 = j2 - j3;
        if ((j2 ^ j4) >= 0 || (j2 ^ j3) >= 0) {
            return j4;
        }
        throw new ArithmeticException("Subtraction overflows a long: " + j2 + " - " + j3);
    }

    public static final <T> Object c(Call<T> call, Continuation<? super Response<T>> continuation) {
        CancellableContinuation cancellableContinuationImpl = new CancellableContinuationImpl(a((Continuation) continuation), 1);
        CancellableContinuation cancellableContinuation = cancellableContinuationImpl;
        cancellableContinuation.invokeOnCancellation(new KotlinExtensions4(call));
        call.a(new KotlinExtensions1(cancellableContinuation));
        Object result = cancellableContinuationImpl.getResult();
        n.l.d.Intrinsics intrinsics = n.l.d.Intrinsics.COROUTINE_SUSPENDED;
        return result;
    }

    public static long d(long j2, long j3) {
        long j4 = j2 + j3;
        if ((j2 ^ j4) >= 0 || (j2 ^ j3) < 0) {
            return j4;
        }
        throw new ArithmeticException("Addition overflows a long: " + j2 + " + " + j3);
    }

    public static long e(long j2, long j3) {
        if (j3 == 1) {
            return j2;
        }
        if (j2 == 1) {
            return j3;
        }
        if (j2 == 0 || j3 == 0) {
            return 0;
        }
        long j4 = j2 * j3;
        if (j4 / j3 == j2 && ((j2 != Long.MIN_VALUE || j3 != -1) && (j3 != Long.MIN_VALUE || j2 != -1))) {
            return j4;
        }
        throw new ArithmeticException("Multiplication overflows a long: " + j2 + " * " + j3);
    }

    public static final void b(byte[] bArr, int i2, byte[] bArr2, int i3, int i4) {
        if (bArr == null) {
            Intrinsics.a("src");
            throw null;
        } else if (bArr2 != null) {
            System.arraycopy(bArr, i2, bArr2, i3, i4);
        } else {
            Intrinsics.a("dest");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T> b a(Observable observable, ViewAction viewAction) {
        if (observable == null) {
            Intrinsics.a("$this$bindTo");
            throw null;
        } else if (viewAction != null) {
            Disposable a = observable.a(viewAction.b);
            Intrinsics.a((Object) a, "this.subscribe(action.consumer)");
            return a;
        } else {
            Intrinsics.a("action");
            throw null;
        }
    }

    public static final <T> Object b(Call<T> call, Continuation<? super T> continuation) {
        CancellableContinuation cancellableContinuationImpl = new CancellableContinuationImpl(a((Continuation) continuation), 1);
        CancellableContinuation cancellableContinuation = cancellableContinuationImpl;
        cancellableContinuation.invokeOnCancellation(new KotlinExtensions3(call));
        call.a(new KotlinExtensions0(cancellableContinuation));
        Object result = cancellableContinuationImpl.getResult();
        n.l.d.Intrinsics intrinsics = n.l.d.Intrinsics.COROUTINE_SUSPENDED;
        return result;
    }

    public static /* synthetic */ void a(EditText editText, Long l2, String str, Date date, Functions0 functions0, int i2) {
        if ((i2 & 1) != 0) {
            l2 = null;
        }
        if ((i2 & 2) != 0) {
            str = "dd.MM.yyyy";
        }
        if ((i2 & 4) != 0) {
            date = null;
        }
        if ((i2 & 8) != 0) {
            functions0 = null;
        }
        if (editText == null) {
            Intrinsics.a("$this$attachDatePicker");
            throw null;
        } else if (str != null) {
            editText.setOnClickListener(new EditTextExtensions(editText, new EditTextExtensions0(editText, str, functions0, date), l2));
        } else {
            Intrinsics.a("format");
            throw null;
        }
    }

    public static long c(long j2, long j3) {
        return ((j2 % j3) + j3) % j3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.SharedPreferences$Editor, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final void a(SharedPreferences sharedPreferences, String str, Object obj) {
        if (sharedPreferences == null) {
            Intrinsics.a("$this$set");
            throw null;
        } else if (str != null) {
            boolean z = true;
            if (obj != null ? obj instanceof String : true) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                Intrinsics.a((Object) edit, "editor");
                edit.putString(str, (String) obj);
                edit.apply();
                return;
            }
            if (obj != null ? obj instanceof Integer : true) {
                SharedPreferences.Editor edit2 = sharedPreferences.edit();
                Intrinsics.a((Object) edit2, "editor");
                if (obj != null) {
                    edit2.putInt(str, ((Integer) obj).intValue());
                    edit2.apply();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
            }
            if (obj != null ? obj instanceof Float : true) {
                SharedPreferences.Editor edit3 = sharedPreferences.edit();
                Intrinsics.a((Object) edit3, "editor");
                if (obj != null) {
                    edit3.putFloat(str, ((Float) obj).floatValue());
                    edit3.apply();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            }
            if (obj != null ? obj instanceof Long : true) {
                SharedPreferences.Editor edit4 = sharedPreferences.edit();
                Intrinsics.a((Object) edit4, "editor");
                if (obj != null) {
                    edit4.putLong(str, ((Long) obj).longValue());
                    edit4.apply();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Long");
            }
            if (obj != null ? obj instanceof Boolean : true) {
                SharedPreferences.Editor edit5 = sharedPreferences.edit();
                Intrinsics.a((Object) edit5, "editor");
                if (obj != null) {
                    edit5.putBoolean(str, ((Boolean) obj).booleanValue());
                    edit5.apply();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Boolean");
            }
            if (obj != null) {
                z = obj instanceof Set;
            }
            if (z) {
                SharedPreferences.Editor edit6 = sharedPreferences.edit();
                Intrinsics.a((Object) edit6, "editor");
                if (obj != null) {
                    edit6.putStringSet(str, (Set) obj);
                    edit6.apply();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.Set<kotlin.String>");
            }
            throw new UnsupportedOperationException("Not acceptable type");
        } else {
            Intrinsics.a("key");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.InputStream, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final Source b(Socket socket) {
        if (socket != null) {
            Okio2 okio2 = new Okio2(socket);
            InputStream inputStream = socket.getInputStream();
            Intrinsics.a((Object) inputStream, "getInputStream()");
            return new AsyncTimeout1(okio2, new Okio0(inputStream, okio2));
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final <T> List<T> b(List<? extends T> list) {
        if (list != null) {
            int size = list.size();
            if (size == 0) {
                return Collections2.b;
            }
            if (size != 1) {
                return list;
            }
            return c.c(list.get(0));
        }
        Intrinsics.a("$this$optimizeReadOnlyList");
        throw null;
    }

    public static long b(long j2, int i2) {
        if (i2 != -1) {
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                return j2;
            }
            long j3 = (long) i2;
            long j4 = j2 * j3;
            if (j4 / j3 == j2) {
                return j4;
            }
            throw new ArithmeticException("Multiplication overflows a long: " + j2 + " * " + i2);
        } else if (j2 != Long.MIN_VALUE) {
            return -j2;
        } else {
            throw new ArithmeticException("Multiplication overflows a long: " + j2 + " * " + i2);
        }
    }

    public static long b(long j2, long j3) {
        return j2 >= 0 ? j2 / j3 : ((j2 + 1) / j3) - 1;
    }

    public static int b(int i2, int i3) {
        return ((i2 % i3) + i3) % i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Set<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T> Set<T> a(T t2) {
        Set<T> singleton = java.util.Collections.singleton(t2);
        Intrinsics.a((Object) singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }

    public static List<Tuples<d, String>> a(a aVar) {
        if (aVar.b().isEmpty()) {
            return aVar.a();
        }
        SpreadBuilder spreadBuilder = new SpreadBuilder(2);
        Object[] array = aVar.a().toArray(new Tuples[0]);
        if (array != null) {
            spreadBuilder.a(array);
            Object[] array2 = aVar.b().toArray(new Tuples[0]);
            if (array2 != null) {
                spreadBuilder.a(array2);
                return a((Object[]) ((Tuples[]) spreadBuilder.a.toArray(new Tuples[spreadBuilder.a.size()])));
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public static final List<Citizenship> a(List<Citizenship> list) {
        if (list != null) {
            Iterator<Citizenship> it = list.iterator();
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i2 = -1;
                    break;
                }
                String code = it.next().getCode();
                if (code == null) {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                } else if (code.contentEquals("RUS")) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return list;
            }
            ArrayList arrayList = new ArrayList(list);
            arrayList.remove(i2);
            arrayList.add(0, (CitizenshipResponse) arrayList.get(i2));
            return arrayList;
        }
        Intrinsics.a("$this$makeRUSFirst");
        throw null;
    }

    public static final void a(long j2, long j3, long j4) {
        if ((j3 | j4) < 0 || j3 > j2 || j2 - j3 < j4) {
            throw new ArrayIndexOutOfBoundsException("size=" + j2 + " offset=" + j3 + " byteCount=" + j4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [java.io.BufferedInputStream, ?[OBJECT, ARRAY]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [java.io.BufferedInputStream, java.lang.Throwable]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    public static final byte[] a(Context context, Uri uri) {
        if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (uri != null) {
            InputStream openInputStream = context.getContentResolver().openInputStream(uri);
            if (openInputStream == null) {
                return null;
            }
            BufferedInputStream bufferedInputStream = openInputStream instanceof BufferedInputStream ? (BufferedInputStream) openInputStream : new BufferedInputStream(openInputStream, 8192);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Math.max(8192, bufferedInputStream.available()));
                byte[] bArr = new byte[8192];
                for (int read = bufferedInputStream.read(bArr); read >= 0; read = bufferedInputStream.read(bArr)) {
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                Intrinsics.a((Object) byteArray, "buffer.toByteArray()");
                a((Closeable) bufferedInputStream, (Throwable) null);
                return byteArray;
            } catch (Throwable th) {
                a((Closeable) bufferedInputStream, th);
                throw th;
            }
        } else {
            Intrinsics.a("uri");
            throw null;
        }
    }

    public static final void a(View view) {
        if (view != null) {
            view.setVisibility(8);
        } else {
            Intrinsics.a("$this$toGone");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final void a(int i2, String str, Throwable th) {
        int min;
        if (str != null) {
            int i3 = 5;
            if (i2 != 5) {
                i3 = 3;
            }
            if (th != null) {
                StringBuilder b = outline.b(str, "\n");
                b.append(Log.getStackTraceString(th));
                str = b.toString();
            }
            int length = str.length();
            int i4 = 0;
            while (i4 < length) {
                int a = Indent.a((CharSequence) str, 10, i4, false, 4);
                if (a == -1) {
                    a = length;
                }
                while (true) {
                    min = Math.min(a, i4 + 4000);
                    String substring = str.substring(i4, min);
                    Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    Log.println(i3, "OkHttp", substring);
                    if (min >= a) {
                        break;
                    }
                    i4 = min;
                }
                i4 = min + 1;
            }
            return;
        }
        Intrinsics.a("message");
        throw null;
    }

    public static final boolean a(Buffer buffer) {
        if (buffer != null) {
            try {
                Buffer buffer2 = new Buffer();
                long j2 = buffer.c;
                buffer.a(buffer2, 0, j2 > 64 ? 64 : j2);
                for (int i2 = 0; i2 < 16; i2++) {
                    if (buffer2.j()) {
                        return true;
                    }
                    int p2 = buffer2.p();
                    if (Character.isISOControl(p2) && !Character.isWhitespace(p2)) {
                        return false;
                    }
                }
                return true;
            } catch (EOFException unused) {
                return false;
            }
        } else {
            Intrinsics.a("$this$isProbablyUtf8");
            throw null;
        }
    }

    public static final void a(View view, boolean z) {
        if (view != null) {
            view.setVisibility(z ? 0 : 8);
        } else {
            Intrinsics.a("$this$toVisibleOrGone");
            throw null;
        }
    }

    public static <VS, VM extends BaseMviVm1<VS>> void a(BaseMviView1<VS, VM> baseMviView1) {
        baseMviView1.i().a(((BaseMviVm1) baseMviView1.h()).d.b((l) AndroidSchedulers.a()).a((l) AndroidSchedulers.a()).a(new BaseMviView(baseMviView1)), ((BaseMviVm1) baseMviView1.h()).f721e.b((l) AndroidSchedulers.a()).a((l) AndroidSchedulers.a()).a(new BaseMviView0(baseMviView1)));
        ((BaseMviVm1) baseMviView1.h()).a((List<? extends Observable<? extends e.c.c.b>>) baseMviView1.g());
    }

    public static /* synthetic */ void a(IFragmentCoordinator iFragmentCoordinator, Integer num, Object obj, int i2, Object obj2) {
        if (obj2 == null) {
            if ((i2 & 1) != 0) {
                num = null;
            }
            if ((i2 & 2) != 0) {
                obj = null;
            }
            iFragmentCoordinator.a(num, obj);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: back");
    }

    public static final boolean a(char c, char c2, boolean z) {
        if (c == c2) {
            return true;
        }
        if (!z) {
            return false;
        }
        return Character.toUpperCase(c) == Character.toUpperCase(c2) || Character.toLowerCase(c) == Character.toLowerCase(c2);
    }

    public static /* synthetic */ void a(ITopLevelBaseCoordinator iTopLevelBaseCoordinator, String str, Object obj, int i2, Object obj2) {
        if (obj2 == null) {
            if ((i2 & 2) != 0) {
                obj = null;
            }
            iTopLevelBaseCoordinator.a(str, obj);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showMessage");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, char, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    public static final String a(String str) {
        InetAddress inetAddress;
        if (str != null) {
            int i2 = 0;
            int i3 = -1;
            if (Indent.a((CharSequence) str, (CharSequence) ":", false, 2)) {
                if (!Indent.b(str, "[", false, 2) || !Indent.a(str, "]", false, 2)) {
                    inetAddress = a(str, 0, str.length());
                } else {
                    inetAddress = a(str, 1, str.length() - 1);
                }
                if (inetAddress == null) {
                    return null;
                }
                byte[] address = inetAddress.getAddress();
                if (address.length == 16) {
                    Intrinsics.a((Object) address, "address");
                    int i4 = 0;
                    int i5 = 0;
                    while (i4 < address.length) {
                        int i6 = i4;
                        while (i6 < 16 && address[i6] == 0 && address[i6 + 1] == 0) {
                            i6 += 2;
                        }
                        int i7 = i6 - i4;
                        if (i7 > i5 && i7 >= 4) {
                            i3 = i4;
                            i5 = i7;
                        }
                        i4 = i6 + 2;
                    }
                    Buffer buffer = new Buffer();
                    while (i2 < address.length) {
                        if (i2 == i3) {
                            buffer.writeByte(58);
                            i2 += i5;
                            if (i2 == 16) {
                                buffer.writeByte(58);
                            }
                        } else {
                            if (i2 > 0) {
                                buffer.writeByte(58);
                            }
                            buffer.a((long) ((Util.a(address[i2], 255) << 8) | (255 & address[i2 + 1])));
                            i2 += 2;
                        }
                    }
                    return buffer.o();
                } else if (address.length == 4) {
                    return inetAddress.getHostAddress();
                } else {
                    throw new AssertionError("Invalid IPv6 address: '" + str + '\'');
                }
            } else {
                try {
                    String ascii = IDN.toASCII(str);
                    Intrinsics.a((Object) ascii, "IDN.toASCII(host)");
                    Locale locale = Locale.US;
                    Intrinsics.a((Object) locale, "Locale.US");
                    String lowerCase = ascii.toLowerCase(locale);
                    Intrinsics.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    if (lowerCase.length() == 0) {
                        return null;
                    }
                    int length = lowerCase.length();
                    int i8 = 0;
                    while (true) {
                        if (i8 >= length) {
                            break;
                        }
                        char charAt = lowerCase.charAt(i8);
                        if (charAt <= 31) {
                            break;
                        } else if (charAt >= 127) {
                            break;
                        } else if (Indent.a((CharSequence) " #%/:?@[\\]", charAt, 0, false, 6) != -1) {
                            break;
                        } else {
                            i8++;
                        }
                    }
                    i2 = 1;
                    if (i2 != 0) {
                        return null;
                    }
                    return lowerCase;
                } catch (IllegalArgumentException unused) {
                    return null;
                }
            }
        } else {
            Intrinsics.a("$this$toCanonicalHost");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static /* synthetic */ String a(int i2, String str, int i3) {
        if ((i3 & 1) != 0) {
            str = "dd.MM.yyyy";
        }
        if (str != null) {
            try {
                LocalDate a = LocalDate.a(String.valueOf(i2), DateTimeFormatter.a("yyyyMMdd"));
                DateTimeFormatter a2 = DateTimeFormatter.a(str);
                if (a != null) {
                    a((Object) a2, "formatter");
                    return a2.a(a);
                }
                throw null;
            } catch (DateTimeException unused) {
                return null;
            }
        } else {
            Intrinsics.a("resultFormat");
            throw null;
        }
    }

    public static final <T> int a(Iterable iterable, int i2) {
        if (iterable != null) {
            return iterable instanceof Collection ? ((Collection) iterable).size() : i2;
        }
        Intrinsics.a("$this$collectionSizeOrDefault");
        throw null;
    }

    public static final BufferedSource a(Source source) {
        if (source != null) {
            return new RealBufferedSource(source);
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static /* synthetic */ Integer a(String str, String str2, int i2) {
        if ((i2 & 1) != 0) {
            str2 = "dd.MM.yyyy";
        }
        if (str == null) {
            Intrinsics.a("$this$formatToEpguInt");
            throw null;
        } else if (str2 != null) {
            try {
                LocalDate a = LocalDate.a(str, DateTimeFormatter.a(str2));
                DateTimeFormatter a2 = DateTimeFormatter.a("yyyyMMdd");
                if (a != null) {
                    a((Object) a2, "formatter");
                    String a3 = a2.a(a);
                    Intrinsics.a((Object) a3, "LocalDate.parse(this, Da…N\n            )\n        )");
                    return Integer.valueOf(Integer.parseInt(a3));
                }
                throw null;
            } catch (DateTimeException unused) {
                return null;
            }
        } else {
            Intrinsics.a("format");
            throw null;
        }
    }

    public static final BufferedSink a(Sink sink) {
        if (sink != null) {
            return new RealBufferedSink(sink);
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final void a(Closeable closeable, Throwable th) {
        if (closeable != null) {
            if (th == null) {
                closeable.close();
                return;
            }
            try {
                closeable.close();
            } catch (Throwable th2) {
                PlatformImplementations0.a.a(th, th2);
            }
        }
    }

    public static final <K, V> Map<K, V> a(Tuples<? extends K, ? extends V>... tuplesArr) {
        if (tuplesArr == null) {
            Intrinsics.a("pairs");
            throw null;
        } else if (tuplesArr.length <= 0) {
            return Maps.b;
        } else {
            LinkedHashMap linkedHashMap = new LinkedHashMap(a(tuplesArr.length));
            for (Tuples<? extends K, ? extends V> tuples : tuplesArr) {
                linkedHashMap.put(tuples.b, tuples.c);
            }
            return linkedHashMap;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.Instant, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static /* synthetic */ String a(Date date, String str, Locale locale, ZoneId zoneId, int i2) {
        if ((i2 & 1) != 0) {
            str = "dd.MM.yyyy";
        }
        if ((i2 & 2) != 0) {
            locale = Locale.getDefault();
            Intrinsics.a((Object) locale, "Locale.getDefault()");
        }
        if ((i2 & 4) != 0) {
            zoneId = ZoneId.i();
            Intrinsics.a((Object) zoneId, "ZoneId.systemDefault()");
        }
        if (date == null) {
            Intrinsics.a("$this$toStringFormat");
            throw null;
        } else if (str == null) {
            Intrinsics.a("pattern");
            throw null;
        } else if (locale == null) {
            Intrinsics.a("locale");
            throw null;
        } else if (zoneId != null) {
            Instant b = Instant.b(date.getTime());
            if (b != null) {
                a((Object) b, "instant");
                a((Object) zoneId, "zone");
                String a = ZonedDateTime.a(b.b, b.c, zoneId).b.a(DateTimeFormatter.a(str, locale));
                Intrinsics.a((Object) a, "localDateTime.format(Dat…Pattern(pattern, locale))");
                return a;
            }
            throw null;
        } else {
            Intrinsics.a("zoneId");
            throw null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object a(java.lang.Exception r4, n.l.Continuation<?> r5) {
        /*
            boolean r0 = r5 instanceof r.KotlinExtensions5
            if (r0 == 0) goto L_0x0013
            r0 = r5
            r.KotlinExtensions5 r0 = (r.KotlinExtensions5) r0
            int r1 = r0.f3168f
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.f3168f = r1
            goto L_0x0018
        L_0x0013:
            r.KotlinExtensions5 r0 = new r.KotlinExtensions5
            r0.<init>(r5)
        L_0x0018:
            java.lang.Object r5 = r0.f3167e
            n.l.d.Intrinsics r1 = n.l.d.Intrinsics.COROUTINE_SUSPENDED
            int r2 = r0.f3168f
            r3 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r3) goto L_0x0030
            java.lang.Object r4 = r0.g
            java.lang.Exception r4 = (java.lang.Exception) r4
            boolean r0 = r5 instanceof n.Result.a
            if (r0 == 0) goto L_0x0047
            n.Result$a r5 = (n.Result.a) r5
            java.lang.Throwable r4 = r5.b
            throw r4
        L_0x0030:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "call to 'resume' before 'invoke' with coroutine"
            r4.<init>(r5)
            throw r4
        L_0x0038:
            boolean r2 = r5 instanceof n.Result.a
            if (r2 != 0) goto L_0x0048
            r0.g = r4
            r0.f3168f = r3
            java.lang.Object r5 = kotlinx.coroutines.YieldKt.yield(r0)
            if (r5 != r1) goto L_0x0047
            return r1
        L_0x0047:
            throw r4
        L_0x0048:
            n.Result$a r5 = (n.Result.a) r5
            java.lang.Throwable r4 = r5.b
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: n.i.Collections.a(java.lang.Exception, n.l.Continuation):java.lang.Object");
    }

    public static final <T> void a(Appendable appendable, T t2, Functions0<? super T, ? extends CharSequence> functions0) {
        if (appendable == null) {
            Intrinsics.a("$this$appendElement");
            throw null;
        } else if (functions0 != null) {
            appendable.append((CharSequence) functions0.a(t2));
        } else {
            if (t2 != null ? t2 instanceof CharSequence : true) {
                appendable.append((CharSequence) t2);
            } else if (t2 instanceof Character) {
                appendable.append(((Character) t2).charValue());
            } else {
                appendable.append(String.valueOf(t2));
            }
        }
    }

    public static final <T> Class<T> a(KClass<T> kClass) {
        if (kClass != null) {
            Class a = ((ClassBasedDeclarationContainer) kClass).a();
            if (!a.isPrimitive()) {
                return a;
            }
            String name = a.getName();
            switch (name.hashCode()) {
                case -1325958191:
                    return name.equals("double") ? Double.class : a;
                case 104431:
                    if (name.equals("int")) {
                        return Integer.class;
                    }
                    return a;
                case 3039496:
                    if (name.equals("byte")) {
                        return Byte.class;
                    }
                    return a;
                case 3052374:
                    if (name.equals("char")) {
                        return Character.class;
                    }
                    return a;
                case 3327612:
                    if (name.equals("long")) {
                        return Long.class;
                    }
                    return a;
                case 3625364:
                    if (name.equals("void")) {
                        return Void.class;
                    }
                    return a;
                case 64711720:
                    if (name.equals("boolean")) {
                        return Boolean.class;
                    }
                    return a;
                case 97526364:
                    if (name.equals("float")) {
                        return Float.class;
                    }
                    return a;
                case 109413500:
                    if (name.equals("short")) {
                        return Short.class;
                    }
                    return a;
                default:
                    return a;
            }
        } else {
            Intrinsics.a("$this$javaObjectType");
            throw null;
        }
    }

    public static final boolean a(char c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c);
    }

    public static final <T> List<T> a(T... tArr) {
        if (tArr == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (tArr.length > 0) {
            return _Arrays.a(tArr);
        } else {
            return Collections2.b;
        }
    }

    public static final <T extends Comparable<?>> int a(Comparable comparable, Comparable comparable2) {
        if (comparable == comparable2) {
            return 0;
        }
        if (comparable == null) {
            return -1;
        }
        if (comparable2 == null) {
            return 1;
        }
        return comparable.compareTo(comparable2);
    }

    public static final <T extends Parcelable> T a(Bundle bundle) {
        if (bundle != null) {
            return bundle.getParcelable("EXTRA_SCREEN_DATA");
        }
        return null;
    }

    public static <T> T a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(outline.a(str, " must not be null"));
    }

    public static final boolean a(byte[] bArr, int i2, byte[] bArr2, int i3, int i4) {
        if (bArr == null) {
            Intrinsics.a("a");
            throw null;
        } else if (bArr2 != null) {
            for (int i5 = 0; i5 < i4; i5++) {
                if (bArr[i5 + i2] != bArr2[i5 + i3]) {
                    return false;
                }
            }
            return true;
        } else {
            Intrinsics.a("b");
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0082, code lost:
        if ((r14 - r9) != 0) goto L_0x0085;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.net.InetAddress a(java.lang.String r16, int r17, int r18) {
        /*
            r0 = r16
            r1 = r18
            r2 = 16
            byte[] r3 = new byte[r2]
            r4 = -1
            r5 = 0
            r6 = r17
            r7 = 0
            r8 = -1
            r9 = -1
        L_0x000f:
            r10 = 0
            if (r6 >= r1) goto L_0x00d2
            if (r7 != r2) goto L_0x0015
            return r10
        L_0x0015:
            int r11 = r6 + 2
            r12 = 255(0xff, float:3.57E-43)
            r13 = 4
            if (r11 > r1) goto L_0x0031
            java.lang.String r14 = "::"
            boolean r14 = n.r.Indent.a(r0, r14, r6, r5, r13)
            if (r14 == 0) goto L_0x0031
            if (r8 == r4) goto L_0x0027
            return r10
        L_0x0027:
            int r7 = r7 + 2
            r8 = r7
            if (r11 != r1) goto L_0x002e
            goto L_0x00d2
        L_0x002e:
            r9 = r11
            goto L_0x009f
        L_0x0031:
            if (r7 == 0) goto L_0x009e
            java.lang.String r11 = ":"
            boolean r11 = n.r.Indent.a(r0, r11, r6, r5, r13)
            if (r11 == 0) goto L_0x003f
            int r6 = r6 + 1
            goto L_0x009e
        L_0x003f:
            java.lang.String r11 = "."
            boolean r6 = n.r.Indent.a(r0, r11, r6, r5, r13)
            if (r6 == 0) goto L_0x009d
            int r6 = r7 + -2
            r11 = r6
        L_0x004a:
            if (r9 >= r1) goto L_0x0091
            if (r11 != r2) goto L_0x004f
            goto L_0x0096
        L_0x004f:
            if (r11 == r6) goto L_0x005c
            char r14 = r0.charAt(r9)
            r15 = 46
            if (r14 == r15) goto L_0x005a
            goto L_0x0096
        L_0x005a:
            int r9 = r9 + 1
        L_0x005c:
            r14 = r9
            r15 = 0
        L_0x005e:
            if (r14 >= r1) goto L_0x0080
            char r5 = r0.charAt(r14)
            r2 = 48
            if (r5 < r2) goto L_0x0080
            r4 = 57
            if (r5 <= r4) goto L_0x006d
            goto L_0x0080
        L_0x006d:
            if (r15 != 0) goto L_0x0072
            if (r9 == r14) goto L_0x0072
            goto L_0x0096
        L_0x0072:
            int r15 = r15 * 10
            int r15 = r15 + r5
            int r15 = r15 - r2
            if (r15 <= r12) goto L_0x0079
            goto L_0x0096
        L_0x0079:
            int r14 = r14 + 1
            r2 = 16
            r4 = -1
            r5 = 0
            goto L_0x005e
        L_0x0080:
            int r2 = r14 - r9
            if (r2 != 0) goto L_0x0085
            goto L_0x0096
        L_0x0085:
            int r2 = r11 + 1
            byte r4 = (byte) r15
            r3[r11] = r4
            r11 = r2
            r9 = r14
            r2 = 16
            r4 = -1
            r5 = 0
            goto L_0x004a
        L_0x0091:
            int r6 = r6 + r13
            if (r11 != r6) goto L_0x0096
            r0 = 1
            goto L_0x0097
        L_0x0096:
            r0 = 0
        L_0x0097:
            if (r0 != 0) goto L_0x009a
            return r10
        L_0x009a:
            int r7 = r7 + 2
            goto L_0x00d2
        L_0x009d:
            return r10
        L_0x009e:
            r9 = r6
        L_0x009f:
            r6 = r9
            r2 = 0
        L_0x00a1:
            if (r6 >= r1) goto L_0x00b5
            char r4 = r0.charAt(r6)
            int r4 = o.m0.Util.a(r4)
            r5 = -1
            if (r4 != r5) goto L_0x00af
            goto L_0x00b5
        L_0x00af:
            int r2 = r2 << 4
            int r2 = r2 + r4
            int r6 = r6 + 1
            goto L_0x00a1
        L_0x00b5:
            int r4 = r6 - r9
            if (r4 == 0) goto L_0x00d1
            if (r4 <= r13) goto L_0x00bc
            goto L_0x00d1
        L_0x00bc:
            int r4 = r7 + 1
            int r5 = r2 >>> 8
            r5 = r5 & r12
            byte r5 = (byte) r5
            r3[r7] = r5
            int r7 = r4 + 1
            r2 = r2 & 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2
            r3[r4] = r2
            r2 = 16
            r4 = -1
            r5 = 0
            goto L_0x000f
        L_0x00d1:
            return r10
        L_0x00d2:
            r0 = 16
            if (r7 == r0) goto L_0x00e9
            r1 = -1
            if (r8 != r1) goto L_0x00da
            return r10
        L_0x00da:
            int r1 = r7 - r8
            int r2 = 16 - r1
            java.lang.System.arraycopy(r3, r8, r3, r2, r1)
            int r2 = 16 - r7
            int r2 = r2 + r8
            r0 = 0
            byte r0 = (byte) r0
            java.util.Arrays.fill(r3, r8, r2, r0)
        L_0x00e9:
            java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: n.i.Collections.a(java.lang.String, int, int):java.net.InetAddress");
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        if (obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }

    public static final <T> Object a(Call call, Continuation continuation) {
        CancellableContinuation cancellableContinuationImpl = new CancellableContinuationImpl(a(continuation), 1);
        CancellableContinuation cancellableContinuation = cancellableContinuationImpl;
        cancellableContinuation.invokeOnCancellation(new KotlinExtensions2(call));
        call.a(new KotlinExtensions(cancellableContinuation));
        Object result = cancellableContinuationImpl.getResult();
        n.l.d.Intrinsics intrinsics = n.l.d.Intrinsics.COROUTINE_SUSPENDED;
        return result;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String a(Reader reader) {
        if (reader != null) {
            StringWriter stringWriter = new StringWriter();
            char[] cArr = new char[8192];
            int read = reader.read(cArr);
            while (read >= 0) {
                stringWriter.write(cArr, 0, read);
                read = reader.read(cArr);
            }
            String stringWriter2 = stringWriter.toString();
            Intrinsics.a((Object) stringWriter2, "buffer.toString()");
            return stringWriter2;
        }
        Intrinsics.a("$this$readText");
        throw null;
    }

    public static final <T> boolean a(Collection collection, Iterable iterable) {
        if (collection == null) {
            Intrinsics.a("$this$addAll");
            throw null;
        } else if (iterable == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        } else {
            boolean z = false;
            for (Object add : iterable) {
                if (collection.add(add)) {
                    z = true;
                }
            }
            return z;
        }
    }

    public static final int a(int i2) {
        if (i2 < 3) {
            return i2 + 1;
        }
        if (i2 < 1073741824) {
            return (i2 / 3) + i2;
        }
        return Integer.MAX_VALUE;
    }

    public static final <T> Continuation<T> a(Continuation<? super T> continuation) {
        ContinuationImpl1 continuationImpl1 = null;
        if (continuation != null) {
            if (continuation instanceof ContinuationImpl1) {
                continuationImpl1 = continuation;
            }
            ContinuationImpl1 continuationImpl12 = continuationImpl1;
            if (continuationImpl12 != null && (continuation = continuationImpl12.c) == null) {
                ContinuationInterceptor continuationInterceptor = (ContinuationInterceptor) continuationImpl12.c().a(ContinuationInterceptor.a);
                if (continuationInterceptor == null || (continuation = continuationInterceptor.b(continuationImpl12)) == null) {
                    continuation = continuationImpl12;
                }
                continuationImpl12.c = continuation;
            }
            return continuation;
        }
        Intrinsics.a("$this$intercepted");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.OutputStream, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final Sink a(Socket socket) {
        if (socket != null) {
            Okio2 okio2 = new Okio2(socket);
            OutputStream outputStream = socket.getOutputStream();
            Intrinsics.a((Object) outputStream, "getOutputStream()");
            return new AsyncTimeout0(okio2, new Okio1(outputStream, okio2));
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final <T> boolean a(Collection collection, Object[] objArr) {
        if (collection == null) {
            Intrinsics.a("$this$addAll");
            throw null;
        } else if (objArr != null) {
            return collection.addAll(_Arrays.a(objArr));
        } else {
            Intrinsics.a("elements");
            throw null;
        }
    }

    public static Sink a(File file, boolean z, int i2) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if (file != null) {
            return new Okio1(new FileOutputStream(file, z), new Timeout());
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static final Sink a(File file) {
        if (file != null) {
            return new Okio1(new FileOutputStream(file, true), new Timeout());
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final boolean a(AssertionError assertionError) {
        if (assertionError == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (assertionError.getCause() == null) {
            return false;
        } else {
            String message = assertionError.getMessage();
            return message != null ? Indent.a(message, "getsockname failed", false, 2) : false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String a(String str, int i2) {
        if (str != null) {
            if (i2 >= 0) {
                int length = str.length();
                if (i2 > length) {
                    i2 = length;
                }
                String substring = str.substring(i2);
                Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                return substring;
            }
            throw new IllegalArgumentException(outline.b("Requested character count ", i2, " is less than zero.").toString());
        }
        Intrinsics.a("$this$drop");
        throw null;
    }

    public static int a(long j2) {
        if (j2 <= 2147483647L && j2 >= -2147483648L) {
            return (int) j2;
        }
        throw new ArithmeticException("Calculation overflows an int: " + j2);
    }

    public static int a(long j2, int i2) {
        long j3 = (long) i2;
        return (int) (((j2 % j3) + j3) % j3);
    }

    public static TemporalAdjuster a(DayOfWeek dayOfWeek) {
        return new TemporalAdjusters0(0, dayOfWeek, null);
    }

    public static final void a() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.StringBuilder, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final CharSequence a(CharSequence charSequence) {
        if (charSequence != null) {
            StringBuilder reverse = new StringBuilder(charSequence).reverse();
            Intrinsics.a((Object) reverse, "StringBuilder(this).reverse()");
            return reverse;
        }
        Intrinsics.a("$this$reversed");
        throw null;
    }

    public static final <T, R> Sequence<R> a(Sequence sequence, Functions0 functions0) {
        if (sequence == null) {
            Intrinsics.a("$this$map");
            throw null;
        } else if (functions0 != null) {
            return new Sequences2(sequence, functions0);
        } else {
            Intrinsics.a("transform");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static /* synthetic */ String a(Sequence sequence, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i2, CharSequence charSequence4, Functions0 functions0, int i3) {
        if ((i3 & 1) != 0) {
            charSequence = ", ";
        }
        if ((i3 & 2) != 0) {
            charSequence2 = "";
        }
        if ((i3 & 4) != 0) {
            charSequence3 = "";
        }
        if ((i3 & 8) != 0) {
            i2 = -1;
        }
        if ((i3 & 16) != 0) {
            charSequence4 = "...";
        }
        if ((i3 & 32) != 0) {
            functions0 = null;
        }
        if (sequence == null) {
            Intrinsics.a("$this$joinToString");
            throw null;
        } else if (charSequence == null) {
            Intrinsics.a("separator");
            throw null;
        } else if (charSequence2 == null) {
            Intrinsics.a("prefix");
            throw null;
        } else if (charSequence3 == null) {
            Intrinsics.a("postfix");
            throw null;
        } else if (charSequence4 != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(charSequence2);
            int i4 = 0;
            for (Object next : sequence) {
                i4++;
                if (i4 > 1) {
                    sb.append(charSequence);
                }
                if (i2 >= 0 && i4 > i2) {
                    break;
                }
                a(sb, next, functions0);
            }
            if (i2 >= 0 && i4 > i2) {
                sb.append(charSequence4);
            }
            sb.append(charSequence3);
            String sb2 = sb.toString();
            Intrinsics.a((Object) sb2, "joinTo(StringBuilder(), …ed, transform).toString()");
            return sb2;
        } else {
            Intrinsics.a("truncated");
            throw null;
        }
    }
}
