package n.i;

import j.a.a.a.outline;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import n.n.c.Intrinsics;

/* compiled from: AbstractList.kt */
public abstract class AbstractList<E> extends AbstractCollection<E> implements List<E>, n.n.c.t.a {

    /* compiled from: AbstractList.kt */
    public class a implements Iterator<E>, n.n.c.t.a {
        public int b;

        public a() {
        }

        public boolean hasNext() {
            return this.b < AbstractList.this.c();
        }

        public E next() {
            if (hasNext()) {
                AbstractList abstractList = AbstractList.this;
                int i2 = this.b;
                this.b = i2 + 1;
                return abstractList.get(i2);
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* compiled from: AbstractList.kt */
    public class b extends AbstractList<E>.defpackage.a implements ListIterator<E>, n.n.c.t.a {
        public b(int i2) {
            super();
            int c = AbstractList.this.c();
            if (i2 < 0 || i2 > c) {
                throw new IndexOutOfBoundsException(outline.a("index: ", i2, ", size: ", c));
            }
            super.b = i2;
        }

        public void add(E e2) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public boolean hasPrevious() {
            return super.b > 0;
        }

        public int nextIndex() {
            return super.b;
        }

        public E previous() {
            if (hasPrevious()) {
                AbstractList abstractList = AbstractList.this;
                int i2 = super.b - 1;
                super.b = i2;
                return abstractList.get(i2);
            }
            throw new NoSuchElementException();
        }

        public int previousIndex() {
            return super.b - 1;
        }

        public void set(E e2) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* compiled from: AbstractList.kt */
    public static final class c<E> extends AbstractList<E> implements RandomAccess {
        public int b;
        public final AbstractList<E> c;
        public final int d;

        public c(AbstractList<? extends E> abstractList, int i2, int i3) {
            if (abstractList != null) {
                this.c = super;
                this.d = i2;
                int c2 = abstractList.c();
                if (i2 < 0 || i3 > c2) {
                    throw new IndexOutOfBoundsException("fromIndex: " + i2 + ", toIndex: " + i3 + ", size: " + c2);
                } else if (i2 <= i3) {
                    this.b = i3 - this.d;
                } else {
                    throw new IllegalArgumentException(outline.a("fromIndex: ", i2, " > toIndex: ", i3));
                }
            } else {
                Intrinsics.a("list");
                throw null;
            }
        }

        public int c() {
            return this.b;
        }

        public E get(int i2) {
            int i3 = this.b;
            if (i2 >= 0 && i2 < i3) {
                return this.c.get(this.d + i2);
            }
            throw new IndexOutOfBoundsException(outline.a("index: ", i2, ", size: ", i3));
        }
    }

    public void add(int i2, E e2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(int i2, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        Collection collection = (Collection) obj;
        if (collection != null) {
            if (size() != collection.size()) {
                return false;
            }
            Iterator it = collection.iterator();
            for (Object a2 : this) {
                if (!Intrinsics.a(a2, it.next())) {
                }
            }
            return true;
            return false;
        }
        Intrinsics.a("other");
        throw null;
    }

    public abstract E get(int i2);

    public int hashCode() {
        int i2 = 1;
        Iterator it = iterator();
        while (it.hasNext()) {
            Object next = it.next();
            i2 = (i2 * 31) + (next != null ? next.hashCode() : 0);
        }
        return i2;
    }

    public int indexOf(Object obj) {
        int i2 = 0;
        for (Object a2 : this) {
            if (Intrinsics.a(a2, obj)) {
                return i2;
            }
            i2++;
        }
        return -1;
    }

    public Iterator<E> iterator() {
        return new a();
    }

    public int lastIndexOf(Object obj) {
        ListIterator listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (Intrinsics.a(listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    public ListIterator<E> listIterator() {
        return new b(0);
    }

    public E remove(int i2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public E set(int i2, E e2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public List<E> subList(int i2, int i3) {
        return new c(this, i2, i3);
    }

    public ListIterator<E> listIterator(int i2) {
        return new b(i2);
    }
}
