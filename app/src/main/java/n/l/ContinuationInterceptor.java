package n.l;

import n.l.CoroutineContext;

/* compiled from: ContinuationInterceptor.kt */
public interface ContinuationInterceptor extends CoroutineContext.a {
    public static final a a = a.a;

    /* compiled from: ContinuationInterceptor.kt */
    public static final class a implements CoroutineContext.b<b> {
        public static final /* synthetic */ a a = new a();
    }

    void a(Continuation<?> continuation);

    <T> Continuation<T> b(Continuation<? super T> continuation);
}
