package n.l;

import n.l.c;

/* compiled from: CoroutineContext.kt */
public interface CoroutineContext {

    /* compiled from: CoroutineContext.kt */
    public interface a extends CoroutineContext {
    }

    /* compiled from: CoroutineContext.kt */
    public interface b<E extends c.a> {
    }

    <E extends c.a> E a(b<E> bVar);
}
