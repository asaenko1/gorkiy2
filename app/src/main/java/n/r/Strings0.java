package n.r;

import n.Tuples;
import n.n.b.Functions1;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: Strings.kt */
public final class Strings0 extends j implements Functions1<CharSequence, Integer, Tuples<? extends Integer, ? extends Integer>> {
    public final /* synthetic */ char[] c;
    public final /* synthetic */ boolean d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Strings0(char[] cArr, boolean z) {
        super(2);
        this.c = cArr;
        this.d = z;
    }

    public Object a(Object obj, Object obj2) {
        CharSequence charSequence = (CharSequence) obj;
        int intValue = ((Number) obj2).intValue();
        if (charSequence != null) {
            int a = Indent.a(charSequence, this.c, intValue, this.d);
            if (a < 0) {
                return null;
            }
            return new Tuples(Integer.valueOf(a), 1);
        }
        Intrinsics.a("$receiver");
        throw null;
    }
}
