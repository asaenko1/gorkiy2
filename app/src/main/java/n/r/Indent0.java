package n.r;

import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: Indent.kt */
public final class Indent0 extends j implements Functions0<String, String> {
    public static final Indent0 c = new Indent0();

    public Indent0() {
        super(1);
    }

    public Object a(Object obj) {
        String str = (String) obj;
        if (str != null) {
            return str;
        }
        Intrinsics.a("line");
        throw null;
    }
}
