package org.bouncycastle.util.encoders;

public class EncoderException extends IllegalStateException {
    public Throwable b;

    public EncoderException(String str, Throwable th) {
        super(str);
        this.b = th;
    }

    public Throwable getCause() {
        return this.b;
    }
}
