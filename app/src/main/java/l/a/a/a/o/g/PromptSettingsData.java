package l.a.a.a.o.g;

public class PromptSettingsData {
    public final String a;
    public final String b;
    public final String c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2668e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f2669f;
    public final String g;

    public PromptSettingsData(String str, String str2, String str3, boolean z, String str4, boolean z2, String str5) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = z;
        this.f2668e = str4;
        this.f2669f = z2;
        this.g = str5;
    }
}
