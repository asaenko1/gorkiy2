package l.a.a.a.o.c;

import android.annotation.TargetApi;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {
    public static final int a;
    public static final int b;
    public static final int c;

    public static final class a implements ThreadFactory {
        public final int b;

        public a(int i2) {
            this.b = i2;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.b);
            thread.setName("Queue");
            return thread;
        }
    }

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        a = availableProcessors;
        b = availableProcessors + 1;
        c = (availableProcessors * 2) + 1;
    }

    public <T extends Runnable & b & l & i> PriorityThreadPoolExecutor(int i2, int i3, long j2, TimeUnit timeUnit, DependencyPriorityBlockingQueue<T> dependencyPriorityBlockingQueue, ThreadFactory threadFactory) {
        super(i2, i3, j2, timeUnit, dependencyPriorityBlockingQueue, threadFactory);
        prestartAllCoreThreads();
    }

    public void afterExecute(Runnable runnable, Throwable th) {
        Task task = (Task) runnable;
        task.setFinished(true);
        task.setError(th);
        ((DependencyPriorityBlockingQueue) super.getQueue()).c();
        super.afterExecute(runnable, th);
    }

    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (PriorityTask.isProperDelegate(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(new PriorityFutureTask(runnable, null));
        }
    }

    public BlockingQueue getQueue() {
        return (DependencyPriorityBlockingQueue) super.getQueue();
    }

    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t2) {
        return new PriorityFutureTask(runnable, t2);
    }

    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new PriorityFutureTask(callable);
    }
}
