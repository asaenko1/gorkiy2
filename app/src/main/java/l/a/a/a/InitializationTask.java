package l.a.a.a;

import l.a.a.a.o.b.TimingMetric;
import l.a.a.a.o.c.Priority;
import l.a.a.a.o.c.PriorityAsyncTask;

public class InitializationTask<Result> extends PriorityAsyncTask<Void, Void, Result> {

    /* renamed from: p  reason: collision with root package name */
    public final Kit<Result> f2622p;

    public InitializationTask(Kit<Result> kit) {
        this.f2622p = kit;
    }

    public final TimingMetric a(String str) {
        TimingMetric timingMetric = new TimingMetric(this.f2622p.getIdentifier() + "." + str, "KitInitialization");
        timingMetric.a();
        return timingMetric;
    }

    public Priority getPriority() {
        return Priority.HIGH;
    }
}
