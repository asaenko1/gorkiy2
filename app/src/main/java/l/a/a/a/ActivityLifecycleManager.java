package l.a.a.a;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import l.a.a.a.ActivityLifecycleManager0;

public class ActivityLifecycleManager implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ ActivityLifecycleManager0.b b;

    public ActivityLifecycleManager(ActivityLifecycleManager0.a aVar, ActivityLifecycleManager0.b bVar) {
        this.b = bVar;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        this.b.onActivityCreated(activity, bundle);
    }

    public void onActivityDestroyed(Activity activity) {
        this.b.onActivityDestroyed(activity);
    }

    public void onActivityPaused(Activity activity) {
        this.b.onActivityPaused(activity);
    }

    public void onActivityResumed(Activity activity) {
        this.b.onActivityResumed(activity);
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.b.onActivitySaveInstanceState(activity, bundle);
    }

    public void onActivityStarted(Activity activity) {
        this.b.onActivityStarted(activity);
    }

    public void onActivityStopped(Activity activity) {
        this.b.onActivityStopped(activity);
    }
}
