package l.b;

import j.c.a.a.c.n.c;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.c.FuseToObservable;
import l.b.u.d.BlockingMultiObserver;
import l.b.u.e.d.SingleJust;
import l.b.u.e.d.SingleMap;
import l.b.u.e.d.SingleOnErrorReturn;
import l.b.u.e.d.SingleToObservable;
import l.b.u.h.ExceptionHelper;

public abstract class Single<T> implements SingleSource<T> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [T, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static <T> Single<T> a(T t2) {
        ObjectHelper.a((Object) t2, "item is null");
        return new SingleJust(t2);
    }

    public final T b() {
        BlockingMultiObserver blockingMultiObserver = new BlockingMultiObserver();
        a((SingleObserver) blockingMultiObserver);
        if (blockingMultiObserver.getCount() != 0) {
            try {
                blockingMultiObserver.await();
            } catch (InterruptedException e2) {
                blockingMultiObserver.f2679e = true;
                Disposable disposable = blockingMultiObserver.d;
                if (disposable != null) {
                    disposable.f();
                }
                throw ExceptionHelper.a(e2);
            }
        }
        Throwable th = blockingMultiObserver.c;
        if (th == null) {
            return blockingMultiObserver.b;
        }
        throw ExceptionHelper.a(th);
    }

    public abstract void b(SingleObserver<? super T> singleObserver);

    public final Observable<T> c() {
        if (this instanceof FuseToObservable) {
            return ((FuseToObservable) this).a();
        }
        return new SingleToObservable(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Function<? super T, ? extends R>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final <R> Single<R> a(Function<? super T, ? extends R> function) {
        ObjectHelper.a((Object) function, "mapper is null");
        return new SingleMap(this, function);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.SingleObserver<? super T>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final void a(SingleObserver<? super T> singleObserver) {
        ObjectHelper.a((Object) singleObserver, "observer is null");
        ObjectHelper.a((Object) singleObserver, "The RxJavaPlugins.onSubscribe hook returned a null SingleObserver. Please check the handler provided to RxJavaPlugins.setOnSingleSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
        try {
            b(singleObserver);
        } catch (NullPointerException e2) {
            throw e2;
        } catch (Throwable th) {
            c.c(th);
            NullPointerException nullPointerException = new NullPointerException("subscribeActual failed");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Function<java.lang.Throwable, ? extends T>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final Single<T> b(Function<Throwable, ? extends T> function) {
        ObjectHelper.a((Object) function, "resumeFunction is null");
        return new SingleOnErrorReturn(this, function, null);
    }
}
