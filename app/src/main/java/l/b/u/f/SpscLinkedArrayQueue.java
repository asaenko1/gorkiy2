package l.b.u.f;

import com.crashlytics.android.core.CodedOutputStream;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;
import l.b.u.c.SimplePlainQueue;

public final class SpscLinkedArrayQueue<T> implements SimplePlainQueue<T> {

    /* renamed from: j  reason: collision with root package name */
    public static final int f2753j = Integer.getInteger("jctools.spsc.max.lookahead.step", (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).intValue();

    /* renamed from: k  reason: collision with root package name */
    public static final Object f2754k = new Object();
    public final AtomicLong b = new AtomicLong();
    public int c;
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2755e;

    /* renamed from: f  reason: collision with root package name */
    public AtomicReferenceArray<Object> f2756f;
    public final int g;
    public AtomicReferenceArray<Object> h;

    /* renamed from: i  reason: collision with root package name */
    public final AtomicLong f2757i = new AtomicLong();

    public SpscLinkedArrayQueue(int i2) {
        int d2 = c.d(Math.max(8, i2));
        int i3 = d2 - 1;
        AtomicReferenceArray<Object> atomicReferenceArray = new AtomicReferenceArray<>(d2 + 1);
        this.f2756f = atomicReferenceArray;
        this.f2755e = i3;
        this.c = Math.min(d2 / 4, f2753j);
        this.h = atomicReferenceArray;
        this.g = i3;
        this.d = (long) (i3 - 1);
        this.b.lazySet(0);
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    public boolean isEmpty() {
        return this.b.get() == this.f2757i.get();
    }

    public boolean offer(T t2) {
        if (t2 != null) {
            AtomicReferenceArray<Object> atomicReferenceArray = this.f2756f;
            long j2 = this.b.get();
            int i2 = this.f2755e;
            int i3 = ((int) j2) & i2;
            if (j2 < this.d) {
                atomicReferenceArray.lazySet(i3, t2);
                this.b.lazySet(j2 + 1);
                return true;
            }
            long j3 = ((long) this.c) + j2;
            if (atomicReferenceArray.get(((int) j3) & i2) == null) {
                this.d = j3 - 1;
                atomicReferenceArray.lazySet(i3, t2);
                this.b.lazySet(j2 + 1);
                return true;
            }
            long j4 = j2 + 1;
            if (atomicReferenceArray.get(((int) j4) & i2) == null) {
                atomicReferenceArray.lazySet(i3, t2);
                this.b.lazySet(j4);
                return true;
            }
            AtomicReferenceArray<Object> atomicReferenceArray2 = new AtomicReferenceArray<>(atomicReferenceArray.length());
            this.f2756f = atomicReferenceArray2;
            this.d = (((long) i2) + j2) - 1;
            atomicReferenceArray2.lazySet(i3, t2);
            atomicReferenceArray.lazySet(atomicReferenceArray.length() - 1, atomicReferenceArray2);
            atomicReferenceArray.lazySet(i3, f2754k);
            this.b.lazySet(j4);
            return true;
        }
        throw new NullPointerException("Null is not a valid element");
    }

    public T poll() {
        AtomicReferenceArray<Object> atomicReferenceArray = this.h;
        long j2 = this.f2757i.get();
        int i2 = this.g;
        int i3 = ((int) j2) & i2;
        T t2 = atomicReferenceArray.get(i3);
        boolean z = t2 == f2754k;
        if (t2 != null && !z) {
            atomicReferenceArray.lazySet(i3, null);
            this.f2757i.lazySet(j2 + 1);
            return t2;
        } else if (!z) {
            return null;
        } else {
            int i4 = i2 + 1;
            AtomicReferenceArray<Object> atomicReferenceArray2 = (AtomicReferenceArray) atomicReferenceArray.get(i4);
            atomicReferenceArray.lazySet(i4, null);
            this.h = atomicReferenceArray2;
            T t3 = atomicReferenceArray2.get(i3);
            if (t3 != null) {
                atomicReferenceArray2.lazySet(i3, null);
                this.f2757i.lazySet(j2 + 1);
            }
            return t3;
        }
    }
}
