package l.b.u.f;

import com.crashlytics.android.core.CodedOutputStream;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;
import l.b.u.c.SimplePlainQueue;

public final class SpscArrayQueue<E> extends AtomicReferenceArray<E> implements SimplePlainQueue<E> {
    public static final Integer g = Integer.getInteger("jctools.spsc.max.lookahead.step", (int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
    public final int b = (length() - 1);
    public final AtomicLong c = new AtomicLong();
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public final AtomicLong f2751e = new AtomicLong();

    /* renamed from: f  reason: collision with root package name */
    public final int f2752f;

    public SpscArrayQueue(int i2) {
        super(c.d(i2));
        this.f2752f = Math.min(i2 / 4, g.intValue());
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    public boolean isEmpty() {
        return this.c.get() == this.f2751e.get();
    }

    public boolean offer(E e2) {
        if (e2 != null) {
            int i2 = this.b;
            long j2 = this.c.get();
            int i3 = ((int) j2) & i2;
            if (j2 >= this.d) {
                long j3 = ((long) this.f2752f) + j2;
                if (get(i2 & ((int) j3)) == null) {
                    this.d = j3;
                } else if (get(i3) != null) {
                    return false;
                }
            }
            lazySet(i3, e2);
            this.c.lazySet(j2 + 1);
            return true;
        }
        throw new NullPointerException("Null is not a valid element");
    }

    public E poll() {
        long j2 = this.f2751e.get();
        int i2 = ((int) j2) & this.b;
        E e2 = get(i2);
        if (e2 == null) {
            return null;
        }
        this.f2751e.lazySet(j2 + 1);
        lazySet(i2, null);
        return e2;
    }
}
