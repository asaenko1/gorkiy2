package l.b.u.g;

import j.c.a.a.c.n.c;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Scheduler;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;

public final class SingleScheduler extends Scheduler {
    public static final RxThreadFactory b = new RxThreadFactory("RxSingleScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.single-priority", 5).intValue())), true);
    public static final ScheduledExecutorService c;
    public final AtomicReference<ScheduledExecutorService> a;

    public static final class a extends Scheduler.b {
        public final ScheduledExecutorService b;
        public final CompositeDisposable c = new CompositeDisposable();
        public volatile boolean d;

        public a(ScheduledExecutorService scheduledExecutorService) {
            this.b = scheduledExecutorService;
        }

        /* JADX WARN: Type inference failed for: r0v2, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable, java.util.concurrent.Callable] */
        /* JADX WARN: Type inference failed for: r4v7, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX WARN: Type inference failed for: r4v10, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [java.lang.Runnable, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
            Future future;
            if (this.d) {
                return EmptyDisposable.INSTANCE;
            }
            ObjectHelper.a((Object) runnable, "run is null");
            ? scheduledRunnable = new ScheduledRunnable(runnable, this.c);
            this.c.c(scheduledRunnable);
            if (j2 <= 0) {
                try {
                    future = this.b.submit((Callable) scheduledRunnable);
                } catch (RejectedExecutionException e2) {
                    f();
                    c.b((Throwable) e2);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                future = this.b.schedule((Callable) scheduledRunnable, j2, timeUnit);
            }
            scheduledRunnable.a(future);
            return scheduledRunnable;
        }

        public void f() {
            if (!this.d) {
                this.d = true;
                this.c.f();
            }
        }

        public boolean g() {
            return this.d;
        }
    }

    static {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(0);
        c = newScheduledThreadPool;
        newScheduledThreadPool.shutdown();
    }

    public SingleScheduler() {
        RxThreadFactory rxThreadFactory = b;
        AtomicReference<ScheduledExecutorService> atomicReference = new AtomicReference<>();
        this.a = atomicReference;
        atomicReference.lazySet(SchedulerPoolFactory.a(rxThreadFactory));
    }

    public Scheduler.b a() {
        return new a(this.a.get());
    }

    /* JADX WARN: Type inference failed for: r4v8, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
        Future future;
        ObjectHelper.a((Object) runnable, "run is null");
        ScheduledDirectTask scheduledDirectTask = new ScheduledDirectTask(runnable);
        if (j2 <= 0) {
            try {
                future = this.a.get().submit(scheduledDirectTask);
            } catch (RejectedExecutionException e2) {
                c.b((Throwable) e2);
                return EmptyDisposable.INSTANCE;
            }
        } else {
            future = this.a.get().schedule(scheduledDirectTask, j2, timeUnit);
        }
        scheduledDirectTask.a(future);
        return scheduledDirectTask;
    }
}
