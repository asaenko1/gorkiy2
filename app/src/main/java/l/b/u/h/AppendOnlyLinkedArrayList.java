package l.b.u.h;

import l.b.t.Predicate;

public class AppendOnlyLinkedArrayList<T> {
    public final int a;
    public final Object[] b;
    public Object[] c;
    public int d;

    public interface a<T> extends Predicate<T> {
    }

    public AppendOnlyLinkedArrayList(int i2) {
        this.a = i2;
        Object[] objArr = new Object[(i2 + 1)];
        this.b = objArr;
        this.c = objArr;
    }

    public void a(T t2) {
        int i2 = this.a;
        int i3 = this.d;
        if (i3 == i2) {
            Object[] objArr = new Object[(i2 + 1)];
            this.c[i2] = objArr;
            this.c = objArr;
            i3 = 0;
        }
        this.c[i3] = t2;
        this.d = i3 + 1;
    }
}
