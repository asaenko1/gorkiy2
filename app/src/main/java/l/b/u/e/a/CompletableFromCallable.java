package l.b.u.e.a;

import j.c.a.a.c.n.c;
import java.util.concurrent.Callable;
import l.b.Completable;
import l.b.CompletableObserver;
import l.b.s.RunnableDisposable;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;

public final class CompletableFromCallable extends Completable {
    public final Callable<?> a;

    public CompletableFromCallable(Callable<?> callable) {
        this.a = callable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void b(CompletableObserver completableObserver) {
        Runnable runnable = Functions.b;
        ObjectHelper.a((Object) runnable, "run is null");
        RunnableDisposable runnableDisposable = new RunnableDisposable(runnable);
        completableObserver.a(runnableDisposable);
        try {
            this.a.call();
            if (!runnableDisposable.g()) {
                completableObserver.a();
            }
        } catch (Throwable th) {
            c.c(th);
            if (!runnableDisposable.g()) {
                completableObserver.a(th);
            } else {
                c.b(th);
            }
        }
    }
}
