package l.b.u.e.c;

import j.a.a.a.outline;
import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.b.ObjectHelper;
import l.b.u.d.BasicQueueDisposable;

public final class ObservableFromArray<T> extends Observable<T> {
    public final T[] b;

    public static final class a<T> extends BasicQueueDisposable<T> {
        public final Observer<? super T> b;
        public final T[] c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2733e;

        /* renamed from: f  reason: collision with root package name */
        public volatile boolean f2734f;

        public a(Observer<? super T> observer, T[] tArr) {
            this.b = observer;
            this.c = tArr;
        }

        public int a(int i2) {
            if ((i2 & 1) == 0) {
                return 0;
            }
            this.f2733e = true;
            return 1;
        }

        public void clear() {
            this.d = this.c.length;
        }

        public void f() {
            this.f2734f = true;
        }

        public boolean g() {
            return this.f2734f;
        }

        public boolean isEmpty() {
            return this.d == this.c.length;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [T, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public T poll() {
            int i2 = this.d;
            T[] tArr = this.c;
            if (i2 == tArr.length) {
                return null;
            }
            this.d = i2 + 1;
            T t2 = tArr[i2];
            ObjectHelper.a((Object) t2, "The array element is null");
            return t2;
        }
    }

    public ObservableFromArray(T[] tArr) {
        this.b = tArr;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFromArray$a] */
    public void b(Observer<? super T> observer) {
        ? aVar = new a(observer, this.b);
        observer.a((Disposable) aVar);
        if (!aVar.f2733e) {
            T[] tArr = aVar.c;
            int length = tArr.length;
            int i2 = 0;
            while (i2 < length && !aVar.f2734f) {
                T t2 = tArr[i2];
                if (t2 == null) {
                    aVar.b.a(new NullPointerException(outline.b("The element at index ", i2, " is null")));
                    return;
                } else {
                    aVar.b.b(t2);
                    i2++;
                }
            }
            if (!aVar.f2734f) {
                aVar.b.a();
            }
        }
    }
}
