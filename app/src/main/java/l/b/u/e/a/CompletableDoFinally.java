package l.b.u.e.a;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicInteger;
import l.b.Completable;
import l.b.CompletableObserver;
import l.b.CompletableSource;
import l.b.s.Disposable;
import l.b.t.Action;
import l.b.u.a.DisposableHelper;

public final class CompletableDoFinally extends Completable {
    public final CompletableSource a;
    public final Action b;

    public CompletableDoFinally(CompletableSource completableSource, Action action) {
        this.a = completableSource;
        this.b = action;
    }

    public void b(CompletableObserver completableObserver) {
        this.a.a(new a(completableObserver, this.b));
    }

    public static final class a extends AtomicInteger implements CompletableObserver, Disposable {
        public final CompletableObserver b;
        public final Action c;
        public Disposable d;

        public a(CompletableObserver completableObserver, Action action) {
            this.b = completableObserver;
            this.c = action;
        }

        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.b.a(this);
            }
        }

        public void b() {
            if (compareAndSet(0, 1)) {
                try {
                    this.c.run();
                } catch (Throwable th) {
                    c.c(th);
                    c.b(th);
                }
            }
        }

        public void f() {
            this.d.f();
            b();
        }

        public boolean g() {
            return this.d.g();
        }

        public void a(Throwable th) {
            this.b.a(th);
            b();
        }

        public void a() {
            this.b.a();
            b();
        }
    }
}
