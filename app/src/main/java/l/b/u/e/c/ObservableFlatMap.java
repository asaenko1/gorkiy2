package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;
import l.b.u.c.QueueDisposable;
import l.b.u.c.SimplePlainQueue;
import l.b.u.c.SimpleQueue;
import l.b.u.f.SpscLinkedArrayQueue;
import l.b.u.h.AtomicThrowable;
import l.b.u.h.ExceptionHelper;

public final class ObservableFlatMap<T, U> extends AbstractObservableWithUpstream<T, U> {
    public final Function<? super T, ? extends ObservableSource<? extends U>> c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2708e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2709f;

    public ObservableFlatMap(ObservableSource<T> observableSource, Function<? super T, ? extends ObservableSource<? extends U>> function, boolean z, int i2, int i3) {
        super(observableSource);
        this.c = function;
        this.d = z;
        this.f2708e = i2;
        this.f2709f = i3;
    }

    public void b(Observer<? super U> observer) {
        if (!c.a(super.b, observer, this.c)) {
            super.b.a(new b(observer, this.c, this.d, this.f2708e, this.f2709f));
        }
    }

    public static final class b<T, U> extends AtomicInteger implements l.b.s.b, Observer<T> {

        /* renamed from: r  reason: collision with root package name */
        public static final a<?, ?>[] f2712r = new a[0];

        /* renamed from: s  reason: collision with root package name */
        public static final a<?, ?>[] f2713s = new a[0];
        public final Observer<? super U> b;
        public final Function<? super T, ? extends ObservableSource<? extends U>> c;
        public final boolean d;

        /* renamed from: e  reason: collision with root package name */
        public final int f2714e;

        /* renamed from: f  reason: collision with root package name */
        public final int f2715f;
        public volatile SimplePlainQueue<U> g;
        public volatile boolean h;

        /* renamed from: i  reason: collision with root package name */
        public final AtomicThrowable f2716i = new AtomicThrowable();

        /* renamed from: j  reason: collision with root package name */
        public volatile boolean f2717j;

        /* renamed from: k  reason: collision with root package name */
        public final AtomicReference<a<?, ?>[]> f2718k;

        /* renamed from: l  reason: collision with root package name */
        public Disposable f2719l;

        /* renamed from: m  reason: collision with root package name */
        public long f2720m;

        /* renamed from: n  reason: collision with root package name */
        public long f2721n;

        /* renamed from: o  reason: collision with root package name */
        public int f2722o;

        /* renamed from: p  reason: collision with root package name */
        public Queue<ObservableSource<? extends U>> f2723p;

        /* renamed from: q  reason: collision with root package name */
        public int f2724q;

        public b(Observer<? super U> observer, Function<? super T, ? extends ObservableSource<? extends U>> function, boolean z, int i2, int i3) {
            this.b = observer;
            this.c = function;
            this.d = z;
            this.f2714e = i2;
            this.f2715f = i3;
            if (i2 != Integer.MAX_VALUE) {
                this.f2723p = new ArrayDeque(i2);
            }
            this.f2718k = new AtomicReference<>(f2712r);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMap$b] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.f2719l, disposable)) {
                this.f2719l = disposable;
                this.b.a((Disposable) this);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.ObservableSource<? extends U>, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void b(T t2) {
            if (!this.h) {
                try {
                    Object a = this.c.a(t2);
                    ObjectHelper.a((Object) a, "The mapper returned a null ObservableSource");
                    ObservableSource observableSource = (ObservableSource) a;
                    if (this.f2714e != Integer.MAX_VALUE) {
                        synchronized (this) {
                            if (this.f2724q == this.f2714e) {
                                this.f2723p.offer(observableSource);
                                return;
                            }
                            this.f2724q++;
                        }
                    }
                    a(observableSource);
                } catch (Throwable th) {
                    c.c(th);
                    this.f2719l.f();
                    a(th);
                }
            }
        }

        public boolean c() {
            a<?, ?>[] aVarArr;
            this.f2719l.f();
            a<?, ?>[] aVarArr2 = (a[]) this.f2718k.get();
            a<?, ?>[] aVarArr3 = f2713s;
            int i2 = 0;
            if (aVarArr2 == aVarArr3 || (aVarArr = (a[]) this.f2718k.getAndSet(aVarArr3)) == f2713s) {
                return false;
            }
            int length = aVarArr.length;
            while (i2 < length) {
                a<?, ?> aVar = aVarArr[i2];
                if (aVar != null) {
                    DisposableHelper.a(aVar);
                    i2++;
                } else {
                    throw null;
                }
            }
            return true;
        }

        public void d() {
            if (getAndIncrement() == 0) {
                e();
            }
        }

        public void e() {
            int i2;
            int i3;
            Observer<? super U> observer = this.b;
            int i4 = 1;
            while (!b()) {
                SimplePlainQueue<U> simplePlainQueue = this.g;
                if (simplePlainQueue != null) {
                    while (!b()) {
                        U poll = simplePlainQueue.poll();
                        if (poll != null) {
                            observer.b(poll);
                        }
                    }
                    return;
                }
                boolean z = this.h;
                SimplePlainQueue<U> simplePlainQueue2 = this.g;
                a[] aVarArr = (a[]) this.f2718k.get();
                int length = aVarArr.length;
                int i5 = 0;
                if (this.f2714e != Integer.MAX_VALUE) {
                    synchronized (this) {
                        i2 = this.f2723p.size();
                    }
                } else {
                    i2 = 0;
                }
                if (!z || !((simplePlainQueue2 == null || simplePlainQueue2.isEmpty()) && length == 0 && i2 == 0)) {
                    if (length != 0) {
                        long j2 = this.f2721n;
                        int i6 = this.f2722o;
                        if (length <= i6 || aVarArr[i6].b != j2) {
                            if (length <= i6) {
                                i6 = 0;
                            }
                            for (int i7 = 0; i7 < length && aVarArr[i6].b != j2; i7++) {
                                i6++;
                                if (i6 == length) {
                                    i6 = 0;
                                }
                            }
                            this.f2722o = i6;
                            this.f2721n = aVarArr[i6].b;
                        }
                        int i8 = 0;
                        int i9 = 0;
                        while (i8 < length) {
                            if (!b()) {
                                a aVar = aVarArr[i6];
                                SimpleQueue<U> simpleQueue = aVar.f2710e;
                                if (simpleQueue != null) {
                                    while (true) {
                                        try {
                                            U poll2 = simpleQueue.poll();
                                            if (poll2 == null) {
                                                break;
                                            }
                                            observer.b(poll2);
                                            if (b()) {
                                                return;
                                            }
                                        } catch (Throwable th) {
                                            c.c(th);
                                            DisposableHelper.a(aVar);
                                            AtomicThrowable atomicThrowable = this.f2716i;
                                            if (atomicThrowable != null) {
                                                ExceptionHelper.a(atomicThrowable, th);
                                                if (!b()) {
                                                    a(aVar);
                                                    i9++;
                                                    i3 = i6 + 1;
                                                    if (i3 != length) {
                                                    }
                                                } else {
                                                    return;
                                                }
                                            } else {
                                                throw null;
                                            }
                                        }
                                    }
                                }
                                boolean z2 = aVar.d;
                                SimpleQueue<U> simpleQueue2 = aVar.f2710e;
                                if (z2 && (simpleQueue2 == null || simpleQueue2.isEmpty())) {
                                    a(aVar);
                                    if (!b()) {
                                        i9++;
                                    } else {
                                        return;
                                    }
                                }
                                i3 = i6 + 1;
                                if (i3 != length) {
                                    i8++;
                                }
                                i3 = 0;
                                i8++;
                            } else {
                                return;
                            }
                        }
                        this.f2722o = i6;
                        this.f2721n = aVarArr[i6].b;
                        i5 = i9;
                    }
                    if (i5 == 0) {
                        i4 = addAndGet(-i4);
                        if (i4 == 0) {
                            return;
                        }
                    } else if (this.f2714e != Integer.MAX_VALUE) {
                        while (true) {
                            int i10 = i5 - 1;
                            if (i5 == 0) {
                                continue;
                                break;
                            }
                            synchronized (this) {
                                ObservableSource poll3 = this.f2723p.poll();
                                if (poll3 == null) {
                                    this.f2724q--;
                                } else {
                                    a(poll3);
                                }
                            }
                            i5 = i10;
                        }
                        while (true) {
                        }
                    } else {
                        continue;
                    }
                } else {
                    AtomicThrowable atomicThrowable2 = this.f2716i;
                    if (atomicThrowable2 != null) {
                        Throwable a = ExceptionHelper.a(atomicThrowable2);
                        if (a == ExceptionHelper.a) {
                            return;
                        }
                        if (a == null) {
                            observer.a();
                            return;
                        } else {
                            observer.a(a);
                            return;
                        }
                    } else {
                        throw null;
                    }
                }
            }
        }

        public void f() {
            if (!this.f2717j) {
                this.f2717j = true;
                if (c()) {
                    AtomicThrowable atomicThrowable = this.f2716i;
                    if (atomicThrowable != null) {
                        Throwable a = ExceptionHelper.a(atomicThrowable);
                        if (a != null && a != ExceptionHelper.a) {
                            c.b(a);
                            return;
                        }
                        return;
                    }
                    throw null;
                }
            }
        }

        public boolean g() {
            return this.f2717j;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
            if (decrementAndGet() == 0) goto L_0x0011;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(l.b.ObservableSource<? extends U> r8) {
            /*
                r7 = this;
            L_0x0000:
                boolean r0 = r8 instanceof java.util.concurrent.Callable
                r1 = 0
                r2 = 1
                if (r0 == 0) goto L_0x0093
                java.util.concurrent.Callable r8 = (java.util.concurrent.Callable) r8
                r0 = 2147483647(0x7fffffff, float:NaN)
                java.lang.Object r8 = r8.call()     // Catch:{ all -> 0x0061 }
                if (r8 != 0) goto L_0x0013
            L_0x0011:
                r8 = 1
                goto L_0x0070
            L_0x0013:
                int r3 = r7.get()
                if (r3 != 0) goto L_0x002b
                boolean r3 = r7.compareAndSet(r1, r2)
                if (r3 == 0) goto L_0x002b
                l.b.Observer<? super U> r3 = r7.b
                r3.b(r8)
                int r8 = r7.decrementAndGet()
                if (r8 != 0) goto L_0x005d
                goto L_0x0011
            L_0x002b:
                l.b.u.c.SimplePlainQueue<U> r3 = r7.g
                if (r3 != 0) goto L_0x0044
                int r3 = r7.f2714e
                if (r3 != r0) goto L_0x003b
                l.b.u.f.SpscLinkedArrayQueue r3 = new l.b.u.f.SpscLinkedArrayQueue
                int r4 = r7.f2715f
                r3.<init>(r4)
                goto L_0x0042
            L_0x003b:
                l.b.u.f.SpscArrayQueue r3 = new l.b.u.f.SpscArrayQueue
                int r4 = r7.f2714e
                r3.<init>(r4)
            L_0x0042:
                r7.g = r3
            L_0x0044:
                boolean r8 = r3.offer(r8)
                if (r8 != 0) goto L_0x0055
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r3 = "Scalar queue full?!"
                r8.<init>(r3)
                r7.a(r8)
                goto L_0x0011
            L_0x0055:
                int r8 = r7.getAndIncrement()
                if (r8 == 0) goto L_0x005d
                r8 = 0
                goto L_0x0070
            L_0x005d:
                r7.e()
                goto L_0x0011
            L_0x0061:
                r8 = move-exception
                j.c.a.a.c.n.c.c(r8)
                l.b.u.h.AtomicThrowable r3 = r7.f2716i
                if (r3 == 0) goto L_0x0091
                l.b.u.h.ExceptionHelper.a(r3, r8)
                r7.d()
                goto L_0x0011
            L_0x0070:
                if (r8 == 0) goto L_0x00c7
                int r8 = r7.f2714e
                if (r8 == r0) goto L_0x00c7
                monitor-enter(r7)
                java.util.Queue<l.b.ObservableSource<? extends U>> r8 = r7.f2723p     // Catch:{ all -> 0x008e }
                java.lang.Object r8 = r8.poll()     // Catch:{ all -> 0x008e }
                l.b.ObservableSource r8 = (l.b.ObservableSource) r8     // Catch:{ all -> 0x008e }
                if (r8 != 0) goto L_0x0087
                int r0 = r7.f2724q     // Catch:{ all -> 0x008e }
                int r0 = r0 - r2
                r7.f2724q = r0     // Catch:{ all -> 0x008e }
                r1 = 1
            L_0x0087:
                monitor-exit(r7)     // Catch:{ all -> 0x008e }
                if (r1 == 0) goto L_0x0000
                r7.d()
                goto L_0x00c7
            L_0x008e:
                r8 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x008e }
                throw r8
            L_0x0091:
                r8 = 0
                throw r8
            L_0x0093:
                l.b.u.e.c.ObservableFlatMap$a r0 = new l.b.u.e.c.ObservableFlatMap$a
                long r3 = r7.f2720m
                r5 = 1
                long r5 = r5 + r3
                r7.f2720m = r5
                r0.<init>(r7, r3)
            L_0x009f:
                java.util.concurrent.atomic.AtomicReference<l.b.u.e.c.ObservableFlatMap$a<?, ?>[]> r3 = r7.f2718k
                java.lang.Object r3 = r3.get()
                l.b.u.e.c.ObservableFlatMap$a[] r3 = (l.b.u.e.c.ObservableFlatMap.a[]) r3
                l.b.u.e.c.ObservableFlatMap$a<?, ?>[] r4 = l.b.u.e.c.ObservableFlatMap.b.f2713s
                if (r3 != r4) goto L_0x00af
                l.b.u.a.DisposableHelper.a(r0)
                goto L_0x00c2
            L_0x00af:
                int r4 = r3.length
                int r5 = r4 + 1
                l.b.u.e.c.ObservableFlatMap$a[] r5 = new l.b.u.e.c.ObservableFlatMap.a[r5]
                java.lang.System.arraycopy(r3, r1, r5, r1, r4)
                r5[r4] = r0
                java.util.concurrent.atomic.AtomicReference<l.b.u.e.c.ObservableFlatMap$a<?, ?>[]> r4 = r7.f2718k
                boolean r3 = r4.compareAndSet(r3, r5)
                if (r3 == 0) goto L_0x009f
                r1 = 1
            L_0x00c2:
                if (r1 == 0) goto L_0x00c7
                r8.a(r0)
            L_0x00c7:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFlatMap.b.a(l.b.ObservableSource):void");
        }

        public boolean b() {
            if (this.f2717j) {
                return true;
            }
            Throwable th = (Throwable) this.f2716i.get();
            if (this.d || th == null) {
                return false;
            }
            c();
            AtomicThrowable atomicThrowable = this.f2716i;
            if (atomicThrowable != null) {
                Throwable a = ExceptionHelper.a(atomicThrowable);
                if (a != ExceptionHelper.a) {
                    this.b.a(a);
                }
                return true;
            }
            throw null;
        }

        public void a(a<T, U> aVar) {
            a<T, U>[] aVarArr;
            a<?, ?>[] aVarArr2;
            do {
                aVarArr = (a[]) this.f2718k.get();
                int length = aVarArr.length;
                if (length != 0) {
                    int i2 = -1;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            break;
                        } else if (aVarArr[i3] == aVar) {
                            i2 = i3;
                            break;
                        } else {
                            i3++;
                        }
                    }
                    if (i2 >= 0) {
                        if (length == 1) {
                            aVarArr2 = f2712r;
                        } else {
                            a<?, ?>[] aVarArr3 = new a[(length - 1)];
                            System.arraycopy(aVarArr, 0, aVarArr3, 0, i2);
                            System.arraycopy(aVarArr, i2 + 1, aVarArr3, i2, (length - i2) - 1);
                            aVarArr2 = aVarArr3;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } while (!this.f2718k.compareAndSet(aVarArr, aVarArr2));
        }

        public void a(Throwable th) {
            if (this.h) {
                c.b(th);
                return;
            }
            AtomicThrowable atomicThrowable = this.f2716i;
            if (atomicThrowable == null) {
                throw null;
            } else if (ExceptionHelper.a(atomicThrowable, th)) {
                this.h = true;
                d();
            } else {
                c.b(th);
            }
        }

        public void a() {
            if (!this.h) {
                this.h = true;
                d();
            }
        }
    }

    public static final class a<T, U> extends AtomicReference<l.b.s.b> implements Observer<U> {
        public final long b;
        public final b<T, U> c;
        public volatile boolean d;

        /* renamed from: e  reason: collision with root package name */
        public volatile SimpleQueue<U> f2710e;

        /* renamed from: f  reason: collision with root package name */
        public int f2711f;

        public a(b<T, U> bVar, long j2) {
            this.b = j2;
            this.c = bVar;
        }

        /* JADX WARN: Type inference failed for: r3v1, types: [l.b.u.c.QueueDisposable, l.b.u.c.SimpleQueue<U>] */
        public void a(Disposable disposable) {
            if (DisposableHelper.b(super, disposable) && (disposable instanceof QueueDisposable)) {
                ? r3 = (QueueDisposable) disposable;
                int a = r3.a(7);
                if (a == 1) {
                    this.f2711f = a;
                    this.f2710e = r3;
                    this.d = true;
                    this.c.d();
                } else if (a == 2) {
                    this.f2711f = a;
                    this.f2710e = r3;
                }
            }
        }

        public void b(U u) {
            if (this.f2711f == 0) {
                b<T, U> bVar = this.c;
                if (bVar.get() != 0 || !bVar.compareAndSet(0, 1)) {
                    SimpleQueue simpleQueue = this.f2710e;
                    if (simpleQueue == null) {
                        simpleQueue = new SpscLinkedArrayQueue(bVar.f2715f);
                        this.f2710e = simpleQueue;
                    }
                    simpleQueue.offer(u);
                    if (bVar.getAndIncrement() != 0) {
                        return;
                    }
                } else {
                    bVar.b.b(u);
                    if (bVar.decrementAndGet() == 0) {
                        return;
                    }
                }
                bVar.e();
                return;
            }
            this.c.d();
        }

        public void a(Throwable th) {
            AtomicThrowable atomicThrowable = this.c.f2716i;
            if (atomicThrowable == null) {
                throw null;
            } else if (ExceptionHelper.a(super, th)) {
                b<T, U> bVar = this.c;
                if (!bVar.d) {
                    bVar.c();
                }
                this.d = true;
                this.c.d();
            } else {
                c.b(th);
            }
        }

        public void a() {
            this.d = true;
            this.c.d();
        }
    }
}
