package l.b.u.e.c;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;

public final class ObservableOnErrorReturn<T> extends AbstractObservableWithUpstream<T, T> {
    public final Function<? super Throwable, ? extends T> c;

    public ObservableOnErrorReturn(ObservableSource<T> observableSource, Function<? super Throwable, ? extends T> function) {
        super(observableSource);
        this.c = function;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer, this.c));
    }

    public static final class a<T> implements Observer<T>, b {
        public final Observer<? super T> b;
        public final Function<? super Throwable, ? extends T> c;
        public Disposable d;

        public a(Observer<? super T> observer, Function<? super Throwable, ? extends T> function) {
            this.b = observer;
            this.c = function;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableOnErrorReturn$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            this.b.b(t2);
        }

        public void f() {
            this.d.f();
        }

        public boolean g() {
            return this.d.g();
        }

        public void a(Throwable th) {
            try {
                Object a = this.c.a(th);
                if (a == null) {
                    NullPointerException nullPointerException = new NullPointerException("The supplied value is null");
                    nullPointerException.initCause(th);
                    this.b.a(nullPointerException);
                    return;
                }
                this.b.b(a);
                this.b.a();
            } catch (Throwable th2) {
                c.c(th2);
                this.b.a(new CompositeException(th, th2));
            }
        }

        public void a() {
            this.b.a();
        }
    }
}
