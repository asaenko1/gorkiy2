package l.b.u.e.d;

import java.util.concurrent.atomic.AtomicReference;
import l.b.Scheduler;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.l;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class SingleObserveOn<T> extends Single<T> {
    public final SingleSource<T> a;
    public final Scheduler b;

    public SingleObserveOn(SingleSource<T> singleSource, l lVar) {
        this.a = singleSource;
        this.b = lVar;
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.a.a(new a(singleObserver, this.b));
    }

    public static final class a<T> extends AtomicReference<b> implements SingleObserver<T>, b, Runnable {
        public final SingleObserver<? super T> b;
        public final Scheduler c;
        public T d;

        /* renamed from: e  reason: collision with root package name */
        public Throwable f2750e;

        public a(SingleObserver<? super T> singleObserver, l lVar) {
            this.b = singleObserver;
            this.c = lVar;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.u.e.d.SingleObserveOn$a, java.util.concurrent.atomic.AtomicReference] */
        public void a(Disposable disposable) {
            if (DisposableHelper.b(super, disposable)) {
                this.b.a((Disposable) this);
            }
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public void run() {
            Throwable th = this.f2750e;
            if (th != null) {
                this.b.a(th);
            } else {
                this.b.a((Object) this.d);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
         arg types: [l.b.u.e.d.SingleObserveOn$a, l.b.s.Disposable]
         candidates:
          l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
          l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
        public void a(T t2) {
            this.d = t2;
            DisposableHelper.a((AtomicReference<b>) super, (b) this.c.a(this));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
         arg types: [l.b.u.e.d.SingleObserveOn$a, l.b.s.Disposable]
         candidates:
          l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
          l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
        public void a(Throwable th) {
            this.f2750e = th;
            DisposableHelper.a((AtomicReference<b>) super, (b) this.c.a(this));
        }
    }
}
