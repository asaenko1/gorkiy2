package l.b.u.e.d;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.Disposable;
import l.b.t.Function;

public final class SingleOnErrorReturn<T> extends Single<T> {
    public final SingleSource<? extends T> a;
    public final Function<? super Throwable, ? extends T> b;
    public final T c;

    public SingleOnErrorReturn(SingleSource<? extends T> singleSource, Function<? super Throwable, ? extends T> function, T t2) {
        this.a = singleSource;
        this.b = function;
        this.c = t2;
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.a.a(new a(singleObserver));
    }

    public final class a implements SingleObserver<T> {
        public final SingleObserver<? super T> b;

        public a(SingleObserver<? super T> singleObserver) {
            this.b = singleObserver;
        }

        public void a(Throwable th) {
            T t2;
            SingleOnErrorReturn singleOnErrorReturn = SingleOnErrorReturn.this;
            Function<? super Throwable, ? extends T> function = singleOnErrorReturn.b;
            if (function != null) {
                try {
                    t2 = function.a(th);
                } catch (Throwable th2) {
                    c.c(th2);
                    this.b.a((Throwable) new CompositeException(th, th2));
                    return;
                }
            } else {
                t2 = singleOnErrorReturn.c;
            }
            if (t2 == null) {
                NullPointerException nullPointerException = new NullPointerException("Value supplied was null");
                nullPointerException.initCause(th);
                this.b.a((Throwable) nullPointerException);
                return;
            }
            this.b.a((Object) t2);
        }

        public void a(Disposable disposable) {
            this.b.a(disposable);
        }

        public void a(T t2) {
            this.b.a((Object) t2);
        }
    }
}
