package l.b;

import l.b.s.Disposable;

public interface CompletableObserver {
    void a();

    void a(Throwable th);

    void a(Disposable disposable);
}
